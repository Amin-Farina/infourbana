---
title: "¿Lionel Messi quiere irse del Barcelona?: las versiones sobre su futuro que tienen en vilo a España"
date: 2018-02-13T14:51:12+06:00
author: 
image_webp: images/deportes/messi.webp
image: images/deportes/messi.jpg
description : "Luego del 8-2 que le propinó el Bayern Múnich, el goleador argentino podría dar el protazo y marcar el fin de un ciclo"
---

El Barcelona podría estar a un paso de iniciar la peor crisis de su historia si es que la información que llega desde España es cierta y Lionel Messi ya le comunicó a la dirigencia del club que quiere dar el portazo tras el 8-2 sufrido frente al Bayern Múnich en los cuartos de final de la Champions League.

La masacre futbolística que padeció el conjunto catalán en Lisboa será sin dudas un antes y un después en este ciclo que ahora iniciará una renovación de vestuario, que según adelanta la prensa europea, incluirá nombres de peso como los de Luis Suárez, Gerard Piqué, Jordi Alba e Ivan Rakitic, entre otros. Sin embargo, el apellido del argentino jamás fue cuestionado ni puesto en esa lista, porque el objetivo del presidente Josep María Bartomeu y el deseo de los aficionados es que La Pulga sea el líder de este recambio

Al parecer, este plan podría derrumbarse porque según reveló el periodista brasileño Marcelo Bechler, el delantero quiere abandonar el club azulgrana: **“La información que tenemos es que Lionel Messi quiere dejar Barcelona ya y la directiva ya fue comunicada sobre eso”**. De inmediato, la frase del corresponsal del diario Esporte Interativo repercutió en los principales portales del planeta y varios periodistas catalanes se hicieron eco de lo que hasta entonces era un rumor.

Bechler, cuya data fue replicada por el sitio británico The Sun, apuntó que una persona del club, muy cercana al argentino, le dijo: **“Nunca vi a Messi tan fuera del Barcelona en tantos años aquí dentro como ahora”.**

