+++
author = ""
date = 2021-02-09T03:00:00Z
description = ""
image = "/images/andres-arauz.jpg"
image_webp = "/images/andres-arauz.jpg"
title = "Elecciones en Ecuador: Guillermo Lasso y Yaku Pérez disputan quién ingresa al ballotage con Andrés Arauz"

+++

**_Sorpresa en Ecuador. Mientras todos los sondeos a boca de urna ubicaban a Guillermo Lasso, el candidato de CREO, como claro competidor en la segunda vuelta electoral que se enfrentaría contra Andrés Arauz, el delfín de Rafael Correa, el conteo rápido del Consejo Nacional Electoral ubicó en segundo puesto al indígena Yaku Pérez. Sin embargo, el margen con Lasso es demasiado estrecho, y los contrincantes para el ballotage del 11 de abril próximo podrían cambiar en cuestión de minutos._**

Para obtener la presidencia en primera ronda, en Ecuador se necesita la mitad más uno de los votos válidos, o al menos un 40% con una diferencia de diez puntos por encima del segundo.

Tras procesar los datos del 90,4% de las actas seleccionadas para la muestra de conteo rápido se determinó que la lista 1-5 de Arauz logró votos válidos del 31,50%, seguido por la lista 18 de Pérez, con 20,04%, y Lasso obtuvo 19,97% de los votos válidos. Así lo informó la presidenta del CNE, Diana Antamaint, en una cadena de televisión en la que ofreció los porcentajes de votos válidos con un límite inferior de 30,55% y uno superior de 32,44% para Arauz; de 19,09% y 20,98 para Pérez y de 19,20% y 20,75% para Lasso.

Luego, con el 65% de las mesas escrutadas, los resultados mantienen la tendencia: Arauz, 31,8%; Pérez, 20,5%; y Lasso,19,2 por ciento.