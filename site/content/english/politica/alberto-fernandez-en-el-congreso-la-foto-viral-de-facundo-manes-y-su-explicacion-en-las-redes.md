+++
author = ""
date = 2022-03-01T03:00:00Z
description = ""
image = "/images/facundo-manes-solo-en-el-recinto.jpg"
image_webp = "/images/facundo-manes-solo-en-el-recinto.jpg"
title = "ALBERTO FERNÁNDEZ, EN EL CONGRESO: LA FOTO VIRAL DE FACUNDO MANES Y SU EXPLICACIÓN EN LAS REDES"

+++

##### **_El diputado nacional de Juntos por el Cambio se quedó en su banca cuando el bloque del Pro abandonó el recinto; “No coincido con muchas cosas del discurso, pero si no lo escucho no puedo opinar”, explicó_**

La apertura de sesiones ordinarias en el Congreso generó una catarata de repercusiones. El nivel de rechazo de la oposición a las palabras del presidente Alberto Fernández frente a la Asamblea Legislativa quedó plasmado cuando el bloque del Pro se levantó y abandonó el recinto en plena alocución. Con gran parte del hemiciclo vacío, la presencia del diputado nacional radical, Facundo Manes, en su banca, se volvió viral en cuestión de minutos.

Así, frente a un recinto medio vacío, Manes siguió el discurso del Presidente rodeado de bancas que lucían banderas de Ucrania, en señal de apoyo ante la invasión de tropas rusas. La postal del médico neurólogo solo fue capturada por las cámaras que transmitían la señal desde diputados y se volvió viral.

> “Me preguntan por qué me quedé”, salió a responder Manes en las redes. “Ya probamos con no escucharnos y así estamos”, continuó, y remarcó: “No coincido con muchas cosas del discurso, pero si no lo escucho no puedo opinar”.

De esta manera, Manes consideró que el desarrollo inclusivo es parte de una simple ecuación: “Más empatía y menos grieta”. “La Argentina del futuro será de los que nos quedamos”, cerró.

Si bien Juntos por el Cambio emitió un comunicado con críticas al Presidente, el único bloque que abandonó el recinto fue el del Pro, mientras que los diputados de la Unión Cívica Radical (UCR) y la Coalición Cívica (CC-Ari) quedaron en sus lugares hasta que Fernández terminó su discurso.

##### Cruce entre Cornejo y el Presidente

Con anteojos de marco metálico y lentes redondos, el Presidente leyó un discurso con ideas de gestión y críticas a su antecesor, Mauricio Macri. Sus filosos cuestionamientos vieron la salida de decenas de parlamentarios, quienes objetaron sus palabras. “Yo no miento, Alfredo”, se defendió el mandatario, luego de haber sido interpelado por el diputado mendocino, Alfredo Cornejo.

“Vos no tenés los votos”, exclamó Cornejo a Alberto Fernández, mientras se escuchaban otras voces que acusaban de “mentiroso” al mandatario.

Tras haberse levantado de sus bancas, los legisladores del Pro justificaron su decisión a través de las redes. “Me levanté junto a otros diputados de la sesión de apertura de sesiones del Congreso. No se pueden consentir tantas mentiras e hipocresía”, posteó Waldo Wolff.

“Nos fuimos del recinto porque las mentiras del Presidente no se pueden aceptar pasivamente. Nos retiramos bajo los insultos de los mismos que aplauden al presidente pero no quieren votarle el acuerdo con el FMI. Orgullo republicano”, expresó Fernando Iglesias.

Por su parte, María Eugenia Vidal publicó en sus redes un comunicado firmado por el bloque de Diputados y Senadores del PRO, donde se habló acerca de las “injustificables acusaciones realizadas por el Presidente Alberto Fernández contra la gestión del ex Presidente Mauricio Macri”. “Insulta la inteligencia de los argentinos”, apuntaron.

Horas después, Juntos por el Cambio publicó un comunicado que fue difundido por la tarde. “Los argentinos no sabemos de qué país habló el presidente Alberto Fernández en la apertura de las sesiones ordinarias”, apuntaron, y remarcaron: “No planteó soluciones a los problemas más urgentes”.

Puntualmente, al analizar la filosa crítica del mandatario al analizar el acuerdo con el Fondo Monetario Internacional (FMI), Juntos por el Cambio consideró: “\[El Presidente\] no solo no lo dio a conocer sino que tuvo una actitud provocadora y violenta contra la oposición, que resulta inaceptable para un gobierno que busca un acuerdo de todos”.