+++
author = ""
date = 2021-10-01T03:00:00Z
description = ""
image = "/images/macri2-1.jpg"
image_webp = "/images/macri2.jpg"
title = "CITAN A INDAGATORIA A MAURICIO MACRI"

+++

#### **_El juez federal de Dolores Martín Bava lo citó para el 7 de octubre y prohibió su salida del país mediante un fallo en el que sostiene que existió "un interés político nacional que en el final de la cadena, respondía al entonces Presidente de la Nación"._**

El expresidente Mauricio Macri fue citado hoy a declaración indagatoria con prohibición de salida del país en la causa que investiga espionaje ilegal a familiares de los 44 tripulantes fallecidos en el hundimiento del submarino ARA San Juan en 2017

La decisión fue del juez federal subrogante de Dolores, Martín Bava, quien lo convocó para el próximo 7 de octubre a las 11.00, según la resolución a la que tuvo acceso Télam.

"Es claro que estas acciones ilegales no fueron ejecutadas por los agentes de base que por motu propio realizaron este espionaje ilegal sino, por el contrario, partieron de un interés político nacional que en el final de la cadena, respondía al entonces Presidente de la Nación, Ing. Mauricio Macri", advirtió Bava en el fallo de 166 carillas.

Se trata de la primera citación a declarar en calidad de imputado para Macri desde que dejó la Presidencia de la Nación.

En el mismo fallo, el juez procesó sin prisión preventiva al extitular de la AFI en el gobierno de Cambiemos, Gustavo Arribas, y a la su ex segunda en el organismo, Silvia Majdalani con embargos de 20 millones de pesos para cada uno y a otros imputados.

El espionaje ilegal "se hizo con el objetivo de anticiparle a la máxima autoridad del Estado en ese entonces, cuáles serían los reclamos del colectivo de familiares quienes para ese entonces, eran los protagonistas de uno de los temas de mayor trascendencia nacional", remarcó Bava en su fallo.

"Todo esto es de una gravedad inconmensurable para nuestro país, nuestra democracia y nuestra historia, y como dije, es deber delpoder judicial investigarlo y sancionarlo", agregó.

La "gravedad de los hechos aquí ventilados constituye no solo una conculcación al sistema democrático, sino que vulnera los compromisos asumidos internacionalmente por el Estado Argentino, por inmiscuirse en la vida privada e intimidad de éstas mujeres y de las familias que estaban atravesando uno de los mayores golpes de su vida, como es la desaparición de un ser querido", evaluó.

En el capítulo de la resolución dedicado a la decisión de citar a indagatoria a Macri, el juez sostuvo que, en principio, "determinar la transgresión a la Ley 25520 (ley de Inteligencia) es identificar los límites del poder del Estado para con todos los ciudadanos de la República Argentina".

"Corresponde detallar que se le imputa desde su cargo de Presidente de la Nación, por lo menos en el período comprendido entre el mes de diciembre del año 2017 y finales del año 2018 ha ordenado y posibilitado la realización sistemática de tareas de inteligencia expresamente prohibidas por la ley 25.520 y sus modificatorias", detalló la resolución.

La causa por el espionaje a los familiares de las víctimas del hundimiento del San Juan se inició por una denuncia formulada por la interventora de la AFI, Cristina Caamaño, quien presentó ante la justicia de Mar del Plata elementos que permitían inferir que durante el Gobierno de Macri se los había espiado.

Los elementos que dieron origen a la denuncia fueron hallados en la base Mar del Plata de la AFI tras un requerimiento de información formulado por el juez Ramos Padilla en el marco del caso D'Alessio, más precisamente en el capítulo investigativo dedicado a las denominadas bases AMBA.

En esta causa, un exjefe de la base de la AFI en Mar del Plata presentó un escrito en el que afirmó que esa delegación solía trabajar con la Avanzada Presidencial cuando Macri visitaba alguna localidad dentro de esa jurisdicción, según pudo reconstruir Télam.

En el expediente hay elementos que acreditan que en las base de Mar del Plata se encontraron informes y fotografías que daban cuenta de seguimientos realizados sobre familiares de los tripulantes, producidos sin autorización judicial ni justificación válida alguna, según pudo saber esta agencia.