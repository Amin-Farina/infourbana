+++
author = ""
date = 2022-01-14T03:00:00Z
description = ""
image = "/images/405991-1.jpg"
image_webp = "/images/405991.jpg"
title = "SEGÚN EL MINISTRO DE SEGURIDAD PROVINCIAL, SERGIO BERNI: “POR PRIMERA VEZ SE HA CONSEGUIDO BAJAR EL DELITO DE MANERA CONTUNDENTE”"

+++

##### **_El ministro de Seguridad de la provincia de Buenos Aires se ocupa de supervisar a diario el dispositivo que controla el delito en la Costa Atlántica en los meses de temporada de verano y que se lanzó el 20 de diciembre._** 

Recorre diferentes destinos turísticos para reunirse con los jefes policiales de cada zona, con otros funcionarios, con intendentes y con empresarios. La agenda dinámica que maneja lo llevó a visitar en los últimos días Santa Clara del Mar, Bahía Blanca y Miramar, entre otras localidades.

“Es una temporada récord. Hace más de 12 años que no había tanto afluente de turismo en la costa y para nosotros es un desafío no solamente generar las medidas de seguridad, sino, por sobre todas las cosas, trabajar muy fuerte en lo que es la prevención en la nocturnidad para llevarle tranquilidad a las familias de los chicos que vienen a disfrutar de toda la movida”, dice Berni, que se subió al helicóptero para sobrevolar las playas de Pinamar y Villa Gesell. Infobae participó de la recorrida.

El camino inició a la altura de La Frontera, la zona de médanos donde se producen reiterados accidentes de cuatriciclos, y se extendió a lo largo de toda la línea costera pinamarense y geselina. Después de los saludos a los veraneantes que disfrutan de un chapuzón en el agua, el ministro se mostró satisfecho por el dispositivo de seguridad: “Hay muy buenos resultados. La gente se siente respaldada, cuidada”, subraya.

Berni hizo también una evaluación de los dos primeros años de su gestión al mando de la cartera de Seguridad y resaltó que “por primera vez se ha conseguido bajar el delito de manera contundente”.

> “Sin hablar del 2020, por haber sido un año atípico por la pandemia, en 2021 estuvimos casi un 25% por debajo de todos los números del 2019. Obviamente que el 25% parece una gota en el océano en una provincia que tiene una proyección de 1.100 homicidios en ocasión de robo. Pero es un esfuerzo que alguna vez había que hacer. Es un hito que había que marcar y a partir de allí seguir con la tendencia a la baja hasta que todos los actores que tienen responsabilidad en la generación de seguridad comprendan y asuman su responsabilidad”, repasó.

En esa línea, dejó un mensaje para los intendentes, después de haberlos criticado porque, según su mirada, “no entienden que su responsabilidad no se limita a poner cámaras”.

> “La seguridad no es un concepto abstracto. Es un concepto dinámico, multiagencial, multidisciplinario y que se construye día a día. En esa construcción de seguridad, los intendentes tienen una gran responsabilidad porque la génesis del delito no empieza solo en las familias, sino también en su entorno, en el barrio. Por eso deben generar políticas de inclusión, de trabajo. Es imposible generar políticas de seguridad por fuera de un marco de inclusión”, sostuvo.

El operativo De Sol a Sol abarca a más de 17 mil policías -casi el doble de los que se movilizaron en la temporada 2021-, con la intervención de personal de Infantería, Caballería, Drogas Ilícitas, Delitos Complejos y Seguridad Siniestral, entre otras divisiones. El AS-350 B3 es parte del despliegue y está disponible para casos de patrullajes programados, trasladado de grupos especiales, incendios y otras urgencias -hasta lo que va de la temporada, no surgió ninguna, cuentan los pilotos-.

El recorrido finalizó con maniobras tácticas sobrevolando las dunas y el bosque del norte de Pinamar. Luego, Berni se instaló en la casa rodante donde pasa sus noches, tomó una gaseosa y respondió.

— Día a día se registran números altos de casos de COVID como nunca antes y la tendencia, según parece, se va a mantener al menos por un tiempo más. ¿Puede haber alguna medida restrictiva o la época de restricciones se terminó definitivamente?

— Lo único predecible del COVID es su imprevisibilidad. Pero dentro de eso a mí me parece que prácticamente no hay posibilidad de medidas restrictivas. Lo que nos ha permitido que hoy estemos acá, con 14 millones de personas veraneando, fue la vacuna, que ha permitido que el motor de la economía siga prendido a pesar de 130 mil contagios diarios, que gracias a Dios son leves. Aquellos que están internados en terapia intensiva son, en su gran mayoría, los que no se han vacunado o completado el sistema de vacunación. Por eso para nosotros es un orgullo poder estar llevando adelante esta temporada.

No nos olvidemos que fue el gobernador de la provincia de Buenos Aires quien comenzó las gestiones cuando todos estaban incrédulos con respecto a la aparición de una vacuna. Fue su equipo el que desplegó todos sus contactos a lo largo y a lo ancho del planeta y pudimos seguir el avance de las investigación, dar con la vacuna y ponerla a disposición de los argentinos. Eso fue un mérito del gobernador de la provincia de Buenos Aires y, por sobre todas las cosas, de los bonaerenses. Fuimos una de las provincias que más rápido ha aceptado la campaña de vacunación y se ha inmunizado rápidamente.

— ¿Cómo está impactando la escalada de casos al cuerpo policial? Hay locales gastronómicos que tuvieron que cerrar por un brote interno, en hospitales tuvieron que lidiar con la falta de personal...

— Obviamente los policías son lo que más contacto tienen con la gente y más variado, por lo tanto son los más afectados. Durante la pandemia, cuando no había vacuna, tuvimos casi 110 muertos, hombres y mujeres que estaban trabajando en los barrios más castigados por el COVID. Y hoy gracias a Dios y a que todos están vacunados, se hace mucho más llevadera la enfermedad, sin grandes complicaciones. El único que tuvimos internado en terapia intensiva, no estaba vacunado. La logística que implementamos para el despliegue de 17 mil hombres y mujeres es muy grande. La Costa Atlántica está colmada de turistas, por lo que no hay capacidad física para alojar a nuestro personal y además hay que generar las medidas de aislamiento para todos aquellos que se han contagiado. Pero lo llevamos bien, con mucho trabajo. De eso se trata. El éxito de las operaciones policiales tiene que ver con la logística. Y hemos desplegado una logística en estos últimos dos años que la policía desconocía.

— Tuvo un alto perfil todo el año pasado y amagó con renunciar. ¿Qué le hizo cambiar de opinión y seguir en su cargo?

— No amagué. Ese término es incorrecto. He renunciado porque entendía que había una etapa política que se había agotado. Tuve una larga conversación con el gobernador, quien ha sido quien ha llevado adelante los cambios estructurales necesarios que la provincia necesitaba y en ese sentido me pidió que lo acompañara. Estoy convencido de la gran gestión que está llevando adelante el gobernador y de priorizar estos cambios, más allá de los resultados electorales. Muchos gobernadores se la pasaron utilizando los fondos de la provincia para hacer marketing, y este es el gobernador que ha llevado adelante la transformaciones profundas que necesitaba la provincia sin especular si eso era rentable políticamente.

— ¿Cree que Áxel Kicillof va a necesitar un mandato más para que se consoliden estos cambios que dice?

— Como dije anteriormente, por primera vez la Provincia tiene un gobernador que está haciendo los cambios estructurales sin especular. Esos cambios necesitan de mucho tiempo. Por eso estamos acompañándolo. Estamos convencidos del camino y del rumbo que ha elegido. No tengo ninguna duda de que el tiempo va a rendir con creces el esfuerzo que el gobierno de la provincia de Buenos Aires ha hecho, por sobre todas las cosas, durante la pandemia.

— ¿Cuál considera que fue el error político más importante que tuvo el Gobierno para haber perdido la elección y qué tema tiene que atender este año para no tener en 2023 otro resultado negativo?

— Hubo errores conceptuales y otras cuestiones que tienen que ver con la mezquindad y el egoísmo del ser humano. Hubo un sector que en estas elecciones tenían la plena certeza de que se iba a ganar por diez puntos y, a partir de allí, poder empezar a construir una línea política interna dentro del Frente de Todos. Y esa mezquindad lo cegó, no dejó ver más allá del árbol que les tapó el bosque y las PASO fueron una dura caída a la realidad. No creo que haya sido un error, fue parte de la mezquindad de algunos actores de la política.

Durante la charla, Berni también se refiere al viaje a México de Luana Volnovich, que generó un enorme revuelo y dejó a la titular del PAMI en el centro de los cuestionamientos por vacacionar en el exterior en medio de la pandemia, en plena crisis económica y mientras el oficialismo intenta fomentar el turismo interno.

Puertas adentro hubo críticas hacia la dirigente de La Cámpora y el ex secretario de Seguridad y Política Criminal de la Nación, Eduardo Villalba, pidió su renuncia a través de las redes sociales.

Berni, en cambio, le restó importancia a la polémica en torno a Volnovich y, por el contrario, cuestionó duramente a Villalba, con quien mantuvo varios cruces mientras éste estaba a las órdenes de Sabina Frederic. Incluso lo llegó a echar de una conferencia de prensa anunciada en Luján luego de la aparición de una niña de 7 años que había estado secuestrada tres días.

“Me parece que Luana tiene todo el derecho del mundo a vacacionar donde ella quiera. En un sistema capitalista, uno con su dinero hace lo que quiere y va a donde quiere. Se lo merecía. No nos olvidemos que ha sido la funcionaria que ha tenido sobre sus espaldas la responsabilidad de cuidar, cuando no existía la vacuna, al grupo etario más frágil ante el COVID, que fueron los ancianos”, consideró el funcionario.

Y disparó: “La diferencia entre Luana y Villalba es que ella se fue 15 días de vacaciones, con su dinero, eligió el lugar y todos sabemos dónde está. En cambio, Villalba estuvo dos años de vacaciones pagas por el Estado, y no sabemos dónde. Así que me parece que si alguien se tiene que callar la boca es Villalba”.

La paradisíaca isla donde veranea Vulnovich, Holbox, le agradó a Berni tal punto que se plantea visitar el lugar a futuro. Eso sí, no antes de que finalice el Operativo De Sol a Sol que supervisa día a día. “La verdad, no conocía la isla dónde fue. La googleé y me encantó. Si puedo, me voy a tomar unos días ahí”.