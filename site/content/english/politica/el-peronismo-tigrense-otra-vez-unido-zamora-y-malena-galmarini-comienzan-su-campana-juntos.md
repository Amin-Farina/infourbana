+++
author = ""
date = 2021-07-30T03:00:00Z
description = ""
image = "/images/la-foto-que-se-dio___hbrshfoiz_720x0__1.jpg"
image_webp = "/images/la-foto-que-se-dio___hbrshfoiz_720x0__1.jpg"
title = "EL PERONISMO TIGRENSE OTRA VEZ UNIDO: ZAMORA Y MALENA MASSA COMIENZAN SU CAMPAÑA JUNTOS"

+++
**_Tras años de una fuerte disputa política llegaron a un acuerdo para mantener la paz de cara a las PASO. El jefe comunal ya se muestra con la esposa del presidente de la cámara de Diputados._**

En el Conurbano Bonaerense, Tigre era uno de los distritos que más incomodidad despertaba a la hora de hablar de la unidad del Frente de Todos.

La rivalidad política entre el intendente Julio Zamora y su antiguo padrino político, el presidente de la Cámara de Diputados Sergio Massa, ponía en riesgo el acuerdo electoral forjado en 2019 para las PASO del 12 de septiembre. Sin embargo, el zamorismo, el Frente Renovador y La Cámpora lograron ponerse de acuerdo y armar una única lista.

Así lo grafican las fotos de Zamora con la presidenta de Aysa y esposa de Massa, Malena Galmarini. Las imágenes, una del aniversario de la muerte de Eva Perón y otra de una primera recorrida post cierre de candidaturas, son el resumen del pedido del kirchnerismo para todos los distritos de zona Norte, territorio donde una parte del electorado acompaña al macrismo o está desencantado con la gestión de Alberto Fernández.

Julio Zamora hizo valer su condición de intendente para encumbrar a su esposa, Gisela en lo más alto de la lista. Pero el Frente Renovador se quedó con varias candidaturas en los puestos de abajo.

En esta nueva etapa de equilibrio peronista, Malena Galmarini y Zamora ya se mostraron juntos en un primer acto de campaña, al inaugurar una red de agua potable en el Barrio El Arco, en la localidad de Benavidez. Algo impensado hace unos meses atrás. También estuvieron presentes Gisela Zamora y “Toto” Fernández Miranda, primera segundo precandidato a concejal.

Esta nómina única sigue con los nombre de “Toto” Fernandez Miranda (F.R), Sandra Rossi (F.R), Alejandro Ríos, quien responde a Julio Zamora, y Victoria Etchard (F.R). De los siete concejales restantes, sólo uno más responde a Zamora.

Como primer capítulo de la reconciliación del peronismo local, en una sesión especial realizada este martes en el HCD, el Frente de Todos apartó a Segundo Cernadas de la presidencia del legislativo local. Y en su lugar asumió el concejal massista, Fernando Mantelli.

En cuanto al resto de las autoridades designadas, Gladys Pollán (F.R) quedó como vicepresidenta 1°, Marcos Tenaglia (Juntos) como vicepresidente 2°, y Javier Parbst (F.R) como vicepresidente 3°.

En 2020, luego del fallecimiento de la presidenta del HCD, Alejandra Nardi (F.R), Cernadas era el vicepresidente, y principal referente del PRO dentro del Concejo. Pero en aquella ocasión, la falta de consenso del PJ hizo que no pudieran elegir un candidato para reemplazar a Nardi.

Esta vez, el hombre ganador de Massa, Mantelli, expresó en una entrevista radial que, “era importante volver a la presidencia del HCD, porque habían estado ocurriendo encuentros de tinte político allí. Mi idea es darle un espacio institucional. Era momento de que la presidencia quedará a cargo del bloque mayoritario. Hasta ahora no se había podido dar por diferencias”.

Y aclaró: “Nadie fue desplazado, es propiedad del HCD que se pueda convocar a una reunión para elegir al presidente. Se unificaron criterios y se votó una mayoría".

De esta manera el Frente de Todos quedó con 15 bancas y Cambiemos (Juntos) con nueve. Desde el círculo massista declararon estar "conformes con lo que se logró. Creemos que es un buen cierre y se logró una ecuación que hace tiempo no había”.