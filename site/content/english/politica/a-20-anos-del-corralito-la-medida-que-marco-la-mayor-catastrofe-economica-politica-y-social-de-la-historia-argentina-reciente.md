+++
author = ""
date = 2021-11-29T03:00:00Z
description = ""
image = ""
image_webp = "/images/ripe4dic-jpg_909876355.jpg"
title = "A 20 AÑOS DEL CORRALITO, LA MEDIDA QUE MARCÓ LA MAYOR CATÁSTROFE ECONÓMICA, POLÍTICA Y SOCIAL DE LA HISTORIA ARGENTINA RECIENTE"

+++

#### **_El 1 de diciembre de 2001, el Presidente Fernando de la Rúa decretó un conjunto de iniciativas del ministro de Economía, Domingo Cavallo, que congeló los depósitos, selló el colapso de la convertibilidad y desembocó en la represión del 19 y 20 de diciembre._**

El 1 de diciembre de 2001, el Presidente Fernando de la Rúa firmó el decreto 1570, un paquete de medidas ideado por el ministro de Economía, Domingo Cavallo: el corralito, que desembocaría -días después- en la mayor catástrofe económica, financiera, institucional y social de la historia argentina reciente.

La noche posterior, ante un nerviosismo creciente y un descontento social que empezaba a trasformarse en un aluvión imparable, Cavallo comunicó por cadena nacional los alcances de la iniciativa que ya había sido reflejada en las tapas de los diarios del domingo. El “corralito” iba a tener, según los planes iniciales, una vigencia de 90 días.

##### **A 20 años del “corralito”: “El dinero es suyo, sigue estando ahí”**

“El dinero es suyo, sigue estando ahí. Sigue rindiendo el interés que usted pactó con el banco. Lo puede transformar de pesos a dólares sin ningún costo. Lo puede usar como usted quiera. La única limitación es que podrá extraer en efectivo, en billetes o monedas, hasta 250 pesos por semana”, dijo Cavallo.

Además de la restricción sobre el retiro de efectivo de los bancos, el saldo de las cuentas solo podía ser utilizado a través de tarjetas de crédito y débito, cheques y transferencias. El objetivo de la medida era impedir un quiebre masivo de los bancos mediante la aplicación de un cepo a la salida de depósitos del sistema financiero, que en los primeros 11 meses de 2001 ascendía a 18.000 millones de pesos o dólares, en un escenario marcado por una convertibilidad ya herida de muerte.

Tal vez porque los anuncios no tenían buen aspecto, o porque eran el último manotazo para salvar a un sistema financiero ya ahogado, Cavallo insistía aquella noche con aclaraciones que no hacían más que alimentar la desconfianza de una población pegada al televisor.

“Yo quiero pedirles disculpas. Sé que muchos de ustedes se han asustado por estas medidas. Sé que muchos de ustedes tuvieron dudas y quizás todavía las tengan, pero estén seguros: lo que queremos es evitar que la Argentina entre en el caos”, señalaba.

Y a la vez que intentaba llevar calma a los jubilados y a ciertos sectores no bancarizados de la población activa -como los trabajadores rurales-, agregaba: “Aquí no estamos para defender a los bancos, como algunos dicen. Estamos para defender su ahorro, su dinero”.

Del otro lado del mostrador, y tras haberse filtrado los lineamientos preliminares del paquete de medidas durante las primeras horas del 30 de noviembre, la mayoría de los inversores calificados ya había aprovechado para retirar su dinero. “Estas medidas causan molestia, pero han evitado una derrota de los argentinos”, soltó el ministro de Economía en su frase final.

Días después, pintadas y piedrazos en las fachadas de los bancos eran la antesala del desborde que estaba por venir. En simultáneo, Cavallo anunciaba un acuerdo con empresas que instalan los dispositivos posnet para proveer a los comercios de los aparatos que le permitieran continuar con sus ventas, una iniciativa que tampoco funcionó.

Tras el caos del 19 y 20 de diciembre y las renuncias de Cavallo y De la Rúa, el tiro de gracia a la convertibilidad llegaría el 6 de enero de 2002, con la sanción de la Ley 25.561 de Emergencia Pública y Reforma del Régimen Cambiario.

El “corralito” se transformaría en “corralón” cuando Eduardo Duhalde, sucesor de De la Rúa y ya con Jorge Remes Lenicov como ministro de Economía, congeló los depósitos en plazo fijos y cajas de ahorros en pesos y en dólares. La medida se mantuvo en vigencia por un año.

En este contexto de restricciones de depósitos, los ahorristas comenzaron a organizarse y hubo un aluvión de recursos de amparo para poder disponer libremente de ellos. En todo 2002, un total de 13.612 millones de pesos fue extraído del sistema bancario a partir de estos procedimientos.