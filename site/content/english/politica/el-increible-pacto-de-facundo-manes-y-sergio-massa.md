+++
author = ""
date = 2021-09-04T03:00:00Z
description = ""
draft = true
image = "/images/g5wgw6fydfeoxhtt2le6fathuy.jpg"
image_webp = "/images/g5wgw6fydfeoxhtt2le6fathuy.jpg"
title = "EL INCREÍBLE PACTO DE FACUNDO MANES Y SERGIO MASSA"

+++
### **_El acuerdo operado en secreto por Nicolás Massot le permitió a Manes tener aparato electoral en Tigre y a Massa sumar concejales  para aumentar el control político del municipio._**

Facundo Manes cambió el apoyo de la UCR en Tigre por el respaldo de Sergio Massa y se aseguró una estructura electoral que no posee en el conurbano.

Operada en las sombras por Nicolás Massot, habitué de partidas de cartas en la casa del ex intendente de Tigre, la extraña sociedad busca asegurarle el control total del municipio al presidente de la cámara de diputados.

Cómo ya advirtió este medio el vínculo entre ambos data de mucho tiempo cuando ambos conversaban la incorporación de Massot  al gobierno de Alberto Fernández.

Segun infobae ambos dirigentes mantienen "habituales conversaciones y se consideran viejos amigos de la política, compartiendo almuerzos y conversando largas horas sobre la vida, la familia, la política y el futuro." Tal es el caso, que durante el 2019 se los pudo ver a ambos dirigentes "caminando juntos por la 5ta Avenida y comiendo burritos de pollo en Central Park en un restaurante llamado Tavern on the Green."

"Aunque para ellos fue simplemente un almuerzo de amigos que se repite hace quince años y que en otro momento no generaba interés, Infobae pudo reconstruir que el ex intendente de Tigre lo quiso convencer a Massot de que trabaje con el peronismo en la nueva etapa que se viene en la Argentina."

**Link:**[https://www.infobae.com/politica/2019/10/08/sergio-massa-y-nicolas-massot-almorzaron-en-nueva-york-primeras-senales-de-los-movimientos-dentro-del-peronismo/](https://www.infobae.com/politica/2019/10/08/sergio-massa-y-nicolas-massot-almorzaron-en-nueva-york-primeras-senales-de-los-movimientos-dentro-del-peronismo/ "https://www.infobae.com/politica/2019/10/08/sergio-massa-y-nicolas-massot-almorzaron-en-nueva-york-primeras-senales-de-los-movimientos-dentro-del-peronismo/")