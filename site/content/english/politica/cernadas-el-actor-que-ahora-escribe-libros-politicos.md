+++
author = ""
date = 2021-07-16T03:00:00Z
description = ""
image = "/images/whatsapp-image-2021-07-13-at-12-35-19.jpeg"
image_webp = "/images/whatsapp-image-2021-07-13-at-12-35-19.jpeg"
title = "CERNADAS, EL ACTOR QUE AHORA ESCRIBE LIBROS POLÍTICOS"

+++

###### FUENTE: HORA60

**_La presentación de libros escrito por diversos políticos parece ser el recurso más utilizado del último tiempo. O el negocio más importante para ellos._**

En esta oportunidad llegó el actor y concejal de Tigre, Segundo Cernadas, quien presentó un escrito titulado "Un nuevo sueño: poner a Tigre en Acción". La publicación es en parte autobiográfica y en parte un trabajo académico y técnico, según expresaron tras la presentación del mismo. La presentación del texto se suma a los ejemplares que presentaron Mauricio Macri, María Eugenia Vidal, Patricia Bullrich, Jorge Macri y Alex Campbell. Todos durante el 2021.

Este 2021, el pariente de Patricia Bullrich pretende encabezar la lista de concejales de "Juntos". Básicamente, Cernadas es un eterno candidato en el distrito que lejos esta de plasmar en unos breves renglones su trabajo por el municipio.

Mientras se acerca el cierre de listas la política vuelve a demostrar situaciones muy complejas. Esta vez fue Cernadas quien cayó en un pobre lugar común para traer un texto que parecería reflejar su papel en la política: un actor de segundo plano que hasta ahora solo sirve para los carteles de campaña.