+++
author = ""
date = 2022-02-14T03:00:00Z
description = ""
image = "/images/514910-1.jpg"
image_webp = "/images/514910.jpg"
title = "UN GRUPO DE SENADORES AMENAZA CON ABANDONAR LA CONDUCCIÓN DE CRISTINA KIRCHNER"

+++

##### **_El entrerriano Edgardo Kueider, el correntino Camau Espínola, el jujeño Guillermo Snopek y el salteño Sergio “Oso” Leavy quieren armar su propio bloque para independizarse del mandato de la vicepresidenta_**

En medio de las negociaciones en el oficialismo para aprobar el acuerdo con el Fondo Monetario Internacional, un grupo de senadores quiere independizarse de la conducción de Cristina Kirchner y formar su propio bloque.

El grupo está integrado por el entrerriano Edgardo Kueider, el correntino Camau Espínola, el jujeño Guillermo Snopek y el salteño Sergio “Oso” Leavy, quienes se quejaron públicamente por la falta de discusión interna en la bancada del Frente de Todos, actualmente presidida por José Mayans y conducida por la vicepresidenta.

Los legisladores prometen no romper con la Casa Rosada, pero buscan independencia para plantear sus posición y fijar su propia agenda de temas. En ese contexto, cuestionaron la actitud de Máximo Kirchner de renunciar a la presidencia del bloque en Diputados y adelantaron que votarán a favor del entendimiento entre el Gobierno y el FMI.

> “El costo de no pagarle al FMI significa entrar en default y eso tendrá un impacto mayor desde el punto de vista social; el gesto de Máximo Kirchner generó temor en los mercados y puso tensión nuevamente sobre el dólar”, precisó el senador Kueider en diálogo con el periodista Eduardo Feinmann en Radio Mitre.

“Imaginen un default. Eso va a significar una devaluación, que el peso valga menos y que el sueldo valga menos, yo voy a poner en la balanza a los millones de argentinos que van a sufrir esa crisis”, argumentó.

Kueider responde a Alberto Fernández e impulsa la construcción de una nueva estructura para sostener la gestión e impulsar la reelección en 2023. En ese contexto asegura que hay que trabajar para fortalecer la figura presidencial y criticó las declaraciones que tienden a erosionar su poder y provocar una sensación de desconfianza política que luego se traslada a la economía.

“Cuando el Presidente envíe al Congreso el acuerdo con el FMI, tenemos que aprobarlo y no permitir que la Argentina caiga en una situación de caos e incendio económico”, enfatizó el senador oficialista.

Este grupo de legisladores viene amagando con escindirse de la conducción del bloque oficialista desde el año pasado. Incluso en algún momento se especuló con que eso sucedería a partir del 10 de diciembre, cuando se formalizó el recambio tras las elecciones legislativas.

Una particularidad que presenta este grupo de senadores es que son de provincias en las que no tienen a un gobernador atrás que empuje sus designaciones. Espínola y Snopeck son de provincias gobernadas por el radicalismo, mientras que Kueider responde al gobernador peronista Gustavo Bordet de no muy buena relación con el kirchnerismo. El caso del salteño Leavy también tiene sus particularidades porque su ruptura con el kirchnerismo tiene que ver con que ese sector apoya al gobernador de su provincia, Gustavo Saenz, enfrentado políticamente con el legislador.