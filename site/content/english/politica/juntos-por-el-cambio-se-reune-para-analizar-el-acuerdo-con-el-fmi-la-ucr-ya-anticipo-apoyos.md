+++
author = ""
date = 2022-01-28T03:00:00Z
description = ""
image = "/images/rbwpbc4s3zf65abzpeucoooyzu.jpg"
image_webp = "/images/rbwpbc4s3zf65abzpeucoooyzu.jpg"
title = "JUNTOS POR EL CAMBIO SE REÚNE PARA ANALIZAR EL ACUERDO CON EL FMI; LA UCR YA ANTICIPÓ APOYOS"

+++

##### **_La Mesa Nacional de Juntos por el Cambio analizará este mediodía el acuerdo del Gobierno con el Fondo Monetario Internacional (FMI) y fijará una posición conjunta._** 

###### Será durante un Zoom que comenzó a las 12 y del que podrían participar economistas del espacio para evaluar los alcances de lo anunciado por Alberto Fernández y el ministro de Economía, Martín Guzmán.

Luego del discurso presidencial, las autoridades del PRO y de la Coalición Cívica prefirieron evaluar más en profundidad los anuncios antes de dar una opinión, mientras que desde la UCR se registraron las primeras posiciones de la oposición en favor del acuerdo con el Fondo. “Es un primer paso positivo ya que con el default hubiera sido negativo para nuestra economía”, dijo el presidente del Comité Nacional del radicalismo, Gerardo Morales, en su cuenta de Twitter.

La Mesa Nacional de Juntos por el Cambio analizará este mediodía el acuerdo del Gobierno con el Fondo Monetario Internacional (FMI) y fijará una posición conjunta. Será durante un Zoom que comenzó a las 12 y del que podrían participar economistas del espacio para evaluar los alcances de lo anunciado por Alberto Fernández y el ministro de Economía, Martín Guzmán.

Luego del discurso presidencial, las autoridades del PRO y de la Coalición Cívica prefirieron evaluar más en profundidad los anuncios antes de dar una opinión, mientras que desde la UCR se registraron las primeras posiciones de la oposición en favor del acuerdo con el Fondo. “Es un primer paso positivo ya que con el default hubiera sido negativo para nuestra economía”, dijo el presidente del Comité Nacional del radicalismo, Gerardo Morales, en su cuenta de Twitter.

“Oportunamente el Congreso tratará los términos del acuerdo; reivindico la actitud responsable de la oposición en un tema clave para la vida de los argentinos“, agregó.

El radical Alfredo Cornejo, jefe del interbloque de senadores de Juntos por el Cambio, también opinó desde las redes sociales: “Todo lo que lleve a acordar con los organismos internacionales es una buena noticia. Se podría haber evitado un año de angustia, pero la obsesión por el relato del kirchnerismo nos perjudicó a todos. Ahora tenemos que ver la letra chica del acuerdo”.

Su correligionario Luis Naidenoff, titular del bloque de senadores de la UCR, señaló que “el anuncio del acuerdo con el Fondo es una buena señal” y destacó: “Siempre sostuvimos que era necesario lograr un entendimiento. Ya habrá tiempo para discutir sobre las responsabilidades del endeudamiento. Ahora el Parlamento tiene que analizar las implicancias del acuerdo para el país”.

Para el diputado nacional de la UCR Facundo Manes “es una buena noticia que después de dos largos años estemos llegando a la recta final de las negociaciones con el FMI”. Advirtió, sin embargo, que “además del acuerdo con el FMI y el apoyo de las potencias, necesitamos una visión integral, voluntad política y sentido común en el frente interno”. En ese sentido, afirmó que “no tener un plan económico claro pone en riesgo una discusión seria sobre el futuro del país”.

“No podemos seguir viviendo en eterna incertidumbre -añadió-. Necesitamos conocer el esquema económico que se está planteando para los próximos años. Hace falta un esquema racional que no puede ser solo el de un gobierno ni el de un partido: debe convocarnos realmente a todos”.

Martín Tetaz, diputado del radicalismo y economista, dijo a Infobae que se “alegra de que haya acuerdo (con el FMI) porque pavimenta una hoja de ruta, aunque después el debate es si esa hoja de de ruta es creíble o no”, y destacó que “se plantea que el objetivo es llegar a 2024 con un déficit del 0,9%, que está incluso por arriba del que había cuando el Gobierno asumió, que fue del 0,4″.

Para el legislador, se prevé esa meta “sin financiamiento monetario, con lo cual tiene que crecer el financiamiento en el mercado local”, algo que consideró “demasiado optimista” ya que “van a seguir las condiciones del cepo y las restricciones para compra de otros activos y es difícil un crecimiento a largo plazo”. “El plan monetario del Gobierno luce razonable, pero poco probable”, sintetizó.

Tetaz señaló que “el plan antiinflacionario basado en un acuerdo de precios es absolutamente insostenible y todo el mundo sabe que eso no funciona”, por lo que consideró que “hay una parte del programa que el Gobierno no cuenta, que es que el objetivo, por lo menos de parte del Fondo, de que la inflación baje sobre la base de reducir el financiamiento monetario”. Y agregó: “Eso es lo que hoy no luce tan fácil, pero efectivamente si el sendero se cumple, podría ser”.

Resaltó, de todas formas, que “un gran punto positivo del acuerdo” anunciado “es el plazo de 10 años para pagar, con cuatro años de gracia, el punto que destrabará más el horizonte financiero” del oficialismo. “El logro también es la recomposición de reservas -dijo-. Al ser un programa por los 44.000 millones totales nos devuelven todo lo pagado, y de esa forma prácticamente recuperamos 5000 millones de dólares, que es con lo que el Gobierno planea recuperar las reservas a partir de 2022″.

En cuanto a los aspectos negativos de lo anunciado, Tetaz criticó que “no haya ninguna precisión de cómo va a funcionar el programa cambiario ni el tarifario”. Indicó que a él le consta que “esos debates se dieron con el Fondo, pero no se blanquean ni dicen qué pasará” con esos puntos.

Otro aspecto “no consistente” de los anuncios, según el diputado radical, es el tema exportaciones: “El ministro planteó que uno de los ejes del programa antiinflacionario era que el Gobierno tenía la restricción externa, aunque no parece claro porque anunció que no va haber ninguna devaluación y que la economía, en el acuerdo con el Fondo, tiene que expandir y promocionar exportaciones”. “No sé ve cómo va ser posible. Todo parece indicar que vamos a un 2022 con menos exportaciones que en 2021″, puntualizó Tetaz, para quien “el programa cambiario no cierra muy bien”.

Acerca del respaldo de Juntos por el Cambio en el Congreso al acuerdo con el FMI, el legislador de la UCR porteña indicó: “Es importante que el programa no incluya aumento de impuestos porque (si hubiera sido al revés) nos hubiera impedido apoyarlo, ya que una de las condiciones que habíamos planteado, y nos escucharon, por lo visto, es que no hubiera aumento de impuestos”.

“Para aprobar el programa, vamos a pedir precisiones sobre las cuestiones tarifarias y cambiarias. Si esas precisiones son relativamente satisfactorias, seguramente acompañaremos”, anticipó.

Por su parte, desde el sector del PRO de Patricia Bullrich, se sumó la opinión del diputado nacional Gerardo Milman desde su cuenta de Twitter: “Hace dos años, nosotros hubiésemos tardado cinco minutos en hacer un mejor acuerdo. El Gobierno hizo sufrir innecesariamente a la Argentina todo este tiempo. La pregunta es: Y el kirchnerismo, ¿dónde está ahora?