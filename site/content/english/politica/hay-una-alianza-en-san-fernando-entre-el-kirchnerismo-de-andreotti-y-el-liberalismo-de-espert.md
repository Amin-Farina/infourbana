+++
author = ""
date = 2021-11-03T03:00:00Z
description = ""
image = "/images/espert-san-fernando.jpg"
image_webp = "/images/espert-san-fernando.jpg"
title = "¿HAY UNA ALIANZA EN SAN FERNANDO ENTRE EL KIRCHNERISMO DE ANDREOTTI Y EL LIBERALISMO DE ESPERT?"

+++

##### **_Dos semanas nos separan de las elecciones generales 2021 y las sorpresas en relación al acto electivo no paran de surgir. Ahora es el turno de San Fernando, localidad de la provincia de Buenos Aires._** 

En las últimas horas se hizo de público conocimiento que Juan Molina, el primer candidato a Concejal de Avanza Libertad, espacio que lidera el economista José Luis Espert, es empleado municipal en la gestión oficialista de Juan Andreotti. Dicha gestión lleva más de 10 años en el poder en la localidad ribereña y se la reconoce por su apoyo al kirchnerismo. 

Juan Molina, quien en las PASO obtuvo el 5% votos, es Coordinador de Orden Urbano y vocero del área. En diversas notas de prensa emitidas por el propio municipio, es Molina quien se encarga de dar reporte y ser la cara de la gestión adreottista frente a los ciudadanos como menciona la página municipal: [https://youtu.be/Koa5a3_hPKg](https://youtu.be/Koa5a3_hPKg "https://youtu.be/Koa5a3_hPKg") y la nota de Clarín: [https://www.clarin.com/zonales/fiestas-clandestinas-san-fernando-desbarataron-eventos-180-personas-via-publica_0_anoEkt-MH.html](https://www.clarin.com/zonales/fiestas-clandestinas-san-fernando-desbarataron-eventos-180-personas-via-publica_0_anoEkt-MH.html "https://www.clarin.com/zonales/fiestas-clandestinas-san-fernando-desbarataron-eventos-180-personas-via-publica_0_anoEkt-MH.html")

![](/images/avanza-libertad.jpg "Juan Molina vocero de la gestión de Andreotti en la boleta de Avanza Libertad.")_Juan Molina vocero de la gestión de Andreotti en la boleta de Avanza Libertad._

En su última visita a San Fernando, el pasado 10 de octubre, Espert se pronunció a favor de Molina sosteniendo que “este es el candidato que realmente se preocupa por los problemas de los vecinos”. 

Si bien se desconoce si el referente liberal sabe que su candidato trabaja para el gobierno kirchnerista, es al menos curiosa la situación que se da en el distrito del conurbano bonaerense y levanta las desconfianzas que suelen aparecer alrededor de la política: ¿hay una alianza entre el kirchnerismo y los liberales en San Fernando? ¿Molina es candidato de Espert o de Andreotti? De llegar al concejo, ¿Molina trabajará para el liberalismo o se unirá al bloque mayoritario del kirchnerismo?