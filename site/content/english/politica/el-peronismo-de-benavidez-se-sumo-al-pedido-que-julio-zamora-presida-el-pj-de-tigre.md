+++
author = ""
date = 2021-04-21T03:00:00Z
description = ""
image = "/images/julito-zamorista.jpg"
image_webp = "/images/julito-zamorista.jpg"
title = "EL PERONISMO DE BENAVÍDEZ SE SUMÓ AL PEDIDO QUE JULIO ZAMORA PRESIDA EL PJ DE TIGRE"

+++

**_Fue en el marco de un nuevo plenario de agrupaciones y dirigentes, en el Centro de Jubilados “Amor”. Allí, manifestaron el respaldo a la gestión del intendente de Tigre y la necesidad de contar nuevamente con su conducción en el Partido Justicialista local de cara a los desafíos del año electoral._**

En el Centro de Jubilados “Amor” de Benavídez, agrupaciones políticas y referentes peronistas llevaron adelante un nuevo plenario para respaldar la gestión del intendente de Tigre, Julio Zamora, y pedir que reasuma como presidente del Partido Justicialista local.

El petitorio surgió de una evaluación política realizada por agrupaciones y referentes del distrito, en el marco del escenario nacional y en función a la necesidad de que el dirigente más importante del PJ Tigre conduzca, a su vez, el Frente de Todos, sus alianzas y el consenso en la lista de candidatos a presentar a su debido momento.

Además, tal y como sucedió en el primer encuentro organizado por el Peronismo de El Talar, los presentes repudiaron la difamación de los medios de comunicación y redes sociales que pretenden desacreditar la administración de Zamora. 

Participaron del encuentro: el Consejo del Partido de Benavídez; Agrupación 4 de Junio; Frente Benavídence; Agrupación Perón y Evita; Movimiento Militante y organizaciones sociales representadas por "Tigre la Nueva”.