+++
author = ""
date = 2021-01-19T03:00:00Z
description = ""
image = "/images/gildo-insfran-transmision.jpg"
image_webp = "/images/gildo-insfran-transmision.jpg"
title = "Denunciaron a Insfrán por las \"condiciones inhumanas\" de los centros de aislamiento"

+++

**_Mezclan pacientes con Covid-19 con otros que esperan el resultado de sus tests, donde además los reflectores permanecen prendidos toda la noche._**

El gobernador formoseño Gildo Insfrán fue denunciado penalmente junto al ministro de Gobierno de la provincia por el hacinamiento y "condiciones inhumanas" en un centro de aislamiento de la provincia para personas con coronavirus. Hasta ahora nadie del kirchnerismo se hizo eco de la presentación contra el mandatario que mantiene una cuarentena dura en su provincia.

Las irregularidades que suceden en el Centro de aislamiento preventivo del Estadio Cincuentenario son varias. Desde mezclar pacientes positivos de Covid-19 con otros que esperan el resultado de sus tests hasta malos tratos y falta de condiciones para contener a las personas infectadas. Los pacientes se quejan de las altas temperaturas del lugar, donde además los reflectores permanecen prendidos toda la noche.

La tardanza de la provincia para tener los resultados de los tests hace que algunos de los internados pasen allí más de dos semanas, el tiempo que indica el protocolo. Incluso lo comparan con un centro de detención, más que de aislamiento.

En los últimos días hubo incidentes entre las personas que están alojadas que se viralizaron en las redes y fueron levantadas por la señal TN. El gobernador impuso una de las cuarentenas más estrictas para enfrentar al coronavirus y se registraron excesos como la prohibición de ingreso para personas que habían salido de la provincia y querían retornar.

La abogada Gabriela Neme denunció a Insfrán y a su ministro de Gobierno, Jorge Abel González, y representa a un grupo de pacientes que pasaron por el Estadio Cincuentenario. En el lugar conviven familias con niños junto a adultos y personas ancianas. Una de las cuestiones más controvertidas es que aquellos que esperan el resultado de un tests comparten los baños con los infectados.

"He radicado la denuncia penal contra el gobernador de la provincia y contra el ministro de Gobierno, representante y cabeza de la mesa de emergencia, porque es aberrante lo que está viviendo esta gente", dijo en TN.

> "Si alguien se quiere ir de estos centros directamente te tenés que ir de Formosa, te sacan de la provincia. Ellos ya dijeron que van a seguir con estos centros de hacinamiento en Formosa, porque creen que son los mejores del país", explicó Neme.

"El bebé está en condiciones inhumanas. Mi hijo está sano, mi hijo no tiene COVID. Este nene de 12 años dice que tiene y viene una profesional que no me quiere decir ni la matrícula ni el nombre ni el apellido. Yo me quedo acá 14 días y voy a terminar contagiada también. No doy más, no doy más", se escucha a la mujer que tiene a uno de sus hijos internado. Ella, que no está contagiada, se mudó junto a su niño pequeño para acompañarla.