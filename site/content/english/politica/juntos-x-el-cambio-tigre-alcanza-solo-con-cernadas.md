+++
author = ""
date = 2021-02-15T03:00:00Z
description = ""
image = "/images/whatsapp-image-2021-02-02-at-14-16-13.jpeg"
image_webp = "/images/whatsapp-image-2021-02-02-at-14-16-13.jpeg"
title = "JUNTOS X EL CAMBIO TIGRE: ¿ALCANZA SOLO CON CERNADAS?"

+++

**_La idea de ampliación de Juntos por el Cambio es cada vez más fuerte. Sin embargo, la propuesta es de difícil aplicación en los distritos que se construyeron sobre la centralidad de un perfil y al día de hoy generan más desconfianza que certezas. El caso de Segundo Cernadas en Tigre se convierte en una de las referencias de los municipios._**

El ex actor llegó a Tigre en el 2015 cuando asumió como concejal del distrito, un lugar que alcanzó por ser pariente de Patricia Bullrich y contar con su apoyo político. Un año después presentó su renuncia a la banca para abocarse a su función de ANSES. Pero en el 2017 volvió a presentarse como cabeza de la lista de concejales y asumió funciones en el HCD tigrense nuevamente. Estas maniobras generaron una nueva realidad: el vecinalismo de Acción Comunal, aliado del PRO en el municipio, vio en Cernadas un perfil exclusivamente mediático y nada político. Además, han considerado que su egocentrismo afecta el desarrollo histórico de un partido que supo renovar la gestión cuando encabezaron el Ejecutivo Municipal.

La incertidumbre para el 2021 pasa por cómo revertir esto. La idea de apertura y ampliación del espacio es una misión nacional y provincial de Juntos por el Cambio pero que no genera confianza en el pago chico de la zona norte. «Segundo (Cernadas) demostró que busca su beneficio y permanencia solamente: no construye, no camina, no contiene. Es un real outsider de la política tigrense y estos años no lo han legitimado para ser una voz lider de Tigre», señalan desde el armado de la coalición opositora.

¿Qué hará Juntos por el Cambio en Tigre? ¿Seguirá descansando la política local en el pariente de Patricia Bullrich? ¿Logrará trabajar una idea de apertura o definitivamente se cerrará en el actor? ¿Quién y cómo toma las decisiones ante la falta de sustento político de Cernadas? Algunos de los interrogantes que empiezan a surgir en los cafes de la Avenida Victorica.