+++
author = ""
date = 2022-05-31T03:00:00Z
description = ""
image = "/images/foto-critica-a-macri.webp"
image_webp = "/images/foto-critica-a-macri.webp"
title = "JUNTO A KICILLOF Y MASSA, ALBERTO FERNANDEZ PRONUNCIÓ UN DURO DISCURSO CONTRA MACRI T LO CALIFICÓ DE \"LADRÓN DE GUANTE BLANCO\" "

+++
#### El Presidente encabezó un acto en la localidad de Cañuelas y apuntó con virulencia contra el gobierno anterior. El evento tuvo un marcado tono de campaña en el que se buscó mostrar unidad interna y señalar que el macrismo es el rival a vencer

El gobierno nacional intentó mostrar una imagen de cohesión interna en un acto en la localidad de Cañuelas, donde se congregaron Alberto Fernández, el gobernador Axel Kicillof, el presidente de la Cámara de Diputados, Sergio Massa y el ministro de Obras Púbicas, Gabriel Katopodis. A más de un año de las elecciones, el evento, en el que se anunciaba formalmente el inicio de obras en la Ruta Nacional 3, tuvo un marcado tono de campaña electoral.

La línea discursiva de los funcionarios del Frente de Todos fue clara: todos señalaron con énfasis, una y otra vez, que las políticas de Mauricio Macri fueron perjudiciales para gran parte de la sociedad. Incluso se emitió un spot que incluyó unas palabras del ex Presidente, con el objetivo de exponer que fueron promesas que no se cumplieron.

En ese marco, el jefe de Estado y sus compañeros buscaron bajarle el tono a la feroz interna que divide al oficialismo y señalaron que la pelea, en realidad, no es entre integrantes de un mismo espacio político, sino entre dos modelos totalmente opuestos. “Vinieron a hacer obras maravillosas que nunca hicieron”, ejemplificó el Presidente en el inicio de su discurso, en el que acusó al macrismo de hacer negocios en cada acción de gobierno: “Cuando el Estado se hizo cargo de las obras de PPP (los contratos de Participación Público Privada que promovía Cambiemos), los precios bajaron un 70 por ciento”.

Fue la introducción de un mensaje cargado de furia contra la oposición: “Digo todas estas cosas porque esos ladrones de guante blanco andan dando cátedra de moral y de ética por los medios de comunicación”.

“Estoy esperando que alguna vez un juez llame a esos ladrones de guante blanco y les pida explicaciones por la deuda que tomaron; que expliquen los parques eólicos, la estafa al Correo, la estafa por los peajes... Estoy esperando que alguna vez la Justicia se dignifique a sí misma y llame a rendir cuentas a los ladrones de guante blanco, a los poderosos”, vociferó Alberto Fernández. A su lado, Kicillof y Massa escuchaban y aplaudían con una mueca de satisfacción.

“Yo digo estas cosas porque hay un momento en que uno siente que es hora de que hablemos las cosas con claridad, porque hay momentos en que uno siente que hace un enorme esfuerzo y concreta las obras. Ya entregamos 45 mil viviendas y se están construyend 120 mil más en todo el país. También dimos 50 mil créditos para que construyan su casa los que tienen un lote”, destacó como méritos de su administración.

En el tramo final de su alocución, Alberto Fernández agregó que está “cansado de escuchar tantas mentiras, de las diatribas de estos sinvergüenzas que llevaron al país a la ruina”.

“Hemos vivido el momento más dramático que la humanidad recuerde, vivimos una pandemia que se llevó la vida de 6 millones de seres humanos en todo el paneta. Ahora resulta que la OMS dice cómo se comportó cada país y resulta que la Argentina es un ejemplo. Y nos dijeron de todo cuando combatíamos la pandemia...”, agregó el mandatario. “No nos tocó el mejor momento para gobernar, ¿quién quiere hacerlo en el medio de una pandemia?. Pero somos peronistas y al problema le ponemos el pecho, no nos escapamos”.

“Nada nos debe parar, porque vivimos un tiempo donde la pandemia nos afectó a todos y muchos sinvergüenzas explotan el desánimo. En estos tiempos sembrar esperanza es muy difícil; pero acá estamos nosotros, como diría Fito (Páez), venimos a ofrecer nuestro corazón, para que la Argentina se levante y camine, que nadie nos detenga”, finalizó.