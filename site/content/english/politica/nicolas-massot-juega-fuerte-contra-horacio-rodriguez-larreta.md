+++
author = ""
date = 2021-08-12T03:00:00Z
description = ""
draft = true
image = "/images/pelea-congreso.jpg"
image_webp = "/images/pelea-congreso.jpg"
title = "NICOLÁS MASSOT JUEGA FUERTE CONTRA HORACIO RODRÍGUEZ LARRETA"

+++
###### FUENTES: LA NACIÓN, INFOBAE, IPROFESIONAL, NORTEONLINE

**_El ex diputado por Córdoba Nicolás Masssot se convirtió en la figura clave que busca complicar los planes del Jefe de Gobierno de la Ciudad para consolidar el liderazgo de la oposición. En el PRO hablan de traición y advierten que la candidatura en Tigre solo sirve para restarle votos a Diego Santilli en un distrito donde el Frente de Todos puede perder._**

Nicolás Massot fue Kirchnerista, Macrista, Larretista y ahora aliado del radicalismo. Conversó además con Sergio Massa sobre la posibilidad de incorporarse al gobierno de Alberto Fernández. De todos lados terminó abandonando el espacio enojado y con críticas. Hoy su nuevo capricho es ser intendente de Tigre a donde se mudó hace dos años.

Detrás de la intención se mueve otra jugada política: debilitar la suerte de Juntos en el distrito que su amigo Sergio Massa aspira a seguir controlando. En el camino busca lograr que Segundo Cernadas no sea un sostén más potente a la figura de Horacio Rodríguez Larreta en la primera sección electoral **_(_**[**_Link_**](https://www.lanacion.com.ar/opinion/el-parecido-entre-los-intendentes-de-pro-y-el-pj-incomoda-a-larreta-nid06082021/ "El parecido entre los intendentes de Pro y del PJ incomoda a Larreta")**_)_**.

Muy atrás quedaron las buenas intenciones y Massot comenzó la campaña al igual que Facundo Manes acusando a Cernadas de cogobernar Tigre **_(_**[**_Link_**](https://norteonline.com.ar/tigre-nicolas-massot-duro-contra-cernadas-juntos-por-el-cambio-cogobierna-en-tigre-con-el-oficialismo/ "Tigre: Nicolás Massot duro contra Cernadas, “Juntos por el Cambio cogobierna en Tigre con el oficialismo”")**_)_.** Del mismo modo que el radical se refirió a que no se utilicen los impuestos de los porteños en la campaña de la provincia, ahora Massot se respalda en su cargo y estructura de Director del Banco Ciudad. Una contradicción que no dejan de marcar desde el PRO.

La decisión de Monzó de jugar con Manes y enfrentar a Santilli no cayó demasiado bien en Parque Patricios y hay quienes aconsejan al jefe de Gobierno porteño que debería correr al ex jefe del bloque de diputados Nicolás Massot del acceso a la estructura del Banco Ciudad con salarios que pueden superar el medio millón de pesos **_(_**[**_Link_**](https://www.lapoliticaonline.com.ar/nota/135583-enojado-con-larreta-porque-le-bajaron-candidatos-ritondo-no-fue-al-cierre-de-listas/ "Enojado con Larreta porque le bajaron candidatos Ritondo no fue al cierre de listas")**_)_**

Massot viene de representar a Córdoba como diputado nacional entre 2015 y 2019 y ahora busca convertirse en intendente de Tigre, con el respaldo de Monzó y el ala “disidente” de Juntos por el Cambio, que hoy se encolumna detrás de Facundo Manes en la interna de Juntos **_(_**[**_Link_**](https://www.iprofesional.com/politica/344886-confidencial-larreta-en-alerta-por-traiciones-antes-de-las-paso "Confidencial: Larreta y su equipo ven traiciones y fantasmas ante una elección que los obsesiona")**_)_**