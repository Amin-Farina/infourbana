+++
author = ""
date = 2022-03-22T03:00:00Z
description = ""
image = "/images/alberto-fernandez.webp"
image_webp = "/images/alberto-fernandez.webp"
title = "ALBERTO FERNÁNDEZ: \"VALORO A CRISTINA Y A MÁXIMO KIRCHNER, PERO NO SOY TÍTERE DE NADIE\""

+++
#### El Presidente pidió en plena tensión con la Vicepresidenta que haya cohesión en el oficialismo: “De mi parte, no esperen un solo gesto que rompa la unidad”

El presidente Alberto Fernández habló sobre la crisis en el Frente de Todos y de la máxima tensión por su relación con la vice Cristina Kirchner. Dijo que valora a Cristina Kirchner y a su hijo, el diputado del Frente de Todos Máximo Kirchner, y que no es “un títere de nadie”.

Alberto Fernández dijo que para él la unidad “es absolutamente necesaria”: “De mi parte, no esperen un solo gesto que rompa la unidad”, y agregó: “No podemos darnos el lujo desunirnos”.

Alberto Fernández dijo que valora a Cristina Kirchner y a Máximo Kirchner, que votó en contra del acuerdo con el Fondo Monetario Internacional (FMI), y planteó: “Esto no va a en detrimento de nadie, pero no existe la presidencia colegiada”.

Alberto Fernández reiteró el llamado a la unidad en el Frente de Todos: “En mí solo van encontrar voluntad de seguir trabajando juntos, hice un enorme esfuerzo para unir partes, lo hice sin la idea de ser candidato, lo hice por convicción”, en declaraciones a _El Destape Radio._

##### Las internas en el Frente de Todos

Alberto Fernández se refirió a las internas en el Frente de Todos, reconoció que hay diferencias y dijo que no contribuirá a una ruptura: “No voy a hacer nada para que esto se rompa, hay que convivir con estas diferencias”.

Alberto Fernández dijo que cuando le propusieron encabezar la fórmula presidencial del Frente de Todos en la fórmula con Cristina Kirchner “sabía que iba a tener que tomar decisiones”, que esperaba que lo acompañaran y que no lo acompañaron.

Alberto Fernández dijo que no todos piensan igual en el Frente de Todos y que cree que no pueden darse el lujo de desunirse, “por la causa que sea, narcisismos, egoísmo, política”.

Alberto Fernández dijo que deben estar unidos en el Frente de Todos y que, caso contrario, pueden llegar a perder en las elecciones presidenciales de 2023: “Debemos estar unidos para que no vuelvan a gobernar los que ya lo hicieron y bajaron el ingreso 20 puntos en cuatro años”.

Alberto Fernández dijo que se sentiría “muy mal” si por las diferencias en el Frente de Todos le “abren paso a la derecha”, que consideró que “hizo un daño incalculable”.

Alberto Fernández dijo que dio “todas las peleas que hay que dar”, que no lo hace a los gritos y que no es “moderado, sino un tipo que tiene estos modos”.

##### La "guerra contra la inflación"

Alberto Fernández habló sobre el lanzamiento de la “guerra contra la inflación” y criticó a quienes calificó de “especuladores”. Dijo que los ciudadanos argentinos son su “ejército” y que enfrente “están los especuladores”.

Alberto Fernández criticó que “algunos especulan y suben precios por las dudas” y que la batalla contra la inflación la tienen que dar “entre todos”.

Alberto Fernández dijo que deben “denunciar al supermercadista del barrio que no entiende las cosas y pone los precios que pone”.

Alberto Fernández dijo que es “imperioso” ocuparse del aumento de los precios y que “los alimentos suben a raíz de la guerra en Ucrania”.

##### Críticas a la Corte Suprema

Alberto Fernández volvió a cuestionar a la Corte Suprema: “No está funcionando bien, pero no solo por los fallos que saca. Por la cantidad de fallos que saca, por la cantidad que no saca, por la discrecionalidad que tiene para meterse en unos casos sí y en otros no, por las demoras”.

Alberto Fernández consideró que “hay una protección muy grande a los jueces”: “¿No te llama la atención que cuando hay una causa en cualquier lado el primer planteo es resolver la incompetencia para que venga a Comodoro Py? Acá duermen las causas contra ellos”.