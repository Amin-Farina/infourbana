+++
author = ""
date = 2022-01-11T03:00:00Z
description = ""
image = "/images/f1280x720-60840_192515_5050-1.jpg"
image_webp = "/images/f1280x720-60840_192515_5050.jpg"
title = "HORACIO RODRÍGUEZ LARRETA: “ME PARECE MUY GRAVE EL ATAQUE DEL GOBIERNO NACIONAL A LA CORTE”"

+++

##### **_El jefe de Gobierno porteño Horacio Rodríguez Larreta criticó el respaldo de una parte del Gobierno a la convocatoria del dirigente piquetero Luis D’Elia a una movilización para “echar” a todos los miembros de la Corte Suprema._**

Rodríguez Larreta planteó: “Me parece muy grave el ataque del Gobierno nacional a la Corte”, y cuestionó que el presidente Alberto Fernández “avale” que funcionarios vayan a participar de la marcha contra los integrantes del máximo tribunal prevista para el 1° de febrero.

El jefe de Gobierno porteño dijo que “el principio de división de poderes es uno de los principios básicos del sistema republicano”, en conferencia de prensa en el nuevo Centro de testeo de coronavirus ubicado en Chacarita.

> Rodríguez Larreta dijo que le parece “muy grave el avance y el ataque del gobierno a la Corte”: “Que participen de una marcha funcionarios nacionales pidiendo la renuncia de la Corte es gravísimo”.

###### El respaldo de un sector del Gobierno a la marcha organizada por el kirchnerismo duro contra la Corte Suprema

El lunes el viceministro de Justicia Martín Mena respaldó la movilización contra los integrantes de la Corte. El funcionario dijo que le parece “sano y necesario que la gente se pronuncie”.

Mena dijo que le parece “bien” el llamado a la movilización contra los miembros del máximo tribunal: “Siempre avalo toda expresión popular directa de la gente”.

El viceministro de Justicia dijo que respalda “la manifestación popular de la gente, cuando sin intermediarios le dicen a los poderes hegemónicos su opinión y límites”, en declaraciones a El Destape Radio.

Para Mena se está “llegando a límites que el gobierno de Mauricio Macri los superó, barriendo con todo en materia de manipulación política, judicial y protección del poder mediático”.

###### ¿Qué dijo Alberto Fernández sobre la Corte Suprema y la movilización hacia el Palacio de Tribunales?

El lunes el Presidente dijo con fuertes críticas al máximo tribunal que “la Corte Suprema tiene un problema de funcionamiento muy serio” y que “parece ser un coto cerrado para la oposición”.

Alberto Fernández criticó el fallo del máximo tribunal que declaró inconstitucional la ley de reforma del Consejo de la Magistratura y ordenó volver a la norma anterior, ya que manifestó que “una ley que fue derogada no puede recuperar su utilidad por una sentencia”.

> El Presidente además calificó de “muy positiva” la movilización al Palacio de Tribunales: “Me parece bien. Desde ya toda expresión popular es sana y necesaria”, en declaraciones a AM 750.