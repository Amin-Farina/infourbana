+++
author = ""
date = 2021-07-03T03:00:00Z
description = ""
image = "/images/b12d1bcc350b7a78dc84cd7e58f8e470-1.jpg"
image_webp = "/images/b12d1bcc350b7a78dc84cd7e58f8e470.jpg"
title = "POSSE DESAFÍA A SANTILLI Y MANES EN PROVINCIA"

+++

**_En medio de las repercusiones de la baja de la candidatura en CABA por parte de Patricia Bullrich, en Provincia de Buenos Aires la interna de Juntos por el Cambio se mantiene firme. Uno de los primeros en anotarse en territorio provincial fue el hombre de San Isidro, Gustavo Posse, quien está empeñado en devolver el protagonismo radical a la coalición._**

En ese sentido, Posse reivindica la lista que integrará en territorio bonaerense y, a pesar de la insistencia de JxC con la “unidad”, el intendente del conurbano apuesta a las Primarias Abiertas Simultáneas y Obligatorias. “Me parece bien que haya PASO”, expresó esta mañana Posse en diálogo con Radio 10.

“En la Provincia de Buenos Aires vamos a tener PASO. Nos pareció lo mejor que haya PASO, por eso hay tres listas, la de Santilli, la de Manes y la nuestra”, manifestó Posse, quien se refirió a figuras como Facundo Manes y Diego Santilli, quienes se presentan como los cuadros del radicalismo más ligado al macrismo, por un lado, y el hombre de Rodríguez Larreta para la Provincia, por el otro.

“Valoramos lo de Diego Santilli, sabemos que tiene experiencia, pero de igual manera presentamos nuestra lista con gente que está arraigada a la Provincia”, fue el comentario de Posse en relación al vicejefe de Gobierno porteño, quien hizo pública su intención de “trasladarse”  a territorio bonaerense a dar la pelea electoral, lo que generó posiciones encontradas entre los intendentes del GBA.

En cuanto al neurocirujano que ahora fue lanzado como candidato para encabezar la lista de Diputados, Posse sostuvo: “Me parece que la candidatura de Facundo Manes es una buena incorporación”.

Por otro lado, el alcalde de San Isidro habló sobre la situación de María Eugenia Vidal, quien decidió “atrincherarse” en CABA, lo que la puso en disputa con Patricia Bullrich por protagonizar la lista de ese distrito, hasta que la ex Ministra decidió bajarse de esa pugna de manera reciente. “Se veía venir lo de María Eugenia Vidal, es una decisión política y personal”, señaló Posse.