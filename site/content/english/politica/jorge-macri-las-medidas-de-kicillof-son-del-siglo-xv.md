+++
author = ""
date = 2021-05-03T03:00:00Z
description = ""
image = "/images/inl2kjwiqzeuhcnsznthp4dty4.jpg"
image_webp = "/images/inl2kjwiqzeuhcnsznthp4dty4.jpg"
title = "JORGE MACRI: \"LAS MEDIDAS DE KICILLOF SON DEL SIGLO XV\""

+++

**_El intendente de Vicente López, Jorge Macri, criticó las medidas tomadas por el gobernador Kicillof para frenar el avance de la segunda ola de coronavirus._**

El Intendente de JxC disparó "Cerrar solamente no tiene el efecto deseado. Además, porque se afecta a gastronómicos, gimnasios ahora, salones de fiesta, y esa gente quiere laburar”.

Y disparó: "El gobernador propone aislamiento y cierres como si esa fuera la única herramienta. Puede ser que eso fuera en el Siglo XV, ahora hay otras útiles como vacunar y testear".

Macri criticó al oficialismo por la decisión de prorrogar la suspensión de la presencialidad educativa en el AMBA: "Es raro ver que se permiten ferias al aire libre y que no podamos tener al menos educación inicial y primaria"