+++
author = ""
date = 2022-04-04T03:00:00Z
description = ""
image = "/images/fdt.jpg"
image_webp = "/images/fdt.jpg"
title = "SEGÚN UNA ENCUESTA, EL GOBIERNO DE ALBERTO FERNÁNDEZ Y CRISTINA KIRCHNER \"ATRAVIESA EL PEOR MOMENTO DESDE QUE ASUMIÓ EN MATERIA DE EXPECTATIVAS\""

+++
#### En medio de la interna de Frente de Todos y mientras golpea la inflación, la imagen negativa del presidente y la vicepresidenta creció fuerte entre los electores en marzo, según el sondeo mensual de la consultora Fixer

La encuesta de opinión pública que elabora mensualmente la consultora Fixer reveló que el gobierno de Alberto Fernández y Cristina Kirchner “atraviesa el peor momento en materia de expectativas desde que asumió” y detalló un fuerte impacto político tanto de la interna del Frente de Todos como de la delicada situación económica del país.

Según el informe de opinión pública, la interna de la alianza oficial también está pegando fuerte en la imagen de sus principales líderes. Mientras la inflación se consolida como la principal preocupación de los consultados -seguido más lejos por la corrupción y la inseguridad-, el distanciamiento entre el presidente y la vicepresidenta está generando un alto costo personal y político de cara a sus propios votantes. También, otros líderes clave del dispositivo que ideó la ex presidenta para lograr ganarle a Mauricio Macri las elecciones en 2019, el gobernador bonaerense Axel Kicillof y el líder de La Cámpora, Máximo Kirchner, también tienen un aumento -aunque más moderado- de la imagen negativa, que supera en ambos casos el 60%.

“Este escenario tan complejo vuelve a impactar en la imagen de los principales actores del Frente de Todos. El presidente Alberto Fernández sube dos puntos la negativa intramensual (de 61 a 63%) y baja 3 puntos su imagen positiva (de 27 a 24%). La caída se explica entre los votantes del oﬁcialismo, donde se reﬂeja una baja de 10 puntos (del 57% al 47%)”, resalta el informe de opinión pública publicado hoy.

“La vicepresidenta Cristina Fernández de Kirchner baja 2 puntos la positiva (28%) y sube 1 punto la negativa (64%). El gobernador Axel Kicillof sube 1 punto la positiva (28%) y sube 1 punto la negativa (62%), manteniéndose estable en la comparación febrero-marzo”, agrega el informe y subraya que el ex presidente del bloque Frente de Todos en la Cámara de Diputados también tuvo un empeoramiento de su registro, al pasar del 64 al 66 por ciento de imagen negativa.

Estos números de marzo coinciden con el peor momento de la interna del Frente de Todos, con sus principales referentes -el presidente y la vice- enfrentados y el gobierno nacional en tensión entre las facciones que lo integran. El debate y la aprobación posterior del acuerdo con el Fondo Monetario, el aumento de la inflación y diferencias sobre el rumbo económico confluyeron para agravar una ya debilitada estructura interna del oficialismo.

“La principal preocupación de los argentinos (63%) es la inﬂación. Le siguen, lejos, los hechos de corrupción (40%) y la inseguridad (29%). Es la primera vez en los últimos 3 años que el ﬂagelo inﬂacionario rompe la barrera de los 60 puntos. Además, el 69% de los argentinos cree que la inﬂación será mayor. En esta preocupación no hay grieta”, indica el trabajo.

“En términos de expectativas, el gobierno atraviesa su peor momento desde que asumió. El 51% cree que su situación económica será peor en los próximos dos años”, resalta el trabajo y agrega que “el 51% cree que su situación económica será peor en los próximos dos años”.

Según los datos de la consultora, la expectativa negativa llegó a superar a la que se registró en el momento más complicado de la pandemia de COVID 19 y de octubre del año pasado, en la previa de las elecciones nacionales que terminaron con una dura derrota para el gobierno de Alberto Fernández y Cristina Kirchner. Al ser consultados sobre si “considera que la situación económica dentro de dos años será mejor, igual o peor que la actual”, sólo el 23% respondió de manera optimista y el 17% contestó que estará “igual”.

De febrero a marzo, el sondeo de Fixer también recogió un importante aumento del problema inflación entre los consultados, ya que pasó del 56% al 63%, mientras que el resto de los temas quedaron igual o bajaron con respecto a las consultas realizadas previamente: corrupción, seguridad, sistema judicial, trabajo, la educación y la salud.

Por otra parte, el sondeo resaltó que en Juntos por el Cambio, la foto de marzo impacta de forma heterogénea: Mauricio Macri sube 1 punto la positiva (33%) y sube 3 la negativa (51%), es decir, se mantiene estable en su núcleo duro. También cae 9 puntos la positiva en los desencantados de Alberto Fernández y sube la negativa de 45 a 59%. Asimismo, cae 5 puntos en el votante duro de Juntos por el Cambio, que en febrero tenía el 99% de positiva y en marzo llegó al 94%”.

Además, advierte que Javier Milei sube 3 puntos la positiva (37%) y baja 1 punto la negativa (38%). Desde que lanzó su candidatura en la Ciudad, en septiembre del año pasado, no ha logrado tener diferencial neto positivo a nivel nacional: “El perﬁl de votante del diputado liberal es hombre, tiene entre 16 y 35 años, secundario completo y/o terciario incompleto, y tiene una mirada muy negativa sobre el futuro: el 93% de sus votantes cree que la inﬂación va a ser más alta y el 75% sostiene que su situación económica será peor en los próximos dos años”.