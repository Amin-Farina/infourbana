+++
author = ""
date = 2021-07-12T03:00:00Z
description = ""
image = "/images/20170624014254_0623manesvidalg-1.jpg"
image_webp = "/images/20170624014254_0623manesvidalg.jpg"
title = "ARDE EL PRO BONAERENSE Y RESPONSABILIZAN A MARÍA EUGENIA VIDAL POR LA CANDIDATURA DE FACUNDO MANES"

+++
###### FUENTE: [Letra P](https://www.letrap.com.ar/nota/2021-7-10-11-25-0-vidal-no-compito-en-la-provincia-de-buenos-aires-porque-quiero-que-otros-crezcan)

**_La exgobernadora bonaerense se lanzó en la Ciudad con muletilla 2019: “En esta elección se juega el futuro de los argentinos”. “Nunca especulé”, dice._**

La candidatura de Vidal fue leída en las filas internas del PRO como un semáforo verde para el surgimiento a la figura de Manes. Alineados con esta crítica se encuentran dirigentes de gran peso en el espacio como Jorge Macri, Patricia Bullrich y el mismo Mauricio Macri. Insisten con la conveniencia de una derrota del porteño Diego Santilli en PBA, lo que dificultaría la candidatura de Horacio Rodríguez Larreta.

La exgobernadora "orgullosamente bonaerense", María Eugenia Vidal, lanzó este sábado su precandidatura a diputada nacional por la Ciudad de Buenos Aires y abrió el paraguas al aludir a su cambio de territorio para jugar electoralmente. “No compito en la provincia de Buenos Aires porque quiero que otros crezcan”, ensayó como explicación en un acto a puertas cerradas en Palermo, donde estuvo acompañada por el jefe de Gobierno porteño, Horacio Rodríguez Larreta; la titular del PRO, Patricia Bullrich y el senador Martín Lousteau, entre otros.

En una breve alocución, Vidal se encargó de focalizar en dardos al Gobierno y en explicar los motivos por los que no compite en la provincia que gobernó.

“Jamás especularía, soy consciente de que todo esto es mucho más grande que yo”, expuso luego de anunciar su precandidatura en “la Ciudad que me vio nacer, donde pasé mi infancia y juventud, donde empecé a hacer política, donde fui ministra y vicejefa de Gobierno”. Al dirigirse al pueblo bonaerense, dijo: “Mi amor, compromiso y respeto no va a cambiar nunca”, a la vez que trató de justificarse minimizando límites jurisdiccionales: “Más allá de la General Paz, somos argentinos defendiendo a otros argentinos”.

Al destacar que no compite en terreno bonaerense “porque quiero que otros crezcan”, Vidal, que, como reveló Letra P, no quería arriesgarse a una nueva derrota, sostuvo: “Si siempre estamos los mismos en los mismos lugares, otros no crecen”, por lo que recalcó: “Hoy apuesto por nuestros candidatos en Provincia y estoy segura que harán una gran elección”. E insistió: “Nunca especulé, nunca saqué ventaja”.

Vidal agradeció a Bullrich “por elegir la unidad” y elogió a Lousteau por estar “haciendo un mejor radicalismo en la Ciudad y muchos lugares del país". Y ya en modo Ciudad, enfatizó: "Gracias inmenso a los porteños que me recibieron cuando decidí volver a vivir en la Ciudad con mis hijos".

La exmandataria bonaerense bregó por un Juntos por el Cambio “amplio, unido, diverso para transformar el país”. Aquí, comenzó a desplegar una batería de dardos a la gestión de Alberto Fernández y dijo que pretendía un lugar en el Congreso para “defender a los argentinos de los atropellos de poder, de la insensibilidad y distancia como forma de gobierno”. Machacó contra la “falta de un plan, agresiones a la oposición y soberbia” del Ejecutivo.

“No es lo que hay, hay algo mucho mejor, podemos construir algo mucho mejor”, arengó con reminiscencias al eslogan en desuso “Sí, se puede”.

Tras cuestionar “la inflación y la falta de futuro”, dijo que pretende ser diputada “para que no nos aplasten con impuestos, para que la educación se garantice todos los días y que el aprendizaje que se perdió, vuelva”.  Bajo esa tónica, desempolvó la advertencia épica que usó, sin éxito, en su campaña por la reelección de 2019: “En esta elección se juega el futuro de los argentinos, por eso estoy”.