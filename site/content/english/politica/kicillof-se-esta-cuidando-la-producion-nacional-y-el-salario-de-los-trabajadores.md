+++
author = ""
date = 2021-12-15T03:00:00Z
description = ""
image = "/images/fgmcxlwxiaand1l.jpg"
image_webp = "/images/fgmcxlwxiaand1l.jpg"
title = "KICILLOF: \"SE ESTÁ CUIDANDO LA PRODUCIÓN NACIONAL Y EL SALARIO DE LOS TRABAJADORES\""

+++

#### **_Así lo expresó el mandatario provincial al participar de la inauguración del Centro de Distribución de Alpargatas Textil, en Florencio Varela, donde destacó que "no es casualidad" la reapertura de la firma, sino que es consecuencia de políticas en favor de la producción y el empleo._**

El gobernador bonaerense, Axel Kicillof, destacó este martes las políticas orientadas al "cuidado de la producción nacional y el salario de los trabajadores", medidas que diferenció de las tomadas durante la gestión de Cambiemos, caracterizadas por la "pérdida del acceso al crédito productivo, caída de salarios, apertura de importaciones, endeudamiento y fuga de capitales".

"Alpargatas no es solo una marca comercial, es una empresa que forma parte de la historia argentina y también de la historia de cada uno de nosotros, de nuestra infancia y de nuestra vida cotidiana", expresó Kicillof.

El gobernador cuestionó las políticas aplicadas durante la administración de Mauricio Macri, al señalar que la firma Alpargatas Textil atravesó como tantas otras "épocas de prosperidad y de crisis, llegando a cerrar producto de la aplicación de políticas cuyo único resultado fue la destrucción de la industria nacional".

"Siempre que se pierde el acceso al crédito productivo, caen los salarios y se abren las importaciones, se inicia un ciclo de endeudamiento y fuga de capitales", sostuvo el gobernador y agregó: "Con esas políticas, la industria nacional se contrae y, en el extremo, empieza a desaparecer".

"Así como no fue casualidad que Alpargatas cerrara, tampoco lo es que hoy vuelva a abrir: sucede porque se está cuidando la producción nacional y el salario de los trabajadores", remarcó.

Producto de una inversión de 40 millones de pesos, el Centro de Distribución concentra el 90% de la logística de la empresa en la provincia de Buenos Aires, desde donde distribuye sus productos a todo el país.

Alpargatas Textil (Nea Tex), con el objetivo de crecer en el mercado interno y retomar las exportaciones regionales, prevé inversiones para incrementar su producción en un 40 por ciento.

Kicillof estuvo en la inauguración junto al ministro de Producción, Ciencia e Innovación Tecnológica, Augusto Costa; los secretarios nacionales de la Pequeña y Mediana Empresa y los Emprendedores, Guillermo Merediz; y de Industria, Economía del Conocimiento y Gestión Comercial Externa, Ariel Schale; el intendente local, Andrés Watson; el diputado nacional Julio Pereyra; y el CEO de la compañía, Carlos Peñarrocha.

A su turno, Costa destacó que en todo el territorio bonaerense se observa una "recuperación muy rápida de la actividad económica después de muchos años de pérdidas" y precisó que "entre 2016 y 2019 se perdieron 10.500 pequeñas y medianas empresas, una de las cuales fue Alpargatas.

El funcionario contrastó ese dato con los "37.800 puestos de trabajo registrado" que "se generaron en el último año", número que, evaluó, "muestra la importancia de la industria para la creación de empleo y la recuperación del ingreso".

Ariel Schale enfatizó que "Alpargatas regresa a Buenos Aires y a Florencio Varela, y esto es producto de la decisión política del gobierno nacional, provincial y municipal de impulsar la reindustrialización, con políticas de sustitución de importaciones, cuidado del mercado local y líneas de financiamiento productivo para que nuestros industriales sigan reinvirtiendo y generando trabajo en Argentina".

"Hoy estamos viviendo un hecho de reivindicación histórica: después de un período de retiro de esta empresa del distrito, en solo dos años logramos que Alpargatas se instale nuevamente en Florencio Varela", expresó Watson y agregó que "fue producto de la decisión política de la Nación y de la Provincia de impulsar la industria y el trabajo genuino".

En tanto, Merediz aseguró que "constantemente recorremos empresas como Alpargatas que, a pesar de que aún hay dificultades, hoy tienen trabajo, pedidos, movimiento y eso es lo fundamental, fruto de la apuesta diaria que hace el sector privado y también del acompañamiento que hacemos desde el Estado".

Sostuvo que "esta empresa que hoy visitamos en la reapertura de su planta de Florencio Varela acaba de recibir un certificado de elegibilidad de la SEPYME para un crédito por 250 millones de pesos, que les permitirá incorporar nueva maquinaria para aumentar su capacidad de producción, exportar y sumar personal".

Por su parte, Peñarrocha remarcó que "estamos cumpliendo el sueño de volver nuevamente a Florencio Varela, después de conseguir que empresarios argentinos invirtieran en la empresa".

Por último, Kicillof reafirmó el compromiso de "sostener estas políticas para acompañar a los empresarios nacionales que han sufrido las políticas de desindustrialización junto con los trabajadores".

"Necesitamos que inviertan, trabajen y crean en el país para que, junto a nuestro pueblo, nos ayuden a reconstruir la provincia de Buenos Aires", completó el mandatario.

En el acto también estuvieron presentes el presidente de la empresa, Alberto María; el vicepresidente, Enzo María; la subsecretaria de Industrias, PyMEs y Cooperativas, Mariela Bembi; el director provincial de Desarrollo Territorial y PyMEs, Ariel Aguilar; el presidente de la Unión Industrial de Florencio Varela, Antonio Da Costa; y el presidente del Concejo Deliberante, Gustavo Rearte.