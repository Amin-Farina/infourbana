+++
author = ""
date = 2022-03-10T03:00:00Z
description = ""
image = ""
image_webp = "/images/foto-larreta.webp"
title = "RODRÍGUEZ LARRETA Y WADO DE PEDRO NO LLEGARON A UN ACUERDO EN LA AUDUENCIA ANTE LA CORTE POR LA COPARTICIPACIÓN"

+++
#### El máximo tribunal les dio un plazo de 30 días para llegar a un acuerdo. Ahora la Corte resolverá la cautelar

Sin acuerdo finalizó la audiencia de conciliación convocada por la Corte Suprema de Justicia de la Nación en el marco del litigio por la quita de recursos de la coparticipación a la Ciudad de Buenos Aires dispuesta por el gobierno de Alberto Fernández en 2020. El máximo tribunal les dio un plazo de 30 días a las partes para que intenten arribar a una conciliación. Si transcurrido ese plazo no acuerdan la Corte resolverá la cautelar solicitada por el gobierno de la Ciudad de Buenos Aires.

El monto actualizado que la administración que encabeza Horacio Rodríguez Larreta reclama que sea restituido por la Nación es de 120 mil millones de pesos. Además, el tribunal dispuso que informen semanalmente los avances de la negociación.

Estuvieron presentes los cuatro ministros del máximo tribunal, Horacio Rosatti, Carlos Rosenkrantz, Ricardo Lorenzetti y Juan Carlos Maqueda. Por el gobierno nacional asistió el ministro del Interior, Eduardo Wado de Pedro; y por la administración porteña lo hicieron el Jefe de Gobierno, Horacio Rodríguez Larreta; el Jefe de Gabinete, Felipe Miguel; el ministro de Justicia y Seguridad, Marcelo D’Alessandro y el procurador de la Ciudad, Gabriel Astarloa.

El conflicto tiene como telón de fondo la transferencia de las funciones de seguridad a la órbita de la Ciudad de Buenos Aires y se inició con dos causas promovidas por el gobierno porteño en el año 2020 a raíz del recorte de más de un punto de coparticipación por el Estado Nacional. La poda de recursos fue dispuesta primero por el dictado del decreto 735 de septiembre de 2020 y luego una ley sancionada en diciembre del mismo año. Esto llevó al gobierno de la Ciudad a solicitar a la Corte Suprema de Justicia de la Nación que declare la inconstitucionalidad de las normas y el pago de una suma que compense la pérdida sufrida desde entonces.

Por su parte, el Estado Nacional cuestionó la legitimidad del decreto 194/2016 dictado por el entonces presidente Macri en enero de ese año, a pocos días de asumir en su cargo, y por el cual se había elevado de 1,40 a 3,75 el porcentual que recibía la Ciudad de la masa de fondos coparticipables. Para el gobierno que encabeza Alberto Fernández ese porcentaje fue fijado de modo completamente arbitrario por la administración macrista.

Con la finalidad de posibilitar el arribo a una solución, la Corte Suprema llamó a las partes a la audiencia de conciliación que se celebró este jueves en el Palacio de Tribunales.

El ministro de Pedro llevó al encuentro informes que -según sostuvo- muestran que el presupuesto que el expresidente Mauricio Macri otorgó durante su gobierno a la Ciudad fue más del doble de lo que correspondía.

En su reclamo el gobierno porteño afirmó que a partir de 1994 la Ciudad de Buenos Aires adquirió un nuevo estatus jurídico -asimilable al de las provincias según jurisprudencia de la Corte Suprema de Justicia de la Nación- circunstancia que impide que el Estado Nacional de manera unilateral modifique el porcentaje de coparticipación sin el acuerdo previo de CABA. Esto afectaría su autonomía -argumentó la administración porteña- generando una discriminación por parte del Estado Nacional respecto del resto de las provincias.