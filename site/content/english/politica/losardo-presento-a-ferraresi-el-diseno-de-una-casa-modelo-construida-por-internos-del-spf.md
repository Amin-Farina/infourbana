+++
author = ""
date = 2021-01-26T03:00:00Z
description = ""
image = "/images/losardo.jpg"
image_webp = "/images/losardo.jpg"
title = "Losardo presentó a Ferraresi el diseño de una casa modelo construida por internos del SPF"

+++

**_El proyecto de casas modulares reúne el trabajo de varios de los talleres ubicados en las unidades penitenciarias, como herrería, carpintería y carpintería metálica, y las partes que se producen en los diferentes establecimientos, son enviadas posteriormente al Complejo de Ezeiza para construir las viviendas._**

La ministra de Justicia y Derechos Humanos, Marcela Losardo, presentó a su par de Desarrollo Territorial y Hábitat, Jorge Ferraresi, el diseño de una casa modelo, sustentable y ecológica, construida por los internos en el marco del proyecto piloto de construcción de viviendas modulares en seco que desarrolló el Servicio Penitenciario Federal (SPF).

La presentación se realizó durante una visita que Losardo y Ferraresi realizaron al Complejo Penitenciario Federal I, en Ezeiza, donde los funcionarios recorrieron los talleres productivos en los cuales las personas privadas de su libertad reciben capacitación en distintos oficios, informó el Ministerio de Justicia en un comunicado.

Según se indicó, en reuniones previas entre representantes de ambos ministerios se estudió la posibilidad de que la cartera de Hábitat adquiera insumos fabricados por personas privadas de su libertad en los talleres laborales de las unidades federales.

Losardo destacó que el SPF cuenta con talleres laborales en todos los establecimientos del país, y resaltó que "este proyecto da a las personas que trabajan en estos talleres una capacitación laboral importantísima para su futuro".

> "Si además logramos articular la producción en las unidades penitenciarias con las necesidades de otras áreas del Estado, podremos potenciar el trabajo que el SPF lleva adelante para lograr que las personas privadas de su libertad cuenten con mejores oportunidades al momento de su egreso", explicó Losardo.

El proyecto de casas modulares reúne el trabajo de varios de los talleres ubicados en las unidades penitenciarias, como herrería, carpintería y carpintería metálica, y las partes que se producen en los diferentes establecimientos, son enviadas posteriormente al Complejo de Ezeiza para construir las viviendas.

Ferraresi, por su parte, señaló que "estamos coordinando esta propuesta junto al Servicio Penitenciario Federal para que aquellos internos que participen de los talleres que les brindan las distintas unidades, puedan contar con la posibilidad de capacitarse en la construcción de viviendas industrializadas".

Acompañaron a los ministros en la visita el secretario de Justicia, Juan Martín Mena, y la subsecretaria de Asuntos Penitenciarios e interventora de la Dirección Nacional del SPF, María Laura Garrigós; el secretario de Coordinación de la cartera de Hábitat, Guillermo Pesce, y el subdirector Nacional del SPF, Sabino Guaymás.