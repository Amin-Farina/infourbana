+++
author = ""
date = 2021-07-06T03:00:00Z
description = ""
image = "/images/macrijpg.jpg"
image_webp = "/images/macrijpg.jpg"
title = "SIN LLEGAR A UN ACUERDO CON LARRETA, JORGE MACRI MANTIENE SU CANDIDATURA"

+++

**_El jefe de Gobierno porteño trata de negociar con Jorge Macri para lograr que baje su candidatura en la provincia de Buenos Aires._**

El jefe de Gobierno porteño, Horacio Rodríguez Larreta, trata de negociar con el intendente de Vicente López, Jorge Macri, para lograr que baje su candidatura en la provincia de Buenos Aires. De esta manera, busca armar una lista encabezada por Diego Santilli, pero todavía no ha sido posible alcanzar un acuerdo.

El primo del ex presidente Mauricio Macri y titular del PRO bonaerense mantuvo un encuentro de una hora y media con el alcalde porteño, de acuerdo a lo confirmado por la agencia Noticias Argentinas. 

Sin embargo, en ese encuentro el jefe comunal ratificó su postura de ser candidato a diputado nacional por la Provincia, en contrario a la idea de Rodríguez Larreta de que sea Diego Santilli el único precandidato del PRO que en las PASO de Juntos por el Cambio se mida contra el radical Facundo Manes.

##### Rodríguez Larreta negocia para que Santilli sea el único candidato que enfrente a Manes en las primarias

A pesar de las gestiones hechas en las últimas horas -en las que desde el ala de Larreta ya se logró cerrar la interna del PRO en la Ciudad de Buenos Aires- todavía no se ha alcanzado un acuerdo entre los referentes partidarios de cara a las primaras del mes de septiembre. De todas maneras, se espera que las conversaciones continúen en los próximos días.

El actual intendente de Vicente López se mostró reticente desde un principio a la posibilidad de que el vicejefe de Gobierno porteño encabece la lista en la Provincia. Así, apuntó a macar territorio junto a un grupo de intendentes del PRO.

No obstante, varios de ellos se acercaron en los últimos días a Rodríguez Larreta, a lo que se sumó que su otra aliada en la pelea, Patricia Bullrich, acordó con el mandatario porteño declinar su aspiración de ser candidata en CABA.

Sin embargo, en las últimas horas se pudo saber que el presidente del PRO bonaerense mantiene conversaciones fluidas con la UCR, partido que lanzó oficialmente a Manes de cara las primarias, al igual que el ex presidente de la Cámara de Diputados Emilio Monzó.

##### Patricia Bullrich declinó su candidatura en CABA

En los últimos días, Patricia Bullrich declinó su candidatura en CABA

Tanto Jorge Macri como Monzó mantienen abiertas las negociaciones con Rodríguez Larreta mientras charlan con el radicalismo de cara al cierre de las listas, el 24 de julio.

No obstante, en las PASO podría haber también otra lista radical, dado que el intendente de San Isidro, Gustavo Posee, busca competir en las internas de Juntos por el Cambio en la Provincia, a la cabeza de su propia nómina.

Por otro lado, la alianza opositora debe resolver también qué hará en la línea interna del Peronismo Republicano que lidera el ex candidato a vicepresidente Miguel Ángel Pichetto.

Este sector, integrado además por dirigentes como Ramón Puerta o Miguel Ángel Toma, se reunirá este martes para definir cómo se posicionarán en la pelea interna, luego de que Pichetto sugiriera que podría bajar su postulación en favor de la unidad del frente opositor.