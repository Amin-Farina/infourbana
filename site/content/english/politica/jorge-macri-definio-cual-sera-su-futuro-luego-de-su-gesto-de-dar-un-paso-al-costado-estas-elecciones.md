+++
author = ""
date = 2021-08-08T03:00:00Z
description = ""
image = "/images/5f296a57d90e8_1004x565-1.jpg"
image_webp = "/images/5f296a57d90e8_1004x565.jpg"
title = "JORGE MACRI DEFINIÓ CUÁL SERÁ SU FUTURO LUEGO DE SU GESTO DE DAR UN PASO AL COSTADO ESTAS ELECCIONES"

+++

Jorge Macri mantuvo hasta el final una tensa discusión con Horacio Rodríguez Larreta por la candidatura de Diego Santilli en la provincia de Buenos Aires. El intendente de Vicente López finalmente dio un paso al costado en las PASO y anoche reveló cuál será su futuro político al no poder ser reelecto nuevamente: “En 2023 por supuesto voy a ser candidato”.

En un reportaje exclusivo brindado a Intratables el primo de Mauricio Macri celebró la candidatura de Facundo Manes y el nuevo rol de los radicales: “Me parece muy bueno que la Unión Cívica Radical tenga ambición de poder, aunque no coincido con la acusación de Gerardo Morales contra Horacio Rodríguez Larreta”. Pidió terminar con las divisiones.

El intendente macrista dejó clara su posición y su alineamiento con el ala dura de Juntos por el Cambio. “Si María Eugenia Vidal hubiera venido a la Provincia, sería más fácil, pero ya está, tengo decidido mi voto, voy a acompañar a Diego Santilli, soy el presidente del PRO y sería muy berreta decir que no voy a votar al candidato de mi partido”, señaló.

Al aire de América TV repudió el nivel de discusión que existe en la interna de la oposición en la provincia de Buenos Aires al considerar que de esta manera “no se resuelven los problemas de la gente”. Además, acusó a ambos sectores de estar siendo funcionales al kirchnerismo y les rogó bajar la intensidad de las críticas tanto a Diego Santilli como a Facundo Manes.

Jorge Macri vio con buenos ojos que sean candidatos personas sin cargos públicos como María Eugenia Vidal y marcó: “Sería bueno que llegue a la Gobernación alguien que haya gestionado antes”. Respecto de su futuro político confirmó su preferencia por los cargos ejecutivos y adelantó que buscará la candidatura a gobernador bonaerense dentro de su espacio.