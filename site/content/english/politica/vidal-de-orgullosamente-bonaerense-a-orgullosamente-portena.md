+++
author = ""
date = 2021-07-01T03:00:00Z
description = ""
image = "/images/60590b97a0175_1004x565-1.jpg"
image_webp = "/images/60590b97a0175_1004x565.jpg"
title = "VIDAL DE \"ORGULLOSAMENTE BONAERENSE\" A \"ORGULLOSAMENTE PORTEÑA\""

+++

**_La ex gobernadora María Eugenia Vidal, quien puertas adentro del PRO ya dijo que no quería competir en la provincia de Buenos Aires en las próximas elecciones legislativas y busca ser candidata en la Ciudad, realizó un cambió en su perfil de Twitter y ya no es "orgullosamente bonaerense"._**

En efecto, mientras Larreta quiere que la candidata en la Ciudad sea María Eugenia Vidal y el ala dura del PRO intenta imponer a Patricia Bullrich, este lunes la ex gobernadora decidió cambiar su bio de Twitter y eliminó el ítem "Orgullosamente bonaerense".

Esto hizo que algunos de sus seguidores tildaran a la referente nacida en el barrio porteño de Flores de "mentirosa". Esto se suma al enojo de muchos de sus copartidarios, entre los que está Mauricio Macri, quienes la consideraban una pieza electoral clave para competir en la provincia de Buenos Aires.

Además de cambiar la descripción, Vidal decidió modificar su foto de perfil: desapareció el retrato a color donde se la veía sonriente y fue la imagen de la campaña por su reelección, y la cambió por una foto en blanco y negro en donde se la ve más seria.

#### Confidencial: lo que nadie reveló de la interna Macri - Vidal

La oposición está logrando concentrar la atención, no por sus propuestas o su desafío al oficialismo, sino por la novela de las candidaturas.

"Yo no controlo a Patricia, creeme", le habría dicho Mauricio Macri a María Eugenia Vidal en una reunión privada, que, como todo en la oposición, dejó de ser privada cuando los "operadores" de uno y otro protagonista, empezaron a dejar trascender las alternativas de la misma.

Dicen que la mueca de Vidal ante semejante aseveración fue suficiente para dejar sentado su descreimiento sobre la misma. La charla, dicen, fue en buenos términos, pero nadie le creyó nada al otro.

Tampoco Macri le creyó a Vidal cuando afirmó, contundente, que si no había lista de unidad en Ciudad, ella directamente se bajaba de la candidatura. Muy cerca del expresidente dicen: "¿Para que sería toda esta puesta en escena si su postulación depende de lo que haga Patricia? Si ese es el esquema, Bullrich no se baja ni en pedo".

> "Si la Heidi se baja, qué suerte, acá ni un paso atrás", explican en la intimidad más íntima de Bullrich.

Es cierto que nadie controla a Patricia, pero "la Piba" es un soldado, si Macri trata de convencerla, lo va a escuchar, seguramente argumente a su favor y busque convencerlo, pero de ningún modo va a ignorar a quien reconoce hoy como su jefe político.

El problema para que eso ocurra, es que Macri no quiere que se baje Bullrich y vería con mucho agrado que la que se baje sea Vidal. La amenaza de "la Heidi" (como la llaman en el bullrichismo) en realidad no es tal, a nadie le importa si se baja, en definitiva, de hacerlo, sería como irse a Provincia como le exigían Macri y Bullrich. "Todo estaba bastante ordenadito cuando esta chica había decidido desaparecer; si se baja, se ordena de vuelta", explican.

Es que el ex líder del PRO, quiere preparar el escenario para volver en 2023, sin Vidal ganando una elección este año, su camino se facilita, le queda, a su criterio, solamente Horacio Rodríguez Larreta como contendiente interno. "Uno menos", grafican en sus cercanías respecto a la "amenaza" de Vidal.

Bullrich no es en ese sentido un obstáculo. En el macrismo reconocen que la ambición de Patricia es la misma que la de Vidal, pero tienen la certeza que si tiene que correrse para que el candidato sea Macri, no va a dudar en hacerlo.

Por su parte, Larreta está ordenando sus fichas y puliendo su plan de acción. Pase lo que pase en Ciudad, entiende que la Provincia es un escenario que debe dejar preparado si quiere ser presidente en 2023 y ese es hoy, uno de los puntales de su estrategia.

Y, por otro lado, su relación con la UCR es otro de los aspectos que entiende que debe cuidar. Los radicales tienen hoy, dos dirigentes de peso nacional: Martín Lousteau y Facundo Manes y alrededor de ellos dibujan toda su estrategia.

Con Lousteau, Larreta ya tiene un plan definido y un acuerdo en curso: le va a dejar la Ciudad en 2023 para que el economista la gobierne, a cambio del apoyo radical a su postulación presidencial. Eso está en marcha y las partes, por ahora, confían una en la otra.

Pero hay que contener a Manes, que además, responde a un sector del radicalismo que no es el mismo que el de Lousteau. "Son como dos acuerdos distintos para sostener", dicen en el larretismo.

Como adelantamos en otra de nuestras entregas confidenciales, el neurocientífico estaba exigiendo ser la cabeza de la lista en las elecciones de septiembre-noviembre, "sin disputas". Manes no quería el desgaste de las primarias y consideraba que su imagen pública y su prestigio, eran suficientes para que no le pongan a nadie a disputarle el espacio.

Su ambición está muy cerca de concretarse. Uno de sus contendientes eventuales, Jorge Macri, está totalmente desdibujado. Su primo, el ex presidente, que lo usó como carta para amenazar con jugar en provincia, empezó a dejar de lado la idea, a sabiendas que quedaría "pegado" al candidato menos votado en una eventual interna.

Otro de quienes gritaban a los cuatro vientos su candidatura era Emilio Monzó. Aunque de menor volumen, en una PASO con cuatro candidatos podría hacer sido medianamente competitivo. Pero lleva un proceso de acuerdo con los radicales que puede terminar muy bien, siempre tuvieron buen vínculo y la presencia de Manes, sin Jorge Macri en carrera y una posible declinación de Diego Santilli, lo forzarían a un acuerdo que tampoco ven tan mal en su entorno.

¿Declinación de Santilli? Si, en realidad de Larreta, en su movimiento táctico para acordar con la UCR que empuja a Manes, un gesto al propio neurocirujano que concretaría su ilusión de no tener una lista que le de pelea en la primaria.

En el larretismo creen que no tiene sentido forzar al actual vicejefe de gobierno teniendo un candidato como Manes, al que, si le sacan a Santilli del medio, podrían incluso "adquirir" como una figura relevante de apoyo a la candidatura presidencial de Horacio.

Manes, una dama del larretismo segunda, y Monzó tercero, sería una lista muy aceptada por todos, menos por Macri, pero en definitiva poco importa. Podrían darle al ex presidente la segunda dama (o el cuarto lugar, es lo mismo). Cerca del jefe de gobierno con aspiración presidencial, creen que el principal pataleo del expresidente podría estar relacionado con que su candidata quede debajo de Monzó, al que desprecia. Es una cuestión mas simbólica que política. Podría negociarse, si el de Catlos Tejedor lo acepta.

Una novela en definitiva, llena de amores, odios, rencores y pujas de poder, que acapara la atención de los electores, mucho mas que las deficiencias y errores del gobierno nacional, que siente alivio cuando los Onur y Sherezade del PRO, se reúnen, se desconfían, se miran de reojo, se sospechan.