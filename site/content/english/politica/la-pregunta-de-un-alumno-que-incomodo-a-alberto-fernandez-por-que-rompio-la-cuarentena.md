+++
author = ""
date = 2022-03-03T03:00:00Z
description = ""
image = "/images/alberto-nenes-la-rioja.jpg"
image_webp = "/images/alberto-nenes-la-rioja.jpg"
title = "LA PREGUNTA DE UN ALUMNO QUE INCOMODÓ A ALBERTO FERNÁNDEZ: “¿POR QUÉ ROMPIÓ LA CUARENTENA?”"

+++

##### **_Un alumno de una escuela en La Rioja incomodó este miércoles al presidente Alberto Fernández durante un acto de inauguración del ciclo lectivo 2022. “¿Por qué rompió la cuarentena?”, fue la pregunta que hizo el menor mientras el primer mandatario estaba brindando su discurso._**

El cuestionamiento del alumno hacía referencia a la celebración del cumpleaños de Fabiola Yáñez en la Quinta de Olivos en julio de 2020, cuando regía en el país una cuarentena estricta por la pandemia de covid. El festejo salió a la luz varios meses después con la publicación de una foto en la que se veía una decena de invitados, violando el aislamiento vigente y sin cumplir los protocolos sanitarios.

“Yo te escucho en un ratito, te escucho en un ratito”, le respondió Fernández al niño. Durante el acto, el menor fue retirado del lugar por personal de seguridad, pero posteriormente el Presidente dialogó con él en un aula de la institución.

El intercambio entre el alumno y Fernández ocurrió en la Escuela Normal Primaria “Doctor Pedro Ignacio de Castro Barros”, donde el primer mandatario asistió junto al jefe de Gabinete, Juan Manzur, y el gobernador riojano, Ricardo Quintela.

Alejandra Ormeño, una profesora que presenció el diálogo entre Fernández y el alumno, contó al medio local Multiplataforma Fenix que la conversación entre ambos fue “buena”. Según informaron medios locales, el menor tiene 11 años pero su identidad se mantiene en reserva.

“El alumno quería preguntar acerca de cuestiones sobre la pandemia, lo interpeló sobre distintos temas, entre ellos, le preguntó sobre si rompió el aislamiento”, relató la docente, quien consideró que el presidente “trató de responderle a todo”, y que “fue muy comprensivo”.

Además, la profesora dijo: “Creo que el alumno quedó conforme con las respuestas. Yo también quedé conforme, porque no fue un adoctrinamiento, sino respuestas neutrales”, al tiempo que destacó la importancia de “tratar los cuestionamientos y crear conciencia crítica”.

Por su parte, fuentes del Poder Ejecutivo consultadas por Clarín coincidieron en que fue “una buena conversación” y aclararon que el menor padece el síndrome de Asperger. Asimismo dijeron que “fue una situación que manejaron muy bien entre la docente y los compañeros. El joven conversó de distintos temas, fue y vino sobre sus curiosidades, y el Presidente disfrutó conversando con él”. Por otro lado, negaron que haya sido una situación violenta o incómoda.