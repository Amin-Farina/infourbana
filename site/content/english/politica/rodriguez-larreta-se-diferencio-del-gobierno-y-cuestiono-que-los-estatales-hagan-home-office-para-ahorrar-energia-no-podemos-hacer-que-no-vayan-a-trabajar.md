+++
author = ""
date = 2022-01-13T03:00:00Z
description = ""
image = "/images/rwxw4f5cujbvrglxs2hn6darv4.jpg"
image_webp = "/images/rwxw4f5cujbvrglxs2hn6darv4.jpg"
title = "RODRÍGUEZ LARRETA SE DIFERENCIÓ DEL GOBIERNO Y CUESTIONÓ QUE LOS ESTATALES HAGAN HOME OFFICE PARA AHORRAR ENERGÍA: “NO PODEMOS HACER QUE NO VAYAN A TRABAJAR”"

+++

##### **_El jefe de Gobierno porteño, Horacio Rodríguez Larreta, tomó distancia con la decisión del Gobierno de que los empleados públicos realicen teletrabajo este jueves y viernes con el objetivo de “ahorrar energía”, luego de la ola de calor que produjo cortes de luz en 700 mil usuarios. “A la gran mayoría los tenemos abocados al cuidado de la pandemia. No podemos hacer que no vayan a trabajar”, justificó._**

En conferencia de prensa, el funcionario respondió que en la Ciudad de Buenos Aires “no podemos aflojar” en las tareas presenciales y que “la prioridad” es sostener el actual esquema de trabajo, tras ser consultado por el Decreto 16/2022, que instruye a los agentes de la administración pública nacional a que no asistan a las oficinas y desempeñen sus tareas a distancia.

Según Horacio Rodríguez Larreta, los empleados públicos porteños están abocados en “los centros de testeo, donde tenemos 50 mil personas por día, en la vacunación, en la estamos arriba de 30 mil vacunas que aplicamos y en las UFUs”.

“También tenemos la escuela de verano, los polideportivos, con casi 40 mil chicos en las colonias de vacaciones. No podemos aflojar. Nuestra prioridad hoy está en el cuidado de la pandemia y estamos todos trabajando ahí”, concluyó.

De esta manera, el alcalde porteño señaló que no imitará la disposición del Ejecutivo nacional para realizar teletrabajo ante las altas temperaturas, y tampoco precisó si tomaría alguna medida para el ahorro de energía, pese al colapso energético de esta semana involucró cortes de luz que afectaron a millones de personas en la Ciudad de Buenos Aires y en partidos del conurbano bonaerense.

Rodríguez Larreta dio estas definiciones esta mañana en el Polideportivo de Parque Patricios, durante la presentación del nuevo secretario de Deporte porteño, Carlos “Chapa” Retegui, ex entrenador de “Las Leonas”.

En la misma rueda de prensa, el jefe de Gabinete porteño, Felipe Miguel, confirmó la adhesión de la Ciudad a los cambios en el protocolo en el aislamiento para los contactos estrechos con dos dosis de la vacuna y un refuerzo. Según se adelantó, se implementará de manera progresiva en las distintas actividades.

##### La medida oficial del Gobierno

A partir del decreto 16/2022, publicado hoy en el Boletín Oficial, el Gobierno nacional dispuso la modalidad de trabajo remoto o a distancia para hoy y mañana en la administración pública nacional para reducir el consumo de agua y energético frente a las altas temperaturas.

“A los fines de reducir el consumo de energía eléctrica, los días 13 y 14 de enero de 2022, a partir de las 12 horas, las y los agentes de todas las jurisdicciones, organismos y entidades del sector público nacional deberán realizar la prestación de servicios mediante la modalidad de prestación de trabajo a distancia, en caso de que ello resulte posible”, dictamina la medida.

La norma nacional establece que los empleados deberán abstenerse de permanecer o concurrir a sus lugares habituales de trabajo”, donde se mantendrán " las guardias necesarias para preservar la continuidad de los servicios esenciales”.

Quedan expresamente excluidos de los alcances de la decisión el personal de la Administración Nacional de Laboratorios e Institutos de Salud “Dr. Carlos G. Malbrán” (ANLIS); las Fuerzas de Seguridad Federales; las Fuerzas Armadas; el Servicio Penitenciario Federal; el personal de Salud y del sistema sanitario; guarda parques nacionales y los del Sistema Federal de Manejo del Fuego. Tampoco alcanza a los empleados de la Dirección Nacional de Migraciones; Instituciones bancarias y entidades financieras.