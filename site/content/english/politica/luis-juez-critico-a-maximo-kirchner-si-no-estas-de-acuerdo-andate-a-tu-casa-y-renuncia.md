+++
author = ""
date = 2022-02-01T03:00:00Z
description = ""
image = "/images/luis-juez.jpg"
image_webp = "/images/luis-juez.jpg"
title = "LUIS JUEZ CRITICÓ A MÁXIMO KIRCHNER: “SI NO ESTÁS DE ACUERDO, ANDATE A TU CASA Y RENUNCIÁ”"

+++

#### **_El diputado de Juntos por el Cambio cuestionó al dirigente de La Cámpora y puso en duda que la oposición convalide ahora el acuerdo con el FMI_**

El diputado nacional de Juntos por el Cambio, Luis Juez, cuestionó con dureza la decisión de Máximo Kirchner de renunciar a la presidencia del bloque del Frente de Todos, y puso en duda el acompañamiento de la oposición para la aprobación del acuerdo de la renegociación de la deuda con el Fondo Monetario Internacional (FMI).

“Si no estás de acuerdo, andate a tu casa. Renunciá”, sostuvo el cordobés. En referencia a la decisión que tomó el dirigente de La Cámpora, el legislador opositor indicó que Kirchner es “un hombre del Gobierno” y que dar el paso al costado en su función “es una cuestión administrativa que a nadie le importa”.

“A ningún argentino le preocupa la presidencia del bloque”, resaltó en diálogo con radio Rivadavia.

Juez señaló que el desacuerdo de Máximo Kirchner a la negociación y su dimisión a la jefatura de la bancada “era de esperar” y que representa un “acto de cobardía”. “Discutí lo que tu gobierno acordó, poné lo que hay que poner y enfrentá a lo que hay que enfrentar. No te podés aferrar a un relato para seguir haciéndote el progre”.

“Esto es la crónica de una muerte anunciada”, resolvió. “Hace mucho que digo que ‘no nos miren a nosotros’, sino a lo que hace el oficialismo y Cristina”, apuntó el diputado cordobés.

El recrudecimiento de la interna en el Frente de Todos abre una incógnita sobre cuál será ahora la resolución de Juntos por el Cambio para el debate del acuerdo con el FMI en el Congreso. Ante la consulta si ahora el bloque opositor apoyará el pacto firmado con el Poder Ejecutivo, Luis Juez puso en duda si habrá un apoyo cerrado a la negociación, ya que, en este nuevo escenario, “no está discutido”.

Sin embargo, alertó: “No quiero que me vengan a gritar el gol en la la cara. Tenemos que discutir todos juntos. Si sos responsable de que Alberto esté sentado (en el poder), y pretendés salvarte con el acompañamiento nuestro, convencé a los tuyos primero. ¿Con qué autoridad moral van a venir a señalar con el dedo?”.

Más allá de su comentario crítico hacia Máximo Kirchner, Luis Juez también se refirió a las últimas declaraciones del ex ministro de Hacienda de Mauricio Macri, Nicolás Dujovne, quien afirmó que la sociedad argentina “no acompañó” al gobierno de Cambiemos en un programa con el FMI que “nos estaba llevando a resolver problemas estructurales de la Argentina”.

“No me caso con nadie. Era en su momento que había que explicar. Si te la prestan y no pudiste evitar una catástrofe que significó económica y políticamente... Económicamente no paraste nada, y políticamente volvieron los peores. Por eso digo, un poco de autocritica. La gente la está pasando mal y nosotros no le cambiamos la vida”, concluyó.

La dimisión de Máximo Kirchner

La renuncia de Máximo Kirchner al jefe del bloque oficialista generó un tembladeral político. Lo hizo en discrepancia con “la estrategia utilizada” por el Gobierno nacional y “los resultados obtenidos” en la negociación para llegar a un acuerdo con el FMI por la deuda externa, aunque aclaró que “permanecerá dentro” de la bancada “para facilitar la tarea del Presidente y su entorno”.

“He tomado la decisión de no continuar a cargo de la presidencia del bloque de diputado del Frente de Todos. Esta decisión nace de no compartir la estrategia utilizada y mucho menos los resultados obtenidos en la negociación con el Fondo Monetario Internacional (FMI), llevada adelante exclusivamente por el gabinete económico y el grupo negociador que responde y cuenta con la absoluta confianza del Presidente de la Nación (Alberto Fernández), a quien nunca dejé de decirle mi visión para no llegar a este resultado”, dijo Kirchner en un comunicado.

Luego de que Kirchner oficializó la renuncia, el presidente Alberto Fernández aclaró que el diputado Máximo Kirchner “en ningún momento le habló de rupturas” en el oficialismo, y anunció que “mañana estaremos decidiendo quién lo reemplaza”.

“Hoy me llamó, me dijo que había tomado esta decisión. Yo le dije que no era necesario. Me dijo que Cristina (Kirchner, su madre y vicepresidenta de la Nación) no estaba de acuerdo con la renuncia”, indicó el Presidente.