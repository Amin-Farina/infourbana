+++
author = ""
date = 2021-11-01T03:00:00Z
description = ""
image = "/images/cervetto-5.jpg"
image_webp = "/images/cervetto-5.jpg"
title = "PACTO EN TIGRE: ESPERT LLENÓ DE KIRCHNERISTAS SU LISTA DE CONCEJALES"

+++

**_Familiares de funcionarios, ex funcionarios y proveedores municipales, vinculados al Frente de Todos, son los primeros tres concejales de la lista “liberal” de José Luis Espert en Tigre._**

Después de la investigación realizada por este medio sobre la inclusión del ex funcionario municipal y candidato de Sergio Massa, **Juan José Cervetto**, quien encabeza la lista de Avanza Libertad en Tigre, salieron a la luz nuevas publicaciones y archivos que vincularía a varios candidatos de Espert con el Frente de Todos tigrense.

El lunes por la tarde, circularon a través de las redes sociales diversas imágenes en las cuales se lo puede ver a Juan José Cervetto en funciones junto con el intendente de Tigre, Julio Zamora o expresando su apoyo y cercanía con el jefe de diputados de la Nación, Sergio Massa.

![](/images/cervetto-4.PNG)

![](/images/cervetto.PNG)

Fuentes del Concejo Deliberante de Tigre aseguran lo que es un secreto a voces "_Cervetto va por la concejalía como parte de un acuerdo Espert y el oficialismo local._” Según indicaron, esta estrategia tendría por fin “_restarle votos a la colación de Juntos y colocar en la lista liberal a candidatos que sean leales al kirchnerismo_".

Son muchas las especulaciones sobre los candidatos de la lista que Espert presenta en Tigre. A la ya conocida postulación de Juan José Cervetto, que fuera funcionario en el Municipio hasta días antes del cierre de lista, se suma información sobre la candidata número dos de la nómina. **María Laura Fernández** es esposa de Enrique Aliot, un estrecho colaborador de Sergio Massa en la Cámara de Diputados. En tiempos en que el Massismo estaba en la intendencia de Tigre, Aliot se desempeñaba como Subsecretario de Gobierno.

Quienes conocen el entramado del presidente de la Cámara de Diputados conocen a sus armadores de listas y candidaturas: Eduardo Cergnul actual Secretario parlamentario y Enrique Aliot. Es por eso que al llenar casilleros de las listas tigrenses pusieron a alguien a quien pueden manejar desde su propia casa.

Si bien Fernández se presenta como alguien nueva en la política, hoy la segunda candidata de Avanza Libertad es garante de aquellos intereses que intentan sumar un concejal en el HCD a través de otra fuerza política. En otras palabras, lo que el Frente de Todos no puede lograr con sus votos, lo intentan buscar en el armado de la lista de José Luis Espert en Tigre.

De este mismo modo a los primeros dos candidatos se les suma un tercero, **Javier Corradino**, quien habría sido beneficiado con concesiones y negocios del gobierno local. Una de esas concesiones es el bar del Centro Universitario Tigre. Corradino a su vez está vinculado con Ximena Guzman, Secretaria de Protección Ciudadana en el municipio y con Rodrigo Alvarez, actual Concejal del Frente de Todos en Tigre.

![](/images/javier-corradino-1.PNG)

![](/images/javier-corradino-3.PNG)

Con este esquema, todo parecería quedar en casa y la “casta” liberal en Tigre demostraría ser Kirchnerista, traicionando de esta manera a una juventud ilusionada con un cambio radical a través de los ideales liberales. Entonces, la pregunta que nos hacemos es ¿Qué diría Javier Milei al respecto?