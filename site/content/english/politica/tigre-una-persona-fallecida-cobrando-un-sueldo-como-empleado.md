+++
author = ""
date = 2021-06-03T03:00:00Z
description = ""
image = "/images/julio-zamora-428-1.jpeg"
image_webp = "/images/julio-zamora-428.jpeg"
title = "TIGRE: UNA PERSONA FALLECIDA COBRANDO UN SUELDO COMO EMPLEADO"

+++

**_El último 29 de abril el intendente de Tigre, Julio Zamora, dejó sin efecto a partir del 1 ese mes las asignaciones por función especifica entre los empleados municipales que eran alcanzados por el decreto 2080 de 2019 y que trabajan cobrando un adicional todos los meses._**

La ordenanza 573 firmada por el jefe comunal tiene como fin recortar el sueldo de 39 empleados que cumplían funciones extras por una función adicional de la que fueron contratados. La maniobra resulta claramente ilegal, en tanto y en cuanto los empleados no pueden cobrar todos los meses un plus no remunerativo.

Sin embargo, lo más llamativo es que dentro de la lista figura Daniel Febres, un empleado que falleció en noviembre de 2020 y sigue figurando en los registros del municipio que conduce el Julio Zamora.

Esta investigación se pudo saber a partir de los recibos de sueldo de Daniel Febres, quien se desempeñaba como director de la Estación Fluvial de Tigre, con fecha del recibo del sueldo el 20 de mayo 2021.