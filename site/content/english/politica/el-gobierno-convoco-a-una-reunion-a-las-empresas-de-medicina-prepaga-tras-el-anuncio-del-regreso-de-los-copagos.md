+++
author = ""
date = 2021-12-26T03:00:00Z
description = ""
image = "/images/vizzotti-hablo-de-una-mayor___6fik5zlbj_1200x630__1.jpg"
image_webp = "/images/vizzotti-hablo-de-una-mayor___6fik5zlbj_1200x630__1.jpg"
title = "EL GOBIERNO CONVOCÓ A UNA REUNIÓN A LAS EMPRESAS DE MEDICINA PREPAGA TRAS EL ANUNCIO DEL REGRESO DE LOS COPAGOS"

+++

#### **_Tras el anuncio del regreso de los copagos, el gobierno nacional convocó a una reunión a las empresas de medicina prepaga, según informó la ministra de Salud, Carla Vizzotti._**

“Hablé esta mañana con el superintendente de servicios de la Salud y le pedí que arme una reunión este mediodía para abordar el tema y evitar que el impacto de esto llegue a la gente”, explicó la funcionaria.

Según explicó, las compañías no necesitan autorización del Poder Ejecutivo para aplicar la medida anunciada el fin de semana, sin embargo trabajarán para limitar el impacto en los bolsillos de sus afiliados.

La Federación Argentina de Prestadores de Salud (FAPS) informó el domingo que a partir del 1° de enero los prestadores de salud (hospitales, clínicas sanatorios, servicios de emergencias médicas, geriátricos, psiquiátricos, centros de atención odontológica, etc) cobrarán un copago de 9% del valor de cada prestación médica a los pacientes como consecuencia de la crítica situación financiera del sistema.

Los prestadores de salud invocan el aumento de los costos operativos y la cobertura de nuevos tratamientos con ingresos regulados y atrasados respecto a la inflación, lo que genera un desfasaje económico que pone en peligro la sostenibilidad del sistema.

Marcelo Kaufman, presidente de CEDIM (Cámara de Entidades de Diagnóstico y Tratamiento Ambulatorio), explicó cómo afectará el copago en la práctica a los afiliados: “Se va a cobrar un 9% sobre todo lo que se haga el paciente; por ejemplo, por hacerse un laboratorio que vale $5.000, va a tener que pagar 450 pesos”.

Y agregó: “Esto no es una amenaza para que nos permitan aumentar, es un hecho, salvo que haya un cambio que nos cubra la financiación”.

##### Alternativas

Las prestadoras dicen que el incumplimiento de las empresas de Medicina prepaga y Obras Sociales de Dirección las obliga a buscar alternativas para evitar un colapso de sus finanzas y a su vez financiar el pago de salarios de su personal. De allí resulta, justifican, el cobro del copago del 9% del costo de la prestación que empezarán a exigir en enero a los pacientes con cobertura de las prepagas.

Las prestadoras advirtieron además que a medida que el resto de los financiadores del sistema, esto es, Obras Sociales sindicales y provinciales, no actualizan los valores de las prestaciones, también exigirán copago a sus afiliados para cubrir los costos operativos, ante una situación económica que califican de “grave”.

Entre las empresas de Medicina Prepaga y Obras Sociales de Dirección se encuentran Cemic, Femedica, Galeno Argentina, Medife, Swiss Medical Group, Medicus SA, Omint SA, Obra Social Luis Pasteur y OSDE.

Las prepagas habían advertido hace un par de semanas a los prestadores que no podrían afrontar el aumento del 9% de enero, último convenido para que los prestadores, a su vez afronten el pago de lo convenido en la paritaria salarial 2021.

Tanto el ministerio de Trabajo como la Superintendencia de Servicios de Salud están al tanto de un problema que se ha vuelto recurrente y que se agrava con la dinámica inflacionaria.

El copago del 9% que exigirán los prestadores agrupados en la FAPS se aplicará sobre los valores de las prestaciones, que también se actualizaría en enero y que depende de cada prestador. Los médicos y otros profesionales de la salud que atienden por cartilla normalmente adecuan el valor de sus prestaciones al de las empresas prestadoras, por lo que también allí habría copago y aumento de los valores de cartilla.

La paritaria de Salud 2021 que invocan y deben cumplir los prestadores de servicios médicos estableció un aumento salarial del 36%, en cuatro cuotas de 9 puntos porcentuales cada una. Los primeros tres aumentos se hicieron efectivos en agosto, septiembre y octubre. El próximo debe pagarse a partir de enero. Ante el anuncio de las empresas de Medicina Prepaga y Obras Sociales de Dirección de que no podrán cumplir con los aumentos pre acordados, los prestadores se lo cobrarán a los pacientes.