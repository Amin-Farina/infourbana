+++
author = ""
date = 2021-04-21T03:00:00Z
description = ""
image = "/images/ezequiel-galli.jpg"
image_webp = "/images/ezequiel-galli.jpg"
title = "EL INTENDENTE DE OLAVARRÍA: \"EL HOSPITAL ESTÁ SATURADO PERO NO COLAPSADO\""

+++

**_Ezequiel Galli negó que estén "eligiendo pacientes" por la falta de camas, aunque reconoció que la situación es "compleja" y detalló que los testeos que se realizan en esa ciudad arrojan una positividad del 40 por ciento._**

El intendente de Olavarría, Ezequiel Galli, aseguró que el Hospital Hospital Municipal Doctor Héctor M. Cura de su distrito no se encuentra colapsado sino "saturado" y negó que estén "eligiendo pacientes" por la falta de camas, aunque reconoció que la situación es "compleja" y que requiere monitoreo diario.

"Se está trabajando para poder lograr que cada una de las personas que ingresa, tenga su cama", dijo este martes el jefe comunal en declaraciones formuladas a radio La Red.

En ese marco, aseguró que "nunca se atendió a un paciente en el pasillo del hospital, ni se llegó a elegir entre un paciente u otro".

"No es real que en Olavarría se haga una selección de pacientes; el hospital está saturado pero no colapsado. Se está trabajando para poder lograr que cada una de las personas que ingresa, tenga su cama", afirmó el jefe comunal, al responder versiones periodísticas que señalaron la presencia de un colapso sanitario en ese distrito.

"Decir eso es de una irresponsabilidad tremenda porque genera pánico en la población. Se naturaliza que está pasando eso, y no es real es muy doloroso para todo el personal de salud", indicó.

No obstante, Galli afirmó que en Olavarría, los casos siguen "parejos", igual que la semana pasada, "con un un 40% de positividad".

En relación a las clases presenciales, el jefe comunal explicó que en el municipio "la mayoría de los colegios tienen una semana sí y otra no", y subrayó que "no hay ninguno que este el 100 por 100 de los días con presencialidad".

"En este momento me parece coherente que sigan de esta manera y, cuando pasemos la segunda ola, se podría pasar en una mayor presencialidad", concluyó.