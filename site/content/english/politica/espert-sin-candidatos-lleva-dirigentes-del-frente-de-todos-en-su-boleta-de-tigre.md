+++
author = ""
date = 2021-10-18T03:00:00Z
description = ""
image = "/images/whatsapp-image-2021-10-18-at-12-16-58-1.jpeg"
image_webp = "/images/whatsapp-image-2021-10-18-at-12-16-58-1.jpeg"
title = "ESPERT ENCABEZA SU LISTA EN TIGRE CON DIRIGENTES DEL FRENTE DE TODOS"

+++
#### **_Juan José Cervetto después de ser candidato del peronismo y ocupar el cargo de subsecretario en el Municipio de Tigre ahora podría ocupar una banca en el Concejo Deliberante_**

En el Frente de Todos esperan contar con un extraño aliado. La ingeniería electoral del Frente de Todos parece haber intercambiado el apoyo local a la lista del partido ultraliberal Libertad Avanza de José Luis Espert a cambio de colocar como candidato a concejal a un ex funcionario y candidato surgido de la filas del peronismo tigrense.

Una vez dentro del Concejo Deliberante, Juan José Cervetto y el Frente de Todos explotarían sus estrechos vínculos con los principales referentes del peronismo para garantizarles quórum propio e impedir que Juntos por el Cambio se consolide como alternativa en Tigre

La jugada oculta de kirchneristas y ultraliberales no resiste un archivo periodístico ni una simple búsqueda en internet. Juan José Cerveto acompañó a Sergio Massa como candidato a Consejero escolar del Frente Renovador como lo publicó el Cronista Comercial en su momento: [https://www.cronista.com/economiapolitica/Massa-presenta-a-sus-candidatos-con-un-acto-en-Tigre-20130708-0104.html](https://www.cronista.com/economiapolitica/Massa-presenta-a-sus-candidatos-con-un-acto-en-Tigre-20130708-0104.html "https://www.cronista.com/economiapolitica/Massa-presenta-a-sus-candidatos-con-un-acto-en-Tigre-20130708-0104.html")

La carrera política de Cervetto siguió en crecimiento hasta alcanzar la estratégica Subsecretaría de Faltas del Municipio desde dónde ahora lo habrían tentado con la posibilidad de ingresar al Concejo Deliberante por medio de un acuerdo con José Luis Espert y el apoyo a su lista para dividir y restarle votos a Juntos por el Cambio.

Como parte de su carrera de funcionario del actual gobierno municipal también existen múltiples y recientes apariciones en prensa y sitios web como las que se lo observa participando de inauguraciones y lanzamientos de talleres de reeducación vial como publicó el portal del periodista Guillermo Andino: [http://www.redderadios.com/tigre-municipio-pionero-en-reeducacion-vial-en-argentina/](http://www.redderadios.com/tigre-municipio-pionero-en-reeducacion-vial-en-argentina/ "http://www.redderadios.com/tigre-municipio-pionero-en-reeducacion-vial-en-argentina/") o remodelando y ampliando oficinas municipales como difundieron diversos medios locales [https://tigrealdia.com.ar/se-inauguro-un-anexo-de-la-subsecretaria-de-faltas-en-nuevo-delta/](https://tigrealdia.com.ar/se-inauguro-un-anexo-de-la-subsecretaria-de-faltas-en-nuevo-delta/ "https://tigrealdia.com.ar/se-inauguro-un-anexo-de-la-subsecretaria-de-faltas-en-nuevo-delta/")