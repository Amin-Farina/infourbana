+++
author = ""
date = 2021-09-17T03:00:00Z
description = ""
image = "/images/1631893375-1.jpeg"
image_webp = "/images/1631893375.jpeg"
title = "ORGANIZACIONES PIQUETERAS MARCHAN AL MINISTERIO DE DESARROLLO SOCIAL"

+++

##### **_Reclaman asistencia alimentaria para comedores y ollas populares, como así también rechazan "las bajas y descuentos" en los planes sociales._**

Distintas organizaciones piqueteras de izquierda se movilizaban desde la estación Constitución hacia el Ministerio de Desarrollo Social, donde reclamarán asistencia alimentaria para comedores y ollas populares, como así también rechazarán "las bajas y descuentos" en los planes sociales.

Así lo informaron en un comunicado de prensa conjunto los dirigentes del Polo Obrero-Tendencia, el MTR Histórico, el FTC M29, Resistencia Popular, el Movimiento 19 y 20 de Diciembre, entre otros.

La protesta generaba algunas demoras para el tránsito vehicular desde y hacia la Capital Federal.

"Este conjunto de organizaciones piqueteras salimos hoy a reclamar nuevamente al Gobierno y denunciamos que llevamos meses sin respuesta a nuestros reclamos. Los comedores y ollas populares aguardan los alimentos comprometidos. Los compañeros sufren bajas y descuentos en sus planes sociales", enumeraron en su convocatoria.

En ese sentido, criticaron que "en el contexto de crisis económica y social, con millones de nuevos desocupados, con la caída a pique de salarios e ingresos, con la inflación y aumentos en los alimentos de la canasta básica, los planes sociales están reducidos a meras ayudas que están lejos de los 29.000 pesos de una canasta de indigencia.