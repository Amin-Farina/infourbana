+++
author = ""
date = 2022-04-01T03:00:00Z
description = ""
image = "/images/foto-9-de-julio.webp"
image_webp = "/images/foto-9-de-julio.webp"
title = "TERCER DÍA DE PIQUETES EN EL CENTRO PORTEÑO: CONTINÚAN LAS PROTESTAS CON CARPAS INSTALADAS EN LA 9 DE JULIO"

+++
#### Insisten con ser recibidos por el Gobierno y se quedarán hasta hoy al mediodía. Ayer la Policía de la Ciudad desalojó un corte en la Autopista 25 de Mayo pero volvió a haber caos de tránsito en el centro porteño

Por tercer día consecutivo, continúa el campamento piquetero en la 9 de Julio. La medida de fuerza, que consistió en el pernocte de dos noches y la permanencia del bloqueo del tránsito durante 48 horas, finalizará este viernes al mediodía en reclamo de la ampliación de los programas de empleo y una mayor ayuda alimentaria para comedores.

Organizaciones piqueteras de izquierda resolvieron la protesta sobre la avenida 9 de Julio de Buenos Aires, frente al Ministerio de Desarrollo Social, como parte de un conjunto de acciones que comenzó el miércoles. La medida se extiende por la 9 de Julio desde avenida de Mayo hasta avenida San Juan, lo que impedía el paso vehicular por la zona y bloqueaba el Metrobús.

La evolución de la protesta dependerá de lo que definan las agrupaciones convocantes en un plenario convocado este viernes, en el medio del acampe, según informó uno de sus voceros, el dirigente del Polo Obrero, Eduardo “Chiquito” Belliboni. “Queremos ver si finalmente el ministro nos recibe. Hay otros ministros que nos dijeron extorsionadores y terminaron mal. Nosotros estamos haciendo un reclamo y él sabe perfectamente por qué estamos acá. Después pasan 10 días y no nos llaman. Entonces, así no funciona. El Gobierno no puede ignorar los problemas”, sostuvo hoy Belliboni en declaraciones a radio Mitre.

Ayer, la jornada tuvo momentos de máxima tensión. A la mañana, varias columnas de manifestantes perteneciente a un frente de organizaciones comandadas por el Polo Obrero Tendencia -la agrupación alineada con Jorge Altamira- se concentró en Plaza Constitución y luego cortó la Autopista 25 de Mayo por una hora, hasta que un operativo policial intervino en el lugar y los desalojó.

Los piqueteros evitaron el conflicto y retrocedieron voluntariamente. Una columna se retiró por Carlos Calvo y otra bajó hacia la Avenida 9 de Julio, mano al norte. El grupo se dirigió entonces hacia el Ministerio de Desarrollo Social, liberando la circulación por la autovía, para sumarse al acampe que estaban realizando las organizaciones nucleadas en la denominada Unidad Piquetera.

Los manifestantes insisten con ser recibidos por el ministro de Desarrollo, Juan Zabaleta, a quien le solicitan más planes sociales.

La portavoz del Gobierno, Gabriela Cerruti, habló del conflicto durante el jueves por la mañana: “El ministro recibió antes de la protesta a quienes están acampando en la 9 de Julio. Ellos (por los movimientos que realizan el acampe) tenían decidido un plan de lucha en el que iban a realizar tres acampes y este era el segundo. Decidieron llevarlo adelante igual, aunque las negociaciones estaban iniciadas. Confiamos en que las negociaciones puedan retomarse. La propuesta es la reconversión de planes en trabajo genuino”.

En tanto, desde la oficina de Zabaleta adelantaron que no habrá nuevas reuniones hasta que terminen las protestas. Pero mientras tanto, siguen los acampes.

Eduardo Belliboni, referente del Polo Obrero, apuntó contra el ministro Zabaleta por haber insinuado que se había llegado a un acuerdo el último lunes, pero “eso no es así”. El dirigente expresó, además, que una nueva reunión que atienda los problemas sociales sería una manera sencilla de levantar el acampe.

El lunes pasado, el ministro de Desarrollo Social, Juan Zabaleta, recibió a dirigentes de Unidad Piquetera, con quienes acordó que “presentarán un plan de actividades laborales para titulares del Potenciar Trabajo” y se implementará un “refuerzo para las políticas alimentarias”.

Sin embargo, los dos puntos de discrepancias consistían en el pedido de las agrupaciones para que se otorgue el alta de nuevos planes sociales y que se levantara la suspensión del cobro de los beneficios que reciben dos personas que fueron detenidas por tirar piedras al Congreso cuando en la Cámara de Diputados se debatía el proyecto de acuerdo con el FMI.

La protesta arrancó este miércoles alrededor de las 16 con los primeros cortes, pero dos horas más tarde las personas que estaban en el lugar comenzaron a armar las carpas para quedarse toda la noche, lo que dificultó aún más el tránsito en el microcentro porteño.

Los manifestantes hablaban de réplicas y acciones en todo el país. En el marco de esas acciones, más de dos mil personas integrantes de organizaciones piqueteras de Jujuy realizaron otro acampe en el centro de San Salvador de Jujuy y se movilizaron al Juzgado Contravencional en rechazo por las actas labradas por la policía y contra la criminalización de la protesta social, informaron fuentes de esas organizaciones.

En Neuquén organizaciones sociales bloquearon el centro de esa ciudad con un acampe que mantenían desde ayer a la tarde, en el marco de una jornada nacional de lucha para exigir trabajo genuino y el abastecimiento de los merenderos y comedores.

”Estamos exigiendo acá, como en todas las plazas de Argentina, la posibilidad de tener un trabajo genuino”, manifestó en diálogo con la prensa el referente del Polo Obrero (PO), Luis Ramírez, con relación a la medida de fuerza que se produjo en el monumento a San Martín, junto a la CTEP, el FOL y el Frente Popular Darío Santillán.

La convocatoria fue impulsada por representantes del Polo Obrero, MTR-Votamos Luchar, el CUBA-MTR-, Barrios de Pie/Libres del Sur, MST Teresa Vive, el Movimiento Territorial (MTL-Rebelde), el Frente de Organizaciones en Lucha (FOL) y fracciones del Frente Popular Darío Santillán (FPDS), entre otras organizaciones que también forman parte de la manifestación.