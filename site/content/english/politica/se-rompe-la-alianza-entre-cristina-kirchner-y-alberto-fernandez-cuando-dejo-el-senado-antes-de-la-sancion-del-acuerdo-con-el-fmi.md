+++
author = ""
date = 2022-03-18T03:00:00Z
description = ""
image = "/images/cristina-y-alberto.webp"
image_webp = "/images/cristina-y-alberto.webp"
title = "SE ROMPE LA ALIANZA ENTRE CRISTINA KIRCHNER Y ALBERTO FERNÁNDEZ CUANDO DEJÓ EL SENADO ANTES DE LA SANCIÓN DEL ACUERDO CON EL FMI"

+++
#### La abrupta retirada de CFK refrendó su ruptura con el Presidente y agravó la crisis política que enfrenta la coalición de Gobierno a pocas horas de lanzar la guerra contra la inflación

En un movimiento calculado al extremo para exhibir su ruptura política con Alberto Fernández, Cristina Fernández de Kirchner abandonó la Cámara Alta antes de la sanción de la ley que respalda el acuerdo con el Fondo Monetario Internacional. Una hora más tarde, CFK confirmó con un chat que su posición antagónica al FMI había sido derrotada por una inédita coalición parlamentaria que unió a senadores peronistas, de partidos provinciales y de Juntos por el Cambio. El resultado fue 56 votos a favor y 13 en contra.

El Presidente cenaba en la casa de Juan Manzur cuando se conoció el resultado. Alberto Fernández y su jefe de Gabinete están en Tucumán, y las sonrisas y aplausos respondieron a dos motivos políticos: la ley evitó el default con el FMI y su debate legislativo demostró que es posible aislar a la Vicepresidente si se negocia con la oposición.

La alegría presidencial es un hecho efímero. Todavía el board del Fondo no desembolsó los Derechos Especiales de Giro (DEG´s) que se necesitan para cancelar los 2.800 millones de dólares que vencen la semana próxima, y no hay una sola posibilidad que Juntos por el Cambio reemplace a La Cámpora y al Instituto Patria para permitir que Alberto Fernández administre su Gobierno sin mayores contratiempos.

El Frente de Todos ya no existe, el jefe de Estado tiene menos de cien diputados y apenas veinte senadores para lograr que sus iniciativas atraviesen las dos cámaras del Congreso. Ello implica que Alberto Fernández gobernará bajo el fuego cruzado de la oposición y la compleja interna palaciega que protagonizarán la Vicepresidente y Máximo Kirchner.

En este contexto, el Presidente grabará hoy un discurso para anunciar su primera ofensiva en la guerra contra la inflación. Se trata de un combate desigual, librado sin plan específico, y sostenido por una secuencia de medidas que aún no se terminaron de definir en los Ministerios de Economía, Desarrollo Productivo y Agricultura y Ganadería.

La inflación de alimentos llegó a un 40 por ciento en las primeras dos semanas de marzo, y la tendencia es alcista. A lo que se debería sumar el incremento exponencial de los combustibles que se usan para proveer energía eléctrica y calefacción en la Argentina.

Los dos comodities sufren las consecuencias de la guerra de Rusia contra Ucrania, y es poco lo que puede hacer el jefe de Estado desde Balcarce 50. Se calcula que la inflación de marzo -para alimentos- puede alcanzar los dos dígitos y que faltará gas cuando el frío empiece apretar en mayo.

En medio de la guerra contra la inflación, y con un coalición oficialista que ya está diezmada, Alberto Fernández debe decidir qué hacer con los casilleros de poder que ocupan el Instituto Patria y La Cámpora.

La Liga de gobernadores, los intendentes del conurbano que siempre desconfiaron de CFK, los legisladores que condenaron la estrategia personalista de Máximo y los ministros más fieles al oficialismo, le exigen a Alberto Fernández que fuerce un cambio de Gabinete, que desaloje las cajas más poderosas de la administración -PAMI y ANSES, por citar dos casos-, y que asuma la conducción total de Balcarce 50.

El jefe de Estado no quiere consolidar la ruptura en plena guerra contra la inflación, pero también asume que pierde poder cada vez que CFK mueve en su contra. Alberto Fernández sabe que la Vicepresidente prepara su réplica y que es mejor adelantar una jugada política a quedar expuesto y a la defensiva.

Los senadores y diputados que votaron en contra del acuerdo con el FMI ya cruzaron y aguardan las órdenes de Cristina y Máximo Kirchner. No tienen intenciones de entregar sus espacios de poder y creen que Alberto Fernández traicionó sus ideales políticos.