+++
author = ""
date = 2022-04-18T03:00:00Z
description = ""
image = "/images/pag14-corte-suprema_optjpeg-1.webp"
image_webp = "/images/pag14-corte-suprema_optjpeg.webp"
title = "TRAS LA EMBESTIDA DE CRISTINA KIRCHNER, LA CORTE SUPREMA ASUMIRÁ EL CONTROL DEL CONSEJO DE LA MAGISTRATURA"

+++
#### Este lunes entra en vigencia el fallo que ordena una recomposición en el organismo que participa en la elección y sanción a jueces. Anoche la vicepresidenta evidenció su enojo

Una serie de movimientos políticos y resoluciones judiciales sucederán en los próximos días para que se concrete el desembarco de la Corte Suprema en el Consejo de la Magistratura, luego una nueva embestida de la vicepresidenta Cristina Kirchner.

A partir de este lunes entra en vigencia el fallo mediante el cual la Corte ordenó que haya una nueva composición del Consejo de la Magistratura, sin embargo aún no asumieron los siete integrantes restantes para que el cuerpo pase de 13 a 20 miembros.

El 16 de diciembre de 2021, el máximo tribunal declaró la inconstitucionalidad de la integración de los 13 miembros del órgano encargado de la selección y acusación de jueces y de la administración del Poder Judicial. Se trata de una iniciativa del año 2006 redactada con la lógica política de Cristina Kirchner. Una ley a medida del kirchnerismo, pensada como una arquitectura para manejar el Consejo que la dinámica política convirtió en un laberinto que no sirve a nadie.

La Corte Suprema declaró la inconstitucionalidad de la norma porque no respeta el equilibro que manda la Constitución Nacional entre los representantes de la política y de los técnicos y dio un plazo de 120 días corridos para que el Congreso dicte una nueva ley que regule el organismo. Al no haberse sancionado una nueva norma, el Consejo debe volver a su anterior composición de 20 miembros y ser presidido por el titular del máximo tribunal.

Esa acordada estableció que los siete consejeros restantes debían ser electos por los jueces (1), los abogados (2), los académicos (1), la oposición parlamentaria (2) y el titular del alto tribunal. Además se señaló que si para el 15 de abril no asumía una nueva integración, todo lo que el Consejo hiciera sería nulo.

Horacio Rosatti ya está designado -como titular del máximo tribunal-; al igual que la jueza, Agustina Díaz Cordero y las abogadas Jimena de la Torre y María Fernanda Vázquez. Pero restan otros tres nuevos miembros. Hoy se confirmará a Pamela Tolosa, decana de la Facultad de Derecho de la Universidad del Sur, de Bahía Blanca, como representante de los académicos. Solo faltaría resolver la trama político-judicial, en la cual están involucrados Alberto Fernández, la vicepresidenta, Sergio Massa y el presidente de la Corte, que empantanó la designación de los representantes de la oposición en el Consejo.

Juntos por el Cambio pidió que sean la diputada radical Roxana Reyes y el senador Luis Juez. Sin embargo, un fallo del juez federal de Paraná Daniel Alonso le ordenó a la presidenta del Senado, CFK, y al titular de Diputados, Massa, que se abstengan de hacerlo hasta que se resuelva un planteo presentado por el diputado oficialista Marcelo Casaretto.

La Corte Suprema tiene prevista una reunión el próximo martes para analizar la precautelar ordenada por el juez Alonso. La intención de los magistrados es aprobar una nota aclarativa a su fallo que permita anular la precautelar tramitada en la justicia federal de Entre Ríos.

Una vez que esto suceda, el alto tribunal notificara a Cristina Kirchner en el Senado y a Sergio Massa en Diputados que ya no hay trabas formales para permitir que Reyes y Juez se incorporen al Consejo de la Magistratura. Habrá que ver si finalmente CFK y el titular de la Cámara Baja acatan la decisión de la Corte.

En el Frente de Todos puede crujir la interna, pero en este punto no hay grieta. El kirchernismo y Alberto Fernández creen que la Corte es un resorte de la oposición y consideran que debe ser reformado. En el Gobierno desconfían de Rosatti e intuyen que jugará con el bloque de Juntos por el Cambio.

Este domingo, a través de su cuenta de _Twitter_, Cristina Kirchner volvió a embestir contra el máximo tribunal. En efecto, la vicepresidenta retuiteó un posteo publicado por el periodista Matías Mowszet que dice: “Mañana el presidente de la Corte Suprema, Horacio Rosatti, va a asumir como presidente del Consejo de la Magistratura (el órgano que designa y remueve jueces) gracias a un fallo dictado por él mismo y sin dejar su puesto en la Corte, que también ganó autoeligiéndose meses atrás”.

El kirchnerismo considera que es una ofensiva de la oposición el nuevo paso de la Corte. Y, frente a la arremetida del oficialismo, el máximo tribunal amplía su poder con el manejo del Consejo de la Magistratura.

Frente al fallo de diciembre de 2021, el Gobierno envió una ley de reforma del organismo en cuestión, se le dio tramite exprés, semanas atrás obtuvo la media sanción y fue enviada a Diputados. Sin embargo, no será tratada porque el oficialismo no cuenta con los votos. En este marco, la oposición opta porque la Corte presida el Consejo.

Pese a la advertencia de CFK, Rosatti no asumirá este lunes. Aguarda, junto a sus colegas Rosenkrantz y Maqueda, que la vicepresidenta y Massa no cuestionen la próxima nota del máximo tribunal, aclarando que no hay ningún cuestionamiento contra Reyes y Juez como representantes de la oposición parlamentaria. Cuando se resuelva este conflicto, Rosatti tomará juramento a los 6 futuros consejeros. Ocurriría a fines de esta semana o antes que concluya abril.