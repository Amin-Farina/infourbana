+++
author = ""
date = 2021-08-24T03:00:00Z
description = ""
image = "/images/3a548042a797062b466b5f1aad211fa00a17a079-1.jpg"
image_webp = "/images/3a548042a797062b466b5f1aad211fa00a17a079.jpg"
title = "EN ARGENTINA HAY QUE TRABAJAR MÁS DE 100 DÍAS PARA COMPRAR UNA COMPUTADORA Y 60 DÍAS PARA COMPRAR UN CELULAR"

+++

**_El país está segundo entre las naciones donde más cuesta acceder a bienes electrónicos, por encima de Uruguay y Chile donde se necesita la mitad de tiempo, según un estudio privado._**

Casi un tercio del año. Ese es el tiempo que hay trabajar para comprarse una notebook en la Argentina. Son 105 días, mientras que en países vecinos como Uruguay y Chile el número se reduce a la mitad.

Las devaluaciones y la sucesiva pérdida de poder adquisitivo de los sueldos llevaron a que el país quede segundo entre las naciones donde más hay que trabajar para comprar bienes electrónicos, según un estudio privado. El podio está encabezado por Venezuela, donde para adquirir una de estas computadores hacen falta 531 días.

El cálculo, realizado por la consultora Focus Market, tomó los salarios en dólares de 11 países de América Latina y el valor promedio de una notebook de 15,6 pulgadas y 8GB, de un celular iPhone de 64 gigas de memoria y de un smart TV de 43 pulgadas. Utilizó una cotización del tipo de cambio de $180, el valor del dólar libre al momento de realizarse el informe.

En la Argentina, se tomó como salario medio uno de US$264 o $47.520, que es el que se informa en Statista. Es un monto un tanto alejado del salario formal promedio que informa el Ministerio de Trabajo, que hoy ronda los $70.000 si se le descuentan los impuestos, y es alrededor del doble del salario mínimo vital y móvil.

Una notebook de ese tipo en la Argentina cuesta, según la consultora, US$1261, un valor en línea con lo que cuestan en otros países. Un celular, US$730, hasta US$180 más caro que en otras naciones (por los aranceles extraordinarios que tiene que pagar quien quiera importar un iPhone, un teléfono donde no intervienen “manos locales”). El smart TV se relevó a US$380, un precio más bajo que en otros países de la región.

Entonces, para comprar una notebook de ese tipo hacen falta 105 días de trabajo contra 50 en Chile, 51 en Uruguay y 531 en Venezuela. Para adquirir un iPhone, 60 días contra 20 en Chile, 24 en Uruguay y 220 en Venezuela. En el caso de la tele, la Argentina se acerca al promedio regional y son 31 las jornadas laborales que hacen falta para llevarse uno, las mismas que en Colombia, pero dos más que en Brasil, 16 más que en Chile y 12 más que en Uruguay.

¿Es una cuestión de precios o de salarios? Excepto por el caso del iPhone, donde entran en juego los aranceles de importación, los valores de venta son más o menos similares en todos los países de la región. Sin embargo, en la Argentina los sueldos pierden contra los precios hace 40 meses, pero además quedaron bajos en dólares por las sucesivas devaluaciones desde la crisis cambiaria de 2018.

La Asociación de Fábricas Argentinas Terminales de Electrónica (Afarte) también verifica que los valores de la compra de electrónica están similares al resto de la región. “En el monitoreo de precios mensual que hacemos vemos un escenario de convergencia entre los valores registrados en el mercado argentino y en países de la región, sea que tengan industria o importen los productos”, detalla Federico Hellemeyer, presidente de la institución.

Afarte realizó un acuerdo con el Ministerio de Desarrollo Productivo para congelar los valores de televisores, celulares, aires acondicionados y microondas de industria nacional desde el 1 de abril hasta fines de octubre, con revisiones mensuales desde junio. Recientemente, además, se renovó el plan “Ahora” con 30 cuotas para electrodomésticos de línea blanca, aunque con un gran faltante: los celulares, que fueron retirados del programa en octubre de 2020.

“En materia de precios, desde el sector mantenemos una agenda de competitividad orientada al consumidor y para celulares le pedimos al gobierno su reincorporación a los planes ‘Ahora’ a efectos de alivianar el costo financiero de la compra en cuotas y que esto se traslade a un menor precio de venta al público”, señala Hellemeyer.

Por otro lado, y si bien hubo un cambio de impuestos internos desde enero de 2021 que hizo que lo fabricado en Tierra del Fuego pasara de pagar 0% de alícuota a un 7%, aclara que ese incremento fue absorbido por las empresas y no hubo variaciones en precio. También hubo un aumento del 8% al 17% en el caso de los productos importados, lo que puede haber encarecido aquellos bienes que no se producen en la Argentina, como las notebooks.

Carlos Scimone, gerente de la Cámara Argentina de Multimedia, Ofimática, Comunicaciones y Afines, apunta que hubo algunos motivos que llevaron a subas en las computadoras: restricciones a la importación, expectativas de devaluación, problemas en la provisión de insumos desde China y en la oferta a nivel general. Como hubo menos oferta, entre la que había subían los márgenes, dice el ejecutivo, y sobre eso se suman los costos y comisiones de la venta online a la que se migró con fuerza durante la pandemia.

“Llegamos hasta acá con valores que están fuera de competencia con respecto a los países vecinos y ni que hablar de los del norte, pero además la tecnología que se está ofreciendo desde 2018 es vieja, por lo que se justifican menos los productos que se ofrecen en el mercado”, señala. “Hay que aumentar la oferta: abrir el mercado y dar mayor posibilidad de fabricación a los que quieran fabricar para que haya mejor competencia, hoy tenemos la mitad de la oferta de la que deberíamos tener”, concluye.