+++
author = ""
date = 2021-05-06T03:00:00Z
description = ""
image = "/images/renata-ghilotti.jpg"
image_webp = "/images/renata-ghilotti.jpg"
title = "UNA CONCEJAL DE JXC EN ROSARIO BUSCA QUE SEA OBLIGATORIO QUE AQUELLOS QUE COBRAN PLANES SOCIALES TENGAN QUE CAPACITARSE Y REALIZAR TRABAJOS"

+++

Renata Ghilotti afirmó que la iniciativa apunta a “volver a poner sobre la mesa la cultura del trabajo y a ir generando un espacio para después ir por la recuperación del empleo formal”.

La concejala de Juntos por el Cambio, Renata Ghilotti, presentó un proyecto para que la Municipalidad de Rosario capacite a los beneficiarios de los planes sociales para poder exigirles “alguna contraprestación”, como trabajar en el cuidado del espacio público, el reacondicionamiento de colegios y, en este contexto de crisis sanitaria a raíz del coronavirus, en la logística de los vacunatorios.

“Está ingresado en el Consejo, tiene trámite parlamentario y a partir de la semana que viene va a empezar el debate en las comisiones”, comenzó explicando la legisladora en diálogo con Infobae.

La iniciativa consiste en la creación del “Programa Municipal de Recuperación de la Cultura del Trabajo”, a partir de una ordenanza. “Lo que propone es que el municipio de Rosario firme un convenio con el Gobierno nacional y el provincial, que son los dos niveles del Estado que tienen planes sociales vigentes en la ciudad, y que se obligue a capacitar a personas que estén cobrando planes para poder hacer trabajos en distintas áreas como protección del espacio público, reacondicionamiento de plazas y colegios, limpieza y, en cuanto a la cuestión sanitaria, en la logística de los vacunatorios”, resumió la idea Ghilotti, quien además planteó que resultaría una ayuda para el “contexto financiero complicado” que atraviesa el municipio, el cual se nota “abandonado en sus espacios públicos”.

“Nos parece interesante por el espíritu de volver a poner sobre la mesa la cultura del trabajo y de poder ir planteando la transformación de los planes sociales, de generar un espacio de trabajo para después ir camino a la recuperación del empleo formal, que es lo que hay que hacer a mediano y a largo plazo en el país”, agregó.

A su vez, Ghilotti hizo hincapié en la necesidad de que el municipio sea el encargado de controlar que los trabajos se lleven a cabo, para poner en evidencia “el desmanejo que tienen las organizaciones sociales con los planes”: “Hay personas que lamentablemente se encuentran en una situación de vulnerabilidad y son rehenes de estas organizaciones. Hoy no se ve mucho porque son cómplices del Gobierno nacional, pero en el momento en que gobernaba Mauricio Macri a nosotros nos decían que no querían ser más rehenes de diferentes agrupaciones que las obligaban a ir a los piquetes para no cortarles el plan”.

“En Rosario hay un problema gravísimo por la cantidad de gente que no quiere un empleo en blanco porque tiene un plan social, como niñeras, personal doméstico e incluso personas que trabajan en pymes, que cobran planes y que si se los registra los pierden. Todo eso está en negro”, añadió la legisladora, que también remarcó que la contraprestación debe ser proporcional al dinero que cada beneficiario cobra.

“A partir de los 90 se empezó a instalar fuertemente la implementación de los planes sociales y año tras año creció eso sin solucionar ningún problema de fondo. Por el contrario, generaciones tras generaciones viven de planes sociales y nunca se termina de generar empleo genuino”, consideró.

En cuanto a lo que espera sobre el próximo debate en el recinto, la concejala manifestó: “Acá el Concejo está dividido entre diferentes frentes del peronismo y el Frente de Todos, que terminan uniéndose; el Frente Progresista, donde está el socialismo unido con otras fuerzas locales; y algunas bancas de un frente de izquierda que se llama Ciudad Futura; con lo cual está bastante relacionado con fuerzas bastante progresistas y de izquierda. Entonces los posicionamientos que vienen de nuestro espacio terminan siendo reactivos para estas fuerzas más populistas que entienden que la salida es el plan social y que nosotros estigmatizamos la pobreza, cuando en realidad estamos haciendo todo lo contrario”.

En ese sentido, Ghilotti recordó que el presidente Alberto Fernández y el ministro de Desarrollo Social, Daniel Arroyo, declararon en un par de oportunidades sobre la ineficacia de los planes sociales como solución de fondo y que incluyeron como antecedentes en el proyecto, para destacar que “si bien no ha dado ejemplos claros de ir camino hacia eso, hay un espíritu”.

#### El antecedente en Mendoza

El intendente de la ciudad mendocina de Malargüe, Juan Manuel Ojeda, dispuso que a partir del 8 de abril pasado todas las personas que tuvieran un plan social en ese distrito comenzarían a trabajar en la limpieza de espacios verdes, veredas, avenidas y ciclovías.

”Cuando hablamos de un cambio real, estas son las cosas que tenemos que lograr”, dijo el jefe comunal. ”A partir de hoy toda persona que reciba una ayuda social del municipio, deberá devolverla a la comunidad de Malargüe con horas de trabajo en contraprestación a lo recibido”, afirmó Ojeda en ese entonces. ”Solicitamos a los vecinos la colaboración y la ayuda para estas personas que están trabajando con chalecos refractarios, escobas y herramientas para el mantenimiento de la vía pública. Hay que cuidar la limpieza y mejorar la ciudad”, concluyó el intendente de Malargüe, donde gobierna el oficialismo local Cambia Mendoza.