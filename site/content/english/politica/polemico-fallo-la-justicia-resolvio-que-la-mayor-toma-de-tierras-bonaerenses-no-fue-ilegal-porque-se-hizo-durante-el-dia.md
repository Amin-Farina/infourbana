+++
author = ""
date = 2022-04-09T03:00:00Z
description = ""
image = "/images/wby4cwxr25hstkhplvhhisk2eu.webp"
image_webp = "/images/wby4cwxr25hstkhplvhhisk2eu.webp"
title = "POLÉMICO FALLO: LA JUSTICIA RESOLVIO QUE LA MAYOR TOMA DE TIERRAS BONAERENSES NO FUE ILEGAL PORQUE SE HIZO DURANTE EL DÍA"

+++
#### La decisión fue de Alejo Ramos Padilla. Se trata de la usurpación en Los Hornos. La municipalidad de La Plata había solicitado el desalojo del predio de 160 hectáreas. Los argumentos del juez y las críticas de la intendencia

El juez federal de La Plata Alejo Ramos Padilla resolvió, en un dictamen de 58 fojas, que la mega usurpación de Los Hornos, ubicada en el ex predio nacional del Club Planeadores, no fue un delito y archivó el expediente que tramitaba desde el 16 de febrero de 2020. El dictamen, además de argumentos jurídicos, está cargado de concepciones ideológicas, políticas y sociales y abre la puerta a más tomas de predios. Se trata de la mayor ocupación de tierras en la provincia de Buenos Aires. El magistrado instó a la gobernación a “concretar el proyecto de urbanización” y “soluciones adecuadas a la ‘magnitud’ de a problemática habitacional”.

La mega toma de Los Hornos se trasformó en un campo de batalla de 160 hectáreas que enfrentaba a punteros políticos, desocupados, familias sin vivienda y vendedores de parcelas usurpadas por Internet. La diáspora llevó a que el gobierno bonaerense anunciara un proyecto de urbanización y a que la municipalidad de La Plata solicitara el desalojo.

##### Los fundamentos del juez 

En su extensa resolución, el magistrado recordó que la causa se había iniciado hace dos años y que la fiscalía federal actuante no había efectuado el requerimiento de investigación ya que esa función le corresponde al Ministerio Público fiscal.

En segunda instancia precisó que previo a que el juzgado federal asumiera la competencia de la causa, el Juzgado de Garantías N°6, a cargo de Agustín Crispo, rechazó el requerimiento de la fiscalía de que se desocupare el predio usurpado, con el argumento de que, antes de ello, podían ejercerse acciones componedores y resolverse por otros medios. Sí tuvo en cuenta el pedido de resguardar los derechos de los usurpadores, realizado por el Defensor del Pueblo de la Provincia de Buenos Aires, Guido Lorenzino.

El juez resaltó que de acuerdo con lo informado por la Agencia Nacional de Bienes del Estado (AABE), después de que la gendarmería Nacional se comprometiera a la custodia del predio con 74 efectivos, siguieron ingresando cientos de personas y que continuaron con la demarcación de lotes. De las 40 familias iniciales que intrusaron el predio de unas 160 hectáreas, pasaron a casi 1.000 familias.

El magistrado le dio relevancia a la propuesta del gobierno de Axel Kicillof, quien acompañado por el de Alberto Fernández presentó un proyecto de urbanización realizado por la Subsecretaría de Desarrollo y Hábitat, tendientes a otorgar a los intrusos la tenencia precaria de las parcelas apropiadas.

Precisó además que “las familias que ocupan las parcelas en conflicto no cuentan con posibilidades económicas y políticas públicas de acceso al suelo” y que “dicho cuadro de situación sumado al carácter ocioso en que se encuentra el predio del Estado Nacional, los lleva -a los usurpadores- a tomar la decisión de apropiarse de un lugar para vivir y desarrollar su vida cotidiana”. Por esa razón, siguió argumentando Ramos Padilla, el ministerio de Desarrollo de la Comunidad, a al frente de Andrés “Cuervo” Larroque, había diseñado un proyecto urbanístico para cubrir las necesidades de esos intrusos.

Ahora bien, cuando el juez se adentra en el análisis de fondo del delito que investigaba, llegó a la conclusión de que “no caben duda de que se ha producido el despojo del inmueble de titularidad del Estado Nacional”, pero agrega que, pese a ello, los hechos “no reunían ninguna de las características legales que la figura legal requiere para que sea considerada delito”.

Para esa afirmación, el letrado, que estuvo a cargo del supuesto espionaje ilegal realizado durante el gobierno de Mauricio Macri, analiza la figura legal de usurpación prevista en el artículo 181 Inciso 1 del C.P y resalta que para que la ocupación sea delito “la figura exige que el despojo se haya llevado a cabo bajo alguna de las modalidades que prevé la norma, entre ellas, violencia, amenazas, engaños, abusos de confianza o clandestinidad”.

Según su interpretación, nada de esto ocurrió. La usurpación fue pacífica, no amenazaron a nadie y como no eran empleados del Estado no se abusó de la confianza de ningún funcionario.

Ramos Padilla también entendió que quienes tomaron las 163 hectáreas del Estado Nacional -después cedidas a la provincia de Buenos Aires- no lo hicieron de manera clandestina, es decir ocultando sus actos, sino que fue a plena luz del día. De hecho, aclara fue a las 16:45.

Entre los argumentos para archivar la causa también tuvo en cuenta que el día de la toma en el predio “había personal de la Agencia Nacional de Bienes del Estado y un casero del inmueble”. Por esa razón, interpretó que “los intrusos no realizaron actos ocultos ni subrepticios para consumar la ocupación”.

En ese sentido, el juez no interpreta, como otros de sus pares, que “la intrusión del predio se realizó de manera frenética y organizada para que quienes pudieran oponerse al ingreso se vieran impotentes de hacerlo”.

“Es preocupante que tras dos años de espera la Justicia resuelva que la mayor usurpación de la provincia de Buenos Aires no es un delito penal porque la gente ingresó de día, cuando en verdad esto muestra claramente la planificación de esa toma inducida y promovida con claros fines políticos y más aún, los argumentos utilizados eran circunstancias que el juzgado ya conocía desde el primer momento en el que entendió en el expediente”, interpretó ante Infobae María Botta, la secretaria de Planeamiento de la alcaldía de La Plata y quien estaba a cargo del seguimiento de la mega toma.

A Botta también le llama la atención que “el juez Ramos Padilla tenga tan claro que las familias no tienen recursos cuando desfilaban los vehículos y camionetas de alta gama, y dónde los materiales entran y salen todos los días”.

En la entrevista con este medio, la Funcionaria de Garro advirtió: “Desde el municipio queremos hacer responsables al gobierno provincial y a la justicia de los hechos de inseguridad que los punteros políticos que manejan la toma, cometan en las zonas aledañas. Cómo así también serán responsables de permitir las construcciones sobre las áreas inundables y los perjuicios que esto pudieran ocasionar, no solo a las familias allí instaladas sino a todo el sector lindante”.