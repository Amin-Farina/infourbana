+++
author = ""
date = 2022-01-13T03:00:00Z
description = ""
image = "/images/algunos-usuarios-en-las-redes-sociales-reportaron-que-los-cortes-de-luz-empezaron-el-domingo-905713-1.jpg"
image_webp = "/images/algunos-usuarios-en-las-redes-sociales-reportaron-que-los-cortes-de-luz-empezaron-el-domingo-905713.jpg"
title = "PARA REDUCIR EL CONSUMO ELÉCTRICO, EL GOBIERNO ORDENÓ QUE LOS EMPLEADOS PÚBLICOS NO TRABAJEN EN SUS OFICINAS"

+++

##### **_En Casa Rosada quieren evitar una situación como la del último martes en la que alrededor de 700 mil usuarios estuvieron sin luz. La medida, oficializada por decreto, regirá este jueves y viernes, jornadas en las que están pronosticadas temperaturas entre los 37 y 40 grados centígrados_**

Luego del fatídico martes en el que en medio de la de calor alrededor de 700 mil usuarios estuvieron durante varias horas sin luz, frente a las sofocantes temperaturas que se esperan para este jueves y viernes, para reducir el consumo eléctrico el Gobierno Nacional ordenó que los empleados públicos no asistan a las oficinas y desempeñen sus tareas a distancia. La medida regirá hoy y mañana a partir del mediodía.

“Trabajo a distancia por ahorro de energía”, reza el Decreto 16/2022 publicado esta madrugada en el Boletín Oficial. El Poder Ejecutivo argumentó que “la reactivación económica sostenida” en diversos sectores durante el transcurso del año 2021 “ha redundado en un incremento considerable de la demanda energética”, y sumado a que “la temporada estival en la República Argentina ha reportado altas temperaturas debido al incremento de las mismas a nivel regional, por directa incidencia del fenómeno de La Niña, el cual afecta a gran parte del hemisferio Sur de América”, se ha producido la situación vivida esta semana en distintos puntos del país.

> “Debido a que los mencionados factores actúan en forma conjunta y concurrente, se ha producido en los principales centros urbanos del país tales como la Ciudad Autónoma de Buenos Aires y los de las provincias de Buenos Aires, Córdoba y Santa Fe, entre otros, una ola de calor extremo registro de temperaturas superiores a los 40° grados centígrados”, agregó el Gobierno.

Además, el Servicio Meteorológico Nacional (SMN) pronosticó máximas por encima de los 37° que pueden escalar a 40° para este jueves y viernes.

En este contexto, el Poder Ejecutivo Nacional “se ve en la obligación de contribuir a mitigar las condiciones en que, temporalmente, se puedan ver afectados los y las habitantes del país, adoptando medidas urgentes, de rápida implementación y breve duración, para lo cual se ha decidido reducir la presencialidad en las actividades de la Administración Pública Nacional a fin de evitar el consumo de energía eléctrica en todos los inmuebles afectados a su normal funcionamiento”.

De esta forma, por decreto se dispuso que los días 13 y 14 de enero de 2022, a partir de las 12 del mediodía, “horario a partir del cual se verifican los mayores picos de consumo energético, las y los agentes de todas las jurisdicciones, organismos y entidades del Sector Público Nacional a los que se refiere el artículo 8° de la Ley N° 24.156, realicen la prestación de servicios mediante la modalidad de prestación de trabajo a distancia, en caso de ser ello posible, absteniéndose de permanecer o concurrir a sus lugares de trabajo habituales”.

> “Tal medida se adopta en el convencimiento que ello se sumará al esfuerzo de diversos sectores del país, que reducirán voluntariamente el consumo de energía eléctrica para así coadyudar a que la población pueda mantener los servicios de agua y energía eléctrica”, concluyó el Gobierno.

El decreto fue firmado por el presidente, Alberto Fernández, y el jefe de Gabinete de ministros, Juan Manzur, quienes ayer analizaron el informe del Sistema Nacional de Gestión de Riesgo sobre “la situación en las diferentes regiones frente a la ola de altas temperaturas que atraviesa el país y los recaudos que estamos tomando para la normal provisión de agua y electricidad”. En Casa Rosada no quieren que se repitan las imágenes del último martes en la que aproximadamente 700 mil usuarios del Área Metropolitana de Buenos Aires (AMBA) estuvieron durante varias horas sin luz. El corte masivo provocó críticas en redes sociales y cruces con la oposición por la administración de la energía eléctrica.

Los organismos estatales deberán implementar las medidas correspondientes para mantener guardias necesarias para preservar la continuidad de los servicios esenciales. “A dicho fin, la o el titular de cada jurisdicción, organismo o entidad determinará los equipos de trabajadores y trabajadoras esenciales que deberán prestar funciones en forma presencial, en tanto se trate de áreas críticas de prestación de servicios indispensables para la comunidad”, se explicó.

No obstante, quedan excluidos del trabajo remoto los trabajadores del Instituto de Salud Carlos Malbrán; las fuerzas de seguridad federales; las Fuerzas Armadas; personal del Servicio Penitenciario Federal; personal de la salud y del sistema sanitario; guarda parques nacionales y personal del Sistema Federal de Manejo del Fuego; de Migraciones; instituciones bancarias y entidades financieras.

Tras el leve descenso de ayer, para hoy se espera el regreso de la ola de calor que se extendería hasta el domingo. El SMN informó que se registrarán en Capital Federal y el conurbano mínimas que rondarán entre los 19 y 21 grados, con máximas que podrían alcanzar los 38°. Un anticipo de lo que será la agobiante jornada de mañana cuando la mínima parta de los 25° y se alcancen los 40°.

El alivio comenzará a sentirse luego del fin de semana en el que están pronosticadas lluvias y tormentas aisladas.