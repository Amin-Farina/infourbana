+++
author = ""
date = 2021-06-01T03:00:00Z
description = ""
draft = true
image = "/images/sc-nm.jpg"
image_webp = "/images/sc-nm.jpg"
title = "CERNADAS Y MASSOT SALEN A LA CANCHA EN LA INTERNA DE JXC DE TIGRE"

+++

**_El presidente del Concejo Deliberante Segundo Cernadas y el exdiputado nacional Nicolás Massot, son dos de los importantes dirigentes de Juntos por el Cambio que tendrán aspiraciones electorales en Tigre. La coalición opositora aún no define su ingeniería electoral._**

Con mucho por discutir, en Juntos por el Cambio se avizora una interna multitudinaria para la provincia de Buenos Aires. Entre Mauricio Macri y Patricia Bullrich, Horacio Rodríguez Larreta y María Eugenia Vidal, el intento de la UCR de presentar una lista propia, las intenciones de Emilio Monzó y de quienes acompañan a Gustavo Posse, más lo que pueda hacer Carrió o el ala de los liberales que intentan sumarse a través de José Luis Espert, la coalición opositora muestra una pluralidad de líneas que, por ahora, no se ponen de acuerdo.

Ese berenjenal de espacios tendrá correlato en los territorios de la provincia. Aún no se definió si la interna mezclará listas con un piso del 10% o del 35%, si habrá dos listas o cinco, pero todo es posible.

Con toda la ingeniería electoral sin definición, en Tigre ya salieron a la cancha dos actores de renombre. Aún no lo confirmó, pero el presidente del Concejo Deliberante Segundo Cernadas seguramente será candidato. Debe renovar su banca, pero su verdadera aspiración es gobernar el distrito en el 2023.

Hace poco se sacó una foto con Rodríguez Larreta y con Vidal, en un encuentro con dirigentes de La Territorial. Pero no quiere definirse dentro de ninguna línea. Tiene estrecho vínculo con Patricia Bullrich y asiduas charlas con el intendente de Vicente López Jorge Macri. Hace poco, además, tuvo un encuentro con Emilio Monzó.

Cernadas representa la institucionalidad del PRO en el distrito. También es acompañado por dirigentes radicales que respaldaron a Maxi Abad en la última interna de la UCR.

Hace pocos días apareció en la cartelería de las principales arterias del distrito la cara y el nombre de Nicolas Massot. En la gráfica no aparece ninguna mención a Juntos por el Cambio. El exdiputado nacional trabaja junto a Emilio Monzó. Representa a una de las líneas que podría convocar a otros peronistas y también a los radicales que acompañan a Gustavo Posse en toda la provincia.

En los afiches que aparecieron los últimos días no se aclara si busca una candidatura para estas legislativas o para 2023. El slogan elegido es “Tigre Avanza”.

En 40 días se inscriben candidaturas y todo comenzará a resolverse en las próximas semanas, pero Cernadas y Massot ya están en la cancha.