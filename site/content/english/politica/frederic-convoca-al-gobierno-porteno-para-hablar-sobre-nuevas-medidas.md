+++
author = ""
date = 2021-04-28T03:00:00Z
description = ""
image = "/images/77ftayz2pbgh7cv42x63tmx2yq.jpg"
image_webp = "/images/77ftayz2pbgh7cv42x63tmx2yq.jpg"
title = "FREDERIC CONVOCA AL GOBIERNO PORTEÑO PARA HABLAR SOBRE NUEVAS MEDIDAS  "

+++

**_Se evaluarán acciones en transporte, nocturnidad y reuniones en el espacio público. La ministra de Seguridad coordinará medidas con el secretario de Seguridad, Marcelo D’Alessandro y el secretario de Transporte porteño, Juan José Méndez._**

La ministra de Seguridad de la Nación, Sabina Frederic, convocó hoy a los secretario de Seguridad porteño, Martín D’Alessandro y de Transporte y Obras Públicas, Juan José Méndez, para redefinir y reforzar los operativos de control de circulación de personas en la Ciudad de Buenos Aires ante el recrudecimiento de la segunda ola de contagios de coronavirus. La mirada está puesta en el espacio público y el transporte en el Área Metropolitana de Buenos Aires (AMBA), donde la demanda de los usuarios de pasajeros se mantiene en el orden del 45 por ciento.

El encuentro, pautado para las 10, tiene como objetivo revisar lo actuado en las últimas dos semanas, desde el último decreto de necesidad y urgencia (DNU) que tensó la relación entre Nación y la Ciudad. Tanto las autoridades porteñas como de la cartera de Frederic mantienen una estricta reserva sobre los puntos del temario.

En la previa, los funcionarios de Alberto Fernández y Horacio Rodríguez Larreta fueron consolidando un piso de común de entendimiento, después del ruido político y judicial generado en torno a la suspensión de las clases presenciales. En la Ciudad de Buenos Aires, ya no se esconde la decisión de intensificar las medidas de restricción, ante la evidente saturación del sistema de salud y la creciente ocupación de camas de terapia intensiva (UTI) en la región metropolitana. En ese punto de vista hay coincidencias entre las tres jurisdicciones, incluida la provincia de Buenos Aires.

La presencia del jefe del área de transporte porteña, Juan José Méndez, apunta a que uno de los ejes del intercambio será garantizar el cumplimiento de las medidas sanitarias en la movilidad urbana. Como contraparte, estará presente el secretario de Articulación Federal, Gabriel Fuks, ante la vacancia del ministerio de Transporte tras el trágico accidente de tránsito que sufrió Mario Meoni el viernes pasado. Completará el cuadro del encuentro el secretario de Seguridad y Política Criminal, Eduardo Villalba.

Gabriel Battistella, subsecretario de Atención Primaria de la Ciudad de Buenos Aires y estrecho colaborador del ministro de Salud porteño Fernán Quirós, anticipó ante el canal IP Noticias que está bajo análisis del Gobierno porteño la “profundización de las restricciones nocturnas”, así como observar “cuestiones el transporte y las reuniones en lugares públicos”. La circulación nocturna ya está prohibida entre las 22 y las 6, con una evaluación relativamente positiva en términos de cumplimiento en Nación y Ciudad. En la administración porteña resaltan que en las últimas dos semanas disminuyó un 24% el movimiento de la población, a niveles de octubre pasado.

Una de las encrucijadas apunta al control del espacio público en toda la región metropolitana. En los circuitos gastronómicos, comerciales y de ferias de la Ciudad y la provincia de Buenos Aires contrastan con la tensión sanitaria. En esa línea, la resolución de bajar la circulación en las calles empezó a dar pasos hacia una mayor restricción. Una de ellas fue la publicación, en el Boletín Oficial porteño, de la suspensión de todas las actividades deportivas al aire libre que se realicen entre más de 10 personas, una restricción que rige desde ayer e impacta en la actividad de los gimnasios, estudios de baile y danzas.

Desde el pasado 8 de abril, el ministerio de Seguridad de la Nación modificó el despliegue de las cuatro fuerzas federales para hacer cumplir las medidas sanitarias ante la emergencia sanitaria del COVID-19. Los controles estuvieron ubicados principalmente en los diez puntos de acceso a la Capital Federal, en zonas específicas en el conurbano bonaerense e inspecciones en trenes y otros medios de transportes públicos que circulan por el área metropolitana.