+++
author = ""
date = 2021-09-23T03:00:00Z
description = ""
image = "/images/zy75d2s6pvddhkv744kp6zx2p4.jpg"
image_webp = "/images/zy75d2s6pvddhkv744kp6zx2p4.jpg"
title = "DENUNCIAN QUE UN MUNICIPIO DE BUENOS AIRES REGALÓ ELECTRODOMÉSTICOS DESPUÉS DE LAS PASO"

+++

#### **Luego de la derrota oficialista en las PASO, la oposición denunció que el Gobierno se dispuso ir a la conquista de los votos perdidos, incluso de aquellos que no fueron a las urnas. El municipio de General Rodríguez realizó la entrega de electrodomésticos para instituciones comunitarias, lo que despertó el rechazo de la oposición, que aseguró se trata de “compra de votos”.**

Así, el Pro denunció hoy que el municipio bonaerense de General Rodríguez, conducido por el intendente Mauro García, regaló cocinas, garrafas y heladeras, una semana después de las elecciones legislativas PASO. “¿Por qué? Porque hay dos formas de hacer política: comprando votos y subestimando a la gente, o escuchando, trabajando, y construyendo un país con futuro”, advirtieron, en una publicación difundidas en las redes.

La denuncia llegó por los trascendidos de que, cinco días después de los comicios, el municipio de la Provincia dispuso la entrega de electrodomésticos (heladeras y cocinas) en un acto público.

“Lo que todos pudimos ver en este video es clientelismo político, es lo que está haciendo el kirchnerismo luego de la derrota del último domingo”, dijo el diputado provincial del Pro, Alex Campbell, luego de presentar imágenes de la entrega de productos.

Campbell denunció públicamente que el oficialismo “sale a comprar votos con la plata de nuestros impuestos”. “Digámosle basta al clientelismo político en la provincia de Buenos Aires”, disparó.

Ante la consulta de LA NACION, fuentes municipales de General Rodríguez negaron que este fuera un acto clientelar y resaltaron que la información fue pública. “Claramente esto tiene que ver con una operación política, donde a partir de algo que no tiene otro fin que ayudar a instituciones de diversos componentes políticos, se usó para buscar dañar”, cuestionaron las fuentes municipales.

En el Ministerio de Desarrollo Social también relativizaron las críticas y dijeron, ante la consulta de este diario, que se trata de un programa que tiene convenios vigentes con distintos municipios, y que las entregas fueron coordinadas con la municipalidad de General Rodríguez a instituciones comunitarias.

General Rodríguez es uno de los municipios en los que el Frente de Todos sufrió uno de los vuelcos más dramáticos en relación a los votos que había cosechado en las PASO de 2019.

### EL ACTO

“Hoy estuvimos en General Rodríguez, junto al intendente Mauro García, participando de la entrega del equipamiento que compró el Municipio en virtud del convenio firmado dentro del marco del Programa Abrazar Argentina”, escribió Alicia Soraire, secretaria de Abordaje Integral Ministerio de Desarrollo Social de la Nación, en un posteo sobre el evento.

Asimismo, la entrega de electrodomésticos fue encabezada por la segunda candidata a concejal del distrito bonaerense y secretaria de Desarrollo Comunitario, Silvia Figueiras.

“Hoy otorgamos 65 kits de electrodomésticos para instituciones comunitarias del distrito, en el marco del programa Abrazar Argentina que impulsa la Secretaría de Abordaje Integral del Ministerio de Desarrollo Social de la Nación”, se detalló en una publicación del Municipio sobre el acto, y se continuó: “Las organizaciones beneficiarias pudieron recibir heladeras, cocinas, estufas y garrafas que permitirán fortalecer la asistencia que brindan hacia los vecinos y vecinas de General Rodríguez”.

Además, se entregaron 12 tarjetas de débito destinadas a la compra de alimentos frescos por parte de comedores y merenderos.