+++
author = ""
date = 2021-01-19T03:00:00Z
description = ""
image = "/images/aviones-de-combate-jf-17.jpg"
image_webp = "/images/aviones-de-combate-jf-17.jpg"
title = "Agustín Rossi habló de requipar a las Fuerzas Armadas con armamento chino"

+++

**_El ministro de Defensa mantuvo un encuentro virtual este lunes por la tarde con Zou Xiaoli, representante en Buenos Aires del régimen de Xi Jinping._**

El ministro de Defensa, Agustín Rossi, mantuvo esta tarde durante 45 minutos un encuentro virtual con el embajador de China en la Argentina, Zou Xiaoli, para avanzar en los vínculos bilaterales. El funcionario argentino y el diplomático abordaron en ese encuentro varios aspectos relacionados con el tratamiento de la pandemia de coronavirus y hablaron sobre la colaboración en ciberdefensa y sobre el interés para la compra de aviones de combate JF-17 y de vehículos blindados a rueda 8x8 al país asiático. También abordaron la evaluación de posibles intercambios y experiencias en misiones de paz, el desarrollo de la infraestructura del muelle y de servicios logísticos para la base naval de Ushuaia y la ampliación del Polo Químico Río Tercero de Fabricaciones Militares

“En el encuentro estuvimos repasando distintos puntos de la amplia agenda que tenemos en común, que incluyó el interés por fomentar el intercambio de oficiales de ambos países para cursos de formación y la colaboración que existe en el marco de la industria para la defensa”, expresó Rossi al término de la reunión por videoconferencia con el representante en Buenos Aires del gobierno de Xi Jinping.

El titular de Defensa destacó también una serie de prestaciones que la República Popular de China hizo desde el inicio de la pandemia. “La más importante de ellas es que en pocos días está llegando el segundo hospital militar reubicable, una donación del gobierno chino a las Fuerzas Armadas argentinas, que tiene características distintas a los que tienen las fuerzas, ya que este viene montado en camiones, para poder ser trasladable en esos mismos vehículos a distintos lugares de nuestro país. Es algo que en la Argentina no existía y será de muchísima utilidad en este marco”.

Además, el ministro Rossi se refirió al interés que tiene la cartera a su cargo para la adquisición de vehículos blindados a rueda 8x8: “Estamos analizando distintas opciones para los vehículos blindados a rueda que necesita el Ejército Argentino. Obviamente el embajador chino expresó su mirada y nos anunció que la empresa Norinco ( North Industries Corporation) está haciendo modificaciones a su propuesta inicial, que estudiaremos y analizaremos con el resto de las ofertas que tenemos.”

Participaron de la conferencia junto al ministro Rossi el secretario de Asuntos Internacionales para la Defensa, Francisco Cafiero; la subsecretaria de Planeamiento Operativo y Servicio Logístico de la Defensa, Lucía Kersul; y el subsecretario Roberto De Luise.

El régimen chino aparece para el Gobierno de Alberto Fernández como un aliado estratégico a nivel geopolítico para América Latina. Además de los tratados bilaterales, Argentina recibió en los últimos años varios riesgosos créditos blandos –aquellos que suelen significar una trampa para países pobres– para ayuda financiera y acuerdos comerciales con los chinos. 

El jefe de estado Xi Jinping está obsesionado con los recursos naturales de América Latina y nuestro país no escapa a esa lógica. Es por eso que facilita exportaciones hacia sus gigantescos mercados (cerdos, limones y soja, entre otros). A cambio, consiguió el compromiso oficial para construir dos centrales nucleares en la provincia de Buenos Aires, que ya estaban aprobadas durante la administración de Mauricio Macri, además de otras concesiones.

Con respecto a la compra de los blindados por parte del Ministerio de Defensa no es la primera vez que un gobierno kirchnerista intenta adquirir vehículos de ese tipo a la empresa china NORINCO. Hacia fines de 2015, Cristina Kirchner avaló la compra de 110 blindados, que se suspendió por el cambio de gobierno. En agosto de 2020 se iniciaron de nuevo las conversaciones con Beijing. La operación comercial sería por alrededor de 200 vehículos del modelo ZBL09.

Las autoridades militares argentinas solicitaron que 60 de los blindados porten torretas con un cañón de 30 mm, y que el resto tengan una sola ametralladora de 12,7 mm. Además, el Ejército pidió que tuvieran capacidad para transportar soldados armados y rampa para bajar, en lugar de las puertas que tiene el modelo ZBL09. Viene preparado para una tripulación de 3 personas y puede transportar a otras 8, aunque esto se diseñó según parámetros orientales y aquí la dotación debería reducirse.

Esa versión de blindados sólo fue vendida a precio muy bajo a Venezuela en 2012, mientras que otros países del Tercer Mundo como Tailandia, Kenia y Gabón demostraron algún interés en sumarlos a sus Fuerzas Armadas. El ministerio que lidera Rossi podría comprar las unidades desarmadas y luego asignarlas a Tandanor o Tamse, que tienen capacidad ociosa para reconstruir los blindados que llegarían desde China.

Que se opte por la compra a China despierta algunas objeciones internas. Porque ese tipo de vehículos sólo son compatibles con la tecnología china y se reportaron importantes contratiempos en maniobras militares que se ejecutaron en Kenia y Filipinas. NORINCO (North Industries Corporation, por sus siglas en inglés) es una empresa china creada en agosto de 1988 que produce una amplia variedad de vehículos para uso militar y también civil. Pero la calidad de esa maquinaria “blindada” es lo que más preocupa.

El Ejército Argentino necesita contar con un Vehículo de Combate Blindado a Rueda (VCBR) después de haber firmado un memorando con Chile donde se acordó la formación de la Fuerza de Paz Conjunta (FPC) denominada “Cruz del Sur”. Esa fuerza de los dos países incluye componentes aéreos, navales y terrestres. En este último rubro es donde cada nación debe poner a disposición un equipamiento con medios mecanizados a rueda.

Acompañado por el jefe del Estado Mayor Conjunto de las Fuerzas Armadas, el general de Brigada Juan Martín Paleo, y de otros funcionarios de su ministerio, Rossi había visitado en noviembre del año pasado la fábrica de Iveco en Minas Gerais, Brasil, donde había mostrado interés ante empresarios brasileños en la adquisición de los blindados Guaraní 6x6, de los que algunas piezas, como los motores y los chasis, se producen en la planta que Iveco posee en la provincia de Córdoba. En esa ocasión el ministro reconoció que también existían conversaciones con otros proveedores de ese material como General Dynamics (que fabrica el modelo Stryker, en Estados Unidos) y la china NORINCO.

En cuanto a la posible compra de los aviones de combate JF-17 que son desarrollados por Chengdu Aircraft Industry Corporation y por Pakistan Aeronautical Complex desde 2007 se los destinaría a la Fuerza Aérea Argentina para remplazar a los antiguos caza Mirage III/Mirage V y a los Chengdu F-7.