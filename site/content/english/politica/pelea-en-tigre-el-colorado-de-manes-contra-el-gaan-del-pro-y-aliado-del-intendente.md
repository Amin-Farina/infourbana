+++
author = ""
date = 2021-09-01T03:00:00Z
description = ""
draft = true
image = "/images/cernadas-massot-e1627182391244.jpg"
image_webp = "/images/cernadas-massot-e1627182391244.jpg"
title = "PELEA EN TIGRE: EL \"COLORADO\" DE MANES CONTRA EL \"GALÁN DEL PRO\""

+++

###### FUENTE: MendozaOnline

#### En el distrito de Tigre se pronostica una dura puja en Juntos entre Segundo Cernadas y Nicolás Massot en la categoría de concejales. El expresidente del bloque de diputados nacionales del PRO ya salió con los botines de punta acusando al actor de cogobierno con el intendente Zamora.

En Tigre, el distrito donde hace más de una década solo se habla de Sergio Massa, se pronostica una dura pelea interna en la PASO de Juntos por Cambio. Lo más llamativo es que ambos rivales son del PRO pero uno de ellos encabeza la lista de concejales del espacio de Facundo Manes. Se trata de Nicolás Massot. Del otro lado del ring está el actor Segundo Cernadas que compite por la escudería de Diego Santilli. 

Quien ocupara la presidencia del bloque de diputados nacionales del PRO durante la gestión de Mauricio Macri ha decidido meterse en el barro con el objetivo de competir por la intendencia de Tigre. Hasta ahora Cernadas es concejal y el máximo referente amarillo en el distrito de la zona norte. Fue candidato a intendente en 2019 y ahora va por su reelección en el Concejo Deliberante. El desembarco de Massot lo sorprendió al actor porque tendrá una competencia dura.

Ya se sacaron chispas desde antes del cierre de listas porque el extitular de la bancada de diputados arrancó su campaña acusando a Cernadas de “ser funcional” al intendente peronista Julio Zamora. La estocada que más le molestó al concejal del PRO fue la de la supuesta  existencia de un “cogobierno” en Tigre. “No conozco muy bien a ese señor y no entiendo que dice”, fue la respuesta del candidato de Santilli. “Hasta ahora nunca dio explicaciones de su alianza con Zamora”, dicen cerca del "Colorado" de Manes. 

“Lo cierto es que hoy no veo un rol opositor por parte de JxC. Las votaciones importantes en el Concejo Deliberante de Tigre han salido siempre por unanimidad. JxC en general ha votado igual que lo ha hecho el bloque de Massa y el de Zamora.”, dijo Massot en la presentación de su lista. Evidentemente gran parte de la campaña pasará por el rol de Cernadas como opositor. Incluso, hasta hace pocos días ejercía la presidencia del Concejo. Aunque esta situación que despertó sospechas tuvo más que ver con la dura pelea interna entre el actual intendente y Massa.

“Cuando esa alianza se rompió en 2019 Zamora necesitó del apoyo de Cernadas para poder garantizar la gobernabilidad”, dicen en defensa del concejal. Pero desde la otra lista aseguran que “hubo un pacto político”. Esta pelea interna genera tal impacto que el diputado Cristian Ritondo decidió presentarse como candidato a concejal suplente de Cernadas. Mientas tanto Zamora llegó a un acuerdo con Massa y van con lista única liderada por la esposa del jefe comunal, Gisela Zamora, probable aspirante a la sucesión del municipio.