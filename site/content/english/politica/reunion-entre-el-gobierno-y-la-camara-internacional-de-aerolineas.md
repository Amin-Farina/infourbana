+++
author = ""
date = 2021-07-06T03:00:00Z
description = ""
image = "/images/aeropuerto-ezeiza-1.jpg"
image_webp = "/images/aeropuerto-ezeiza.jpg"
title = "REUNIÓN ENTRE EL GOBIERNO Y LA CÁMARA INTERNACIONAL DE AEROLÍNEAS"

+++

**_El Ministerio de Transporte convocó nuevamente a una reunión a la Asociación Internacional de Transporte Aéreo (Iata), que en los últimos días manifestó duras quejas por las restricciones a los vuelos que pueden ingresar al país y la situación de los pasajeros que permanecen varados en el exterior._**

La cartera de transporte conducida por Alexis Guerrera convocó a las autoridades de Iata a una nueva reunión para este martes a las 11, que se realizará a través de la plataforma Zoom. “Se espera poder realizar finalmente el contacto entre el Gobierno y la asociación”, informaron desde el ministerio.

“La reunión fue confirmada por ambas partes para ser llevada adelante luego de la última suspensión realizada la semana pasada por parte de la entidad aérea”, agregaron. Consultados por Infobae, desde Iata respondieron que el encuentro forma parte del diálogo regular entre la asociación, la Administración Nacional de Aviación Civil (Anac) y el Ministerio de Transporte.

El 30 de junio pasado, Iata rechazó participar del encuentro cuando advirtieron que no estaría presente el jefe de Gabiente, Santiago Cafiero, quien fue el responsable de la firma de la Decisión Administrativa 643, que redujo de 2.000 a 600 la cantidad de pasajeros del exterior que pueden ingresar por día a la Argentina. En ese momento, desde Iata habían solicitado que la reunión se reprograme para cuando Cafiero pueda asistir.

Con todo, las autoridades de Jurca, la cámara que agrupa a las empresas aéreas que operan en la Argentina (locales e internacionales) también habían solicitado ser recibidos por las autoridades. Sin embargo, no fueron convocadas a la reunión que se realizará este martes.

La restricción para el regreso de los turistas que están en el exterior dejó a miles de argentinos fuera del país sin fecha cierta de regreso al país. Según estimaron desde Jurca, para los que están varados en destinos que solo tienen un solo vuelo aprobado (por ejemplo, Lima o Bogotá) la demora para retornar podría extenderse hasta cinco meses si se mantiene la restricción diaria para el ingreso.

Los argentinos que están en el exterior o tienen fecha de salida para estos días no tienen ninguna certeza sobre qué puede pasar con su regreso. El Gobierno argentino no autorizó todavía ninguna operación aérea internacional con fecha posterior al 12 de julio.

Desde ANAC, el organismo gubernamental que administra la aviación civil, señalaron que el reacomodamiento de los pasajeros es una decisión que corresponde a cada aerolínea y, por lo tanto, no hay un criterio único. Si las aerolíneas prefieren hacer esperar a algún pasajero para priorizar a otro que lleva más tiempo varado, lo podría hacer, pero no es algo que se regule desde el organismo. Con todo, no hay nada definido por el momento.

En una de las aerolíneas europeas que tienen vuelos a Buenos Aires señalaron que las empresas van ubicando a los pasajeros en los cupos disponibles, pero que necesitan definiciones urgentes para reprogramar vuelos y poder traer a los varados. “Estamos pidiendo al Gobierno eliminar el cupo para traer a los varados. Un vuelo no se puede programar de un momento para otro”, alertaron.

“Los vuelos que están restringidos son los de regreso y se les dio prioridad a los argentinos y residentes por sobre los que estaban iniciando el viaje desde Europa”, explicaron en otra línea aérea que opera desde Europa hacia la Argentina, con varios vuelos semanales. En ese caso, tampoco tienen vuelos autorizados con fecha posterior al 12 de julio.

Este lunes, la Asociación de Transporte Aéreo Internacional (Iata) había insistido en la necesidad de establecer con el Gobierno requisitos “de forma coordinada y previsible” que permitan a la industria operar de forma estable, mientras siga la pandemia.

“Esta normativa establece una reducción del 70% de la capacidad diaria anterior de 2.000 pasajeros que pueden ingresar al país y cada día que pasa está dejando literalmente ‘tiradas’ a unas 1.400 personas en diversos lugares del planeta. La incertidumbre es máxima ya que, a diez días de haberse decretado esta medida, el gobierno argentino no ha autorizado todavía ninguna operación aérea posterior al 12 de julio”, explicaron.

Consultadas por Infobae, fuentes del Gobierno aseguraron que desconocen si se extenderán o no las limitaciones actuales de los vuelos a partir del 9 de julio. Antes de la pandemia se operaban 155 vuelos internacionales diarios y actualmente el límite es de tres o cuatro por día. Las restricciones actuales también impactan a la carga aérea: según los datos de IATA, alrededor del 70% de los productos que se importan —entre ellos insumos básicos para la salud— se trasladan en las bodegas de los aviones de pasajeros.