+++
author = ""
date = 2021-10-21T03:00:00Z
description = ""
image = ""
image_webp = "/images/eenclx8xoaajiop.jpg"
title = "LUCAS GHI DEFENDIÓ QUE LOS JEFES COMUNALES SALGAN A CONTROLAR PRECIOS"

+++

#### **_El intendente de Morón, Lucas Ghi, afirmó este jueves que los jefes comunales tienen "la obligación institucional" de controlar que se cumpla con el congelamiento de los valores de 1.432 productos dispuesto por la Secretaría de Comercio Interior, puesto que la medida busca "que la comunidad pueda consumir a precios razonables"._**

"Corresponde, dada la situación, que cada municipio aporte (con la fiscalización) desde todos los signos partidarios dado que el precio del yogurt o de la leche no reconoce la pertenencia política a la hora de golpear en los bolsillos de las familias", sostuvo Ghi en declaraciones a Radio Provincia.

El gobernador de la provincia de Buenos Aires, Axel Kicillof, respaldó el miércoles la decisión del Gobierno nacional de fijar temporalmente precios máximos para los productos de consumo masivo, y aseguró que trabajará junto con los municipios “para ponerse a la cabeza del control de los precios”.

Junto con el secretario de Comercio Interior, Roberto Feletti, el mandatario bonaerense encabezó una reunión con intendentes de la provincia, a la que no asistieron los jefes comunales de Juntos por el Cambio.

Allí se coordinaron medidas destinadas a garantizar el cumplimiento de las medidas adoptadas por el Gobierno nacional para ordenar los precios.

En ese sentido, Ghi sostuvo que "el gobernador nos pidió que pongamos a disposición los equipos que tenemos en los municipios, organismos fiscalizadores, defensa del consumidor, que monitoreen el cumplimiento de esta resolución mientras continúa el diálogo y el intercambio, que continúan abiertos".

"El gobernador y Feletti nos pidieron que toda vez que detectemos un incumplimiento o un faltante, lo podamos poner en conocimiento de la Secretaría", indicó.

Analizó el jefe comunal de Morón que en los últimos 10 días hubo aumentos "por las dudas" de algunos productos, y dijo que esas subas "no se explican si no es por el carácter especulativo".

"Nadie quiere que las empresas dejen de ganar plata. Queremos que ganen, que inviertan, que generen nuevas unidades de negocios, que den empleo, pero no a costa de la capacidad de consumo de la comunidad", evaluó.

En ese marco, dijo que "generar cierto clima de control ciudadano es parte de lo que se busca", y añadió: "Hay una instancia de diálogo, no es que se patea la mesa y no se habla con nadie".