+++
author = ""
date = 2021-11-12T03:00:00Z
description = ""
image = "/images/oyiugawvfvgnpkgau3glipsoay.jpg"
image_webp = "/images/oyiugawvfvgnpkgau3glipsoay.jpg"
title = "CON EL DÓLAR BLUE ARRIBA DE $200, AXEL KICILLOF DENUNCIÓ \"INTERESES MUY PODEROSOS QUE BUSCAN LA DEVALUACIÓN\""

+++

##### **_A tan solo dos días de las elecciones generales del domingo 14 de noviembre, el gobernador bonaerense, Axel Kicillof, habló sobre el avance del aumento del dólar blue, que ayer cerró en $206,50, y acusó a “determinados sectores” de generar “movidas cambiarias y devaluaciones” y aseguró que “hay intereses muy poderosos que buscan la devaluación”._**

“Llegando al momento electoral, en general hay incertidumbre, que provoca inestabilidad financiera. Pero, además, hay permanentemente intentos de forzar movidas cambiaras y devaluaciones, identificables con determinados sectores. Hay intereses muy poderosos que buscan la devaluación”, denunció Axel Kicillof este viernes 12 de noviembre en diálogo con el programa Mañana Sylvestre, por Radio 10.

En tal sentido, el mandatario provincial advirtió que “hay quienes se benefician muchísimo con esto, porque tienen sus carteras dolarizadas, capacidad de acceder a divisas o su plata fugada en el exterior”. “Creo que es típico en la previa de un proceso electoral, incluso le pasó al propio Macri. Y además cuando gobierna el peronismo se utiliza para generar más incertidumbre y malestar y eso canalizarlo al voto opositor”, añadió.

Kicillof se mostró optimista respecto del panorama actual: “Por un lado, estamos teniendo un superávit de la balanza comercial de US$12 mil millones, uno de los más altos de los últimos tiempos. Así que, en disponibilidad de divisas de acá en adelante, tenemos condiciones que son favorables”, explicó.

Asimismo,el gobernador bonaerense  se refirió al préstamo que el gobierno de Mauricio Macri tomó del Fondo Monetario Internacional (FMI) y le adjudicó la responsabilidad de la inestabilidad económica de la Argentina.

“Lo que más inestabilidad genera es responsabilidad total y completa de Macri, (María Eugenia) Vidal, (Horacio Rodríguez) Larreta y (Diego) Santilli. El año que viene hay vencimientos por US$ 19 mil millones. Eso lo arregló Macri y es muy importante que quede claro la cuestión de las responsabilidades”, sostuvo el exministro de Economía.

A pesar de ello, Kicillof consideró que este año “vamos a crecer un 8% o un 9% en la producción”, pero resaltó dos cuestiones: “En primer lugar, que no alcanza, porque lo que hace es compensar la caída de la pandemia. Además, nos queda recomponer la destrucción del aparato productivo y ver cómo se distribuye el crecimiento. Ahí es donde entran las políticas públicas. Necesitamos seguir aplicando las políticas para que la distribución del crecimiento sea igual para todos”.