+++
author = ""
date = 2021-05-10T03:00:00Z
description = ""
image = "/images/609916e30ef88_1004x565.jpg"
image_webp = "/images/609916e30ef88_1004x565.jpg"
title = "ALBERTO FERNÁNDEZ SE REUNIÓ CON EL PRIMER MINISTRO DE PORTUGAL"

+++
**_El presidente Alberto Fernández se encontraba reunido esta mañana en la ciudad de Lisboa con el primer ministro de Portugal, Antonio Costa, y, tras el encuentro, firmarán una declaración conjunta, informaron fuentes oficiales._**

La reunión se realizaba desde las 11:35 hora local (7.35 de la Argentina) en la residencia oficial del primer ministro, en la capital Lisboa.

Luego de la declaración conjunta, Fernández y Costa compartirán un almuerzo en el mismo lugar, añadieron las fuentes.

Por la tarde, el Presidente y su comitiva partirán hacia la ciudad de Madrid, en España, segunda escala de esta gira que comprenderá también Francia, Ciudad del Vaticano e Italia.