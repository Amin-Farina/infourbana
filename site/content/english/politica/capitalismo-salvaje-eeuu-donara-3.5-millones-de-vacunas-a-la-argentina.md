+++
author = ""
date = 2021-07-16T03:00:00Z
description = ""
image = "/images/vacuna-moderna-1202798-1.jpg"
image_webp = "/images/vacuna-moderna-1202798.jpg"
title = "¿CAPITALISMO SALVAJE? EEUU DONARÁ 3.5 MILLONES DE VACUNAS A LA ARGENTINA"

+++

**_Se trata de la mayor donación de la administración de Joe Biden a un país de América Latina. Dos aviones de Aerolíneas Argentinas ya se encuentran en Memphis para iniciar la operación de traslado._**

El gobierno de los Estados Unidos confirmó este viernes que donará 3.5 millones de vacunas contra el coronavirus elaboradas por el laboratorio Moderna. Para instrumentar la donación, el gobierno argentino firmó un acuerdo este viernes que le permitirá acceder por primera vez a fórmulas desarrolladas por compañías norteamericanas.

Se trata de la mayor donación de vacunas de los EEUU a un país de América Latina hasta el momento. Las dosis están disponibles en un depósito en la ciudad de Memphis, donde ya se encuentran dos aviones de Aerolíneas Argentinas. Los vuelos llegarán este mismo sábado al Aeropuerto de Ezeiza.

“Estados Unidos y Argentina están trabajando juntos como socios para luchar contra esta pandemia global”, aseguró MaryKay Carlson, Encargada de Negocios de la Embajada de Estados Unidos en Argentina y portavoz oficial del anuncio.

“Estamos orgullosos de poder entregar estas vacunas, de reconocida seguridad y efectividad, al pueblo argentino. Este es un momento único en la historia y requiere liderazgo y trabajo en conjunto. Estados Unidos continuará liderando los esfuerzos para construir un mundo más seguro y protegido contra la amenaza de las enfermedades infecciosas, y nos complace trabajar en estrecha colaboración con Argentina en esta importante tarea”, agregó.

“La administración de Biden entiende que para poner fin a esta pandemia es necesario erradicarla en todas partes. Por eso, el Presidente Biden anunció en mayo que Estados Unidos donaría 80 millones de vacunas para enfrentar las necesidades globales”, se precisó en un comunicado difundido por la Embajada de EEUU en Buenos Aires.

A nivel bilateral, el gobierno de Estados Unidos ha donado más de 4 millones de dólares en suministros para la lucha contra la pandemia en Argentina. En abril, el Almirante Craig S. Faller, jefe del Comando Sur, visitó Argentina para donar tres hospitales de campaña, equipos de búsqueda y rescate y otros suministros para ayudar a combatir el brote de COVID-19.

El presidente Alberto Fernández agradeció a Biden por “su decisión solidaria de ayudar a los demás países del mundo y por el trabajo conjunto en la lucha por la pandemia”. En ese marco, el Jefe de Estado reiteró que “la pandemia es una advertencia y, al mismo tiempo, una oportunidad para avanzar hacia sociedades más equitativas, más inclusivas y más justas”.

Y agregó: “Esta donación del gobierno de Estados Unidos es una contribución muy importante que marca un camino de cooperación”.

### La negociación

Por parte del Gobierno, los detalles del acuerdo estuvieron a cargo de la ministra de Salud, Carla Vizzotti; la asesora presidencial, Cecilia Nicolini, y el embajador argentino en Estados Unidos, Jorge Argüello. Los tres estuvieron inmiscuidos en las negociaciones.

Las dos funcionarias del gobierno nacional llegaron este lunes a Reino Unido. El viaje hacia Europa tuvo como objetivo intercambiar experiencias con autoridades locales sobre estrategias de vacunación, sostener un puñado de reuniones con expertos sobre el manejo de la pandemia y encontrarse con representantes del laboratorio AstraZeneca.

Sin embargo, durante la estadía hubo permanente contacto con Estados Unidos y se avanzó en el acuerdo para concretar la donación.

El lunes 8 de julio, el jefe de Gabinete, Santiago Cafiero, anunció la firma de un contrato con Moderna para la compra de vacunas. Finalmente hoy, ocho días después, la Casa Rosada informó que se concretó el acuerdo con el laboratorio por un total de 20 millones de dosis.

Hasta el momento, el gobierno de Fernández compró vacunas a China Rusia, y formó parte de la cadena de producción de dosis del laboratorio AstraZeneca, que desarrolló la Universidad de Oxford.

La compañía estadounidense ha presentado los ensayos y la documentación requerida por las principales autoridades regulatorias con el objetivo de lograr la autorización de emergencia para su aplicación en adolescentes. La posibilidad de vacunar adolescentes es importante para el Gobierno, ya que puede avanzar con el resto de la población con otras vacunas.

Por el momento, Pfizer es el único laboratorio que tiene inoculantes habilitados para chicos que tengan entre 12 y 18 años. Con la modificación de la ley de vacunas, el Gobierno espera poder avanzar con la compra de esas dosis para ampliar el universo de aplicación antes de que se cierre el año.

El producto de Moderna será el primero en llegar al país de laboratorios norteamericanos; podrá empezar a arribar a partir del DNU que firmó Alberto Fernández para modificar la ley de vacunas. Luego el Gobierno podría avanzar en la compra de dosis de Pfizer y Johnson & Johnson, dos laboratorios con los que está negociando desde hace tiempo.