+++
author = ""
date = 2021-08-23T03:00:00Z
description = ""
draft = true
image = "/images/3-tristes-tigres.PNG"
image_webp = "/images/3-tristes-tigres.PNG"
title = "MANES “MIENTE DESCARADAMENTE”, NIEGA SU PASADO CON VIDAL, PIERDE APOYO DE LA UCR Y FAVORECE A MASSA"

+++
## _Facundo Manes dejó a muchos radicales enojados. El periodista Luis Otero renunció en Avellaneda. En Tigre fueron desplazados por la candidatura de Massot. Carrió lo acusó de mentiroso._

“_Yo no fui parte de Cambiemos, no pertenecí al gobierno de Macri ni de Vidal_” disparó el neurocientífico a Infobae ([Link]()). La respuesta estuvo a cargo de Lilita Carrió: “_Manes está mintiendo. En Juntos por el Cambio tenemos una regla que es no mentirnos. Podemos tener discusiones pero todos tenemos amistad política. Este señor miente descaradamente._” respondió también al mismo medio nacional ([Link](https://www.infobae.com/politica/2021/07/27/elisa-carrio-tambien-cargo-contra-facundo-manes-y-arde-la-interna-de-juntos-por-el-cambio-miente-descaradamente/)).

Las declaraciones de Manes pusieron en apuros a Juntos y dieron un argumento al Frente de Todos. El jefe de gabinete de Axel Kicillof,  Carlos Bianco también acusó a la oposición de mentir y aseguró a la agencia Telam “_tener pruebas de que el neurocientífico formó parte del gobierno de María Eugenia Vidal a pesar que él diga lo contrario._” ([Link]())

Por otro lado el frente radical se complica para el candidato quien perdió el apoyo de sus propios aliados internos y no logró sumar a Gustavo Posse en su candidatura. Mas de medio partido radical retiró su apoyo.

La semana pasada se conoció la renuncia del periodista Luis Otero a la presidencia de la Unión Cívica Radical de Avellaneda que obtuvo un gran resultado en 2019. Su hartazgo vino, también, porque sintió que no se le había respetado lo acordado para encabezar la lista de candidatos a diputados provinciales, informó el Cronista Comercial (Link)

En el caso de Tigre se acerca un pronunciamiento oficial partidario ya que ninguno de los referentes mayoritarios de la UCR fue incorporado en las listas de Manes, de modo que su delfín en Tigre, Nicolás Massot, solo cuenta con su cercanía a algunos dirigentes del Frente de Todos, especialmente Sergio Massa, como ya indicaron diferentes medios ([Link](https://www.cronista.com/columnistas/el-peor-cierre-de-listas-una-insolita-confusion-de-afiches-y-maximo-el-que-menos-grita/)).