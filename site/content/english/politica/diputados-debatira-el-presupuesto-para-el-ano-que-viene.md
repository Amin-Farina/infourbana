+++
author = ""
date = 2021-12-16T03:00:00Z
description = ""
image = "/images/vs6sapi2brak7o26wttbba7uj4.jpg"
image_webp = "/images/vs6sapi2brak7o26wttbba7uj4.jpg"
title = "DIPUTADOS DEBATIRÁ EL PRESUPUESTO PARA EL AÑO QUE VIENE"

+++
#### **_El Presupuesto 2022 girado por el Gobierno prevé un crecimiento del 4% del Producto Bruto Interno (PBI), una inflación del 33% y un dólar a $ 131,1, y contempla un incremento real del gasto social y una reducción de los subsidios energéticos._**

La Cámara de Diputados debatirá en la sesión convocada para este jueves al mediodía el Presupuesto 2022 girado por el Gobierno, que prevé un crecimiento del 4% del Producto Bruto Interno (PBI), una inflación del 33% y un dólar a $ 131,1, y contempla un incremento real del gasto social y una reducción de los subsidios energéticos.

El miércoles se firmó el dictamen de mayoría que aglutinó la aceptación de los 24 integrantes del Frente de Todos y del representante del Frente de la Concordia misionero.

Por la oposición, los dos diputados de Evolución Radical que integran la comisión firmaron un dictamen de rechazo; en tanto que los otros 21 integrantes del principal conglomerado opositor expresarán el rechazo en el recinto, aunque aún no firmaron dictamen en ese sentido.

Además, el interbloque Federal, por intermedio del representante de Córdoba Federal, Ignacio García Aresca, firmó un dictamen de minoría con sello propio.

Más allá de haber conseguido el número para alzarse con el dictamen de mayoría, el oficialismo aún no tiene asegurada la cantidad de diputados para imponerse en el recinto, por lo que durante toda la jornada de ayer se sucedieron diferentes reuniones en busca de obtener consensos con algunos bloques opositores.

El proyecto será el único tema de la sesión convocada para las 12 y que, según fuentes parlamentarias se extendería por al menos 24 horas.