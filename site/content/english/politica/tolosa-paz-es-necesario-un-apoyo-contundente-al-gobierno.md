+++
author = ""
date = 2021-08-06T03:00:00Z
description = ""
image = "/images/6109c8367904f_1004x565-1.jpg"
image_webp = "/images/6109c8367904f_1004x565.jpg"
title = "TOLOSA PAZ: \"ES NECESARIO UN APOYO CONTUNDENTE AL GOBIERNO\""

+++

**_La precandidata a primera diputada nacional del Frente de Todos por la provincia de Buenos Aires, Victoria Tolosa Paz, afirmó este viernes que es necesario un "apoyo contundente" al Gobierno en estas elecciones y subrayó que la Argentina "necesita diputados y diputadas capaces de dar un debate en el Congreso sobre cómo y de qué manera la Argentina sale adelante"._**

"Para reafirmar el camino del crecimiento, necesitamos que haya un apoyo contundente al presidente Alberto Fernández y al gobernador Axel Kicillof", remarcó la precandidata este viernes por la mañana en declaraciones formuladas a la radio online FutuRöck.

"La Argentina necesita diputadas y diputados capaces de debatir cómo la Argentina sale adelante mirando la enorme capacidad productiva y hay algunas cosas para resolver, por ejemplo la matriz impositiva. Tenemos que discutir una matriz cada vez más progresiva de impuestos, como el aporte solidario, que nos ayudó mucho a redistribuir", planteó Tolosa Paz.

En ese sentido, la precandidata del Frente de Todos consideró que "las discusiones tienen que centrarse en qué hacemos para crecer, pero que el crecimiento no sea desigual".

En la misma línea, sostuvo que ella, al igual que el segundo de la lista del FdT en el distrito bonaerense, el exministro Daniel Gollan, tienen la "mirada puesta en cómo hace el Estado, a partir de sus incentivos y su liberación de carga impositiva, para generar las condiciones que acompañan el desarrollo y el crecimiento".

La presidenta​ saliente del Consejo de Políticas Sociales se refirió a las mediciones de inflación desde enero del 2021 y, sobre ese punto, aseguró que el proceso de suba de precios está en descenso, y entonces adelantó que esa tendencia se comprobará cuando se conozcan los números de julio.

"El primer trimestre fue muy malo y en marzo llegamos a una inflación de 4.8", dijo Tolosa Paz pero luego remarcó que "algunos (especialistas) nos dicen que la medición de julio va a estar por debajo de los 3 puntos".

"Seis de cada diez niños es pobre. Ese 57.7 de pobreza en la infancia de 0 a 14 es donde primero van a impactar las medidas que tomamos. Va a haber una reducción de la pobreza en ese grupo etario", puntualizó la precandidata.

En relación a la negociación salarial y por condiciones de trabajo, Tolosa Paz resaltó que la administración del FdT "ha abierto paritarias cada vez que ha tenido la posibilidad".

"Si nosotros dejamos que el salario vaya perdiendo capacidad de compra, las familias compran menos, y nosotros necesitamos sostener la demanda de lo que la Argentina produce", reafirmó.

En otro orden, al ser consultada por los precandidatos de la oposición, consideró que "(Facundo) Manes y (Diego) Santilli expresan un mismo proyecto de país" y que "ninguno de ellos puso ni un 'pero' ante la gestión de Maria Eugenia Vidal".

Luego, advirtió que "los niveles (sobre todo de endeudamiento, pero también en materia social) a los que llegó la Argentina en 2019 se van a repetir si alguna vez vuelve a gobernar Cambiemos" ya que "cuando gobiernan llevan a cabo políticas diametralmente opuestas a lo que prometen en campaña".

"Ellos prometieron una Argentina que crezca y la Argentina decreció", recordó.