+++
author = ""
date = 2021-09-06T03:00:00Z
description = ""
draft = true
image = "/images/manes-massot-paseo-victorica-tigre-696x522-1.jpeg"
image_webp = "/images/manes-massot-paseo-victorica-tigre-696x522.jpeg"
title = "NICOLÁS MASSOT SOSTUVO: \"SOMOS LA MEJOR OPCIÓN PARA GANARLE AL KIRCHNERISMO EN NOVIEMBRE\""

+++
###### FUENTE: QUEPASAWEB

##### **_El precandidato a diputado nacional de Dar el Paso dentro de Juntos acompañó al precandidato a concejal en el Paseo Victorica de Tigre. "Le tienen miedo al voto de la sociedad argentina, por eso el 12 de septiembre el voto va a ser el lenguaje universal de la rebelión", definió._**

En la tarde de este domingo, Facundo Manes y Nicolás Massot encabezaron un acto en el Paseo Victorica de Tigre, donde dialogaron los vecinos que se encontraban en el lugar y realizaron un acto. Allí el precandidato a diputado nacional de Dar el Paso dijo que en estas elecciones “hay que ganarle al kirchnerismo pero además transformar la Argentina para siempre”.

En materia económica, Manes señaló que la Argentina sufre de “una involución crónica” y por ese motivo “no podemos seguir emparchando: esto no lo va a cambiar una ley solamente, lo va a cambiar la sociedad argentina, millones de personas con una rebelión como está empezando a suceder de abajo para arriba, como pasó con la reconstrucción democrática del ’83: si millones de argentinos no encaramos el siglo XXI, la modernidad y el progreso, no lo va a hacer ningún dirigente“.

“Hoy la economía del mundo es el conocimiento y los países compiten por los sistemas científicos y tecnológicos, mientras acá estamos en una grieta que nos empobrece, nos embrutece y sólo le conviene a los antagónicos de esa grieta para mantener el poder“, manifestó el referente de Dar el Paso y agregó que “le tienen miedo al voto de la sociedad argentina, por eso el 12 de septiembre el voto va a ser el lenguaje universal de la rebelión y vamos a rebelarnos contra este sistema que quiere que nada cambie”.

Además, cuestionó a quienes “dicen que no tenemos experiencia, pero nosotros no queremos la experiencia de un 50% de la población viviendo en la pobreza, de una corrupción sistémica, de una inflación que nos come los salarios. Queremos una nueva experiencia, que sea imparable, no tiene fecha ni vencimiento ni techo. Se está generando en toda la provincia de Buenos Aires”.

“Tenemos enfrente un modelo de país que no queremos: el kirchnerismo representa el pasado. Gobernaron 16 de los últimos 20 años en Argentina, y en la Provincia 30 de los últimos 34 y cada vez estamos peor. ¿Y saben quién es la mejor opción para ganarle en noviembre y en el 2023? Nosotros. Estamos juntando gente que estaba desilusionada con Cambiemos, y gente que nunca había votado a Cambiemos”, enfatizó y cerró: “No queremos ganarle con el envión de pensar que no pasó nada entre el 2015 y el 2019: sí pasaron cosas y tenemos que hacer la autocrítica. Queremos una coalición más amplia, que revise lo que se hizo mal y que no sólo le gane al kirchnerismo. Hay que ganarle, pero además transformar la Argentina para siempre”.

Link: [https://www.quepasaweb.com.ar/manes-massot-paseo-victorica-acto-tigre/](https://www.quepasaweb.com.ar/manes-massot-paseo-victorica-acto-tigre/ "https://www.quepasaweb.com.ar/manes-massot-paseo-victorica-acto-tigre/")