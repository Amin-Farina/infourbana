+++
author = ""
date = 2021-07-20T03:00:00Z
description = ""
image = "/images/9-de-julio.jpg"
image_webp = "/images/9-de-julio.jpg"
title = "PIQUETES Y MOVILIZACIONES EN LA 9 DE JULIO"

+++

**_Organizaciones sociales y partidos de izquierda cortan la 9 de Julio y la avenida Corrientes desde las 8.30 horas y dificultan la circulación de los vehículos hacia el norte de la Ciudad._**

Con carteles y redoblantes, un nutrido grupo de manifestantes está dispuesto a acampar en la Plaza de la República hasta el mediodía y luego decidirán en asamblea si marchan hacia el Congreso.

El Metrobus, por el momento, se encuentra habilitado por un operativo dispuesto por la Policía de la Ciudad.

Los reclamos son variados. Se leen pancartas que exigen la reincorporación de trabajadores despedidos de Edenor y de trabajadores de la salud que reclaman por aumento de salarios. También están los que reclaman aumento en los cupos y montos de planes sociales.

Cinco días atrás la 9 de Julio también fue escenario de protestas y reclamos, pero a la altura de la avenida San Juan. En aquella ocasión, le pedían al gobierno porteño tierras para la construcción de viviendas y para la producción de alimentos.

“Nosotros, nosotras, queremos tierra para trabajar, para desarrollar más espacios productivos y autogestionados. Necesitamos ampliar nuestra producción alimenticia. Porque de esto depende el trabajo de miles de familias en todo el país. Necesitamos tierra para construir viviendas, para vivir con dignidad”, subrayaron las organizaciones en un comunicado conjunto.

La protesta había sido convocada por el Frente de Organización en Lucha (FOL), el Frente Popular Darío Santillán (FPDS), el MTD Aníbal Verón, el Movimiento Resistencia Popular (MRP) y el Frente de Organizaciones de Base (FOB).

Y a mediados de julio, militantes del Movimiento Independiente de Jubilados y Desocupados (MIJD) también realizaron un corte de tránsito en la intersección de las avenidas 9 de Julio y Corrientes para visibilizar su reclamo de aumento de cupos de planes de empleo y de asistencia sanitaria.

El 12 de julio por la mañana, los manifestantes protagonizaron algunos incidentes al enfrentarse con el cordón de efectivos de la Policía de la Ciudad de Buenos Aires que procuraba desplazar el corte. Luego de acordar que la protesta no afectara el tránsito en forma total en esa neurálgica intersección porteña, los manifestantes mantuvieron la protesta en torno al Obelisco y luego del mediodía dieron por finaliza la movilización.