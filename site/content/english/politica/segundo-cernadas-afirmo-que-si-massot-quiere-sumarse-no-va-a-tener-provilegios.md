+++
author = ""
date = 2021-06-15T03:00:00Z
description = ""
draft = true
image = "/images/img_1886.JPG"
image_webp = "/images/img_1886.JPG"
title = "SEGUNDO CERNADAS AFIRMÓ QUE: \"SI MASSOT QUIERE SUMARSE, NO VA A TENER PROVILEGIOS\""

+++
# 

**_El presidente del Concejo Deliberante y referente de Juntos Por El Cambio de Tigre Segundo Cernadas habló sobre el respaldo que le brindaron Horacio Rodriguez Larreta y María Eugenia Vidal al acercarse al distrito y también sobre los nuevos actores de la política local._**

“Fue un apoyo muy grande porque estuvieron los dos juntos y fue una de las primeras visitas a la provincia de Horacio” expresó Cernadas.,

Sobre el encuentro el concejal indicó que “hablamos de la estrategia de acá en más para una elección complicada en un año de pandemia pero que es muy significativa”.

En ese sentido, Cernadas aseguró que “hay una preocupación por la situación que estamos viviendo, por las vacunas que no dan y la falta de explicaciones, una economía totalmente destrozada y con una perspectiva de pobreza del 50 por ciento para fin de año, lo que es dramático, seguimos peleando para que los chicos vuelvan a clases, no porque estamos locos sino porque hay datos concretos de que las escuelas no son un lugar de contagio importante”.

Consultado sobre si Vidal le dio indicios de su proyecto político, Cernadas aseguró que “María Eugenia no tiene definido si va a jugar en Capital, si lo hará en provincia o si no jugará. La queremos en provincia pero confíamos en que su decisión va a ser lo mejor” adujo.

Sobre los nuevos actores que buscan espacio en Juntos Por El Cambio de Tigre como Nicolás Massot y Oscar Flores, Cernadas indicó que “por supuesto hay que ampliar” pero marcó que “si Massot quiere sumarse, también hay que sumar a todos los demás”.

“No le vamos a dar el privilegio a una persona y a los demás no. Hay que ir de a poco dando oportunidad de conocer lo que pasa en Tigre para después conformar las listas” concluyó.