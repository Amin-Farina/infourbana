+++
author = ""
date = 2021-10-22T03:00:00Z
description = ""
image = "/images/gustavo-gennuso-21102021-1252625-1.jpg"
image_webp = "/images/gustavo-gennuso-21102021-1252625.jpg"
title = "LA RESPUESTA DEL INTENDENTE DE BARILOCHE AL PRESIDENTE ALBERTO FERNÁNDEZ: \"NOS SENTIMOS ABANDONADOS\""

+++

#### **_La carta que el presidente Alberto Fernández le envió ayer a la gobernadora de Río Negro, Arabela Carreras, desató un nuevo enfrentamiento entre ambas administraciones por el conflicto por los ataques de comunidades mapuches contra la propiedad privada y símbolos de la provincia._** 

Mientras que la Provincia acusa a la Nación por inacción ante la escalada de violencia, desde la Rosada aseguran que “no es función del Gobierno brindar más seguridad en la región”.

En ese contexto, el intendente de Bariloche, Gustavo Gennuso, advirtió: “La carta del Presidente es un quiebre institucional muy fuerte. La presencia del Estado nacional es muy importante para la situación que vivimos acá”.

Las alarmas provinciales volvieron a encenderse este miércoles, cuando grupos autodenominados mapuches destruyeron por completo las instalaciones del Club Andino Piltriquitrón, el más emblemático de la localidad de El Bolsón, y los autores dejaron panfletos con amenazas para el Intendente Bruno Pogliano y la gobernadora Arabela Carreras.

En el lugar se hallaron bidones con combustible, lo que confirma la intencionalidad del hecho. Además aparecieron panfletos en los que mencionan al jefe comunal y la mandataria rionegrina y a los magnates Lewis y Benetton.

Gennuso aclaró que los responsables de los ataques, como en El Bolsón, son un grupo minoritario por lo que el problema no es de difícil solución si es tomado seriamente. “No es tan grave el problema si se lo aborda. Venimos reclamando que no se está abordando el problema en términos de gobierno nacional. De acciones concretas y del diálogo que tiene que hacer”, señaló al ser entrevistado en Radio Continental.

“El gobierno nacional ha declamado que vienen por la vía del diálogo pero yo no veo que estén dialogando. No es un hecho que haya cientos de personas, es un grupito muy minúsculo”, agregó en la misma entrevista.

Consultado por el rol de la Policía de Río Negro, el funcionario explicó que hicieron un buen trabajo, intentaron custodiar los territorios y lograron desestimar varias tomas. Sin embargo, remarcó que lo que es territorio nacional les corresponde a las fuerzas federales.

“Denota que el Estado nacional no hace nada por nosotros, nos deja solos y es un quiebre institucional muy fuerte. El ciudadano tiene que sentir que el gobierno te cuida. Esto puede llevar a que la gente quiera hacer justicia por sí misma. Me parece que la carta es muy grave. Nosotros vemos que las fuerzas nacionales van a otros lugares”, sentenció al referirse a la presencia de la Policía Federal en Rosario o el conurbano bonaerense, por ejemplo. “Nos sentimos abandonados más que indefensos. No es una cuestión tanto de defensa en términos de seguridad sino de acción política”, apuntó.

Además, el intendente insistió en que hace falta que la Provincia y Nación retomen el diálogo porque está convencido de que es un problema que se puede solucionar si hay voluntad política. “Yo reclamo el tema seguridad, que no haya cortes de rutas, que no haya violencia contra bienes incluso del propio Estado y por otro lado que no nos abandonen en la acción política. Que dialoguen, pero en serio”, enfatizó el intendente de Bariloche.

“Quiero pensar que no actúan porque no saben qué hacer. No le encuentran la vuelta, pero pienso esto porque quiero pensar bien”, indicó luego de la “grave carta” que dio a conocer el Presidente y de la experiencia vivida con el gobierno anterior.

“Nos pasó algo parecido de Macri. Creo que es por desconocer el territorio. Los que vienen llegan con un preconcepto. Hay un desconocimiento fuerte del territorio y un dejar hacer que se va a solucionar solo y las cosas no se solucionan solas”, recordó.

En la carta de la polémica, el Presidente -además- aconsejó que, para prevenir futuros incidentes, la provincia patagónica forme “un cuerpo específico que se ocupe de los refuerzos del control y mayor seguridad en el futuro”.

Más temprano, el ministro de Seguridad, Aníbal Fernández, afirmó que la decisión de reforzar la seguridad en Río Negro con el redespliegue de fuerzas federales tras el ataque al Club andino Piltriquitrón, en El Bolsón, fue adoptada por “solidaridad” con la gobernadora Arabela Carreras y para “colaborar” con la provincia.

A principios de mes, Bariloche también fue escenario de un ataque mapuche en el campamento de Vialidad Provincial, camino al cerro Catedral, a unos 15 kilómetros del centro de Bariloche. Los integrantes de la agrupación identificada con las siglas RAM son los principales sospechosos de este ataque, a quienes el Gobierno de Río Negro denunció por “terrorismo”.