+++
author = ""
date = 2022-01-25T03:00:00Z
description = ""
image = "/images/featured-1.jpg"
image_webp = "/images/featured.jpg"
title = "ARGENTINA RETROCEDIÓ 18 LUGARES EN EL RANKING GLOBAL QUE MIDE LA CORRUPCIÓN"

+++

#### **_Obtuvo solo 38 puntos sobre 100 asignados al país más transparente, según la edición 2021 del Índice de Transparencia Internacional_**

En 2021, el año atravesado por los intentos de reformar la Justicia por parte del Gobierno, el escándalo del “vacunatorio vip”, el impacto de la foto del cumpleaños de primera dama en Olivos durante la cuarentena estricta y los cuestionamientos por la falta de transparencia en la compra de vacunas, Argentina volvió a empeorar en el Índice de Percepción de la Corrupción.

![](/images/qflzz5vwefhxjbzxuqgxss6dtq.jpg)

La mala noticia para el gobierno de Alberto Fernández se conoció en la última edición del indicador elaborado por Transparencia Internacional y difundido esta mañana en Berlín (Alemania), sede central de esta organización no gubernamental (ONG) líder en la lucha contra la corrupción.

En el segundo año de gestión del Frente de Todos, Argentina obtuvo 4 puntos menos que el 2020 - 38 sobre 100 máximos posibles para el país más transparente - por debajo de la media global de 43. Retrocedió así 18 lugares y se ubicó en el puesto 96 entre 180 países. En 2020, Argentina había obtenido 42 puntos y quedado en el lugar 78 del ranking.

En 2021, el año atravesado por los intentos de reformar la Justicia por parte del Gobierno, el escándalo del “vacunatorio vip”, el impacto de la foto del cumpleaños de primera dama en Olivos durante la cuarentena estricta y los cuestionamientos por la falta de transparencia en la compra de vacunas, Argentina volvió a empeorar en el Índice de Percepción de la Corrupción. La mala noticia para el gobierno de Alberto Fernández se conoció en la última edición del indicador elaborado por Transparencia Internacional y difundido esta mañana en Berlín (Alemania), sede central de esta organización no gubernamental (ONG) líder en la lucha contra la corrupción.

En el segundo año de gestión del Frente de Todos, Argentina obtuvo 4 puntos menos que el 2020 - 38 sobre 100 máximos posibles para el país más transparente - por debajo de la media global de 43. Retrocedió así 18 lugares y se ubicó en el puesto 96 entre 180 países. En 2020, Argentina había obtenido 42 puntos y quedado en el lugar 78 del ranking.

El mejor score de nuestro país en el Índice fue en 2019, cuando escaló 19 posiciones y obtuvo su mejor desempeño desde 2012 con 45 puntos. Ese último año de la gestión de Mauricio Macri, Argentina quedó en el puesto 66, treinta lugares más arriba que el último Índice del 2021. La peor performance había sido en 2015, cuando obtuvo solo 32 puntos sobre 100, y quedó en el puesto 107 sobre 168 países, mucho más cerca del final de la tabla que hoy.

El Índice de Percepción de la Corrupción (IPC) clasifica esa cantidad de países y territorios según el nivel de percepción de la corrupción en el sector público, en una escala de cero para los muy corruptos, a cien, el máximo puntaje para los más limpios.

A la cabeza, vuelven a ubicarse Dinamarca, Finlandia y Nueva Zelanda, con 88 puntos. Estos tres países también se encuentran entre los diez mejor puntuados en cuanto a libertades civiles según el informe del Índice Democrático. Somalía y Siria, con 13 puntos, y Sudán del Sur, con 11 sobre 100, obtienen de nuevo las puntuaciones más bajas del Índice. Siria es también el último país en materia de libertades civiles.

“El resultado para Argentina es malo y se observa que mantiene una caída en la percepción de los consultados sobre el nivel de corrupción en el sector público en nuestro país. Creemos que este resultado está influenciado por diferentes motivos. En primer lugar, por los intentos de reformas en el sistema de justicia, especialmente el proyecto que intentó modificar el Ministerio Público Fiscal. Pero, especialmente, el comienzo irregular y discrecional del reparto de vacunas por parte del Poder Ejecutivo, y el acontecimiento de la fiesta privada del Presidente, mientras se establecían restricciones a la circulación y reunión de la ciudadanía, entendemos que pueden explicar esta caída en la percepción”, analizó Pablo Secchi, Director Ejecutivo de Poder Ciudadano, capítulo argentino de Transparencia Internacional.

##### **La situación en América**

En el continente americano, el ranking lo lideran los mismos cuatro países. Canadá es el mejor posicionado con 74 puntos sobre 100, pero con una caída respecto del año anterior cuando había obtenido 77. Le siguen Uruguay con 73 puntos, dos más que en 2020 y al tope de la región, y en tercer lugar, Chile, que mantuvo el puntaje de 67.

En cuarto lugar en América se ubica Estados Unidos, con 67 puntos, igual que en 2020. Sin embargo, este país dejó de estar entre los 25 países mejor calificados por primera vez.

De Sudamérica, por encima de Argentina en el ranking se ubica - además de Uruguay y Chile- Colombia, con 39 puntos. Brasil obtuvo el mismo puntaje que nuestro país (38); Ecuador y Perú, aparecen con 36, y Bolivia y Paraguay con 30, se ubican por debajo.

El peor país de la región vuelve a ser Venezuela en el puesto 177 sobre 180 países. “El gobierno del presidente Nicolás Maduro ha acallado el disenso entre sus rivales políticos, periodistas e incluso trabajadores sanitarios. A lo largo de la última década, el país ha sufrido una caída considerable en el índice, con su calificación más baja hasta la fecha, 14 puntos, en 2021″. señaló Transparencia Internacional.

Nicaragua, la otra nación de la región cuestionada internacionalmente por la violación a los derechos humanos y las libertades civiles, sólo obtuvo 20 puntos, en el puesto 167.

##### **Los resultados a nivel global**

Según esta nueva edición del Índice de Percepción de la Corrupción, los niveles de corrupción se encuentran estancados a nivel mundial, con escaso o ningún progreso en el 86% de los países evaluados en los últimos diez años. Dos tercios de los 180 países medidos no llegan a 50 puntos.

No solo Argentina tuvo una mala performance en 2021. Otros 27 países, como Chipre (53), Líbano (24) y Honduras (23) sacaron puntuaciones más bajas que en 2020. En la última década, 154 países sufrieron un deterioro en materia de transparencia y percepción de la corrupción en el sector público. Desde 2012, 23 países han decaído en el índice, entre ellos algunas economías avanzadas como Australia y Japón (73 puntos ambos en el IPC 2021).

El ranking de los 10 más transparentes lo completan, debajo de Dinamarca, Finlandia y Nueva Zelanda, Noruega, Singapur, Suecia, Suiza, Países Bajos, Luxemburgo - todos con más 80 puntos- y Alemania, que mantuvo esa puntuación respecto del año pasado.

Transparencia Internacional identificó que los países que vulneran las libertades civiles obtienen de forma consistente puntuaciones más bajas “La complacencia en la lucha contra la corrupción da pie a violaciones de derechos humanos de mayor gravedad y socava la democracia, detonando así una espiral viciosa. Conforme se erosionan los derechos y libertades y se debilita la democracia, el autoritarismo avanza, lo cual contribuye a aumentar aún más la corrupción”, aseveró esta ONG.

“Los derechos humanos son más que algo deseable en la lucha contra la corrupción. Los modelos autoritarios destruyen los controles y contrapesos independientes y hacen que tomar acción contra la corrupción dependa de los caprichos de una élite. La única ruta sostenible hacia una sociedad libre de corrupción es asegurar que la población tenga la posibilidad de hablar libremente y trabajar colectivamente para pedir cuentas a aquellos en el poder”, sostuvo por su parte la abogada argentina Delia Ferreira Rubio, presidenta de Transparencia Internacional.

##### **Cómo se elabora el Índice**

El IPC mide anualmente la percepción de la corrupción de empresarios y expertos pero sólo en relación a la conducta del sector público, o sea en el ámbito político y administrativo. Se calcula tomando 13 fuentes externas, entre ellas, el Banco Mundial, el Foro Económico Mundial, empresas privadas especializadas en análisis de riesgo, firmas de consultoría y comités de expertos. Las puntuaciones reflejan las opiniones de especialistas y empresarios sobre cómo perciben la corrupción en el Estado.

Para incluir un país o territorio en el índice es necesario que haya sido evaluado por un mínimo de tres fuentes. La puntuación de cada país se determina calculando la media de todas las puntuaciones estandarizadas disponibles sobre ese país, redondeada a un número entero.

En el caso de Argentina, la medición se hizo en base a ocho encuestas de distintas entidades internacionales: el Índice de Transformación de la Fundación Bertelsmann, las Calificaciones de Riesgo País de Global Insight, el Anuario de Competitividad Mundial del International Institute for Management Development (IMD), The Economist Intelligence Unit Country Ratings, la Guía Internacional de Riesgo País de PRS Group, el Proyecto Variedades de Democracia, el Foro Económico Mundial EOS, y el Índice de Estado de Derecho del Proyecto Justicia Mundial.

Las preguntas se refieren, entre otros aspectos, a la existencia de sobornos, el desvío de fondos públicos, la falta de castigo penal o de protección a los denunciantes, los mecanismos para obtener contratos con el Estado, la prevención de los conflictos de interés por parte de funcionarios y el acceso a información por parte de los ciudadanos a asuntos públicos, entre otros aspectos.