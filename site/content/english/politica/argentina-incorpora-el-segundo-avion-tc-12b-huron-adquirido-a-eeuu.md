+++
author = ""
date = 2022-02-15T03:00:00Z
description = ""
image = "/images/avion-beechcraft-tc-12b-huron-20220214-1312667-1.jpg"
image_webp = "/images/avion-beechcraft-tc-12b-huron-20220214-1312667.jpg"
title = "ARGENTINA INCORPORA EL SEGUNDO AVIÓN TC-12B HURON ADQUIRIDO A EEUU"

+++

##### _Durante una ceremonia llevada a cabo en el sector militar del Aeroparque Metropolitano encabezada por el ministro de Defensa, Jorge Taiana, junto al titular de la Fuerza Aérea, brigadier general Xavier Isaac, la aviación militar incorporó la segunda de las diez aeronaves estadounidenses de la serie “Beechcraft” que el Estado Nacional adquirió en abril de 2021 dentro del programa de ventas militares al extranjero conocido como FMS y propiciado por el gobierno de Estados Unidos._

El costo de la operación ronda los US$ 16.600.000 y dentro del monto abonado se incluyen dos unidades similares destinadas a la Armada Argentina y servicios relativos al mantenimiento y entrenamiento de pilotos.

Al hacer uso de la palabra ante el personal militar presente Taiana sostuvo: “El FONDEF (Fondo Nacional de la Defensa) nos permite hacer esta compra plurianual con una estrategia y distribución de costos planificada. Estamos dando un paso cualitativo en el fortalecimiento y recuperación de nuestras capacidades militares”.

Vale recordar que el FONDEF surge a partir de una iniciativa del ex ministro Agustín Rossi durante su gestión como diputado nacional y mediante el cual se creó un fondo de afectación específica al reequipamiento militar y por fuera de las partidas presupuestarias atinentes a solventar los gastos de funcionamiento corrientes de las FFAA.

La llegada de esta aeronave coincide con el anuncio de una próxima visita de altos jefes de la Fuerza Aérea Argentina a China para ver en directo los aviones de combate JF-17 que Xi Jinping ofreció hace poco más de un año al Ministerio de Defensa.

La intención de adquirir al menos 12 unidades de aviones caza viene de arrastre de la gestión presidencial de Mauricio Macri y cobró notoriedad cuando en el borrador del presupuesto nacional 2022 figuró una partida de poco más de U$S 600.000.000 millones nominada específicamente para las aeronaves chinas cuando aún no se había realizado la licitación respectiva.

> “La compra de aviones a Estados Unidos demuestra que estamos abiertos a todos los proveedores pero para el caso de aviones de combate no podemos soslayar el hecho de que sufrimos un embargo impuesto por el Reino Unido luego del conflicto Malvinas que impide a muchos países vendernos equipamiento”, recuerdan en el Edificio Libertador.

##### Sobre el Beechcraft Hurón.

Si bien esta es la denominación militar, se trata de la versión modernizada de la aeronave civil Beechcraft Super King con más de 50 años de existencia con sucesivas actualizaciones.

Está provisto de dos motores turbohélice con una potencia de 875 HP cada uno. La tripulación está integrada por piloto y copiloto y admite hasta 13 pasajeros. Posee una longitud de 13 metros y una envergadura ( ancho entre punta de alas) de 16 metros.

Su peso vacío es de 3520 kg. y a plena carga asciende a 5670 Kg. alcanza una velocidad máxima de casi 550 km/h y una velocidad crucero de 536 Km/h. Su techo de vuelo es de 11.000 Mt.

##### Otros proyectos en curso

Desde Defensa también se recordó que en este momento están en curso proyectos de construcción en el país de un buque polar para apoyo a las campañas antárticas y la adquisición de una importante cantidad de vehículos a rueda 8x8 para el Ejército Argentino.

Respecto a estos últimos si bien también hay una oferta firme de China, los altos mandos de la fuerza se inclinan por los vehículos ofrecidos por Brasil fundamentalmente por las prestaciones y la ventaja de contar con motores producidos en el país.