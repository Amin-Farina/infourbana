---
title: Kicillof volvió a extender el plazo para reestructurar la deuda bonaerense
date: 2020-09-14T19:57:00.000+00:00
author: ''
image_webp: "/images/3229449.jpg"
image: "/images/3229449.jpg"
description: El plazo de aceptación de la oferta de reestructuración de deuda por
  US$ 7.148 millones se extiende hasta el 9 de octubre.

---
_El plazo de aceptación de la oferta de reestructuración de deuda por US$ 7.148 millones se extiende hasta el 9 de octubre._

El gobernador de la provincia de Buenos Aires, Axel Kicillof, extendió hasta el 9 de octubre próximo, y por séptima vez, el plazo de aceptación de su oferta de reestructuración de deuda por US$ 7.148 millones, una negociación que será clave para definir el futuro de los bonos que otras once provincias del país buscan renegociar.

"La provincia de Buenos Aires informa que extiende el período de presentación de órdenes para canjear los bonos elegibles por nuevos bonos hasta el próximo 9 de octubre", precisó hoy el Ministerio de Hacienda y Finanzas, a través del sitio web: [https://www.gba.gob.ar/hacienda_y_finanzas](https://www.gba.gob.ar/hacienda_y_finanzas "https://www.gba.gob.ar/hacienda_y_finanzas")

La cartera que conduce Pablo López indicó que, **"en este período, continuará manteniendo conversaciones con los acreedores privados externos para poder alcanzar un acuerdo acorde con la capacidad de pago de la Provincia y que permita recuperar la sostenibilidad de la deuda pública".**

Cerrado el capítulo de la deuda a nivel nacional, el foco pasa ahora por el futuro de las reestructuraciones que encaran una docena de provincias y que, encabezadas por Buenos Aires, buscan canjear más de US$ 13.000 millones con acreedores privados, atentas a las limitaciones que dejó el coronavirus en la recaudación de las arcas provinciales.

El viernes pasado venció la sexta prórroga del plazo de negociación que Buenos Aires inició el 23 de abril, cuando propuso cambiar 23 bonos -casi el 70% de la deuda bruta provincial- por títulos con un período de gracia de tres años, recorte del 55% en intereses y del 7% en capital, además de la extensión de la vida promedio de los bonos a 13 años.

"Tenemos una deuda de US$ 7.000 millones y el 80% vence en los próximos cuatro años. Eso es impagable; entonces, la propuesta que se ha hecho es un perfil de vencimientos que nos permita comprometernos a algo que podamos pagar", aseguró el gobernador Axel Kicillof.

Al mismo tiempo, la provincia de Mendoza también deberá anunciar los resultados de su proceso de negociación por una deuda de casi $ 600 millones que, al igual que Buenos Aires, el plazo venció el viernes.

La propuesta, que quedó cerca la última vez de alcanzar el apoyo para cumplir con las Cláusulas de Acción Colectiva (CAC) del 75%, ofrece a los tenedores un nuevo bono con repago en 2029, un año de gracia, cupón de 4% y tasa reducida hasta 2023.