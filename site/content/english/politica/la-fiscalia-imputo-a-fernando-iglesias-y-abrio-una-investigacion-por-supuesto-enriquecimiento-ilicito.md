+++
author = ""
date = 2021-02-11T03:00:00Z
description = ""
image = "/images/3272498.jpg"
image_webp = "/images/3272498.jpg"
title = "La fiscalía imputó a Fernando Iglesias y abrió una investigación por supuesto enriquecimiento ilícito"

+++

**_El diputado de Juntos por el Cambio fue denunciado por el también legislador Rodolfo Tailhade, integrante del Frente de Todos (FdT)._**

El fiscal federal Jorge Di Lello imputó por presunto enriquecimiento ilícito al diputado de Juntos por el Cambio (JxC) Fernando Iglesias y dispuso las primeras medidas de prueba, tras la denuncia presentada por el también legislador Rodolfo Tailhade, integrante del Frente de Todos (FdT).

Di Lello tiene delegada la investigación por parte del juez federal Sebastián Casanello y además lleva una denuncia similar presentada por Tailhade contra el exsecretario de la presidencia en el Gobierno de Cambiemos, Fernando De Andreis, informaron fuentes judiciales.

En el caso de Iglesias, la fiscalía dispuso que se compulse la información sobre su persona y otros dos investigados en el sistema "Nosis", según el dictamen al que tuvo acceso Télam.

En la denuncia, se alude a declaraciones juradas presentadas por Iglesias ante la Oficina Anticorrupción, en base a las cuales se denunció que "se habría enriquecido patrimonialmente de manera apreciable e injustificada".

Di Lello investigará a Iglesias y a otras dos personas vinculadas con él, Ana Kliauga y José Bocles.

Esta semana la fiscalía tomó la misma medida en la denuncia que investiga a De Andreis.