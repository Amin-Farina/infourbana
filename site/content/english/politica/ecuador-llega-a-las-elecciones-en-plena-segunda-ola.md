+++
author = ""
date = 2021-02-03T03:00:00Z
description = ""
image = "/images/ecuador.jpg"
image_webp = "/images/ecuador.jpg"
title = "Ecuador llega a las elecciones en plena segunda ola"

+++

**_La gestión de la pandemia se presenta como uno de los principales desafíos del próximo gobierno, ante el deficiente sistema sanitario y la corrupción endémica que el brote puso de manifiesto._**

Los ecuatorianos elegirán este domingo a un nuevo presidente en medio de un ascenso exponencial de los casos de coronavirus, que los expertos ya califican como una segunda ola en el país, donde la gestión de la pandemia se presenta como uno de los principales desafíos del próximo Gobierno ante el deficiente sistema sanitario y la corrupción endémica que el brote puso de manifiesto.

Aunque atrás quedaron las escenas de colapso sanitario con cadáveres en las calles que vivió Ecuador en abril pasado, enero fue el mes con mayor cantidad de contagios desde el inicio de la pandemia en el país.

Oficialmente, 38.316 personas resultaron infectadas por Covid-19, un 96% más que las 19.827 de diciembre, mientras que las muertes aumentaron más del 71%, con 825 fallecidos frente a los 573 del mes anterior.

Si bien las autoridades atribuyen este fuerte incremento a la relajación social y al efecto de las fiestas de fin de año, epidemiólogos como la exasesora de la Organización Panamericana de la Salud Catalina Yépez, ya hablan de una "segunda ola" que ha puesto al borde la capacidad hospitalaria en las principales ciudades del país, con casi el 100% de ocupación en terapia intensiva.

"Está colapsado el sistema, ya no hay camas en terapia intensiva. La gente que las necesita está a la espera, dado el aumento de contagios y fallecimientos. Ya es una realidad que está sucediendo", afirmó a Télam el Defensor del Pueblo, Freddy Carrión.

#### Campaña de vacunación y denuncias

En medio de esta alarmante situación, Ecuador inició el pasado 21 de enero la campaña de vacunación contra la Covid-19, con un primer lote de 8.000 dosis del inoculante de Pfizer, destinado a inmunizar al personal médico de primera línea, a los mayores residentes en geriátricos y a sus cuidadores.

En un país de 17 millones de habitantes, solo 4.000 personas resultarán vacunadas en esta etapa inicial, a la espera de las 86.000 dosis de Pfizer que, según la cartera de Salud, llegarán antes de fin de mes para cumplir con el plan piloto de inmunización a 43.000 individuos.

Pero, apenas dos días después del lanzamiento de la campaña de inoculación, surgieron las primeras denuncias por la vacunación de personas no prioritarias, entre ellos familiares del ministro de Salud, Juan Carlos Zevallos.

Su madre y otros parientes fueron inmunizados en una lujosa residencia privada de adultos mayores, un escándalo por el que la Asamblea Legislativa aprobó el inicio de un juicio político en su contra por supuesto manejo discrecional de las vacunas y la Fiscalía lanzó una investigación al respecto.

"Es un claro conflicto de intereses porque él es la autoridad de control en la distribución y entrega vacunas y, tomando en cuenta el número tan insuficiente de dosis, se debe priorizar al personal médico", dijo Carrión, quien también reclamó la destitución de Zeballos y presentó una denuncia en su contra por presuntos delitos de peculado y tráfico de influencias.

Otros casos de vacunados no prioritarios resonaron en los últimos días en el país, algo que el ombudsman atribuyó a la "falta de transparencia" del plan de inmunización que, a su juicio, se presta a una serie de actos de corrupción.

Desde la asunción de Zeballos, en marzo pasado, el Ministerio de Salud fue salpicado por otras polémicas, como la presunta emisión fraudulenta de carnets de discapacidad, por la que está siendo investigada una veintena de funcionarios.

La crisis sanitaria destapó también cientos de irregularidades en la compra de materiales médicos como tapabocas, pruebas de Covid-19 o bolsas para transportar cadáveres, tanto a nivel provincial como municipal.

"En Ecuador estamos ante una pandemia también de corrupción", opinó Carrión y agregó: "Se percibe que la corrupción se encuentra imperando en el país y además está la impunidad, que agrava mucho más la situación".

#### La salud pública

La Covid-19 puso, asimismo, al desnudo las deficiencias del sistema público de salud ecuatoriano, golpeado por las políticas de ajuste del Gobierno de Lenín Moreno, que en 2019 despidió a más de 2.500 funcionarios sanitarios y redujo drásticamente su inversión y presupuesto.

En un país donde sólo una pequeña parte de la población puede pagar los 70.000 dólares que cuesta el tratamiento por coronavirus en hospitales privados, el fortalecimiento del sistema de salud público se impone como uno de los principales desafíos para el futuro mandatario.

Según Carrión, el nuevo Gobierno debería "priorizar" a la salud pública y asignarle un presupuesto adecuado, garantizar una correcta atención con la contratación estable del personal médico, además de ejercer mayores controles en los procesos de compra de insumos para limitar al máximo la corrupción.

"Si tenemos sistemas de salud débiles, una próxima pandemia lo único que generará es mayores casos de vulneración de derechos humanos y, obviamente, será inmanejable", estimó.

#### El protocolo para el domingo

En medio de este crítico contexto, más de 13 millones de ecuatorianos están llamados a votar en los comicios generales del próximo domingo, para los que se dispuso un protocolo sanitario particular.

En un intento de evitar las aglomeraciones, la votación estará repartida en dos turnos: de 7 de la mañana al mediodía para los ciudadanos con documento terminado en número par, y del mediodía hasta las 5 de la tarde para aquellos terminados en impar.

El uso de tapabocas será obligatorio -salvo cuando se pida quitarlo para comprobar la identidad- y estará prohibido ingresar a los centros de votación con acompañantes.

"No sé si el país está preparado para votar en estas condiciones, pero urge un cambio de Gobierno", concluyó Carrión, quien señaló que en el mandato de Moreno la situación de los derechos humanos ha sido "completamente regresiva en todos los niveles y en todos los órdenes".