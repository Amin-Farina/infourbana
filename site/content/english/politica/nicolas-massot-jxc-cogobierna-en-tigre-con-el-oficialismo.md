+++
author = ""
date = 2021-06-10T03:00:00Z
description = ""
draft = true
image = "/images/colorado-yeta.jpg"
image_webp = "/images/colorado-yeta.jpg"
title = "NICOLÁS MASSOT: \"JXC COGOBIERNA EN TIGRE CON EL OFICIALISMO\""

+++

###### Entrevista de LaNoticiaWebTV

**_El exdiputado nacional confirmó que buscará ser candidato en el distrito. Fue duro con  Segundo Cernadas, de quien dijo «tiene un acuerdo con el Ejecutivo local y que no ejerce su rol opositor en el Concejo Deliberante»,  sino en las redes sociales._**

Valoró que en las PASO se puedan dirimir los liderazgos de Juntos por el Cambio, y destacó que este año se ponen en juego dos modelos socio-económicos diferentes en el país.

Nicolás Massot confirmó que buscará competir electoralmente en Tigre. Fue duro con el rol de oposición que se ejerce en la coalición del distrito. También rescató la posibilidad de dirimir en las PASO los liderazgos de Juntos por el Cambio.

##### Fue contundente la foto del jueves en Tres de Febrero, con muchos dirigentes apoyando a Diego Santilli. ¿Qué pensas de esa foto?

Me parece muy bien. El gran desafío que tiene la oposición en general, y Juntos por el Cambio en particular como la expresión mayoritaria de la oposición, es regenerar expectativas dentro de una gran porción del electorado argentino que en 2015 se sintió atraído por nuestra propuesta y por distintos motivos y frustraciones en 2019 no nos volvió a elegir. La mejor manera de poder regenerar expectativas y volver a convocar a esos miles de argentinos es consolidar un rumbo donde todos estemos de acuerdo en Juntos por el Cambio. Un conjunto de políticas públicas que creemos que pueda sacar al país delante de la mano de la inversión y el empleo, pero claramente con distintos liderazgos, distintos estilos.

Las PASO son una buena herramienta para exponerlos, y que cada ciudadano y vecino se sienta convocado a Juntos por el Cambio a través de distintos liderazgos. Me parece natural y creo que le hace bien, no solamente a nuestro espacio sino a la política argentina.

Hemos visto los últimos días afiches con la cara de Emilio Monzó, planteando se deseo de ser Diputado Nacional. Pero también hay deseos de Santilli, los radicales con Manes, Jorge Macri, y Gustavo Posse que nos expresó su diálogo con Monzó.

Efectivamente. Emilio y Gustavo tienen una buena relación y una construcción política en conjunto, y sobre todo una visión compartida respecto a que Juntos por el Cambio debe romper el Status Quo en el cual la provincia de Buenos Aires es variable de ajuste de la Capital Federal. Eso se ha venido expresando. En esa misma línea va la reacción de la UCR nacional al invitar a Facundo Manes a encabezar una propuesta. Más allá de los nombres, lo que es sano que estas discusiones se den a plena luz del día, sin perder nuestra unidad y el rumbo que nos une en políticas públicas, pero animándonos a exponer estos liderazgos y distintos enfoques para constituirnos como alternativa.

##### El expresidente Macri señala que la discusión del 2023 no se debe adelantar, en un mensaje directo a Rodríguez Larreta. ¿Se debe discutir el 2023 y el 2021 juntos, o hay que esperar?

No es tan divisible. Lo que se está discutiendo en Argentina va mucho más allá de algo electoral. Se están discutiendo modelos de organización socio-económica. Hay un modelo preponderante en el oficialismo de hoy día, donde se concibe que el Estado es un mero redistribuidor de lo que hay, aumentando impuestos o con la extracción de excedentes en ciertos sectores, como esta novedad de querer estatizar el sistema de salud buscando únicamente el flujo de fondos de los aportes a las obras sociales. Es un modelo que cree que en esos excedentes y en la suba de impuestos hay un flujo redistributivo para garantizar ingresos universales para todas las personas. No está mal como aspiración, pero es insostenible. Y hay otro modelo que se basa en que la generación de empleo descansa en la inversión del sector privado. El empleo no se genera por ley ni lo genera el estado, porque caemos en la pésima y nociva costumbre de la inflación, que al final termina sacándole a la gente de un bolsillo lo que se le pone en el otro. Nosotros creemos que lo que se juega ahora es precisamente ese modelo que queremos volver a encarnar. Un modelo donde el sector privado, PyMe, y grandes empresas, vuelvan a apostar, a invertir, y mover la maquinaria del empleo formal. Eso se discute cada día, en cada ley, y en esta elección también.

##### Belocopitt habló de su temor a que suceda con el sistema de salud lo mismo que pasó con las AFJP. ¿Ves algo similar?

Por supuesto que hay un punto de contacto. Lo que busca el kirchnerismo es financiación. El país está quebrado y hay un modelo que ahuyenta al empleo y a la inversión, con todo tipo de controles de capitales, de exportación, todo tipo de regulaciones, y la única salida para sostener ese andamiaje y el tejido social de las familias argentinas es aumentar las jubilaciones mínimas, los planes de cooperativas o la AUH. Eso es infinanciable si no se genera crecimiento económico y empleo. El gobierno busca cuál es la caja, el flujo de dinero, que queda disponible, y ve al sistema de salud como algo que es financiado con aporte de los trabajadores y que es una oportunidad. En ese sentido es parecido a las AFJP, con la diferencia de que muchas de ellas estaban a punto de quebrar y no se vio con malos ojos esa jugada. Por el contrario las empresas de salud privada, en general, funcionan bien e invierten porque tienen alguna rentabilidad. Es algo que va a generar mucha mayor resistencia, y vulnera un derecho elemental de toda la ciudadanía que es el acceso a la salud, por la cual mucha gente paga mucho. Creo que va a haber una reacción social que lo va a impedir.

##### Vos hablas del regreso de un modelo de producción y empleo, de 2015 a 2019, pero no hay muchos valores que señalen que ustedes hayan sido exitosos en ese modelo.

Son discusiones distintas. Yo comparto, y mi autocrítica ha sido publica en momentos donde éramos oficialismo, y nunca tuve tapujos en decir las cosas que pensaba. Creo que hay muchísimas cosas que revisar de las que hizo nuestro gobierno. Lo que no es discutible son las herramientas con las que la gente progresa. El único ingreso universal que hay que garantizarle a la gente es el salario. No podemos ir garantizando ingresos universales insustentables. Para garantizar el salario hay que generar empleo. Y para generar empleo hay que garantizar reglas de juego claras a largo plazo para el sector privado. No hay más magia. No quiere decir que nosotros lo hicimos bien y que hay que volver a hacer lo mismo. Quiere decir que hay que revindicar ese rumbo rectificando los errores, pero no caer en la trampa de pensar que hay una vaca lechera que nos da leche para siempre sin hacerla crecer para distribuir. Eso es falso.

##### Estás trabajando políticamente en Tigre, donde está Segundo Cernadas que es el principal referente de Juntos por el Cambio, que preside el Concejo Deliberante. ¿Vas a ser candidato ahora, o trabajas para dentro de 2 años con Emilio Monzó?

Esta diferencia de modelos se discute en cada ley, y también en la calle, permanentemente. Y la caja de resonancia es nuestra Área Metropolitana, donde hay tantos municipios y ciudades, y donde también está Tigre. Soy un convencido de que la militancia del modelo de inversión y empleo hay que hacerla puerta a puerta, comercio a comercio. Vivo en Don Torcuato y quiero colaborar desde ahí y desde Juntos por el Cambio, para poder imponer ese modelo político. En lo local va en el mismo sentido, con preocupaciones locales muy marcadas, con la ambición de darle a Tigre un salto de calidad que no tiene hace tiempo, y sobre todo con ganas de que Juntos por el Cambio asuma su rol de oposición. Que no se quede cómodamente, como parece desde algún tiempo a esta parte, en un acuerdo con un oficialismo local que deja mucho que desear y tiene muchas falencias, envuelto en una discusión intestinal dentro del peronismo y el kirchnerismo, y que está deteniendo la gestión. Juntos por el Cambio, en vez de revisar las acciones del Ejecutivo, discutir responsablemente el destino de los fondos, la transparencia de los actos de gobierno, la rendición de cuentas, ha decidido entregarse al oficialismo de Tigre. Eso es lo que hay que discutir hacia el interior de Juntos por el Cambio.

##### ¿Vos decis que Juntos por el Cambio cogobierna en Tigre?

Sí, es lo que perciben todos los vecinos. Es lo que se ve cuando, lamentablemente, votamos un Presupuesto sin revisar convalidando aumentos importantísimos de tasas municipales, y después salimos por las redes sociales a pedir que se bajen las tasas. La oposición no hay que hacerla en redes sociales, hay que hacerla en el Concejo Deliberante, con altura y responsabilidad, sin poner palos en la rueda, pero revisando la transparencia que deja mucho que desear en los hechos de gobierno. Revisar el destino de los fondos y auditar es el principal rol que tiene una oposición desde el poder legislativo sobre el poder ejecutivo. Esa es la discusión que yo quiero dar en términos locales hacia el interior de Juntos por el Cambio