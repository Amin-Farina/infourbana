+++
author = ""
date = 2021-12-09T03:00:00Z
description = ""
image = "/images/031188dc424bc02bc87a96b223d0c542_xl.jpg"
image_webp = "/images/031188dc424bc02bc87a96b223d0c542_xl.jpg"
title = "FABIOLA YAÑEZ OFRECERÁ COMPRAR UN RESPIRADOR COMO REPARACIÓN POR EL OLIVOS GATE"

+++

#### **_Juan Pablo Fioribello no es sólo el abogado de la Primera Dama en el “Olivos Gate”. Es mucho más. Aunque él lo niegue, se ha constituido en el coordinador legal de las acciones judiciales de toda la familia presidencial._**

Es verdad que firma como el defensor de Fabiola Yañez en la causa de la fiesta de la quinta presidencial. Pero además la ha patrocinado exitosamente en otros temas como agravios personales que recibió de portales de noticias, firma las acciones judiciales del hijo de Alberto Fernández para defenderlo de agresiones homofóbicas y, en silencio, monitorea el accionar de otros colegas en causas en donde el matrimonio Fernández-Yañez aparece.

“Yo voy a proponer una reparación relacionada con la salud. Puede ser la compra de un respirador o el pago de unos días de una internación en terapia intensiva”, le dice Fioribello a Infobae rompiendo un largo silencio sobre el tema.

Yañez se presentará en estos días ante el juez Mirabelli acompañando la estrategia del Presidente que propuso extinguir la causa penal consignando 4 sueldos del poder ejecutivo. La Primera Dama quiere que el eventual acuerdo con la Justicia tenga que ver con la cuestión sanitaria. “Un respirador, el costo de la internación de unos días en terapia intensiva. Lo estamos viendo”, reitera el abogado.

Fioribello cree que luego de las disputas de competencia, la causa está más clara. Señala al pasar, sin embargo, que el proceso penal por el cumpleaños de Fabiola resintió las relaciones personales de los asistentes. “El estado de ánimo de Fabiola Yañez es muy bueno. Está en reposo por su embarazo. Ella está arrepentida de la fiesta. ¿Cómo no va a estar arrepentida?”, explica y agrega: “Sofia Pacchi y Fabiola Yañez hoy no tienen relación. Se conocen desde hace años pero el vínculo de amistad se rompió antes”.

Cabe recordar que Pachi, la todavía asesora presidencial con funciones no claras en Casa de Gobierno, era una amiga íntima de la Primera Dama. Hoy, ya no más. Aunque lo nieguen, el video que las mostraba festejando un nuevo año de Fabiola en medio de la cuarentena más dura dinamitó ese vínculo.

El abogado asegura que no hay privilegios para Alberto o su esposa. “La mayoría de las causas abiertas por violación de las restricciones a la cuarentena se han extinguido con acuerdos legales previstos por el código de reparación integral. El sistema funciona con una propuesta de la defensa que se eleva al fiscal quien, a los efectos de evitar el costo del proceso penal, acepta y lo eleva al juez para que lo homologue. Esta propuesta no es obligatoria. Puede, claro, ser rechazada por el ministerio público o el magistrado actuante.

Fioribello está convencido de alcanzar el éxito: “Tanto el presidente como su mujer están absolutamente de acuerdo. No están tranquilos sino que es así como se han solucionado el resto de las causas”.

¿Se sabe quién filtró las imágenes a la prensa?: “No me corresponde decir quién fue. Sé con certeza cómo fue y quién fue”, afirma convencido.

##### Aquí, las 21 definiciones más importantes de Juan Pablo Fioribello:

“La causa del Olivos Gate está bien. Hay muchas más claridad”.

“Mi vínculo con el Presidente es profesional. Lo asesoro en varias causas”.

“Llego a patrocinar a la primera dama por una recomendación de una persona muy importante de la justicia”.

“Mi trabajo de defensa en el caso de Olivos es sin costo alguno y a disposición del Presidente”.

“El Presidente, ahora, está menos preocupado por la causa porque se empezó a aclarar el tema”.

“Esto (las fiesta de Olivos) fue un error. No se tendría que haber hecho”.

“El Presidente lo reconoció, pidió disculpas y asumió su responsabilidad”.

“Primero se empezó diciendo que había reuniones sexuales. Luego se aclaró que esto era absolutamente falso”.

“El Presidente tiene claro que la fiesta tuvo un costo ante la sociedad”.

“Sofia Pachi y Fabiola Yañez hoy no tienen relación. Se conocen desde hace años pero el vínculo de amistad se rompió antes”.

“El proceso se soluciona con un acuerdo”.

“Estamos tratando de conseguir un acuerdo con el fiscal”.

“Yo voy a proponer una reparación relacionada con la salud. Puede ser la compra de un respirador o el pago de unos días de una internación en terapia intensiva”.

“Tanto el Presidente como su mujer están absolutamente de acuerdo. No están tranquilos sino que es así como se han solucionado el resto de las causas”.

“El estado de ánimo de Fabiola Yañez es muy bueno”.

“Fabiola está arrepentida de la fiesta. ¿Cómo no va a estar arrepentida?”.

“Tenemos claro cómo se filtraron las imágenes. No me corresponde decir quién fue. Sé con certeza cómo fue”.

“Muchos de los que estaban en la fiesta tenían PCR al día y no se generó ningún contagio”.

“No se van a permitir los ataques personales”.

“He recibido ataques personales por defender a la familia presidencial”.