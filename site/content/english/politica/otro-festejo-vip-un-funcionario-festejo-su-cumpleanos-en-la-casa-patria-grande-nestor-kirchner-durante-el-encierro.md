+++
author = ""
date = 2021-11-01T03:00:00Z
description = ""
image = "/images/la-sede-de-casa-patria___wcxntxv4x_1200x630__1.jpg"
image_webp = "/images/la-sede-de-casa-patria___wcxntxv4x_1200x630__1.jpg"
title = "OTRO FESTEJO VIP: UN FUNCIONARIO FESTEJÓ SU CUMPLEAÑOS EN LA \"CASA PATRIA GRANDE NÉSTOR KIRCHNER\" DURANTE EL ENCIERRO "

+++

**_Matías Capeluto es el director ejecutivo de la Casa Patria Grande Néstor Kirchner, un organismo descentralizado bajo la órbita de la Secretaría General que tiene como objetivo “impulsar la integración de los pueblos latinoamericanos” y brindar cursos y seminarios para jóvenes._**

Las oficinas de esta dependencia se encuentran en una pintoresca casona del barrio porteño de Retiro ubicada en Carlos Pellegrini 1289, que el año pasado, en plena cuarentena por coronavirus, fue testigo de una celebración íntima que ahora se encuentra bajo la mira de la Justicia.

El 24 de noviembre de 2020, Capeluto celebró el primer año de su hijo junto a su esposa y otros presuntos familiares en el interior del edificio en donde funciona el organismo que conduce. Un año después de ese festejo, fue denunciado el pasado 21 de octubre por la diputada de la Coalición Cívica ARI, Mónica Frade.

El caso recayó en el juzgado Criminal y Correccional Federal 11 que subroga el juez Julián Ercolini. La instrucción quedó a cargo del fiscal Guillermo Marijuán. Capeluto está acusado de cometer los delitos de peculado, abuso de autoridad e incumplimiento de los deberes de funcionario público.

La diputada, de las más cercanas a Elisa Carrió, pidió que se investigue la utilización de un edificio del Estado como espacio para llevar adelante otra posible fiesta VIP como la del cumpleaños de Fabiola Yáñez en Olivos y como respuesta la Justicia ya tomó una primera medida.

#### Un cumpleaños en medio de la cuarentena por coronavirus

En noviembre de 2020 regía el decreto 875/2020, que prohibía los eventos sociales o familiares en espacios cerrados, además de restringir en ámbitos de trabajo la reunión de personas para momentos de descanso o esparcimiento sin el cumplimiento de los protocolos.

La diputada Frade incluyó en su denuncia una serie de fotos del festejo que fueron reveladas por el diario Perfil. Las imágenes habrían sido publicadas en la cuenta de Instagram del hermano de Capeluto.

Durante la cuarentena, un funcionario festejó el cumpleaños de su hijo en la “Casa Patria Grande Néstor Kirchner” y ahora lo investiga la Justicia

En el primero de los dictámenes de inicio de la causa, el fiscal Marijuán pidió conocer si existen más fotografías. “Se dirige a determinar la responsabilidad penal o no del funcionario”, explicaron fuentes judiciales.

Para Frade, el festejo de Capeluto adquiere una mayor gravedad ya que se realizó en un edificio público. “El denunciado, su cónyuge y hermano sortearon la prohibición, transformando un inmueble del Estado, en casa de fiesta, a la que asistieron personas ajenas (familia). Esa desnaturalización del uso del inmueble fue posible por estar en posesión de éste en razón del cargo que ocupa”, acusó la legisladora en el documento al que accedió este medio.

#### Denuncia contra Capeluto

La diputada explicó los motivos por los que formuló la acusación: “Arrastro una gran preocupación por la cantidad de organismos que el Gobierno ha generado, si bien la Casa Patria Grande ya estaba, sus competencias y objetivos se superponen y esta clase de dependencias terminan siendo bolsas de trabajo que no cumplen ninguna función y no se sabe bien qué hacen”.

> Y continúa: “Cuando tomé conocimiento de este festejo, empecé a investigar y entiendo que esto es solo la punta del iceberg, no se agota acá, es el principio de toda una investigación, que luego ampliaré con más elementos, un final abierto que tiene otras implicancias”.

Capeluto asegura que no fue un festejo de cumpleaños, sino una “salutación espontánea”

Capeluto  acepta que celebró el cumpleaños de su hijo en la Casa Patria Grande Néstor Kirchner, pero afirma que se trató de una “salutación espontánea y breve”.

> El descargo de Capeluto fue: “El día 24 de noviembre de 2020 asistí a mi lugar de trabajo acompañado por mi hijo que ese día cumplía años. En horas de la tarde los compañeros de trabajo ofrecieron una torta para celebrar su aniversario. Se trató de una salutación espontánea y breve, no constituyendo un festejo. Finalizada la misma, se continuó con las tareas programadas. Estas circunstancias fueron puestas en conocimiento de las autoridades de la Secretaría General de la PRESIDENCIA DE LA NACIÓN, las que no merecieron reproche sancionatorio”.