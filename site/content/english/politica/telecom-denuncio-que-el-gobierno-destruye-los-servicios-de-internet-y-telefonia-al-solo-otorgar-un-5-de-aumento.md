+++
author = ""
date = 2020-12-19T03:00:00Z
description = ""
image = "/images/telecom.jpg"
image_webp = ""
title = "Telecom denunció que el gobierno \"destruye\" los servicios de internet y telefonía al solo otorgar un 5% de aumento"

+++
**_La Prestación Básica Universal Obligatoria con un precio base de $150 beneficiará a unos 10 millones de usuarios._**

El gobierno fijó este viernes una tarifa social de telefonía celular desde 150 pesos por mes para población vulnerable y autorizó a las empresas del sector a aplicar un 5% de aumento para el resto de los usuarios. Para el sector que lidera Telecom del Grupo Clarín, implica no solo una fuerte limitación a sus ingresos, sino directamente una destrucción de los servicios de telefonía y de internet.

"El gobierno ha buscado intervenir en la gestión, eliminando incentivos para la innovación y la competencia" al cambiar "drásticamente las reglas de juego" con el decreto que congeló los precios en agosto, dijo Telecom en un crítico comunicado oficial.

> "Con un aumento del 5% para los servicios de telefonía fija, telefonía celular, TV por suscripción e internet, está destruyendo la ecuación económica de las empresas TIC, poniendo en riesgo la calidad de los servicios que prestan, su desarrollo, sus miles de puestos de trabajo directos e indirectos y con consecuencias en muchos casos, irreversibles", agrega el texto.

Desde las empresas, adelantaron este sábado a LPO que tras el anuncio del Gobierno estaban terminando de analizar el impacto en las inversiones y la factibilidad de brindar los servicios a los precios prefijados. Telecom ya advirtió al gobierno por las "oportunidades perdidas" en materia de inversiones dado el cambio en las reglas de juego implementado a partir del decreto 690/20 de fines de agosto que congeló tarifas y declaró al servicio como esencial.

"Si este es el camino que el Gobierno Nacional elige para el sector de la industria TIC, será también el de las oportunidades perdidas", concluye el duro comunicado.

Telecom había anunciado sobre el final del gobierno de Macri una inversión multimillonaria, que con el primer congelamiento dispuesto por la actual gestión redujo. Los 500 millones de dólares de este año se sostenían, sostuvo Roberto Nobile, CEO de Telecom, mientras que los de 2021 y los siguientes años quedaban en suspenso.

En la empresa explican que con 5% de aumento para el grueso de las líneas frente a más de 35% de inflación desde el último aumento en noviembre del año pasado y con una prestación básica de un dólar para cerca de 10 millones de usuarios, la ecuación de rentabilidad se desdibuja.

"Un aumento del 5% no nos permite seguir trabajando como lo hicimos hasta ahora", advirtió Telecom al tiempo que recordó que durante la pandemia no le cortó el servicio a 700 mil usuarios con hasta siete meses de boletas impagas y además proveyó con planes de conectividad inclusiva a 600 mil líneas.

Movistar directamente sostuvo que el aumento otorgado no les permite cubrir los costos operativos. Y desde la cámara argentina de internet (Cabase) ya señalaron que estos precios de servicios ponen en peligro la continuidad de "más de1.200 pymes y cooperativas" que prestan servicios de conectividad a internet en localidades del interior del país. 

Es que la Prestación Básica Universal Obligatoria de $150 que entrará en vigencia el 1° de enero incluye el uso de Whatsapp y SMS ilimitados, ingreso a webs educativas y gubernamentales irrestricto y 550 minutos de llamadas (500 de ellos a teléfonos de la misma compañía). Y la adición de datos tendrá un valor de $200 el mega por mes o $18 diarios, es decir, el mismo precio que entró en vigencia en julio de 2019. Desde entonces la inflación avanzó casi un 70% y ese sería el atraso tarifario con el que comenzaría la PBU.

Actualmente los planes básicos arrancan en $680 o bien un gasto de $20 diarios de datos como mínimo. Con el esquema tarifario aplicado con la PBU, para los usuarios sería un ahorro del orden del 49% respecto de los precios actuales y para las empresas una caída de la facturación del orden de los $4.000 millones por mes solo por la telefonía celular, que se suma al atraso en los precios desde que el gobierno resolvió limitarles los aumentos y declararla servicio básico por decreto.

millones de usuarios.

El jefe de Gabinete, Santiago Cafiero.

El jefe de Gabinete, Santiago Cafiero.	Télam

LPO

19/12/2020

El gobierno fijó este viernes una tarifa social de telefonía celular desde 150 pesos por mes para población vulnerable y autorizó a las empresas del sector a aplicar un 5% de aumento para el resto de los usuarios. Para el sector que lidera Telecom del Grupo Clarín, implica no solo una fuerte limitación a sus ingresos, sino directamente una destrucción de los servicios de telefonía y de internet.

"El gobierno ha buscado intervenir en la gestión, eliminando incentivos para la innovación y la competencia" al cambiar "drásticamente las reglas de juego" con el decreto que congeló los precios en agosto, dijo Telecom en un crítico comunicado oficial.

El oficialismo dictaminó el DNU que congela tarifas de internet y anticipó que regulará los precios 

"Con un aumento del 5% para los servicios de telefonía fija, telefonía celular, TV por suscripción e internet, está destruyendo la ecuación económica de las empresas TIC, poniendo en riesgo la calidad de los servicios que prestan, su desarrollo, sus miles de puestos de trabajo directos e indirectos y con consecuencias en muchos casos, irreversibles", agrega el texto.

Con un aumento del 5% están destruyendo la ecuación económica de las empresas TIC, poniendo en riesgo la calidad de los servicios que prestan, su desarrollo, sus miles de puestos de trabajo directos e indirectos y con consecuencias en muchos casos, irreversibles.

Desde las empresas, adelantaron este sábado a LPO que tras el anuncio del Gobierno estaban terminando de analizar el impacto en las inversiones y la factibilidad de brindar los servicios a los precios prefijados. Telecom ya advirtió al gobierno por las "oportunidades perdidas" en materia de inversiones dado el cambio en las reglas de juego implementado a partir del decreto 690/20 de fines de agosto que congeló tarifas y declaró al servicio como esencial.

"Si este es el camino que el Gobierno Nacional elige para el sector de la industria TIC, será también el de las oportunidades perdidas", concluye el duro comunicado.

Telecom había anunciado sobre el final del gobierno de Macri una inversión multimillonaria, que con el primer congelamiento dispuesto por la actual gestión redujo. Los 500 millones de dólares de este año se sostenían, sostuvo Roberto Nobile, CEO de Telecom, mientras que los de 2021 y los siguientes años quedaban en suspenso.

Roberto Nobile, CEO de Telecom Argentina.

En la empresa explican que con 5% de aumento para el grueso de las líneas frente a más de 35% de inflación desde el último aumento en noviembre del año pasado y con una prestación básica de un dólar para cerca de 10 millones de usuarios, la ecuación de rentabilidad se desdibuja.

"Un aumento del 5% no nos permite seguir trabajando como lo hicimos hasta ahora", advirtió Telecom al tiempo que recordó que durante la pandemia no le cortó el servicio a 700 mil usuarios con hasta siete meses de boletas impagas y además proveyó con planes de conectividad inclusiva a 600 mil líneas.

En la empresa explican que con 5% de aumento para el grueso de las líneas frente a más de 35% de inflación desde el último aumento en noviembre del año pasado y con una prestación básica de un dólar para cerca de 10 millones de usuarios, la ecuación de rentabilidad se desdibuja.

Movistar directamente sostuvo que el aumento otorgado no les permite cubrir los costos operativos. Y desde la cámara argentina de internet (Cabase) ya señalaron que estos precios de servicios ponen en peligro la continuidad de "más de1.200 pymes y cooperativas" que prestan servicios de conectividad a internet en localidades del interior del país.  

Es que la Prestación Básica Universal Obligatoria de $150 que entrará en vigencia el 1° de enero incluye el uso de Whatsapp y SMS ilimitados, ingreso a webs educativas y gubernamentales irrestricto y 550 minutos de llamadas (500 de ellos a teléfonos de la misma compañía). Y la adición de datos tendrá un valor de $200 el mega por mes o $18 diarios, es decir, el mismo precio que entró en vigencia en julio de 2019. Desde entonces la inflación avanzó casi un 70% y ese sería el atraso tarifario con el que comenzaría la PBU.

Actualmente los planes básicos arrancan en $680 o bien un gasto de $20 diarios de datos como mínimo. Con el esquema tarifario aplicado con la PBU, para los usuarios sería un ahorro del orden del 49% respecto de los precios actuales y para las empresas una caída de la facturación del orden de los $4.000 millones por mes solo por la telefonía celular, que se suma al atraso en los precios desde que el gobierno resolvió limitarles los aumentos y declararla servicio básico por decreto.

Alberto Fernández en Olivos con el presidente del Enacom, Claudio Ambrosini.

Para internet domiciliaria fija también habrá una tarifa social del orden de los $700 mensuales a cambio de una cantidad que varía de los 2 a los 10 megas en función de la ubicación geográfica y la cantidad de clientes que posea la empresa prestadora. Frente a una demanda creciente de tráfico de datos por el streaming de entretenimiento (Netflix, Disney), la socialización online (zoom) y el teletrabajo, la reducción de los ingresos reales de las compañías podría redundar en un empeoramiento de la calidad del servicio por una caída en la inversión media por usuario.

Claudio Ambrosini, presidente del Enacom, sostuvo no habría motivos para desincentivar la inversión de las empresas porque, según declaró en la Casa Rosada durante la presentación de la PBU, "apuntamos a que ningún argentino se quede sin servicio y que las compañías siguen invirtiendo en un mercado cada vez más grande" y agregó que, en materia de inversiones, "el foco del Gobierno está puesto en el 5G, y eso lo vamos a lograr trabajando juntos las empresas y el Estado".

Sin embargo, en el sector disienten y hay muestras claras de que las inversiones: Movistar argentina quedó fuera de la compra que Liberty y Millicom hicieron del paquete de activos latinoamericanos que compraron a Telefónica de España. Las dos grandes de las telecomunicaciones no quisieron enterrar inversiones en Argentina. En cambio, actualmente el grupo Olmos estaría interesado en quedarse con la mayoría accionaria mediante una oferta de unos 200 millones de dólares. Como para poner en contexto: la totalidad de Movistar Costa Rica se vendió por 500 millones de dólares este año, y es un mercado de una décima del volumen argentino, aunque con un mayor PBI per cápita.