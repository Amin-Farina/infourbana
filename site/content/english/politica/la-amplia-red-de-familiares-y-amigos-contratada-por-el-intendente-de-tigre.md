+++
author = ""
date = 2021-03-26T03:00:00Z
description = ""
image = "/images/zamora_nepotismo-1.png"
image_webp = "/images/zamora_nepotismo.png"
title = "LA AMPLIA RED DE FAMILIARES Y AMIGOS CONTRATADA POR EL INTENDENTE DE TIGRE"

+++

**_El municipio gasta millones en pagos a allegados del intendente Julio Zamora. Al mismo tiempo, se recortan salarios de los empleados y se despiden a trabajadores sin previo aviso. La corrupción de los "barones de Benavídez" no tiene fin._**

Tras los escándalos de corrupción, contrataciones fraudulentas, vacunaciones VIP y cargos regalados, Julio Zamora perdió el timón de la gestión. Su mano derecha y amigo, Pedro Heyde, es quien está involucrado en el último escándalo de compras ilegales a los amigos y ex funcionarios de la gestión, como fue el caso de la empresa fantasma de pinturas del ex secretario de Prensa, Fernando Campdepadrós (imputado por casos de abuso, acoso y alcohol al volante).

En ese marco, desde el municipio intentaron dar una tibia defensa, argumentando que se trataba de una operación en su contra, pero ante las pruebas físicas que salieron a la luz, tuvieron que dar marcha tras y llamarse al silencio.

En mayo del 2020, se dieron a conocer los sueldos de los familiares del intendente de Tigre, a raíz del accionar del municipio de recortar sueldos y despedir al personal sin previo aviso en plena pandemia. El clan se compone de:

* Mario Zamora (hermano del intendente. Secretario de Gobierno, Control Urbano y Recursos Humanos).
* Graciela Basso (esposa de Mario Zamora. Directora de Familia y Niñez).
* Gisela Hortazo de Zamora (esposa de Julio Zamora. Concejal del Bloque. Directora del Instituto de Alimentación y encargada de las Huertas Municipales).
* Andrés Pérez (yerno de Julio Zamora. Director de Mesa de Entradas).
* Magali Zamora (hija de Julio Zamora. Trabaja en Recursos Humanos).
* Gabriela Zamora (hija de Julio Zamora. Trabaja en el municipio).
* Damián Roche (Yerno de Julio Zamora y director de Prensa y fotografía).

En esta segunda investigación, el árbol genealógico se amplía, sumando a los Lima.

* Héctor “Chingolo” Lima, amigo íntimo de Julio Zamora y presidente del PJ de Tigre, cobra un sueldo en el Honorable Concejo Deliberante de Tigre, pero además sus hijos, las novias y exnovias de sus hijos, hijas, hermanos, cuñadas, todos ellos perciben un sueldo del municipio.
* Lima Jose Maria (hijo de Chingolo, secretario de Privada de la Unidad Intendente).
* Lima Héctor (padre y además miembro del PJ y empleado del HCD).
* Lima María Jose (es la hermana y directora general de Deportes).
* Lima Ricardo (tío y director General de Redes Urbanas).
* Lima Facundo (primo y empleado de Redes Urbanas).
* Lima Esteban (hermano y empleado de la delegación Talar).
* Lima Luis (hermano y empleado de un polideportivo).
* Lima Sofía (prima y empleada de Redes Urbanas).
* Bauza Anabela (novia y empleada de Ceremonial).
* Dorrego Anabella (ex mujer y empleada del Concejo Deliberante).
* Dorrego Matías (ex cuñado y chofer del intendente).

Este nepotismo le cuesta al municipio, por mes, alrededor de **3 millones de pesos, y por año, más de 30 millones en sueldos de cargos regalados y familiares que no siempre hacen ejercicio de sus funciones.**

Lo que indigna es que con fondos municipales se mantiene a todo el clan Zamora, Lima y familia política de ambos. Además, Héctor Chingolo Lima, percibe con fondos del municipio de Tigre, la suma de 350 mil pesos mensuales, en concepto de fondos para el PJ local.

¿Son competentes todos estos familiares y amigos para ocupar todos los cargos? ¿Corresponde que con los impuestos que pagan los vecinos se destine a pagar sueldos de familiares? ¿Cuándo será que el Honorable Concejo Deliberante pida explicaciones sobre el uso de los fondos públicos? ¿Dónde está la oposición? ¿O es que también tienen contratos que compran su silencio?

La comuna tiene muchas explicaciones que darle a los vecinos de Tigre