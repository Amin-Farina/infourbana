+++
author = ""
date = 2022-05-16T03:00:00Z
description = ""
image = "/images/foto-fiesta-alberto.jpg"
image_webp = "/images/foto-fiesta-alberto.jpg"
title = "EL FISCAL APROBÓ LA DONACIÓN OFRECIDA POR ALBERTO FERNÁNDEZ DE $1.6 MILLÓN PARA CERRAR LA CAUSA DE OLIVOS Y PROPUSO QUE SEA PARA EL MALBRÁN"

+++
#### Ahora el acuerdo debe ser homologado por el juez federal Lino Mirabelli. El Presidente evalúa sacar un crédito para pagar esa suma. En realidad serían 3 millones porque Fabiola Yañez propuso una donación de $1,4 millón y no tiene trabajo.

El fiscal federal de San Isidro Fernando Dominguez aceptó hoy la donación ofrecida por el presidente Alberto Fernández de $1,6 millón para cerrar la causa por la Fiesta en Olivos. De esa manera, ahora solo resta que el juez federal Lino Mirabelli homologue el acuerdo para que el Presidente termine sobreído.

“Considero razonable la propuesta efectuada”, sostuvo el fiscal. Y agregó que el monto ofrecido “cubriría el precio de un respirador para ser utilizado en módulos de atención UCI de pacientes con COVID 19, o un total de cuarenta días de internación en modulo de atención UCI con ARM de paciente con COVID 19″.

El fiscal Dominguez propuso además que se destine el dinero a la Administración Nacional de Laboratorios e Institutos de Salud Dr. Carlos G. Malbrán. “No es para que el Malbrán compre un respirador o solvente una internación, puede aplicarlo a sus tareas”, explicó una fuente judicial.

Desde que el juez homologue el acuerdo, el presidente tendrá un plazo de diez días hábiles para depositar ese dinero en una cuenta bancaria, tal como se comprometió en la presentación que hizo la semana pasada. Tal como adelantó ayer Infobae, Alberto Fernández evalúa pedir un crédito porque no tendría ese dinero en efectivo. Otra opción sería vender algún bien a su nombre, pero eso llevaría mucho más tiempo.

La donación en realidad es una “reparación económica” prevista en el artículo 59 inciso 6 del Código Penal. De esa manera, se evita la indagatoria por violar las restricciones a la circulación que él mismo había firmado.

En las últimas semanas, varios de los abogados que figuran en la causa desfilaron por la Fiscalía para intentar llegar a un acuerdo. En representación del Presidente, fueron los dos abogados designados, Marcelo Antonio Sgro y Fabián Musso. La cifra fue bajando hasta llegar a $1,6 millón, muy por encima del primer ofrecimiento que había hecho el Presidente en agosto del año pasado, cuando propuso donar la mitad de su sueldo durante cuatro meses al Instituto Malbrán.

Como el Código Penal no prevé ningún parámetro, se tuvo como referencia un informe del Ministerio de Salud de la Nación sobre el costo de los respiradores contra el COVID-19. “La cifra variaba entre 6 mil y 11 mil dólares”, explicó una fuente judicial.

El fiscal Dominguez también aceptó la donación de $1,4 millón ofrecida por la primera dama Fabiola Yañez, defendida por Juan Pablo Fioribello y Mariano Lizardo.

El monto de esa reparación fue más complejo porque no cobra un sueldo fijo. ¿Quién la pagará?. La presentación ante el fiscal Dominguez solo habla de que contará con una “ayuda económica familiar”. En los hechos, deberá hacerse cargo el Presidente.

De esa manera, se trata de una donación total de $3 millones. Aunque se llegue a un acuerdo para pagarlo en cuotas, se trata de una suma que debe estar justificada en el patrimonio de Alberto Fernández.

La propuesta del Presidente solo había sido objetado por un abogado, Alejandro Sarubbi Benitez, el mismo que denunció la Fiesta en Olivos ante la Justicia de San Isidro. “El ofrecido es dinero obtenido de la función pública, y resulta por demás irrazonable que se ofrezcan a reparar económicamente sus delitos con dinero obtenido del bolsillo de los propios damnificados”, sostuvo. Sin embargo, tal como adelantó este medio, el desenlace estaría sellado.

Con la aprobación del fiscal, solo resta que el juez Lino Mirabelli homologue el acuerdo y decida qué hospital público recibirá el dinero. Unas horas después, el Presidente y la primera dama quedarán sobreseídos por unos de los mayores escándalos de su gestión.

El Fiscal también debe contestar los ofrecimientos realizados por otros tres imputados: Fernando Consagra, Emmanuel López y Santiago Basavilbaso. La semana pasada actualizaron sus propuestas y ofrecieron montos cercanos a los $250 mil. Para justificarlo tuvieron que presentar declaraciones juradas y hasta contratos de alquiler. No ocurrió lo mismo con Alberto Fernández ni con la primera dama.

El resto de los imputados todavía no hicieron una presentación. En el caso del colorista Federico Abraham, sus abogados, el ex camarista federal Jorge Ballestero y Pablo Slonimsqui, harán un ofrecimiento en los próximos días, pudo saber este medio. En cambio, el abogado de Stefanía Dominguez, Mauricio D’Alessandro, seguirá batallando en Tribunales para intentar conseguir una declaración de inconstitucionalidad de los decretos firmados por el Presidente. El resto de los imputados son defendidos por el estudio de Fernando Burlando. Si no hacen un ofrecimiento en los próximos días, podrían terminar yendo a indagatoria.