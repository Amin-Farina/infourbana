+++
author = ""
date = 2022-02-02T03:00:00Z
description = ""
image = "/images/303497-af-20chi-1.jpeg"
image_webp = "/images/303497-af-20chi.jpeg"
title = "EL PRESIDENTE INICIA UNA GIRA CLAVE QUE INCLUYE ENCUENTROS CON PUTIN Y XI JINPING"

+++

##### **_El presidente Alberto Fernández arribará hoy a las 14 a Moscú, en el inicio de su gira por Rusia, China y Barbados, que incluye reuniones con sus pares Vladimir Putin y Xi Jinping y con la primera ministra del país caribeño, Mia Mottley._**

Fernández inició el viaje a las 22.30 desde el Aeropuerto Internacional de Ezeiza rumbo a Moscú en un vuelo de Aerolíneas Argentinas.

El primer mandatario viaja acompañado por el canciller Santiago Cafiero; la gobernadora de Río Negro, Arabela Carreras; los gobernadores de Buenos Aires, Axel Kicillof, y de Catamarca, Raúl Jalil; la portavoz de la Presidencia, Gabriela Cerruti; la secretaria de Deportes, Inés Arrondo; el senador Adolfo Rodríguez Saá, el diputado Eduardo Valdés, la asesora presidencial Cecilia Nicolini, y el intendente de José C. Paz, Mario Ishii.

Además, la comitiva a Rusia estará integrada por el ministro de Economía, Martín Guzmán.

##### Toda la actividad

"La primera actividad oficial del viaje será una reunión privada seguida por un almuerzo de trabajo con Putin en el Kremlin el jueves a las 7 De la Argentina. El viernes Fernández asistirá a la inauguración de los Juegos Olímpicos de Invierno de Beijing, mientras que el domingo mantendrá un encuentro con su par chino, Xi Jinping", se explicó.

La gira finalizará el martes 8 de febrero en Barbados, donde Fernández se reunirá con la primera ministra y con representantes de la Organización de Estados del Caribe Oriental (OECS).

Tras el encuentro con Putin, ambos mandatarios brindarán una declaración conjunta a la prensa, de acuerdo a lo programado.

La Federación Rusa hizo un acuerdo temprano con la Argentina para proveer vacunas Sputnik V, con lo cual el país empezó a aplicarlas en diciembre del 2020.

Tras el encuentro entre los jefes de Estado, Fernández partirá a Beijing a las 18.30 (12.30 de la Argentina), adonde viaja invitado especialmente por Xi Jinping para asistir a la inauguración de los Juegos Olímpicos de Invierno.

El viernes, el Presidente participará a las 15 (4 de la Argentina), de forma virtual, de la ceremonia en la que le será otorgado el título de Profesor Honoris Causa de la Universidad de Tsinghua.

Seguidamente, a las 7.30 de la Argentina, visitará el Museo del Partido Comunista Chino, donde hará entrega de una ofrenda floral, realizará una recorrida guiada y firmará el libro de visitantes ilustres.

Luego, a las 9 hora argentina y junto al embajador en China, Sabino Vaca Narvaja, el Presidente presenciará la ceremonia inaugural de los Juegos Olímpicos de Invierno que se desarrollará en el Estadio Nacional de Beijing, conocido como El Nido.

Ambos países firmaron en el 2014 un Tratado de Asociación Estratégica integral, y China tiene inversiones en infraestructura y otras áreas en varias provincias de la Argentina.

El sábado, a las 22.30 de la Argentina, Fernández visitará el Museo del Palacio, ubicado en la llamada Ciudad Prohibida, un complejo de casi mil edificios construido a principios del siglo XV y que hasta comienzos del siglo XX fue el centro político de China y la residencia de los emperadores.

Después, de acuerdo a la agenda, a tan solo 1500 metros de allí, en la Plaza de Tiananmen, colocará una ofrenda floral en el mausoleo de Mao Zedong.

Más tarde, a la 1.15 del domingo en la Argentina, el Presidente será recibido en el Gran Salón del Pueblo, donde se ofrecerá un almuerzo en honor a las jefas y los jefes de Estado que concurran a la inauguración de los JJ OO, y a las 8.15 se reunirá con la delegación olímpica argentina.

El encuentro entre Fernández y Xi Jinping se desarrollará el domingo, a las 0.40 de la Argentina (11.40 hora de China), en el Gran Salón del Pueblo.

La actividad oficial en el país asiático finalizará ese mismo día con visitas al Centro Tecnológico Huawei y a la gran Gran Muralla de Mutianyu -a poco más de 70 kilómetros de Beijing-, previo a tomar un vuelo que llevará al jefe de Estado y su comitiva a Bridgetown, Barbados, en las Antillas menores, en el Mar Caribe.

El encuentro con la primera ministra Mia Mottley tendrá lugar el martes, a las 8 de la Argentina, y será seguido de una reunión con representantes de la Organización de Estados del Caribe Oriental (OECS, por su sigla en inglés).

La isla caribeña, después de siglos de influencia británica -que incluyen más de 200 años de esclavitud, hasta 1834- logró la independencia definitiva en 1966 y, finalmente, el 30 de noviembre del año pasado se convirtió en una república parlamentaria independiente.