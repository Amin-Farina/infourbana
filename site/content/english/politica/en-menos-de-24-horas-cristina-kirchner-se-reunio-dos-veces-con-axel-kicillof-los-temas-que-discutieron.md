+++
author = ""
date = 2021-12-17T03:00:00Z
description = ""
image = "/images/axel-kicillof-y-cristina-kirchner-959579-1.jpg"
image_webp = "/images/axel-kicillof-y-cristina-kirchner-959579.jpg"
title = "EN MENOS DE 24 HORAS, CRISTINA KIRCHNER SE REUNIÓ DOS VECES CON AXEL KICILLOF: LOS TEMAS QUE DISCUTIERON"

+++

#### **_El gobernador bonaerense, Axel Kicillof, se reunió con la vicepresidenta Cristina Kirchner este miércoles. Fue un encuentro reservado en su domicilio del barrio porteño de Recoleta. En menos de 24 horas, ambos dirigentes volvieron a verse las caras; pero esta vez en el Senado de la Nación._**

Las reuniones de trabajo se dieron en medio de negociaciones con el Fondo Monetario Internacional, a horas de la discusión del Presupuesto nacional y en la previa del debate del presupuesto bonaerense. También en el marco de los cambios que el gobernador viene implementando en su gabinete. Los encuentros tiene una lectura política.

Desde aquel viaje relámpago de Kicillof a El Calafate luego de la derrota en las elecciones Primarias Abiertas Simultáneas y Obligatorias, el gobernador cambió parte su equipo de trabajo de más cercano. Llegaron los intendentes de Lomas de Zamora, Martín Insaurralde, a la jefatura de Gabinete por Carlos Bianco, y del intendente de Malvinas Argentinas, Leonardo Nardini, al Ministerio de Infraestructura y Servicios Públicos en lugar de Agustín Simone. Tanto Bianco como Simone fueron designados en otras áreas de menor exposición. Los cambios, no deseados por el mandatario, abrieron un nuevo escenario y distribución de poder en el esquema bonaerense, donde hubo un empuje preciso de Máximo Kirchner, que además este sábado asumirá al frente del PJ bonaerense.

Pero hoy por hoy, Cristina -o el Instituto Patria- no es exactamente lo mismo que Máximo Kirchner y La Cámpora. Balanceando en medio de todo eso está Kicillof, que gobierna la provincia de mayor peso político, que mantiene distancia con algunas decisiones del gobierno nacional y que tiene diálogo directo con la Vicepresidenta.

Cristina Kirchner busca en Kicillof su opinión sobre cómo se debe abordar la negociación con el Fondo Monetario Internacional (FMI). La diferencia con el presidente Alberto Fernández se evidenció el último 10 de diciembre ante una multitud en Plaza de Mayo. Allí la Vicepresidenta advirtió que “no se va a aprobar ningún plan que no permita la recuperación”. También le pidió a Fernández que incluya en la negociación la posibilidad de pagar parte de la deuda con dólares argentinos que están en paraísos fiscales: “A la Argentina no le faltan dólares, los dólares de la Argentina se los llevaron afuera. Necesitamos que el FMI nos ayude a recuperar de los paraísos fiscales que se han ido miles y millones de dólares en evasión. Comprométase a que cada dólar que encuentre en el exterior se lo vamos a dar al Fondo, de los que se la llevaron sin pagar impuestos, se la fugaron. Que sea un punto de negociación”. Fue el mismo día en que el Fondo aseguró que las conversaciones estaban avanzadas

Seguidamente, fue el Presidente frente a la misma multitud quien le contestó a la vicepresidenta: “Tranquila Cristina, no vamos a negociar nada que ponga en compromiso el crecimiento de la Argentina”. El modo de abordar la negociación de la deuda no muestra uniformidad dentro del Frente de Todos. En este contexto, el gobernador bonaerense y ministro de Economía de la Nación durante la presidencia de Cristina Kirchner se posiciona bien cercano a la ida de la ex mandataria.

Kicillof aprovechó ciertas actividades que tuvo en la Ciudad para reunirse con la Vicepresidenta. Este jueves participó del cierre del seminario “La ciencia ante la Covid-19″, organizado por la Facultad de Ciencias Exactas y Naturales (FCEyN) de la Universidad de Buenos Aires. Fue en el aula magna de Ciudad Universitaria, en el marco de un panel moderado por la periodista Nora Bär e integrado por la asesora presidencial Cecilia Nicolini y los científicos Guillermo Durán, Diego Garbervetsky, Juliana Cassataro, Rodrigo Castro, Daniel Feierstein y Jorge Geffner.

En las charlas también repasaron el relanzamiento que el mandatario realizó de su gestión la semana pasada que incluye la creación de algunas áreas con dirigentes del agrado de la vicepresidenta como el desembarco de Alberto Sileoni al frente de la cartera educativa o de la diputada nacional y dirigente de La Cámpora, Daniela Vilar como futura ministra de Ambiente bonaerense o de Florencia Saintout al frente del Instituto Cultural de la provincia.