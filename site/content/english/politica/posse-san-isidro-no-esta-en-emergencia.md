+++
author = ""
date = 2021-05-03T03:00:00Z
description = ""
image = "/images/gustavo-posse-en-la-entrevista-con-jorge-fontevecchia-1060703.jpg"
image_webp = "/images/gustavo-posse-en-la-entrevista-con-jorge-fontevecchia-1060703.jpg"
title = "POSSE: \"SAN ISIDRO NO ESTÁ EN EMERGENCIA\""

+++

**_Gustavo Posse, intendente de San Isidro, analizó la situación epidemiológica de su distrito y del AMBA._**

En torno a esto, señaló que “nuestro problema, a diferencia del año pasado, es que antes nosotros conseguíamos nivelar a un paciente dentro de un nosocomio municipal y después referenciarlo en el centro que le daba su prepaga, llevarlo a una clínica privada. 

Hoy es cierto que no hay camas privadas en toda el Área Metropolitana. Entonces eso cambia nuestra manera de ver dónde está el borde”.

En declaraciones radiales, el jefe comunal sostuvo que “hoy lo preocupante es, sobre todo en el sistema privado, la cantidad de personas que llegan de manera tarde. Personas 10 años promedio más joven que llegan más tarde al centro de atención privada y obviamente van a tardar mucho más tiempo en recuperarse y en alguno de los casos no se van a recuperar”.

“San Isidro no está en emergencia. Tomamos la precaución con muchísima anterioridad de capacitar personas para las unidades de terapia intensiva. Convertir, pensando en el rebrote de covid, a los hospitales principales y llevar a los centros de salud primarios la atención de medicina asistencial u otras especialidades”, subrayó.

Por último, Posse dijo estar “muy agradecido por quienes sostienen esta posición respecto a los cuidados y tenemos que seguir en esa línea. El coronavirus no tiene sector social, va para todo el mundo”