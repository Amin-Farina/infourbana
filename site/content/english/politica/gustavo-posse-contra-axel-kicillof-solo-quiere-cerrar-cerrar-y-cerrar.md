+++
author = ""
date = 2021-04-21T03:00:00Z
description = ""
image = "/images/0d4be46703e4caec7901c928d2f5e122_l.jpg"
image_webp = "/images/0d4be46703e4caec7901c928d2f5e122_l.jpg"
title = "GUSTAVO POSSE CONTRA AXEL KICILLOF: \"SOLO QUIERE CERRAR, CERRAR Y CERRAR"

+++

Gustavo Posse, intendente de San Isidro, hizo una presentación ante la Justicia para que los chicos vuelvan a las aulas en su ciudad. A su vez, cargó contra el gobernador de la provincia de Buenos Aires, Axel Kicillof, del que dijo que solo quiere «cerrar, cerrar y cerrar«, principalmente en AMBA. Además, comentó que en San Isidro solo tienen un 21% de positivos en los hisopados que hacen.

Indicó en diálogo con CNN Radio que Axel Kicillof no lleva mal el tema sanitario, teniendo en cuenta las condiciones en las que está el mundo. «Pero él tiene una apreciación desde hace mucho tiempo que es la de cerrar, cerrar y cerrar, sobre todo el AMBA. Nosotros en San Isidro llevamos a cabo una práctica que es la de hisopar de forma sumamente masiva. Tenemos 21 centros que hisopan todos los días en la ciudad», manifestó.

«Buscamos tener mucha cantidad de positivos. Normalmente el país está en un 43% de positivos respecto de los hisopados y nosotros estamos en el 21%. De esa cantidad, esperamos poder dirigir la acción de cada una de esas personas en el sentido de que se quede en su casa para que no puedan contagiar», indicó. Expresó que el resultado que obtienen es que «aunque busquemos muchas personas infectadas, tenemos pocas personas infectadas y poca cantidad de muertes cada 100 mil habitantes».

> “No hay contagio en las aulas. Entiendo la situación sanitaria. El asunto es que no se puede dejar de vivir, trabajar y estudiar”, expresó el jefe comunal. «El porcentaje de contagio en las escuelas es de un 0.12% o 0.14%. Presentamos protocolos para que podamos hacer un seguimiento, junto con los directivos de las escuelas, de las burbujas», indicó.

En este sentido, Posse consideró que “ayudaría muchísimo que se diese la información que después de los 14 días vuelven las clases” y no se extiende la medida sobre la prohibición de la presencialidad escolar en territorio bonaerense. “La gente tiene miedo de que sean más de 14 días”, acotó el intendente de San Isidro, quien comentó que, al igual que en la ciudad de Buenos Aires, hizo una presentación ante la Justicia para que los chicos vuelvan a las aulas.