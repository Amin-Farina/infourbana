+++
author = ""
date = 2022-01-04T03:00:00Z
description = ""
image = "/images/1119976947_0_29_2849_1631_1920x0_80_0_0_465f4c344bffb994490e3545f6ab8756-1.jpg"
image_webp = "/images/1119976947_0_29_2849_1631_1920x0_80_0_0_465f4c344bffb994490e3545f6ab8756.jpg"
title = "UN DOCUMENTO REVELA QUE REINO UNIDO MOVILIZÓ 31 ARMAS NUCLEARES EN GUERRA  DE MALVINAS"

+++

#### **_En caso de confirmarse la veracidad de archivos desclasificados sobre el envío de buques con 31 armas nucleares a la isla durante el conflicto bélico de 1982, Cancillería anticipó que planteará la "preocupante" situación ante los organismos internacionales._**

La Cancillería adelantó este miércoles que el Gobierno argentino reiterará su reclamo al Reino Unido, en caso de confirmarse la veracidad de archivos desclasificados sobre el envío de buques con 31 armas nucleares a Malvinas durante el conflicto bélico de 1982, como parte de la política contraría a la proliferación de pertrechos bélicos atómicos que sigue el país, y también anticipó que planteará la "preocupante" situación ante los organismos internacionales.

"De confirmarse la existencia de archivos desclasificados que aporten mayores detalles respecto de la gravedad de los hechos difundidos en medios de prensa, por la magnitud y circunstancias que se hubieran revelado, el Gobierno argentino reiterará su reclamo al gobierno del Reino Unido y en el marco de su invariable política contraria a las armas nucleares así como respecto de su uso, prevé plantear esta situación ante los organismos internacionales competentes", indica un comunicado emitido por la Cancillería.

En ese plano, la Cancillería argentina destaca, "en relación con la reciente información publicada en el portal Declassified UK sobre el envío de buques británicos con 31 armas nucleares al conflicto del Atlántico Sur, que en el año 2003 el Ministerio de Defensa británico publicó un informe en el que se mencionaba que la fuerza de tarea que se constituyó para ir al Atlántico Sur durante el conflicto de 1982 incluyó navíos equipados con armamento nuclear".

En ese momento, el Reino Unido negó que hubiera violado el Tratado de Tlatelolco de no proliferación nuclear y que todas las armas regresaron al suelo británico "en buen estado".

Recuerda que la Cancillería "remitió en 2003 una nota de protesta al Reino Unido manifestando la suma gravedad de la situación y requiriendo precisas y completas informaciones sobre los distintos aspectos involucrados en los hechos revelados".

Al respecto, hace hincapié que "en particular, que se asegure que en forma fehaciente no hay armas nucleares en ningún lugar del Atlántico Sur, ni en buques hundidos, el lecho del mar ó bajo ninguna otra forma ni circunstancia".

Sobre este punto, el Palacio San Martín señaló que "en ese momento, el Reino Unido negó que hubiera violado el Tratado de Tlatelolco y que todas las armas regresaron al Reino Unido en buen estado".

"Pese a la reticencia del Reino Unido a brindar información detallada al respecto, nuestro país expresó en diversas oportunidades su preocupación ante distintos foros internacionales sobre la posibilidad, confirmada en 2003, de que el Reino Unido hubiera introducido armamento nuclear en el Atlántico Sur", concluye el comunicado oficial.