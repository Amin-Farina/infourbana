+++
author = ""
date = 2021-07-27T20:00:00Z
description = ""
draft = true
image = "/images/whatsapp-image-2021-07-27-at-15-50-45.jpeg"
image_webp = "/images/whatsapp-image-2021-07-27-at-15-50-45.jpeg"
title = "NICOLAS MASSOT Y EL MASSISMO BUSCAN IMPEDIR QUE SEGUNDO CERNADAS SEA CANDIDATO EN TIGRE"

+++
**_Operadores judiciales trabajan para evitar que el candidato de Juntos en Tigre participe de las elecciones. Las dificultades del ex diputado cordobés para lograr un buen resultado electoral refuerzan la estrategia de impedir por otros medios la candidatura de Cernadas._**

La legislación que limita las reelecciones es clara y le permite a Segundo Cernadas renovar su banca de concejal que obtuvo en 2017 cuando ganó en Tigre. Si resulta electo este año el próximo será el segundo mandato. La operación judicial que busca perjudicarlo no tiene en cuenta que Cernadas nunca cumplió el plazo mínimo establecido para computar un mandato por lo tanto solo cumplió hasta ahora con el mandato que comenzó en 2017 y puede volver a ser candidato.

Segundo Cernadas renunció el 13 de diciembre de 2016 un año antes de lo establecido por la norma para ser considerado como el primer período computable. El decreto reglamentario establece en su artículo primero que la prohibición es para quienes “_hayan asumido sus funciones y ejercido por mas de dos años continuos o alternados_”.

La presentación ante la justicia electoral fue realizada por un funcionario municipal de Tigre vinculado al massismo como integrante de sus listas como la del año 2013. [**_https://www.cronista.com/economiapolitica/Massa-presenta-a-sus-candidatos-con-un-acto-en-Tigre-20130708-0104.html_**](https://www.cronista.com/economiapolitica/Massa-presenta-a-sus-candidatos-con-un-acto-en-Tigre-20130708-0104.html "https://www.cronista.com/economiapolitica/Massa-presenta-a-sus-candidatos-con-un-acto-en-Tigre-20130708-0104.html")

Desde hace días circula el entendimiento entre el ex diputado cordobés de Juntos por el cambio Nicolás Massot y el actual presidente de la Cámara de Diputados Sergio Massa del que dieron cuenta diferentes medios como [**_infobae_**](https://www.infobae.com/politica/2019/08/13/sergio-massa-a-partir-del-11-de-diciembre-tenemos-que-trabajar-con-dirigentes-como-lavagna-monzo-y-massot/), [**_Hora 60_**](http://www.hora60.com/massot-el-verdadero-plan-de-massa-en-tigre/) y [**_la Voz de Córdoba_**](https://www.lavoz.com.ar/politica/massa-pidio-por-massot-pero-fernandez-parece-ponerle-un-freno/).

[“**_A partir del 11 de diciembre tenemos que trabajar con Nicolás Massot y Emilio Monzó_**”](https://www.lavoz.com.ar/politica/massa-pidio-por-massot-pero-fernandez-parece-ponerle-un-freno/) fue la frase citada por **La Voz del Interior** de Córdoba que pronunció Sergio Massa apenas cuarenta y ocho horas después de las Paso y luego de almorzar en Nueva York con el ex diputado cordobés Nicolás Massot con quienes son amigos. (link)