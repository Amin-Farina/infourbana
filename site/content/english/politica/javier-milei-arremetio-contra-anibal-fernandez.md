+++
author = ""
date = 2021-10-12T03:00:00Z
description = ""
image = "/images/x64mtuzmvnhuzm2qxzgvxutc6q.jpg"
image_webp = "/images/x64mtuzmvnhuzm2qxzgvxutc6q.jpg"
title = "JAVIER MILEI ARREMETIÓ CONTRA ANÍBAL FERNÁNDEZ"

+++

##### **_El candidato a diputado nacional sostuvo que el presidente Alberto Fernández le "debería pedir la renuncia" al ministro de Seguridad por su ataque contra el humorista gráfico Nik. Además cuestionó a los dirigentes de Juntos por el Cambio que mantienen canales de diálogo con sectores kirchneristas._**

El candidato a diputado nacional Javier Milei sostuvo que el presidente Alberto Fernández le "debería pedir la renuncia" a Aníbal Fernández por su ataque contra el humorista gráfico Nik, y recordó que el ministro de Seguridad "tiene la costumbre de utilizar información personal para perseguir" a sus críticos. Además alabó nuevamente a Patricia Bullrich y cuestionó a los dirigentes de Juntos por el Cambio, como María Eugenia Vidal, que mantienen canales de diálogo con sectores ultrakirchneristas. 

Según Javier Milei, el virulento posteo de Aníbal Fernández contra Nik en la red social Twitter fue "verdaderamente aberrante" y muestra "cómo los políticos utilizan el Estado y su monopolio de la fuerza para perseguir a los particulares". En ese sentido, y al igual que otros dirigentes de la oposición, subrayó que "el presidente debería haberle pedido ya la renuncia por esta barbaridad".

Asimismo, señaló anoche en declaraciones al canal TN que tanto Aníbal Fernández como otros funcionarios del Frente de Todos "tienen esta costumbre de utilizar información personal para perseguir, lo cual no es nuevo".

En Twitter, el dibujante Nik expresó su malestar por los últimos anuncios del Gobierno en materia económica: "Regalar heladeras, garrafas, viajes de egresados, planes, platita, lo que sea, lo que venga. Qué triste no escuchar nunca la palabra TRABAJO, ESFUERZO, FUTURO, PORVENIR. Los va a volver a derrotar la DIGNIDAD del pueblo". Luego, Aníbal Fernández tomó esa declaración y en tono amenazante, le respondió: "Muchas escuelas y colegios de la CABA reciben subsidios del estado y está bien. Por ejemplo la escuela/colegio ORT. ¿La conoces? Si que la conoces. ¿O querés que te haga un dibujito? Excelente escuela lo garantizo. Repito ¿Lo conoces?".

Por otro lado, Javier Milei fue consultado sobre su vínculo con la titular del PRO, Patricia Bullrich, y no dudó en tirarle puras flores: "Tengo una excelente relación con Bullrich y la respeto fuertemente porque es alguien que habla siempre de frente y con la verdad. Es uno de los pocos políticos que cada vez que me dijo algo, no me mintió". 

En cambio, el economista libertario no tuvo reparos para cuestionar a los socios de Bullrich en Juntos por el Cambio, especialmente (y sin mencionarla directamente) a su rival en las elecciones de noviembre, María Eugenia Vidal: "Voy a respetar el ideario liberal y nunca voy a tranzar con el kirchnerismo. Pero hay algunos sectores de la oposición que chatean con Máximo Kirchner y que además tienen algunos proyectos que les resultan amigables, como la Ley de Alquileres". La referencia a Vidal viene por el lado de que en más de una oportunidad, la exgobernadora bonaerense confesó haber hablado más de una vez por teléfono con el líder de La Cámpora.