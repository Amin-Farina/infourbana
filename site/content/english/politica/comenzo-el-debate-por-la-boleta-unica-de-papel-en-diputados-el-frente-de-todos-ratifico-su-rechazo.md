+++
author = ""
date = 2022-05-11T03:00:00Z
description = ""
image = "/images/qw4jf7d3mbayzblsjrjoy3xrtu.webp"
image_webp = "/images/qw4jf7d3mbayzblsjrjoy3xrtu.webp"
title = "COMENZÓ EL DEBATE POR LA BOLETA ÚNICA DE PAPEL EN DIPUTADOS: EL FRENTE DE TODOS RATIFICÓ SU RECHAZO"

+++
#### Todas las reuniones del plenario de comisiones serán presenciales y la oposición espera poder firmar un dictamen el 31 de mayo. Las críticas del oficialismo

En una reunión conjunta de las comisiones de Asuntos Constitucionales, Justicia y Presupuesto, comenzó este miércoles el debate sobre la implementación de la Boleta Única de Papel para las elecciones de 2023.

La iniciativa fue impulsada por casi todos los partidos de la oposición que alcanzaron un amplio acuerdo que se materializó en una convocatoria a una sesión especial. Allí sumaron 132 votos para aprobar una moción para emplazar a un plenario de comisiones para los días 11, 17, 24 y 31 de mayo. A fin de mes deberán emitir dictamen para poder debatir el tema en el recinto a principios de junio.

La discusión comenzó con un contrapunto en torno a la posibilidad de que las primeras sesiones informativas sean mixtas, es decir, con la posibilidad de que los diputados y los expositores participen de manera virtual. Desde Juntos por el Cambio argumentaron que el artículo 114 bis del reglamento permite la modalidad mixta. Sin embargo, la bancada oficialista, en las voces de Leopoldo Moreau y Germán Martínez, señaló que ese artículo sólo se refiere a las audiencias públicas y que la decisión de adoptar la modalidad mixta solo la puede tomar el pleno de la Cámara.

Ante la falta de acuerdo y la demora en el inicio de la discusión, Mario Negri expuso que Juntos por el Cambio estaba dispuesto a aceptar que todas las reuniones, incluso las informativas, sean presenciales.

Una vez resueltas estas diferencias, los diputados pasaron a debatir la implementación de la Boleta Única. Llamativamente, ningún diputado del Frente de Todos hizo uso de la palabra para esgrimir sus argumentos en contra de la reforma electoral.

No obstante, en la previa del plenario, el diputado Leopoldo Moreau confirmó el rechazo del oficialismo. “Cuando no hay un problema, no hay que crearlo. El sistema electoral funciona perfectamente bien y el proceso es transparente. Además, los argentinos ya tienen incorporada esta forma de votación”, dijo en una entrevista con _Diputados TV_.

En esa línea, explicó que tampoco es cierto que la BUP sea más barata dado que es más grande y se imprime a color. También agregó que para mejorar la transparencia hay otras cuestiones más importantes como discutir el financiamiento de las campañas electorales.

Durante el debate, la diputada Silvia Lospennato (PRO) recordó que el sistema de boletas partidarias “está cumpliendo un siglo”. “Fue muy importante para conseguir una mayor democratización de la sociedad. Pero es un sistema que ha abandonado el mundo. Las democracias han evolucionado hacia una forma que es más sencilla, más transparente y más democrática, porque asegura que toda la oferta electoral va a estar disponible en el cuarto oscuro”, explicó y agregó que en la actualidad una fuerza política necesita 102.000 fiscales para controlar todas las mesas de una elección nacional.

También instó al Frente de Todos a buscar consensos para que el proyecto sea aprobado con mayor respaldo. “Me preocupa que esto no vuelva a morir en el Senado, porque ya tuvimos una media sanción de boleta única con otro sistema”, dijo en referencia la ley de Boleta Única Electrónica que obtuvo media sanción en 2016 pero no avanzó en la Cámara alta.

El jefe del bloque de la Coalición Cívica, Juan Manuel López, también hizo referencia a Cristina Kirchner: “La Vicepresidenta el viernes les bajó una orden: boleta única, no. En el Senado Cristina Kirchner se va a tener que hacer cargo si quiere más transparencia o no”, desafió al oficialismo.

Ante el silencio del Frente de Todos, las críticas a la BUP quedaron en boca de Myriam Bregman, del Frente de Izquierda. “¿De qué tamaño sería la boleta en la provincia de Buenos Aires, que en la última elección tuvo 28 listas?”, se preguntó. Además, denunció que debido al formato de la boleta, que solo muestra la foto de los primeros candidatos, se promueve la “farandulización de la política”.

El plenario de comisiones deberá emitir un dictamen el 31 de mayo si busca cumplir con el emplazamiento de la Cámara. Según se acordó, hasta el viernes habrá tiempo para que los bloques presenten la lista de oradores para las próximas sesiones informativas. El martes a las 10 continuará el debate.