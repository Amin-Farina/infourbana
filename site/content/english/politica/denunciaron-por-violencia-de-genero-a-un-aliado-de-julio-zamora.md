+++
author = ""
date = 2021-05-27T03:00:00Z
description = ""
draft = true
image = "/images/20210526153351_zamora-750x450-1.png"
image_webp = "/images/20210526153351_zamora-750x450.png"
title = "DENUNCIARON POR \"VIOLENCIA DE GÉNERO\" A UN ALIADO DE JULIO ZAMORA"

+++

**_Se trata del locutor oficial de la municipalidad, Leandro Randolini, quien quedó en el ojo de la tormenta tras haber llamado el último viernes a una empleada comunal a la que habría agraviado en tono amenazante. Trabajadores municipales exigieron su remoción._**

El intendente de Tigre, Julio Zamora, sufrió un duro cuestionamiento en las últimas horas luego de que trascendiera que un sindicalista comunal, de su riñón, llamara telefónicamente a los gritos a una empleada municipal. El hecho generó un repudio masivo de la política local.

Las acusaciones contra el locutor oficial, Leandro Randolini, llovieron este fin de semana, luego que se conociera que había llamado a la empleada municipal Patricia Lima, quien se desempeña en la dirección General de Inspección Industrial, para “basurearla”.

“El día viernes llamó telefónicamente a los gritos a Patricia para acusarla de un hecho que ella misma desconocía. Las mujeres no vamos a permitir que ningún hombre ejerza violencia verbal y le falte el respeto a ninguna mujer. Sepan que no estamos solas y no nos bancaremos ningún atropello, vinieren de donde vinieren”, explicaron trabajadoras municipales que salieron en defensa de Lima.

Vale destacar que Randolini es conocido por ser locutor de boxeo, aunque también por ponerle la voz a muchos actos oficiales del intendente, Julio Zamora, con quien mantiene una relación de tinte personal.

Según la denuncia, el llamado en tono amenazante de Randolini fue concretado “haciendo gala de de su rol como integrante del sindicato municipal”. Es que, el locutor de boxeo, además de ponerle voz a las peleas en el ring, es uno de los titulares del Sindicato de Trabajadores Municipales de Tigre.

Tras la denuncia, que cobró fuerza el fin de semana y circuló en los grupos de WhatsApp de vecinos y espacios de debate vecinal, trabajadores exigieron la remoción de su cargo: “Es una vergüenza que el intendente deje en manos de un violento el sindicato de empleados municipales”, explicaron.