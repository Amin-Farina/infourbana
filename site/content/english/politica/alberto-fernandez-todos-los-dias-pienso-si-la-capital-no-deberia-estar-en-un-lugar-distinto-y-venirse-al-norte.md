+++
author = ""
date = 2021-12-14T03:00:00Z
description = ""
image = "/images/uyvrdv5d2rhkfaiwsenox5nycq.jpg"
image_webp = "/images/uyvrdv5d2rhkfaiwsenox5nycq.jpg"
title = "ALBERTO FERNÁNDEZ: “TODOS LOS DÍAS PIENSO SI LA CAPITAL NO DEBERÍA ESTAR EN UN LUGAR DISTINTO Y VENIRSE AL NORTE”"

+++

#### **_El jefe de Estado encabezó una reunión de Gabinete en la provincia de Tucumán, en donde hizo hincapié en los desafíos que debe afrontar la Argentina luego de la pandemia._**

El presidente Alberto Fernández aseguró durante un acto en Tucumán que todos los días piensa en la posibilidad de trasladar la Capital al norte del país. “Todos los días pienso si la Capital no tendría que estar en un lugar distinto y venirse al norte; ¿no será hora de que empecemos a tomar estos desafíos?”, se preguntó al finalizar una reunión de gabinete, en la ciudad de Monteros.

“Tucumán progresó no solo con un edificio, sino también con un sistema judicial más moderno, más ágil. Y que puede ser un ejemplo para muchos otros, entre otros para el sistema federal que todavía no ha podido implementar el sistema acusatorio en pleno”, enfatizó el Presidente en su discurso, que marcó el cierre de una nueva reunión de Gabinete Federal en Capitales Alternas.

Para equilibrar las oportunidades y descentralizar las riquezas que generan la producción en el país, también apuntó: “Hay que impulsar que inviertan a los que más plata tienen. Hay que impulsar que los que más plata tienen, la traigan y la pongan a producir en Argentina dando trabajo y crecimiento”.

En su discurso, Alberto Fernández habló de una ‘Argentina Central’ y las ‘Argentinas Periféricas’: lo destacó como un sistema que es necesario cambiar “definitivamente”. “No es posible que un chico que vive en el norte puede encontrar mejor suerte en los suburbios de ciudades como Córdoba, Buenos Aires o Rosario”, aclaró.

El Presidente volvió a una provincia que visitó varias veces en sus primeros dos años de gobierno. En la ciudad ubicada 53 kilómetros al sudoeste de la capital provincial presidió el Gabinete Federal. El Presidente hizo pie en un territorio amigable ya que llegó acompañado de Juan Manzur, gobernador en uso de licencia y actual jefe del Gabinete Nacional desde el 20 de septiembre, cuando reemplazó en el cargo a Santiago Cafiero, tras la derrota oficialista en las PASO.

Además de Manzur, al Presidente lo secundaron 11 ministros: el de Justicia y Derechos Humanos, Martín Soria; Seguridad, Aníbal Fernández; Defensa, Jorge Taiana; Interior, Eduardo de Pedro; Desarrollo Productivo, Matías Kulfas; Agricultura, Ganadería y Pesca, Julián Domínguez; Cultura, Tristán Bauer; Ciencia, Tecnología e Innovación, Daniel Filmus; Trabajo, Empleo y Seguridad, Claudio Moroni; Turismo, Matías Lammens; y Desarrollo Territorial y Hábitat, Jorge Ferraresi; además del secretario General de la Presidencia, Julio Vitobello.

No viajaron el ministro de Economía, Martín Guzmán (abocado a la negociación de la deuda externa con el FMI) y los de Obras Públicas, Gabriel Katopodis; de Salud, Carla Vizzotti y de Transporte, Alexis Guerrera, quienes estuvieron en la Cámara de Diputados para defender el Presupuesto 2022.

La delegación fue recibida por el vicegobernador a cargo del Ejecutivo, Osvaldo Jaldo, en el aeropuerto Benjamín Matienzo de la capital, San Miguel de Tucumán. Luego, junto a autoridades provinciales inauguraron en Monteros una sede del Centro Judicial. Luego se trasladaron hasta el predio del Gimnasio Municipal de este departamento provincial para la reunión central de los gabinetes nacional y provincial y para suscribir diversos acuerdos. Luego de esta actividad compartieron un almuerzo de trabajo.

En la inauguración del Centro Judicial el Presidente pidió que “la política no se involucre en la Justicia”. Y agregó que “para la que la democracia funcione mejor, hace falta que la Justicia haga lo que tiene que hacer, que es impartir justicia, simplemente eso”.

El jefe de Estado también sostuvo que la Justicia “es un poder central que debe servir a los ciudadanos y no debe ser una corporación”.

La elección de Tucumán en esta ocasión no parece una casualidad. El Norte argentino es una zona en la que los resultados electorales le fueron favorables al Frente de Todos. En la provincia en la que se firmó la Independencia en 1816, en las elecciones legislativas, el oficialismo logró ganar aunque por un margen más escaso del que se esperaba, apenas tres puntos por encima de Juntos por el Cambio (42% contra 39,42%). Por eso, la decisión de fortalecer la presencia presidencial. Especialmente el Gobierno intenta reforzar la infraestructura a través de un impulso a la obra pública. Se firmarán convenios de obras viales, para adquisición de maquinarias, sistemas de riego, la construcción de un centro ambiental para el tratamiento integral de residuos urbanos y se implementará la tarjeta SUBE a nivel provincial.

En Monteros se hizo la quinta reunión del Gabinete Federal luego de las realizadas en Comodoro Rivadavia (Chubut), Río Grande (Tierra del Fuego), Chilecito (La Rioja) y Rosario (Santa Fe), en cumplimiento con la ley de Capitales Alternas (27.589), sancionada por el Congreso de la Nación en noviembre de 2020 con el objetivo de descentralizar la gestión con políticas públicas equitativas para superar las asimetrías territoriales.