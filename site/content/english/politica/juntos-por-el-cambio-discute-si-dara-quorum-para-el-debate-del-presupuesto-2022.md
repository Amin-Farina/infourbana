+++
author = ""
date = 2021-12-13T03:00:00Z
description = ""
image = "/images/donw5p7uzrfybo5mbu4pmyflvi.jpg"
image_webp = "/images/donw5p7uzrfybo5mbu4pmyflvi.jpg"
title = "JUNTOS POR EL CAMBIO DISCUTE SI DARÁ QUÓRUM PARA EL DEBATE DEL PRESUPUESTO 2022"

+++

#### **_El ministro de Economía, Martín Guzmán, concurrirá esta tarde a la Cámara de Diputados para exponer sobre el proyecto de ley de Presupuesto 2022, que contempla un crecimiento del 4% y deriva la mayoría de recursos a la obra pública y el crecimiento económico. El objetivo del oficialismo es que se vote el jueves, pero necesita consenso de la oposición._**

Guzmán, junto con su equipo económico, concurrirá a las 13 para exponer ante la Comisión de Presupuesto sobre el proyecto para el 2022 y los cambios que se introducirán en las partidas establecidas en la iniciativa enviada el 15 de septiembre, como lo establece la Ley de Administración Financiera. El ministro responderá además preguntas de los diputados y diputadas del oficialismo y la oposición.

Las modificaciones y las preguntas son centrales para terminar de definir la posición que adoptará Juntos por el Cambio. En especial en lo que se refiere al posible acuerdo con el Fondo Monetario Internacional. El texto original ingresó al Congreso el 15 de septiembre, antes de que se conocieran los avances en la negociación.

La comisión se conformó el pasado viernes con 24 legisladores del Frente de Todos con Alicia Aparicio, Rosana Bertone, Daniel Brue, Guillermo Carnaghi, Marcelo Casaretto, Sergio Casas, Marcos Cleri, Emiliano Estrada, Eduardo Fernández, Silvana Ginocchio, José Luis Gioja, Itai Hagman, Carlos Heller, Susana Landriscini, Mario Leito, Germán Martínez, Roberto Mirabella, Blanca Osuna, Sergio Palazzo, María Graciela Parola, Juan Manuel Pedrini, Hernán Pérez Araujo, Carlos Ybrhain Ponce y Jorge Antonio Romero.

Por Juntos por el Cambio habrá 23 representantes con voz y voto: Federico Angelini, Lidia Ascarate, Miguel Angel Bazze, Sofía Brambrilla, Ricardo Buryaile, Alejandro Cacace, Germana Figueroa Casas, Pedro Galimberti, Gustavo Hein, Ingrid Jetter, María de las Mercedes Joury, Luciano Laspina, Hernán Lombardi, Juan Manuel López, Juan Martín, Gerardo Milman, Graciela Ocaña, Paula Oliveto, Jorge Rizzotti, Laura Rodríguez Machado, Victor Hugo Romero, Martín Tetaz y Pablo Torello.

A estos dos grupos se le suma un diputado por el Interbloque Federal el diputado cordobés y del partido del gobernador Juan Schiaretti, Ignacio García Aresca; y el misionero Diego Sartori quien lo hace en representación del interbloque Provincias Unidas.

Estos dos legisladores son los que permitirán destrabar la discusión respecto de la capacidad de obtener dictamen este miércoles próximo para poder, como tiene intención el oficialismo, sesionar el jueves. A partir de esto es que la posición de los gobernadores se volverá relevante en la discusión.

Un dato no menor es que los economistas José Luis Espert, Javier Milei y Ricardo López Murphy no formarán parte de la comisión como miembros sino que hoy estarán presentes como oyentes. Aunque todos los diputados pueden acceder a todas las comisiones, sólo tienen voto para dictaminar los miembros, por lo que estos tres economistas deberán esperar a que el proyecto llegue al recinto.

Esta mañana, Milei apeló a la ironía para comparar al ministro con Walt Disney porque “se dedica a hacer dibujos”. “No vamos a avalar el presupuesto ni un acuerdo con el FMI que contenga déficit fiscal”, adelantó.

La discusión del quórum

Pero en paralelo a la conformación de la comisión, el oficialismo comenzó con el “poroteo” entre los 15 diputados que son de partidos provinciales -Vamos con Vos, Juntos Somos Río Negro, Movimiento Popular Neuquino, Frente de la Concordia de Misiones, Frente Amplio Progresista de Santa Fe y Somos Energía de Santa Cruz- los del Interbloque Federal, conducido por el lavagnista Alejandro “topo” Rodríguez, los 4 de Avanza Libertad y los 4 para el Frente de Izquierda, con la intención de sumar los 11 legisladores que necesita el Frente de Todos -tiene 118 diputados- para alcanzar los 129 necesarios para obtener el quórum.

Necesita sumar diputados frente a la posibilidad de que se imponga un sector “duro” de Juntos por el Cambio que se opone a brindar el quórum alegando la inconsistencia del proyecto de Guzmán.

“Es irreal este proyecto de presupuesto, es casi imposible que se cumpla ninguna pauta. Además, no tiene recursos para el pago al FMI y no sabemos en qué situación están las negociaciones. No sabemos nada”, explicó un diputado del PRO.

“El ministro tiene que dar respuesta las proyecciones macroeconómicas no están bien. El año pasado Guzmán nos mintió con el dato de inflación, ¿cómo le vamos a creer ahora?. El otro tema es el de los subsidios y nadie sabe cómo lo va a hacer”, dijo el diputado Alejandro Cacace, del bloque de Evolución Radical.

“El otro punto que le queremos preguntar es sobre el financiamiento. Dice que los organismos internacionales le van a dar a la Argentina 12.500 millones de dólares pero no sabemos quiénes son esos organismos, porque no es el BID ni el Banco Mundial porque a la hora de señalarlo dice “otros””, agregó Cacace.

Pero aunque en Juntos por el Cambio arrecian las críticas contra el proyecto de Guzmán, la discusión interna es si hay que dar quórum o no. En el bloque del radicalismo, tanto en el de Evolución como en el “tradicional” persiste la idea de sentarse y dar el número para sesionar.

“Nos conviene que tengan un presupuesto porque si el Gobierno no lo consigue, prorrogará el del 2021 y eso le otorga una mayor discrecionalidad en el manejo de fondos”, señaló un radical del ala “tradicional”.

El proyecto girado por el Gobierno prevé un crecimiento del 4% del Producto Bruto Interno (PBI), una inflación del 33% y un dólar a $ 131,1; y contempla un incremento real del gasto social y una reducción de los subsidios energéticos.

Los otros números macroeconómicos proyecta financiamiento del BCRA al Tesoro de 1,8% del PBI (con una caída de -50% 2021), el consumo privado con una suba de 4,6% y la Inversión: 3,1%. En el caso del comercio exterior prevé un crecimiento de 7,5 en exportaciones y 9,4% importaciones y la Recaudación fiscal podría alcanzar los $15,7 billones (+45% contra 2021) .