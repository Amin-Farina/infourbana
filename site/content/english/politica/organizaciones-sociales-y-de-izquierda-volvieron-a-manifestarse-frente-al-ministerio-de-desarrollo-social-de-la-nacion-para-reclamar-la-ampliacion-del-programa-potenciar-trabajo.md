+++
author = ""
date = 2022-02-17T03:00:00Z
description = ""
image = "/images/llvi5vqfcfdltionsmireq43vi.jpg"
image_webp = "/images/llvi5vqfcfdltionsmireq43vi.jpg"
title = "ORGANIZACIONES SOCIALES Y DE IZQUIERDA VOLVIERON A MANIFESTARSE FRENTE AL MINISTERIO DE DESARROLLO SOCIAL DE LA NACIÓN PARA RECLAMAR LA AMPLIACIÓN DEL PROGRAMA POTENCIAR TRABAJO"

+++

#### **_“Nos movilizamos hoy con el reclamo principal de la falta de trabajo. Hay desocupados acá en la 9 de Julio reclamando por trabajo y el ingreso al Potenciar Trabajo”, dijo a Patricio Meincke, del Polo Obrero Tendencia (POT)._**

Luego de una multitudinaria concentración el martes pasado, con agrupaciones y organizaciones cercanas al Frente de Izquierda-Unidad, el miércoles se movilizó ante la cartera que conduce Juan Zabaleta un sector distinto, identificado con el partido Política Obrera que represetan los dirigentes Jorge Altamira y Marcelo Ramal.

La jornada del miércoles tuvo momentos de tensión. Durante el mediodía y las primeras horas de la tarde, el Polo Obrero Tendencia y la agrupación Resistencia Popular cortaron toda la traza de la avenida, incluso los carriles del metrobús como forma de protesta ante la falta de respuesta de las autoridades.

Pasadas las 16 se produjeron incidentes con las fuerzas de seguridad que les exigieron la liberación de la traza para que circulen los colectivos. Hubo enfrentamientos cara a cara y la Policía, con escudos y bastones, apoyados con un camión hidrante, los obligó a retroceder. Un cordón de Infantería de la Ciudad rodeó a los manifestantes para liberar la traza del Metrobús y la protesta se trasladó a la vereda en 9 de Julio y México. Por los gases arrojados, una mujer tuvo que ser asistida por un ambulancia.

Alrededor de las 21 se produjo otro episodio, cuando los manifestantes intentaron montar un gazebo pero tras negociar con oficiales de la Policía, desistieron. Finalmente, lograron instalar carpas hasta que los reciba algún funcionario.

Patricia Meincke indicó a la agencia Télam que recibieron como respuesta que “no va a haber ningún tipo de ingreso” a los programas sociales en el Ministerio de Desarrollo Social y que “no va a haber ningún nuevo subsidio al desocupado”, aunque criticó el Potenciar Trabajo, una de las principales políticas del Gobierno destinada a organizaciones sociales con más de un millón de beneficiarios. “Todos los que en este momento se encuentran cobrando el Potenciar Trabajo tienen que hacer una contraprestación laboral en obras y en distintas tareas, por los 16 mil pesos sin ART, obra social ni estabilidad laboral”, cuestionó.

Otras organizaciones sociales que integran la denominada corriente de “Unidad Piquetera” habían marchado frente al edificio de la misma cartera nacional, con reclamos idénticos. El planteo fue por separado de la última concentración. El Polo Obrero Tendencia que se trata de un sector de la izquierda que pertenecía al Partido Obrero que ahora encabezan los diputados Gabriel Solano y Romina del Plá, tras la ruptura con su histórico dirigente, Jorge Altamira.

Además del POT, adhieren al acampe frente a Desarrollo Social las agrupaciones Ana María Villareal, 19 y 20 de Diciembre, Resistencia Popular, MP 24 de Marzo, Arriba los que luchan, Frente de Trabajadores Combativos–Movimiento 29 de Mayo (FTC-M29), y el Bloque Obrero por el Socialismo.

“El reclamo específico es que el Ministerio de Desarrollo Social cumpla con los compromisos ya asumidos por ellos del ingreso de compañeros al Potenciar Trabajo. Durante el año pasado se comprometió a pagar e ingresar con un compromiso de pago al 28 de diciembre. Hay compañeros comprometidos para ingresar al 5 de febrero y tampoco lo cumplió. Y ahora nos dicen que no van a ingresar más”, insistió hoy Eva Gutiérrez, líder del Polo Obrero Tendencia.