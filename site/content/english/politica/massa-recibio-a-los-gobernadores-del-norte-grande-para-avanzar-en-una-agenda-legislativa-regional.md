+++
author = ""
date = 2022-04-19T03:00:00Z
description = ""
image = "/images/massa-norte-grande-jpg-1.webp"
image_webp = "/images/massa-norte-grande-jpg.webp"
title = "MASSA RECIBIÓ A LOS GOBERNADORES DEL NORTE GRANDE PARA AVANZAR EN UNA AGENDA LEGISLATIVA REGIONAL"

+++
#### Participaron Gerardo Zamora, Jorge Capitanich, Gerardo Morales, Ricardo Quintela y Raúl Jalil.

El presidente de la Cámara de Diputados de la Nación, Sergio Massa, recibió en el Salón de Honor del Congreso a gobernadores de las provincias del Norte Grande para avanzar en el trabajo de leyes federales de promoción económica para la región en los próximos 30 días.

Participaron el gobernador de Santiago del Estero y presidente Pro Témpore del Consejo Regional del Norte Grande, Gerardo Zamora; el gobernador de Chaco, Jorge Capitanich; el gobernador de Jujuy, Gerardo Morales; el gobernador de La Rioja, Ricardo Quintela; y el gobernador de Catamarca, Raúl Jalil.

También estuvieron presentes el presidente del bloque del Frente de Todos en el Senado, José Mayans, y su par en Diputados, Germán Martínez.

En el marco del encuentro de trabajo, los mandatarios provinciales le presentaron al presidente de la Cámara de Diputados un listado de temas y le solicitaron que formen parte de la agenda legislativa en los próximos 30 días.

Los temas presentados por los gobernadores fueron el Tratado de Integración del Norte Grande; la Ley Automotriz y Desarrollo Federal de Autopartes; la Ley de Electromovilidad y Diseño en la Norma de Régimen de Promoción de las provincias productoras de Litio; la Ley de Cannabis Medicinal para promover en el Norte la industria médica; el Comité de Bajos Submeridionales; la ley de Compre Argentino; la transferencia de Inmuebles; y la Ley de Tarifa Energética Diferencial.

##### Declaraciones de los gobernadores

Zamora dijo a los periodistas que “el motivo de la reunión fue para plantear una agenda de proyectos en común que tenemos las diez provincias del Norte Grande” y en ese sentido mencionó el “tratado de Integración del Norte Grande y los tratados bilaterales entre Argentina con cuatro países” ya que “tenemos inversiones que van a realizarse y que generan trabajos en la región”. También mencionó como temas importantes para esa área del país el Compre Argentino y el cannabis medicinal.

Morales dijo que el Norte Grande “viene desarrollando temas importantes para la región” y señaló que transmitirá a los miembros de Juntos por el Cambio la agenda de iniciativas que piden que se traten desde ese conglomerado de provincias. “Pedimos que estos proyectos se pueden aprobar en los próximos treinta días”, agregó el gobernador jujeño.

En tanto, Capitanich dijo que esas iniciativas tiene un alto consenso y recordó que el Norte Grande “tiene 30 senadores y 56 diputados” para poder avanzar en ésta agenda parlamentaria.

El bloque del Norte Grande está compuesto por las provincias de Catamarca, Corrientes, Chaco, Formosa, Jujuy, La Rioja, Misiones, Santiago del Estero, Salta y Tucumán.