+++
author = ""
date = 2021-09-08T03:00:00Z
description = ""
draft = true
image = "/images/burs-massa-y-zamora.jpg"
image_webp = "/images/burs-massa-y-zamora.jpg"
title = "VICTORIA BURS LA CANDIDATA DE MANES CUESTIONADA POR ACOMODAR FAMILIARES Y CONTRATAR A SUS AMIGAS COMO \"ÑOQUIS\" DEL MUNICIPIO DE TIGRE"

+++
##### **_Victoria Burs fue expulsada de la lista de Juntos en varias oportunidades por favorecer a su hermano con un doble contrato y a sus amigas con millonarios contratos a cambio de votar ordenanzas para el municipio. Hoy con fondos de Sergio Massa y Julio Zamora busca asegurar que la oposición no pueda ganarle al Frente de Todos._**

Victoria Burs colocó de manera absolutamente incompatible a su hermano Alberto como empleado del Municipio y de la Provincia de Buenos Aires hasta el año 2019 motivo por el cual fue desplazada de la lista de concejales.

A pesar de ello y de haber terminado su mandato de concejal, tres de sus amigas entre las que se encuentra Carolina Dickson mantuvieron sus contratos en acuerdo con el municipio de Tigre manejado por el Frente de Todos.

Desde Juntos se niegan a hacer comentarios pero nadie desmiente que la candidata de Nicolás Massot recibió ingresos millonarios durante estos años por parte del tándem Massa y Zamora.