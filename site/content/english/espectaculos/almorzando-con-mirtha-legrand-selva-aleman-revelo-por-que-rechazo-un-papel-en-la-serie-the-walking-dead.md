+++
author = ""
date = 2020-12-20T05:00:00Z
description = ""
image = "/images/selva-aleman.jpg"
image_webp = ""
title = "Almorzando con Mirtha Legrand: Selva Alemán reveló por qué rechazó un papel en la serie The Walking Dead"

+++
**_La actriz Selva Alemán estuvo invitada en Almorzando con Mirtha Legrand (eltrece) y habló de cuando fue convocada para interpretar un papel en la exitosa serie The Walking Dead, pero tuvo que rechazarlo._**

"¿Es cierto que te habían llamado para trabajar en The Walking Dead y tuviste que rechazar la oferta porque estabas ensayando una obra de teatro?", le consultó Juana Viale. "Sí, estaba por estrenar. Fue difícil y fue raro que me llamaran. Yo la serie no la conocía, la empecé a ver y dije 'que horror, tendré que hacer alguno de estos monstruos espantosos'. Se que era un personaje latino que hablaba muy poco inglés", recordó Selva.

"Yo estaba por estrenar en una semana Madres e Hijos. Estaba el teatro tomado, la escenografía hecha, la publicidad mandada, los ensayos. Había mucha gente involucrada, cómo iba a decir 'me voy a filmar a Los Ángeles'", continuó la actriz. "Era una decisión muy difícil porque la serie es exitosísima en todo el mundo", agregó Juana.

"¡Y sigue! Lo único que me alegró es que pensé que me llamaban para hacer de zombie. Son de esas cosas que te ocurren en la vida, aparecen una vez y algunas veces el tren hay que dejarlo pasar", afirmó Selva.