+++
author = ""
date = 2021-06-21T03:00:00Z
description = ""
image = "/images/rsaztxf4endihexwkhh4aob6jq.jpg"
image_webp = "/images/rsaztxf4endihexwkhh4aob6jq.jpg"
title = "MASTERCHEF CELEBRITY: GASTÓN DALMAU Y GEORGINA BARBAROSSA A LA FINAL"

+++

Tres participantes, dos finalistas, un campeón. La recta final de la segunda temporada de Masterchef Celebrity está en su momento más caliente y dio un nuevo paso camino a la consagración. Al empezar la gala, Georgina Barbarossa, Gastón Dalmau y Sol Pérez se disponían a dar todo para llegar a la gran cita del jueves. A la medianoche, el menú ya estaba servido para solo dos comensales. En el medio, pasaron unas cuantas cosas.

La anteúltima tarea para los cocineros era preparar un menú de dos platos salados libres. Para ello, iban a disponer de 70 minutos sin la necesidad de ir al mercado. Los ingredientes principales estaban en dos cajas de las que estaban obligados a usar al menos la proteína. En una, pulpo; en la otra, hígado de pollo. Cada diez minutos, sonaba la chicharra y tenían que alternar en cada una de las estaciones. Y con el correr de la prueba se les sumaría una nueva dificultad: una canasta con nueve productos de precios cuidados, que debían utilizar en su totalidad.

Georgina fue la primera en pasar al frente: “No puedo hablar, estoy como perro en bote”, señaló la actriz, habitualmente locuaz, para graficar sus nervios ante el jurado integrado por Germán Martitegui, Damián Betular y Donato De Santis. Los elogios se los repartieron la presentación del pulpo y la preparación de los alcauciles. Las críticas estuvieron por el exceso de ajo, un producto del que se recomienda su uso cuidadoso, y en el sabor del cous cous calificado como “correcto”. A esta altura de la competencia, casi un aplazo.

A su turno, Gastón se lució por partida doble. Primero, al elaborar unas cintas con una salsa en la que incorporó el pulpo: “Es una bocanada de mar”, dijo Betular, pero el gran éxito vendría después, con el segundo plato. El jurado evaluó no solo el sabor del paté de hígado de pollo con champiñones, sino también la valentía del participante por animarse a prepararlo: “Son sabores contundentes, no sé como te ocurrió, pero está fantástico”, aportó Donato, que elevó la pasta al podio de los mejores de la competencia con todo lo que eso significa.

Quedaba Sol nomás, que arrancó las devoluciones con el pie derecho y se fue desdibujando. Según Martitegui, la bruschetta de pulpo tenía una buena presentación, con la cocción en el punto justo, y solo objetó el corte del pan: demasiado grueso para su gusto. En cambio, la pasta no fue una buena decisión no tanto en la elaboración sino, sobre todo, en la elección de un plato playo para el emplatado, lo que impidió la integración de los sabores.

Con este panorama, había un voto cantado y un mano a mano que se definiría por detalles. Donato anunció el formalismo que colocaba a Gastón Dalmau en la final de la segunda temporada de Masterchef Celebrity. El nombre de su acompañante era el misterio. Y la elegida, por pequeño margen, fue Georgina Barbarossa: “Estoy anonadada”, dijo la actriz, separando bien las sílabas como haciendo tiempo y lugar para caer en la noticia que acababa de recibir.

Después de saludar a la finalista, la conductora recibió las cálidas palabras del jurado, que inició Donato. “Solito, un camino eterno, muchas semanas de cocinar, probar, un repechaje... Sé que te sentís ganadora, así que no solo conquistaste a todo el país sino a este trío de jueces. Te vamos a recordar para siempre”, señaló el italiano. “Mi querida Sunny” -continuó Betular- “para mí sos una campeona. El éxito en la gastronomía se hace en base a estudios y esfuerzos, y vos sos el ejemplo de eso en esta competencia. Te adoro”, se emocionó el pastelero.

“Sol, querida” -cerró Martitegui con una enorme sonrisa- “Todo el tiempo hablábamos que eras competitiva. La autoexigencia y la competencia sana son una virtud y vos fuiste un ejemplo en un momento del mundo tan difícil. Terminaste cocinando de puta madre y fue una alegría tenerte”, cerró el jurado más exigente.

”No me queda más que agradecimiento con este programa, es realmente una familia. Estoy feliz, siento que le voy a poder cocinar a mis hijos, que voy a poder hacer otras cosas”, inició Sol su despedida cuando Santiago del Moro la interrumpió. “¿Estás embarazada?”, preguntó el conductor para descontracturar. “Por ahora no, falta un montón”, respondió la ex chica del clima que se fue con los oídos dulces de elogios: “Me encanta que me digan lo que me están diciendo porque siento que di todo lo mejor de mí y que se haya visto eso creo que es lindo”, señaló antes del anteúltimo aplauso de despedida. Hasta el jueves entonces, cuando Georgina y Gastón se enfrenten en la gran final para definir quien coloca su nombre en el trofeo de la segunda temporada de Masterchef Celebrity.