+++
author = ""
date = 2022-03-28T03:00:00Z
description = ""
image = "/images/will-smith.jpg"
image_webp = "/images/will-smith.jpg"
title = "WILL SMITH CONFRONTA A CHRIS ROCK, Y LUEGO GANA EL OSCAR"

+++
#### Will Smith caminó al escenario y aparentemente golpeó al presentador Chris Rock durante la ceremonia de los Oscar después de que Rock hizo un chiste sobre el aspecto de la esposa de Smith, Jada Pinkett Smith

Poco antes de que Smith ganara el premio al mejor actor por su interpretación del padre de Venus y Serena Williams, Richard Williams, en “King Richard” (Rey Richard: Una familia ganadora”), subió al escenario y aparentemente golpeó a Rock el domingo en la ceremonia de los Premios de la Academia luego que el cómico hiciera un chiste sobre el aspecto de la esposa de Smith, Jada Pinkett Smith.

Smith le gritó a Rock: “¡Deja el nombre de mi esposa fuera de tu (grosería) boca!”, y el público susurró sorprendido cuando quedó claro que no era una escena actuada. Smith se disculpó después con mucha gente, incluidas Venus y Serena, pero no mencionó a Rock en su disculpa.

“Richard Williams era un férreo defensor de su familia”, dijo al comenzar su discurso de aceptación.

Hizo una pausa como para dejar en claro su elección de palabras, y lloró al hablar sobre ser un protector de aquellos con los que trabajó en la película.

“He recibido un llamado en mi vida para amar a la gente y proteger a la gente y ser un río para mi gente”, dijo Smith en su discurso. “Ustedes saben que para hacer lo que hacemos tenemos que aguantar abuso. Tenemos que aguantar que gente diga cosas locas de nosotros. En este negocio tienes que aguantar que la gente te falte el respeto y tienes que sonreír y hacer como que está bien”.

El intercambio comenzó cuando Rock se burló de la cabeza rapada de Pinkett Smith comparándola con la de un oficial del ejército.

“Jada, te amo. ‘GI Jane’ 2, no puedo esperar”, dijo.

Pinkett Smith, quien pareció inmediatamente molesta por el chiste, reveló en 2018 que padece alopecia (pérdida anormal del cabello). En varias ocasiones ha hablado sobre esta situación en Instagram y en sus redes sociales.

Smith subió hacia el escenario desde su asiento en primera fila y le dio una bofetada a Rock que sonó fuerte. Smith regresó a su lugar y le gritó a Rock que dejara a su esposa en paz. Rock repitió que sólo estaba haciendo un chiste sobre “GI Jane” y Smith volvió a gritar que la dejara.

“Esa fue la noche más grande en la historia de la televisión”, dijo Rock y retomó su papel como presentador.

Unos minutos después, el rapero Sean Combs — en el escenario para presentar un homenaje a “The Godfather” (“El padrino”) — trato de que hicieran las paces y sugirió que Smith y Rock arreglaran sus diferencias después.

“Will y Chris, vamos a arreglar esto como una familia en la fiesta dorada”, dijo Combs.

El suceso generó distintas reacciones. Varias personas se acercaron a Smith y Pinkett Smith en el corte a comerciales que siguió al incidente. Keith Urban abrazó al actor durante una pausa y Nicole Kidman también fue a decirle unas palabras.

Tras bambalinas, durante una sesión de entrevistas con los ganadores, el incidente Rock-Smith fue algo de lo que pocos querían hablar.

“No voy a hablar al respecto”, dijo Questlove, el director del documental ganador “Summer of Soul”, la categoría que presentó Rock.

Pinkett Smith también fue objeto de los chistes de Rock cuando el cómico fue anfitrión de los Oscar en 2016. Ella no asistió a la ceremonia en ese entonces argumentando la falta de diversidad en entre los nominados de ese año y la representación inadecuada de artistas negros.

“Espero que la Academia me vuelva a invitar”, dijo Smith al concluir su discurso.

Al final de la ceremonia, la organización tuiteó la siguiente declaración:

“La Academia no aprueba la violencia de ninguna forma. Esta noche estamos encantados de celebrar a los ganadores de la 94ta edición de los Premios de la Academia, que merecen este momento de reconocimiento por parte de sus compañeros y cinéfilos de todo el mundo”.