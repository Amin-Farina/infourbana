+++
author = ""
date = 2021-07-01T03:00:00Z
description = ""
image = "/images/f800x450-222572_274018_5050-1.jpg"
image_webp = "/images/f800x450-222572_274018_5050.jpg"
title = "JUICIO CONTRA EL PATO CABRERA POR VIOLENCIA DE GÉNERO CONTRA SU EX"

+++

Por primera vez en su vida Ángel “Pato” Cabrera se sentará en el banquillo de los acusados. Tras permanecer varios meses prófugo de la justicia, haber sido detenido en Río de Janeiro y luego extraditado a Córdoba el golfista enfrenta el primer juicio en su contra por golpear, insultar y amenazar a su ex pareja, la agente de policía C.T.M.

Desde hoy y durante 4 audiencias se juzgarán dos hechos sucedidos en 2016 y 2018. No será la única vez que el deportista se vea cara a cara con la justicia, ya que en lo que resta de este año y el que viene hay previstos varios debates orales por las múltiples causas por violencia contra sus ex parejas.

La defensa de Cabrera fue la que solicitó expresamente que el ex campeón del Master de Augusta esté de manera presencial en la sala de audiencias del edificio de tribunales de Córdoba. Por su parte, la víctima también lo hará de manera presencial y, según está estipulado, en esta primera jornada tendrá la posibilidad de hablar.

La jueza Mónica Traballini también le dará la palabra al imputado, que puede declarar o negarse a hacerlo. Según fuentes de la defensa, hará uso del micrófono brevemente para asegurar que es inocente. En total se espera que el juicio, llevado adelante por la fiscal Laura Battisteli, transcurra durante 4 jornadas en las que desfilarán casi 20 testigos. La sentencia podría escucharse el próximo jueves.

Puntualmente desde hoy se juzgarán dos hechos: el primero se basa en la denuncia sobre los golpes efectuados por el golfista a su entonces novia a la salida de un country; el segundo, por haberle producido heridas en la propia casa de Cabrera. En este último episodio la denuncia incluyó también el robo del celular de la joven por parte del deportista lo que le valió ser juzgado, además, por hurto.

Cabrera, que permaneció prófugo de la Justicia argentina durante varios meses escondido en Estados Unidos, llegó a Brasil clandestinamente en un vuelo de línea el 31 de diciembre pasado. A pesar de que Interpol había emitido una alerta rojo con su fotografía logró pasar todos los controles e ingresar a tierra brasileña. Allí, se escondió durante 14 días en Leblon, un lujoso barrio de Río de Janeiro, hasta que finalmente fue detenido y enviado a la cárcel federal Plácido de Sá Carvalho.

Una vez que se realizaron todos los trámites burocráticos correspondientes a la extradición, Cabrera fue enviado por Interpol directamente a una cárcel cordobesa a principios del mes pasado. Desde que llegó recibió apenas las visitas de su abogado Carlos Hairabedian, su hijo y su representante.

La primera de sus ex parejas en denunciarlo fue S.R., ex esposa y madre de sus dos hijos, que radicó una denuncia en la fiscalía de Violencia Familiar de Córdoba por amenazas. En el mismo sentido, M.E., que estuvo en pareja dos años con el golfista, hizo una presentación por amenazas y coacción.

La otra mujer que también lo demandó fue la agente policial C.T.M., que estuvo en pareja con Cabrera durante tres años. En sus declaraciones testimoniales de 2016 relató que su entonces novio la atacó a golpes de puño e incluso intentó atropellarla con una camioneta. Luego, el deportista de élite sumó cuatro causas más porque supuestamente siguió amenazándola, a pesar de que el juez había impuesto una prohibición de contacto. A eso se le sumó una denuncia por hurto.

La última de las demandas fue a principios de mayo, también presentada por C.T.M., a raíz de un video que la mujer logró recuperar de su teléfono en el que se ve a Cabrera acostado en medio de una videollamada y le dice a su ex: “Yo voy a salir, pero vos del cementerio no”.

Estos delitos, denunciados por la ex policía, son los que se juzgarán desde hoy. El inicio de este juicio estaba previsto, en realidad, para el año pasado pero justamente el día en que las partes debían notificarse fue cuando Cabrera se subió a un avión privado y voló a Chicago para profugarse.

Según fuentes de la querella, la pena que se pedirá rondará los dos años de prisión. Es una incógnita aún determinar si la fiscal Battisteli acompañará esta solicitud. Se trata de una de las funcionarias judiciales más respetadas y que tuvo un papel preponderante en la condena de septiembre de 2019 a Brenda Barattini, la mujer que le cortó los genitales a su pareja