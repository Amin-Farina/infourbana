+++
author = ""
date = 2022-05-30T03:00:00Z
description = ""
image = "/images/china-suarez.webp"
image_webp = "/images/china-suarez.webp"
title = "LA FUERTE REACCIÓN DE RUSHERKING TRAS LAS CRÍTICAS POR SU NOVIAZGO CON LA CHINA SUAREZ "

+++
#### El músico y la actriz blanquearon su relación este fin de semana y las redes sociales estallaron. Primero respondió ella y ahora él

En las últimas horas, la China Suárez y Rusherking se convirtieron en tendencia luego de que ambos confirmaran su noviazgo. El sábado por la noche, el artista de 22 años dio detalle de su relación con la actriz y entre otras cosas, confesó que está muy enamorado. Los usuarios en las redes sociales estallaron con mensajes a favor y en contra y ambos salieron a responder.

“Estoy muy enamorado y muy bien con alguien. De verdad hace mucho que no me pasaba sentirme así”, declaró Rusherking el sábado por la noche en diálogo con Andy Kusnetzoff, además narró que se conocieron en una fiesta y que ambos tuvieron un encuentro en Madrid. Tras sus declaraciones románticas, la China Suárez compartió en sus redes la primera foto de ambos acaramelados.

De inmediato la nueva pareja revolucionó las redes sociales y una de las mayores críticas fue la diferencia de edad. La primera en reaccionar fue La China, quien en su cuenta de Twitter escribió: “Vivan como quieran. Les van a romper las pelotas igual. Feliz domingo”.

Por su parte, el joven oriundo de Santiago del Estero también se le cuestionó por haber engañado a María Becerra y al respecto sostuvo: “Quién se cree la gente para opinar quién puede estar con quién, sean felices y déjense de romper los huevos. ¡Que bien ver a todos mis amigos felices!”.

Hasta el momento, solo había como testimonios de la relación un baile in fraganti en el after party de la entrega de los Martín Fierro 2022, una almuerzo en la casa de ella y el festejo de cumpleaños de él. Pero hasta que las cámaras de _LAM_ los encontraron en un hotel de Recoleta, en una noche de amor que terminó accidentada -con los dos autos, el de la China y el de _Rusher_, acarreados por la grúa-, nada etiquetaba la historia más que como incipiente noviazgo. El cantante le puso fin a todas las especulaciones y oficializó la pareja: “Estoy muy enamorado y muy bien con alguien. De verdad hace mucho que no me pasaba sentirme así”.

Después le puso nombre propio al motivo de su felicidad: “La China me dice _Thomy_”. También se explayó en cómo conoció a la ex _Casi ángeles_: “Nos conocimos en una fiesta, nos estuvimos mirando toda la noche, y no sabía si acercarme o no. A mí no me gusta sobrepasar nada, pero ya habíamos chateado antes”, señaló, y luego aclaró que no hablaba del after party de los Martín Fierro 2022 donde los captaron a los besos, sino de una salida anterior donde coincidieron.

“Estuvimos toda la noche cruzando miraditas en esa fiesta, después hablamos y después yo me fui a Madrid. Ahí la vi, estuvimos en Madrid y después volví para Argentina y nos vimos acá también”, relató, sin dar fechas ni más precisiones. “Nos estamos conociendo y estoy disfrutando de ese proceso, estoy muy enamorado”. Veinte minutos después de sus declaraciones en el ciclo de Telefe que va por la sexta temporada, la China reaccionó en sus _stories_.