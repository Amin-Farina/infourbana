+++
author = ""
date = 2021-04-26T03:00:00Z
description = ""
image = "/images/16193814047499.jpg"
image_webp = "/images/16193814047499.jpg"
title = "¿QUIÉNES FUERON LOS GANADORES EN LOS PREMIOS OSCAR?"

+++

**_“Nomadland”, de la directora chino-estadounidense, resultó ganadora de la categoría de Mejor Película en la 93ra. edición de los premios que entregó la Academia de Cine de Hollywood en Los Ángeles._**

##### La siguiente es la lista completa de ganadores:

* Mejor película: “Nomadland”, de Chloé Zhao.
* Mejor Dirección: Chloé Zhao (“Nomadland”).
* Mejor Actriz Protagonista: Frances McDormand (“Nomadland”).
* Mejor Actor Protagonista: Anthony Hopkins (“El padre”).
* Mejor Actriz de Reparto: Yuh-Jung Youn (“Minari”).
* Mejor Actor de Reparto: Daniel Kaluuya (“Judas y el mesías negro”).
* Mejor Guion Original: Emerald Fennell por “Hermosa venganza”.
* Mejor Guion Adaptado: Christopher Hampton y Florian Zeller por “El padre”.
* Mejor cinematografía: Erik Messerschmidt (“Mank”).
* Mejor edición: Mikkel E.G. Nielsen (“El sonido del metal”).
* Mejor diseño de producción: Donald Graham Burt y Jan Pascale por “Mank”.
* Mejor diseño de vestuario: Ann Roth por “La madre del blues”.
* Mejor Sonido: Nicolas Becker, Jaime Baksht, Michelle Couttolenc, Carlos Cortés Navarrete y Phillip Bladh por “El sonido del metal”.
* Mejor maquillaje y peluquería: Sergio Lopez-Rivera, Mia Neal y Jamika Wilson por “La madre del blues”.
* Mejor Banda de sonido: Trent Reznor, Atticus Ross y Jon Batiste por “Soul”.
* Mejor Canción original: “Fight for You” de “Judas y el mesías negro” - H.E.R., D’Mile y Tiara Thomas.
* Mejores Efectos visuales: Andrew Jackson, David Lee, Andrew Lockley y Soctt R. Fisher por “Tenet”.
* Mejor Largometraje Documental: “Mi maestro el pulpo”, de Pippa Ehrlich y James Reed.
* Mejor Cortometraje Documental: “Colette”, de Anthony Giacchino y Alice Doyard.
* Mejor largometraje animado: “Soul”, de Pete Docter y Kemp Powers.
* Mejor corto animado: “If Anything Happens I Love You”.
* Mejor corto de ficción: “Two Distant Strangers”, de Travon Free y Martin Desmond Roe.
* Mejor Filme Internacional: “Otra ronda” (Dinamarca), de Thomas Vinterberg.