+++
author = ""
date = 2021-05-11T03:00:00Z
description = ""
image = "/images/vwvids7wqjh2heccnezt3c4v5a.jpg"
image_webp = "/images/vwvids7wqjh2heccnezt3c4v5a.jpg"
title = "GASTÓN DALMAU Y UN PROBLEMA DE SALUD QUE LO OBLIGÓ A FALTAR A MASTERCHEF"

+++

**_MasterChef continúa navegando por las mieles del éxito, las mediciones permanecen altísimas en esta segunda temporada, más allá que algunas voces cuestionan a este grupo de famosos y los consideran un poco menos interesantes que la primera camada._**

La fórmula conserva su efectividad, la que se relaciona con los roles específicos de los tres jurados, con la habitual polémica que siempre despierta Germán Martitegui con su rigurosidad y sus modos poco amables con los participantes.

En ese compendio, Gastón Dalmau experimenta un renacer de su visibilidad, tras muchos años de lejanía de los primeros planos en la pantalla argentina. El actor optó por un camino alternativo y se mudó a Estados Unidos a estudiar y trabajar en la producción de películas.

En cuanto a su raid por el certamen, el ex Casi Ángeles camina con cierta solvencia, con una buena pericia para resolver en las hornallas. Aunque el lunes se vio envuelto en una polémica con Martitegui, quien le recriminó una supuesta falta de ganas.

Gastón no aparecerá en tres galas de MasterChef y esa ausencia posee un motivo, una razón, que se vincula con un problema de salud. A partir del proceso de grabación de los episodios con antelación, recién la próxima semana se verá al aire la no participación del actor.

Trascendió que padeció una gastroenteritis aguda y esto lo obligó a sumirse en la cama, muy abatido y cansado por consecuencia de esa afección. No obstante, la producción decidió esperarlo y no le buscaron un reemplazo.