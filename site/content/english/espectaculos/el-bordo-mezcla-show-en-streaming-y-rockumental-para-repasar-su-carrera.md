+++
author = ""
date = ""
description = "Ale Kurz, líder del grupo que se presentará los días 20 y 27 de septiembre y 4 de octubre, asegura que esas funciones son “un espacio de comunicación con el público”."
image = "/images/5f5fb82565285_1004x565.jpg"
image_webp = "/images/5f5fb82565285_1004x565-1.jpg"
title = "El Bordo mezcla show en streaming y rockumental para repasar su carrera"

+++
###### _Ale Kurz, líder del grupo que se presentará los días 20 y 27 de septiembre y 4 de octubre, asegura que esas funciones son “un espacio de comunicación con el público”._

El cantante y guitarrista Ale Kurz, líder del grupo rockero El Bordo que repasará su carrera con tres presentaciones por streaming los días 20 y 27 de septiembre y 4 de octubre, considera que esas funciones representan “un espacio de comunicación con el público”.  

Para llegar a entablar ese lazo virtual, Kurz contó a Télam que El Bordo generó “capítulos en los que vamos a contar la historia de la banda a través de repasar cómo grabamos cada uno de los discos y eso mechado con la banda tocando para dar forma a un material muy jugoso”.

Para esta apuesta denominada “La Serie” a la que podrá accederse a través de Passline, la banda ya grabó tres presentaciones en La Trastienda que podrán apreciarse en tres domingos consecutivos, siempre desde las 21.

Pero no serán los típicos conciertos, ya que el quinteto que completan Diego Kurz en guitarra y coros, Pablo Spivak en bajo y coros, Miguel Soifer en batería y Leandro Kohon en teclados, armó segmentos audiovisuales con imágenes de viejos conciertos, entrevistas a colegas y productores.

El 20 se verá “Canciones y amigos” e incluirá los discos “Carnaval de las heridas” y “Un grito en el viento”; el 27 será el turno de “Y por eso yo canto” basándose en “En la vereda de enfrente”, “Yacanto” e “Historias perdidas”; y el 4 de octubre cerrará con “De sangre y elección” que tendrá como epicentro a los discos “Hermanos” y “El refugio”.

**Télam: ¿Cómo realizaron todo el proceso audiovisual para estos streamings?**

Ale Kurz: El parate obligado nos llevó a hacer esta retrospectiva y la queremos disfrutar y valorizar el camino que venimos haciendo hasta ahora. Nos da mucho orgullo ver que arrancamos con la banda cuando teníamos 15 años y que seguimos con proyectos, sueños y queriendo ir para adelante con este mismo proyecto. Además hay varios músicos a los cuales admiramos muchísimo que tuvieron la gentileza de querer participar y todo eso va a ir mechado con el material de archivo que tenemos que es riquísimo en 22 años de vida. Tenemos fotos y videos y situaciones de cuando éramos muy chicos que en este momento queremos compartir.

**T:¿Va a tocar la banda sola o invitaron a colegas?**

AK: Es solamente la banda. Para una banda tocar frente a un lugar vacío es casi una prueba de sonido filmada, entonces estábamos buscando la forma de que se generara energía que es lo que más nos gusta del vivo. Decidimos hacer una puesta circular y tocamos en círculo, y las cámaras es como que nos están abrazando y se dio que logramos transmitir esa energía que se da cuando tocamos juntos. Obviamente si lo llevamos a comparación con un show en vivo estamos a años luz, pero por eso decidimos sumar testimonios y material de archivo. Intentamos conectar con la emoción, que siempre fue y es mi objetivo cuando hago música o cualquier tipo de arte, tratar de emocionar al que lo recibe.

**T: ¿Cómo se llevan con los shows por streaming?**

AK: No nos gustan y técnicamente estamos a años luz. A mí la de tocar en un lugar vacío, que las cámaras me enfoquen y plantear el escenario como lo veníamos haciendo es algo que no quisimos hacer. Por eso nos pusimos a pensar variantes.

**T: ¿Fue productivo el confinamiento?**

AK: Fue muy productivo artísticamente. Hacía años que no practicaba tanto los instrumentos, incluso me puse a jugar y descubrí un programa maravilloso que es el Garage Band y me puse a grabar maquetas de todo tipo. En ese aspecto fue bueno porque puse la energía ahí. Yo soy muy de eso, de ponerme una zanahoria adelante y entonces empecé a limpiar el archivo del cuaderno y empecé a grabar canciones que tenía guardadas. En ese afán aparecieron canciones nuevas y otras que están pidiendo su lugar para el próximo disco de El Bordo.

**T: ¿Cómo está el proceso de armado del disco nuevo?**

AK: Estábamos por entrar al estudio a grabar el nuevo disco pero obviamente los planes se vieron frustrados. Luego de terminar el trabajo con el nuevo disco de Las Pastillas del Abuelo, seguimos con las canciones de El Bordo, había un demo armado con 14 o 15 ideas para encarar un último mes de ensayo y seleccionar las que íbamos a grabar pero ahora alguna más está apareciendo. Estoy ansioso, una vez que terminemos “La Serie” el plan sería retomar los ensayos y ver cuándo se puede grabar.

**T: ¿Qué onda estaba tomando el disco de ustedes?**

AK: Pasó algo muy lindo en este disco impulsado por Ale Vázquez. Nos armamos nuestro propio estudio y al tener todo listo para grabar, hasta la pandemia se dio que si bien trabajamos en canciones nuevas hubo mucha experimentación y zapada. A partir de la zapada surgieron muchos escenarios musicales que me los llevé a mi casa y les ponía melodía y una letra. Muchos temas salieron con esa dinámica, que es distinta a la que veníamos trabajando, y hay ideas musicales de los chicos que las trabajamos en la sala. Viene con bastantes matices, con bastantes colores, pero creo que la palabra que se merece es la presencia energética que se da cuando coincidimos en la tocada, se da algo tremendo.

**T: ¿Cómo fue el trabajo de producir lo nuevo de Las Pastillas del Abuelo junto a Ale Vázquez y Pablo Spivak?**

AK: Estos últimos años estoy viviendo con mucha intensidad el tema de generar música, hacer música y producir música. Hacer el disco de Las Pastillas del Abuelo, produciendo y grabando en nuestra sala, fue una experiencia tremenda porque ellos son amigos. Abrimos las puertas de nuestra sala y disfrutamos un montonazo con ellos. El disco está terminado. Antes de que arranque la pandemia ya tenían las últimas canciones, las últimas mezclas, y faltaba masterizarlo.

**_Fuente: Télam_**