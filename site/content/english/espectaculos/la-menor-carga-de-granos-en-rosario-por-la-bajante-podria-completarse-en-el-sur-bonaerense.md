+++
author = ""
date = 2021-07-05T03:00:00Z
description = ""
image = "/images/rosario-fluvial.jpg"
image_webp = "/images/rosario-fluvial.jpg"
title = "LA MENOR CARGA DE GRANOS EN ROSARIO POR LA BAJANTE PODRÍA COMPLETARSE EN EL SUR BONAERENSE"

+++

**_La bajante del río Paraná volvió a obligar a los barcos a operar con un calado menor, disminuyendo la carga de sus bodegas en los puertos fluviales de la provincia de Santa Fe y su parcial reemplazo por los marítimos del sur bonaerense._**

"Esta bajante del Paraná es extraordinaria y está generando complicaciones logísticas como la carga de menor tonelaje de granos en barcos de gran porte en el puerto de Rosario", explicó Liliana Spescha, docente de la cátedra de Climatología y Fenología Agrícolas de la Facultad de Agronomía de la Universidad de Buenos Aires (Fauba).

Debido al bajo nivel del Paraná, la salida de buques de carga por la hidrovía se encuentra limitada a unos 31 pies; y los buques que normalmente se despachan a unos 34 pies de profundidad se están despachando a unos tres pies menos, coincidió Guillermo Wade, gerente de la Cámara de Actividades Portuarias y Marítimas (Capym).

En contrapartida a la reducción de la carga en los barcos en los puertos del Gran Rosario, se observa un aumento del volumen promedio de granos cargados en los puertos marítimos del sur de Buenos Aires, agregó Wade.

Según el sitio especializado Data Portuaria, la escasez del calado limita la carga de los buques, que parten del Río Paraná con 5.000 toneladas menos en sus bodegas; los buques se dirigen a otros puertos de mayor calado, completando las bodegas y reduciendo así los costos logísticos; y los puertos más concurridos para realizar este tipo de operaciones suelen ser Quequén y Bahía Blanca.

"Los nodos portuarios de Rosario y Bahía Blanca son complementarios, básicamente granos", explicó a Télam el secretario técnico de la Comisión de Transporte de la Bolsa de Comercio de Rosario (BCR), Alfredo Seré, para quien "es probable que se carguen más granos de lo habitual (en los puertos del sur bonaerense)", pero aclaró que "en harinas lo harán en el sur de Brasil".

En ese sentido, Gustavo Idígoras, presidente de la Cámara de Industria Aceitera de la República Argentina (Ciara), explicó en declaraciones periodísticas que "hay varaduras todo el tiempo, menos cargas en barcos que luego se van a Brasil a terminar de completar, costos de logística que se incrementan y pérdida de ventas entre otros aspectos".

Por su parte, el presidente del Consorcio de Gestión del Puerto de Bahía Blanca (CGPBB), Federico Susbielles, señaló que esa terminal del sur de la provincia de Buenos Aires "está preparado para absorber mayor cantidad de carga en caso de que sea necesario".

"El 90% de las operaciones de granos en el puerto de Bahía Blanca son de completamiento de carga", señaló Susbielles a Télam al indicar que "siempre funciona como un esquema de complemento de todo el complejo Rosafe, de la hidrovía".

Para el presidente del ente portuario, "la bajante va a impactar en una mayor capacidad de completamiento de los buques en Bahía Blanca; el año pasado funcionó de esta manera, especialmente entre los meses de julio y octubre y el puerto de Bahía Blanca está preparado para absorber mayor cantidad de carga en caso de que sea necesario".

"Normalmente alrededor de 65% de los embarques de granos -poroto de soja, maíz, cebada- sale por Rosario y el 35% restante por Bahía Blanca; así que si un buque que cargaba 40 mil toneladas y hoy puede cargar sólo 32.000 toneladas, 20% menos por la bajante, podría llegar a ir a completar su carga en Bahía Blanca", arriesgó Seré.

De esta forma, continuó, "en lugar del 65%, podríamos cargar en Rosario el 60%; y en Bahía Blanca, en lugar del 35%, un 40%", pero consideró que hacer una estimación "aún es muy difícil".

Por otra parte, Seré advirtió sobre los sobrecostos que implica esta operatoria, por el denominado "flete falso" implícito en el movimiento de un buque sin estar completamente cargado -el costo debe prorratearse en una menor cantidad de toneladas transportadas por la menor ocupación de las bodegas- y el encarecimiento que implica conseguir mercadería en otro puerto.

"La mayor parte debería corresponder a granos que estén en el 'hinterland' o las zonas cercanas al puerto; o algún determinado producto debería ser trasladado desde el norte hasta el puerto, de allí los sobrecostos logísticos, eso obligaría a pagar una prima mayor para poder cubrir los mayores costos", consideró.

Finalmente, Seré estimó que así como "el año pasado el sector tuvo sobrecostos del orden de los US$ 245 millones, este año estaría en ese orden. pero es un análisis muy complicado, no es un cálculo muy sencillo, siempre son estimaciones".

El año pasado, en una situación similar, la Dirección de Informaciones de la Bolsa de Comercio calculó que los sobrecostos habían sido de entre US$ 240 y 245 millones.