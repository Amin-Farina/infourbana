+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/tinelli.jpg"
image_webp = "/images/tinelli.jpg"
title = "¿CÓMO FUE EL REGRESO DE SHOWMATCH?"

+++

**_Después de un año y medio alejado de la televisión, este lunes Marcelo Tinelli regresó a la pantalla con ShowMatch (eltrece). Tras de un despliegue lleno de música y baile con artistas de primera línea, siguió una ficción con guiños a las series más exitosas del 2020, actuaciones de reconocidos actores y mucho humor._**

Luego de que apareciera en pantalla una inscripción de “LaFlia contenidos presenta”, el histórico conductor se vio reflejado en el cuerpo de la reina Isabel II de The Crown que le preguntó si planeaba quedarse otro año en su hogar. Sin entender demasiado, cambió de canal y de repente apareció en el del personaje de Beth Harmon, la protagonista de Gambito de Dama. “Ay, cómo me está pegando no hacer el programa”, reflexionó desencajado.

Entonces pasaron a otra secuencia en la que Marcelo se había puesto manos a la obra y se encontraba ultimando detalles desde su casa del debut de su programa. Allí la empleada doméstica era nada más y nada menos que Soledad Silveyra que se lamentaba por no haber aceptado el trabajo de Susana Giménez.

Acto seguido salió de su casa para ir al canal. Pero tuvo un percance al ver que tenía pinchada la rueda del auto. Allí conversó con el portero del edificio -interpretado por Jey Mammón- que le contó los chismes de sus vecinos y casi mete la pata al decirle que su esposa le era infiel. Cuando se dispone a cambiar la cubierta apareció Oscar Ruggeri vestido con uniforme de policía con quien mantuvo un desopilante intercambio futbolístico en el que no faltó alguna que otra chicana.

Inmediatamente, el conductor aparece en la peluquería de “El Tano”, donde la recepcionista -que es interpretada por Laurita Fernández- está más interesada en hacer un video para las redes. Entonces llega el Puma Goity que promete hacerle un cambio de look, pero termina vengándose por ser del equipo contrario.

Como si todo esto no fuera suficiente para el conductor en el día de su debut, se encuentra con Guillermina Valdés que hace de una mamá superada por las clases virtuales y finalmente le pasa factura al confundirlo con Matías Lammens.

Sin embargo, el momento de mayor alegría parecía ser cuando se encuentra con Pablo Echarri para ofrecerle ser parte de la apertura de ShowMatch, pero tras una seguidillas de malos entendidos se va ofendido al grito de “¡Aguante MasterChef!”.

Después de un día desastroso, Tinelli llegó al canal pero fue interceptado por el guardia de seguridad, Pablo Codevilla, que no lo deja pasar hasta que no se hisope. Entonces aparece en escena el médico que es Adrián Suar y los tres mantuvieron una insólita conversación.