+++
author = ""
date = 2020-09-01T21:00:00Z
description = "Así fue informado por el sello productor Sony, si bien el filme tenía previsto su rodaje en el verano boreal."
image = "/images/5f5e98a292de9_1004x565-2.jpg"
image_webp = "/images/5f5e98a292de9_1004x565-1.jpg"
title = "El rodaje de \"Spider-Man 3\" deberá esperar a que Tom Holland supere el posible virus"

+++
##### Así fue informado por el sello productor Sony, si bien el filme tenía previsto su rodaje en el verano boreal.

![](https://www.telam.com.ar/advf/imagenes/2020/09/5f5e98a292de9_1004x565.jpg)

_El rodaje en julio, en Atlanta, ya había sido postergado_

Lo interesante es que a Holland no se le revelaron síntomas de coronavirus pero igual que durante el pasado marzo los médicos le aconsejaron someterse a una cuarentena y el intérprete británico de 24 años prefirió no correr riesgos, reforzó la fuente.  
  
El impacto de la pandemia de Covid-19 sigue haciendo mella en la industria cinematográfica y los estudios buscan la mejor forma de reanudar las producciones siguiendo las medidas sanitarias requeridas. Y aunque algunos directores como Matt Reeves (“The Batman”) o James Cameron (“Avatar”) volvieron al trabajo, otros tendrán que esperar un poco más.  
  
Según informa la página especializada The Direct, la productora decidió retrasar el rodaje de la tercera aventura en solitario de “Spider-Man” que protagoniza el precavido Tom Holland y dirige Jon Watts, hasta enero o febrero de 2021.  
  
La tercera película de “Spider-Man” promete ser muy distinta de las predecesoras, con el Hombre Araña desenmascarado en público por el villano Mysterio y un Peter Parker que busca su propio lugar en el mundo, más allá de la sombra de los Vengadores. En cuanto a los villanos, se rumorea que podría aparecer Kraven o incluso el Doctor Octopus.