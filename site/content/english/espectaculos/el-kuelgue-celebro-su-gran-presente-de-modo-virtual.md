+++
author = ""
date = ""
description = "El sexteto que reúne a Julián Kartún, como la voz principal, participó del ciclo de recitales por streaming \"Se siente en casa\" y ofreció a su público canciones inéditas."
image = "/images/5f5cd38e85361_1004x565-1.jpg"
image_webp = "/images/5f5cd38e85361_1004x565.jpg"
title = "El Kuelgue celebró su gran presente de modo virtual"

+++
El ascendente y performático grupo rockero El Kuelgue protagonizó anoche una nueva velada del[“Se siente desde casa”](https://www.movistararena.com.ar/), primer ciclo de recitales por streaming desde el estadio Movistar Arena, donde repasó su historia de **16 años** plasmada **en tres álbumes y un EP**.

Para recorrer su historia para el show virtual, el sexteto que reúne a Julián Kartún (voz), Santiago Martínez (voz y teclados), Juan Martín Mojoli (bajo), Nicolás Morone (guitarra), Pablo Vidal (saxo) y Tomás Baillie (batería), recurrió a CYBER 1995, un robot humanoide cuyo último deseo antes de su obsolescencia programada era ver un show de su banda preferida. 

El Kuelgue, además, **ofreció algunas canciones inéditas**, interpretó un cover de Los Abuelos de la Nada, “Ir a más” y cerró su actuación encadenando “Chiste”, “Por ahora” y el hit “Góndola”.

La apertura de la velada estuvo a cargo de Mel Muñiz, en el marco de Festival Nuestro 2020 y “Nuevos talentos”, quien entonó “Ni una gota de amor”, “Harta”y “Apasionadamente somos hermanas”.

En la continuidad del ciclo por streaming desde el Movistar Arena se anuncian a MYA & Ruggero (el 19), David Lebón (el 26), Axel (el 27) y para octubre Vicentico (el 3), Dante Spinetta (el 9), Soledad. (el 12) y Bandalos Chinos (el 29).