+++
author = "John Doe"
date = 2021-03-23T03:00:00Z
description = ""
image = "/images/viviana-canosa-954982.jpg"
image_webp = "/images/viviana-canosa-954982.jpg"
title = "EDI ZUNINO: \"VIVIANA CANOSA ES LA NIETA FATAL DE LA DOÑA ROSA DE NEUSTADT\""

+++

**_Viviana Canosa siempre estuvo en medio de escándalos varios desde sus programas de chimentos y espectáculo, pero desde hace un tiempo la periodista se convirtió en el eje de la polémicas. Edi Zunino, Director de Contenidos Digitales y Multimedia del Grupo Perfil, realizó una columna radial en la que analizó esta realidad._**

"Me dirán que ocuparse de Viviana Canosa no vale la pena o que es poco serio o que me gusta el periodismo de periodistas… y puede ser, pero la exchimentera de los pelos colorados tiene un programa que puede ver cualquier desprevenido, ahora se dedica a la política y hace el pase a las 8 de la noche con un periodista de los considerados “serios” como Luis Novaresio. No sería de extrañar que, en un futuro no muy lejano, Canosa se consolide como integrante del llamado círculo rojo por sus posturas cada vez más desencajadas: otros que hicieron ese trayecto antes que ella", comenzó el periodista en Radio Perfil, en una columna titulada: El Síndrome Viviana Canosa sería más contagioso que el Covid-19.

"Canosa juega a ser una especie de petit fille fatal de aquella Doña Rosa inexistente pero tan estereotípica con la que interactuaba el viejo Bernardo Neustadt. Lástima que una mujer de estos tiempos que se pretende editorialista busque agrupar con más efusividad y desparpajo que ideas a esa parte de la audiencia que sólo busca encontrar un espejo donde ver reflejados sus más bajos instintos", reforzó Zunino.

Y continuó: "Pero el problema no es Canosa, claro que no. Ella es libre de hacer lo que le plazca, desde tomar dióxido de cloro en cámara -por más que sea considerado poco menos que un veneno- a amenazar, como hizo ayer, con una rebelión contra las restricciones impuestas por un aumento del 35% por ciento de los casos de Covid-19 en la última semana. El problema es el Síndrome Canosa, el efecto, porque hay gente dispuesta a creer que de esas maneras se puede arreglar algo de este constante desarreglo. O sea, viendo y escuchando barbaridades desde el sillón del living.

Tampoco el problema es que Canosa sea opositora, ya que, antes que ella, fue Patricia Bullrich que convocó a rebelarse contra las tibias medidas sanitarias. Hace un rato, Diego Santilli, que además de ser vicejefe del Gobierno porteño es un opositor, dijo, muy preocupado, que 'las manifestaciones generan aglomeraciones y riesgos de contagio'”.

Para finalizar, Edi no anduvo con rodeos: "El verdadero asunto es que una epidemia de irracionalidad sería suicida. Y eso no suena bien, por más que suicidarse en cámara pueda dar un buen número en la tele y el video más viralizado de la historia".