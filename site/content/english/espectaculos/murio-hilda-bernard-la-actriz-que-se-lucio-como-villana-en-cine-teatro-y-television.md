+++
author = ""
date = 2022-04-20T03:00:00Z
description = ""
image = "/images/gqzwimbzmi3dkzlfmeytqyjqgi.jpg"
image_webp = "/images/gqzwimbzmi3dkzlfmeytqyjqgi.jpg"
title = "MURIÓ HILDA BERNARD, LA ACTRIZ QUE SE LUCIÓ COMO VILLANA EN CINE, TEATRO Y TELEVISIÓN"

+++
#### La artista tenía 101 años. Protagonizó programas de radio, televisión y obras de teatro, entre ellos “Floricienta”, “Chiquititas” y “Rebelde Way”.

A los 101 años, murió la actriz Hilda Bernard, dejando como legado su larga trayectoria en la televisión argentina. La artista fue figura, entre otros programas, de _Chiquititas_, _Rebelde Way_ y _La extraña dama._ Con sus personajes, conquistó el corazón de millones y pasó a la historia.

“Con gran dolor despedimos a la actriz Hilda Bernard, una de las más importantes referentes de la actuación de nuestro país, quien se destacó en cine, teatro, televisión y radio. Era nuestra afiliada más longeva, registrada en 1942 bajo el número 26″, escribieron desde la Asociación Argentina de Actores, al confirmar la noticia.

El año pasado, durante el festejo de su último cumpleaños, lo celebró a lo grande. Para eso, se mostró en redes sociales rodeada por sus seres queridos. En esa ocasión, una de las que le dedicó tiernas palabras fue Cris Morena. _“_101 años llenos de talento, magia y amor. Eterna Carmen, Hilda, Nilda... todos tus personajes nos marcaron… hoy te abrazo y te festejo”, escribió la productora junto a un video con varias de sus mejores escenas y videoclips.

##### Quién era Hilda Bernard, la “villana” más querida de las telenovelas

Bernard tuvo una larga carrera en la TV. Fue parte de _Alta comedia, El amor tiene cara de mujer, Un mundo de veinte asientos, María de nadie, La extraña dama, Celeste, Antonella, Chiquititas, Rebelde Way_, entre otras ficciones.

* _Chiquititas_
* _Floricienta_
* _Rebelde Way_
* _Muchacha italiana viene a casarse_
* _Rosa... de lejos_
* _Malparida_
* _Se dice amor_
* _La extraña damafer_

##### Qué películas protagonizó Hilda Bernard

Hilda Bernard no sólo fue una estrella de la televisión argentina sino también que será recordada por los papeles que interpretó en el cine. En ese sentido, estos fueron algunos de los elencos que integró durante su carrera:

* _Cama adentro_
* _Mala gente_
* _Cuerpos perdidos_
* _El reclamo_
* _Vení conmigo_
* _Autocine mon amour_
* _La sombra de Jenninfer_