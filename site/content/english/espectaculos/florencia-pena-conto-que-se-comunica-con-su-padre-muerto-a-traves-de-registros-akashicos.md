+++
author = ""
date = 2021-08-01T03:00:00Z
description = ""
image = "/images/maxresdefault-1.jpg"
image_webp = "/images/maxresdefault.jpg"
title = "FLORENCIA PEÑA CONTÓ QUE SE COMUNICA CON SU PADRE MUERTO A TRAVÉS DE REGISTROS AKÁSHICOS"

+++

**_El programa que conducido por Andy Kusnetzoff en la pantalla de Telefe, Podemos Hablar (PH), recibió a la actriz y conductora Florencia Peña; al actor Tomás Fonzi; a la periodista Marcela Tauro; y a los músicos Manuel Wirtz y Joaquín Levinton, líder de la banda Turf._**

Durante la dinámica del Punto de Encuentro, la consigna interpeló a aquellos “que creen en las energías y la espiritualidad”. En medio de experiencias de diversa índole, Florencia dio un paso al frente para recordar a su padre Julio, quien falleció en abril del año pasado.

> “Te vi pelear con tanta fuerza, te vi luchar con el cuerpo y con el alma contra una enfermedad que siempre te llevo la delantera, pero que te puso a prueba cada día”, escribió la artista en sus redes sociales para despedir a su papá, que según contó, enfrentaba “un cáncer terminal” desde junio de 2018.

“En estos dos años -contó la actriz tiempo después a Teleshow- mi papá me mostró una fuerza... Le puso el cuerpo a una enfermedad... lo vi atravesar... El cáncer de páncreas te consume, te consume. Y lo vi luchar por nosotros, por no irse tan pronto, por darnos tiempo, por sanar cosas...”, señaló la conductora de Flor de equipo, que recordó cómo fue la despedida en plena fase uno de la cuarentena. “Fue difícil porque no teníamos a nuestros afectos acompañándonos, pero en definitiva es un pensamiento muy egoísta: su entorno más chiquito, por quienes mi papá luchó, nosotros... estuvimos ahí para él”, agregó.

Pasó más de un año y la conductora aprovechó la consigna para volver a traer su recuerdo. “No creo en la muerte, creo que la muerte es un paso hacia otro lugar mejor que este”, comentó a modo de introducción y habló de la conexión espiritual con su padre a través de la apertura de registros akáshicos. A partir de una serie de sesiones virtuales con su guía espiritual, logró contactarse con su padre y animó a Norma, su madre, para que hiciera el intento. “Está muy triste. Fueron cincuenta años de casados, es como si le hubieran extirpado una parte de su ser”, señaló. “Costó convencerla”, agregó, pero la espiritualidad le ganó a la reticencia e hicieron juntas la sesión.

Madre e hija se enfrentaron a la experiencia, sin saber lo que las esperaba. Y el resultado fue sorprendente: “Cuando ella abre los registros, lo primero que me dice es ‘acá está tu papá'”, reveló Florencia, que pudo dialogar con su padre. Luego llegó el turno de Norma, que lo había cuidado los últimos años de la enfermedad. “Mi papá tenía una manta roja con la que ella lo tapaba cuando tenía frío”, explicó Florencia, y detalló lo que su padre le pidió a su madre a través de la medium.

“Primero, que por favor saques las cenizas del cuarto y las lleves a hacer un último viaje en bicicleta, que era lo que hacían cuando vivían en Córdoba. Y que sepas que estoy, y cuando me necesites, te tapes con la manta roja que yo voy a estar ahí”, contó al borde de las lágrimas: no había forma de que la guía supiera que las cenizas estaban en la habitación, ni de los paseos en bicicleta, ni de la manta roja.

A la hora de compartir las experiencias, la emoción las había alcanzado por completo. “Mi mamá no podía parar de llorar”, afirmó la conductora, y agregó que su madre iba con la esperanza de, a lo sumo, recibir un placebo, pero se encontró con algo parecido a la sanación. “Cuando apareció mi papá y le dijo lo de la manta y lo de las cenizas, y que su misión ya había terminado y que todavía tenía cosas para hacer....”, enumeró la actriz, y cerró con contundencia: “Mi mamá está mejor desde ese momento”.