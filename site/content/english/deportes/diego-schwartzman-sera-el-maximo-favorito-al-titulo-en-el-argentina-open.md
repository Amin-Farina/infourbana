+++
author = ""
date = 2021-02-02T03:00:00Z
description = ""
image = "/images/schwartzman.jpg"
image_webp = "/images/schwartzman.jpg"
title = "Diego Schwartzman será el máximo favorito al título en el Argentina Open"

+++

**_Los otros argentinos que participarán del torneo serán Guido Pella, Federico Delbonis, Juan Ignacio Lóndero y Federico Coria, más los que intentarán sumarse desde la clasificación._**

Diego Schwartzman, actual número uno del tenis en Sudamérica, será el máximo favorito al título en la próxima edición del Argentina Open a celebrarse en el Buenos Aires Lawn Tennis Club entre el 1 y 7 de marzo próximos, según la lista oficial que dio a conocer hoy la organización.

Schwartzman, ubicado en el noveno puesto del ranking mundial de la ATP, fue finalista del Argentina Open en 2019 (perdió la definición con el italiano Marco Cecchinato) y semifinalista el año pasado, y partirá en la próxima edición como primer cabeza de serie, por delante del chileno Cristian Garín (22), el francés Benoit Paire (28) y el serbio Miomir Kecmanovic (42), según lo anticipó la organización del ATP porteño.

Entre los argentinos que jugarán el torneo, además del "Peque", están Guido Pella (44), Federico Delbonis (77), Juan Ignacio Lóndero (81) y Federico Coria (92), más los que intentarán sumarse desde la clasificación.

En cuanto a la nómina de extranjeros, están inscriptos el serbio Laslo Djere (56), el estadounidense Frances Tiafoe (62), los italianos Cecchinato (79) y Salvatore Caruso (76), los españoles Albert Ramos (46), Pablo Andújar (59) y Pedro Martínez (87), el uruguayo Pablo Cuevas (70) y el portugués Joao Sousa (93), entre los más destacados.

El Argentina Open tendrá en su edición 2021, que se jugará con no más de 200 personas en el estadio debido a la pandemia de coronavirus, a un nuevo campeón, ya que no se anotó el noruego Casper Ruud (27), quien venció el año pasado en la final al lusitano Pedro Sousa (108).