+++
author = ""
date = 2021-01-20T03:00:00Z
description = ""
image = "/images/pochetino.jpg"
image_webp = "/images/pochetino.jpg"
title = "Pochettino se mantiene \"atento\" a la situación de Messi en Barcelona"

+++

**_El DT del Paris Saint Germain no pierde las espereanzas de fichar al rosarino, pero sabe que depende de la voluntad del astro que debe decidir si sigue en el club catalán después del 30 de junio._**

El entrenador del París Saint Germain (PSG), el argentino Mauricio Pochettino, dijo este miércoles que se mantiene "atento" a la situación que atraviesa Lionel Messi en el Barcelona pese a que ignora si será posible que su club lo contrate una vez que concluya el vínculo del astro rosarino con los catalanes el 30 de junio próximo.  
  
"Uno siempre sueña con tener a los mejores en el equipo. No sé si será posible fichar a Messi y cualquier declaración de mi parte se puede malinterpretar, así que prefiero no hacerlo, pese a que estoy atento a la situación que atraviesa en Barcelona", expresó Pochettino en declaraciones a Cadena SER, de España, que consignó el periódico catalán Sport.  
  
El argentino, de 48 años, asumió este año la conducción del PSG y actualmente se encuentra sometido a un período de cuarentena por haber contraído coronavirus.  
  
Por su parte, el PSG, ganador de la Liga y la Supercopa de Francia, y finalista de la Liga de Campeones (perdió con el Bayern Munich), planifica su futuro y sigue con atención el tema Messi, según lo reveló en las últimas horas el director deportivo del club el brasileño Leonardo.