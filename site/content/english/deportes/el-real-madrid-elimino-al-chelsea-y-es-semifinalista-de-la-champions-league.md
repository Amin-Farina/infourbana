+++
author = ""
date = 2022-04-12T03:00:00Z
description = ""
image = "/images/frgazw2yznbhdfmyh3ipb2h6d4.jpg"
image_webp = "/images/frgazw2yznbhdfmyh3ipb2h6d4.jpg"
title = "EL REAL MADRID ELIMINÓ AL CHELSEA Y ES SEMIFINALISTA DE LA CHAMPIONS LEAGUE"

+++
#### Los dirigidos por Carlo Ancelotti perdieron 3-2 en el Santiago Bernabéu, pero por la victoria 3-1 que habían cosechado en Inglaterra se quedaron con el boleto

Real Madrid es semifinalista de la Champions League. El cuadro blanco perdió 3-2 con Chelsea como local en un duelo que se definió en el alargue, pero por el triunfo en Londres se impuso en la serie por 5-4. Para el local marcaron Rodrygo y Karim Benzema, mientras que para los de Thomas Tuchel lo hicieron Mason Mount, Antonio Rüdiger y Timo Werner. En la siguiente instancia el cuadro blanco se verá las caras con el ganador del cruce entre Atlético de Madrid y Manchester City.

Fue un primer tiempo parejo en España. El conjunto inglés logró ponerse en ventaja tras una gran acción colectiva por izquierda se cambió de ritmo tras una gran asociación entre Ruben Loftus-Cheek, Timo Werner y Mason Mount. El estadounidense, gracias a su diagonal entre líneas, logró pisar el área con la pelota en su poder y no perdonó al quedar cara a cara con Courtois.

La tenencia del balón fue repartida entre ambos conjuntos y los dos tuvieron las mismas falencias al tenerlo: les costó encontrar huecos entre la defensa adversaria. Fue por eso que ambos, sobre todo el Real Madrid, parecían sentirse más cómodos jugando de contragolpe. Fue justamente de esa manera que el local tuvo sus chances más claras, casi todas iniciadas con acciones individuales de Vinícius Junior.

En el complemento, Chelsea gritó el 2 a 0 de arranque, antes de los cinco minutos cuando en un tiro de esquina enviado al segundo palo apareció Antonio Rüdiger por atrás de todos y metió el frentazo para igualar la serie 3-3.

A los 18 minutos ocurrió la gran polémica del encuentro cuando Marcos Alonso desbordó por izquierda, dejó en el camino a Dani Carvajal tras un rebote y de derecha clavó la pelota en un ángulo. Sin embargo, el VAR detectó que el balón dio en la mano izquierda del lateral español y anuló la acción.Tras esa acción, Benzema hizo sonar el travesaño de Mendy con un cabezazo, pero más allá de esa acción, era claro que el cuadro británico era superior en el juego. Por eso, Carlo Ancelotti mandó a la cancha al joven Camavinga en lugar de Toni Kroos, en un intento para recuperar la mitad de cancha.

El que marcó el 3 a 0 fue Timo Werner. El alemán armó una jugada fantástica por izquierda, dejó en el camino a dos rivales y cuando quedó mano a mano con Courtois amagó dos veces hasta encontrar el hueco, para definir fuerte por abajo y poner por primera vez al Chelsea arriba en la llave.

Parecía entonces que los de Thomas Tuchel festejarían e n el Santiago Bernabéu al conseguir una remontada épica, pero al Real Madrid nunca hay que darlo por muerto. A los 35 minutos, Luka Modric, más suelto tras la salida de Casemiro y Kroos, dominó por izquierda y sin pensarlo demasiado lanzó un pase aéreo con la cara externa de su pie derecho para la corrida al vacío de Rodrygo, que había entrado un rato antes. El extremo brasileño definió de primera con la cara interna y gritó el 1-3 para empatar la serie 4-4. De esta manera, la definición se estiró al alargue.

En el primer tiempo del alargue, el Real Madrid pudo descontar. Fue Camavinga, que entró muy bien, el que recuperó en zona media y soltó rápido para el desborde de Vinícius Jr. El brasileño llegó hasta el fondo, levantó la cabeza y como en la ida en Stamford Bridge levantó el centro para que Karim Benzema, de cabeza, estampara el 2-3 en el partido y el 5-4 para el conjunto blanco en la serie.

En la segunda mitad, Tuchel mandó a la cancha a Saúl y a Jorginho para renovar el mediocampo y eso le permitió adueñarse de la pelota y tener algunas oportunidades de gol con Ziyech y Havertz, pero una gran tapada de Courtois y una mala definición del alemán, privaron al elenco vestido de amarillo de gritar el cuarto.

Real Madrid pudo aguantar las embestidas de Chelsea y con puro corazón festejó en el Santiago Bernabéu, una vez más, una clasificación heroica que estuvo a punto de escapársele. Nuevamente con Vinicius Júnior, Luka Modric y Karim Benzema, quien terminó lesionado, como figuras los de Carlo Ancelotti se metieron en las semifinales en donde cho carán an te el ganador de Manchester City vs. Atlético de Madrid.

Del otro lado de la llave, Villarreal dio el batazao an te Bayern Múnich y espera por el ganador de Liverpool y Benfica, serie que se define el miércoles y tiene en ventaja 2-1 a los ingleses.