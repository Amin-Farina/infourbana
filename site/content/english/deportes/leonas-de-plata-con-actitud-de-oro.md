+++
author = ""
date = 2021-08-06T03:00:00Z
description = ""
image = "/images/1628249439_980223_1628249540_noticia_normal_recorte1-1.jpg"
image_webp = "/images/1628249439_980223_1628249540_noticia_normal_recorte1.jpg"
title = "LEONAS DE PLATA CON ACTITUD DE ORO"

+++

**_El seleccionado nacional de hockey sobre césped perdió por 3 a 1 ante a las europeas. De esta manera obtuvieron el mejor resultado de toda la delegación argentina y aportaron la segunda presea después del bronce que ganaron Los Pumas 7s._**

El seleccionado argentino femenino de hockey sobre césped perdió con Países Bajos por un inapelable 3 a 1 en la final de los Juegos Olímpicos Tokio 2020 y se colgó la medalla de plata por tercera vez en la historia olímpica.

Las Leonas no tuvieron opciones frente al poderío del equipo europeo, que resolvió el partido con una ráfaga de goles en el segundo cuarto, todos a través de córners cortos facturados por la mediocampista Margot van Geffen y la defensora Caia Jacqueline van Maasakker en dos ocasiones.

En ese mismo período, a segundos del final, las argentinas lograron el descuento de Agustina Gorzelany por la misma vía.

Al margen de la frustración por el oro esquivo y de los llantos desatados sobre la cancha al sonar la bocina del final, Las Leonas cerraron un torneo formidable en el que demostraron la vigencia del hockey femenino luego de la profunda renovación experimentada por el equipo.

En ocho participaciones olímpicas, las chicas argentinas se hicieron acreedoras de cinco medallas y tres diplomas: medalla de plata en Sydney 2000, Londres 2012 y Tokio 2020; bronce en Atenas 2004 y Beijing 2008 y séptimo puesto en Seúl 88, Atlanta '96 y Río 2016.

En estos Juegos, Las Leonas lograron el mejor resultado de toda la delegación nacional y aportaron la segunda medalla después del bronce que ganaron Los Pumas 7s. durante la primera semana de competencia.

Países Bajos frustró a las argentinas por segunda vez (la anterior en Londres 2012), sumó su tercera medalla de oro y se cobró revancha de la final perdida por penales ante Gran Bretaña hace cinco años en Río de Janeiro.

El favoritismo del seleccionado europeo, bicampeón mundial vigente, se expresó con claridad en el primer tiempo de la final. Basadas en la presión sobre campo rival, las neerlandesas tomaron el control de la bocha, la gestionaron con lucidez durante el 60% del tiempo e impusieron condiciones reflejadas en las estadísticas.

Cinco córners cortos y nueve penetraciones al semicírculo fueron toda una demostración del poderío naranja, que en apenas un minuto y medio de partido se insinuó con un remate al travesaño de Belén Succi.

La Argentina procuró escalonarse ordenadamente en los metros finales de la cancha para cortar la circulación del rival y replicar con velocidad en ataque, pero pocas veces pudo encontrar la profundidad reclamada desde el banco por el entrenador Carlos Retegui.

La contención defensiva de Las Leonas funcionó durante el primer cuarto en base a coberturas y al despliegue físico para disputar cada pelota con la tenacidad propia de la identidad del equipo argentino.

Países Bajos, siempre ajustado a libreto, mantuvo la tendencia dominante de su juego en el segundo cuarto y lastimó sin piedad mediante el recurso del corto. De cuatro que dispuso en ese lapso de 15 minutos anotó tres con notable variedad de recursos.

El primero de Van Geffen significó un duro golpe para el equipo argentino, que hasta el descuento de Gorzelany en los segundos finales de ese mismo período, transitó su peor momento en la final.

El 1-3 dejó a Las Leonas en partido para el segundo tiempo pero, en rigor, las neerlandesas controlaron el juego sin mayores sobresaltos e incluso dispusieron una ocasión clarísima que Succi salvó en un mano a mano con la delantera Maria Verschoor en el tercer cuarto.

En el último segmento, el equipo de Retegui se jugó el resto en busca de la hazaña, hizo retroceder a su adversario en el campo pero sin chances claras de recortar la distancia en el marcador. Las dos opciones de córners cortos que dispuso no las pudo capitalizar.

En suma, Países Bajos jugó con el reloj a su favor hasta que la cuenta regresiva marcó su brillante coronación invicta con puntaje ideal.