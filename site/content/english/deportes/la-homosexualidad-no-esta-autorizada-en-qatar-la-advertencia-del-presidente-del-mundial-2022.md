+++
author = ""
date = 2021-11-30T03:00:00Z
description = ""
image = "/images/nasser-al-khater-1.jpeg"
image_webp = "/images/nasser-al-khater.jpeg"
title = "“LA HOMOSEXUALIDAD NO ESTÁ AUTORIZADA EN QATAR”: LA ADVERTENCIA DEL PRESIDENTE DEL MUNDIAL 2022"

+++
#### **_“La homosexualidad no está autorizada”, sentenció Nasser Al-Khater, presidente del comité organizador del Mundial de Qatar 2022 en una entrevista que brindó a la CNN cuando fue consultado por la política represiva que el emirato tiene sobre los derechos de las personas de la comunidad LGBTQ +_**

En este sentido, el ejecutivo aseguró que todas las personas que así lo deseen podrán ingresar a la nación de la próxima Copa del Mundo, aunque puntualizó que “las demostraciones públicas de afecto están mal vistas y esto se aplica a todos”, y utilizó la palabra “respeto” cuando explicó que eso es lo que la sociedad qatarí le exigirá a sus visitantes.

##### **El futbolista Josh Cavallo, recientemente declarado homosexual, será “bienvenido”**

Así lo aseguró Al-Khater cuando le preguntaron por la situación del futbolista australiano que el pasado 27 de octubre declaró abiertamente que es gay, mientras que tiempo después aseguró tener miedo de ir a Qatar por las severas penas a las que se arriesgan las personas de su condición. “Leí que en Qatar aplican la pena de muerte a la gente gay. Es algo de lo que tengo mucho miedo y me hace no querer ir allí”, había señalado el futbolista en diálogo con el podcast Guardian’s Today in Focus.

Al respecto, Al-Khater respondió: “Al contrario, le damos la bienvenida aquí en Qatar, incluso le invitamos a que venga a visitar el país antes del Mundial”, respondió el responsable del evento. Nadie se siente amenazado aquí, nadie se siente amenazado (reiteró). Qatar es un país tolerante. Es un país acogedor. Es un país hospitalario. Creo que esta percepción de peligro se debe a las múltiples acusaciones y noticias que dan una visión negativa del país“.

##### **Cárcel y hasta pena de muerte: el castigo en Qatar a la homosexualidad**

La homosexualidad es ilegal en ese país y se castiga, para empezar, con hasta tres años de prisión. Según un informe dado a conocer por Amnistía Internacional, 69 Estados miembros de la ONU todavía criminalizan los actos sexuales entre personas adultas del mismo sexo (67 por disposiciones legales explícitas y 2 de facto).

En seis de ellos -Arabia Saudí, Brunei, Irán, Mauritania, Nigeria (12 estados del norte) y Yemen-, la pena de muerte es un castigo legalmente prescrito. En otros cinco -Afganistán, Emiratos Árabes Unidos, Pakistán, Qatar y Somalia- esa sentencia podría llegar a imponerse siguiendo ciertos códigos legales o religiosos como, por ejemplo, la interpretación de la Sharia, que es la ley de la religión islámica que recoge un conjunto de supuestos mandamientos de Alá relativos a la conducta humana, pero hay menos certeza jurídica sobre la situación.

##### **Sobre los trabajadores inmigrantes fallecidos en Qatar**

Finalmente, al ser consultado sobre la cantidad de obreros muertos en el marco de las obras para el Mundial, el hombre lanzó una respuesta protocolar: “La gente también debería reconocer las responsabilidades que Qatar ha tomado para progresar, promulgar leyes, y proteger los derechos de los trabajadores y su bienestar”.

Cabe mencionar que un informe reciente de la Organización Internacional del Trabajo (OIT) encontró al menos 50 muertes y medio millar de lesiones graves en 2020 entre los trabajadores inmigrantes en el país.