+++
author = ""
date = 2022-05-26T03:00:00Z
description = ""
image = "/images/julian-alvarez-2.webp"
image_webp = "/images/julian-alvarez-2.webp"
title = "LOS 6 GOLES DE JULIÁN ÁÑVAREZ EN UNA NOCHE SOÑADA CON RIVER PLATE: A CUÁNTO QUEDÓ DE LA MARCA HISTÓRICA DE SANTOS BORRÉ "

+++
#### El Araña por primera vez anotó esa cantidad de tantos en un partido, una gesta que nunca se había logrado en la historia de la institución. Fue en el estadio Monumental, ante Alianza Lima, por la Copa Libertadores

River Plate logró una victoria histórica ante Alianza Lima, pero la actuación de Julián Álvarez se llevó todos los flashes. El delantero de 22 años convirtió seis de los ocho tantos con los que el Millonario goleó al elenco peruano en el estadio Monumental, por la última fecha de la fase de grupos de la Copa Libertadores.

Los restantes goles fueron obra de Santiago Simón y Elías Gómez. De este modo, el equipo de Marcelo Gallardo se aseguró no solo el liderazgo de su zona, sino también que terminó como el segundo mejor equipo de todos los primeros que se clasificaron a los octavos de final. Solo Palmeiras, con puntaje ideal, superó al elenco argentino.

El _Araña_ no para de romper marcas en River Plate, ya que se transformó en el primer jugador de toda la historia del club en marcar seis goles en un partido. El último futbolista que había logrado anotar cinco goles en un partido había sido Ignacio Scocco. El ex delantero de Newell’s lo hizo el 22 de septiembre de 2017, también en el Monumental, en la goleada de River Plate ante Jorge Wilstermann por 8 a 0, también por la última fecha de la fase de grupos. De esta manera, el Millonario confirmó su pase a los octavos de final de la Copa Libertadores.

En materia personal, Julián Álvarez también superó su propio récord, que era de cuatro tantos en un mismo juego. Además, el oriundo de Calchín, Córdoba, alcanzó 51 conquistas con la camiseta de River Plate en 115 partidos oficiales y acecha la marca histórica de Rafael Santos Borré, que con 55 es el máximo goleador de la era Marcelo Gallardo. El _Araña_ quedó a solo cuatro celebraciones del colombiano. Completa el podio Lucas Alario (41 goles en 82 encuentros). 