+++
author = ""
date = 2022-01-20T03:00:00Z
description = ""
image = "/images/seleccion-argentina-hinchas.jpg"
image_webp = "/images/seleccion-argentina-hinchas.jpg"
title = "ARGENTINA ES EL SEGUNDO PAÍS QUE MÁS ENTRADAS PIDIÓ PARA EL MUNDIAL"

+++

#### **_En las primeras 24 horas de venta se ubicó solo detrás de Qatar, el organizador. Los hinchas albicelestes mostraron su entusiasmo por la Copa del Mundo que se jugará a fin de año._**

Argentina se posicionó como el segundo país de mayor demanda de entradas para el Mundial Qatar 2022, solo detrás del organizador, cumplida la primera etapa de reserva de 24 horas, informó este jueves la FIFA.

En esa ventana de tiempo, el ente rector del fútbol mundial asignó un total de 1,2 millones de localidades, en su mayoría solicitadas desde el emirato anfitrión y con Argentina como primera nación extranjera con ventaja sobre México, Estados Unidos, Emiratos Árabes Unidos, Inglaterra, India, Arabia Saudita, Brasil y Francia, que completaron los diez primeros lugares.

La euforia vivida en Argentina desde la conquista de la Copa América 2021 en Río de Janeiro contra Brasil, con el gol de Ángel Di María, sumada a la posible última función de Lionel Messi llevaron a los hinchas a moverse en el primer día de solicitud de tickets.

De hecho, en comparación con Rusia 2018, el inicio de la demanda lo mejoró, ya que en aquella ocasión se colocó tercero en el pedido, pero con el tiempo terminó cayendo al séptimo lugar.

El Mundial se jugará desde el 21 de noviembre al 18 de diciembre en Qatar.

##### La lista de Scaloni para jugar ante Chile y Colombia

Sin Lionel Messi y con varios futbolistas que habitualmente no son convocados, Lionel Scaloni dio a conocer la nueva lista de la Selección argentina para los próximos partidos de Eliminatorias Sudamericanas contra Chile y Colombia.

La ausencia de Messi en el seleccionado fue acordada entre el cuerpo técnico y el futbolista para que pueda recuperarse plenamente en lo físico luego de haber tenido Covid-19.

La lista de la Selección se presentó le miércoles, para jugar ante Chile y Colombia entre el 27 de enero y el 1 de febrero.

La principal novedad se trata de la convocatoria del ex Boca Alexis Mac Allister, del Brightón inglés, que está en un alto nivel en la Premier League. También resalta la vuelta de Lucas Ocampos luego de haber quedado afuera de la Copa América y reaparece Esteban Andrada como cuarto arquero.