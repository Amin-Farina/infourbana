+++
author = ""
date = 2022-04-05T03:00:00Z
description = ""
image = "/images/leandro-paredes.webp"
image_webp = "/images/leandro-paredes.webp"
title = "ALERTA EN LA SELECCIÓN ARGENTINA: LEANDRO PAREDES DEBERÁ SER OPERADO POR UNA LESIÓN"

+++
#### El mediocampista del París Saint Germain sufrió un desgarro en el aductor y aprovechará la inactividad para pasar por el quirófano en busca de solucionar una dolencia en el abdomen que viene arrastrando hace tiempo

Los días posteriores al sorteo del Mundial de Qatar 2022 los fanáticos de la selección argentina comenzaron a prender velas para que ningún futbolista que conforma el plantel liderado por Lionel Scaloni se lesione. Sin embargo, la racha duró poco con Ángel Di María y una dolencia muscular al que se le sumó Leandro Paredes tras el encuentro del último fin de semana entre el París Saint Germain y el Lorient, correspondiente a la jornada 30 de la Ligue 1.

El volante argentino aguantó hasta los 38 minutos de la primera mitad: hizo gestos de dolor en el aductor izquierdo y automáticamente Mauricio Pochettino lo reemplazó por Georginio Wijnaldum. En la repetición de la acción se vio cómo el mediocampista argentino intenta recuperar una pelota y al estirar la pierna derecha siente el pinchazo. Pochettino mostró una cara de preocupación que seguramente repetirá este martes al enterarse que se trata de un desgarro.

Según informaron _ESPN_ y _TyC Sports_, el futbolista “tiene que operarse ya que se rompió la inserción del abdomen y se lastimó el aductor”. Por otro lado, aprovecharía esta inactividad para también intervenirse por una dolencia en la zona emparentada a una pubalgia. Lo cierto es que los tiempos de recuperación pactados tienen fecha para dentro de dos meses y Paredes se perdería la Finalissima contra Italia del 1 de junio en el estadio de Wembley. Scaloni tiene como rueda de auxilio a Guido Rodríguez y deberá pensar una tercera opción para sumar a la próxima convocatoria que irá por el título internacional.

Vale recordar que Leandro venía de disputar la doble fecha de Eliminatorias Sudamericanas, que finalizaron con triunfo sobre Venezuela en la Bombonera y empate en Guayaquil ante Ecuador, y que en su primera presentación con la camiseta del PSG tras la ventana internacional se lesionó. Junto a Rodrigo De Paul y Giovani Lo Celso, la figura del volante surgido en Boca Juniors es muy importante en el mediocampo de la _Albiceleste_ y es una de las piezas claves del entrenador para extender la racha invicta hasta los 31 partidos sin conocer la derrota.

Más allá de las alarmas que se encendieron, la lesión no pone en riesgo la participación de Paredes en la próximo Copa del Mundo y se proyecta que para principios de junio regrese a los entrenamientos junto a sus compañeros en Francia. En tanto, el pasado viernes, en Doha, se llevó a cabo el sorteo del Mundial de Qatar 2022 y Argentina conoció a sus rivales en el Grupo C: Arabia Saudita, México y Polonia.