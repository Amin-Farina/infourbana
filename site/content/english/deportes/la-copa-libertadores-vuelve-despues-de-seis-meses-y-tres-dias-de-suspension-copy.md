+++
author = ""
date = 2020-09-14T21:00:12Z
description = "Debido a la pandemia, el reglamento de la Libertadores permitió a los clubes ampliar la lista de buena fe de 30 a 50 jugadores para afrontar cualquier complicación por contagios."
image = "/images/834337.jpg"
image_webp = "/images/834337.jpg"
title = "Nadal criticó la iniciativa de Djokovic de crear un sindicato de jugadores"

+++
_Debido a la pandemia, el reglamento de la Libertadores permitió a los clubes ampliar la lista de buena fe de 30 a 50 jugadores para afrontar cualquier complicación por contagios._

El español también se mostró molesto debido a que tantó él como el suizo Roger Federer se enteraron la iniciativa del serbio a través de una carta y fueron consultados previamente.

El español Rafael Nadal criticó al serbio Novak Djokovic, número uno del tenis mundial, no por su descalificación en el US Open, tras darle un pelotazo a una jueza de línea, sino por haber creado un nuevo sindicato de jugadores al margen de la ATP.

Nadal, que se prepara para reaparecer en el Masters de Roma, no ve con buenos ojos la Asociación de Jugadores creada por "Nole", al señalar que "hay varias cosas que a mi modo de entender no son adecuadas. Y este es un momento para estar más juntos que nunca, no que cada uno mire por sus intereses", según publicó el diario español Mundo Deportivo.

"La iniciativa es totalmente respetable, nada que objetar. Cada uno puede tener sus ideas y luchar por ellas, pero creo que hay maneras de cómo hacer las cosas", agregó el tenista mallorquí.

Nadal se quejó también porque “estando en el consejo (de la ATP) no nos tenemos que enterar ni Federer ni yo a través de una carta. Si quieren que estemos apoyando eso, lo normal es que sepamos las cosas con antelación"

"Después, eso no es para mí, yo sí creo en la estructura de la ATP, con matices, porque hay cosas que se pueden mejorar”, afirmó el tenista español.

Finalmente, para Nadal la acción que le costó a Djokovic la eliminación en los octavos de final de Flushing Meadows "fue algo desafortunado, pero la regla dice que es descalificación, lo lamento por él".