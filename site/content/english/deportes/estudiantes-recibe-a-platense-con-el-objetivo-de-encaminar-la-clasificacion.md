+++
author = ""
date = 2021-05-03T03:00:00Z
description = ""
image = "/images/516d7957556e6e36644b4859706e4e754273754c37413d3d.jpg"
image_webp = "/images/516d7957556e6e36644b4859706e4e754273754c37413d3d.jpg"
title = "ESTUDIANTES RECIBE A PLATENSE CON EL OBJETIVO DE ENCAMINAR LA CLASIFICACIÓN"

+++

**_El encuentro, que cierra la 12ma fecha, se jugará desde las 18 en el estadio Jorge Luis Hirschi, con el arbitraje de Leandro Rey Hilfer, y será transmitido por la TV Pública, ESPN y Fox Sports Premium._**

Estudiantes de La Plata recibe este lunes a Platense en busca de la victoria que lo dejará perfilado para clasificarse a los cuartos de final de la Copa de la Liga Profesional.

El partido que cerrará la 12ma fecha de la zona A se disputará desde las 18 en el estadio Jorge Luis Hirschi, con arbitraje de Leandro Rey Hilfer, y será transmitido por la TV Pública, ESPN y Fox Sports Premium.

Estudiantes tiene 19 puntos y en la última fecha dio un gran paso hacia la clasificación tras vencer a Rosario Central por 1-0 en el Gigante de Arroyito.

Platense perdió el lunes pasado contra Aldosivi por 2-0, como local, y con 11 unidades es uno de los tres peores de la zona A junto a su último rival y Arsenal.

El "Pincha" de Ricardo Zielinski suma cuatro partidos sin perder con dos triunfos y dos empates y saldrá a la cancha con todos los resultados puestos de sus competidores.

El equipo de Juan Manuel "Chocho" Llop tuvo sus mejores resultados como visitante ya que en esa condición consiguió 10 de los 11 puntos que suma en su regreso a la máxima categoría.

El DT del "Calamar" no podrá contar con Mauro Bogado, expulsado, pero recuperó al capitán Hernán Lamberti, quien superó el coronavirus pero todavía está en duda su titularidad ya que recién se reincorporó al plantel el pasado viernes.

#### Probables formaciones

Estudiantes: Mariano Andújar; Leonardo Godoy, Agustín Rogel, Fabián Noguera y Nicolás Pasquini; Ángel González, Jorge Rodríguez, Juan Sánchez Miño y Lucas Rodríguez; Martín Cauteruccio y Federico González. DT: Ricardo Zielinski.

Platense: Jorge De Olivera; Brian Lluy, Nicolás Zalazar, Luciano Recalde y Juan Infante; Roberto Bochi y Cristian Núñez o Hernán Lamberti; Franco Baldassarra, Alexis Messidoro y Nicolás Bertolo; Jorge Pereyra Díaz. DT: Juan Manuel Llop.

Árbitro: Leandro Rey Hilfer

Estadio: Estudiantes (La Plata)

Hora: 18:00.

TV: TV Pública, ESPN y Fox Sports Premium.