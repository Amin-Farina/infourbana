+++
author = ""
date = 2022-01-27T03:00:00Z
description = ""
image = "/images/f608x342-41091_70814_0-1.jpeg"
image_webp = "/images/f608x342-41091_70814_0.jpeg"
title = "LA SELECCIÓN ARGENTINA SE MOSTRÓ CRITICA POR LAS DEMORAS PARA INGRESAR A CHILE"

+++
##### 

##### **_Dibu Martínez fue uno de los jugadores que estalló en las redes sociales por los controles, que incluyeron perros antidrogas. Este jueves la albiceleste enfrenta a la Roja en Calama._**

Los futbolistas de la Selección argentina expresaron en las redes sociales la molestia por el mal recibimiento que tuvieron en la noche del miércoles en el aeropuerto de Calama, Chile.

El avión que trasladaba a la Albiceleste aterrizó antes de las 20.30 en el país trasandino. La idea era ir rápidamente para el hotel para poder descansar. Sin embargo, el trámite se demoró mucho más de lo esperado: estuvieron cerca de dos horas varados y esto generó el malestar de jugadores y miembros del cuerpo técnico del conjunto nacional.

##### Los mensajes de bronca los jugadores de la Selección argentina en Instagram

Uno de los que hizo notoria su furia fue Emiliano “Dibu” Martínez. El arquero subió varias historias a su cuenta de Instagram. “Como siempre, quilombo... quilombo”, se lo escucha decir al arquero en una de ellas, mientras que muestra el accionar de las autoridades locales.

Inmediatamente compartió otro video en el que se puede ver a la policía revisar los equipajes con perros antidrogas. “¡Encontraron un Sugus!”, insinuó.

Otro que ironizó sobre la situación fue Nicolás Otamendi. El central compartió imágenes de lo que estaba ocurriendo y escribió: “Bueno, hoy dormimos acá (en el aeropuerto), pero no pasa nada”.

A las 22:30, finalmente el plantel argentino fue trasladado al hotel, donde pasará la noche a la espera del duelo del jueves por la fecha 15 de las Eliminatorias.

##### Lionel Scaloni dio positivo y no viajó a Chile

El test PCR de Lionel Scaloni dio positivo en coronavirus, por lo que no pudo viajar con la Selección argentina a Chile. Pablo Aimar tampoco fue al país trasandino por ser contacto estrecho.

Estoy curado, me siento bien. Estoy en condiciones para poder estar acá, pero el PCR sigue dando positivo”, reconoció Scaloni. Ante esa situación, el entrenador no puede ingresar a Chile. Los tres integrantes del cuerpo técnico que estarán en Chile al frente del combinado nacional son Walter Samuel, Roberto Ayala y Diego Placente.

A esa situación se sumó otro caso de Covid-19, pero entre los jugadores. “Por otra parte, cómo última noticia, Alexis Mac Allister dio positivo anoche. Él y (Emiliano) Buendía, por ser contacto estrecho, tampoco podrán viajar”, informó el DT del conjunto nacional en conferencia de prensa.

##### Quiénes estarán al frente de la Selección argentina en Chile

“Estoy curado, me siento bien. Estoy en condiciones para poder estar acá, pero el PCR sigue dando positivo”, reconoció Scaloni. Ante esa situación, el entrenador no puede ingresar a Chile. Los tres integrantes del cuerpo técnico que estarán en Chile al frente del combinado nacional son Walter Samuel, Roberto Ayala y Diego Placente.

##### Probables formaciones de Chile y Argentina

Chile: Claudio Bravo; Paulo Díaz, Gary Medel, Guillermo Maripán, Sebastián Vegas; Marcelino Núñez, Erick Pulgar, Charles Aránguiz; Ben Brereton, Eduardo Vargas y Alexis Sánchez. DT: Martín Lasarte.

Argentina: Emiliano Martínez; Nahuel Molina, Nicolás Otamendi, Lisandro Martínez, Marcos Acuña; Rodrigo De Paul, Leandro Paredes, Giovani Lo Celso; Ángel Di María, Lautaro Martínez y Nico González o Paulo Dybala. DT: Lionel Scaloni (no estará en el banco, en su lugar saldrán Walter Samuel, Roberto Ayala y Diego Placente).

* Hora: 21.15.
* Estadio: Zorros del Desierto (Calama).
* Árbitro: Anderson Daronco (Brasil).
* TV: TV Pública y TyC Sports.