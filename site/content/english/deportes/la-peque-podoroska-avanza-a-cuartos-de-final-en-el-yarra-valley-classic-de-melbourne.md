+++
author = ""
date = 2021-02-03T03:00:00Z
description = ""
image = "/images/nadia-podoroska-le-gano-a___jz0qjbnye_1200x630__1.jpg"
image_webp = "/images/nadia-podoroska-le-gano-a___jz0qjbnye_1200x630__1.jpg"
title = "La \"Peque\" Podoroska avanza a cuartos de final en el Yarra Valley Classic de Melbourne"

+++

**_Nadia Podoroska avanzó a los cuartos de final del WTA 500 Yarra Valley Classic, que se juega en Melbourne, Australia, tras un triunfo resonante sobre la "top ten" checa Petra Kvitova por 5-7, 6-1 y 7-6 (9-7)._**

La tenista argentina Nadia Podoroska se clasificó este miércoles a los cuartos de final del WTA 500 Yarra Valley Classic, que se juega en Melbourne, Australia, tras un triunfo resonante sobre la "top ten" checa Petra Kvitova por 5-7, 6-1 y 7-6 (9-7).

Podoroska, nacida en Rosario y ubicada en el puesto 47 del ranking mundial de la WTA, empleó dos horas y 39 minutos para doblegar a Kvitova (9) y conseguir así la segunda victoria de su carrera sobre una 'top ten', luego de la que había logrado en octubre del año pasado ante la ucraniana Elina Svitolina (5) en los cuartos de final de Roland Garros, donde cumplió una histórica actuación con su acceso hasta las semifinales.

La rosarina, de 23 años, asumirá su próximo partido el viernes frente a otra checa, Marketa Vondrousova (21), quien se impuso sobre la rusa Vera Zvonareva (156) por 7-6 (7-4), 6-7 (4-7) y 6-4.

Podoroska, quien no había comenzado bien el año ya que perdió el mes pasado en la ronda inicial del WTA de Abu Dhabi con la española Sara Sorribes por 6-3 y 6-3, recuperó su mejor versión en el Yarra Valley Classic, un torneo que oficia de aperitivo para su principal objetivo en el corto plazo, el abierto de Australia, primer Grand Slam del año a jugarse desde el lunes 8 de febrero en Melbourne.

La argentina se presentó en el certamen con una victoria sobre la británica Francesca Jones (244) por un cómodo 6-1 y 6-3, luego superó a la belga Greet Minnen (110) por 6-3 y 6-4, y hoy rendió de menor a mayor para doblegar a una tenista importante como Kvitova, doble campeona de Wimbledon, en las ediciones de 2011 y 2014, y finalista en Australia en 2019.

Podoroska perdió ajustadamente el primer set por 7-5, cuando jugó demasiado contenida en los momentos claves y eso permitió al lucimiento de la "zurda" Kvitova, con tiros más profundos y "winners" de ambos lados.

Lejos de achicarse, la rosarina tomó la iniciativa en el segundo parcial y mejoró su devolución; así quebró cuatro veces el servicio de la checa, que encima acusó un fuerte dolor en el pie izquierdo que motivó atención médica, en un combo que le significó perder el parcial por 6-1.

Podoroska mantuvo la iniciativa y sustentada en su buena derecha se adelantó 3-0 en el set definitivo, pero cuando parecía encaminarse al triunfo la checa emparejó hasta igualar en seis y forzar el tie break.  
  
En la definición rápida, la checa se adelantó 4-2 y luego estiró a 5-2, pero la rosarina reaccionó a tiempo y ganó cuatro puntos consecutivos para disponer de su primer match point.  
  
Podoroska falló el primero por apurarse demasiado, luego tuvo otro que la checa resolvió muy bien subiendo a la red, y en el tercero con su servicio forzó una devolución que se fue larga para sentenciar el 9-7 y festejarlo con el puño cerrado.  
  
La rosarina, quien comenzó a ser captada por el radar de las mejores del mundo el año pasado tras su increíble actuación en Roland Garros, donde superó la clasificación y llegó hasta las semifinales (perdió con la polaca Iga Swiatek, luego campeona), demostró que su juego puede adaptarse también a la superficie rápida de las canchas australianas, e intentará dar un pasito más en su próximo compromiso ante Vondrousova.  
  
En el caso de avanzar hasta las semifinales, Podoroska se encontraría en esa instancia con la española Garbiñe Muguruza (15) o la estadounidense Sofía Kenin (4).  
  
El WTA 500 Yarra Valley Classic se juega sobre superficie rápida, repartirá premios por 565.530 dólares y tiene como máxima favorita a la tenista local Ashleight Barty, número uno del mundo que regresó al circuito luego de casi un año sin competir por decisión propia a causa de la pandemia de coronavirus.

En tanto, en otros resultados de la jornada, la española Garbiñe Muguruza (15) le ganó a la rusa Anastasia Pavlyuchenkova (39); la estadounidense Serena Williams (11) venció a la búlgara Tsvetana Pironkova (137) por 6-1 y 6-4, y la norteamericana Danielle Rose Collins (46) derrotó a la checa Karolina Pliskova (6) por 7-6 (7-5) y 7-6 (7-3).  
  
El certamen continuará el viernes próximo, debido a que se detectó un caso positivo de coronavirus en el Grand Hyatt Hotel de Melbourne y por precaución se procedió a aislar a las 600 personas que estaban alojadas, incluidos los tenistas que participaron de los cinco torneos que se desarrollan esta semana en la ciudad: dos ATP, dos WTA y la Copa ATP.  
  
La medida fue adoptada por la Federación Australiana de tenis por el caso positivo de coronavirus de un trabajador del hotel, cuando restan apenas cinco días para el inicio del Abierto de Australia, que debería empezar el lunes.