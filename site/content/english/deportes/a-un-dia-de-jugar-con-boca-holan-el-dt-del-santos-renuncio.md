+++
author = ""
date = 2021-04-26T03:00:00Z
description = ""
image = "/images/ariel-holan-se-fue-de___osx81pu8c_640x361__1.jpg"
image_webp = "/images/ariel-holan-se-fue-de___osx81pu8c_640x361__1.jpg"
title = "A UN DÍA DE JUGAR CON BOCA, HOLAN, EL DT DEL SANTOS RENUNCIÓ"

+++

**_El ex entrenador de Independiente presentó su dimisión consciente de que "los resultados fruto del trabajo no estaban apareciendo", tras las derrotas con Barcelona de Ecuador, el humilde Novorizontino y Corinthians._**

El argentino Ariel Holan renunció este lunes a la dirección técnica de Santos de Brasil a un día de visitar a Boca Juniors por la segunda fecha del Grupo C de la Copa Libertadores.

El presidente del club paulista, Andrés Rueda, anunció la decisión en una conferencia de prensa virtual convocada para esta mañana luego de la derrota de anoche con Corinthians (0-2) en el torneo estadual.

Rueda explicó que Holan "pidió que el juego de mañana ante Boca sea el último al frente del equipo", situación que era evaluada en estas horas por la Comisión Directiva del club.

El extécnico de Banfield e Independiente fue el principal apuntado durante una manifestación de hinchas organizada en el estadio de Vila Belmiro luego del partido ante Corinthians.

"Equipo sinvergüenza", cuestionaron los "torcedores" tras la tercera caída del "Peixe" en una misma semana.

Santos, finalista de la Copa Libertadores 2020 en enero de este año bajo la conducción de Cuca, debutó en la fase de grupos de la edición 2021 con una derrota como local ante Barcelona de Ecuador (0-2), el martes; el viernes perdió con el humilde Novorizontino (0-1) en el Paulista y anoche sucumbió frente a Corinthians en ese mismo torneo.

Holan intentó calmar los ánimos de la hinchada santista, al explicar que frente al "Timao" jugaron mayoría de suplentes y que ante Boca sería "un partido diferente".

Sin embargo, en una conversación posterior con el presidente del club, el argentino presentó su renuncia consciente de que "los resultados fruto del trabajo no estaban apareciendo", admitió el dirigente.

"De común acuerdo se aceptó la decisión. No era lo que queríamos porque Ariel llegó con un contrato por tres años. Es una persona íntegra, trabajadora al extremo y con un excelente conocimiento del fútbol", reconoció Rueda.

Holan, de 60 años, asumió la dirección técnica de Santos el 22 de febrero pasado y se marcha con una campaña de cuatro victorias, tres empates y cinco derrotas.