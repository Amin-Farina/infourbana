+++
author = ""
date = 2021-10-04T03:00:00Z
description = ""
image = ""
image_webp = "/images/senegaleses.jpg"
title = "CUATRO COMERCIANTES RECIBIERON UNA PALIZA TRAS ENFRENTARSE CON 15 MANTEROS SENEGALESES"

+++

##### _Reclamaban por la ocupación ilegal de sus veredas. Los vendedores ambulantes se negaron a desalojar el espacio público y los increparon con golpes e insultos. Desde la ONG Buenos Vecinos alertaron que hay 7.000 puestos en la zona._

La situación que viven los comerciantes y los vecinos de Once por la proliferación de la venta callejera ilegal es compleja. Así como hay muchos que se niegan a denunciar a los manteros por miedo a represalias hay otros que ya están cansados de tener bloqueados los accesos a sus locales o edificios, y la tensión en las calles se volvió cotidiana.

Pero este sábado, un episodio ocurrido en la puerta de una lencería -situada sobre Sarmiento y Pueyrredón- se desbordó más de la cuenta y terminó en una batalla campal entre senegaleses y los dueños de ese comercio.

Cansada de tener que lidiar con ellos para pedirles que por favor liberen la vereda para que los clientes puedan entrar a comprar su local, sumado a que en algunos de los puestos también venden los mismos productos que ella sin pagar los impuestos correspondientes, Isabel tuvo un intercambio subido de tono con uno de los senegaleses.

Al advertir los gritos, el marido de Isabel intervino para tratar de terminar con la discusión y todo fue para peor. “Vinieron 15 senegaleses a defender a sus compatriotas, empujaron al señor y lo golpearon en la cara. Sus dos hijos varones salieron a defenderlo y se armó una batalla campal porque ellos eran un montón”, contó Gloria Llopiz, presidenta de la ONG Buenos Vecinos..

“Eran 4 contra 15. A Isabel la desmayaron de una trompada. Terminó con el ojo lastimado y la cara golpeada. Uno de los hijos resultó con el tabique quebrado y al otro hijo le tiraron un silla por la cabeza y está con traumatismo de cráneo. El marido tiene herido el pómulo y le rompieron los anteojos”, detalló la misma mujer, quien alertó sobre la proliferación de los manteros en el barrio desde octubre de 2020.

Las zonas más afectadas son las calles Bartolomé Mitre, entre Pueyrredón y Larrea; Presidente Perón, entre Pueyrredón y Larrea; Valentín Gómez, entre Pueyrredón y Paso; Castelli, de Rivadavia a Corrientes; Paso, de Rivadavia a Corrientes; en una parte de Larrea y Corrientes; y unas tres cuadras de Corrientes, del 2500 al 2300.

“En la Comisaría 3° nos dijeron que hay aproximadamente 7.000 manteros en Once y que la policía no puede hacer nada si no recibe la orden del Ministerio Público Fiscal. Lo único que hacen es mediar cuando hay algún altercado y evitar levantar actas. Acá hay connivencia porque los propios senegaleses me contaron que ellos pagan $500 por día para que los dejen trabajar. Esto es una mafia”, advirtió la presidenta de la ONG.

Tras las agresiones sufridas, la familia fue asistida por el SAME y luego trasladada al Hospital Ramos Mejía. En la causa intervino el personal de la Comisaría 3° y la Fiscalía en lo Penal, Contravencional y de Faltas 17 a cargo del doctor Federico Topea.

“La familia está indignada y va a pedir hablar con el juez. No puede ser que la policía haya declarado lesiones recíprocas cuando la pelea la empezaron los senegaleses y a los comerciantes no les quedó otra que defenderse”, se lamentó Gloria.

Fuentes policiales precisaron que “la fiscalía dispuso notificar a dos comerciantes de nacionalidad peruana y a dos manteros, uno senegalés y otro argentino. Se inició causa por infracción al artículo 96 del Código Penal de la Nación por lesiones recíprocas”.

Si bien durante la pandemia la venta callejera ilegal había caído un 30,5%, el último relevamiento realizado por Cámara Argentina de Comercio y Servicios (CAC) arrojó que la situación se empezó a revertir en julio, cuando se detectó una suba del 4,8%.

Hoy, el barrio de Once concentra casi el 70% de los puestos ubicados en calles, avenidas y peatonales porteñas. El rubro más comercializado en la zona es indumentaria y calzado, que abarcó el 60,8%, seguido por alimentos y bebidas, con el 18,3% del total.

La ONG Buenos Vecinos nació en 2012, justo el año en que los manteros empezaron a organizarse mejor y pasaron de trabajar sobre las mantas a montar puestos con toldos y sombrillas y copar más espacio público. “Vimos que había una estructura mafiosa con varios recaudadores que tenían todo muy organizado y ahí arrancamos con las denuncias”, recordó Gloria, quien precisó que el 85% de los manteros son senegaleses y el resto peruanos.