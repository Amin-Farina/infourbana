+++
author = ""
date = 2021-07-28T03:00:00Z
description = ""
image = "/images/facundomanes1_crop1627485459792-jpeg_1792534485.jpeg"
image_webp = "/images/facundomanes1_crop1627485459792-jpeg_1792534485.jpeg"
title = "FACUNDO MANES DEFENDIÓ LA INCLUSIÓN DE JESÚS CARIGLINO EN SU LISTA"

+++

**_El precandidato a diputado en la provincia de Buenos Aires, Facundo Manes, dijo que "no se puede importar noruegos" para ubicar en las nóminas, ante las sorpresa de algunos dirigentes de su sector por la incorporación de dirigentes peronistas como postulantes._**

Además evitó responder a la ola de críticas que recibió desde diferentes sectores dentro de la coalición opositora: "La Argentina está demasiado estresada como para que los candidatos nos critiquemos. Yo no voy a criticar a nadie", sostuvo.

Manes fue consultado en la señal LN+ por la sorpresa que mostró la líder del GEN Margarita Stolbizer por la presencia del exintendente de Malvinas Argentinas, Jesús Cariglino, en la lista que los tres comparten.

"Es una alianza con Joaquín de la Torre, que es del peronismo republicano. Necesitamos cambiar la Argentina con los argentinos que se comprometan con este proyecto de país. No podemos importar noruegos, la renovación pasa con que todos los de la lista se comprometan con la Argentina del conocimiento", afirmó Manes, candidato que compite por la UCR asociado al GEN y el peronismo.

Más temprano Stolbizer había señalado que ni ella sabía "qué lugar iba a tener" en la nómina de Manes hasta último momento.

"Yo me enteré de cómo era la composición prácticamente después de que estaba hecha, también me sorprendió, pero Cariglino formaba parte de Juntos", señaló, consultada por el canal IP Noticias por esa presencia.

Manes además evitó responder las diferentes críticas que recibió el martes de parte de su propio espacio y desde el oficialismo. "La Argentina está demasiado estresada como para que los candidatos nos critiquemos. Yo no voy a criticar a nadie", concluyó el neurocientífico.

JxC exhibió fuertes cruces y tensión interna en el inicio de la campaña electoral rumbo a las elecciones primarias abiertas, simultáneas y obligatorias (PASO) de septiembre, que incluyeron reproches entre sus precandidatos y dirigentes de diferentes distritos.

La nueva rencilla en la alianza opositora involucró al propio jefe de Gobierno porteño, Horacio Rodríguez Larreta; a la exgobernadora bonaerense María Eugenia Vidal y a la líder de la Coalición Cívica (CC), Elisa Carrió, quienes apuntaron a Manes, competidor de Diego Santilli, en la interna de Juntos en la estratégica provincia de Buenos Aires.

Manes había pedido que "no se gasten los impuestos de los porteños en la campaña", lo que le valió una catarata de críticas por parte de sus oponentes internos, que advirtieron al neurocientífico que el "adversario" electoral de la coalición "es el kirchnerismo".