+++
author = ""
date = 2021-07-21T03:00:00Z
description = ""
image = "/images/6051f9dc5d5df_1004x565-1.jpg"
image_webp = "/images/6051f9dc5d5df_1004x565.jpg"
title = "GUSTAVO POSSE BAJÓ SU CANDIDATURA"

+++

###### FUENTE: LETRA P

**_En nombre de la unidad, el intendente de San Isidro dio un paso al costado. Interna mano a mano en Juntos._**

Tras el lanzamiento de Diego Santilli este jueves en La Plata, Gustavo Posse, anunció a través de un comunicado que no competirá en la interna de Juntos en la provincia de Buenos Aires, allanando así el camino de Facundo Manes, quién no tendrá que disputar el voto radical. “Luego de una intensa labor inclusiva damos fin a la postulación de nuestro espacio para las próximas elecciones” afirmó.

El intendente de San Isidro argumentó que desde un principio planteó que el radicalismo debía ser “protagonista” y que “ese objetivo fue cumplido luego de la masiva concurrencia a la elección interna”. Y que “no se podía someter a la ciudadanía a la humillación de llevarla a una encuesta obligatoria sino a ser efectivamente electora de los dirigentes que considere más representativos de sus sueños y aspiraciones”. Objetivo que, dijo, también se cumplió, luego de que se formalicen las listas de Santilli y Manes.

 Finalmente afirmó que “en pos de la necesaria unidad de Juntos realizamos nuestro aporte y estamos confiados en que esta campaña por las elecciones Primarias tendrá la altura del debate que la realidad actual demanda para encontrarnos al día siguiente con mayor fortaleza y decisión para luchar por mejorar la vida de los bonaerenses”.

Pese a la derrota en la interna partidaria en el mes de marzo y que su principal socio en esa contienda, Martín Lousteau, rápidamente se alineara en apoyo a la candidatura de Manes, Gustavo Posse insistió hasta último momento con su postulación. Sin embargo, en una entrevista a Letra P ya había abierto la puerta a un acuerdo: “Todos entienden que, si existiese la oportunidad para tener una acción integradora, por supuesto se haría” aseguró semanas atrás.