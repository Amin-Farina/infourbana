+++
author = ""
date = 2021-10-21T03:00:00Z
description = ""
image = "/images/tvr7uyi3j7j2pux7evymchdnrq.jpg"
image_webp = "/images/tvr7uyi3j7j2pux7evymchdnrq.jpg"
title = "EL EXJEFE DE INTELIGENCIA DE HUGO CHÁVEZ CONFESÓ QUE MANDARON 21 MILLONES DE DÓLARES PARA FINANCIAR A CRISTINA KIRCHNER"

+++

#### **_Según un medio español, “El Pollo” Caravajal declaró ante la Justicia española que ocurrió antes del escándalo de las valijas de Antonini Wilson._**

La declaración ante la Justicia española del exjefe de Inteligencia de Hugo Chávez, ”El Pollo” Carvajal, puede convertirse en un testimonio que podría impactar de lleno en la política nacional. El exagente afirmó que el escándalo de las valijas de Guido Antonini Wilson en la Argentina no se trató de un caso aislado, si no que habían existido “20 entregas previas de 1 millón de dólares”.

Según consignó el medio español OK Diario, Caravajal hizo referencia al escándalo de los 800 mil dólares que le incautaron a Antonini Wilson en la Argentina en 2007 y detalló: “Lo que no se supo es que ese era su vuelo número 21, con 20 entregas previas de 1 millón de dólares cada una y entregadas sin problemas ya que pagaban a funcionarios del aeropuerto argentino que los dejaban pasar sin ningún inconveniente”.

El exfuncionario chavista sostuvo que esos dólares servían para financiar la campaña electoral de 2007 de la actual vicepresidenta Cristina Kirchner, cuando logró proclamarse presidenta con el 45% de los votos.