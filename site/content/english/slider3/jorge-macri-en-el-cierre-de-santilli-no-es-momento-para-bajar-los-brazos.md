+++
author = ""
date = 2021-09-10T03:00:00Z
description = ""
image = "/images/jorge-macri-1.jpg"
image_webp = "/images/jorge-macri.jpg"
title = "JORGE MACRI EN EL CIERRE DE SANTILLI: \"NO ES MOMENTO PARA BAJAR LOS BRAZOS"

+++

**_Durante el acto de cierre de campaña de Diego Santilli, el intendente de Vicente López criticó al gobierno por el manejo de la pandemia y la vacunación VIP. "Este país que plantea el kirchnerismo no es un buen país", recalcó._**

Jorge Macri, intendente de Vicente López y titular del PRO bonaerense, pidió a la sociedad concurrir el domingo a realizar el voto para elegir la lista de Diego Santilli y contraponerse al país que propone el kirchnerismo, durante el acto de cierre de ese precandidato de Juntos por el Cambio en la ciudad de La Plata.

Tras tomar la palabra luego de la exposición de Miguel Ángel Pichetto, integrante de Peronismo Republicano, el jefe comunal se refirió a un acuerdo interno luego de la elección para gobernador bonaerense que galardonó a Axel Kicillof: "Todos los que estamos acá en 2019 asumimos un compromiso, más allá de cuál fuera el resultado de la elección no abandonar a los bonaerenses y acá estamos, codo a codo con cada bonaerense, presentes, defendiendo las cosas en las que creemos y una provincia que se merece estar mejor”.

Macri afirmó que la nación que planteada la presidencia de Mauricio Macri, su primo, era encaminarse hacia "el país correcto", contrario a la propuesta del kirchnerismo, que "escribe las leyes para que los demás las cumplan y ellos no".

"Un país donde saltear la fila y vacunarte dejando de vacunar a los abuelos que tenían que ser vacunados está bien, para nosotros no. Un país donde no podíamos despedir a nuestros seres queridos y en Olivos, en la residencia del presidente se festejaba un cumpleaños. Ese país no es el que queremos”, criticó.

Y reiteró que: "El país al que queríamos ir es el país correcto. Por ahí la tiramos afuera como dice Mauricio, por ahí le erramos en algunas cosas pero el lugar al que queríamos ir, ese país que queremos construir y que queremos volver a trabajar y en el que queremos volver a concentrarnos es un buen país".

El presidente del principal cuadro opositor bonaerense sostuvo que Argentina posee "buena gente, gente que quiere laburar, levantar las persianas de sus comercios".

“No vamos a aceptar los argentinos que nos lleven al país que no queremos y a patadas en el culo y de manera prepotente", le advirtió al oficialismo.

En ese marco, expresó que sabe que "hay mucha gente enojada, frustrada, desencantada" y señaló que "no es un momento para bajar los brazos", al tiempo que subrayó que "el domingo no es un día para quedarse en casa. Es un día para participar”.

“Cada uno de los argentinos, cada uno de los bonaerenses, tiene que salir de casa e ir a ejercer como ciudadanos y votar, llenar las urnas de votos. La rebelión es pacífica, la rebelión es democrática, la rebelión es cívica. Les vamos a ganar con el voto”, concluyó.