+++
author = ""
date = 2021-08-30T03:00:00Z
description = ""
image = "/images/_116402823_em_071018_nicholas-christakis00253-1.jpg"
image_webp = "/images/_116402823_em_071018_nicholas-christakis00253.jpg"
title = "¿CÓMO PIENSA LA POSPANDEMIA UN EXPERTO DE YALE? UNA ERA DE DESENFRENO SEXUAL Y DESPILFARRO ECONÓMICO"

+++

FUENTE: INFOBAE

**_El prestigioso médico y sociólogo, Nicholas Christakis, examinó en diálogo con Infobae las perspectivas sociológicas del fin del COVID-19. Pronosticó que 2023 será un año de transición y que, en 2024, se vivirán años de gran libertad sexual, mayor consumo económico y retirada de las religiones._**

Para muchos, imaginar cómo será el fin de la pandemia es muy similar a imaginar cómo será el paraíso. Tras más de un año y medio de restricciones sanitarias, severas consecuencias económicas y cambios profundos en la forma de vinculares con los íntimos y los desconocidos, muchos auguran una suerte de “tierra prometida” al final del túnel.

El médico y científico social estadounidense Nicholas Christakis, profesor de la Universidad de Yale y codirector del Yale Institute for Network Science, estimó en su último libro, Apollo’s Arrow: The Profound and Enduring Impact of Coronavirus on the Way We Live (en español, La flecha de Apolo: el impacto profundo y duradero del coronavirus en la forma en que vivimos), que tras el fin de la pandemia, y una vez superado el impacto psicológico y social que causó el SARS-CoV-2, se esperan años de más libertad sexual y mayor consumo. En definitiva, “los nuevos años ´20″, que podrían empezar entre 2023 y 2024, serán años de despliegue sexual y despilfarro económico.

Los “felices años 20″ se conocen como el período de entreguerras, que en Europa y Estados Unidos estuvo signado por la reactivación económica y mayor libertad social y sexual. Tras la primera gran guerra (1914-1919) y el fin de la epidemia de gripe española (1918), de a poco, el consumo se recuperó y más personas pudieron acceder a una mejor calidad de vida, tras años de sacrificios y crisis sanitaria. Los años 20 del siglo XX, que empezaron con fuerza en 1924, y se extendieron hasta 1929, fueron los años de las fiestas glamorosas y del auge de nuevas corrientes musicales como el jazz y el blues. También fueron los años que vieron nacer a los espectáculos masivos en el deporte y el teatro, la época de oro de los cabarets, y del nacimiento de la radio.

En diálogo con Infobae, Christakis, que es autor de más de 200 artículos científicos y varios libros, advirtió que la vida en pandemia “se siente muy extraña y antinatural, pero las plagas no son nuevas para nuestra especie. Son sólo nuevas para nosotros. Creemos que esto es una locura, pero no lo es. Durante miles de años, la gente ha estado lidiando con plagas. Las plagas están en la Biblia, están en Homero, están en Cervantes con Don Quijote, están en Shakespeare. Las plagas son parte de la experiencia humana. En ese sentido, podemos mirar la historia de los seres humanos para comprender cómo lidiamos y respondemos a una plaga”. “Una plaga de esta magnitud es rara, pero tiene precedentes”, remarcó durante la entrevista.

El investigador se animó hasta a pronosticar una fecha en la que se esperan los nuevos cambios: “Si nos fijamos en todos los siglos precedentes de epidemias, está claro que vamos a tener un período intermedio en el que aceptaremos el costo psicológico, social y económico de la pandemia. Creo que durará hasta 2023, aproximadamente. Necesitamos recuperarnos del terrible impacto de esta experiencia”.

En su último libro, Christakis, hijo de padres griegos, estudió en su investigación la crisis causada por el COVID-19 desde perspectivas epidemiológicas, virológicas, sociológicas e históricas. Y estimó que, en algún momento de 2023, se entrará en el período posterior a la pandemia, que va a iniciarse con más fuerza en 2024. “Y creo que se va a sentir un poco como los locos años 20 del siglo pasado. Y espero que la economía aumente y las artes prosperen a medida que nuestra tendencia a socializar se acelera”, profundizó el investigador.

Los cambios no van darse de un día para el otro porque la salida de la pandemia será paulatina, anticipó Christakis, y apuntó a un período de transición: “Necesitamos recuperarnos del terrible impacto de esta experiencia. Millones de negocios han cerrado. Millones están sin trabajo. Millones de niños han faltado a la escuela de forma significativa. Millones de personas han perdido familiares a causa del virus. Muchos tendrán discapacidades crónicas por contraer el virus. Necesitamos aceptar todas estas cosas, lo que llevará tiempo”, compartió el profesor a través de una entrevista publicada en el sitio de Yale University.

Christakis, que fue elegido como una de las 100 personas más influyentes del mundo en 2009 por la revista Time, explicó en diálogo con Infobae que “somos las primeras generaciones de seres humanos que hemos podido desarrollar en tiempo real medidas efectivas contra la plaga, pero tendremos que lidiar con el virus hasta el 2022″.

¿Y qué pasa después? ¿Al final del túnel llega el paraíso? El sociólogo lo definió, en una entrevista con Infobae, como “dejaremos atrás el impacto biológico y epidemiológico de la pandemia, la ola, como un tsunami, bajará y se verá el daño. Países devastados. Vamos a entrar en un período intermedio y vamos a tener que hacer frente al shock psicológico, social y económico. Si miras la historia de las plagas desde hace años, tomará un año, dos años. Digamos, hasta finales de 2023.”

En su pronóstico más provocador, Christakis definió, en diálogo con Infobae, que “a partir de 2024, entraremos en el período pospandémico y creo que será como los locos años 20 del siglo XXI, en comparación con los locos años 20 del siglo XX. La gente, que ha estado encerrada, querrá salir (ya sabes cómo son los argentinos, van a salir), va a haber fiestas, va a salir a discotecas, restaurantes, eventos deportivos y recitales. Es posible que veamos algunos cambios en los comportamientos sexuales y habrá gente que gastará dinero, que consumirá. Creo que tendremos algo así cuando finalmente dejemos atrás la plaga”.

En algún momento de 2024, “la vida lentamente comenzará a volver a la normalidad, con algunos cambios persistentes y las tendencias de la pandemia se habrán revertido. La gente va a buscar interacción social sin pausa. Eso podría incluir sexo licencioso, mucho gasto y una retracción de la religiosidad”, evaluó Christakis, quien además trabajó como médico de cuidados paliativos en Chicago y Boston hasta 2011.