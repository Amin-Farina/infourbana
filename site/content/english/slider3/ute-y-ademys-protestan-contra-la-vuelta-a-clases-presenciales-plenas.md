+++
author = ""
date = 2021-08-16T03:00:00Z
description = ""
image = "/images/whatsapp-image-2021-06-04-at-11-27-18.jpeg"
image_webp = "/images/whatsapp-image-2021-06-04-at-11-27-18.jpeg"
title = "UTE Y ADEMYS PROTESTAN CONTRA LA VUELTA A CLASES PRESENCIALES PLENAS"

+++

**_El gremio de la Unión de Trabajadores de la Educación (UTE) realizará entre este lunes y el vienes próximo jornadas de lucha en protesta contra la decisión del Gobierno porteño de volver a la presencialidad plena de un total de 290.000 estudiantes de nivel primario._**

Desde esa organización gremial se informó que se realizarán clases públicas, semaforazos y abrazos a las escuelas y habrá asambleas en las escuelas para continuar con "la conformación de comités de autocuidado" conformados por docentes, estudiantes y familias.

"El negacionismo sanitario de Larreta, Quirós y Acuña suprime distanciamiento social para todas las actividades dentro de las escuelas, a su vez no garantiza espacios con ventilación cruzada ni elementos de protección e higiene suficientes. Exigimos la compra de medidores de dióxido de carbono para todas las escuelas de la Ciudad para monitorear la ventilación de las aulas mientras se dictan clases", señaló el gremio a través de un comunicado.

En ese sentido, remarcaron desde UTE que durante las dos primeras semanas, en las que la presencialidad total comenzó en el nivel secundario, hubo más de 20 divisiones y grados aislados, con 500 estudiantes y más de 50 docentes en esa situación".

Por su parte, el secretario adjunto del gremio docente Ademys, Jorge Adaro, consideró que "hay datos que no acompañan" la decisión del Gobierno porteño de retornar a la presencialidad plena en las aulas, al ratificar el paro de actividades que realiza esa organización gremial a raíz del regreso a la presencialidaden las aulas.

“Que no haya distanciamiento entre los alumnos es muy grave, no hay ningún estudio científico que avale que ya no es necesario mantener el distanciamiento", afirmó Adaro en declaraciones formuladas a El Destape Radio.

Un total de 290.000 estudiantes de nivel primario vuelven este martes a la presencialidad plena en las escuelas públicas y privadas de la Ciudad de Buenos Aires, y el sindicalista sostuvo que esta medida demuestra que “la ministra (de Educación porteña) Soledad Acuña, no tiene ninguna preocupación por el cuidado de los alumnos".

La semana pasada, desde Ademys, se pidieron explicaciones científicas al ministro de Salud porteño, Fernán Quirós, sobre el retorno escolar sin distanciamiento, y Adaro comentó que “no hubo ninguna definición" por parte del funcionario sobre "el distanciamiento y la presencialidad plena”.

De acuerdo al nuevo protocolo que rige en la Ciudad, la burbuja sanitaria pasa a ser el aula completa de cada curso, lo que va a contramano de lo dispuesto en el marco del Consejo Federal de Educación a instancias de las recomendaciones sanitarias para prevenir la propagación del coronavirus en el marco de la pandemia.

Para el dirigente gremial, decir que la burbuja es el aula “es un distractivo” y “una manera de validar que se rompa la burbuja".

"En una burbuja reducida, si hay un caso que requiera aislamiento se da con menos pibes. Ahora, si hay un caso el aislamiento, será con el triple de pibes", advirtió el referente de Ademys.

En el marco de la jornada de paro se realiza una caravana desde el Ministerio de Salud porteño hacia la Jefatura de gobierno de la Ciudad.

En tanto, en una posterior asamblea, el gremio evaluará "cuál es el estado de la situación y cómo continuar para garantizar condiciones para que los alumnos y trabajadores podamos desarrollar esta actividad con el menor riesgo posible”.

Por otro lado, Adaro recordó que la represalia principal tomada por el Gobierno de Horacio Rodríguez Larreta contra los paros de los sindicatos docentes fue “el descuento de los días".

“Larreta jugó el descuento de los días de paro para bajar el nivel de participación en las medidas de fuerza. Hubo docentes que recibieron descuentos de hasta 60 mil pesos y hubo un fallo que prohibió los descuentos, pero no lo cumplieron", indicó Adaro.