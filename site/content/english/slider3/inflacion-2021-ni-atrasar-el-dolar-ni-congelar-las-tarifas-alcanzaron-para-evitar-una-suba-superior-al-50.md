+++
author = ""
date = 2022-01-13T03:00:00Z
description = ""
image = "/images/arton210319-1.jpg"
image_webp = "/images/arton210319.jpg"
title = "INFLACIÓN 2021: NI ATRASAR EL DÓLAR NI CONGELAR LAS TARIFAS ALCANZARON PARA EVITAR UNA SUBA SUPERIOR AL 50%"

+++

##### **_Hoy se conocerá el Indice de Precios al Consumidor (IPC) de diciembre. La inflación del año pasado quedó muy lejos del 29% que había proyectado Martín Guzmán en el Presupuesto. Cuáles son los factores que seguirán impulsando los precios al alza_**

El 2021 finalmente arrojó una inflación arriba del 50%, reflejando los efectos de la fuerte emisión monetaria para cubrir el déficit fiscal y los fuertes desequilibrios que acumula la economía. El incremento (que será anunciado hoy por el Indec) estuvo apenas por debajo del casi 54% que arrojó el 2019, durante la última gestión de Mauricio Macri. Ni siquiera la estrategia del Gobierno de atrasar el tipo de cambio oficial ni el semicongelamiento tarifario fueron suficientes para evitar el fuerte salto del índice, que rondará el 51% para todo el año.

Tampoco fue efectiva la estrategia de congelar los precios de los alimentos que intentó el secretario de Comercio Interior, Roberto Feletti, durante el último cuatrimestre del año obtuvo mayores resultados. Si bien la inflación cayó a 2,5% puntualmente en noviembre, en diciembre los aumentos volvieron a ser muy significativos, retomando la tendencia que fue constante a lo largo de 2021. El último mes del año, además, tiene componentes estacionales vinculados con las fiestas y el inicio de las vacaciones que también impulsan los precios. Se estima que el índice nacional arrojaría entre 3,8% y 4%. Enero es otro mes de “estacionalidad alta”.

La elevada inflación del 2021 resultó una gran aliada para que el Gobierno consiguiera una mejora sustancial de la recaudación y una reducción del déficit fiscal, que pasó del 6,5% del PBI en 2020 a finalizar apenas por encima del 3% el año pasado. La aceleración inflacionaria produce dos efectos en las cuentas públicas. Por un lado, genera un fuerte aumento de la recaudación a través del IVA por precios mucho más elevados y, por otra parte, provoca una licuación del gasto del sector público, especialmente jubilaciones pero también empleo estatal.

Los salarios privados a duras penas consiguieron acercarse a la inflación acumulada anual para no perder nuevamente poder adquisitivo. Sin embargo, en muchos sectores el incremento estuvo lejos del 51% que acumuló la inflación en el año. La suba de ingresos fue mucho menos en el caso de los empleados informales y los cuentapropistas, sectores cada vez más numerosos y en los que los aumentos constantes de precios producen estragos.

Además, la canasta básica también tuvo un salto sustancial, que se acercó al 50% y es la principal traba para que la pobreza se sostenga cómodamente arriba del 40% aún cuando la actividad económica tuvo una mejora de 10%, recuperando todo lo perdido durante la pandemia.

Las perspectivas para 2022 no son alentadoras ni mucho menos. Las consultoras económicas que participan del Relevamiento de Expectativas de Mercado pronosticaron un aumento de casi 55% para el año que acaba de comenzar, es decir un pico todavía mayor al del año pasado.

> Andrés Borenstein, director de la consultora Econviews, proyecta un salto aún mayor, en torno al 58% para este año. “Hay muchos factores que impulsarán todavía más la inflación en 2022. Uno de los principales será el mayor ajuste que tendrá el dólar oficial, que ya no podrá seguir atrasándose como sucedió este año. Podemos discutir si será ahora o un poco más adelante, pero ese aumento llegará y en parte va a trasladarse a los precios”.

El economista también citó el mayor ajuste de tarifas como otro factor que también tendrá incidencia en la inflación. En febrero ya se anunció un ajuste del 20% para luz y gas, además habrá segmentación por lo que los aumentos serán mayores en los hogares de mayor poder adquisitivo de AMBA. No se descarta algún incremento adicional a lo largo del 2022 para evitar que sigan aumentando los subsidios económicos, en línea con la exigencia del FMI.

Pero además tampoco aflojará la emisión monetaria, a pesar de la baja del déficit fiscal que podría ocurrir durante el año. Sucede que la mayor parte del agujero de las cuentas públicas será cubierto mucho más con emisión que con colocación de deuda por parte del Tesoro. Y además la suba de tasas también obligará al presidente del Banco Central, Miguel Pesce, a emitir más para pagar las Leliq y los pases pasivos.

El escenario de una inflación de alrededor del 55% puede considerarse relativamente optimista. Si no hay acuerdo con el FMI, por ejemplo, la presión cambiaria podría ser mucho mayor y generar un impacto aún más fuerte en los precios. En ese escenario estaría mucho más cerca una “hiper”, es decir niveles arriba del 80%.