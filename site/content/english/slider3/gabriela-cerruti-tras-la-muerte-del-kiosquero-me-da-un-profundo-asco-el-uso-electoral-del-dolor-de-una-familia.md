+++
author = ""
date = 2021-11-11T03:00:00Z
description = ""
image = "/images/portavoz-de-la-presidencia-gabriela-cerruti-dialogo-prensa-foto-twitter-gabicerru-919098-101509-1.jpeg"
image_webp = "/images/portavoz-de-la-presidencia-gabriela-cerruti-dialogo-prensa-foto-twitter-gabicerru-919098-101509.jpeg"
title = "GABRIELA CERRUTI, TRAS LA MUERTE DEL KIOSQUERO: \"ME DA UN PROFUNDO ASCO EL USO ELECTORAL DEL DOLOR DE UNA FAMILIA\""

+++

La portavoz de la Presidencia, Gabriela Cerruti, aseguró este jueves 11 de noviembre que el Gobierno nacional va a "seguir trabajando para reformar la ley penal juvenil, que es viejísima, injusta e inequitativa" para reforzar el trabajo en la "reinserción y revinculación de los jóvenes que cometen delitos". Y agregó: "Me da un profundo asco la utilización electoral del dolor de una familia" 

“El Presidente está a disposición de la familia Sabo para cuando ellos lo deseen”, dijo Cerruti. “Somos absolutamente conscientes de la situación de inseguridad y empezar a discutir estadísticas está fuera del foco en esta circunstancia, pero la estadística dice que hay una suba en la Ciudad de Buenos Aires y no en la provincia”, subrayó la vocera del presidente Alberto Fernández.

En conferencia de prensa desde Casa Rosada, Gabriela Cerruti sostuvo: “Lo que tenemos que hacer es que haya más seguridad y prevención, más policía en la calle, más seguridad en general con prevención, que el estado esté presente, este es un problema estructural y creemos que estamos avanzando, pero sabemos que es insuficiente".