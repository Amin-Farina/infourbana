+++
author = ""
date = 2021-11-09T03:00:00Z
description = ""
image = "/images/3gdju3evwzbp7jtxxowedne5ta.jpg"
image_webp = "/images/3gdju3evwzbp7jtxxowedne5ta.jpg"
title = "“Mi novia y yo pensamos en irnos del país, tengo miedo de ir a trabajar”: el dolor de uno de los hijos del kiosquero asesinado en Ramos Mejía"

+++
#### 

#### **_Horas después del asesinato a quemarropa de Roberto Sabo a manos de un delincuente que asaltó el kiosco que tenía en Ramos Mejía, Nicolás, uno de sus hijos, pidió que se haga justicia. “Estamos devastados”, dijo el joven._**

Expresó que tiene miedo de ir a trabajar y pidió que se condene al acusado a cadena perpetua. “Que se pudran en la cárcel”, sostuvo. “Y no se olviden de la chica, aunque sea menor”.

“Ahora vendrá el momento más duro. Nunca perdí a nadie cercano y hoy me toca hacer todo esto por mi papá. Ayer mirábamos el kiosco de reojo y pensabamos que en algún momento vamos a tener que volver”, consideró el joven de 25 años. “Tengo miedo de ir a laburar”, se sinceró.

El joven se refirió a las declaraciones de su hermano menor, que cumplirá 18 años la semana que viene, que manifestó en plena marcha el lunes por la noche que quería irse a vivir al exterior. “Él ya lo dijo. Mi novia y yo lo pensamos, es una frase recurrente”, dijo.

#### “Él hacía todo por nosotros”

Nicolás recordó a su papá en medio de la conmoción por el brutal asesinato. “Él hacía todo por nosotros. Cuando desbloqueamos el celular lo primero que vimos fue una página de viajes con la que nos íbamos siempre. Porque seguramente estaba buscando para hacer un viaje en familia”, relató.

Agradeció a los vecinos y a quienes se solidarizaron con la familia tras lo sucedido y se refirió al reclamo que realizan los vecinos de la zona. “La cantidad de gente que estaba ayer no es casualidad. Es una causalidad; por tantos años que la vienen pasando mal. Si todos los vecinos piden que se vaya la misma persona, es por algo que está haciendo mal”, reclamó.

El joven contó que se hará cargo del kiosco de su familia de ahora en más. “Mi abuela ayer me dijo que venda el kiosco y no vaya más. La entiendo, yo también tengo miedo, pero hoy por hoy me toca ocupar el lugar de mi papá”, sostuvo. “Él entregó todo y lo mataron con frialdad”, se lamentó.

En ese sentido, pidió que se haga justicia por el crimen. “Pedimos que marque un cambio, que no sea algo más. En la fiscalía nos dijeron que van a ir por la cadena perpetua para el acusado”, expresó. “Se los agradezco, pero que no se olviden de la chica. Porque por más de que tenga 15 o 16 años y sea menor, sabía perfectamente lo que hacía”, apuntó.

Los detenidos son Leandro Daniel Suárez, de 29 años, y una adolescente de 15 que lo acompañaba y actuó como “campana”. Ambos fueron arrestados por la policía tras intentar escapar primero en un remís robado y luego en una moto de un “delivery” de la zona.

“Escuché (al detenido) que dijo que si le dan perpetua se iba a matar. Si se quiere matar, que se mate”, sostuvo Nicolás.

El joven contó además que desde que sucedió todo, desde el Gobierno solo el ministro de Seguridad Sergio Berni se comunicó. “Aceptamos que viniera porque quiso acercarse a nuestra casa. Entiendo que hay cosas que no puede cambiar él, pero si los funcionarios están ahí es porque, mal o bien, la gente los eligió y tienen que trabajar por nosotros, no esconderse”, cerró.

Además, agradeció también al candidato a Diputado por la Ciudad de Buenos Aires Javier Milei, que se puso en contacto con la familia para mostrar solidaridad.