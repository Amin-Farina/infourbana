+++
author = ""
date = 2021-05-26T03:00:00Z
description = ""
image = "/images/gzpsxzigffhrthdiwkg2pwv2de.jpg"
image_webp = "/images/gzpsxzigffhrthdiwkg2pwv2de.jpg"
title = "MARZO: LAS VENTAS EN LOS SUPERMERCADOS BAJARON 8.8%"

+++

**_Esa variación se dio respecto del mismo mes de 2020. Sin embargo, medidas a precios constantes, estuvieron 10,3 % por encima de las de febrero, destacó el Indec en un informe._**

Las ventas en los supermercados bajaron durante marzo 8,8 % en relación a igual mes del año pasado, cuando la cercanía de las medidas de aislamiento para enfrentar el Covid-19 alentaron una sobredemanda, informó este miércoles el Instituto Nacional de Estadística y Censos (Indec).

Sin embargo, las ventas de marzo, medidas a precios constantes, estuvieron 10,3 % por encima de las de febrero, destacó el organismo.

En tanto, en los grandes centros de compra, las ventas de marzo estuvieron un 59,7 % por sobre las de igual mes del año pasado, y 14,4% por sobre las de febrero.

Durante el tercer mes del año, las ventas totales a precios corrientes en el salón de ventas ascendieron a $ 103.207 millones, lo que representó un 96,3 % del total y mostró un aumento del 22,6 % respecto a marzo de 2020.

Por su parte, las ventas por el canal online sumaron $ 4.015 millones, lo que representó un 3,7 % del total y un aumento del 150,5 % respecto al mismo mes del año anterior.

También en marzo, las ventas a precios corrientes realizadas en efectivo ascendieron a $ 33.643 millones, lo que representó un 31,4 % de las ventas totales y reflejó un aumento del 26,8 % respecto a marzo de 2020.

Por su parte, las efectuadas mediante tarjeta de débito sumaron $ 28.345 millones, lo que representa un 26,4 % del total y una variación positiva de 26,4 % respecto al mismo mes del año anterior.

Las ventas a precios corrientes abonadas con tarjeta de crédito sumaron un total de $ 39.665 millones, lo que representa un 37,0 % del total y una suba del 17,6 %.

Por último, las realizadas mediante otros medios de pago alcanzaron a $ 5.567 millones, un 5,2 % del total y un aumento del 79,1 % respecto al mismo mes del año anterior.

En los grupos de artículos, los aumentos más significativos durante marzo respecto al mismo mes del año anterior fueron: “Indumentaria, calzado y textiles para el hogar”, 110,4 %; “Electrónicos y artículos para el hogar”, 99,9 %; “Bebidas”, 50,4 %; y “Otros”, 40, 7%.

Las jurisdicciones donde se registraron las subas más importantes de las ventas totales a precios corrientes fueron: Misiones, 49,2 %; Santiago del Estero, 43,2 %; Formosa, 39,6 %; La Pampa, 36,4 %; y Jujuy, con 36,3 %.

El índice de precios implícitos para marzo de 2021 mostró una variación porcentual de 37 % respecto al mismo mes del año anterior y una variación porcentual de 4,6 % respecto al mes anterior.

Además, el personal ocupado en los supermercados ascendió a 93.437 empleados, lo que marco una baja del 2,5 % respecto al plantel de igual mes del año pasado, y del 0, 2% en relación a febrero.

En los autoservicios mayoristas las ventas sumaron $ 18.311,5 millones, lo que representa un incremento de 20,7 % respecto al mismo mes del año anterior, pero medidas a precios constantes marcaron una caída de 7 % interanual.

En cuanto a los grandes centros de compra, el Indec contabilizó en todo el país 6.134 de los cuales 1.620, el 26 %, estaban inactivos, de los cuales solo 34 estaban cerrados “por reforma”.

Por su parte, los rubros que en marzo de 2021 tienen la mayor participación para el total del país son: “Indumentaria, calzado y marroquinería”, 51,4 %; “Patio de comidas, alimentos y kioscos”, 15,9 %; y “Otros”, 13,5 %.