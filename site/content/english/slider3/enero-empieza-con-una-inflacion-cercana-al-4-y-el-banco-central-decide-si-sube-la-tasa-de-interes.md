+++
author = ""
date = 2022-02-14T03:00:00Z
description = ""
image = "/images/602c32e3018d9_600_315.jpg"
image_webp = "/images/602c32e3018d9_600_315.jpg"
title = "ENERO EMPIEZA CON UNA INFLACIÓN CERCANA AL 4% Y EL BANCO CENTRAL DECIDE SI SUBE LA TASA DE INTERÉS"

+++

##### **_El Gobierno y las consultoras estiman que enero tuvo una IPC similar a la de diciembre, que fue de 3,8 por ciento. De qué manera los índices de precios forman parte del acuerdo con el Fondo Monetario_**

En las próximas horas el Gobierno dará a conocer el índice de inflación de enero, un dato que marcará el inicio del sendero de suba de precios de este año tras el 50,9% que registró el 2021 de punta a punta. Será, además, un elemento que podría condicionar una nueva suba de tasas de interés del Banco Central, en medio de las negociaciones con el Fondo Monetario Internacional.

Sucede que la inflación, además de ser una cuestión decisiva en el devenir de la macroeconomía y de los bolsillos de los hogares, también forma parte de la discusión con el organismo financiero con el que el Gobierno busca cerrar en los próximos días un acuerdo técnico definitivo y la puesta en marcha de un nuevo programa que refinancie el préstamo Stand By de 2018.

Hubo una debate extenso entre los funcionarios argentinos y los del FMI respecto a qué definición encontraban en común para explicar la persistencia de la inflación en la economía argentina. El Gobierno incluso celebró como un logro que el Fondo haya admitido la explicación de “multicausalidad” que sostuvo Martín Guzmán, donde se suman factores fiscales, monetarios, de expectativas y de balanza comercial.

La suba de precios tendrá algunos parámetros específicos en el paquete de políticas económicas que el Poder Ejecutivo y el Fondo Monetario pero en la Casa Rosada aseguran que existirá una hoja de ruta que actúe como las metas de inflación fallidas durante el mandato macrista. De todas formas, habrá rangos de precios con los que los técnicos puedan hacer las proyecciones para cada año. En el caso de 2022, estiman fuentes cercanas a la negociación, se discute una inflación que oscila en torno al 50%.

La suba de tasas de interés es otra de los elementos que componen el “corazón” del programa de políticas clave anunciado el pasado 28 de enero. Tendrá en consideración, según adelantaron desde el Ministerio de Economía, a los plazos fijos y a los bonos del Tesoro. Esos activos en pesos deberán tener rendimientos positivos frente a la suba de precios para incentivar el ahorro en moneda local. El IPC de enero es, además, un dato que espera el BCRA que conduce Miguel Pesce para determinar si habrá esta semana un nuevo ajuste en la tasa de referencia, de las Leliq.

El Banco Central ya aumentó en dos puntos la tasa de interés, del 38% al 40% y descongeló así la tasa de interés en pesos tras un año sin cambios. Además, también subió la posición de plazo fijo para los ahorristas en orden del 39% anual.

##### La inflación que viene: qué esperan las consultoras

Para la consultora LCG, “la última semana del mes cerró con una inflación semanal del 1%. Aunque desaceleró medio punto porcentual contra la semana anterior, se sostiene en niveles elevados, prácticamente por encima del 1% en las cuatro semanas del mes. Así, enero cierra con una inflación promedio de 3,9%, pero de 4,6% medida punta contra punta, 2,1 puntos por encima del acumulado en diciembre. La proporción de productos con aumentos se redujo a 16% (desde 27% en la semana previa) pero todavía resulta alto en términos del promedio del último año (14%)”, consideraron en su último informe.

Eco Go, por ejemplo, ubicó a la suba de precios en torno del 3,8 por ciento. La consultora Equilibra estimó una suba de precios cercana a 4% en el primer mes del año, número similar a Analytica. Ecolatina estimó un 3,5% mientras que Orlando Ferreres tuvo un 3,6% en sus registros.

El relevamiento de precios del primer mes del año que realizó Consumidores Libres arrojó un aumento de 5 por ciento. Las frutas y verduras subieron 20,90 por ciento, la carne tuvo un incremento del 1,95 por ciento y los productos de almacén aumentaron un 1,89 por ciento.

Por su parte, Focus Market, estimó un alza de 4,5% en alimentos, mientras que para los rubros de indumentaria, calzado, vivienda y salud prevé una suba por encima del 4 por ciento. Por el lado de la consultora Libertad y Progreso, el cálculo de la inflación de enero se ubicó en el orden del 4 por ciento.

Este fin de semana el secretario de Comercio Interior Roberto Feletti sostuvo que la inflación de enero será similar a la de ese mismo mes del año pasado, cuando marcó un 4%, y estimó que este año el acuerdo con el FMI podría funcionar como “ancla antiinflacionaria” por lo que la suba de precios a lo largo de 2022 podría “no ser peor” que el 50,9% con que finalizó el 2021.

A fines de diciembre del año pasado, antes de los últimos saltos cambiarios y el acuerdo con el FMI, los analistas de mercado proyectaban que la inflación minorista para 2022 se ubicará en 54,8% interanual, 2,7 puntos porcentuales superior a la encuesta de un mes atrás. Los datos surgen del Relevamiento de Expectativas de Mercado (REM) realizado por el Banco Central entre consultoras, bancos y fondos de inversión.

En una medición similar que hace LatinFocus, respecto a la suba de precios las consultoras involucradas destacaron: “Se espera que la inflación sea del 51,4% a fines de 2022, lo que representa un aumento del 1,6% puntos del pronóstico del mes pasado”. Y añadieron que la estimación es que la misma se desacelere a 41,4% a finales de 2023.

“Estimamos que el índice de enero va a dar igual que el del 2021, por lo cual no va a hay un agravamiento del problema en un contexto más difícil”, dijo el secretario de Comercio, encargado de los programas de controles de precios. Por otra parte, el funcionario sostuvo que los resultados de las elecciones legislativas pasadas, la espera por el cierre del acuerdo con el Fondo Monetario Internacional (FMI) y la presión sobre las reservas internacionales plantean un panorama “difícil”.

“Sin embargo, no tenemos una aceleración -de la inflación- No va a ser peor que en 2021, por ahí puede ser mejor. Es muy importante el acuerdo con el FMI, que se recuperen reservas y que se mantenga el nivel de recuperación económica”, indicó el funcionario nacional. En este sentido, afirmó que “si todo eso se ordena, se crea un horizonte de vencimientos a más largo plazo, se acumulan reservas, y el sector privado sigue en esta dinámica, creo que la inflación de 2022 no se va a agravar respecto a 2021″.