+++
author = ""
date = 2021-05-03T03:00:00Z
description = ""
image = "/images/608eab22de83a_1004x565-1.jpg"
image_webp = "/images/608eab22de83a_1004x565.jpg"
title = "QUISIERON INGRESAR A UNA CÁRCEL MARIHUANA OCULTA EN UNAS PIZZAS Y LAS DETUVIERON"

+++

**_En poder de las dos jóvenes de 23 y 27 años se secuestraron 114 gramos de cannabis ocultos debajo del queso de cuatro pizzas ya preparadas, listas para meter en el penal de Melchor Romero._**

Dos mujeres de 23 y 27 años fueron detenidas en las últimas horas cuando intentaban ingresar varios envoltorios con marihuana oculta en pizzas preparadas a una cárcel de Melchor Romero, en La Plata, informaron este domingo fuentes policiales.

El hecho se registró el sábado por la tarde en la Unidad Penal 34, ubicada en la zona de 179 y 524.

Según indicaron los voceros, efectivos del Grupo Técnico Operativo (GTO) de la comisaría 14 habían sido advertidos sobre la maniobra que pretendían realizar las mujeres para ingresar drogas al penal. Ante esta situación, se montó un operativo y cuando divisaron a las sospechosas procedieron a interceptarlas en inmediaciones del penal.

Las fuentes indicaron que en poder de las dos mujeres, oriundas de las localidad bonaerense de General Rodríguez, se secuestraron 114 gramos de cannabis ocultos debajo del queso de cuatro pizzas ya preparadas.

Voceros de la fuerza indicaron que las mujeres fueron detenidas y trasladadas a la comisaria 14. por el delito de infracción a la Ley 23.737 donde quedaron aprehendidas a disposición de la Unidad Funcional de Instrucción (UFI) en turno en el Departamento Judicial de La Plata.