+++
author = ""
date = 2021-08-06T03:00:00Z
description = ""
draft = true
image = ""
image_webp = ""
title = "EL JUEGO DE AJEDREZ DE MASSA CON NICOLÁS MASSOT, MALENA Y JULIO ZAMORA PARA GANAR TIGRE"

+++

Según publicó el diario **La Nación** la operación para perjudicar a Segundo Cernadas en su candidatura habría sido ideada por Sergio Massa e implementada por un ex funcionario municipal de Julio Zamora ([**_link a noticia_**](https://www.lanacion.com.ar/opinion/el-parecido-entre-los-intendentes-de-pro-y-el-pj-incomoda-a-larreta-nid06082021/ "El parecido entre los intendentes de Pro y del PJ incomoda a Larreta")) e integrante en el año 2013 de la lista de concejales del Frente Renovador (link a noticia) que impulsó el actual presidente de la Cámara de Diputados de la Nación.

El periodista Daniel Bilotta en su articulo del día 6 de agosto disparó: “Massa desea que lo suceda Malena Galmarini, su esposa. Lo mismo quiere Zamora para la suya, Gisella, que será reelegida concejal. Una disputa detrás de la que está la impugnación de Juan José Cervetto contra Segundo Cernadas (Pro) para que no pueda ser reelegido en ese cargo. Cervetto es candidato a concejal de Avanza Libertad, el partido de José Luis Espert.” (**_link a nota_**)

Desde hace meses el líder del Frente Renovador desarrolló una estrategia para debilitar al actual intendente Julio Zamora y en los últimos días buscó un acuerdo que le permitiera reunificar al Frente de Todos frente a la posibilidad de ser derrotado en su territorio.

(Insertar captura de pantalla del párrafo de la nota del diario la nación en la web)

Cómo ya se publicó anteriormente Nicolás Massot también forma parte de este ajedrez. El archivo periodístico de diversos medios relata desde el ofrecimiento a Massot para formar parte del gobierno de Alberto Fernández, hasta los almuerzos en Nueva York donde el rival de Segundo Cernadas sería “El verdadero plan de Sergio Massa en Tigre”

Toda la información consignada en esta nota está respaldada por los siguientes links a otros medios profesionales.

 