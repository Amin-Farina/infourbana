+++
author = ""
date = 2021-10-15T03:00:00Z
description = ""
image = "/images/7gn7zbtxbnbybp4ghlw2ly3ba4.jpg"
image_webp = "/images/7gn7zbtxbnbybp4ghlw2ly3ba4.jpg"
title = "KICILLOF AL BORDE DEL ABISMO: SE VIÓ OBLIGADO A ANUNCIAR UN AUMENTO SALARIAL DEL 11% PARA LA POLICÍA BONAERENSE"

+++

##### **_El gobernador Axel Kicillof anunció este jueves que otorgará un aumento salarial del 11% a las y los integrantes de la Policía Bonaerense y del Servicio Penitenciario para los meses de octubre y noviembre y remarcó que con otras subas otorgadas a lo largo del año se acumulará un incremento del 46,5% con relación a diciembre de 2020._**

El anuncio del aumento salarial fue realizado este jueves por Kicillof en el marco de un acto que encabezó en la localidad de Olavarría junto al ministro de Seguridad, Sergio Berni, en una escuela descentralizada de la Policía Bonaerense.

Desde el gobierno explicaron que la medida dispuesta por Kicillof contempla un aumento salarial del 11% correspondiente a los meses de octubre y noviembre que se suma a los percibidos en marzo, julio y septiembre, lo cual representa una suba acumulada en promedio de 46,5% en relación a diciembre de 2020.

“También seguimos mejorando la calidad del salario, reforzando los sueldos básicos para recomponer la retribución según la jerarquía”, expuso Kicillof.

El anuncio también incluyó un aumento en el valor de la compensación por recargo de servicio, tanto para Policía (conocidas como horas Cores) como para el personal del Servicio Penitenciario, que se incrementará en noviembre hasta $ 180, “lo que implica una suba de cuatro veces y media desde septiembre del año pasado”, enfatizaron desde el gobierno.

El aumento dispuesto se conoció luego de que en los últimos días a través de las redes sociales comenzara a circular un rumor -replicado por diversos medios masivos de comunicación- de una posible nueva protesta policial, tal como ocurrió en septiembre de 2020.

El porcentaje de aumento promedio del 46,5% otorgado se equipara con una última oferta realizada al sector de las y los profesionales de la salud bonaerenses en el marco de la negociación paritaria y supera en un punto al 45,5% que acordaron otros gremios estatales y docentes.