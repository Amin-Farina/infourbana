+++
author = ""
date = 2021-12-10T03:00:00Z
description = ""
image = "/images/presupuesto-01.jpg"
image_webp = "/images/presupuesto-01.jpg"
title = "LAS CONSULTORAS ANTICIPARON CUÁL SERÁ LA INFLACIÓN DE NOVIEMBRE Y EN QUÉ NIVEL CULMINARÁ EL AÑO"

+++

#### **_La inflación de noviembre habría registrado una suba en los precios minoristas cercana al 3%, según arrojaron las estimaciones de las consultoras privadas, mostrando una leve desaceleración respecto de los dos últimos meses._**

Si bien las proyecciones muestran una desaceleración en el precio de los alimentos, luego del congelamiento dispuesto por el Gobierno, la inflación se mantendría en valores altos, aunque por debajo del 3,5% que el indicador mostró en octubre y septiembre.

Con las proyecciones de noviembre el Índice de Precios al Consumidor (IPC), que el Instituto Nacional de Estadística y Censos (INDEC) difundirá el próximo martes, mostraría un acumulado anual en torno al 51%, superior a la última estimación del Ministerio de Economía del 45,1%.

Ya en los primeros 10 meses del año, el indicador acumulado desde enero pasado alcanzó el 52,1%.

De acuerdo con el relevamiento del estudio Ferreres & Asociados, la inflación de noviembre se situó en el 2,9%, impactada por los precios de gastos en la vivienda, indumentaria y transporte y comunicaciones.

La Fundación de Investigaciones Económicas Latinoamericanas (FIEL) estimó un alza de precios minoristas del 3%, mientras que para la consultora Seido llegó al 3,5% en noviembre y al 52,6% en los últimos 12 meses.

El análisis de Equilibra indicó que el IPC para noviembre mostró un aumento del 3,3% respecto de octubre, lo que arroja una inflación acumulada de 46,5% y un alza interanual de 52,3%".

El pronóstico de la consultora Ecolatina señaló que la inflación se ubicó en 3,2%, con una suba mayor en alimentos, en particular la carne y estimó que desde enero la suba de los precios fue del 44,2%.

El relevamiento de Eco Go Consultores proyectó que el alza del costo de vida fue del 3,2% para el mes pasado y 51,5% en los últimos 12 meses.

La consultora Focus Market relevó que "la inflación general parte del 3% en noviembre con desaceleración de la categoría alimentos y bebidas al 2,8%, mientras que los rubros de indumentaria y vivienda están por encima del indicador general superando ambos casos el 4% de inflación mensual.

El pronóstico de C&T, la misma medición para el Gran Buenos Aires, registró un alza de 2,6% mensual en noviembre, menor que el 3,7% de octubre y el 3,2% de noviembre de 2020, mientras que la variación de los últimos doce meses fue de 51,4%".

Los rubros que habrían impactado mayormente en noviembre habrían sido los de la educación, por el aumento autorizado a diversos colegios privados, seguido de esparcimiento por los precios del turismo, y la indumentaria.

El rubro alimentos y bebidas, se habrían moderado por el congelamiento de precios que el Gobierno dispuso a fin de octubre y según la consultora LCG "en la quinta semana de noviembre la suba de precios de los alimentos promedió 1,1%, desacelerando 0,7 puntos porcentuales respecto la anterior y alimentos presentó una inflación mensual de 3% promedio en las últimas 4 semanas y 4,5% punta a punta en el mismo periodo".