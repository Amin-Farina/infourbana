+++
author = ""
date = 2021-08-26T03:00:00Z
description = ""
image = "/images/dx3pthyrvnbcvpoh2x5oeqjwcu.jpg"
image_webp = "/images/dx3pthyrvnbcvpoh2x5oeqjwcu.jpg"
title = "SUSPENDIERON A LA DOCENTE K QUE FUE FILMADA MIENTRAS ADOCTRINABA A SUS ALUMNOS"

+++

**_Laura Radetich no podrá dar clases mientras es sometida a una investigación administrativa, confirmaron las autoridades educativas en la puerta de la escuela donde se grabó el video de la polémica._**

Laura Radetich (59), docente de historia de la Escuela Secundaria Técnica Nº2 “María Eva Duarte”, de la localidad bonaerense de Ciudad Evita, fue suspendida provisoriamente después de que se diera a conocer un polémico video en el que se muestra cómo la mujer increpó de manera violenta a un estudiante de cuarto año que cuestionó al kirchnerismo y a la gestión del gobierno de Alberto Fernández.

Luis La Scaleia, supervisor de Secundaria de la Jefatura Distrital 1 de La Matanza, confirmó en la entrada del colegio que la profesora no podrá dictar clases durante el tiempo que dure la investigación. “Se va a proceder a una investigación simple. Se va a revisar el caso con el equipo de conducción, los padres y estudiantes para determinar si es un conducta reiterada de la docente o un hecho aislado. La docente fue separada preventivamente y la investigación se extenderá por algunos días”, dijo el funcionario a este medio.

La misma información fue ratificada por las autoridades de la Dirección de Escuelas bonaerense, que explicaron ante la consulta de este medio: “Durante este procedimiento, la docente no seguirá dando clases”. Al respecto, indicaron: “En situaciones de estas características corresponde realizar una investigación por presunta falta aplicando el artículo 139 del estatuto docente para reunir las pruebas pertinentes y en caso de que corresponda, relevar transitoriamente a la docente de sus funciones”.

En las imágenes, se observa cómo Radetich intentó adoctrinar y exponer a un alumno en plena clase de historia por pensar diferente a ella sobre los gobiernos kirchneristas y de Mauricio Macri. El video, grabado por uno de los chicos de cuarto año, se viralizó esta madrugada en redes sociales a partir del posteo de una periodista de Jujuy.

Las imágenes son elocuentes. En ellas, la mujer defiende al oficialismo, critica sin disimulo a la gestión de Cambiemos e interpela agresivamente a uno de los estudiantes -casi sin dejarlo hablar- para convencerlo sobre las consecuencias de una y otra administración. El joven, claramente mucho más tranquilo que ella, se plantó y le explicó que no pensaba igual.

“¿Que esto es gratis? ¿que porque tiene ojitos celestes no va a robar? Te robó. Te robó el futuro”, le dijo la docente con el tono de voz elevado e increpando al estudiante. “¿Y este no?”, le contestó el chico para indicarle si con el actual presidente, Alberto Fernández no ocurría lo mismo. “No me robó nadie, nadie te robó. vos podés venir acá y comer esta porquería porque te lo da el Estado. Andá a pagar con el sueldo de tu papá una escuela privada como esta… andá dale, andá!”, fueron algunas frases de la docente.

En ese sentido, las autoridades de la cartera educativa señalaron que “si se corrobora el hecho, se continuará con un presumario y en caso de corresponder, un sumario”. En efecto, la Dirección General de Cultura y Educación de la provincia de Buenos Aires, a cargo de Agustina Vila, informó que estaba al tanto del hecho y habían iniciado una investigación administrativa.

Por otra parte, se le dio intervención a la Dirección de Psicología Comunitaria y Pedagogía Social para que a través de sus equipos profesionales se acompañe a los estudiantes. “Cabe aclarar que la Dirección General de Cultura y Educación a través de sus orientaciones pedagógicas y su política de formación docente permanente trabaja para que la enseñanza y el aprendizaje se den siempre en un marco de diálogo y construcción colectiva del conocimiento basado en la libertad de expresión, la pluralidad y el respeto a la diversidad”, agregaron.

Radetich, 59 años, es una mujer con una amplia experiencia en la docencia y una maestría en la Universidad de Salamanca en España, según surge de su perfil en Linkedin.

De acuerdo a sus registros previsionales, Radetich trabajó en el Congreso de la Nación, en la Universidad de Buenos Aires y en la Ciudad de Buenos Aires -renunció en 2012- . Además de la escuela de Ciudad Evita donde quiso adoctrinar a sus alumnos, trabaja también en el Instituto Superior de Formación Docente y Técnica Nº46 “2 de Abril”, donde es jefa del área de Historia y Geografía y capacita a futuros docentes.

La producción del programa radial de Ernesto Tenembaum mantuvo un diálogo con Radetich. Según reprodujo el periodista al aire, la mujer aseguró: “Me filmó un alumno macrista que me pinchó, que me hizo hablar de Macri, me puedo quedar sin laburo y voy a tener que buscar un abogado“.