+++
author = ""
date = 2021-06-29T03:00:00Z
description = ""
image = "/images/j57jhvr62fgdnaugazcn3tsvlm.jpg"
image_webp = "/images/j57jhvr62fgdnaugazcn3tsvlm.jpg"
title = "UNA FISCAL SOLICITÓ EL SOBRESEIMIENTO DE TODOS LOS IMPUTADOS POR EL \"PLAN QUNITA\""

+++

**_Gabriela Baigún consideró que a lo largo de la investigación no se logró constatar que los acusados hayan incurrido en los delitos de fraude a la administración pública, ni el de abuso de autoridad, en el caso de los exfuncionarios investigados._**

La fiscal de juicio Gabriela Baigún dictaminó por sobreseer a todos los imputados de la causa del "Plan Qunita" tras considerar que a lo largo de la investigación no se logró constatar que los acusados hayan incurrido en los delitos de fraude a la administración pública, ni el de abuso de autoridad, en el caso de los exfuncionarios investigados, informaron fuentes judiciales.

La fiscal formalizó la presentación ante el Tribunal Oral Federal (TOF) 1, que ahora deberá determinar si sobresee o no a los acusados y, en consecuencia, si se hace o no el juicio oral y público.

El expediente de la causa del Plan Qunita fue instruido por el fallecido juez federal Claudio Bonadio, quien en su momento procesó al exjefe de Gabinete Aníbal Fernández y al exministro de Salud de la Nación y actual titular de esa cartera en la provincia de Buenos Aires, Daniel Gollan, y su actual viceministro, Nicolás Kreplak, entre otros.

"La nueva prueba colectada en autos en el marco de la instrucción suplementaria y su valoración conjunta con los elementos que se encontraban acumulados al expediente, permiten concluir que, en este caso, ninguna de ambas conductas por las que se promovió la acción penal constituye delito", sostuvo la fiscal en el dictamen al que accedió Télam.

Durante la instrucción suplementaria del caso Qunita, en el que se investigó si hubo irregularidades en la licitación para la entrega de kits –de cunas, chupete, moisés, entre otros-, se llevó a cabo una pericia que determinó que no hubo perjuicio a la administración pública ni se pudo constatar el direccionamiento hacia las empresas que ganaron la licitación.

#### Los argumentos

Entre las primeras consideraciones formuladas por la fiscal en su dictamen de 70 páginas, Baigún recordó que en este caso “recién se llevó adelante un peritaje contable y uno técnico con posterioridad a la elevación a juicio, como consecuencia de los pedidos realizados por las defensas y por este Ministerio Público Fiscal”.

La causa avanzó hasta el punto de ser elevada a juicio oral, a partir de una instrucción que, al no haber logrado determinar a través de un peritaje los precios de los kits, utilizó como parámetros para acreditar la existencia de sobreprecios una valuación realizada en función de facturas aportadas por la denunciante Graciela Ocaña y los valores aportados por la Sigen resultantes de las órdenes de trabajo.

En el dictamen presentado, la fiscal Baigún analizó cada uno de los precios utilizados para la instrucción del caso y luego aseveró que el análisis de los informes elaborados en el marco del peritaje contable terminó de convencerla “sobre la inexistencia de sobreprecios en la licitación investigada”.

La fiscal se diferenció de su colega que intervino en la instrucción de la causa, Eduardo Taiano, y señaló que no era posible afirmar que las empresas que resultaron ganadoras de las licitaciones se hubieran hecho de ganancias extraordinarias, información que quedó especificada en el peritaje.

“Dicha labor arrojó que las empresas Dromotech SA, Delta Obras y Proyectos SA, Cia. Comercial Narciso SRL y Grupo Diela SRL obtuvieron una utilidad neta final que va del 13,78% al 18,30%. Es evidente que tampoco a partir de estos extremos se puede concluir existencia de sobreprecios, tal como planteara la hipótesis acusatoria de Taiano”, sostuvo la fiscal.

Los márgenes de utilidad detectados, “sumado a que en esta investigación se acreditó que las seis empresas recurrieron, en muchos casos, a los mismos proveedores para la compra de insumos, me permite sostener que, aunque no contemos con información certera sobre las utilidades de Fasano SRL y Fibromad SA, puede inferirse, fundamente, que estas no pudieron haber sido irrazonables”, enfatizó Baigún.

La fiscal resaltó en su dictamen que “este proceso se inició para investigar una licitación pública en la que el Estado Nacional habría sido presuntamente perjudicado” y, sin embargo, “nunca se practicaron medidas conducentes y concretas para despejar ese interrogante”.

Por el contrario, -remarcó- “en la búsqueda de ese perjuicio potencial se irrogaron perjuicios reales a las arcas públicas, tales como que se haya omitido distribuir los kits según la finalidad pública que era perseguida, que se hayan vencido elementos perecederos que lo integraban y que se hayan afrontado los gastos derivados del depósito de todos esos kits durante un plazo innecesariamente prolongado”.

En su momento, el juez Bonadio había ordenado destruir los elementos de los kits que pudieran resultar peligrosos para la salud de los recién nacidos, decisión que no llegó a concretarse.