+++
author = ""
date = 2021-10-13T03:00:00Z
description = ""
image = "/images/varios-26-696x364-1.jpg"
image_webp = "/images/varios-26-696x364.jpg"
title = "ESPERT ARREMETIÓ CON FUERZA CONTRA CERNADAS"

+++

###### FUENTE: Norteonline

#### **_El candidato a Diputado Nacional pidió que el concejal de ‘Juntos’ en Tigre, Segundo Cernadas, “la corte con ser candidato permanente”. Durante una recorrida por Tigre y la zona norte, el referente de ‘Avanza Libertad’ apoyó a los postulantes de su espacio._**

El economista y candidato a diputado José Luis Espert recorrió la zona norte, visitó Tigre con sus candidatos y también realizó un acto en una plaza de San Fernando lugar donde respaldó a los postulantes de su fuerza y fue duro con Cernadas: “Nuestros candidatos realmente se preocupan por los problemas de los vecinos, no como Cernadas, que vive de postularse a algún puesto electivo”.

“Que se dedique a laburar en serio alguna vez, que la corte con ser candidato permanente”, enfatizó allí el dirigente de ‘Avanza Libertad’, quien en su lista propone a Juan José Cervetto como primer postulante a concejal con quien caminó y conversó con los vecinos de Tigre.

La crítica fue apuntada contra Segundo Cernadas, primer candidato a concejal de ‘Juntos’ en Tigre. “Queremos vecinos que lauren desde el lugar que sea y por los vecinos”, completó Espert.