+++
author = ""
date = 2021-09-30T03:00:00Z
description = ""
image = "/images/20210929185947_casamiento.jpg"
image_webp = "/images/20210929185947_casamiento.jpg"
title = "TIGRE: OTRA VEZ EL CLUB DE AMIGOS DE BANAVÍDEZ ENVUELTO EN NEGOCIOS MILLONARIOS"

+++
FUENTE: RealPolitik

#### **_En esta ocasión, el involucrado es Ernesto Kloster, que benefició a su padrino de bodas, Néstor Massicott, con contrataciones directas de más de 56 millones, en sólo un año._**

La semana pasada hemos visto como el intendente Julio Zamora, se enojaba por los resultados de las PASO con Alberto Fernández y con Axel Kicillof: “Nación nos arrastró”.

No hubo en el municipio una autocrítica profunda de la gestión, que se hizo sobre todo de la pandemia. Recordemos que el intendente personalmente salió a denunciar a los vecinos que se manifestaban en auto y que durante la cuarentena dura se conocieron gastos exorbitantes de protocolo y ceremonial, siendo que no podían organizarse actos ni reuniones y la directora del área, Victoria Moresi, estaba de licencia.

A esto se suma, además, los casos de nepotismo. De eso no hablaron, ni siquiera el secretario todoterreno Pedro Heyde, quien es además el jefe de campaña.

Ahora, cuando tendrían que estar trabajando para revertir (o al menos intentar mejorar) los pésimos resultados, se conoce que uno de los miembros del club de amigos de Benavídez, el delegado Ernesto Kloster, soldado del mencionado Pedro Heyde, se casó recientemente.

Este no sería el dato llamativo, sino que el padrino, Nestor Massicott, fue beneficiado con contrataciones directas de más de 56 millones de pesos, en conceptos de arreglos y mantenimiento de máquinas del municipio de Tigre solo en un año.

Nuevamente, como ya dio a conocer REALPOLITIK, la impunidad más cruda se ve en el manejo de fondos municipales.

Quizás Zamora tendría que empezar a repasar cuales son las cosas que realmente impactaron en los resultados electorales antes de eximirse de toda responsabilidad asumiendo que hace una buena gestión.