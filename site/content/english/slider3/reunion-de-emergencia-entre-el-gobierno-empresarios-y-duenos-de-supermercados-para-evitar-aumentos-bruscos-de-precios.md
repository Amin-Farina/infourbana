+++
author = ""
date = 2021-09-27T03:00:00Z
description = ""
image = "/images/supermercados.jpg"
image_webp = "/images/supermercados.jpg"
title = "REUNIÓN DE EMERGENCIA ENTRE EL GOBIERNO, EMPRESARIOS Y DUEÑOS DE SUPERMERCADOS PARA EVITAR AUMENTOS BRUSCOS DE PRECIOS"

+++

#### **_Esta tarde, la secretaria de Comercio Interior Paula Español tendrá un encuentro con directivos de las principales cadenas. La semana pasada fue el turno de empresas alimenticias y de consumo masivo._**

Esta tarde, la secretaria de Comercio Interior Paula Español se reunirá con los directivos de las grandes cadenas de supermercados que integran la Asociación de Supermercados Unidos —entre ellas Carrefour, Jumbo, Coto y Día—. Si bien este tipo de reuniones son habituales entre las dos partes, la Secretaría no suele difundirlas con anticipación, como sí lo hizo esta vez a través de un comunicado.

Desde la Secretaría indicaron que el objetivo del encuentro es que “la recomposición de los ingresos fortalezca el poder adquisitivo de los salarios y no se traduzcan en suba de precios de los bienes y servicios básicos”. De esta forma, en el sector empresario lo que esperan es que el Gobierno les pida algún tipo de “compromiso” para que no haya aumentos bruscos de precios en los próximos meses, previo a las elecciones.

Es que mientras el Gobierno comenzó a lanzar y preparar medidas que apuntan a “poner plata en el bolsillo de la gente” hay un temor de que ese dinero se termine trasladando a los precios de los productos básicos: alimentos y artículos de higiene y limpieza. La semana pasada, la Secretaría ya tuvo una serie de reuniones con directivos de empresas de consumo masivo, que seguirán durante esta semana. La idea, según el comunicado oficial, es establecer “reglas que permitirán en los próximos meses preservar la capacidad de compra en el mercado interno”.

Fuentes cercanas a las empresas señalaron que el objetivo de las conversaciones fue reafirmar el compromiso de las grandes compañías para sostener las “mesas de coordinación” con la Secretaría de Comercio por el tema precios. “La idea es que ningún aumento de precios de los proveedores sea pasado a los supermercados y a la consumidores sin una autorización previa de la Secretaría de Comercio. Esas mesas viene funcionando y el objetivo es que se sostengan y que sean cada vez más estrictas”, indicaron.

En la práctica, las empresas —al menos las más importantes— pasan los aumentos de precios previstos que son analizados por la Secretaría, que puede validar todo, una parte o nada, de acuerdo con cada producto y cada empresa. “Así viene funcionando”, indicaron.

Desde la Secretaría de Comercio Interior confirmaron que, desde la semana pasada, Paula Español retomó reuniones con las principales empresas de consumo masivo para trabajar en una “agenda determinada de precios, que tiene como objetivo cuidar el acceso a los productos de la canasta básica de las y los argentinos y evitar que se produzcan abusos”, dijeron.

Español comenzó a levantar nuevamente su perfil luego de la crisis interna que se produjo en el Gabinete de Alberto Fernández luego de la derrota del oficialismo en las PASO. La secretaria de Comercio fue una de las funcionarias que presentó se renuncia, pero que finalmente no fue aceptada.

La relación entre el Gobierno, las empresas de consumo masivo y las grandes cadenas atravesó varios momentos de tensión durante este año. Por un lado, la aprobación y reglamentación de la Ley de Góndolas y luego la creación de un sistema de monitoreo de precios y stocks a grandes firmas de distintos rubros (el Sipre, Sistema Informativo para la Implementación de Reactivación Económica).

Los números de consumo masivo durante 2021 siguen en baja, con una caída acumulada de 5,4% en comparación con el año anterior. Con todo, durante agosto pasado, el resultado para el sector —que incluye la venta de alimentos, bebidas, productos de higiene y limpieza, entre otros— fue de 0%, un empate comparado con el número de agosto 2020 que había sido negativo.

Sin embargo, este resultado no fue parejo entre los distintos canales de comercialización: las cadenas de supermercados lograron una variación positiva de 5,1%, mientras que los autoservicios independientes (almacenes y supermercados de cercanía) tuvieron un desempeño negativo de 4,1%, de acuerdo al relevamiento mensual que realiza la consultora Scentia entre más de 20.000 puntos de venta a través del sistema de scanning.