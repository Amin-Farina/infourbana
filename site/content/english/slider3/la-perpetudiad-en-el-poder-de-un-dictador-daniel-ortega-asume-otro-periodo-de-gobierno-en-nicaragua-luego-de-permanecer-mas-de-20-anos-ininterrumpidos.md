+++
author = ""
date = 2022-01-10T03:00:00Z
description = ""
image = "/images/57604195_6-1.jpg"
image_webp = "/images/57604195_6.jpg"
title = "LA PERPETUDIAD EN EL PODER DE UN DICTADOR: DANIEL ORTEGA ASUME OTRO PERÍODO DE GOBIERNO EN NICARAGUA LUEGO DE PERMANECER MÁS DE 20 AÑOS ININTERRUMPIDOS"

+++

##### **_Conservará una abrumadora mayoría en el Parlamento, pero afrontará un creciente aislamiento internacional tras los fuertes cuestionamientos a las elecciones de noviembre pasado._**

Daniel Ortega asumirá este lunes su quinto mandato de cinco años como presidente de Nicaragua. Será el cuarto consecutivo y el segundo junto con su esposa Rosario Murillo como vicepresidenta. Así estará en el poder de manera consecutiva durante 20 años (2007-2027), aunque acumula otro lustro de su primera presidencia (1985-1990).

Y lo hará en medio de fuertes cuestionamientos internacionales a su última reelección alcanzada en las elecciones del 7 de noviembre tras el arresto de siete potenciales candidatos presidenciales de la oposición. Además, tendrá un Parlamento con una abrumadora mayoría que le permitirá llevar las riendas del país centroamericano sin ningún contratiempo interno.

Qué países enviarán delegaciones oficiales a la asunción de Daniel OrtegaLa ceremonia de toma de posesión se llevará a cabo este lunes a las 18:00 locales (21:00 de la Argentina) en la Plaza de la Revolución de Managua. No se espera la presencia de ningún jefe de Estado, aunque el Gobierno nicaragüense informó que asistirán delegaciones de Argentina, Bielorrusia, Bolivia, Cuba, China, Corea del Norte, Irán, México, Palestina, Rusia, Siria, Turquía, Venezuela y Vietnam, entre otros países, según Efe.

Tras el apoyo de Nicaragua a la Argentina para alzarse con la presidencia de la Comunidad de Estados de Latinoamérica y Caribe (CELAC), el Gobierno de Alberto Fernández se aprestaba a enviar una misión oficial al acto según confirmó el embajador argentino en Nicaragua, Mateo Daniel Capitanich al diario local La Prensa. Sin embargo finalmente se dio marcha atrás con esta decisión.

Las elecciones en las que fue reelecto Ortega fueron desconocidas por unos 50 países, la Unión Europea, y la Organización de Estados Americanos (OEA), que las tildó de “ilegítimas”.

Daniel Ortega contará con una mayoría abrumadora en el Parlamento Ortega tendrá en su nuevo período de gobierno una abrumadora mayoría en el Parlamento que le permitirá gobernar sin contrapesos hasta el final de su mandato.

En la nueva Asamblea Nacional instalada el domingo el grupo parlamentario sandinista suma 75 de los 91 escaños, un 82,4 % del total. De esa manera podrá aprobar reformas a la Constitución y leyes ordinarias de forma unilateral. En el período legislativo anterior los sandinistas y sus aliados contaron con 71 diputados.

Los legisladores reeligieron por unanimidad al diputado sandinista Gustavo Porras como presidente de la Junta Directiva del Parlamento. Este dirigente sandinista se encuentra sancionado por Estados Unidos y Canadá por promulgar “leyes represivas” en el marco de la crisis sociopolítica que vive el país desde abril de 2018.

> “Vamos a continuar trabajando en favor de la paz, de la independencia definitiva de nuestro país, independencia en todos los sentidos”, dijo Porras. 

Durante su gestión se aprobaron proyectos polémicos como la ley de ciberdelitos que castiga las noticias falsas, la de agentes extranjeros y otras reformas legales que criminalizan las protestas y establecen la cadena perpetua.

##### La oposición nicaragüense convocó a una jornada de protesta

En tanto, diversas organizaciones opositoras convocaron a una jornada de protesta en varios países bajo el lema “Nicaragua no tiene Gobierno ni poderes legítimos del Estado”.

La oposición local sostiene que el triunfo de Ortega en las últimas elecciones es absolutamente ilegítimo ya que antes de los comicios su gobierno disolvió tres partidos políticos y detuvo a más de 40 dirigentes opositores, entre ellos a siete aspirantes presidenciales, incluida Cristiana Chamorro, la gran favorita según las encuestas.

Los 16 diputados derechistas que integran el parlamento son 10 del Partido Liberal Constitucionalista (PLC), dos de la Alianza Liberal Nicaragüense (ALN), dos del Partido Liberal Independiente (PLI), uno a la Alianza por la República (APRE) y uno al partido indígena Yatama.

La oposición política, que no tiene asiento en el Congreso y fue excluida de los comicios del 7 de noviembre, tilda a estos legisladores derechistas de “colaboradores” del gobierno.