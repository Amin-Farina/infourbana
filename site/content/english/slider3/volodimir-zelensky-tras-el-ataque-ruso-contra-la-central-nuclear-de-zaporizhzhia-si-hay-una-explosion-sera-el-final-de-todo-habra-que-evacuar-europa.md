+++
author = ""
date = 2022-03-04T03:00:00Z
description = ""
image = "/images/1645727657741chernobyl-ucrania-rusia-guerra.jpg"
image_webp = "/images/1645727657741chernobyl-ucrania-rusia-guerra.jpg"
title = "VOLODIMIR ZELENSKY, TRAS EL ATAQUE RUSO CONTRA LA CENTRAL NUCLEAR DE ZAPORIZHZHIA: “SI HAY UNA EXPLOSIÓN, SERÁ EL FINAL DE TODO, HABRÁ QUE EVACUAR EUROPA”"

+++

##### **_El presidente de Ucrania, Volodimir Zelensky, se refirió este viernes al ataque ruso contra la central de nuclear de Zaporizhzhia y aseguró que, de producirse una explosión, “será el final de todo”._**

“Es la primera vez en nuestra historia, en la historia de la humanidad, que un Estado terrorista recurre al terror nuclear. No es una amenaza, ahora es real. Hay que detener de inmediato al ejército ruso. Si hay una explosión, será el final de todo. El final de Europa. Habrá que evacuar Europa. No permitamos que Europa muera en esta catástrofe”, dijo Zelensky a través de un video este viernes.

“Es necesario que se endurezcan inmediatamente las sanciones contra el estado terrorista nuclear”, declaró el mandatario, al tiempo que pidió a los rusos “que salgan a la calle” para exigir que se detengan los ataques de su país a plantas nucleares ucranianas.

Zelensky alegó que las tropas rusas habían disparado a sabiendas contra la instalación nuclear. “Se trata de tanques equipados con cámaras térmicas, por lo que saben dónde están disparando”, precisó.

La planta nuclear de Zaporizhzhia, ubicada en la región homónima de la ciudad ucraniana de Energodar, se incendió durante varias horas este viernes luego de haber sido atacada por las fuerzas invasoras de Rusia, lo que desató la preocupación en toda Europa debido al peligro de una potencial catástrofe atómica.

De hecho, cuando el fuego todavía no había sido controlado, el alcalde de este distrito, Dmitry Orlov, advirtió en su cuenta de Telegram que este hecho constituía una “amenaza a la seguridad mundial”.

La Zaporizhzhia NPP (Nuclear Power Plant) comenzó a ser construida por la Unión Soviética en 1979 y la primera de sus seis unidades de potencia empezó a funcionar en 1984, menos de dos años antes de la tragedia de Chernobyl.

Se encuentra en la zona de estepa de Ucrania, en la orilla de un embalse de agua de la ciudad de Kakhovka, situada a unos 71 kilómetros al nordeste de Kherson. Es la planta nuclear más grande de Europa y una de las mayores de todo el mundo.

Posee un total de seis reactores del tipo VVER-1000, los cuales funcionan con agua presurizada y pueden producir genera entre 40 000 y 42 000 millones de kWh de potencia, lo que representa una quinta parte del abastecimiento anual promedio de electricidad en el país. Estos reactores disponen de una duración de entre 40 y 60 años, o incluso más gracias al avance tecnológico.

Es operada por la empresa nacional NNEGC Energoatom y en sus piscinas de enfriamiento hay cientos de toneladas de combustible nuclear altamente radiactivo, razón por la cual tres de ellas han sido cerradas cuando comenzó el conflicto armado con Rusia.

De acuerdo con lo que se sabe hasta el momento, el incendio principal se desató en uno de esos seis reactores VVER-1000 que se encuentran en la central, el cual está en renovación y no opera, pero adentro tiene combustible nuclear.