+++
author = ""
date = 2021-09-02T03:00:00Z
description = ""
image = "/images/gisela-julio-zamora-acto-campana-1.jpeg"
image_webp = "/images/gisela-julio-zamora-acto-campana.jpeg"
title = "JULIO ZAMORA: \"EL FDT TIENE EL COMPROMISO DE INVERTIR EN EDUCACIÓN, SALUD Y GENERAR EMPLEO PARA CADA ARGENTINO Y ARGENTINA\""

+++

**_En un acto en la Sociedad de Fomento de Ricardo Rojas, el intendente Julio Zamora acompañó a la primera precandidata a concejal Gisela Zamora en la presentación de la lista de unidad del Frente de Todos Tigre y convocó a la comunidad a respaldar el proyecto político en las próximas elecciones legislativas._**

El Frente de Todos tiene el compromiso de invertir en educación, en salud y generar empleo para cada argentino y argentina. La única salida que tenemos es por intermedio del trabajo genuino y por eso vamos a garantizar las mejores condiciones para que las PyMES puedan transitar por el camino del crecimiento y así beneficiar a cada habitante de esta república en la etapa de la pos pandemia, expresó el jefe comunal.

Consultado sobre el lanzamiento en Ricardo Rojas, señaló: Mantuvimos un encuentro muy cálido con vecinos e instituciones para expresarle las propuestas y los sueños que deseamos desde nuestro espacio para seguir mejorándoles la calidad de vida. Tenemos proyectos previstos en materia de infraestructura educativa, sanitaria y deportiva, nuevas plazas y un diseño de ciudad integral que nos permitirá abarcar cada rincón de nuestro querido Tigre.

En materia de obra pública para Ricardo Rojas, el Municipio de Tigre tiene proyectada la construcción de 2 baterías de baños en las escuelas Secundaria N°34 y Primeria N°44, entre otras labores, la reforma completa de la Técnica N°2 y nuevas plazas con juegos para niños y niñas. Además, Zamora comunicó que a través de la articulación con el Gobierno Nacional ya se ejecutaron 35 mil metros de cloacas en la localidad y se llevará adelante la reparación de la Ruta 9, desde Henry Ford hasta el Hospital Provincial de Pacheco.

Estamos visitando una localidad que avanzó mucho a raíz de obras importantes que ha hecho el Municipio, Provincia y Nación en los últimos años en educación, en cloacas, agua corriente, labores de pavimentación y la puesta en valor en los espacios públicos. Queremos seguir trabajando desde el Honorable Concejo Deliberante y junto al intendente Julio Zamora para seguir transformando la vida de todas y todos los vecinos de Tigre. La prioridad que tenemos es cuidar lo que se hizo bien y avanzar en lo que nos falta, ponderó la precandidata a concejal Gisela Zamora.

Los actos de presentación de candidatos a concejales y consejeros iniciaron en el Club 12 de Octubre de Benavídez y en la Sociedad de Fomento de Dique Luján. Luego, continuaron en el Club Pacheco, en la localidad de Tigre centro y en el Club Junta Vecinal de Troncos del Talar. El 12 de septiembre se celebrarán las Primarias, Abiertas, Simultáneas y Obligatorias (P.A.S.O); mientras que el 14 de noviembre se llevarán adelante elecciones legislativas generales en todo el país.

Para nosotros es un enorme orgullo presentar esta lista con candidatos que son de Tigre y que hace muchísimos años vienen trabajando de manera comprometida con la comunidad. Después de tanto tiempo de pandemia y las dificultades que eso implicó, tenemos la esperanza de encarar un futuro prometedor y saber que vamos hacia la vida que queremos, expresó la concejal Micaela Ferraro Medina, y agregó: Invitó a todas y todos a caminar por el distrito y ver la cantidad de obras en ejecución de cloacas y agua corriente que inició el gobierno nacional a través de la gestión de Malena Galmarini al frente de AySA.