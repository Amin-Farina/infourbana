+++
author = ""
date = 2021-08-11T03:00:00Z
description = ""
image = "/images/daniel-funes-de-rioja-nuevo___cbf_i2ous_1256x620__1.jpg"
image_webp = "/images/daniel-funes-de-rioja-nuevo___cbf_i2ous_1256x620__1.jpg"
title = "PARA LA UIA, LAS EMPRESAS TIENEN DERECHO DE DEJAR DE PAGARLE EL SALARIO A LOS EMPLEADOS QUE NO QUIERAN VACUNARSE"

+++

**_La Unión Industrial Argentina (UIA) aseguró que la normativa vigente habilita a las empresas a dejar de pagar el salario de aquellos empleados que, teniendo la posibilidad de vacunarse, deciden no hacerlo, lo que les impide volver a trabajar de manera presencial. Los industriales no descartan que esa interpretación pueda ser judicializada._**

La cuestión de los empleados que evitaban ser inmunizados contra el Covid a pesar de que existiera la disponibilidad de vacunas en el país es desde hace semanas un “gris” legal. La última resolución del Ministerio de Trabajo habilitó a las empresas a reclamar presencialidad a su dotación de personal que cuente con al menos una dosis.

Pero no había ninguna especificación para los casos de empleados que no quieren vacunarse. Este martes en su reunión de Junta Directiva, la UIA consensuó una interpretación por la cual un trabajador que no desea vacunarse pondría en peligro los protocolos puestos en marcha por la pandemia, por lo que ese incumplimiento habilitaría lo que en derecho se llama cese de dispensa.

Así lo aseguró el presidente de la entidad fabril Daniel Funes de Rioja en una conferencia de prensa virtual. “Quien quiera entrar en un lugar de trabajo colectivo tiene que tener las medidas (de prevención) que son vacunarse. Si no quiere, no podría ingresar a ese lugar, lo que implica que cesa la obligación de pago”, dijo el ejecutivo fabril.

En ese sentido, Funes de Rioja mencionó que es una interpretación que fue consensuada “en el ámbito de la junta directiva de la UIA y hemos tenido en cuenta la experiencia internacional de las regulaciones”, agregó.

“Nadie puede obligar a vacunarse, pero nadie puede obligar a reinsertar a alguien en el mercado laboral”, enfatizó el también titular de la Coordinadora de las Industrias de Productos Alimenticios (Copal), y aclaró que la propuesta aplica a “aquellos que están en condiciones de vacunarse en las respectivas jurisdicciones”.

Por otra parte, el ejecutivo industrial apuntó que esa decisión deberá ser conversada “sector por sector” con los sindicatos sobre los que, dijo, considera que tuvieron “un alto sentido positivo de cooperación para que todo el mundo se cuidara. Hay un sentido de cuidado recíproco. Para cuidarnos entre todos, nos vacunamos. Esto no significa que esto se pueda imponer, pero la persona que no se vacuna no ingresa (al lugar de trabajo) porque configura un riesgo”, continuó.

Por otra parte, afirmó que el cese de la dispensa debería implicar la interrupción del pago del salario a ese trabajador, aunque no aseguró que eso también incluya la posibilidad de terminar con el contrato laboral, en medio de la vigencia de la prohibición de despidos. “Cada uno verá como lo interpreta, pero no estamos propiciando despidos”, explicó.

Sobre la implementación de una interpretación de ese estilo de la normativa vigente, Funes de Rioja consideró que la UIA “como institución, hasta ahi llega”. “Cada empresa y cada trabajador lo definirán. Hay países que están generando incentivos para que la gente se vacune. Ese libre albedrío tiene un límite”, mencionó el pope fabril.

Además, el presidente de la Unión Industrial mencionó que “todo en la Argentina puede judicializarse”. “La junta directiva de la UIA no tiene imperio, cada uno hace lo que decida desde su punto de vista. Aún con las normas claras, hay un principio de buena fe, a nadie se le puede ocurrir ganar un salario sin trabajar. Lo deseable sería que no se judicialice. Hay fundamento para eso”, apuntó Funes de Rioja.

Hace algunos días, desde el Ministerio de Trabajo habían asegurado que ese “gris” normativo era una situación que estaba en estudio pero que no se analizaba una resolución adicional que dé cobertura a las empresas para poder exigirles a esos trabajadores que vuelvan a sus puestos.

En el estado actual de cosas, la obligación de volver a los puestos de trabajo corren solamente para aquella parte de la dotación de personal que ya cuente con al menos una dosis de alguna vacuna y que ya acumule 14 días desde la inoculación. Se trata de una resolución que determinaron los ministerios de Trabajo y de Salud a principios abril, cuando el ritmo de inmunización era mucho menor al de la actualidad.

De esa manera, la única normativa vigente es la de aquel 9 de abril con la cual el Gobierno flexibilizó los criterios de presencialidad habilitada para los empleados y determinó que las empresas podrían exigir que vuelvan a sus puestos de trabajos a todos aquellos empleados que hayan recibido al menos una dosis de una vacuna contra el coronavirus.