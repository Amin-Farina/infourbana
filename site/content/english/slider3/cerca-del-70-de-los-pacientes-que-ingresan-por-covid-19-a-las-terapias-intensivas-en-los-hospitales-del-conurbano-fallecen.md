+++
author = ""
date = 2021-05-04T03:00:00Z
description = ""
image = "/images/5ef5cdc01bfd3_1004x565.jpg"
image_webp = "/images/5ef5cdc01bfd3_1004x565.jpg"
title = "CERCA DEL 70% DE LOS PACIENTES QUE INGRESAN POR COVID-19 A LAS TERAPIAS INTENSIVAS EN LOS HOSPITALES DEL CONURBANO, FALLECEN"

+++

Las Unidades de Terapia Intensiva de los hospitales bonaerenses que forman parte del AMBA están estresados no solo por la falta de plazas en esa área sensible para atender a los pacientes afectados por el COVID-19, sino también por la alta letalidad. Casi 7 de cada 10 mayores de 60 años que ingresan a las UTI mueren.

La cifra es oficial. Fue confirmada por Daniel Gollan, el ministro de Salud de la provincia de Buenos Aires, ante la consulta de Infobae. La preocupación entre las autoridades del gobierno de Axel Kicillof es tal que su jefe de Gabinete, Carlos Bianco, afirmó: “Hay que lograr que no lleguen a terapia intensiva porque una vez que lo hacen no se puede asegurar su supervivencia”.

El 27 de abril, tan solo ocho días atrás, el funcionario, a través de su cuenta de Twitter informaba que “de cada 10 pacientes que ingresan a terapia intensiva, 6 fallecen”.

El número exacto, según el reporte del día de ayer al que accedió este medio, refleja que el pico de muertes de pacientes COVID fue del 67,9%.

El porcentaje de letalidad exhibió un aumento a medida que las camas UTI se poblaron. Por ejemplo, el pico de enero fue del 56,3%. En febrero la cifra disminuyó al 50,0%; al mes siguiente trepó al 56,6% y en abril tocó, hasta ahora, el techo del casi 70%.

Las cifras de mortalidad de mayores de 60 años en la provincia de Buenos Aires no sorprenden. A nivel nacional, el 83% de todos los fallecidos tienen más de 60 años, más allá de si ingresaron o no a una cama UTI.

Para Gollan, la cantidad de fallecidos en terapia “se estabilizó” y aspira a que comience a disminuir a medida que -se supone después de esta semana- el número de infectados comience a descender por las restricciones impuestas por el presidente Alberto Fernández y adoptadas por Kicillof.

Tanto Gollan como Bianco advierten que “no alcanza para evitar las muertes el tener más camas”. Según argumentan, “tener más camas morigera un poco la mortalidad, pero lo que hay que lograr es que la gente no llegue a las terapias porque, cuando llegan, para un porcentaje altísimo de los pacientes ya es tarde”.

Julieta Calmels, la subsecretaria de Salud Mental del ministerio de Salud provincial le dijo a Infobae, después de recorrer buena parte de los hospitales bonaerenses: “Ayer estuve con un intensivista que tenía más de veinte pacientes en terapia. Él gestiona todos los partes diarios. La tasa de letalidad en las terapias es del sesenta o el setenta por ciento. Este médico, va a informar una cantidad de noticias de fallecimientos, en corto tiempo, como nunca en su vida”.

La funcionaria, que trabajó en emergentología por ejemplo en la tragedia del boliche Cromañón el 30 de diciembre de 2004, puso en palabras la situación actual que se vivencia en las terapias, una “catástrofe”.

“Hoy los trabajadores de la salud no solo tienen que cuidar del cuerpo biológico, también tienen que administrar la angustia de los pacientes, de los familiares. Ellos están agotados. Están agotados del año de trabajo que tienen encima. Es difícil trasmitir el agotamiento mental y físico. Atraviesan situaciones de mucha angustia y de mucha exigencia, como si en el cuerpo y en el psiquismo de ellos cayera una parte muy importante de la catástrofe que estamos viviendo”, dijo la psicóloga colaboradora de Gollan.

Si se observa la curva de fallecimientos en las UTI del conurbano en pacientes COVID menores de 60 años, los porcentajes son menores. Entre esa franja etaria fallecen 4 de cada 10 de los pacientes que ingresan con complicaciones. El pico de abril fue exactamente de 43,0.

Juan Riera el Director Provincial de Hospitales se lo explica de manera sencilla a este medio: “El deterioro de los pacientes es más rápido que durante la primer ola”.

El diagnóstico tiene una razón científica: las nuevas cepas de coronavirus que circulan en el país, como las variantes 501Y.V1 (Reino Unido), la 501Y.V3 (Manaos), la P.2 (Río de Janeiro) y la CAL.20C (linaje B.1.427, California), aceleran la velocidad con la que afectan a los pacientes, sobre todo en las vías superiores. El año pasado, un paciente, que se complicaba, podía demorar hasta 15 días para requerir una internación en terapia intensiva. Las mutaciones aceleraron ese proceso.

“Estamos muy preocupados los que estamos del lado hospitalario, de las terapias, porque en la provincia de Buenos Aires tuvimos picos de 15.000 casos diarios de COVID positivo. Ese acumulado aún no impacto en las terapias intensivas. Tenemos un stock de camas que se va agotando; podemos agregar alguna cama, pero no mucho más, el sistema tiene un límite. Por eso tenemos que descomprimir, comenzar a dar altas y eso tiene que estar acompañado de un descenso de la curva”, explicó Riera.

Ayer las cifras de infectados detectados en el país en las últimas 24 horas no tranquilizaron los ánimos. Al contrario. El reporte del ministerio de Salud de la Nación reportó 26.238 nuevos casos y 12.211 pertenecen a la provincia de Buenos Aires; También notificó 412 muertes y 193 de ellos eran pacientes que se atendían en los hospitales públicos, privados o municipales bonaerenses.