+++
author = ""
date = 2022-01-25T03:00:00Z
description = ""
image = "/images/17hendersonisland1-articlelarge.jpg"
image_webp = "/images/17hendersonisland1-articlelarge.jpg"
title = "BASURA EN LAS PLAYAS BONAERENSES: MÁS DEL 80% CORRESPONDE A RESIDUOS PLÁSTICOS"

+++

##### **_El dato se desprende del Censo Provincial de Basura Costero Marina 2021, realizado en 21 localidades de la provincia de Buenos Aires. Las colillas de cigarrillo, el residuo que más abunda_**

Organizada por diferentes ONGs de las principales ciudades costeras de la provincia de Buenos Aires y coordinada por 438 voluntarios, entre los meses de septiembre y octubre de 2021 se llevó a cabo la quinta edición del Censo Provincial de Basura Costera Marina, la cual arrojó, entre otros resultados salientes, que la mayor parte de los residuos encontrados corresponden a desechos plásticos.

El censo, que cubrió un área total de 422.501 m2 -equivalente a 58 canchas de fútbol profesional- y contó con la colaboración de más de 20 organizaciones civiles de 21 localidades bonaerenses, registró un total de 40.331 residuos censados, de los cuales el 84,5% corresponde a plásticos y ratifica una tendencia que se repite respecto a los cuatro censos anteriores.

Este dato toma mayor relevancia si se tiene en cuenta el último informe de las Naciones Unidas para el Medio Ambiente, en el cual se concluyó que, si las condiciones continúan siendo las mismas, la contaminación por plástico en los ecosistemas marinos se podría duplicar para el año 2030.

Según precisa la Fundación Vida Silvestre Argentina, la basura marina responde a cualquier material persistente de fabricación humana y sólido que es abandonado en el medio marino y costero. Sumado a los desechos arrojados por las personas en los ambientes costeros, diariamente toneladas de basura llegan a los mares a través de los sistemas de drenaje urbanos -bocas de tormenta y pluviales, por ejemplo-, la desembocadura de los ríos y por la acción del viento que los traslada alrededor del mundo.

Este escenario sirve para explicar un dato reiterado y desalentador: el 80% de la basura marina proviene de las plataformas continentales, ya sea por la mala disposición y un manejo inadecuado de los residuos urbanos o bien de las aguas pluviales no tratadas. Asimismo, muchos provienen de los barcos comerciales y pesqueros que navegan por aguas internacionales.

El Censo Provincial de Basura Costero Marina 2021 también determinó que, en segundo lugar, los residuos más recurrentes en ámbitos costeros y marinos son vidrios, con el 4,5% sobre el total del material censado, mientras que papeles y cartones representan un 2,7%. Los metales (2,3%) y otros residuos englobados en la categoría “Otros” (5,9%) cierran esta edición del informe.

“Los residuos plásticos, sean macro o microplásticos, ya forman parte de todos los ambientes naturales y del alimento de muchas especies, incluidos los seres humanos. La disminución de la producción de embalajes y plásticos de un solo uso, la compra consciente, la disposición correcta de los residuos, y un sistema eficaz de disposición final y recuperación todavía están pendientes”. analizó Verónica García, especialista en Ecosistemas Marinos y Pesca Sustentable de la Fundación Vida Silvestre Argentina.

**Entre los residuos más encontrados en las playas bonaerenses, se destacan: colillas de cigarrillo (19,6%), fragmentos plásticos (18,7%), envoltorios plásticos (13,2%), bolsas plásticas (10%) y restos de nylon (8,1%).**

Por su parte, las 21 localidades censadas para la edición 2021 fueron: San Pedro, Punta Lara, Magdalena, Punta del Indio, San Clemente del Tuyú, Las Toninas, Santa Teresita, Mar del Tuyú, Mar de Ajó, Nueva Atlantis, Ostende, Villa Gesell, Mar Chiquita, Mar del Plata, Balneario Marisol, Monte Hermoso, Pehuen Có, Punta Alta, Bahía Blanca, Villa del Mar e Isla Lucero.