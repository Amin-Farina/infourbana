+++
author = ""
date = 2021-09-20T03:00:00Z
description = ""
image = "/images/111627_3-1.jpg"
image_webp = "/images/111627_3.jpg"
title = "DANIEL FILMUS SOBRE SU NOMBRAMIENTO COMO MINISTRO DE CIENCIA Y TECNOLOGÍA: \"EL MÁS SORPRENDIDO FUI YO\""

+++

#### **_Daniel Filmus sostuvo que se sorprendió con su designación como ministro de Ciencia y Tecnología en lugar de Roberto Salvarezza, luego de la crisis que generó la derrota en las PASO, el enfrentamiento entre Alberto Fernández y Cristina Kirchner y los cambios en el Gabinete._**

"Me encantó la propuesta, me toca hacerlo después de un ministro de lujo como Salvarezza, así que muy entusiasmado y asumiendo un desafío con científicos y con gente que ha trabajado durante todo este tiempo en colocar a la ciencia en el tope de las prioridades del país, porque hace a una política de Estado y al futuro", expresó en FM Milenium.

El pase de Filmus de secretario de Malvinas, Antártida y Atlántico Sur de la Cancillería argentina a la cartera de Ciencia y Tecnología fue uno de los cambios más sorpresivos, tras la crisis en la Casa Rosada.

"Con el nombramiento el más sorprendido fui yo",expresó. Y agregó que la evaluación del cambio fue por cuenta del Presidente. 

"Yo también planteé que me parecía una buena gestión y él me dijo que hay que seguir en el mismo camino, pero darle una impronta mucho más articulada con los otros ministerios", expresó.

Al tiempo que habló de los aportes científicos que pueden hacerse al desarrollo productivo del país, a partir del impulso a medidas sanitarias, ambientales y sociales, en sintonía con los ministerios de Economía y de Desarrollo Productivo.

Filmus también contó los motivos por los que aceptó el ofrecimiento del jefe de Estado, para sumarse al Gabinete luego del duro revés en las primarias y remarcó el "desafío enorme" desde lo personal.

"Si hubiéramos triunfado no me lo hubieran ofrecido. En la cabeza del Presidente debía estar un cambio de Gabinete después de noviembre quizás", señaló en referencia a una de los temas que generó cortocircuito entre el Presidente y su vice.

Al ser consultado por la desprolijidad en la salida del canciller Felipe Solá, que será reemplazado por Santiago Cafiero, sostuvo que es una situación "difícil" y destacó su buena gestión.

"Me tomó de sorpresa como pasa en estas cuestiones, siempre que hay cambios de Gabinete se producen cuestiones difíciles tanto para los que tienen que salir como los que tienen que cambiar de posición, estaré abrazándolo y acompañándolo cuando lo pueda ver", expresó.