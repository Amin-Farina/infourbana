+++
author = ""
date = 2021-12-20T03:00:00Z
description = ""
image = "/images/a7f8cc39-ae30-4f2b-9fad-14c71ca50ca1_16-9-discover-aspect-ratio_default_0-1.jpg"
image_webp = "/images/a7f8cc39-ae30-4f2b-9fad-14c71ca50ca1_16-9-discover-aspect-ratio_default_0.jpg"
title = "TRAS LAS MASIVAS PROTESTAS, LA LEGISLATURA DE CHUBUT DEROGÓ LA LEY DE ZONIFICACIÓN MINERA"

+++

#### **_A casi una semana de su sanción, las masivas movilizaciones forzaron al gobernador Mariano Arcioni a dar un paso atrás en la ley de zonificación minera en Chubut. Este martes, la Legislatura provincial derogó la normativa que autorizaba la megaminería en la zona conocida como “la meseta”._**

La derogación fue aprobada de manera unánime por todo el cuerpo. Así, los legisladores que aprobaron la iniciativa la semana pasada, resolvieron modificar su postura ante el estallido social y el clima tenso que se vive en la provincia. El permiso oficial duró apenas seis días.

Afuera de la Legislatura, cientos de manifestantes se habían congregado para esperar los resultados de la votación y celebrar la decisión que puso en freno a la actividad metalífera en Chubut. En lo concreto, la norma habilitaba que la minera canadiense Pan American Silver avance con la extracción de plomo, plata y cobre en el marco del proyecto “Navidad”. La iniciativa era cuestionada con dureza por el movimiento ambientalista, por tratarse de una actividad contaminante.

Ante la virulencia que tuvieron las protestas, desde las 8 de la mañana, el debate del cuerpo legislativo se realizó de manera virtual ya que “no se encontraban dadas las condiciones edilicias, ni de seguridad necesarias para continuar con el normal desarrollo de la labor legislativa y administrativa”. El edificio había recibido daños durante las multitudinarias marchas.

En la reunión programada se trató el proyecto firmado por Mariano Arcioni que deroga la Ley de Zonificación de la Actividad Minera en Chubut. Si bien había cuatro proyectos en carpeta sobre el tema, se decidió por avanzar con la iniciativa propuesta por el gobernador.

Durante el desarrollo de la sesión, la diputada del Frente de Todos, Mónica Saso revirtió su voto a favor de la megaminería de la semana pasada, pero lo defendió al asegurar que muchas de los artículos en el texto incluían parte del “reclamo social” ambiental. Saso dijo era legítimo que “la meseta tenga la oportunidad de dignificar a su pueblo para un desarrollo sustentable” y su progreso” y cuestionó “la demagogia oportunista” de quienes ahora se oponen.

Sin embargo, aclaró: “Quiero adelantar mi voto afirmativo a la derogación y que aprobemos un fondo de reparación histórica para la meseta”.

La diputada de Chubut Unido y ex intendenta de Rawson, Rossana Artero, consideró que “cambiar de opinión es de gente racional” y una “actitud de grandeza”. “Sé de muchos diputados que votaron convencidos esta ley que hoy seguramente derogaremos”, elogió, y señaló que otros legisladores que estaban en contra y que fueron “presionados por otros intereses, votaron en contra de sus voluntades”.

“Pienso en los diputados oficialistas, en cuánta seguridad les brinda un gobernador que los presiona para votar una ley totalmente anti popular, los expone y los crucifica. Que después de asegurar ‘no voy a dar un paso atrás’, los entrega”, se preguntó Artero durante la sesión.

María Andrea Aguilera, de la UCR, sostuvo que el proyecto registraba una “carecia de licencia social y de legitimidad” no solo del desarrollo minero, sino “de un gobierno”, en relación a Mariano Arcioni, al que calificó de una gestión “sin rumbo y sin plan, con intereses desconectados de las necesidades del pueblo chubutense”.

“No podemos dar la vuelta la página como si nada. Esto no puede seguir ocurriendo”, afirmó la diputada, quien enumeró todos los edificios gubernamentales destruidos en el marco de las protestas. ¿Quién se va a hacer cargo de esto? Con la derogación de la ley no se termina esto y el daño se ha generado a todas las instituciones estatales”, sostuvo Aguilera. “Acá nadie gana, pierde todo el pueblo de la provincia de Chubut”, concluyó.

Manuel Pagliaroni, del bloque de la UCR, advirtió que desde el año 2010 comenta que “no están dadas las condiciones” para permitir la megaminería en la provincia. Además, manifestó que los legisladores “son todas personas de bien y han votado convencidos” y, en ese marco, lamentó que “no se puede entrar en una caza de brujas o marcar viviendas” como la declaración de persona no grata de varios concejos deliberantes a los diputados que se inclinaron a favor del proyecto minero.

Carlos Gómez, diputado peronista de Chubut al Frente y referente del sindicato de Petroleros Privados, informó que le pidió al gobernador Arcioni que derogue la legislación pro minera con el objetivo de garantizar la paz social, aunque ratificó su apoyo que esta actividad se desarrolle en la provincia, al compararla con los beneficios que traen aparejados las regalías petroleras. “Vamos a estar movilizados para atraer inversiones, trabajo y desarrollo sustentable”, anticipó.

En ese marco, leyó un comunicado de los presidentes comunales de Telsen, Gastre y Gangan, en el que exigieron que la Legislatura permita la habilitación de la actividad minera “para que la región salga de su ostracismo” y “brinde trabajo y dignidad a esta gente”. “Ojalá la meseta no tenga que esperar 18 años más”, completó Gómez, quien repudió además las amenazas y escraches que sufrió su familia luego de que votara a favor de la legislación.

#### El derrotero de la ley

Ayer, el gobernador Mariano Arcioni anunció la decisión de derogar la legislación prominera que provocó un estado de conmoción social, donde se produjo actos de vandalismo en varias oficinas estatales de Chubut, como la Casa de Gobierno. El mandatario anticipó también la convocatoria a un plebiscito “para escuchar a todas las voces del pueblo” en este tema, con un mensaje a través de su cuenta en la red social Twitter.

“Respeto profundamente a quienes se han manifestado pacíficamente estos días y quiero pedirles de abrir una ventana de tiempo durante la cual nos demos una oportunidad, la oportunidad de pensar cómo damos trabajo a quienes no lo tienen, cómo generamos inversiones para industrializar nuestra producción, y con qué recursos mejoramos nuestra educación, nuestra seguridad y nuestra salud”, planteó Arcioni.

Frente al revés político, Arcioni anunció que se abrirá un nuevo proceso de diálogo social con todos los actores involucrados en este proyecto, ya sea que “estén a favor o en contra”.

El rechazo a la normativa se produjo de manera transversal en la provincia patagónica desde ámbito político, sindical y representantes de la Iglesia en la región. El panorama se recrudeció a raíz de que el gobierno provincial avanzó con la represión de las protestas.

Según sus defensores, la Ley de Zonificación de la Actividad Minera en Chubut avalaba la “diversificación productiva” en los departamentos de Gastre y Telsen, una zona deprimida del interior provincial, a través de la habilitación de la explotación de los recursos sin cianuro.

La nueva zonificación autorizaba el desarrollo del denominado Proyecto Navidad, un yacimiento de plata considerado como uno de los más importantes del mundo, lo que implicaría una inversión superior a los USD 1.000 millones, la creación de miles de puestos de trabajo y de una red de proveedores locales.

La norma había quedado registrada como “XVII N° 149″ y fue promulgada a través del Decreto 1285/21 hace apenas seis días. La Cámara de Diputados aprobó por 14 votos a favor y 11 en contra, y contaba con el respaldo del gobierno nacional a través del Ministerio de Desarrollo Productivo que tiene bajo su órbita a la Secretaría de Minería y del Consejo Federal de Minería (Cofemin).

Del otro lado de la discusión, quienes se oponían a la ley aseguraban que la empresa minera Pan American Silver, propietaria del proyecto, generará distintos pasivos ambientales, entre ellos, el uso de un método distinto al cianuro pero no menos nocivo: el xantato

“La decisión del gobierno y la legislatura provincial de avanzar con la megaminería no cuenta con licencia social, a lo que se suma el cuestionamiento de diversos organismos científicos y académicos”, sostuvo un documento firmado por Abuelas y Madres de Plaza de Mayo, el Centro de Estudios Legales y Sociales (CELS), CTERA, Barrios de Pie, Jóvenes por el Clima, y personalidades como Adolfo Pérez Esquivel, Nora Cortiñas, Maristella Svampa y Rita Segato.

Compra de votos y vandalismo: las investigaciones

Debido de los destrozos en la provincia, el Procurador General del Chubut, Jorge Miquelarena, anunció la creación de un equipo especial que “se encargará de investigar todo lo relacionado con los incidentes”, con el objetivo de “identificar a los vándalos que produjeron los destrozos a los edificios públicos y también si hubo compra de voluntades” entre los diputados que aprobaron el proyecto la semana pasada.

“Son tres expedientes que se abrieron”, explicó Jorge Miquelarena en una conferencia de prensa, que se realizó en una de las pocas oficinas que quedaron en condiciones, tras el raid de incendios de edificios públicos.

Según los cálculos de la Fiscalía y la Procuración, quedaron afectadas unas 754 investigaciones debido a los incidentes e incendio en la sede judicial. El director de la Dirección de Control de Gestión de la Procuración, Daniel Corrópoli, dijo que “muchas computadoras se quemaron, otras desaparecieron y suponemos que han sido robadas”.

Miquelarena precisó que “hay causas que se perdieron enteras, otras que hay que reconstruir. “Muchas podrían caerse porque desaparecieron las pruebas o por extinción de los plazos procesales. Esto afecta a las víctimas, detrás de cada expediente hay personas. Fue un grupo reducido de vándalos”, reveló.

En el marco de estos incidentes, otro de los establecimientos afectados fue el edificio donde funciona el diario El Chubut, en la ciudad de Trelew, e incendiaron las instalaciones cuando todavía se encontraban trabajando en el lugar periodistas. Varios de los legisladores durante la sesión de este martes en la Legislatura de Chubut repudiaron el hecho.