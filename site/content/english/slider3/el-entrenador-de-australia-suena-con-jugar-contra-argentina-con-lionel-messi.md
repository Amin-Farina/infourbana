+++
author = ""
date = 2021-04-23T03:00:00Z
description = ""
image = "/images/graham-arnold-dt-australia-quiere-enfrentar-argentina-messi-los-juegos-olimpicos-1.jpg"
image_webp = "/images/graham-arnold-dt-australia-quiere-enfrentar-argentina-messi-los-juegos-olimpicos.jpg"
title = "EL ENTRENADOR DE AUSTRALIA SUEÑA CON JUGAR CONTRA ARGENTINA CON LIONEL MESSI"

+++

**_Graham Arnold, entrenador del seleccionado australiano de fútbol que participará de los Juegos Olímpicos de Tokio, admitió este viernes que sueña con jugar ante Argentina, con la participación de Lionel Messi, en la cita deportiva que se desarrollará entre julio y agosto próximo en la capital japonesa._**

Australia integrará el Grupo D de los Juegos junto a Argentina, contra la cual debutará el 22 de julio venidero, Egipto y España.

"Espero que Argentina lleve a Tokio a Lionel Messi; sería un sueño debutar contra él", señaló Graham al diario The Sydney Moring Herald, aunque la posibilidades de que el astro del Barcelona juegue en Tokio son nulas ya que intervendrá en la Copa América 2020 a celebrase en Argentina y Colombia.

"Para mí no es el 'grupo de la Muerte' es el 'grupo de los sueños', Esto es lo que se sueña en el fútbol mundial y en este tipo de torneos, los jugadores lo que quieren. Se trata de nosotros y de hacer una buena preparación, de elegir a los mejores jugadores disponibles y que lleguen con gran energía y gran ilusión por ser olímpicos", afirmó el entrenador de los australianos.

Australia debuta contra Argentina, ganadora del oro en 2004 y 2008, luego enfrentará a España, campeón de Europa sub-21, y finalmente a Egipto, que van a incluir a la estrella del Liverpool Mohamed Salah como jugador mayor de edad en su plantilla sub-23.

El duelo entre Australia y Argentina será el próximo 22 de julio en Sapporo, aunque saben que no hay chances de que suceda, los australianos añoran que ese día Messi pueda encontrarse en el terreno de juego.