+++
author = ""
date = 2021-09-17T03:00:00Z
description = ""
image = "/images/cambio-climatico-640x360.jpg"
image_webp = "/images/cambio-climatico-640x360.jpg"
title = "CRISIS CLIMÁTICA: LA ONU ADVIERTE QUE VAMOS EN RUMBO A UNA CATÁSTROFE AMBIENTAL"

+++

##### **_"Si bien existe una clara tendencia a la reducción de las emisiones de gases de efecto invernadero (GEI) a lo largo del tiempo, los países deben redoblar urgentemente sus esfuerzos", planteó el informe del organismo de la ONU sobre cambio climático._**

El mundo sigue un rumbo "catastrófico", que provocará un aumento de la temperatura media de 2.7 °C de aquí a finales de siglo, alertó este viernes el secretario general de la ONU, Antonio Guterres.

La comunidad internacional se comprometió a luchar contra las emisiones de gases de efecto invernadero, pero esas promesas "van en la dirección equivocada", explicó un informe de la ONU.En consecuencia, "el mundo sigue un camino catastrófico", alertó Guterres.

"Si bien existe una clara tendencia a la reducción de las emisiones de gases de efecto invernadero (GEI) a lo largo del tiempo, los países deben redoblar urgentemente sus esfuerzos", pidió el informe del organismo de la ONU sobre cambio climático.

El documento es una evaluación de los compromisos de los 191 países que suscribieron el Acuerdo de París de 2015 de lucha contra el cambio climático.

De todos los países, solamente 113, que representan el 49% de las emisiones de GEI, han actualizado sus compromisos nacionales al 30 de julio, como estipulan los plazos acordados.

El informe prevé que las emisiones de ese grupo, en el que están incluidos Estados Unidos y la Unión Europa, "disminuyan un 12% en 2030 en comparación con 2010". Eso sería "una luz de esperanza", según la responsable del programa de la ONU, Patricia Espinosa.

Pero las aportaciones de todos los 191 países en su conjunto "implican un aumento considerable de las emisiones globales de GEI en 2030 en comparación con 2010, de alrededor del 16%", añade.

China o Rusia no han actualizado sus compromisos de reducción de emisiones, critica una ONG que analizó el informe de la ONU, Climate Action Tracker.

Según el grupo de expertos del clima (IPCC) que cita la ONU, ese aumento de emisiones, "a menos que se tomen medidas inmediatas, puede provocar un incremento de la temperatura de unos 2.7 °C a finales de siglo".

"Esto significa romper con la promesa hecha hace seis años, de buscar un objetivo de +1.5 °C", recordó Guterres. "El fracaso a la hora de cumplir con ese objetivo resultará en la pérdida masiva de vidas", añadió.

Los países ricos se comprometieron en 2009 a que en 2020 entregarían anualmente 100.000 millones de dólares a los países en vías de desarrollo para adaptarse al impacto del cambio climático, y a reducir sus emisiones de gases de efecto invernadero.

Entre 2018 y 2019 la "financiación climática" de los países desarrollados, para ayudar a los más desfavorecidos, aumentó apenas un 2%, hasta totalizar 79.600 millones de dólares, informó este viernes el club de países industrializados (OCDE).

Por regiones, Asia se llevó el 43% de la financiación, seguida de África, con el 26%, mientras que América Latina y el Caribe se situó en tercer lugar, con el 17%.

"Necesitamos que todas las naciones arrimen el hombro", pidió en rueda de prensa Espinosa, secretaria ejecutiva de la Convención Marco de la ONU sobre el Cambio Climático (CMUNCC).Y los países en vías de desarrollo necesitan ayuda, y urgente, insistió Espinosa.