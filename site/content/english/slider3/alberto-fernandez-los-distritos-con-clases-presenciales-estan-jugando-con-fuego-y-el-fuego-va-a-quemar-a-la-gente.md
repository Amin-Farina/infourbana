+++
author = ""
date = 2021-05-31T03:00:00Z
description = ""
image = "/images/alberto-fernandez-anuncio-restricciones-covid-14-04-2021-1.jpg"
image_webp = "/images/alberto-fernandez-anuncio-restricciones-covid-14-04-2021.jpg"
title = "ALBERTO FERNÁNDEZ: \"LOS DISTRITOS CON CLASES PRESENCIALES ESTÁN JUGANDO CON FUEGO Y EL FUEGO VA A QUEMAR A LA GENTE\""

+++

El presidente Alberto Fernández criticó a la Ciudad de Buenos Aires y a las provincias que decidieron habilitar este lunes las clases presenciales pese a que el decreto nacional vigente lo prohíbe expresamente. “Todo eso es jugar con fuego y lo que lamento es que el fuego va a quemar a la gente, a los argentinos y a las argentinas de esos lugares”, aseguró el mandatario en una entrevista concedida a AM 990.

Además de la Capital Federal, Mendoza y Córdoba decidieron que los chicos regresen a las aulas pese a que son distritos que se encuentran en “alerta epidemiológico”, categoría para la cual el gobierno nacional suspendió el dictado de clases presenciales.

“En Alemania hay una estimación que se hace para medir el riesgo de la pandemia, a la que llamamos tasa de incidencia. La tasa de incidencia mide cuántas personas se contagian cada 100 mil habitantes. Cuando llega a 150, se suspenden las clases presenciales. En todos esos lugares (que hoy retomaron las clases sin aval presidencial), estamos hablando de tasas de incidencia de 600 a 700, cuatro veces lo que marca Alemania”, explicó el jefe de Estado.

Y continuó: “Yo digo esto para que la gente entienda por qué recomendamos lo que recomendamos, no es un capricho que se nos ocurrió para jorobarle la vida a la gente, está comprobado que la educación moviliza alrededor del 25% de la cantidad de personas que circulan y el gobierno de Buenos Aires también lo sabe, por eso no permite el regreso de los alumnos más grandes”.

El Presidente fue consultado dos veces por las declaraciones de Mauricio Macri en la mesa de Juana Viale. Su antecesor reconoció que en el peor momento de la crisis económica, a las 7 de la tarde se iba a la Quinta de Olivos y se ponía a ver series en Netflix.

“Tengo opinión sobre Mauricio Macri desde mucho antes, no me hizo falta ver el programa; pero no quisiera opinar, porque algunas cosas inclusive me las dijo a mí como recomendación al día siguiente de ganar las elecciones, cuando me planteó que cada dos semanas me tome unos días de descanso, o que no trabaje más allá de las seis o siete de la tarde porque era muy agotador... y yo lo miraba con cara de que estábamos en un problema, porque no debería haber sido el primer tema a tocar... pero así es Macri: lo que vimos es algo genuino”, respondió.

Sobre el final del reportaje, el periodista Mariano Hamilton hizo un chiste sobre el tema y Fernández contestó: “Cuando uno es Presidente, no tiene horarios. Es la 1 de la mañana y estoy intercambiando mensajes con China por las vacunas. Mi día comienza a las 7 de la mañana y no sé cuándo termina, pero no me quejo”.