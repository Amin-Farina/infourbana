+++
author = ""
date = 2021-06-13T03:00:00Z
description = ""
image = "/images/corte-suprema-argentina-telam-1.jpg"
image_webp = "/images/corte-suprema-argentina-telam.jpg"
title = "LA CORTE CREÓ UN REGISTRO PARA EL INGRESO DE PERSONAS TRANS"

+++

La Suprema Corte de Justicia bonaerense creó este lunes el "Registro de aspirantes a ingresar al Poder Judicial de la Provincia de Buenos Aires, en el marco de la Ley Nº 14783", en el que se podrán inscribir las personas travestis, transexuales y transgénero con interés de incorporarse y que reúnan las condiciones de idoneidad requeridas para el cargo.

En los fundamentos de su decisión citó la Ley Nº 14783 y su decreto reglamentario, tratados internacionales de Derechos Humanos y los Principios de Yogyakarta sobre "Aplicación de la Legislación Internacional de Derechos Humanos en Relación con la Orientación Sexual y la Identidad de Género".

Para su implementación, el pronunciamiento establece que la Subsecretaría de Tecnología Informática desarrollará el sistema informático para incorporar el formulario electrónico de inscripción en el sitio web "scba.gov.ar".

La Secretaria de Personal realizará un relevamiento que permita determinar el porcentaje de personas pertenecientes a este colectivo que se desempeña actualmente en el Poder Judicial.