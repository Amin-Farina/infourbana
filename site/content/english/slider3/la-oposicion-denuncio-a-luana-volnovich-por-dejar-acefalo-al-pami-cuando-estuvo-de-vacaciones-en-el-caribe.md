+++
author = ""
date = 2022-01-21T03:00:00Z
description = ""
image = "/images/h5fnxqwlmzd4pg7sljnsrofw2u.jpg"
image_webp = "/images/h5fnxqwlmzd4pg7sljnsrofw2u.jpg"
title = "LA OPOSICIÓN DENUNCIÓ A LUANA VOLNOVICH POR DEJAR ACÉFALO AL PAMI CUANDO ESTUVO DE VACACIONES EN EL CARIBE"

+++

#### **_La diputada de Juntos por el Cambio Graciela Ocaña consideró que la titular y el número dos del organismo incumplieron con sus deberes como funcionarios públicos_**

A menos de una semana de haber regresado a la Argentina tras pasar unos días de descanso en el caribe mexicano, Luana Volnovich y Martín Rodríguez, directora y subdirector del PAMI, respectivamente, fueron denunciados por haber dejado prácticamente acéfalo al organismo.

La diputada nacional de Juntos por el Cambio Graciela Ocaña, que estuvo al frente de esa entidad entre los años 2004 y 2007, realizó la presentación judicial por considerar que los funcionarios incurrieron en varios delitos al haberse ido de vacaciones al mismo tiempo.

Puntualmente, la acusación es por presunto incumplimiento de los deberes de funcionario público, abuso de autoridad y malversación de caudales públicos, entre otros cargo, y la causa recayó en el juzgado federal que está a cargo de Luis Rodríguez.

La legisladora de la oposición se basó en el artículo 3 del DNU 2/2004, el cual sentó las bases de funcionamiento de la obra social de jubilados y pensionados, que establece que el Subdirector Ejecutivo, que en este caso es Rodríguez, tiene como principal tarea reemplazar al Director Ejecutivo, Volnovich, ante su ausencia o impedimento.

> “La irresponsabilidad e inhumanidad de los funcionarios y sus vacaciones dejó a la deriva a millones de jubilados que necesitan una respuesta. La acefalía de una obra social como el PAMI, que brinda servicios de salud a millones de jubilados, repercute seriamente en el modo y el tiempo y forma en que los beneficiarios reciben las prestaciones, que en algunos casos revisten cuestiones de vida o muerte”, señaló Ocaña en su cuenta de Twitter.

El decreto al que hizo mención la diputada de Juntos por el Cambio indica que el Instituto Nacional de Servicios Sociales para Jubilados y Pensionados, nombre oficial del organismo, será manejado por el Director Ejecutivo, que tendrá las facultades de gobierno y administración, y “será asistido por el Subdirector Ejecutivo”, el que “lo reemplazará en caso de ausencia o impedimento”.

El viaje de Volnovich y Martín Rodríguez cayó muy mal en el oficialismo, incluso en La Cámpora, organización política a la que ambos pertenecen, porque calificaron como un error no solo que se hayan ido del país, sino que no avisaran.

Sucede que, antes del inicio de la temporada, cuando el Gobierno decidió prohibir la venta de pasajes al exterior en cuotas, en un intento de no agravar la escasez de dólares, el presidente Alberto Fernández le pidió a sus funcionarios que disfruten de sus vacaciones dentro del país.

Este pedido informal buscaba que los referentes del oficialismo “den el ejemplo” -sobre todo en medio de la explosión de contagios de coronavirus- y eviten generar indignación entre quienes no pudieron viajar.

> “El tema está terminado, pasemos a lo que realmente le importa a la gente; el Gobierno no puede detenerse en contestar lo mismo cuando hay una cantidad de temas más importantes, como el crecimiento económico o la posibilidad del acuerdo con el FMI“, manifestó la portavoz presidencial, Gabriela Cerruti, cuando en una reciente conferencia de prensa en la Casa Rosada le consultaron una vez más si Alberto Fernández pensaba solicitarle a Volnovich su renuncia.

Desde que estalló la polémica, los principales referentes del oficialismo evitaron hablar del asunto y se mostraron cautos al responder sobre un eventual apartamiento de la funcionaria de su cargo que, según trascendió en su momento, iba a depender de la decisión del jefe del bloque del Frente de Todos en la Cámara de Diputados y líder de La Cámpora, Máximo Kirchner, ya que en el armado de las carteras que hizo la coalición tras la victoria electoral del 2019, la dirigente fue designada al frente de la obra social de los jubilados como representante de este sector.

De hecho, la propia Cerruti había contestado la semana pasada “sin comentarios” cuando en otra rueda de prensa en Casa Rosada le preguntaron por la situación de Volnovich y de su pareja, que todavía estaban en suelo mexicano.

Finalmente, las autoridades nacionales decidieron no pedirle la renuncia a ninguno de los dos: ”El tema crónica de viajes está superado, no da para más, es un tema terminado, ¿por qué se supone que el Presidente tiene que darle instrucciones a alguien?“, aclaró este jueves la portavoz de Alberto Fernández ante los medios acreditados en Casa Rosada.

Por su parte, Ocaña ya había cuestionado duramente a la titular del PAMI en las redes sociales e incluso en el Congreso impulsó junto a sus compañeros de banca un pedido de informes sobre sus vacaciones en el Caribe, para saber quién las autorizó.

> “Mientras Luana Volnovich está en el Caribe, los mayores sufren la ola de calor. ¿Dónde están los funcionarios del PAMI que se tienen que hacer cargo de nuestros jubilados?”, cuestionó la legisladora en un mensaje que publicó días atrás en su cuenta de Twitter.