+++
author = ""
date = 2022-03-02T03:00:00Z
description = ""
image = "/images/5f595ae5e587f_920_517.jpg"
image_webp = "/images/5f595ae5e587f_920_517.jpg"
title = "HUMEDALES: VUELVEN A PRESENTAR EL PROYECTO DE LEY EN DIPUTADOS"

+++

##### **_El proyecto, cuyo dictamen perdió estado parlamentario en diciembre pasado, volverá a ser presentado por diputados nacionales y organizaciones ambientales, sociales y científicas. Se busca penalizar los incendios intencionales, como los que afectan a Corrientes y a distintas zonas del país._**

Diputados nacionales y organizaciones ambientales, sociales y científicas volverán a presentar este miércoles en la Cámara de Diputados el proyecto de Ley de Humedales que busca penalizar los incendios intencionales, como los que afectan a Corrientes y a distintas zonas del país.

El proyecto, cuyo dictamen perdió estado parlamentario en diciembre pasado, será presentado a las 17 en el marco de una convocatoria que tendrá lugar en el segundo piso del Anexo C de la Cámara de Diputados realizada por el diputado del Movimiento Evita, Leonardo Grosso, integrante del bloque del Frente de Todos, junto a diversas organizaciones y legisladores nacionales.

##### Detalles del proyecto

Se trata de un proyecto que recibió dictamen en noviembre pasado en la Comisión de Recursos Naturales y Conservación del Ambiente Humano de la Cámara de Diputados -presidida hasta diciembre por Grosso-, donde se había logrado consensuar un texto basado en una decena de iniciativas.

El dictamen propone, entre otras cuestiones, penalizar los incendios intencionales en estos territorios, regula la aplicación de sustancias contaminantes, productos químicos o residuos de cualquier origen, fumigaciones incluidas.

La iniciativa también crea un Inventario Nacional de Humedales, que consiste en un mapa que daría cuenta de dónde están los humedales en Argentina, qué características tienen y qué función cumplen en cada lugar.

En declaraciones a Télam, Grosso sostuvo que "es un proyecto que tiene un gran consenso porque fue producto de mucho debate, cientos de organizaciones, científicos y empresarios aportaron sus ideas para construir el mejor proyecto de ley de protección de humedales".

"Realizamos cuatro audiencias públicas en las cuales expusieron todos los sectores involucrados y, además, conformamos distintas mesas de trabajo entre los distintos actores interesados para que se logre un proyecto uniforme", recordó el legislador.

En ocasión del Día Mundial de los Humedales, el 2 de febrero pasado, el ministro de Ambiente y Desarrollo Sostenible, Juan Cabandié, anunció la conformación de una mesa de trabajo para elaborar un nuevo proyecto de ley, "con diálogo y con una visión federal".

La sanción de una norma que proteja y regule el uso de los humedales -que representan el 21% del territorio nacional- es reclamada además por distintas organizaciones de la sociedad civil, entre ellas Greenpeace, Amnistía Internacional, la Fundación Ambiente y Recursos Naturales (FARN), Eco House, entre otras.

En tanto, más de 700.000 personas sumaron su firma a una petición lanzada en la plataforma de Change.org por un grupo de ONGs ([http://change.org/PorLosHumedales](http://change.org/PorLosHumedales "http://change.org/PorLosHumedales")) para reclamar al Congreso Nacional la urgente aprobación de una norma: "La Ley de Humedales es necesaria para conservarlos y planificar de manera responsable una producción que mantenga su integridad ecológica, su valor intrínseco y la continuidad de estos servicios ecosistémicos claves para garantizar la justicia socioambiental y la adaptación al cambio climático", según plantearon.

A través de Twitter, Grosso anunció la presentación de hoy: "El 2 de Marzo nos encontramos en el Congreso, en el marco de la re-presentación del proyecto de Ley de Humedales, para volver a darle fuerza a este proyecto fundamental. Queremos que en 2022 sea el año en que salga la ley y para eso necesitamos el acompañamiento de todos y todas!".

Con esta norma, se busca atender y penalizar los incendios intencionales como los ocurridos en la provincia de Corrientes.

Se trata de una iniciativa que regula cuáles son las actividades que pueden llevarse a cabo dentro de los territorios de humedales, qué actividades productivas pueden realizarse y de qué modos, teniendo en cuenta que son ecosistemas vitales para la reproducción de la vida y que por eso necesitan protección especial.

La ley de humedales es una demanda que lleva más de 10 años en la Argentina, sostenida centralmente por organizaciones ambientalistas.

presentada ya en tres oportunidades pero que hasta el momento no pudo llegar a ser sancionada.

A través de sus redes sociales, Amnistía Internacional también se refirió ayer al tema: "Hoy -por ayer- inicia un nuevo período de sesiones ordinarias en el Congreso y vale recordar que, pese a la crisis climática cada vez más grave, Argentina NO tiene una ley que proteja a sus humedales. Desde 2020 se presentaron 13 proyectos pero ninguno llegó al recinto para ser votado".