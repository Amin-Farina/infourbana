+++
author = ""
date = 2021-12-29T03:00:00Z
description = ""
image = "/images/60ae2c982da89_1260_.jpg"
image_webp = "/images/60ae2c982da89_1260_.jpg"
title = "MAURICIO MACRI RECHAZÓ LA RE-REELECCIÓN DE INTENDENTES: “LO QUE SE VOTÓ AYER VA EN CONTRA DE LA TRANSPARENCIA”"

+++

#### **_El ex Presidente criticó la ley aprobada ayer en la provincia de Buenos Aires y apuntó contra los referentes de Juntos por el Cambio que apoyaron los cambios._**

El ex presidente Mauricio Macri criticó la ley aprobada en la provincia de Buenos Aires que les permitirá a 90 intendentes ir por un período más de gobierno.

“Los intendentes peronistas encontraron un agujero en la reglamentación y se sintió que había un espacio para igualar situaciones, pero eso desnaturaliza nuestro compromiso”, introdujo.

“Yo creo que en la alternancia; no creo que nadie sea imprescindible. Ya en Boca puse como límite máximo dos periodos presidenciales. Es importante la alternancia para darle transparencia, dinámica. Tenemos que fortalecer mecanismos que nos acerquen a la transparencia y lo que pasó hace algunas horas va en la dirección contraria”, reflexionó en un reportaje concedido a radio Mitre Córdoba.

Macri se sumó así a los rechazos planteados por la ex gobernadora María Eugenia Vidal y el actual presidente de la Cámara de Diputados, Sergio Massa, impulsores en 2016 de la norma que impide la reelección indefinida de mandatarios comunales.

Ayer, parte de la oposición y del oficialismo se unieron para sancionar una ley que habilita a los intendentes a poder presentarse nuevamente en 2023, pese a haber sido electos en 2015 y haber cumplido con dos mandatos consecutivos. Los diputados de Juntos por el Cambio que votaron con el Frente de Todos alegan que buscan poner en pie de igualdad a los intendentes que pidieron licencia antes del 10 de diciembre con el objetivo de volver a competir en 2023 con aquellos que no lo hicieron. “Eso es igualar para abajo”, se quejó hoy Macri.

Al realizar un balance, Macri volvió sobre los conceptos expresados recientemente en una carta pública y desarrolló: “Termina un año duro que en realidad son dos; hemos confirmado que tenemos un gobierno incapaz, mentiroso, que nos han llevado a tener años muy difíciles”. Y completó: “Lo peor de todo ha sido el manejo de la pandemia, ideologizado, lo que generó muertes innecesarias y más crisis económica”.

Durante la entrevista concedida esta mañana al periodista Jorge “Petete” Martínez, Macri volvió a gambetear las preguntas sobre su futuro político. Dijo que trabaja para consolidar la unidad y aspira a que en 2023 Juntos por el Cambio realice una gran interna con todos los dirigentes que aspiran a llegar a la Casa Rosada. No se anotó ni se bajó de esa carrera.

E insistió: “Está claro que el que se sale de Juntos por el Cambio desaparece del mapa político; ese núcleo potente de la Argentina que hoy ya supera el 41% lo haría desaparecer del mapa político”.

El ex Presidente volvió a ser muy crítico de los diputados opositores que viajaron y cuya ausencia le permitió al Frente de Todos imponer su proyecto de Bienes Personales. “No tenemos que subestimar la maldad del kirchnerismo, la habilidad de ellos para darnos vuelta un par de votos; tenemos que tener mucho cuidado a la hora de ejercer ese poder que nos ha dado la gente y por eso hicimos una reunión con una profunda autocrítica; hubo mucho de improvisación, se hizo de un día para el otro”, cerró.