+++
author = ""
date = 2022-02-04T03:00:00Z
description = ""
image = "/images/el-negocio-mundial-de-la-alimentacion-2.jpg"
image_webp = "/images/el-negocio-mundial-de-la-alimentacion-2.jpg"
title = "EN EL 2021 EL CAMPO APORTÓ CASI UNA CUARTA PARTE DE LOS IMPUESTOS QUE RECAUDÓ EL ESTADO"

+++

##### **_Según cálculos de la Bolsa de Comercio de Rosario, fueron unos 2,8 billones (millones de millones) de pesos, de los cuales poco más de $ 861.000 millones provinieron de  las retenciones a la exportación. La importancia del complejo soja, y el récord de exportaciones de maíz._**

Un trabajo de los economistas de la Bolsa de Comercio de Rosario (BCR), Emilce Terré y Javier Treboux, reflejó que en 2021 las cadenas agroindustriales aportaron en materia tributaria unos $ 2,8 billones al Estado Nacional, equivalentes al 23,5% de los recursos tributarios del Estado. Esto demuestra que el sector no solamente hace un aporte extraordinario de divisas, a través de la liquidación de exportaciones, sino también en materia impositiva.

El relevamiento se construyó a partir del análisis de los seis eslabones que conforman las cadenas agroalimentarias. Ellos son: sector primario, sector industrial (fabricación de manufacturas de origen industrial), sector comercial, transporte, sector de maquinaria agrícola y servicios conexos a la actividad. Los impuestos considerados fueron los de mayor incidencia recaudatoria: Impuesto al Valor Agregado (IVA), Derechos de Exportación (DEX), Impuestos a las Ganancias de Sociedades y Personas Físicas, Aportes y contribuciones a la seguridad social e Impuesto a los Débitos y Créditos bancarios (habitualmente llamado “impuesto al cheque”).

Aclarando que las estimaciones pueden ser revisadas con posterioridad, dado que para su cálculo se utilizan estimaciones tendenciales de variables que determinan la ponderación que se aplica a cada impuesto, el informe señaló que en 2021 las cadenas agroindustriales aportaron cerca de $ 2,8 billones, de los 11,9 billones que recaudó el organismo público en el año, según información de la AFIP. En términos relativos, entonces, la agroindustria aportó el 23,5% de los recursos tributarios totales, es decir, casi $ 1 de cada $ 4 que recaudó el Estado Nacional en tributos.

Los Derechos a la Exportación fueron el tributo de mayor impacto recaudatorio en la estructura de las Cadenas Agroindustriales en 2021, generando un saldo de $ 861.093 millones a favor del fisco, un 31% del total de las cadenas en los eslabones, teniendo en cuenta los tributos considerados. Luego se ubicó el IVA, con $ 745.416 millones, cerca del 27% de lo tributado por las cadenas, y en tercer lugar Ganancias, que aportó $ 542.107 millones, el 19% del total.

Según explicaron los economistas, el momento más importante del aporte impositivo del campo fue en el mes abril, en pleno desarrollo de la cosecha gruesa, donde en el segundo trimestre de 2021, “la actividad primaria agropecuaria llegó a representar casi el 15% del valor agregado bruto total de Argentina, marcando una estacionalidad coincidente con nuestros meses de mayor incidencia recaudatoria”.

##### El aporte de los Derechos de Exportación

La Bolsa de Comercio de Rosario, en base a las Declaraciones Juradas de Ventas al Exterior por parte del sector exportador, durante el último mes del 2021 el aporte en materia de derechos de exportación, las famosas retenciones, alcanzó los $ 79.252 millones, o unos USD 778 millones. Se trata del cuarto mayor registro en el 2021, siendo apuntalado principalmente por el incremento en las retenciones pagadas por la cadena de maíz, que anotó en diciembre pasado exportaciones por aproximadamente 9,8 millones de toneladas, que es cerca de un 20% del saldo exportable total con el que se espera cuente la campaña 2021/22.

En relación al aporte total del 2021, los derechos de exportación alcanzaron un total de $ 861.093 millones en el año bajo este concepto, un total cercano a los USD 9.104 millones. En ese sentido, el complejo soja fue el mayor aporte, ya que enfrenta la mayor alícuota de retenciones sobre sus productos. La soja aportó recursos al Estado por USD 6.721 millones, el 74% del aporte de las cadenas analizadas. Lo siguió en importancia el complejo maíz, con una tributación de USd 1.560 millones, y representando el 17% del total estimado para las principales cadenas. En tercera posición se ubicó el complejo trigo con USD 600 millones en retenciones, y una participación del 7%.

El mes de mayor recaudación individual en Derechos a la Exportación en todo 2021 fue el mes de abril, en el que por la gran cantidad de negocios anotados por el complejo soja, y producto de los fuertes precios internacionales imperantes, se recaudó un total cercano a los USD 1.141 millones. Hay que recordar que en ese momento del año, los precios internacionales de la soja en el mercado de referencia de Chicago, se ubicaron en los valores máximos en casi una década, y donde se anotaron cerca del 20% del saldo exportable de harina de soja del ciclo agrícola 2020/2021.