+++
author = ""
date = 2021-12-24T03:00:00Z
description = ""
image = "/images/mylifbs4wvcm7d7d3u7jr6frpq.jpg"
image_webp = "/images/mylifbs4wvcm7d7d3u7jr6frpq.jpg"
title = "LAS ENTIDADES SIN FINES DE LUCRO DEJARÁN DE PAGAR EL 35% DEL IMPUESTO PAIS"

+++
#### **_La Administración Federal de Ingresos Públicos indicó que estas entidades fueron incorporadas al conjunto de sujetos no alcanzados por el régimen de percepción a cuenta de los impuestos a las Ganancias y sobre los Bienes Personales por las operaciones con moneda extranjera._**

Cooperativas y asociaciones civiles, entre otras entidades sin fines de lucro dejarán de pagar el 35% del Impuesto Para una Argentina Inclusiva y Solidaria (PAIS) cuando compren divisas extranjeras, de acuerdo con información difundida por la Administración Federal de Ingresos Públicos (AFIP).

La medida expresada en la Resolución General 5123/2021 publicada este lunes en el Boletìn Oficial, dispone que "no se encontrarán alcanzadas por el régimen de percepción las entidades enunciadas en los incisos b), d), e), f), g), l) y p) del artículo 26 de la Ley de Impuesto a las Ganancias, texto ordenado en 2019".

La AFIP, a través de un comunicado, indicó que estas entidades fueron incorporadas al conjunto de sujetos no alcanzados por el régimen de percepción a cuenta de los impuestos a las Ganancias y sobre los Bienes Personales por las operaciones con moneda extranjera.

Precisó que las entidades beneficiadas son las asociaciones deportivas y de cultura física; las entidades religiosas; las instituciones internacionales sin fines de lucro con personería jurídica y con sede central en la Argentina; y las asociaciones, fundaciones y entidades civiles de asistencia social, salud pública, caridad, beneficencia, educación e instrucción, científicas, literarias, artísticas, gremiales y las de cultura física o intelectual, entre otras.