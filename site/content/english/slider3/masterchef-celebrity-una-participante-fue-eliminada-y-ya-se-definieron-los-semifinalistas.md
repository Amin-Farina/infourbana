+++
author = ""
date = 2021-06-17T03:00:00Z
description = ""
image = "/images/f800x450-29165_80611_5050-1.jpg"
image_webp = "/images/f800x450-29165_80611_5050.jpg"
title = "MASTERCHEF CELEBRITY: UNA PARTICIPANTE FUE ELIMINADA Y YA SE DEFINIERON LOS SEMIFINALISTAS"

+++

El miércoles de beneficios en Masterchef Celebrity empezó con unas cuantas sorpresas. Primero, la ambientación que sugería un viaje en el tiempo y el espacio, directo a los Estados Unidos de la década del ‘50 para vivir un rato al compás y los sabores de los inicios de rock and roll. Con dos estrellas en su delantal, Sol Pérez se disponía a seguir con su cosecha para evitar ser parte de la gala de eliminación del próximo domingo, cuando el anuncio del jurado la tomó por sorpresa.

“Les dijimos que esta semana las estrellas eran muy importantes. Esas dos estrellas te hacen subir al balcón”, le informó Damián Betular. La conductora festejó como por acto reflejo, hasta que algo empezó a hacerle ruido. ¿Qué clase de beneficio era ese en una gala de medallas? ¿Por qué privarla de seguir acumulando premios y beneficios? La respuesta no tardó en llegar. Lookeado con campera de cuero, lentes negros y jopo al tono, Donato De Santis anunció que se trataba de un miércoles de eliminación. De esta manera, la ex chica del clima se convertía en la primera semifinalista de la segunda temporada del reality gastronómico de Telefe.

La alegría de una participante significó el alerta para otros cuatro. Claudia Fontán, Georgina Barbarossa, Gaston Dalmau y Cande Vetrano debían medirse en una gala de eliminación fuera de planes pero con todas las de la ley. Cuatro delantales negros, las tensiones y la adrenalina a tope como si fuera domingo y al final de la jornada, un participante menos en la competencia.

Germán Martitegui anunció el desafío para la noche. Tenían que preparar tres hamburguesas dobles, con papas fritas o aros de cebolla. La guarnición se resolvió en un desafío de milkshakes en el que se impuso la Gunda, que eligió las papas. El resto se dirimió por orden de llegada al mercado. Gastón se abalanzó sobre la canasta de las papas, dejando para Georgina y Cande la difícil tarea de los aros de cebolla.

Georgina fue la primera en recibir las devoluciones y justamente lo más elogiado fue la guarnición. “Si hablamos de aros de cebolla, son estos. Si hablamos de una hamburguesa clásica, simple, pero a la vez impactante, es esta. Esto es Las Vegas”, elogió Martitegui. “Es espectacular”, avaló Betular. “No la vamos a olvidar nunca, es súper, súper, rica”, corroboró De Santis. Con estas palabras, la actriz, lookeada como la Princesa Leia de La Guerra de las Galaxias, parecía tener un lugar seguro en el balcón.

Luego pasó la Gunda que apostó más por el sabor que por el emplatado y pagó las consecuencias. Donato cuestionó la elección del mango en detrimento de otros ingredientes. “No encuentro el zig zag de sabores”, afirmó el italiano, mientras Martitegui criticó la cocción de las papas fritas y las fallas en la presentación. La actriz no se fue conforme con su preparación y quedó en posición de riesgo de cara al final de la jornada.

A continuación, Cande exhibió sus hamburguesas mezcla de carne vacuna y de res, con panceta y chorizo. Esta vez, el jurado tenía una mirada opuesta a la anterior. De Santis apuntó contra una presentación demasiado casera y prolija, y sus colegas coincidieron en esta apreciación. La ex Casi ángeles se retiró a su estación sabiéndose candidata a dejar el certamen.

Gastón presentó una hamburguesa con papas fritas buscando ser lo más clásico posible: “Tomate, lechuga y panceta. Para mí, eso es sagrado”, justificó. Sin embargo, al jurado no le pareció una buena idea. Betular la calificó como demasiado normal y De Santis cuestionó, además, la cocción de la carne. A su favor, se llevó los elogios al aderezo casero, una mayonesa de berenjena a lo lo que debía aferrarse si quería seguir en carrera.

Como se suponía en las devoluciones, Georgina fue la primera salvada por el jurado y Gastón sacó una pequeña ventaja con su salsa. En una definición bien cerrada, la eliminada fue Claudia Fontán, que se quedó en la puerta de las semifinales pero se llevó hermosas palabras del jurado. “Pasaste lo que pasaste, pero tu manera de cocinar siempre tuvo la intención de brillar. Te vamos a recordar con este aire de alegría”, la despidió De Santis.

“Aportaste a esta competencia platos únicos que elevaron la vara para tus compañeros. Estamos muy orgullosos del nivel de gastronomía que presentaste. Sn detalles los que te hacen quedar afuera”, apuntó Betular. “Sos una excelente mujer, trabajadora. Los picos de sabor de esta temporada son tuyos. Tuviste un error de percepción, una equivocación por los nervios. Te ganaste nuestro respeto por tu aceptación de tu error. Quiero que salgas feliz por esa puerta, es todo ganancia lo tuyo y todo pérdida lo nuestro porque te vamos a extrañar”, cerró Martitegui.

A esa altura, la Gunda hacía lo imposible para contener las lágrimas, y el conductor Santiago del Moro le dio el pie para la despedida, con palabras para cada uno de los presentes: “‘Tano’ siempre tan positivo. Disfrutás tanto la comida que dan ganas de cocinarte siempre; Damián, sos la persona más alegre del mundo y se te nota tanto cuando algo te gusta y cuando no; Germán, todos estamos cansados de decirte que sos un dulce de leche. Es un orgullo cocinar para vos y que te guste; Santi, sos el psicólogo de este programa”. Luego, agradeció a sus flamantes ex compañeros y se retiró entre aplausos. A partir de este jueves, los cuatro magníficos -Cande, Gastón, Sol, Georgina- empezarán a definir quién se lleva el premio en la segunda temporada de Masterchef Celebrity.