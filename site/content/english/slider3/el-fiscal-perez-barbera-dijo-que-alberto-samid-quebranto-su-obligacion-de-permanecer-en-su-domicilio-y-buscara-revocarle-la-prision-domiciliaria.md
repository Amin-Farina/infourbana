+++
author = ""
date = 2021-06-27T03:00:00Z
description = ""
image = "/images/alberto-samid-20210622-1193672-1.jpg"
image_webp = "/images/alberto-samid-20210622-1193672.jpg"
title = "EL FISCAL PÉREZ BARBERÁ DIJO QUE ALBERTO SAMID QUEBRANTÓ SU OBLIGACIÓN DE PERMANECER EN SU DOMICILIO Y BUSCARÁ REVOCARLE LA PRISIÓN DOMICILIARIA"

+++

**_El fiscal Gabriel Pérez Barberá pidió este lunes revocar la prisión domiciliaria del empresario Alberto Samid, luego de que el empresario violara esta condición de detención al ser escrachado en un restaurante de Ramos Mejía._**

El empresario de la carne debía cumplir prisión domiciliaria en una causa por la que fue condenado a 4 años de prisión por integrar una asociación ilícita dedicada a la evasión de impuestos. Sin embargo, la semana pasada fue echado por un grupo de comensales cuando almorzaba en un bodegón de Ramos Mejía, en el oeste del Gran Buenos Aires.

“Samid, a mi juicio, ha quebrantado injustificadamente su obligación de permanecer en su domicilio particular, dado que no cumplió las condiciones impuestas por el tribunal al concederle autorización para egresar de su domicilio por motivos de índole laboral”, señaló el fiscal Barberá.

El Tribunal Oral en lo Penal Económico N°1 le había corrido traslado a ese representante del Ministerio Público Fiscal para que emita opinión respecto de la salida realizada por el empresario condenado sin autorización del tribunal, que se conoció a través de redes sociales.

Samid tiene efectivamente habilitadas salidas laborales desde hace unos meses. El permiso fue otorgado por ese tribunal como parte del régimen previsto. El empresario va tres veces por semana a trabajar al Mercado Central desde la mañana hasta la tarde. Sin embargo el contexto en el que fue sorprendido poco tiene que ver con sus actividades en ese sitio comercial.

Tras el escándalo, Samid intentó explicar los motivos por los que había violado la domiciliaria. “Tengo salida laboral los lunes, los miércoles y los viernes, desde las 9 a las 14. En el Mercado Central, a partir de mañana, hay un paro general por tiempo indeterminado. Tengo a dos cuadras de mi casa una parrilla. Me llaman y me dicen ‘traeme todo lo que puedas’. Salí más temprano del Mercado, les traje la mercadería, estaba esperando. Le dije al mozo ‘usted traiga la mercadería porque yo no la puedo bajar’, les llevé tres cajones. Estaba esperando que los bajen”, aseguró en diálogo con Radio 10.

Samid contó que los mozos del bodegón le pidieron que aguardara un momento porque había mucho trabajo, buscando justificar el video que lo muestra sentado almorzando: “Ahí me conoce todo el mundo, vivo hace 50 años, había dos amigos que me dijeron ‘vení, Turco, sentate un cachito’”.

El tribunal tiene a su cargo el monitoreo de la ejecución de la pena que pesa sobre el empresario y su cumplimiento, por lo que por estos días evalúan si lo que hizo Samid excedió el permiso laboral. El fiscal Barberá ya expresó su posición y ahora será el tribunal el que defina si el empresario continuará en esa condición o si debe volver a prisión.