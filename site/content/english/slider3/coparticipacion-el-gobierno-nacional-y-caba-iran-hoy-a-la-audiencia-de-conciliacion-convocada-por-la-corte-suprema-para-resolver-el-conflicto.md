+++
author = ""
date = ""
description = ""
image = "/images/7e6a7026fde545197d5e630e0a6d68a7-1.jpg"
image_webp = "/images/7e6a7026fde545197d5e630e0a6d68a7.jpg"
title = "COPARTICIPACIÓN: EL GOBIERNO NACIONAL Y CABA IRÁN HOY A LA AUDIENCIA DE CONCILIACIÓN CONVOCADA POR LA CORTE SUPREMA PARA RESOLVER EL CONFLICTO"

+++

**_Representantes del Gobierno nacional y del gobierno porteño volverán a verse las caras este miércoles en la audiencia de conciliación que convocó la Corte Suprema de Justicia para resolver el conflicto por los fondos de la coparticipación federal._**

El máximo tribunal citó para hoy a las 11 a las partes para retornar el encuentro del 21 de abril pasado, entre autoridades de Nación y CABA, que pasó a cuarto intermedio. En dicha audiencia no hubo acuerdo por lo que se fijó la nueva fecha para este 12 de mayo. Cabe recordar que en aquella oportunidad estaba latente la disputa entre Alberto Fernández y Horacio Rodríguez Larreta por la educación presencial en la Ciudad.

En representación del gobierno porteño concurrieron en aquella oportunidad el procurador Gabriel Astarloa, el ministro de Hacienda Martín Mura, y el secretario de Seguridad Marcelo D’Alessandro. Y por el Estado nacional fueron tres abogados del Ministerio de Economía que suelen litigar ante la Corte: Sergio Acevedo, Ricardo Eduardo Yamone y Guillermo Anderson.

Por entonces el procurador General del Tesoro, Carlos Zannini, sorprendió con una presentación en la que sostuvo que la Corte era “incompetente” para resolver el litigio entre ambas jurisdicciones. Fue llamativa la respuesta del máximo tribunal a Zannini. La Corte rechazó in limine, es decir, sin discutir los argumentos de la Procuración del Tesoro. De esa manera, mandó un mensaje en medio de la batalla política entre ambos gobiernos.

La disputa surgió luego de la decisión del Presidente de reasignar el 1,18% del fondo de coparticipación de la Ciudad en septiembre de 2020 tras el conflicto que tuvo el Gobierno de Axel Kicillof con la policía bonaerense. A esa medida, le siguió la sanción de una ley en el Congreso nacional que podó $65 mil millones al presupuesto porteño. El oficialismo argumentó que era un excedente de recursos que el ex presidente Mauricio Macri le cedió a la administración que encabeza Horacio Rodríguez Larreta en el 2016 en materia de seguridad.

Anteriormente, la administración de la Ciudad había presentado un recurso de amparo en septiembre en respuesta a la decisión de Alberto Fernández de aplicar el ajuste vía DNU. Con la sanción de la ley, a fin del año pasado, presentó una nueva demanda. Casi siete meses después, y luego de agotado el plazo para dialogar que estaba previsto en la ley, la Corte decidió iniciar al expediente y llamó a una audiencia, una decisión que había dejado conformes tanto a Alberto Fernández como a Rodríguez Larreta. Pero tras no haber acuerdo en el primer encuentro, se fijó la nueva fecha para este miércoles.

La audiencia de este mediodía será la previa a la convocatoria de los ministros de Economía y del Interior, Martín Guzmán y Eduardo “Wado” de Pedro, a Rodríguez Larreta para acordar criterios sobre el traspaso de la Policía Federal a la Capital, que se concretará el martes 18. ”Vamos a la audiencia de mañana a informarle a la Corte sobre la reunión del 18 en el marco del espacio de diálogo abierto, pero mantenemos que la ley sancionada en el Congreso es inconstitucional”, indicaron este martes voceros de los funcionarios porteños que serán parte del encuentro.

Según la nueva iniciativa, la cifra que percibirá la Ciudad será actualizada de acuerdo a un índice compuesto en un 80% por el índice nominal del salario promedio de la Policía Federal y en un 20% por el índice de precios al consumidor (IPC) elaborado por la Dirección General de Estadística y Censos de la ciudad. Para la administración porteña, ese monto no es suficiente para que Rodríguez Larreta afronte los gastos que implica mantener el funcionamiento de la fuerza de seguridad. Sospechan que se trata de una nueva quita de fondos pero se definirá cuando se vean las caras la semana próxima. Larreta no va a retirar su cautelar y demanda contra el Estado Nacional, más allá de lo que se discuta en la reunión.