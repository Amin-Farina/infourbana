+++
author = ""
date = 2021-07-04T03:00:00Z
description = ""
image = "/images/corrientes-marihuana.jpg"
image_webp = "/images/corrientes-marihuana.jpg"
title = "INCAUTARON MÁS DE CINCO TONELADAS DE MARIHUANA EN CORRIENTES"

+++

**_Más de cinco toneladas de marihuana fueron secuestradas ocultas en un camión abandonado en la ciudad correntina de Ituzaingó, informó Prefectura Naval Argentina (PNA) este domingo._**

El operativo fue realizado por efectivos de PNA que montaron una serie de patrullajes combinados por la zona de Lago Yacyretá, a la altura del kilómetro 1.527 del río Paraná, en dicha ciudad de la provincia de Corrientes.

Según las fuentes, los efectivos hallaron un camión recientemente abandonado, que estaba cargado con gran cantidad de bultos y al inspeccionar la carga descubrieron unos 5.300 kilos de marihuana distribuidos en 6.887 "panes".

Ante esta situación, el personal decomisó la droga, que quedó a disposición del Juzgado Federal 2 de Corrientes, a cargo de Juan Carlos Vallejos.