+++
author = ""
date = 2021-06-02T03:00:00Z
description = ""
image = "/images/matanza1.jpg"
image_webp = "/images/matanza1.jpg"
title = "LA POLICIA BONAERENSE SE VIO OBLIGADA A DEVOLVER AUTOS DE ALTA GAMA A LOS NARCOS "

+++

**_El gobierno de la provincia de Buenos Aires, a cargo de Axel Kicillof, deberá devolver dos patrulleros que tuvieron ese fin cuando en 2019 la justicia secuestró los vehículos de alta gama de una red de narcotráfico._** 

Tras la finalización del juicio el Tribunal Oral Federal 1 de La Plata concluyó que los autos no formaron parte del delito, por lo que deben ser restituidos.

La historia comienza en 2017 cuando Jaime Rosado y Hugo Alberto Rivas fueron detenidos en varios allanamientos en la provincia de Buenos Aires en los que se secuestraron más de 40 kilos de cocaína. La particularidad que tenía la droga era que en cada pan tenían la marca de una Cruz Esvástica, símbolo de la alemana nazi de Adolf Hitler. Además estaba confeccionada de una manera para que no ser detectada por los perros policías ni por los scaners que se usan en las fronteras.

La investigación judicial concluyó que los acusados cobraron parte de la venta de droga con autos de alta gama. Dos de ellos fueron secuestrados, un Mercedes Benz C200 Kompresor y un Peugeot RCZ, y decomisados. Los dos vehículos fueron entregados a la Policía de la provincia de Buenos Aires que los convirtió en patrulleros, lo que fue presentado en un acto que encabezó el entonces ministro de Seguridad bonaerense, Cristian Ritondo.

La causa siguió, llegó a juicio oral y la semana pasada los dos imputados fueron condenados por tenencia de drogas con fines de comercialización. Recibieron penas de cinco años y seis meses y de cinco años de prisión. Pero los jueces del Tribunal Oral Federal 1, Nelson Jarazzo, Nicolas Toselli y Ricardo Basílico, no avalaron la solicitud de decomiso definitivo de los dos autos de alta gama que pidió durante su alegato el fiscal federal Hernán Schapiro.

Los magistrados, según los fundamentos de su decisión, explicaron que “los automóviles no fueron instrumento de la tenencia” de la droga que comercializaban. También descartaron tras las audiencias del juicio oral que “que los bienes cuyo comiso se requiere hayan sido el producto del comportamiento acreditado”. Así concluyeron que los vehículos no fueron obtenidos por la venta de droga y por lo tanto ordenaron que sean reintegrados a los acusados. La decisión del Tribunal Oral Federal será apelada tanto por la Fiscalía, para insistir en el decomiso de los dos autos de alta gama, como por la defensa de los imputados para que se revoque la condena y sean absueltos. Esas decisiones pasarán ahora a la Cámara Federal de Casación Penal.

El Peugeot RCZ ya no existía. Fue destruido después de que fue chocado en la autopista La Plata-Buenos Aires en noviembre de 2018 mientras estaba en un retén policial. Dos autos que circulaban por la autopista chocaron y embistieron al patrullero. Tres personas, los dos conductores y un policía, resultaron heridos.

La ley argentina permite el decomiso anticipado de bienes que fueron obtenidos con fondos provenientes de delitos. “El juez podrá adoptar desde el inicio de las actuaciones judiciales las medidas cautelares suficientes para asegurar la custodia, administración, conservación, ejecución y disposición del o de los bienes que sean instrumentos, producto, provecho o efectos relacionados con los delitos previstos en los artículos precedentes”, establece el artículo 305 del Código Penal de la Nación. Agrega que “en operaciones de lavado de activos, serán decomisados de modo definitivo, sin necesidad de condena penal, cuando se hubiere podido comprobar la ilicitud de su origen” y el destino de “los activos que fueren decomisados serán destinados a reparar el daño causado a la sociedad, a las víctimas en particular o al Estado. Sólo para cumplir con esas finalidades podrá darse a los bienes un destino específico.

La norma también les permite a los acusados a quiénes les incautan los bienes iniciar una demanda judicial. “Todo reclamo o litigio sobre el origen, naturaleza o propiedad de los bienes se realizará a través de una acción administrativa o civil de restitución. Cuando el bien hubiere sido subastado sólo se podrá reclamar su valor monetario”, explica el texto.