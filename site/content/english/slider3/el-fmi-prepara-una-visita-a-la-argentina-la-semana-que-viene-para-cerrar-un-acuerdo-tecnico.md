+++
author = ""
date = 2021-12-01T03:00:00Z
description = ""
image = "/images/fmi-argentina-1.jpg"
image_webp = "/images/fmi-argentina.jpg"
title = "EL FMI PREPARA UNA VISITA A LA ARGENTINA LA SEMANA QUE VIENE PARA CERRAR UN ACUERDO TÉCNICO"

+++

#### **_Según la Casa Rosada, el Fondo ultima los detalles de la próxima misión. Buscan definir el acta de compromiso y la carta de entendimiento._**

El Fondo Monetario Internacional (FMI) confirmó al Gobierno que la semana próxima desembarcará una nueva misión para cerrar un acuerdo técnico con la Casa Rosada, informaron fuentes oficiales.

El Gobierno y el staff técnico del FMI busca acelerar un entendimiento antes de fin de año. La visita al país de la misión apunta a terminar de definir los últimos detalles hacia una carta de intención del acuerdo de reestructuración de la deuda.

La misión llegaría el lunes 6 de diciembre y estaría encabezado por Julie Kozak y Luis Cubeddu, los dos negociadores del FMI para la Argentina.

##### Los objetivos del FMI en su nueva misión a la Argentina

Según informaron desde la Casa Rosada, las autoridades del Fondo, que comanda Kristalina Georgieva, buscará con la visita cerrar los aspectos técnicos de:

Acta de compromiso: donde están contenidos los ajustes de política económica que el Gobierno establece hará para superar los problemas que le llevaron a solicitar asistencia financiera y que sirven también sirven para garantizar que el país será capaz de reembolsar los recursos al FMI.

Carta de intención: donde queda plasmada una explicación más detallada en un “memorando de entendimiento” entre el FMI y el país y que debe ser aprobada por el directorio ejecutivo del organismo.

##### La negociación del Gobierno con el FMI por la deuda de US$44.000 millones

La negociación para un nuevo acuerdo entró en tiempo de descuento. Se produce en momentos en que las reservas internacionales del Banco Central se ubican por debajo de los vencimientos que el país tiene que afrontar hasta marzo, según estiman distintos economistas.

La negociación apunta a un plan de facilidades extendidas, a 10 años con algo más de 4 de gracia, con lo que el país comenzaría a pagar hacia 2026. Aunque está pendiente si habrá o no reducción de la sobretasa.