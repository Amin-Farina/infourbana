+++
author = ""
date = 2021-10-12T03:00:00Z
description = ""
image = "/images/consecha-de-soja-1.jpg"
image_webp = "/images/consecha-de-soja.jpg"
title = "ENOJO DE LOS PRODUCTORES AGROPECUARIOS CON EL GOBIERNO POR LAS NUEVAS TRABAS A LAS EXPORTACIONES DE MAÍZ"

+++

##### **_El ministerio de Agricultura modificó la tramitación de las ventas al exterior del cereal aunque descartó que haya un cierre de las exportaciones, como ocurrió con la carne. Para el presidente de la Sociedad Rural, es “una pésima señal”_**

Si bien desde el ministerio de Agricultura señalaron que las exportaciones de maíz se encuentran abiertas en relación al actual ciclo comercial 2020/21 y al completarse días atrás el saldo exportable asignado para esa campaña de 38,5 millones de toneladas, se conoció la resolución que lleva la firma del Subsecretario de Mercados Agropecuarios, Javier Preciado Patiño, mediante la cual se determinó que a partir de hoy solamente se aceptará el registro de exportaciones del cereal de la campaña 2020/21 en el marco del régimen Declaraciones juradas de Ventas al Exterior por operaciones respaldadas por mercadería física ya adquirida. Solamente quedarán habilitadas las exportaciones a corto plazo (30 días) confirmadas. El objetivo del Gobierno, a través del pedido de más documentación respaldatoria a los exportadores, es asegurar el abastecimiento interno.

La comunicación que fue enviada a los exportadores por parte de la Subsecretaría, sostiene lo siguiente: “A partir del día de la fecha, sólo se aceptarán para su registro de Declaraciones Juradas de Venta al Exterior dentro del régimen especial denominado DJVE-30 establecido por el Artículo 13 de la citada Resolución N° 128/19, siempre que las mismas correspondan con operaciones debidamente respaldadas con sus compras físicas y con barcos nominados con fecha estimada de arribo”.

Ante esta decisión, el presidente de la Sociedad Rural Argentina, Nicolás Pino, comentó desde su cuenta de Twitter: “Seguir interviniendo y burocratizando los mercados es una pésima señal que trae desconfianza en los productores. Ya lo vivimos desde abril con la carne y ahora con el maíz. Nuestro país necesita que todos podamos trabajar libremente para producir sabiendo que vamos a poder vender”.

Días atrás el titular de la cartera agropecuaria, Julián Domínguez, durante la presentación del proyecto del Gobierno de desarrollo agroindustrial había definido al maíz, el trigo, y la carne como bienes culturales, a los cuales había que controlar sus saldos exportables. Por su parte, el secretario de Agricultura, Jorge Solmi, confirmó que se está monitoreando el mercado y no descartó tomar medidas se ameritaba la situación para garantizar el abastecimiento del mercado interno.

“Queremos hacer el esfuerzo de que todo lo que se pueda exportar se exporte, pero al mismo tiempo asegurar que quede disponible el maíz que se necesita para abastecer el consumo interno. No hay ni limitaciones ni nada para comercializar al mercado externo, el mercado exportador está abierto y hay prioridad para el otorgamiento de declaraciones juradas de ventas al exterior para aquellos que ya tienen un negocio cerrado”, dijeron fuentes del ministerio de Agricultura.

Hay que recordar que hace 7 días atrás se completó el cupo tácito de exportación de maíz, a través de la declaración de más de 690.000 toneladas de embarques del cereal por parte de Cofco, Bunge y Cargill, entre otras compañías. El actual ciclo comercial vence el próximo 28 de febrero, y por ese motivo los exportadores no podrán adquirir más cereal para embarcar antes de esa fecha.

A todo esto, los controles a la exportación de maíz se registran en momento de conflicto entre el Gobierno y el campo por las restricciones a la comercialización de carne vacuna. Mañana se estará anunciando cómo se implementará la venta a China de carne de vaca conserva o manufactura. Una medida que fue anunciada por el ministro Domínguez hace 15 días aproximadamente y la misma hasta el momento no fue oficializada a través de la publicación en el Boletín Oficial.

#### Exportadores

Fuentes del sector exportador comentaron que “no hay un cierre del registro de declaraciones juradas de ventas al exterior, pero sí algunas restricciones”, y señalaron que es una atribución de la Subsecretaría de Mercados fijar condiciones, hasta el cierre de la campaña comercial, esas condiciones serán que una declaración jurada sea autorizada hay que demostrar que la mercadería ya está adquirida, que la misma se embarcará en los próximos 30 días y, por último, que haya un barco nominado. “En la práctica son condiciones de cumplimiento”, dijeron.

También comentaron que se habrían detectado en el ministerio de Agricultura operaciones “dudosas” de exportadores no tradicionales que no pertenecen al Centro de Exportadores de Cereales y para evitar ese tipo operaciones desde la cartera agropecuaria se tomaron decisiones que se oficializaron este lunes, mediante una comunicación a todos los exportadores.