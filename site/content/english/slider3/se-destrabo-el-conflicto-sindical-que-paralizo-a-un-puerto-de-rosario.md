+++
author = ""
date = 2021-04-08T03:00:00Z
description = ""
image = "/images/lesk5jlpapp7ifi2gg5oz7djim.jpg"
image_webp = "/images/lesk5jlpapp7ifi2gg5oz7djim.jpg"
title = "SE DESTRABÓ EL CONFLICTO SINDICAL QUE PARALIZÓ A UN PUERTO DE ROSARIO"

+++

**_Se destrabó el conflicto entre los trabajadores asociados al Sindicato de Obreros y Empleados Aceiteros (SOEA) y la ex Aceitera Buyatti que impedía el ingreso de los camiones al puerto General San Martín._** 

Los empleados habían tomado la medida de fuerza encabezada por el gremio para exigir el cumplimiento en el pago de las indemnizaciones adecuadas y la vigencia de la prepaga que fue suspendida desde marzo pasado cuando se hicieron los despidos.

El paro duró más de 24 horas y se generó frente a la ex Aceitera, por lo que impedía el ingreso de camiones a las plantas de Bunge y ADM. El gremio consiguió llegar a un acuerdo con la empresa gracias a la intervención del Ministerio de Trabajo nacional para que se efectúe el pago adecuado de las compensaciones económicas que demandaban los trabajadores.

“Se va a pagar la totalidad de la indemnización dadas las antigüedades de los empleados, con cada uno de los montos se pagaría de una sola vez a un grupo de 40 personas del total”, afirmó Pablo Reguera, secretario general de Soea. Por otro lado, se acordó que los empleados que tienen más antigüedad podrán cobrar sus indemnizaciones conforme a una escala establecida por la empresa, el sindicato y el Ministerio de Trabajo.

“Después, tienen tres cuotas aquellos que tienen más tiempo trabajado y van a estar cobrando en cinco cuotas iguales aquellos que tienen mucha más antigüedad consecutiva. Eso fue lo que se resolvió y la gente lo aceptó, por lo tanto, ya queda prácticamente cerrado el conflicto”, agregó Reguera.

El sindicato había amenazado con extender la medida de fuerza de manera indeterminada en caso de que no se solucionara el problema. Al hacerse frente a la ex Aceitera vinculada a Vicentin afectaba a las plantas vecinas.

“El viernes (por mañana) vamos a firmar un acuerdo marco entre la empresa y el sindicato y de ahí se van a sacar los montos de cada uno de los empleados donde la gente irá a firmar individualmente al Ministerio de Trabajo, en el departamento de San Lorenzo de Rosario”, sostuvo el secretario general de SOEA. Esta sería la última instancia y lo exigible para que la empresa cumpla con lo pactado.

En enero pasado, la aceitera Buyatti puso en venta la planta procesadora en la localidad santafesina de Puerto General San Martín, en el departamento de San Lorenzo y mientras aparecían posibles compradores del complejo, envió 84 telegramas de despido a empleados asociados al SOEA.

En ese entonces prometió hacerse cargo de las indemnizaciones correspondientes y de la prepaga, pero 73 de los 84 trabajadores que se desempeñaban en la planta aludieron no haber recibido el beneficio correspondiente en el último mes. El ministerio de Trabajo ya había intimado a la ex Aceitera a cumplir con lo acordado luego de dos reuniones entre las partes, aseguró Reguera.

El establecimiento situado en la localidad santafesina del Puerto General San Martín ya venía trabajando muy por debajo de su nivel habitual en los últimos años. Trascendió que en 2020 operó al 60% de su capacidad, hasta que paralizó la operación a mediados de noviembre, a la espera de una definición para evaluar la continuidad de su actividad.

#### Antecedentes

Luego de que la empresa anunció el cierre de la procesadora comenzó a ejecutar los despidos de todos los trabajadores, sin embargo, la intervención del sindicato se produjo tras la denuncia de los exempleados quienes aseguraron que la liquidación se estaba aplicando por sobre un 70% de su sueldo.

De acuerdo con los trascendidos, la aceitera había decidido solo reducir la planilla y conservar a un grupo de empleados para hacer las tareas de mantenimiento y seguridad hasta la cosecha de soja, pero finalmente decidieron centrar la producción en el norte antes de lo pensado.

La preocupación de los 73 trabajadores radicaba en que, al no tener más fábrica en San Lorenzo, la empresa quería pagar las indemnizaciones en cuotas cuya cobranza debía realizarse a más 500 kilómetros de la localidad santafesina. Por eso la protesta de las últimas semanas para encontrar una salida al conflicto.