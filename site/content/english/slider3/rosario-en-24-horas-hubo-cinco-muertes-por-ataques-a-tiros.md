+++
author = ""
date = 2021-12-03T03:00:00Z
description = ""
image = "/images/narcotrafico-rosario-20201026-1066669-1.jpg"
image_webp = "/images/narcotrafico-rosario-20201026-1066669.jpg"
title = "ROSARIO: EN 24 HORAS HUBO CINCO MUERTES POR ATAQUES A TIROS"

+++

#### **_El 67% de los homicidios registrados en Santa Fe se produjeron en el departamento de Rosario. Según datos del Observatorio de Seguridad Pública santafesino, en la provincia ya hubo 332 asesinatos, en su mayoría, ligados al narcotráfico_**

Tres personas que habían sido baleadas días atrás fallecieron entre este lunes y hoy martes en la ciudad de Rosario, que sumadas a las víctimas del doble crimen ocurrido anoche contabilizan cinco muertes violentas en las últimas 24 horas en el departamento que concentra el 67% de los homicidios de la provincia de Santa Fe, informaron fuentes oficiales.

De esta manera ya son 223 las personas asesinadas en el departamento Rosario en lo que va del año, mientras que en toda la provincia suman 332, según datos del Observatorio de Seguridad Pública santafesino.

##### Doble crimen

Un joven de 25 años y un hombre de 43 fueron asesinados en la noche del lunes a balazos por dos sicarios que, a bordo de una moto, dispararon contra la vivienda del primero, en la que presuntamente funcionaba un búnker de venta de drogas del barrio Ludueña de Rosario, informaron fuentes judiciales.

Los investigadores creen que el ataque estuvo dirigido contra Miguel Farías (25) y que, accidentalmente, también tuvo como víctima a Gerardo Miqueo (43), quien se encontraba en la vereda con una pala construyendo un cantero en su casa, agregaron los voceros.

El hecho ocurrió en Gandhi y Teniente Agneta, cuando dos hombres en moto atacaron una casa en la que, según los vecinos del lugar, funcionaba un búnker de venta de drogas comandado por un hombre apodado "El Oso".

A raíz del ataque fueron asesinados Farías y Miqueo, lo que derivó luego en una protesta de vecinos indignados por la muerte del trabajador, un albañil que de acuerdo a la investigación fue alcanzado por una bala de la ráfaga disparada contra el punto de venta de drogas.

##### Una muerte después de cinco días de agonía

Según informó el Ministerio Público de la Acusación (MPA), el lunes a la noche murió en el hospital de Emergencias de Rosario un joven identificado como Agustín Vallejos, de 26 años, que había sido baleado en la cabeza el 23 de noviembre último.

De acuerdo a la investigación, unos minutos antes de las 22 de ese día, Vallejos fue baleado por dos personas que se movían en moto en calle Granaderos a Caballo al 2600 de la ciudad de Villa Gobernador Gálvez.

El joven fue trasladado en un primer momento con una herida en el cráneo, como consecuencia del ataque armado, al hospital "Gamen" de Villa Gobernador Gálvez. Luego fue trasladado al hospital de Emergencias rosarino donde murió el lunes , en un caso que no tiene detenidos, precisaron voceros del MPA.

##### Asesinato en la madrugada

En tanto, esta madrugada, murió en el hospital Roque Sáenz Peña de Rosario un joven de 22 años, identificado como Josué Esteban Godoy, que había ingresado al centro de salud la madrugada del 28 de noviembre con un tiro en la cabeza y heridas en el brazo derecho.

Según comunicó el MPA, el joven fue atacado en la calle a la 1.50 del último domingo en Malvón al 1700 de esa ciudad por personas aún no identificadas que circulaban en un automóvil.

##### Baleado desde un auto

Por último, un hombre de 50 años llamado Ricardo Emilio Bravo, de 50 años, murió este martes tras ser atacado a tiros anoche alrededor de las 23.30 en la zona norte de Rosario.

De acuerdo a los primeros datos recogidos por los investigadores, el hombre caminaba el lunes por la noche por calle Colombres al 1300 cuando fue atacado a tiros desde un automóvil que, tras la balacera, escapó del lugar.

La víctima fue trasladada al hospital Eva Perón de la localidad de Granadero Baigorria, en el que falleció esta madrugada, señalaron voceros judiciales.

La Policía secuestró en la escena del crimen varias vainas servidas de calibre 9 milímetros y tiene las características del vehículo empleado en el crimen, aunque aún no hay detenidos, ampliaron los informantes.