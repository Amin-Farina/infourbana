+++
author = ""
date = 2022-04-11T03:00:00Z
description = ""
image = "/images/798-1.jpg"
image_webp = "/images/798.jpg"
title = "LA SUBA DE LOS PRECIOS DEL GAS TRAE PREOCUPACIONES PARA EL INVIERNO ARGENTINO"

+++
#### El precio del GNL tocó los US$100 en medio de la guerra Rusia-Ucrania y complica las cuentas argentinas

A casi dos semanas del ataque de Rusia a Ucrania, y con los precios del petróleo y el gas en máximos, el Gobierno elabora un esquema de emergencia para enfrentar lo que será un otoño e invierno más que complicado en materia energética.

En ese marco, el ministro de Economía, Martín Guzmán, se encuentra en Estados Unidos para reunirse con directivos de las petroleras más importantes, mientras otros funcionarios monitorean de manera permanente las cotizaciones externas y los plazos de licitaciones para materiales y obras del gasoducto a Vaca Muerta.

La preocupación es doble: las importaciones de gas requerirán hasta el triple de dólares que los proyectados y sin esas compras puede haber cortes para las industrias, justo en un momento en el que el Gobierno requerirá de mayor producción para cumplir con las metas con el FMI.

La Argentina, aun con Vaca Muerta -el segundo reservorio mundial de gas no convencional- produciendo cada vez más, necesita importar petróleo (para las naftas y gasoil de mayor refinación) y sobre todo de Gas Natural Licuado (GNL), a precio internacional.

Ese gas líquido luego se transforma en fluido para inyectar al sistema nacional y abastece el 30% de la demanda que tienen hogares, comercios e industrias en las épocas más frías del año.

En 2021, la Argentina destinó US$1000 millones para comprar el gas líquido (GNL) que luego regasifica en los barcos instalados en Bahía Blanca y Escobar. Llegaron unos 58 buques entre mayo y agosto pasados para satisfacer la demanda en medio de la incipiente reactivación que se daba tras la segunda ola de la pandemia de coronavirus.

Para este año, con la “nueva normalidad”, preveía comprar al menos 70 barcos para cubrir la demanda de invierno. Eso significa, incluso antes del acuerdo con el Fondo Monetario Internacional (FMI), un inconveniente grande para las reservas estresadas del Banco Central.

La situación se agudizó con el precio internacional en alza y hasta el momento Integración Energética Argentina (IEASA), la estatal exEnarsa encargada de las licitaciones de GNL, no lanzó las compras masivas para este año.

En enero pasado, a modo de testeo, se licitó un barco con un precio de US$27 por MMBTU, un valor que desde ese momento no paro de escalar.

De acuerdo con un cálculo elaborado por la consultora Economía & Energía, en 2021, la Argentina importó GNL a US$8,4 por MMBTU (la manera en la que se “mide” el gas).

Si el precio de importación durante 2022 se eleva hasta los 20USD/MMBTU por el mismo volumen comprado el año pasado, el costo se incrementaría en US$1520 millones.

Si se mantienen los precios actuales del GNL (en torno a US$40 USD, aunque tuvo picos de US$100 este lunes), el costo de importación respecto de 2021 se elevaría hasta los US$5411 millones, asegura la firma. Todo sucede en un contexto en el que dólares no sobran.

Este es el escenario con el que Martín Guzmán, ministro de Economía, se encontraba desde este miércoles en el evento mundial de energía CERA Week, en Houston.

Allí se reunirá con ejecutivos de empresas energéticas como Total, Chevron, Shell y Equinor, entre otras, y funcionarios de la Secretaría de Energía estadounidenses. Los ojos estarán puestos en esta cumbre en medio de la crisis energética global que desató el conflicto entre Rusia y Ucrania.

Ante el Congreso, Martín Guzmán se cruzó el lunes por diputados opositores, que le reclamaban precisiones sobre cómo iba a llegar a una baja en los subsidios energéticos del 0,6% del PBI y cuál iba a ser el impacto real en las tarifas de luz y gas de los usuarios finales.

“Estamos en una situación global muy diferente de aquella en que se estaban realizando estos planes. Esa incertidumbre hay que tenerla en cuenta. El precio del GNL está ahora un valor mucho mayor que cuando estábamos negociando esto”, subrayó el ministro.

Este miércoles, en una entrevista radial, el presidente del Banco Central (BCRA), Miguel Pesce puntualizó: “hay una altísima volatilidad en los precios según el continente donde se mida el GNL. No podemos saber todavía el precio al que nos van a vender el gas para el invierno”.

Añadió que los últimos cálculos de la autoridad monetaria permiten estimar que la suba de precios del trigo, el maíz, la soja y sus derivados compensará el gasto que debe hacerse para importar petróleo y derivados durante este año.

“Quedamos empatados entre los efectos de los precios de los combustibles y el aumento de los precios de los granos”, planteó el jefe del BCRA.