+++
author = ""
date = 2021-05-07T03:00:00Z
description = ""
image = "/images/maria-estela-regidor-1.jpg"
image_webp = "/images/maria-estela-regidor-1.jpg"
title = "UNA DIPUTADA DE CORRIENTES FUE ACUSADA DE QUEDARSE CON EL 50% DE LOS SUELDOS DE SUS ASESORES"

+++

**_La diputada nacional de la Unión Cívica Radical, la correntina Estela Regidor, quedó envuelta en un escándalo por una serie de audios que se viralizaron en las últimas horas en los que se la escucha a la legisladora organizando un “sistema de reintegros” con los sueldos de sus asesores._**

En los audios que circulan por las redes sociales, se puede escuchar a la diputada decir: “... el que no está de acuerdo me dice porque hoy es día 25 y podemos rescindir perfectamente el contrato. Van a cobrar 40 mil pesos cada uno de ustedes; yo sé que vos me dirás que tu familia, y yo también tengo familia, también he dejado de ganar, he dejado de ahorrar”.

Y remata: “Yo sé que otros diputados los cupos se los quedan, no hacen las donaciones que yo hago, no contratan en negro como yo contrato, tengo gente a la que le doy ayuda económica en negro. Van a cobrar cada uno 40 mil pesos, el resto es reintegro”.

Los audios dan cuenta de un planificado sistema de recaudación que la diputada radical llevaría a cabo con sus asesores, quienes en persona la habrían grabado, aunque por el momento no hay denuncia en la Justicia:

“Vos tenés que tener sobre de cada uno; entonces cuando ellos te dan, vos tenés que hacer el recuento, ponés el nombre y cerrás. Y yo te voy a decir cada mes a quién le vas a llevar, a quién le vas a entregar. Vos te encargás de ellos y yo me encargo de lo que es mío. Lo mío es mío. En un papelito así, vos me decís “Damián Arroyo” en mano cuánto tenés que cobrar en mano tu recibo. Tu recibo en mano dice 80, entonces 40 te quedan, 40 ponés en la otra”, se explica paso a paso en el audio.