+++
author = ""
date = 2022-02-08T03:00:00Z
description = ""
image = "/images/sube3-1.jpg"
image_webp = "/images/sube3.jpg"
title = "BOLETO DE COLECTIVO: HAY DIFERENCIAS DE HASTA CUATRO VECES EN EL VALOR ENTRE CABA Y EL INTERIOR"

+++

##### **_En medio del debate por el traspaso del transporte de jurisdicción nacional a la Ciudad, resurge la desigualdad en el pago de pasajes entre el Área Metropolitana de Buenos Aires y las provincias_**

Un nuevo centro de conflicto se desató entre la Ciudad y la Nación. El Gobierno Nacional dejó trascender ayer que, para recortar subsidios, intentará traspasar los 32 colectivos cuyo recorrido empieza y termina dentro de los límites porteños al gobierno de Horacio Rodríguez Larreta. Mientras la decisión recién comienza a debatirse, se reavivó la discusión sobre las asimetrías entre el Área Metropolitana de Buenos Aires (AMBA) y el interior.

En el AMBA, la región compuesta por la Ciudad de Buenos Aires y 40 municipios de la provincia de Buenos Aires, el boleto mínimo cuesta $18. Los colectivos que la transitan son de jurisdicción nacional. En otras ciudades del interior, en cambio, hay diferencias de hasta cuatro veces y la potestad es de las provincias.

Los casos más resonantes son los de Córdoba, Rosario y Santa Fe: aún en algunas de las ciudades más pobladas de la Argentina, el boleto cuesta poco más del triple que en la Capital y sus alrededores ($59,35, según la última actualización).

El boleto más cercano al del AMBA es el de San Juan, de $29,60. En la otra punta, el de Bahía Blanca, de $76. Lo que sucede en muchos lugares del interior es que hay una tarifa única sin importar cuánta distancia recorra el colectivo en cuestión.

##### **¿Por qué el boleto de colectivo en el AMBA es más barato?**

La principal razón detrás de las asimetrías en los boletos es, claro, los subsidios. Según datos públicos disponibles hasta el momento, la Nación destinó a las provincias en 2021 alrededor de $28.000 millones por el Fondo Compensador, creado en 2019. El Gobierno canaliza los subsidios a través de ese fondo, y a su vez las provincias ponen “su parte”, es decir, otro tanto para poder subsidiar los pasajes.

En el AMBA, en cambio, fueron alrededor de $96.000 millones. El economista especializado en transporte Rafael Skiadaressis explica que también hay una diferencia en la cantidad de colectivos -unos 18.000 en el AMBA y casi 13.000 en todo el interior-, pero de todos modos los subsidios al Área Metropolitana son visiblemente mayores.

“La brecha tan importante que se ve entre AMBA y las provincias es por los subsidios, especialmente desde 2006 cuando se crea una caja de subsidios especial para AMBA. En 2007 se creó un régimen de compensaciones provincial, pero de menor tamaño”, explica Skiadaressis.

Suponiendo que los subsidios fueran cero para todo el país, los costos fijos son casi homogéneos (con algunas diferencias en el régimen laboral de las provincias, señala el economista), pero los variables sí cambian, porque dependen de los pasajes por kilómetro.

##### **La pelea detrás de los subsidios para los colectivos**

Esta mañana, el Gobierno Nacional dejó trascender que analiza traspasar la administración de los 32 colectivos que empiezan y terminan en la Capital Federal al Gobierno de la Ciudad.

La decisión implicaría un recorte de unos $14.000 millones en subsidios, que la Ciudad de Buenos Aires debería cubrir o bien aumentando el valor del pasaje o reasignando partidas presupuestarias.

Por la tarde, el secretario de Transporte de la Nación, Diego Giuliano, dio más detalles sobre la decisión en estudio. “La Ciudad de Buenos Aires debe recuperar su autonomía plena en relación a la regulación y control de su propio transporte, definiendo modalidades, recorridos y tarifas de las líneas de TUP que son exclusivas de su territorio”, escribió en un tweet el funcionario.

“Esta no es una posición unilateral. Con @alexisguerrera \[el ministro de Transporte nacional\] siempre apostamos al diálogo de reflexión federal, como el ya planteado por varios gobernadores y los Intendentes de Córdoba, Rosario y Santa Fe”, añadió.

Ayer por la tarde, desde el Gobierno de la Ciudad afirmaban que nadie los había convocado al diálogo y que la decisión los tomó por sorpresa. Reclamaban un tratamiento justo de los subsidios. “¿Qué pasará con los colectivos que empiezan y terminan en la provincia de Buenos Aires?”, se preguntaban.

Los colectivos podrían ser el nuevo gran tema de discusión de los próximos días, sobre todo en el marco de las negociaciones con el Fondo Monetario Internacional, que pidió ajustar la cantidad de subsidios para llegar a un acuerdo.