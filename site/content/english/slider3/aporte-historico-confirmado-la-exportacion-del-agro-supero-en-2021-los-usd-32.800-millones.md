+++
author = ""
date = 2022-01-03T03:00:00Z
description = ""
image = "/images/granos-1.jpg"
image_webp = "/images/granos.jpg"
title = "APORTE HISTÓRICO CONFIRMADO: LA EXPORTACIÓN DEL AGRO SUPERÓ EN 2021 LOS USD 32.800 MILLONES"

+++

#### **_En un contexto de precios internacionales favorables, las exportaciones agropindustriales aportaron durante el año pasado un ingreso de divisas por 32.807.933.377 dólares, récord absoluto desde comienzos de este siglo._**

Así lo informó la Cámara de la Industria Aceitera de la República Argentina (CIARA) y el Centro de Exportadores de Cereales (CEC), que son las entidades que representan el 48 % de las exportaciones argentinas. Además, en el pasado mes al país ingresaron unos 2.678.296.709 dólares, un 31,11% con respecto al precedente mes de noviembre.

Según precisaron desde las entidades, la reconstitución de stocks en los países centrales mantiene elevados los precios internacionales de los commodities, en tanto se inició la campaña de trigo en la Argentina. Sin embargo, en la Argentina se debió afrontar la persistente bajante del río Paraná, junto con una menor cosecha, y diferentes conflictos gremiales que se registraron en los principales puertos exportadores.

Todo esto sucedió en momentos donde las reservas del Banco Central atraviesan un período crítico y la entidad sigue perdiendo dólares. Para principios del 2022, la mirada del Gobierno está puesta en los ingresos de dólares que generarán las exportaciones de los cultivos de invierno, que se están cosechando en estos momentos. El trigo podría aportar más de 4.000 millones de dólares en concepto de exportaciones.

A todo esto, la Bolsa de Comercio de Rosario proyectó para el 2022 una campaña agrícola con una siembra récord de 38,8 millones de hectáreas, la cosecha total de granos y oleaginosas alcanzaría los 144,5 millones de toneladas y divisas de la exportación por 38.400 millones de dólares.

Si bien se espera que los precios de los commodities agrícolas “caigan levemente”, los serán compensados por un mayor venta externa en volumen. Además, habrá que seguir con atención la situación climática, en momento de una ausencia notoria de precipitaciones en las principal zonas productivas.

Hay que recordar que la liquidación de divisas está fundamentalmente relacionada con la compra de granos que luego serán exportados ya sea en su mismo estado o como productos procesados, luego de una transformación industrial.

La mayor parte del ingreso de divisas en este sector se produce con bastante antelación a la exportación, anticipación que ronda los 30 días en el caso de la exportación de granos y alcanza hasta los 90 días en el caso de la exportación de aceites y harinas proteicas. Esa anticipación depende también del momento de la campaña y del grano de que se trate, por lo que no existen retrasos en la liquidación de divisas.

##### Aporte

El complejo oleaginoso-cerealero, incluyendo al biodiésel y sus derivados, aportó el año pasado el 48% del total de las exportaciones de la Argentina, según datos del INDEC. Por otro lado, el Departamento de Agricultura de los Estados Unidos (USDA, por sus sigas en inglés) proyectó que la Argentina mantendrá el primer puesto en exportaciones 2020/21 de aceite y harina de soja.

En ese sentido, el principal producto de exportación del país es la harina de soja, representando un 14,2 % del total. Se trata de un subproducto industrializado generado por este complejo agroindustrial, que tiene actualmente una elevada capacidad ociosa cercana al 50%. El segundo producto más exportado el año pasado, de acuerdo con el INDEC, fue el maíz (11 %) y el tercero el aceite de soja (6,9 %).