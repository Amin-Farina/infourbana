+++
author = ""
date = 2021-08-02T03:00:00Z
description = ""
image = "/images/juntos.jpg"
image_webp = "/images/juntos.jpg"
title = "PARA BAJAR LA TENSIÓN INTERNA, JXC SELLÓ UN PACTO DE CONVIVENCIA"

+++

**_La principal coalición opositora consiguió un “alto al fuego” en medio del enfrentamiento interno entre referentes y precandidatos a las elecciones legislativas. Referentes de Juntos por el Cambio debatieron por Zoom y alcanzaron un “acuerdo político de convivencia entre todos los partidos” que integra la coalición._**

La Mesa Nacional de la coalición se va a reunir todas las semanas y será la garante de que se cumpla con el pacto de no agresión, que va a reemplazar al “código de convivencia” que impulsaba el espacio para terminar con los duelos verbales de los integrantes de esta agrupación.

Uno de los referentes de Juntos por el Cambio que participó del encuentro virtual planteó que “no hacía falta un código de ética ni un manual” para bajar la tensión y evitar el fuego cruzado. Consideró que un manual hubiera expuesto a la coalición como “un gran espacio de confrontaciones."

El objetivo del acuerdo pasa por tratar de consensuar “los cuatro o cinco ejes que los precandidatos deberán priorizar en todas las provincias” con el foco en que la unidad debe ser “el valor máximo a defender”.

Durante la cumbre manifestaron que la sociedad les pide “unidad y fortaleza para frenar al kirchnerismo” y que por eso deben mostrar concordancia. Los referentes de la coalición aseguraron que durante la reunión “no se habló de ninguna persona en particular, ni se hizo alusión a ningún episodio en particular”.

Explicaron que se diagramará un informe entre las tres fundaciones, Alem (UCR), Pensar (PRO) y Hannah Arendt (Coalición Cívica) “con los principales acuerdos programáticos que se trabajaron en conjunto”.