+++
author = ""
date = 2021-08-17T03:00:00Z
description = ""
image = "/images/ags-1.jpeg"
image_webp = "/images/ags.jpeg"
title = "ROSARIO: ASALTARON CON UN CUCHILLO A UNA CONCEJALA DEL FDT"

+++

**_La concejala del Frente de Todos, Alejandra Gómez Sáenz, fue víctima de un violento asalto el pasado sábado en Rosario, Santa Fe mientras hacía un recorrido de campaña. El hecho ocurrió en Empalme Graneros, un barrio en el noroeste de la ciudad, cuando un delincuente la apuntó con un cuchillo para robarle._**

Gómez Sáenz estaba acompañada por el precandidato a senador, Marcelo Lewandowski, y su equipo, que nada pudieron hacer para detener el hurto. El ladrón pudo llevarse las pertenencias de la funcionaria, pero no llegó muy lejos ya que fue perseguido, golpeado y atrapado por los vecinos que lo entregaron a la policía.

Cuando llegó a la comisaría local, los uniformados descubrieron que tenía dos mochilas llenas de otros objetos que también había robado. El delincuente fue identificado como Maximiliano G, y a raíz de las heridas fue trasladado al Hospital de Emergencias Clemente Álvarez.

Gómez Sáenz publicó en su cuenta de Twitter una explicación de lo que pasó: “El sábado (14 de agosto) por la tarde, en una actividad de campaña que vinculó dos centros comunitarios con los que trabajamos en Empalme Graneros, ocurrió el hecho que tomó conocimiento público a través de medios y redes sociales”.

“No es ni la primera ni la última vez que nos pasará esto”, siguió. “Caminamos los distintos barrios a diario y uno de los mayores reclamos del pueblo rosarino es, desde hace años, más seguridad”.

Asimismo, reconoció: “Sostengo desde siempre la importancia de las políticas de bienestar de las fuerzas como uno de los ejes fundamentales de las políticas de seguridad. Queremos más y mejor policías, con mejor formación y con una carrera profesional que enorgullezca a la comunidad”.