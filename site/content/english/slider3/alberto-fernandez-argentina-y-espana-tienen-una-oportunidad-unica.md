+++
author = ""
date = 2021-06-09T03:00:00Z
description = ""
image = "/images/qnrnh5iytzggreeqfrxp6bncfq.jpeg"
image_webp = "/images/qnrnh5iytzggreeqfrxp6bncfq.jpeg"
title = "ALBERTO FERNÁNDEZ: \"ARGENTINA Y ESPAÑA TIENEN UNA OPORTUNIDAD ÚNICA\""

+++
**_El presidente Alberto Fernández destacó hoy que la Argentina y España tienen una "oportunidad única para entenderse y poder avanzar" en las relaciones bilaterales y destacó que las empresas españolas "tienen toda las posibilidades para seguir invirtiendo" en el país._**

Al participar de un encuentro junto a su par español Pedro Sánchez y empresarios de ambos países, Fernández llamó a hacer "aún más sólidos" los vínculos y "mancomunar esfuerzos a un lado y otro del Atlántico".

Fernández le agradeció a Sánchez el apoyo de su país en el proceso de renegociación de la deuda que encara la Argentina con el Fondo Monetario Internacional y el Club de París.

"Quiero darle las gracias al Gobierno de España por todas las veces que requerí de su ayuda para enfrentar los momentos más álgidos cuando asumimos. Siempre estuvieron del lado nuestro, acompañándonos y atendiendo nuestros reclamos en el tema de la deuda", dijo Fernández.

#### LA GIRA

Sánchez llegó anoche a la Argentina en un vuelo oficial junto a la comitiva de funcionarios y empresarios para llevar adelante hoy una serie de actividades junto al mandatario argentino, con el foco puesto en relanzar la relación estratégica y avanzar en la "asociación pública y privada" de la economía de los dos países.

Además, viajaron junto al mandatario una decena de empresarios con importantes inversiones en la Argentina y dos líderes de centrales sindicales.

Entre los acuerdos que firmarán se destacan el Plan de Acción Estratégica; la Declaración de Buenos Aires; la Declaración sobre el intercambio de archivos diplomáticos referidos a la última dictadura militar en Argentina y la Declaración conjunta sobre temas de género, según detalló el Gobierno nacional en un comunicado.

Antes de partir hoy por la noche hacia Costa Rica -próximo destino de su gira por Latinoamérica-, Sánchez completará su agenda con la visita al Espacio de la Memoria, ubicado en el lugar en el que funcionó el excentro de detención y torturas de la Escuela de Mecánica de la Armada (ex ESMA) para luego realizar -en el jardín de la embajada ibérica en Buenos Aires- un homenaje a los españoles desaparecidos durante la última dictadura cívico militar.

La visita del jefe de Gobierno a Buenos Aires había sido anunciada el 11 de mayo pasado por los propios mandatarios tras el encuentro que mantuvieron en Madrid, en el marco de la gira europea del presidente argentino.

En una entrevista con Télam antes de embarcarse hacia Buenos Aires, el embajador argentino en España, Ricardo Alfonsín, había adelantado que "existe un gran interés público y privado de asociar" las economías de ambos países, lo que traerá "grandes oportunidades", y destacó la confianza española en la "recuperación argentina" en el mediano plazo.

También precisó que España buscará promover sus negocios en áreas estratégicas como las energías renovables, el litio, el hidrógeno verde, la industria alimentaria, la economía del conocimiento y la tecnología, entre otras.

El pasado 26 de mayo, el jefe de Gobierno español presentó en Madrid su plan de internacionalización de la economía para 2021-2022, que destinará 4.500 millones de euros a reforzar la actividad económica española en América Latina.

En el encuentro que mantuvieron en Palacio de la Moncloa el mes pasado, Sánchez le había dicho a Fernández que la Argentina sería "uno de los países prioritarios" en la agenda del programa.

"Se trata de una visita de gran significación económica y política para ambos países, en la que continuarán y ampliarán las conversaciones que ambos presidentes iniciaron en sus anteriores encuentros", contó Alfonsín desde Madrid.

También, resaltó que "con España hay coincidencias muy importantes que tienen que ver con los fuertes vínculos históricos".

"Compartimos la defensa de la democracia y los derechos humanos, el multilateralismo, la solución pacífica de los conflictos, las reivindicaciones de género, la necesidad de una globalización más justa, la lucha contra el cambio climático, la reivindicación del acceso igualitario a la salud y en esta coyuntura, a las vacunas", resumió el embajador de origen radical e hijo del fallecido expresidente Raúl Alfonsín.