+++
author = ""
date = 2022-02-01T03:00:00Z
description = ""
image = "/images/fernandez-maximo-1284945-1.jpg"
image_webp = "/images/fernandez-maximo-1284945.jpg"
title = "MÁXIMO KIRCHNER RENUNCIÓ A LA PRESIDENCIA DEL BLOQUE DEL FRENTE DE TODOS EN DIPUTADOS"

+++
#### **_El hijo de la vicepresidenta lo anunció tras el acuerdo del gobierno de Alberto Fernández con el FMI_**

En este caso la carta fue de Máximo Kirchner. Después de tres días de silencio de Cristina, sin que se pronunciara sobre el entendimiento con el FMI, el diputado llevó otra vez a un punto de tensión extrema la crisis interna en el Gobierno con un texto en el que anunció su renuncia a la jefatura del bloque del Frente de Todos. Lo argumentó con su desacuerdo con la negociación con el Fondo Monetario y responsabilizó a Alberto Fernández y al gabinete económico encabezado por Martín Guzmán.

Máximo aseguró que se mantendrá en la bancada oficialista, en los hechos virtualmente fracturada, lo que quedará expuesto cuando el acuerdo con el FMI se discuta en el Congreso. En el escrito, a su vez, fue más allá de las diferencias sobre la estrategia con el organismo y pasó facturas por los “agravios recibidos” antes de la conformación del Frente de Todos en 2019 y por el trato de sus socios durante los años siguientes. También dejó una frase sugestiva al fundamentar que era conveniente que el Presidente eligiera para conducir al bloque a alguien que respalde el programa con el Fondo con la mirada “más allá del 10 de diciembre de 2023”.

Antes de publicar la carta, Máximo llamó a Alberto Fernández para adelantarle la renuncia y los motivos. Tambien mantuvo conversaciones con Sergio Massa -le envió el borrador del texto-, el titular de la Cámara que sin éxito intentó convencerlo de que replanteara su determinación. “Cristina también tiene matices con el tema del Fondo, pero el Presidente soy yo”, dijo el jefe de Estado a C5N, y buscó desligar a la ex mandataria de la determinación que tomó su hijo.

"Esta decisión nace de no compartir la estrategia utilizada y mucho menos los resultados obtenidos en la negociación con el FMI llevada adelante exclusivamente por el gabinete económico y el grupo negociador que responde y cuenta con la absoluta confianza del Presidente de la Nación, a quien nunca dejé de decirle mi visión para no llegar a este resultado”, explicó Máximo de ese modo su alejamiento de la jefatura del bloque, luego del silencio en el que se mantenían Cristina y los principales referentes de La Cámpora ante el anuncio que el viernes habían encabezado Alberto Fernández -con un mensaje grabado-, Guzmán y el jefe de Gabinete, Juan Manzur.

Una vez cerrado el acuerdo con el FMI tendrá que pasar por el Congreso, por la ley sancionada en 2020. El nuevo capítulo de la crisis interna abre interrogantes sobre el trámite parlamentario y la postura que bajará Cristina Kirchner en el Senado. Juntos por el Cambio se pronunció a favor, aunque esperaba justamente la definición de la vicepresidenta, además de la letra chica del resultado de las negociaciones.

El ahora ex titular de la bancada oficialista publicó el comunicado al caer la tarde de este lunes, en la previa de la partida del Presidente a Rusia y China, con un paso por Barbados a su regreso, y Guzmán como parte de la comitiva en Moscú. La decisión intempestiva remitió a las renuncias en cadena en la semana siguiente a la derrota del Frente de Todos en las primarias de septiembre, iniciadas por el ministro del Interior, Wado de Pedro, uno de los dirigentes más cercanos a Cristina Kirchner.

“El FMI demuestra que lo importante no son las razones ya que sólo se trata de fuerza. Quizás su nombre debiera ser Fuerza Monetaria Internacional. Y como veo que siempre se interesan por los gastos, podrían ahorrar en economistas caros ya que para hacer lo que hacen sólo basta con gente que sepa apretar prometiendo el infierno si no se hace lo que ellos quieren”, esgrimió Máximo. Hasta entrada la noche de este lunes, no estaba definido el nuevo jefe de bloque. Alberto Fernández aseguró que se resolverá este martes.

En la carta, Máximo también cuestionó la reestructuración de la deuda con los acreedores privados: marcó un “profundo desacuerdo” con algunos artículos de aquella ley que avaló como jefe de bloque por “pragmatismo”, según alegó: “Comprendí el contexto y arreciaba la pandemia”.

En otro tramo pasó facturas: “Dejé los prejuicios de lado y también los agravios recibidos para conformar el Frente de Todos, no así las convicciones. No lamento haber brindado un trato que no fue recíproco”. Más: “Entendí desde el momento en que CFK nos dio la instrucción de construir el Frente de Todos que lo sucedido hasta su conformación no podía obturar lo que vendría. Lamentablemente fui uno de los pocos que actuó de esa manera”.

Al “entorno” -así se refirió- del Presidente también le dedicó una crítica por dejar trascender que el kirchnerismo lo presionaba: “No. Eso lo hace el Fondo Monetario Internacional”.

##### El comunicado de Máximo Kirchner

> He tomado la decisión de no continuar a cargo de la presidencia del Bloque de Diputados del Frente de Todos. Esta decisión nace de no compartir la estrategia utilizada y mucho menos los resultados obtenidos en la negociación con el Fondo Monetario Internacional (FMI), llevada adelante exclusivamente por el gabinete económico y el grupo negociador que responde y cuenta con la absoluta confianza del Presidente de la Nación, a quien nunca dejé de decirle mi visión para no llegar a este resultado.
>
> Permaneceré dentro del bloque para facilitar la tarea del Presidente y su entorno. Es mejor dar un paso al costado para que, de esa manera, él pueda elegir a alguien que crea en este programa del Fondo Monetario Internacional, no sólo en lo inmediato sino también mirando más allá del 10 de diciembre del 2023.
>
> Desde el día en que, en el año 2018, Mauricio Macri trajo de regreso a la Argentina al FMI, a quien Néstor Kirchner a través de la cancelación histórica de una deuda de 9.800 millones de dólares, lograra desterrar de nuestro país por ser el detonante clave en cada crisis económica desde la vuelta de la democracia y cuyo símbolo más elocuente fue la crisis del año 2001, sabíamos que este sería el problema más grande para nuestro país. Sobre todo por la magnitud y excepcionalidad del préstamo que alcanzara los 57.000 millones de dólares (casi 6 veces el monto cancelado en el año 2005) y del cual se desembolsaron 44.500 millones de dólares en sólo un año, perdonando además, todos y cada uno de los incumplimientos del Gobierno de Macri y violando su propio Estatuto constitutivo. Macri tenía que ganar la elección.
>
> No aspiro a una solución mágica, sólo a una solución racional. Para algunos, señalar y proponer corregir los errores y abusos del FMI que nunca perjudican al Organismo y su burocracia, es una irresponsabilidad. Para mí lo irracional e inhumano, es no hacerlo. Al fin y al cabo, el FMI demuestra que lo importante no son las razones ya que sólo se trata de fuerza. Quizás su nombre debiera ser Fuerza Monetaria Internacional. Y como veo que siempre se interesan por los gastos, podrían ahorrar en economistas caros ya que para hacer lo que hacen sólo basta con gente que sepa apretar prometiendo el infierno si no se hace lo que ellos quieren.
>
> Y si algunos dudan de mi nivel de pragmatismo les recuerdo que como Jefe de Bloque acompañé la sanción de la Ley que aprobó la reestructuración de la deuda privada en moneda extranjera, sin quita de capital pero con quita de intereses, realizada por el Gobierno en el año 2020, a pesar de estar en profundo desacuerdo con algunos de sus artículos. Comprendí el contexto y arreciaba la pandemia.
>
> Dejé los prejuicios de lado y también los agravios recibidos para conformar el Frente de Todos, no así las convicciones. No lamento haber brindado un trato que no fue recíproco. Entendí desde el momento en que CFK nos dio la instrucción de construir el Frente de Todos que lo sucedido hasta su conformación no podía obturar lo que vendría. Lamentablemente fui uno de los pocos que actuó de esa manera.
>
> Sería más que incorrecto aferrarse a la Presidencia del Bloque cuando no se puede acompañar un proyecto de una centralidad tan decisiva en términos del presente y los años que vendrán.
>
> Algunos se preguntaran qué opción ofrezco. En principio, llamar a las cosas por su nombre: no hablar de una dura negociación cuando no lo fue, y mucho menos hablar de “beneficios”. La realidad es dura. Vi al presidente Kirchner quemar su vida en este tipo de situaciones.
>
> En la cena de fin de año, realizada en la Quinta de Olivos, cada uno de los integrantes del bloque, así como el Presidente de la Nación, me pudieron escuchar cuando aseguré que cualquiera de mis compañeros y compañeras podía hacerse cargo de mi lugar.
>
> Asimismo, ningún Diputado ni Diputada de nuestro bloque recibió una instrucción para expresarse en contra de la imposición del Fondo. Es cierto también, que no recibieron una instrucción para hacerlo en su favor. Y es por ello mismo que decido liberar al Presidente para que no se sienta “presionado”, como tantas otras veces ha hecho trascender su entorno. ¿Presionar? No. Eso lo hace el Fondo Monetario Internacional.
>
> Agradezco a cada compañero y compañera el trabajo realizado: la Ley de Fuego, el Aporte Solidario y extraordinario, Etiquetado Frontal, Ley de Cupo Trans, Ley de Tele-Trabajo, la Ley de Zona Fría, Ley de personas en situación de calle, fondo PREIMBA, Equidad en Medios y otras tantas que buscaron cumplir en parte demandas y necesidades de nuestros ciudadanos y ciudadanas. Muchas de ellas en acuerdo con diferentes bloques y otras no, como es lógico en democracia. Y otras, como fue recientemente en Bienes Personales, de vital importancia para el Gobierno, que se logró por el trabajo de acuerdos realizado en la Cámara de Diputados.
>
> Seguiré trabajando, como lo hice hasta ahora, con una agenda que incluya leyes como Humedales, Producción de Cannabis Medicinal y Cáñamo Industrial, Acceso a la Tierra, Envases, Fomento al Desarrollo agro-industrial Federal e Inclusivo y Promoción de la Industria Automotriz.
>
> Agradezco también a los Presidentes de los demás Bloques, con quienes hemos discutido fuerte pero siempre sin faltarnos el respeto y de quienes también aprendí mucho. Entre otras cosas, el lamentarse por no haber levantado su voz cuando no estaban de acuerdo en temas de extrema centralidad promovidos por su propio Gobierno.
>
> Por comprensión histórica, por mandato popular y por decisión política, mi postura no busca señalar a quienes acompañan de manera crítica o directamente festejan, como he leído. Ojalá todo salga en los próximos años como el sistema político, económico y mediático argentino promete y mis palabras sean las de alguien que en base a la experiencia histórica solo se equivocó y no hizo otra cosa que dejar un lugar para seguir ayudando. No busco estar a la izquierda, ni mucho menos a la derecha de nadie, categorías que ya no alcanzan para explicar la realidad.
>
> Abrazo muy fuerte a todos mis compañeras y compañeras, y a cada argentino y argentina, con quienes, voten como voten, compartimos el mismo suelo y la misma bandera.

DS