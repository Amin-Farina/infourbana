+++
author = ""
date = 2021-04-29T03:00:00Z
description = ""
image = "/images/eeuu.jpg"
image_webp = "/images/eeuu.jpg"
title = "LA ECONOMÍA DE EEUU SE RECUPERA Y CRECE 6.4% EL 1ER TRIMESTRE DE ESTE AÑO"

+++

**_La reapertura de empresas y el aumento del gasto también hicieron subir los precios, con un aumento de la inflación del 3,5% en el mismo periodo. En otra señal positiva, el número de estadounidenses que solicitaron beneficios por desempleo se redujo al nivel más bajo desde el comienzo de la pandemia._**

La economía de Estados Unidos creció un 6,4% en el primer trimestre de este año por una recuperación del consumo y un aumento del gasto público, que reflejan la recuperación en marcha que va dejando atrás la crisis inducida por la pandemia, según datos del gobierno publicados este jueves.

Con la reapertura de muchos comercios y el gasto alentado por las ayudas públicas, los precios aumentaron y la inflación se aceleró 3,5% entre enero y marzo, en comparación con un nivel de 1,5% en el último trimestre de 2020, informó el Departamento de Comercio.

Este es el primer cálculo hecho por el Gobierno de Estados Unidos sobre la actividad económica en el inicio del año, después de la contracción anual del Producto Interior Bruto (PIB) del 3,5% registrada en el mismo periodo de 2020.

La evolución del PIB entre enero y marzo fue del 1,6 % respecto al trimestre precedente.

“El aumento en el primer trimestre reflejó la continuada recuperación económica, la reapertura de negocios, y la continuada respuesta del gobierno relacionada con la pandemia”, señala el informe trimestral.

Entre ellos, mencionó las transferencias directas, la extensión del subsidio de desempleo y las ayudas a pequeñas y medianas empresas.

El presidente estadounidense, Joe Biden, que ha propuesto un multimillonario plan de inversión en infraestructuras y en programas de protección social, ha asegurado que el crecimiento de EEUU en 2021 podría superar el 6 % anual, algo que no ha sucedido en el país desde la década de 1980.