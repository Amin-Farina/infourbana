+++
author = ""
date = 2021-09-06T03:00:00Z
description = ""
image = "/images/bvvrawyswjalplarjtx5gu5xwu.jpg"
image_webp = "/images/bvvrawyswjalplarjtx5gu5xwu.jpg"
title = "LOS BANCO PRESTAN AL ESTADO 9 DE CADA 10 PESOS QUE DEPOSITAN LOS AHORRISTAS"

+++

#### **_Por el elevado déficit fiscal, el sector público absorbe la casi totalidad del crédito disponible a través de bonos del Tesoro y las Leliq del BCRA._**

Por el elevado rojo fiscal, el Gobierno se ve obligado a recurrir a los mecanismos de financiamiento que tenga a mano, en momentos en que una economía diezmada por la recesión y las medidas de aislamiento por el coronavirus.

Es así, que en el presente el equivalente al 90% de los depósitos del sector privado están invertidos en deuda pública, tanto en títulos emitidos por el Banco Central como en Letras colocadas por el Tesoro para hacer frente a sus necesidades de caja.

En los países desarrollados, el negocio de los bancos es el de tomar dinero de los ahorristas -a cambio de una tasa de interés- y prestarlo fundamentalmente al sector privado, a una tasa superior. Con ese crédito se financian los proyectos de inversión que respaldan el crecimiento de la economía. Y la ganancia de los bancos es el diferencial de tasas entre préstamos y depósitos.

En una Argentina en crisis y con alto déficit público e inflación, el sector privado no tiene margen para endeudarse ni horizontes de crecimiento y, por lo tanto tampoco invierte, lo que genera una suerte de círculo vicioso. En el mismo sentido, un déficit fiscal cada vez más elevado lleva a que los fondos disponibles sean aspirados casi en su totalidad por el sector público, que desplaza al privado.

La consecuencia de este fenómeno es que la mayor parte de los depósitos que reciben los bancos de empresas e individuos no vuelve al sector privado, sino que es redirigido a la compra de deuda pública.

Un informe elaborado por la consultora Gabriel Rubinstein & Asociados concluyó que esta relación está en su pico máximo de los últimos tiempos.

“El 90% de los depósitos en pesos del sector privado están colocados en instrumentos que emite el BCRA (62%) o el Tesoro (28%). Esta exposición creció fuertemente desde marzo de este año y se consolida en el rango que va del 85% al 90% donde fluctúa desde hace varios meses”, precisó.

“La exposición al sector público viene creciendo muy fuerte en los últimos meses. En septiembre del año pasado llegaba al 78% de los depósitos del sector privado y ahora ya estamos en el 90%”, subrayó.

Si tomamos puntualmente el stock de plazos fijos del sector privado, el BCRA informó que al 31 de agosto totalizaban unos $3,38 billones ($3,07 billones por plazos fijos tradicionales; $176.839 millones ajustados por UVA y $132.862 millones en saldos inmovilizados), en su totalidad colocados en pasivos remunerados del Banco Central, por unos $3,98 billones ( $2,08 billones en Leliq más $1,9 billón en Pases pasivos).

Ese aumento se relaciona al menos en parte con la fuerte emisión monetaria del año pasado. Los pesos emitidos terminaron en buena medida en los bancos y luego el Central sale a absorberlos vía Leliq y pases pasivos. A este fenómeno se agregó el Tesoro nacional y sus crecientes necesidades de financiamiento para salir a cubrir el déficit cuando no recurre a la emisión. Según Rubinstein, el 62% de los depósitos privados se encuentra colocado en instrumentos emitidos por el Central para absorber dinero excedente y un 28% en letras del Tesoro.

La tasa nominal anual que el BCRA les paga a los bancos tanto los Pases como las Leliq ronda el 38% y, en el caso de los títulos del Tesoro, levemente superior. Esto se traduce en una tasa efectiva anual (al renovarse permanentemente los vencimientos, el llamado rollover) del 45,5%, inferior a la inflación levemente sobre 50% anual.

Este rendimiento negativo se traslada a los ahorristas. En el caso de los plazos fijos tradicionales entre 30 a 44 días y hasta por $100.000, la tasa nominal anual asciende a un 36,6% (por lógica, inferior al 38% por las Leliq que percibe la entidad financiera) y la tasa efectiva anual alcanza el 43,3% si se renueva el plazo fijo mes a mes durante un año.

Se trata de un rendimiento que está unos siete puntos porcentuales por debajo de la inflación, lo que implica una importante pérdida patrimonial para el ahorrista que, en definitiva, es el que carga con el peso del financiamiento público, compartido con el BCRA, que emite pesos sin respaldo con el mismo fin.

¿Hay peligro en semejante concentración de recursos que destinan los bancos a deuda emitida por el sector público, tanto el Tesoro como el Banco Central? El principal riesgo -hoy reprimido por el estricto control de capitales- es que se reduzca la demanda de pesos de la gente y de las empresas, por ejemplo ante el temor de una devaluación. En ese caso, podría haber retiro de depósitos que presione a los bancos a desarmar parte de su cartera en bonos. En ese escenario, el Central debería devolver las Leliq a los bancos y eso implicaría automáticamente mayor emisión monetaria.

Al mismo tiempo, también correría riesgo el programa de financiamiento del Tesoro, que en buena medida descansa en el financiamiento que aportan los bancos.

Este fenómeno se denomina crowding out, también conocido como efecto “desplazamiento” o efecto “expulsión”, una situación en la que la capacidad de inversión de las empresas se reduce debido a la deuda pública, es decir: la expulsión del sector privado de la economía por parte del sector público.