+++
author = ""
date = 2021-07-08T03:00:00Z
description = ""
image = "/images/elections.jpg"
image_webp = "/images/elections.jpg"
title = "SEGÚN ENCUESTA DE LA CONSULTORA TAQUION, EL 44% DE LOS ARGENTINOS PIENSA VOTAR DISTINTO AL 2019"

+++

**_Una encuesta de la consultora Taquion Research Estrategy da cuenta de esta volatilidad: el 55,4% de los consultados volvería a votar a la misma persona a la que eligieron en 2019._** 

La encuesta, registrada entre el 20 y 25 de junio pasado, fue realizada de manera online y está basada en 2503 casos relevados en todo el país. Uno de los objetivos del estudio de la consultora es si prevalecerá el “voto cautivo” habrá un “éxodo” hacia otras opciones.

Los resultados apuntan hay mayor volatilidad entre Juntos por el Cambio y el resto de los espacios de la oposición que en el Frente de Todos. Los electorados conviven en un escenario definido por Taquión entre “la coherencia y la desilución”.

Según la muestra, el 95% votantes que se inclinaron por Alberto Fernández en 2019 volverían a hacerlo si se repetiera la oportunidad. No pasa lo mismo en Juntos por el Cambio, ya que esa cifra se reduce al 67 por ciento, lo que “deja en evidencia la interna que prevalece en este espacio”, dice el informe.

El análisos se acerca a cómo se encuentra la opinión de la ciudadanía sobre la gestión de las vacunas contra el coronavirus. Tanto el Gobierno como la oposición siguen de cerca el impacto de la campaña y si tendrán alguna influencia en el clima políticos y los votos.

Según Taquión, 6 de cada 10 consultados sostienen que la vacunación “no cambia su opinión sobre el gobierno nacional y su gestión con las vacunas”. La consultora afirma que “la gente considera que esto es un deber hacer del Estado, más que un éxito de la gestión de Alberto Fernández. Incluso, 21.3% sostiene que cambió para peor”, indicó.

Otro foco del estudio es el alcance de la polarización política. La percepción de aquellos que piensan que la grieta “está más viva que nunca” creció un 40,9 por ciento desde abril de 2020, cuando había un clima de diálogo entre las fuerzas políticas por el comienzo de la pandemia. En el último mes, el 83% de los consultados creen que considera que sigue creciendo el enfrentamiento, de acuerdo al monitor nacional Taquion, Horus, Inclusión y Gestión Federal.

El clima de opinión sigue siendo negativo para el Gobierno. Aumentó 10% la preocupación sobre el “acceso a oportunidades en la Argentina”, en sintonía con el impacto económico, social y laboral que arrojó la pandemia. De acuerdo al análisis, los profesionales, dueños de pymes o emprendedores y monotributistas son los más preocupados sobre este punto, alcanzando el 40 por ciento, es decir, unos 10 puntos por encima del promedio general.

“Esto conlleva un problema grave mirando hacia el futuro. Y este porcentaje de preocupados no solo abarca al universo opositor: también a aquellos que votaron al Frente de Todos en las últimas elecciones”, señaló la consultora fundada y dirigida por Sergio Doval.

Además, otro indicador arroja que 7 de cada 10 personas interrogadas “están poco o nada satisfechos con el funcionamiento de la democracia” en Argentina. Pero ese malestar no se refleja en actitudes autoritarias marcadas: 8 de cada 10 están de acuerdo con la frase “más allá de los problemas que pueda tener, la democracia es mejor que cualquier otra forma de gobierno”. El 83% afirma que concurriría a las urnas aunque no exista la obligación.

Acerca de las preferencias generales, 4 de cada 10 personas creen que la honestidad como valor de un político aparece como un significante que busca reinstalarse en las demandas. Para el votante de Frente de Todos, hay dos cualidades más importantes que la honestidad: conciencia social (31%) y compromiso con la gestión (23,9%).

Entre las figuras del Frente de Todos, todas bajaron en su imagen sobre la marcha de la gestión. Alberto Fernández cayó al 61,1% de percepción negativa, Cristina Kirchner obtuvo el 67% y el gobernador Axel Kicillof alcanzó el 64,6 por ciento.

En el caso de la oposición de Juntos por el Cambio, Mauricio Macri sigue con baja imagen positiva, pero subió 1,5 puntos al 27,5 por ciento. El jefe de Gobierno porteño, Horacio Rodríguez Larreta, creció del 41,1 al 43 por ciento, y registró una baja de su imagen negativa del 41,8 al 38,5 por ciento.

Como expresión de la interna, hubo variaciones entre quienes estarían ejerciendo el liderazgo de la oposición. En ese marco, crecieron en ese rol las figuras de Patricia Bullrich (+2,4%), Mauricio Macri (2,3%) y Javier Milei (1,2%), mientras que bajó Rodríguez Larreta (1,7%), aunque sigue encabezando como dirigente principal con un índice en torno al 20 por ciento. Ahora bien, el 20,4% cree que no hay un líder de la oposición identificado.

Un matiz aparece en torno al escenario regresivo para el oficialismo. Aunque 5 de cada 10 argentinos se muestran todavía preocupados por la situación nacional, la medición detectó que subió la esperanza un 3% respecto al futuro entre abril y junio de este año. Es un síntoma de que, lentamenta, “la sociedad empieza a percibir que lo peor de la pandemia ya pasó: la vacunación y el nivel de contagios estabilizados son variables que inciden en los cambios de humor social”, señala Taquion.

Si bien la disparidad entre el pronóstico de las encuestas y los resultados finales de las últimas décadas ocasionó un debate en torno a la incapacidad predictiva de los sondeos electorales, los sondeos siguen siendo un instrumento importante en las campañas electorales. Son valoradas tanto por la dirigencia política como por la ciudadanía, ya que tienen utilidad para la toma de decisiones o determinar estrategias de campaña.