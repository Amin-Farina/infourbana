+++
author = ""
date = 2021-09-23T03:00:00Z
description = ""
image = "/images/926x0_275893_20210923163542.jpg"
image_webp = "/images/926x0_275893_20210923163542.jpg"
title = "MANZUR Y WADO DE PEDRO COMENZARON A REUNIRSE CON LOS CANDIDATOS QUE PERDIERON LAS PASO"

+++

#### **Con la mira en las elecciones generales, el jefe de Gabinete y el ministro del Interior tantean el terreno en distritos clave y prometen instrumentos y fondos nacionales para apuntalar a postulantes y gobernadores “propios”. El primer turno fue para Santa Fe, Tierra del Fuego y La Pampa.**

Con la mira en las elecciones generales, el nuevo jefe de Gabinete, Juan Manzur, y el ministro del Interior, Eduardo “Wado” de Pedro empezaron hoy una maratón de reuniones con gobernadores y candidatos de las provincias donde el Frente de Todos perdió en las PASO. La agenda oficial de los encuentros se pautó para hablar de la gestión en relación a la campaña, con el objetivo de mejorar -e inclusive revertir, según el caso- los resultados que sacudieron a la coalición nacional en la mayor parte del país hace 11 días.

Las conversaciones con los gobernadores y candidatos del interior nunca cesaron desde la derrota del 12 de septiembre, e inclusive continuaron durante el cimbronazo interno que derivó en una serie de cambios en el Gabinete. Sin embargo, ahora los dos funcionarios con más llegada al interior del país empezaron a formalizar los encuentros y a orientar los diálogos al rediseño de la campaña electoral y a las ayudas nacionales en los temas prioritarios para cada distrito.

Esta mañana, De Pedro y Manzur recibieron a los candidatos de la lista ganadora en la interna del Frente de Todos en Santa Fe: al senador provincial Marcelo Lewandowski, que se impuso en la disputa contra el ex ministro de Defensa Agustín Rossi; y a los senadores nacionales María de los Ángeles Sacnun, cercana a Cristina Kirchner, y Roberto Mirabella, hombre del riñón del gobernador Omar Perotti. Los tres compitieron con el apoyo expreso de la Casa Rosada después de la descarnada batalla en el oficialismo local que terminó ordenando sobre el cierre de listas de julio la Vicepresidenta.

Ahora, el gobierno nacional tiene fuerte interés en apuntalar a sus tropas en la tercera provincia más poblada del país, donde el Frente de Todos, que compitió dividido, perdió contra Juntos por el Cambio por 11 puntos. También la oposición compitió con listas separadas, donde se impuso la periodista Carolina Losada con el apoyo del radicalismo.

El encuentro de hoy entre los santafecinos, el ministro y el jefe de Gabinete se produjo a puertas cerradas. Se habló sobre cómo mejorar en las principales preocupaciones de los electores a nivel local: los problemas de seguridad, en especial vinculados al avance del narcotráfico en la provincia; y la actividad agrícola ganadera, central para la economía local.

En Santa Fe cayó muy mal la polémica suspensión a las exportaciones a la carne, que se sumó al antecedente de la expropiación de Vicentin. Días atrás, para discutir los mismos temas, el gobernador Perotti se había reunido con los nuevos ministros de Seguridad, Aníbal Fernández; y de Agricultura, Ganadería y Pesca, Julián Domínguez; así como con el titular de Hacienda, Martín Guzmán.

Hoy, hacia el mediodía, De Pedro recibió en la Casa Rosada al gobernador de Tierra del Fuego, Gustavo Melella, que más tarde se reunirá con Manzur. El caudal electoral que aporta la pequeña provincia es ínfimo -el padrón apenas supera los 100 mil votantes-, pero en el adverso contexto post-PASO el Gobierno evalúa que cada voto suma. Además, en la provincia más austral del país el Frente de Todos perdió por una diferencia menor en comparación con las catástrofes en el centro del país, y hay esperanza de revertir el resultado. Se habló del proyecto de desarrollo del Polo Logístico Antártico y de la prórroga del subrégimen industrial, en conexión con el escenario electoral.

En las primeras horas de la tarde, el ministro coordinador y el ministro político se preparaban para recibir a Sergio Ziliotto, jefe provincial de La Pampa, donde Juntos por el Cambio, con el total de sus listas, obtuvo una ventaja de 10 puntos sobre el oficialismo local y nacional. El objetivo de los encuentros, que continuarán en los próximos días con otros jefes provinciales, fue escuchar los planteos y diagnósticos sobre las realidades electorales locales de primera mano y empezar a organizar la campaña que se lanza dentro de un mes y que en los papeles se pondrá en marcha a partir de la semana que viene.

Según pudo reconstruir Infobae, se discutieron estrategias y se escucharon las necesidades de las provincias. “El gobierno nacional va a apoyar para que se resuelvan los temas más importantes de las agendas locales. Estamos todos a disposición para dar vuelta la historia”, dijeron fuentes provinciales al tanto de las conversaciones.

Tras el revés en 18 de los 24 distritos, en el Gobierno se fijan como objetivo “no bajar de 30 puntos” en las generales, según detalló un asesor. “La elección está perdida, ahora solo hay algo de expectativa por los cambios en el Gabinete”, dijo un funcionario. En el Frente de Todos reina la cautela y nadie quiere hacer pronósticos positivos para los comicios del 14 de noviembre, pero la derrotada coalición oficialista ya puso los engranajes en marcha con el claro objetivo de mejorar en noviembre, aunque sea por algunos puntos.