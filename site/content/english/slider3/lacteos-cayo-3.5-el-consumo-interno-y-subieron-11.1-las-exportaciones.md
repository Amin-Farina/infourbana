+++
author = ""
date = 2021-11-04T03:00:00Z
description = ""
image = "/images/buenos-aires-argentina-recoleta-supermercado-tienda-de-comestibles-interior-baldas-venta-la-serenisima-productos-lacteos-leche-uht-ultra-alto-temperamento-madpef-1.jpg"
image_webp = "/images/buenos-aires-argentina-recoleta-supermercado-tienda-de-comestibles-interior-baldas-venta-la-serenisima-productos-lacteos-leche-uht-ultra-alto-temperamento-madpef.jpg"
title = "LÁCTEOS: CAYÓ 3.5% EL CONSUMO INTERNO Y SUBIERON 11.1% LAS EXPORTACIONES"

+++

##### **_Son cifras registradas entre enero y agosto de este año, comparadas con las de 2020, según el Observatorio de la Cámara Láctea. Desde la industria advierten que “se afectará producción si continúa el congelamiento de precios”._**

El Observatorio de la Cadena Láctea Argentina (OCLA) informó que entre enero y agosto se registró una caída promedio del 6,6% interanual en las ventas de productos del sector, y del 3,5% en litros de leche, en base a datos oficiales del Panel de Industrias Lácteas establecido por la Resolución 230/16 del Ministerio de Agricultura. Mientras que la última variación mensual registrada, para ventas internas, fue para agosto un 0,4% inferior a julio, y también cayó un 0,8% interanual.

Sin embargo, el declive en el mercado interno fue compensado por un repunte de las exportaciones en el mismo período, que fue del 11,1% en volumen y del 19,9% en el monto recaudado, al llegar a los US$ 807,4 millones. Si se toma el lapso de enero a septiembre, la suba en esas variables fueron del 8,4% y 17,8% (US$ 931 millones), respectivamente. En el caso de la situación en el frente externo, el análisis del OCLA fue en base a “datos provisorios” del INDEC.

En litros de leche equivalentes, las ventas al extranjero abarcaron el 23,9% de la producción total de enero a agosto, y del 24,1% a septiembre, un guarismo similar al año pasado en ese período. “Si extrapolamos el comportamiento a septiembre, al valor anual, se podría dar un nivel de alrededor de los 3.000 millones de litros de leche equivalentes, récord de exportaciones medidas bajo este concepto”.

Al analizar los destinos de los envíos al exterior, se dividieron entre Argelia (26%), Brasil (26%), Rusia (10%), Chile (8%) China (6%), y el restante 24% en países como Perú, Egipto, Indonesia y Uruguay, entre muchos otros. En la participación de cada producto, un 46,4% fue para la leche en polvo; 25,6% para los quesos en sus diferentes pastas; 18,8% en el resto de productos (dulce de leche, manteca, aceite butírico, suero, etc.); y 9,2% de productos confidenciales (lactosa, caseína, yogures, etc.).

No obstante, las exportaciones del 2021, aunque son superiores a las del período anterior, presentaron una importante oscilación mensual debido a varias circunstancias. “Iniciaron muy altas en enero debido a que se habían postergado operaciones a Brasil en diciembre del 2020, luego se normalizaron en febrero, y volvieron a crecer fuertemente en marzo debido a la mejora de precios y a partir de allí se dan algunos inconvenientes de logística interna (dificultades en los puertos y aspectos administrativos) y externa (disponibilidad de contenedores/barcos) que demoran las exportaciones y producen meses bajos y altos”.

##### Deterioro salarial y críticas al congelamiento de precios

En tanto, en su reporte sobre el ámbito doméstico, el OCLA cuestionó el congelamiento de precios dispuesto por el Gobierno, ya que “va a agudizar el proceso de menor recaudación de la cadena que seguramente desemboque, debido al menor incentivo, a una caída de la producción para el año próximo si no se revierten herramientas como estas que nunca surtieron el efecto buscado”.

Asimismo, puntualizó que las bajas en las ventas internas, de enero a agosto, se dieron por “un fuerte deterioro” de los niveles de ingresos reales, fundamentalmente de los segmentos medios de la pirámide, lo cual “al margen de reducir el volumen de consumo, afecta el valor del mix de ventas haciendo que se facture menos” en el mercado interno. “Claramente lo reflejamos en los análisis de la cadena de valor cuando mencionamos los bajos valores del VLE Mercado Interno en dólares respecto a otros países, que sería aún menor de no existir el fuerte retraso cambiario actual”, señalaron.

También pudo observarse una baja importante en leches fluidas y en polvo, pero un crecimiento en algunos productos que presentaron pronunciados descensos en 2020 como postres, leches saborizadas, y otros quesos. En tanto, yogures que habían recuperado algo de la caída del año anterior, volvieron a arrojar un guarismo negativo.

Además pudo verse un descenso generalizado en la comercialización de leches fluidas, y se registró un crecimiento en la participación de las no refrigeradas en detrimento de las refrigeradas. “La fuerte caída de este año es producto de comparar con una base alta del año pasado donde hay efectos del fenómeno pandemia/cuarentena. Una situación similar ocurre para las leches en polvo tanto enteras como descremadas”, puntualizó el OCLA.

Por otra parte, las leches saborizadas, o chocolatadas, presentaron un alza del 67,5% en los ocho primeros meses del 2021, por efecto de comparar con el año pasado, donde las ventas fueron “extremadamente bajas”, y se continúa así con la tendencia negativa de esta categoría.

En su informe, el OCLA analizó: “Es contundente que las ventas, y por ende el consumo, presentan en los últimos años una tendencia a la baja en general y una primarización del mismo (productos más básicos: leches fluidas no refrigeradas, quesos de pasta blanda y yogures bebibles de litro) en detrimento de aquellos de mayor valor agregado y por ende económico, que obviamente afectan el mix de ventas de la cadena de valor láctea”.

##### Diferencias estadísticas

Por último, el OCLA aclaró que la Resolución 230, de la Secretaría de Agricultura de la cual proviene gran parte de la información analizada, captura entre el 80 y 90% de las ventas totales, quedando fuera algunas que posiblemente puedan tener un comportamiento inverso al descripto. “Además, en escenarios como el actual donde hay un fuerte deterioro de los ingresos reales de la población, hacen que proliferen las ventas informales que obviamente ninguna estadística puede registrar, y han crecido fuertemente productos que por precio sustituyen el consumo de lácteos, como rayados, bebidas con lácteos, y otros similares”.

La entidad detalló que las diferencias que pueden observarse entre las estadísticas de ventas de la Resolución 230 (a la baja) y el consumo total que arroja el Balance Lácteo (a la suba entre 1 y 2%) puede deberse a diversos factores, como una sobrestimación de la producción, sobre todo en algunos rubros, como la masa para mozzarella, que fueron “fuertemente afectados” por el efecto de la cuarentena.

Asimismo, el OCLA mencionó que las disparidades estadísticas quizás se expliquen por un incremento más que significativo en las ventas de aquellas industrias que no releva la Resolución 230 (la misma abarca unas 46 empresas, entre el 75 y 80% de la producción total), y una subestimación del Balance Lácteo sobre el volumen exportado que obviamente incrementa el destino hacia consumo doméstico, ya que esa variable es la única que surge por diferencia respecto a las otras, porque el INDEC refleja las exportaciones efectivizadas y por cuestiones de las demoras logísticas muchos productos que se cuentan como exportados no son registrados en la estadística.