+++
author = ""
date = 2021-05-20T03:00:00Z
description = ""
image = "/images/33c913d07599027af5ad83db5bc8b202041-1621515245.jpg"
image_webp = "/images/33c913d07599027af5ad83db5bc8b202041-1621515245.jpg"
title = "LA VECINA DE VILLA LUGANO QUE FUE APUÑALADA POR DENUNCIAR A NARCOS, PIDIÓ A LA JUSTICIA QUE LA TRASLADEN DE BARRIO"

+++

Fabiana fue una de las pocas vecinas del Barrio Padre Mugica de Villa Lugano que se animó a denunciar a los narcos ante las cámaras, y horas después fue apuñalada en la cara. La sospecha de que los dos hechos estuvieran vinculados fue inevitable. Tras ser operada de urgencia en el hospital Santojanni, expresó: “Voy a pedir a la Justicia que me reubique”.

Para ella, el ataque del que fue víctima este miércoles tiene relación con el grupo de narcos que hasta hace algunos años manejaba el negocio de la venta de drogas en la zona y que, aprovechando la tensión de los últimas días, intenta ahora recuperar su lugar. “Fui la única que los enfrentó muchos años, con o sin policía”, remarcó Fabiana, pero llegó a su límite. “Voy a pedir que me saquen del barrio, tengo miedo”, dijo a TN, tras lo cual subrayó: “Sino que vengan a vivir ellos acá, pero que se pongan un chaleco antibalas para salir a comprar”.

De acuerdo a su relato, la custodia que tiene asignada justamente por su enfrentamiento con estos delincuentes no estaba ayer en el momento en que aparecieron y empezaron a molestar a sus hijas. Cuando ella quiso intervenir, una mujer a la que identificó como “Brenda” y acusó de “dealer”, la atacó. “Me tiró más puñaladas”, recordó la víctima y remarcó que si no hubiera sido por la ropa que llevaba puesta anoche, podría estar muerta. “Me tiró al pecho pero como tenía puesta una campera abrigada no llegó a lastimarme”, precisó.

Aún así, la agresora consiguió alcanzarla con el cuchillo en la cara en medio de un forcejeo y Fabiana fue llevada de urgencia al hospital Santojanni, donde fue operada. “Me asusté muchísimo, pensé que había perdido el ojo”, manifestó esta mañana.

Desde hace años, contó, vive en el barrio con sus cuatro hijas mujeres. Denunció a los grupos de narcotraficantes, incluso le usurparon la casa pero ella no se dejó intimidar. Volvió a denunciar y recuperó lo que era suyo. Sin embargo, convivir con el peligro tiene un precio y ya no está dispuesta a seguir exponiéndose, como lo hizo hasta ahora. “Mi hija de 11 años tiene un bloqueo emocional de tantos tiros que escuchó”, se lamentó, y añadió: “Mi otra hija tiene cinco y ya conoce todas las armas”.

#### Vivir con los narcos

Este martes, cansados de los narcos que coparon el barrio, los vecinos visibilizaron a través de videos la dramática situación que viven a diario. A raíz de la publicación de las imágenes, la policía entró el mismo día por la noche al Barrio Padre Mugica, que está controlado por los traficantes. Esto antes de haber evitado por la tarde que los habitantes del lugar instalaran una carpa sobre la colectora de la avenida General Paz a modo de protesta.

Los vecinos del mencionado barrio porteño denunciaron que hay una “zona liberada”, y que los narcotraficantes les tirotean sus viviendas si se resisten a cumplir sus órdenes.

De acuerdo a los testimonios recogidos por TN en el lugar, los narcos andan libres por la zona de los monoblocks y cuando alguno de ellos se atreve a denunciarlos les demuestran a los tiros el alcance de su fuerza.

Días atrás atacaron a balazos el frente de una peluquería como represalia contra sus dueños por pretender enfrentarlos. En la balacera hirieron al perro de la familia y solo el azar evitó que los chicos que estaban dentro del negocio en ese momento resultaran heridos también.