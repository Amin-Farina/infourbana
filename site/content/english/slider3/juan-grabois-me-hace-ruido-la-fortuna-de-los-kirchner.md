+++
author = ""
date = 2021-10-25T03:00:00Z
description = ""
image = "/images/u6how46vynd5pcbgmjhxu6q3dm.jpg"
image_webp = "/images/u6how46vynd5pcbgmjhxu6q3dm.jpg"
title = "JUAN GRABOIS: \"ME HACE RUIDO LA FORTUNA DE LOS KIRCHNER\""

+++

#### **_El dirigente social de la CTEP se mostró crítico con la familia de la funcionaria. Expresó su apoyo al congelamiento de precios y habló de que los políticos desoyen los reclamos de la gente._**

El dirigente social Juan Grabois se mostró crítico con los "malos hábitos" de la cultura política argentina, y dijo que le “hace ruido” el patrimonio y la fortuna de la vicepresidenta Cristina Kirchner. Además marcó diferencias con la familia de la funcionaria, la cual tiene "usos y costumbres de la burguesía argentina”, lo que genera un “divorcio entre la gente y los políticos”, indicó.

En declaraciones a Radio con Vos, el dirigente de la Confederación de Trabajadores de la Economía Popular (CTEP) se mostró a favor del congelamiento de precios como una medida “necesaria y útil”, para frenar un espiral "que tiene que ver con situaciones especulativas.

También habló sobre la fortuna que acumula la familia de Cristina Kirchner: “A mí me hace ruido, a la base no”, afirmó ante la consulta. En ese sentido, el dirigente social enfatizó la idea de que “la regeneración de la política implica necesariamente un voto de simplicidad de vida", cuestionando los vicios y malos hábitos de la clase política nacional. Por lo tanto Grabois consideró que la familia Kirchner tiene "usos y costumbres de la burguesía argentina".

Al respecto el referente de la CTEP aclaró: “No pienso que esté mal tener un patrimonio importante”. Citó los casos de políticos como Ángela Merkel y Joe Biden, los cuales son tomados como referentes en el círculo del Gobierno nacional, cuyos “patrimonios son superiores a los de Argentina y nadie los investiga demasiado", explicó Grabois.

Por eso destacó que este tipo de temas “son parte del divorcio de la agenda de los políticos con la gente”, marcando distancia con los referentes del Gobierno nacional. En la misma línea, también hizo referencia al conflicto con grupos mapuches en el Sur.

"Acá no hay nada raro, hay 40 pueblos originarios reconocidos, 1.600 comunidades registradas en la Inai y entre todos no tiene ni la mitad de la tierra que tiene los Benetton, los Lewis, los Lázaro Baéz. Quiero que se distribuya la tierra, no me importa en qué partido político este", sostuvo Grabois.