+++
author = ""
date = 2021-09-08T03:00:00Z
description = ""
image = "/images/7614w980h1800-1.jpg"
image_webp = "/images/7614w980h1800.jpg"
title = "SABINA FREDRIC DIJO QUE NO REFORZARÁ LA SEGURIDAD DE ROSARIO"

+++

**_La ministra explicó que van a “mejorar el despliegue” de las fuerzas y criticó al precandidato Diego Santilli, a cuatro días para las PASO._**

En pleno recrudecimiento de la violencia en Santa Fe por crímenes ligados al narcotráfico la ministra de Seguridad Sabina Frederic adelantó que no va enviar efectivos adicionales a esa provincia. Explicó que en cambio van a “mejorar el despliegue” de las fuerzas federales porque advirtió que “los recursos humanos son escasos”. Además cuestionó al precandidato a diputado nacional en provincia de Buenos Aires por Juntos, Diego Santilli. La funcionaria planteó que la tasa de homicidios en la ciudad de Buenos Aires “creció más que en Santa Fe”, en un dardo al exministro de Seguridad porteño a cuatro días para las PASO.

La funcionaria se refirió a la dura situación por la que atraviesa la provincia gobernada por Omar Perotti, donde solo en Rosario el martes hubo seis asesinatos en 20 horas. Consideró que “realmente es una situación grave, que se agravó en los últimos días”. Explicó que trabajan con la provincia para enfrentar esta problemática y mencionó que “con el despliegue en estos días de otros 160 efectivos se duplicó la cantidad de fuerzas federales que había, la mayor parte de las cuales están concentradas en Rosario”.

Frederic aclaró que no van a enviar más agentes sino que van a reorganizarlos: “Más que hacer crecer la cantidad de efectivos, que son 4000 prestando colaboración en la calle estamos trabajando para mejorar el despliegue. O sea optimizar los recursos humanos, que son escasos”. Explicó que entre las medidas dispuestas por la violencia en la provincia están la creación de una unidad ministerial, además del trabajo de prevención de las fuerzas federales junto con la policía de Santa Fe. Dijo que los agentes colaboran con el fortalecimiento del trabajo de investigación criminal, lo que aseguró que “venía demorado”.

Para la ministra “está claro que Rosario tiene un problema crónico que no pudo resolverse” aunque destacó que el gobernador Perotti “está poniendo mucha atención en el tema”. Advirtió que la ola de violencia por la que pasa el distrito presenta “algunos ribetes políticos que se han planteado en el pasado y eso puede estar incidiendo” y pidió que se impulse un “cambio estructural”: “No nos podemos permitir las oleadas de violencia que venimos atravesando desde el año 2012″.

Con críticas y chicanas a Santilli, Frederic criticó la gestión del exministro de Seguridad porteño a poco para las PASO. Consideró que el salto del exfuncionario a la provincia de Buenos Aires tiene que ver con “no hablar de sus desaciertos de gestión” en la ciudad de Buenos Aires, en declaraciones a radio 10.

La ministra dijo que el año pasado no aumentó la tasa de homicidios en la provincia de Buenos Aires y que en Capital Federal el índice creció 22%, “más que en Santa Fe”. Planteó con dardos a precandidato de Juntos: “Ellos no pueden hablar de la seguridad en la Ciudad porque el peor de los delitos creció de una manera exponencial”.