+++
author = ""
date = 2021-12-14T03:00:00Z
description = ""
image = "/images/kun-aguero-contener-llanto-anuncio-1.jpg"
image_webp = "/images/kun-aguero-contener-llanto-anuncio.jpg"
title = "EL \"KUN\" AGÜERO: \"HE DECIDIDO DEJAR EL FÚTBOL PROFESIONAL\""

+++

#### **_El delantero argentino, actualmente en el Barcelona, anunció este miércoles públicamente su retiro definitivo del fútbol profesional a causa de la afección cardíaca que padece._**

El argentino Sergio "Kun" Agüero, de 33 años, anunció este miércoles su retiro del fútbol, luego de 18 años de carrera y de vestir la camiseta de cuatro clubes y del seleccionado de la Argentina, a causa una arritmia cardíaca maligna que se le detectó en octubre pasado jugando en el Barcelona español.

"Esta conferencia de prensa es para comunicarles que dejó de jugar al futbol profesional. Es un momento muy duro, pero estoy muy feliz igual por la decisión que tomé, primero está mi salud", afirmó Agüero, sumamente emocionado y presa del llanto, acompañado por el presidente del club catalán Joan Laporta.

Agüero sufrió una arritmia cardíaca el pasado 30 de octubre en un partido de la Liga española contra el Alavés, donde debió se reemplazado a los 42 minutos. Luego de rigurosos estudios médicos se decidió que era aconsejable que dejara de competir a nivel profesional.

El exjugador de Independiente, Atlético de Madrid, Manchester City y Barcelona, agregó: "Ya saben que tomé la decisión por el problema que sufrí. Estuve en buenas manos de los médicos, que han hecho lo mejor y me dijeron que debía dejar de jugar, tomé la decisión hace 10 días o una semana".

Estuvieron presente en el acto desarrollado en el estadio Camp Nou parte del plantel del Barcelona, su entrenador en el Manchester City Josep Guardiola, el actual DT del equipo catalán Xavi Hernández y el consul argentino Alejandro Alonso Sainz, entre otras personalidades.

"Hice todo lo posible para saber si había una esperanza, pero no la hubo. Estoy orgulloso por mi carrera, feliz, siempre soñé de jugar al futbol, desde los 5 años que toqué una pelota ya quería jugar en primera, nunca soñé con llegar a Europa" dijo el ya exgoleador.

Agüero fue Campeón de América en Brasil con la casaca del seleccionado argentino en este mismo año, además de haber sido bicampeón mundial Sub-20 en Canadá 2005 y Holanda 2007 y Campeón Olímpico en Beijing 2008.

En este duro momento, se acordó de los clubes en los cuales brilló y se convirtió en uno de los goleadores más destacados del mundo y pretendido por clubes del máximo nivel.

"Le doy gracias a Independiente, en donde me formé, al Atlético de Madrid, que apostó por mi a los 18 años para jugar en Europa, a la gente del Manchester City, en donde dejé lo mejor, y al Barcelona y Joan Laporta, porque se contactaron conmigo y sabía que venía a uno de los mejores equipos del mundo. Gracias porque me trataron muy bien, y al seleccionado argentino que es lo que más amo", enfatizó el "Kun".

Enorme expectativa periodística provocó esta rueda de prensa de Agüero, que fue muy contenido por su familia, amigos y compañeros del Barcelona, en donde tenía contrato hasta 2023, en esta dolorosa circunstancia.

"Gracias a la gente que vino, a mi familia, a mis compañeros, creo que hice lo mejor para ayudar a ganar, me voy con la cabeza alta y muy feliz, no se que me esperara en la vida, tengo mucha gente que me quiere y deseo lo mejor" señaló quien debutó en la casaca de Independiente a los 15 años con Oscar Ruggeri como entrenador.

Tras la congoja el "Kun", quien deslizó que seguirá ligado de alguna forma al fútbol aunque no sabe cuando, se refirió con una sonrisa a la prensa: "le doy gracias también a los periodistas, a los que me trataron bien y a los que no me trataron bien, pero no pasa nada, hacen su trabajo".

Laporta, el presidente del Barcelona tomó la palabra luego el anunció de Agüero, y le comentó al exdelantero: "Has tomado la decisión correcta y tienes nuestro apoyo en lo que necesites en esta cuestión. Eres un jugador reconocido a nivel mundial, un goleador nato al que hubiéramos querido tener antes de que llegaras al Atlético de Madrid, eras la envidia de los "culés"... te deseamos lo mejor en tu nueva etapa, lo mereces".

En Barcelona Agüero solo pudo completar los 90 minutos ante Rayo Vallecano, producto de una lesión ni bien llegó a cataluña.

Desde entonces se ha sometido a un tratamiento para ver si podía volver a jugar, pero hoy finalmente el "Kun" le dijo adiós al fútbol tras 685 cotejos y 385 tantos a nivel clubes y 97 encuentros y 42 conquistas para el seleccionado nacional.