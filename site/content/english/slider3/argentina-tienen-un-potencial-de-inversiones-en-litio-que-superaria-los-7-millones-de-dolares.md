+++
author = ""
date = 2021-11-01T03:00:00Z
description = ""
image = "/images/614e54488be48-1.jpg"
image_webp = "/images/614e54488be48.jpg"
title = "ARGENTINA TIENEN UN POTENCIAL DE INVERSIONES EN LITIO QUE SUPERARÍA LOS 7 MILLONES DE DÓLARES "

+++

##### **_El país cuenta con 19 proyectos mineros para explotación del recurso en distintos grados de avance, de los cuales las dos operaciones que se encuentran operativas son Mina Fénix en Catamarca y Salar de Olaroz en Jujuy._**

Argentina tiene un potencial de inversiones en explotaciones mineras de litio de US$ 6.473 millones si se toman en cuenta los 19 proyectos mineros para explotación del recurso en distintos grados de avance, lo que le permitiría multiplicar por 10 su producción y alcanzar 373,5 mil toneladas partir de su capacidad actual de 37,5 mil toneladas.

Así se desprende de un informe de la Secretaría de Minería, en el que se relevó que actualmente Argentina cuenta con 19 proyectos mineros para explotación del recurso en distintos grados de avance, de los cuales las dos operaciones que se encuentran operativas, Mina Fénix en Catamarca y Salar de Olaroz en Jujuy.

Estos dos desarrollos cuentan con planes de expansión de sus capacidades de producción en 20 mil y 25 mil toneladas de carbonato de litio adicionales, respectivamente, en tanto que los que están en la etapa de construcción, sólo uno de los proyectos, Cauchari – Olaroz iniciará operaciones en 2022.

Así, siguiendo en orden de avance de los proyectos, 4 de ellos se encuentran en etapa de factibilidad, otros 3 en prefactibilidad, 6 en PEA y 7 en exploración avanzada.

Según lo informado por las distintas empresas controlantes de estos proyectos, Argentina cuenta con un potencial de inversiones en explotaciones mineras de litio de US$ 6.473 millones, si se contabiliza todo lo previsto hasta el momento.

Y en el caso de potenciar la exploración en el país, este número podría aumentar significativamente cuando los proyectos en exploración inicial empiecen a escalar en los distintos grados de avance.

Para la Secretaria de Energía que conduce Alberto Hensel, en el caso de computar tan sólo las cantidades de expansión, Argentina podría producir unas 45 mil toneladas adicionales en el futuro cercano.

Si a esto se suma el año próximo la puesta en marcha de Cauchari – Olaroz, se sumarían otras 40 mil toneladas adicionales, totalizando 85 mil toneladas.

Por otro lado, hay expectativa en el sector a las novedades del proyecto Centenario Ratones, que se encontraba en etapa de construcción y, ante las complicaciones surgidas por el año de pandemia, ha disminuido su grado de avance a factibilidad.

Pero en caso de volver a reactivar su construcción, se contarían con 24 mil toneladas LCE adicionales, totalizando 109 mil toneladas de producción.

En caso de ponerse en funcionamiento en los próximos años el resto de los proyectos que hoy tienen menores grados de certidumbre, Argentina podría totalizar una producción de 373,5 mil toneladas adicionales a su capacidad actual de 37,5 mil toneladas.

De esta manera, para la cartera energética el país contaría con un enorme potencial que le permitiría mantenerse entre los primeros productores a nivel global, e incluso mejorar la posición que tiene en la actualidad de cara a los aumentos esperados en la demanda futura.

Un punto a tener en cuenta en cuanto al análisis de la actividad, es el empleo generado en el país: para el mes de marzo del año 2021, el sector alcanzó los 1.581 empleados directos.

Esto refleja que el sector no disminuyó significativamente su planta de empleados en el contexto de pandemia, y que a inicios del 2021 la cantidad de empleados se encontraba próxima a los valores prepandemia cuando alcanzó un pico histórico de 1.606 trabajadores.

El tipo de empleo que se desarrolla en esta actividad son puestos que se generan en zonas alejadas de las grandes urbes y con poca oferta laboral y que los sueldos se suelen ubicar muy por encima de los promedios provinciales donde se desarrolla la actividad.

Las previsiones de la demanda agregada de litio indican que de las actuales 429 toneladas previstas para este 2021, alcance para 2030 los 1,793 millones de toneladas de carbonato, de las cuales aproximadamente el 78%, será para abastecer a los vehículos eléctricos.

El crecimiento exponencial registrado en las perspectivas a futuro para el mercado del litio, vinculado a una mayor demanda de vehículos eléctricos, está explicado por las cantidades del mineral requerido para la fabricación de las baterías para cada tipo de transporte.

En lo que respecta a producción global, Argentina se posiciona en cuarto lugar en la producción del litio con el 7,4% del market share en el año 2019, después de Australia (52,2%), Chile (22,4%) y China (12,5%).

Estos valores mostraron variaciones durante el año 2020 y, aunque mantuvieron las posiciones del año previo, la participación de China aumentó a un 17% absorbiendo parte de la representación del mercado de Australia y Chile.

Focalizando en el triángulo del litio, el mismo contiene cerca del 65% de los recursos mundiales de litio y, mediante la suma de la producción entre Argentina y Chile pueden explicar el 29,9% de la producción mundial total para el año 2019.

Este valor mostró una leve caída durante el 2020 pasando a ubicarse en el 29,5%, y se destaca que el triángulo cuenta solamente con la producción comercial de Argentina y Chile, dado que Bolivia se encuentra en producción a escala piloto.

En este proceso de desarrollo, la Argentina tuvo un incremento en la producción de litio del 72,2% entre 2015 y 2020, mientras que Chile aumentó en 71,4%.

En lo que respecta al tipo de producción, el país actualmente produce dos variantes de Litio, el cloruro y el carbonato, y el destino de estos compuestos es el mercado externo casi en su totalidad.

Ahora bien, cada uno de ellos ha mostrado una evolución particular en cuanto a su producción en el país, y es así como el cloruro de litio fue el protagonista en la década de 1990, ya que su principal uso es industrial en las aplicaciones más tradicionales.

En tanto que los carbonatos comenzaron a tomar relevancia a mediados de la primera década del 2000, con la masificación en el uso de dispositivos electrónicos y el auge de los autos eléctricos.