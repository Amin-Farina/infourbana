+++
author = ""
date = 2021-04-21T03:00:00Z
description = ""
image = "/images/phishing-2-kk0b-u1208951377029gg-624x385-el-correo.jpg"
image_webp = "/images/phishing-2-kk0b-u1208951377029gg-624x385-el-correo.jpg"
title = "AUMENTARON LAS ESTAFAS VIRTUALES DURANTE LA PANDEMIA"

+++

**_Los fraudes suelen ocurrir en los canales virtuales, con pedidos de pagos extra argumentando “diferencia en el valor declarado” por compras en el exterior. Se roban datos de tarjetas y cuentas._**

La Defensoría del Pueblo bonaerense advirtió sobre el aumento de estafas virtuales instrumentadas a través de mails y mensajes de Whatsapp, y elaboró una guía práctica para evitar el "Phishing", que consiste en la suplantación de identidad por parte de ciberdelincuentes para robar dinero a sus víctimas.

En la modalidad denunciada también por el Correo Argentino, en los mails y Whatsapp se pide un pago extra por “diferencia en el valor declarado” para la entrega de productos procedentes del exterior.

#### El ciberdelito creció un 60% promedio

“El año pasado quedó demostrado que una de las situaciones derivadas de los mayores niveles de conectividad, producidos por la pandemia, es la proliferación de ciberdelitos", dijo el Defensor del Pueblo Adjunto de la provincia de Buenos Aires, Walter Martello, quien advirtió que algunos registros, como los informados por la Asociación Argentina de Lucha contra el Cibercrimen, "hablan de un incremento promedio del 60%".

Por eso, precisó que "resulta trascendente reforzar la prevención ante esta nueva ola de Covid-19 y la imperiosa necesidad de que las familias se queden en sus casas”.

Según la Defensoría, otro dato a tener en cuenta es que "el 81% de las plataformas de ventas por internet sufrieron maniobras vinculadas a la comisión de delitos informáticos".

#### Más fiscalías especializadas

Martello consideró que “se debería avanzar en la creación de más fiscalías especializadas en delitos informáticos, para poder dar respuesta a las crecientes demandas que se vienen registrando a lo largo y ancho de la Provincia”.

Los casos como los denunciados por Correo Argentino que presentan las características del “Phishing” o suplantación de identidad, se caracterizan en una comunicación que presenta "elementos que tratan de imitar una comunicación oficial reproduciendo nombre, logo, uso del nombre @correoargentino.com.ar, cuyo objetivo es generar en la víctima la impresión que la comunicación procede de una fuente real y legítima".

#### El elemento engañoso de la verosimilitud

También, señaló que estos comunicados plantean un “problema” con cierta verosimilitud, "es creíble, refiere a alguna circunstancia que puede pasar", aconsejan y tienen "pedido de dinero que implica un monto no demasiado excesivo".

El organismo explicó que en los mismos, "se solicita dinero por un problema menor que rápidamente se solucionará a través del pago de la suma requerida" y se "indica que el pago debe ser efectuado en un tiempo perentorio generando así en la víctima la carga de actuar rápidamente".

Por último, desde la Defensoría precisaron que "se indica un CBU en una entidad bancaria reconocida incrementando la verosimilitud del requerimiento".