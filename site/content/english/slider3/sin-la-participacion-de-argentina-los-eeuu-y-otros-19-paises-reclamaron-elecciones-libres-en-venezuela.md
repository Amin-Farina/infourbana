+++
author = ""
date = 2022-02-17T03:00:00Z
description = ""
image = "/images/protesta-12-1.jpg"
image_webp = "/images/protesta-12.jpg"
title = "SIN LA PARTICIPACIÓN DE ARGENTINA, LOS EEUU Y OTROS 19 PAÍSES RECLAMARON ELECCIONES LIBRES EN VENEZUELA"

+++

##### **_Sin la participación de la Cancillería Argentina, los Estados Unidos junto a otros 19 países y la Unión Europea (UE) mantuvieron una reunión en la que reclamaron que se “restaure la democracia” en Venezuela con elecciones “libres” y se excarcele a los presos políticos, informó este miércoles el Departamento de Estado estadounidense_**

En la Reunión de Coordinación de Alto Nivel, los participantes reiteraron la necesidad de “una solución negociada liderada por Venezuela para restaurar la democracia” en el país e insistieron en la importancia del marco de diálogo lanzado el pasado 13 de agosto en Ciudad de México, añade en un comunicado.

Según informó el Departamento de Estado estadounidense en un comunicado, llamaron a reanudar “urgentemente las negociaciones inclusivas en México de buena fe”, como contempla el Memorando de Entendimiento firmado en esa ciudad, y piden un acuerdo sobre un organismo electoral “independiente e imparcial, con máxima autoridad” que ejerza como garante de un proceso electoral a más tardar en 2024, cuando están previstos los comicios presidenciales.

A principios de febrero, Argentina se negó a suscribir un pedido del Grupo de Contacto para que el régimen de Maduro y la oposición venezolana retomen el diálogo.

Las negociaciones entre el gobierno y la oposición en México, lanzadas bajo el auspicio de Noruega, fueron suspendidas en octubre por el chavismo en rechazo a la extradición a Estados Unidos del empresario colombiano y aliado clave del presidente Nicolás Maduro Alex Saab, considerado un testaferro del mandatario.

Los países participantes en la reunión de alto nivel se declaran dispuestos a revisar las sanciones si se constata “un progreso significativo” en las negociaciones y se comprometen a tratar “la grave situación humanitaria” del país.

El fin de las sanciones económicas internacionales, que le han cortado al régimen vías de financiamiento, es una de las principales peticiones del chavismo.

Según la ONU, más de 6 millones de venezolanos han dejado su país en los últimos años huyendo de una grave crisis, agudizada tras la llegada al poder de Maduro en 2013.

La reunión de alto nivel abordó asimismo la necesidad de elecciones presidenciales “libres y justas, la liberación inmediata e incondicional de todos los detenidos arbitrariamente”, la independencia del poder judicial y de las autoridades electorales, el derecho de los partidos a participar en el proceso político sin restricciones y a la libertad de expresión y “el fin de las violaciones de los derechos humanos” en Venezuela.

Washington no reconoce la reelección en 2018 de Maduro y considera presidente al líder opositor Juan Guaidó. A su vez, la Unión Europea, aunque no le da el título de presidente, considera a Guaidó el único interlocutor válido en Venezuela, pero conserva canales abiertos con el gobierno de Maduro.

En la reunión participaron Estados Unidos, Brasil, Chile, Colombia, Costa Rica, República Dominicana, Ecuador, Panamá, Paraguay, España, Australia, Canadá, la Unión Europea, Francia, Alemania, Italia, Japón, Nueva Zelanda, Portugal, Suecia y Reino Unido.