+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/un6ilare3zcw3fpoh7unx7vob4.jpeg"
image_webp = "/images/un6ilare3zcw3fpoh7unx7vob4.jpeg"
title = "ALBERTO FERNÁNDEZ ADELANTÓ QUE CONTINUARÁN LAS RESTRICCIONES"

+++

**_El Presidente les reclamó a gobernadores y municipios que refuercen los controles para reducir la circulación de personas y frenar así los contagios de coronavirus._**

“En estos términos debemos seguir con las restricciones”. Con esa frase el presidente Alberto Fernández confirmó que continuarán las medidas para intentar reducir la circulación de personas ante el avance de la segunda ola de coronavirus.

El mandatario descartó este martes volver a una fase 1 en el Área Metropolitana de Buenos Aires (AMBA) y el resto del país al asegurar que la sociedad “no resistiría” una medida de esta clase. Sin embargo le pidió a los gobernadores e intendentes que refuercen los controles para reducir la cantidad de personas en la calle.

“No una fase 1 no, no la resistimos. Tenemos que pedirle a todas las jurisdicciones que vuelvan a poner todos los controles. En esas cosas debemos volver al punto de inicio”, remarcó el jefe de Estado en diálogo con Radio 10.

En este sentido, al ser consultado sobre el posible cambio de postura de Horacio Rodríguez Larreta, que estaría pensando en endurecer las restricciones en la Ciudad, Fernández respondió: “Lamento el tiempo que han perdido porque definitivamente soy respetuoso y lo último que hubiera querido era restringir la circulación pero no hay otro modo de resolver este problema, vengo de Europa, en París desde las 19 no encontré un bar para comprar un café”.

Y continuó: “Pido que se hagan cargo porque esta mayor cantidad de casos es el resultado de decir que la vida siga sin que pase nada. Es un virus que nos contagia y nos mata. Nos tiene que doler el último número de personas que mueren por día, no nos estamos cuidando y todos estamos cansados. Tenemos que dejar de hacer política con la pandemia”.

El Presidente contó que cuando tomó la determinación de suspender las clases presenciales y establecer restricciones a la circulación a partir de las 20 fue luego de que un grupo de infectólogos le dijeran que de mantener el ritmo de positivos el país iba a llegar a un pico de 40.000 casos diarios.

El Gobierno nacional observa con preocupación el incremento de casos de coronavirus en la Argentina y entiende que las restricciones adoptadas hasta el momento evitaron un colapso del sistema sanitario, pero no lograron bajar la curva de positivos.

Las últimas cifras así lo indican: el lunes se registraron 505 muertes y 28.680 contagios, un número que trajo más que incertidumbre en la Casa Rosada que analiza aplicar restricciones más duras a partir del 21 de mayo, fecha en la que vence el actual DNU firmado por Alberto Fernández.

Tal como adelantó TN.com.ar el lunes, el Gobierno analiza adelantar el horario de cierre y prohibición de circulación y achicar el listado de actividades permitidas para intentar contener los contagios de coronavirus. La idea es replicar el modelo de restricciones de París que logró frenar la curva de casos. Durante su gira a Europa el jefe de Estado observó las calles vacías a partir de las 19:00 y ese podría ser el nuevo límite que comience a regir desde este fin de semana.