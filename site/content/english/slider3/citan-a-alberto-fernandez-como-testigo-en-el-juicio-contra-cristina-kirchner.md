+++
author = ""
date = 2021-06-23T03:00:00Z
description = ""
image = "/images/la-formula-de-unidad-ciudadana___zedzjfedq_1256x620__1.jpg"
image_webp = "/images/la-formula-de-unidad-ciudadana___zedzjfedq_1256x620__1.jpg"
title = "CITAN A ALBERTO FERNÁNDEZ COMO TESTIGO EN EL JUICIO CONTRA CRISTINA KIRCHNER"

+++

**_Al igual que Sergio Massa y otros ex jefes de Gabinete, Alberto Fernández declarará en los tribunales en agosto sobre el presunto fraude en la obra pública durante su gestión de la actual Vicepresidenta._**

El presidente Alberto Fernández y el titular de la Cámara de Diputados, Sergio Massa, serán convocados para agosto para declarar como testigos en el juicio que se le sigue a la vicepresidenta Cristina Fernández de Kirchner por presunto fraude en la obra pública durante su gestión.

Así lo informó el Tribunal Oral Federal 2 a las partes, al tiempo que indicó que próximamente empezará a convocar a los ex funcionarios que ocuparon la Jefatura de Gabinete durante la gestión de la ex mandataria, entre ellos Fernández y Massa, quienes se estima que sean citados para agosto.

La lista de testigos fue confeccionada al inicio del debate pero la novedad es que también se los citará a declarar en el juicio que se sigue a Lázaro Báez, por la sospecha de la Justicia de que el empresario fue beneficiado con contratos de obra pública vial por la ex Presidenta. Se trata de la causa conocida como "Vialidad", sobre el supuesto direccionamiento de contratos a Austral Construcciones, incluso antes, en la desde la gestión del fallecido Néstor Kirchner.

De esta manera, en el bloque de ex jefes de Gabinete que serán citados como testigos también serán convocados -como se determinó al inicio del juicio- Aníbal Fernández, Juan Manuel Abal Medina y Jorge Capitanich, actual gobernador de Chaco.

Tanto Fernández como Massa y Capitanich, actual gobernador de Chaco, podrán elegir por sus funciones si declaran por escrito u optan por presentarse en juicio. Para ello el Tribunal informó a las partes que vayan preparando las preguntas, en caso de que tengan que enviar un pliego si optan por declarar por escrito y en lugar de hacerlo de manera presencial.

En tanto, Aníbal Fernández y Abal Medina sí deberán presentarse en la fecha que el TOF 2 fijará en los próximos días para escuchar a los ex jefes de Gabinete.

Hasta el momento declararon como testigos ex funcionarios de Vialidad Nacional, de Vialidad de Santa Cruz, de la Subsecretaria de Obras Públicas y ahora lo están haciendo integrantes de la AFIP.

Luego también será convocado Carlos Zannini, ex secretario de Legal y Técnica del cristinismo y actual procurador del Tesoro de la Nación.