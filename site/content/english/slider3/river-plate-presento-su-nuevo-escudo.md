+++
author = ""
date = 2022-02-22T03:00:00Z
description = ""
image = "/images/escudo-river_w862.jpg"
image_webp = "/images/escudo-river_w862-1.jpg"
title = "RIVER PLATE PRESENTÓ SU NUEVO ESCUDO"

+++

#### **_Hace instantes, el Millonario dio a conocer algunas modificaciones leves en el diseño como parte de un proyecto integral de marketing._**

Lo que era un secreto a voces, finalmente terminó confirmándose este martes: River renovará su escudo. Como parte un plan integral de marketing que inició la dirigencia encabezada por Jorge Brito, el conjunto de Núñez presentó esta mañana el nuevo diseño que identificará al club.

La finalidad de esto está relacionado mayoritariamente con una cuestión de instalar la "marca" del club comercialmente, ya que en términos concretos las reformas en el escudo serán muy sutiles. De acuerdo a las imágenes y videos que mostró River, los cambios serán menores, como por ejemplo que ya no tendrá la líneas negras del interior, incluidas las que rodeaban a las iniciales CARP .

La idea es que se unifique su utilización para todas las competiciones y así se empiece a lucir en la camiseta del equipo de Marcelo Gallardo, algo que en principio sucedería a partir de agosto, cuando se modifique el modelo actual.

##### Jorge Brito explicó por qué se cambió el escudo de River 

Jorge Brito, presidente del club, dialogó con la prensa y explicó los motivos que llevaron a tomar esta decisión. "La idea es darle un poco más de modernidad y hacer un escudo más limpio, en el cual se le de más relevancia a la banda roja. Y también a ponerle estándares, porque veíamos en distintos lugar que había logos que no tenían exactitudes".