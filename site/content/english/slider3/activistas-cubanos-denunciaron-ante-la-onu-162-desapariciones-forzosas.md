+++
author = ""
date = 2021-07-14T03:00:00Z
description = ""
image = "/images/7c6jlhw7hbcmfdwwyo33syz6iu.jpg"
image_webp = "/images/7c6jlhw7hbcmfdwwyo33syz6iu.jpg"
title = "ACTIVISTAS CUBANOS DENUNCIARON ANTE LA ONU 162 DESAPARICIONES FORZOSAS"

+++

**_Activistas cubanos informaron este miércoles que presentaron una denuncia ante la Organización de Naciones Unidas (ONU) que incluye una lista “parcial” de 162 “potenciales desapariciones forzosas”, en el marco de la represión del régimen castrista contra las movilizaciones que se han registrado en Cuba desde el fin de semana._**

“Acabamos de poner una denuncia ante el Alto Comisionado de Naciones Unidas enumerando una lista parcial de 162 potenciales desapariciones forzosas”, señaló la organización disidente Prisoners Defenders, que también indicó las “innumerables limitaciones debidas al apagón de comunicaciones gubernamental. Cada una debe ser explicada”.

El presidente de la ONG, Javier Larrondo, calificó durante la jornada de este miércoles de “máxima urgencia” la situación que atraviesa Cuba, donde se cometen “crímenes de lesa humanidad”.

“En este momento estamos en una urgencia absoluta y necesitamos que el Gobierno, la Unión Europea y Naciones Unidas hagan declaraciones. No de la definición de lo que es Cuba o no, sino de lo que necesita el pueblo cubano para ser protegido”, subrayó Larrondo en declaraciones a la agencia Europa Press.

Ciudadanos cubanos han salido a las calles en los últimos días para expresar su descontento por la escasez de productos básicos, como alimentos y medicinas, y miembros de la comunidad internacional y ONGs, entre otros, han denunciado una dura represión contra los manifestantes.

El régimen cubano consideró el martes que ha visto “escenas peores” de represión y violencia policial en Europa y ha negado un “estallido social”.

La conexión a internet móvil en Cuba sigue cortada tres días después de las protestas, aunque una minoría ha recuperado el servicio de datos y algunos jóvenes están logrando acceder a la red con la ayuda de plataformas VPN e ingeniosos trucos.

Hasta este miércoles, la mayoría de los cubanos seguía sin acceso a internet en sus celulares, lo que en la práctica supone un apagón casi total, ya que en la isla son una pequeña minoría los hogares que se pueden permitir una conexión wi-fi.

Ante esto, ciudadanos -sobre todo jóvenes- de todo el país recurren a servicios de VPN -como Psiphon o Thunder- y trucos para burlar la censura y acceder a las redes de datos móviles 3G y 4G, controladas por el monopolio estatal de telecomunicaciones Etecsa.

“Hay que activar los datos y luego la VPN, y ponerla en la región de Estados Unidos. Después poner el teléfono en modo avión por 5 segundos y al quitarlo se conecta”, explicó a la agencia EFE en La Habana una mujer de 26 años que logró acceder a internet este miércoles tras permanecer desconectada por dos días y medio.

También se han reportado casos excepcionales de cubanos que han recuperado de forma intermitente la conexión sin ayuda de plataformas VPN, aunque no podían acceder a algunas aplicaciones como WhatsApp.

Las redes wi-fi privadas y en espacios públicos no dejaron de funcionar en Cuba, aunque con restricciones intermitentes de WhatsApp.

El servicio de internet móvil quedó deshabilitado el domingo al extenderse las protestas de cubanos por todo el país, alentados por un video en el que vecinos de San Antonio de los Baños (30 km al este de La Habana) se lanzaban a las calles para protestar por la falta de alimentos y medicinas y los cortes de luz, en medio de una grave crisis económica y sanitaria.

Expertos creen que el régimen cortó internet para evitar que esto se repita, aunque también consideran que la medida podría ser contraproducente al aumentar el descontento de la población con las autoridades.

De hecho, el corte de datos ha interrumpido la rutina de parte de los trabajadores del país, ya que el trabajo a distancia se ha generalizado durante la pandemia en algunos sectores como el de la educación, donde se eliminaron las clases presenciales.

Además, muchos lamentan llevar días sin comunicarse con sus familiares en el extranjero, ya que internet es el modo más habitual en el que los cubanos de dentro de la isla mantienen el contacto con la diáspora.

Etecsa no ha dado ninguna explicación del apagón y tampoco lo había hecho la dictadura hasta que el martes el canciller, Bruno Rodríguez, lo asimiló a “las interrupciones de la electricidad” y las dificultades en la alimentación o el transporte.

“Es verdad que faltan datos pero faltan medicamentos también”, expuso el ministro de Exteriores, sin reconocer explícitamente la responsabilidad del Gobierno.

Las manifestaciones ciudadanas iniciadas el domingo en toda Cuba han sido las más importantes en 60 años, con el único precedente del “maleconazo” de agosto de 1994, limitado a La Habana.