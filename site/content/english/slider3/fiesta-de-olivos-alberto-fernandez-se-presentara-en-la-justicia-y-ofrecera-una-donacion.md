+++
author = ""
date = 2021-08-24T03:00:00Z
description = ""
image = "/images/5e85ccc794584_1004x565-1.jpg"
image_webp = "/images/5e85ccc794584_1004x565.jpg"
title = "FIESTA DE OLIVOS: ALBERTO FERNÁNDEZ SE PRESENTARÁ EN LA JUSTICIA Y OFRECERÁ UNA DONACIÓN"

+++

**_El presidente Alberto Fernández y su abogado Gregorio Dalbón preparan la estrategia judicial en el marco de la causa que investiga los ingresos a la Quinta de Olivos y la celebración de cumpleaños VIP de Fabiola Yañez en medio de la cuarentena estricta._**

La defensa del Jefe de Estado sostiene que lo ocurrido en la celebración íntima de la Primera Dama no se configura en un delito penal ya que “no se puso en riesgo la salud pública al no haberse detectado contagios”.

> En este sentido, Dalbón negó que el Presidente vaya a ir a la Justicia a proponer un acuerdo. “No lo necesitamos porque no hubo delito”, le dice a este medio y agrega: “La mera infracción administrativa (el haber violado el propio DNU dictado por el Gobierno), no es un delito. Para que lo haya debe haber una infracción a la normativa y además una lesión, una puesta en peligro del bien que se quiere proteger, en este caso la salud pública, lo que no ocurrió”.

Sin embargo, el letrado que también integra el equipo jurídico de abogados de la Vicepresidenta Cristina Kirchner, adelantó que Alberto Fernández tiene pensando presentarse ante la Justicia y proponerle al fiscal Ramiro González realizar una donación a modo de reparación a una entidad de bien público. Aún no hay fecha para que esto ocurra.

Aunque Dalbón niegue que se trate de un acuerdo, la reparación podría servir para extinguir la acción penal.

Podría ser al Instituto Malbrán”, dice Dalbón, que también es el defensor del Presidente en el caso contra Patricia Bullrich por sus dichos sobre supuestos pedidos de coimas a Pfizer. “Por una cuestión ética y moral quiere presentarse a la Justicia y hacerse cargo de la situación en Olivos”, dijo el abogado.

Según pudo saber este medio, el dinero a donar podría depositarse en una cuenta a nombre de la Justicia o entregarse directamente el Instituto Malbrán. Luego se adjuntará el comprobante para que el juez y el fiscal tomen nota de la entrega de los fondos, que podría ser el equivalente a un sueldo o medio salario.

Más allá de la posible “donación-multa” del Presidente, en su entorno entienden que el caso no representará un dolor de cabeza jurídico. De hecho, un antecedente judicial podría beneficiarlo directamente. Un mes antes, el exdiputado nacional Facundo Moyano fue denunciado por hacer una reunión en su departamento de Puerto Madero. El fiscal Ramiro González desestimó la causa porque entendió que “la insignificancia del comportamiento denunciado no afecta gravemente al interés público”.

Para la defensa de Fernández el fiscal no tiene otra opción que seguir estos mismos pasos. “Si ahora González considera que hubo delito, plantearemos que ha pensando de una manera en una causa y de otra muy distinta en este expediente. Estaría al borde de dictar resoluciones contrarias a derecho”, señaló Dalbón.

El lunes el Gobierno entregó la lista de ingresos y egresos de la Quinta Presidencial de Olivos en el marco del requerimiento que le realizó a la Casa Militar el fiscal Ramiro González, que investiga la entrada de personas a esa residencia durante la cuarentena estricta en 2020, es decir, cuando regían fuertes restricciones a la circulación. El pedido fue por los datos de ingresos y salidas de los invitados al cumpleaños de la primera dama Fabiola Yañez, el año pasado durante la cuarentena dura por coronavirus.

No era la única prueba que González esperaba conocer por estos días: también aguarda por las planillas y bases de datos de la aplicación Cuidar para verificar si los invitados al cumpleaños VIP de Fabiola Yáñez tenían permisos de circulación. Por aquellos días de julio de 2020 solo podían hacerlo los esenciales.

Ahora el fiscal podrá analizar la documentación que envió el Gobierno y determinar posibles imputación. Algunos de los apellidos que figurarán en esos listados son los de Sofía Pachi, asesora de Fabiola Yañez, el taiwanés Chien Chia Hong, Emmanuel López, amigo y colaborador de la Primera Dama, Fernando Consagra, Ariel Alonso Zapata, Lautaro Romero y Florencia Peña. Todos correspondientes al 2020. Luego al difundirse las imágenes del cumpleaños de Yañez el fiscal amplió esa solicitud para todos los invitados. Hay unas 10 personas en la mira.