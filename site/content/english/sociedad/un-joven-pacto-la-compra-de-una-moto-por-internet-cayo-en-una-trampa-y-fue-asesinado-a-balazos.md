+++
author = ""
date = 2021-12-02T03:00:00Z
description = ""
image = "/images/imuvbhn6jrek5g2vfmm5snefbm.jpg"
image_webp = "/images/imuvbhn6jrek5g2vfmm5snefbm.jpg"
title = "UN JOVEN PACTÓ LA COMPRA DE UNA MOTO POR INTERNET, CAYÓ EN UNA TRAMPA Y FUE ASESINADO A BALAZOS"

+++

### **_El hecho ocurrió en Florencio Varela. La víctima fue identificada como Sebastián Rodríguez y tenía 26 años_**

Otro caso de extrema violencia se registró en las últimas 24 horas en Florencio Varela, al sur del Conurbano. Un joven de 26 años pactó por Facebook la compra de una moto, fue engañado y terminó asesinado.

La víctima fue identificada como Sebastián Rodríguez, papá de un nene de cuatro años. Alrededor de las 19:15 junto a un amigo se dirigió hasta el cruce de 414 y La Cautiva de la localidad de Gobernador Costa para realizar la transacción con el supuesto vendedor.

##### ¿Cómo fue el asesinato de Sebastián Rodríguez?

Una vez que llegó al lugar se bajó del auto y su amigo quedó en el interior. Se encontró con el hombre que resultó ser un delincuente y le exigió la entrega de sus pertenencias.

Según informó Télam, Rodríguez regresó al auto para darle su celular, aunque quiso simular que se lo daba adentro de una bolsa que en realidad tenía unas toallitas femeninas.

“¿Qué te hacés el gil? A mí no me vas a cagar”, lo amenazó el asaltante, quien efectuó tres disparos, de los cuales dos impactaron en el abdomen y en la espalda de la víctima.

Tras el ataque, el delincuente fue hasta el auto en el que estaba el amigo del joven, robó el celular que pertenecía a Rodríguez y escapó.

La víctima fue trasladada por su amigo a un hospital de la zona, adonde llegó muerto a raíz de las lesiones. El hecho es investigado por la fiscal María Nuria Gutiérrez, a cargo de la Unidad Funcional de Instrucción (UFI) 4 descentralizada en Florencio Varela, Departamento Judicial Quilmes.