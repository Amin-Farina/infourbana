+++
author = ""
date = 2021-02-17T03:00:00Z
description = ""
image = "/images/vuelta-a-clases-2021.jpg"
image_webp = "/images/vuelta-a-clases-2021.jpg"
title = "Luego de casi 1 año vuelven las clases presenciales en cuatro distritos"

+++

**_La Ciudad de Buenos Aires, Santa Fe, Santiago de Estero y Jujuy retomaron las clases presenciales en algunos niveles, mientras la provincia de Buenos Aires abrió las puertas de las escuelas a los alumnos que tuvieron menos contacto con los docentes el año pasado para reforzar aprendizajes._**

El ciclo lectivo 2021 comenzó a desarrollarse este miércoles en la Argentina con un esquema de presencialidad cuidada y de manera progresiva por la pandemia de coronavirus, con clases presenciales en algunos niveles de la ciudad de Buenos Aires, Santa Fe, Santiago del Estero y Jujuy, mientras la provincia de Buenos Aires abrió las puertas de las escuelas a los alumnos que tuvieron menos contacto con los docentes el año pasado para reforzar aprendizajes.

En jurisdicción porteña, las puertas de las escuelas abrieron para los chicos y chicas desde los 45 días hasta los 5 años, los primeros tres grados del nivel primario y educación especial, y 1º y 2º año de los colegios secundarios; mientras en Santa Fe será el turno de séptimo grado, quinto año de secundaria y sexto de las escuelas técnicas.

En tanto, en Santiago del Estero lo harán los alumnos del último año de primaria y el último de secundaria y en Jujuy los estudiantes de nivel inicial y primaria.

En tanto, a partir del 21 de febrero volverán las clases presenciales para los alumnos secundarios y de escuelas técnicas de Jujuy.

El 1 de marzo será el turno del regreso a las aulas en las provincias de Buenos Aires, Salta, Tucumán, Catamarca, San Juan, San Luis, Mendoza, Santa Cruz, Entre Ríos y Córdoba.

También el 1 de marzo abrirán los establecimientos educativos en Chubut, Chaco, Corrientes y Tierra del Fuego; el 2 de marzo será el turno de Formosa, el 3 de Neuquén y Río Negro, el 8 de La Pampa y La Rioja, el 9 de Misiones y el 15 de marzo completarán los ciclos lectivos Santa Fe y Santiago del Estero.

La decisión final del retorno cuidado a las clases presenciales, en forma alternada con la enseñanza virtual, fue tomada durante la reunión del Consejo Federal de Educación -con los ministros de las 24 jurisdicciones del país- realizada el vienes pasado en la Residencia de Olivos, y tuvo el aval del presidente Alberto Fernández.

"La escuela que teníamos en marzo pasado, no está disponible ahora", dijo el fin de semana a Télam el ministro de Educación, Nicolás Trotta, quien detalló que "seguramente habrá clases presenciales tres veces a la semana, mientras que las escuelas de jornada completa se transformarán en dos escuelas de jornada simple", para cumplir con los protocolos de distanciamiento.

A partir del 15 de marzo, volverán todos los alumnos, docentes y no docentes, en forma alternada según lo disponga cada escuela, lo que implicará una movilización de 15 millones de personas en todo el país, de acuerdo a estimaciones de Educación.

La ciudad de Buenos Aires, con una matrícula de 720.000 alumnos y unos 82.000 docentes y no docentes, comenzó esta mañana a transitar el ciclo lectivo 2021 aunque la Unión de Trabajadores de la Educación (UTE), el gremio docente mayoritario, y Ademys, rechazaron los protocolos.