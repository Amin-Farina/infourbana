+++
author = ""
date = 2021-12-02T03:00:00Z
description = ""
image = "/images/505442_4315-1.jpg"
image_webp = "/images/505442_4315.jpg"
title = "UN GRUPO DE PRESOS DE CHUBUT COMENZÓ UNA HUELGA DE HAMBRE PARA RECLAMAR “MEJORES COLCHONES Y MÁS VISITAS ÍNTIMAS”"

+++

#### **_Diez reclusos del Instituto Penitenciario Provincial firmaron una nota que enviaron a las autoridades del establecimiento en la que manifestaron su descontento por el mal estado de las instalaciones en las que reciben a sus parejas._**

Un grupo de presos alojados en el Instituto Penitenciario Provincial de Chubut inició una “huelga de hambre seca” para reclamar por mayor frecuencia de visitas íntimas y mejores condiciones en las instalaciones, ya que “los colchones se encuentran en mal estado”.

El edificio de la cárcel se encuentra ubicado en la ruta nacional 3, entre Trelew y Puerto Madryn, y allí solo se aloja a presos provinciales. Varias veces fue inspeccionado por las quejas de los detenidos.

A través de una nota manuscrita dirigida al director del penal, el grupo de presos detalló los reclamos que los llevó a establecer la medida a partir de este lunes a las 8.45. En el texto piden por “una mayor frecuencia” para los encuentros íntimos porque “por ley tienen que tener dos íntimos por mes, o sea cada 15 días”. Además, reclaman mejores condiciones en las instalaciones donde se realizan dichas visitas.

En el mensaje también piden que “se tapen las ventanas” de los lugares donde se llevan a cabo los encuentros, ya que las mismas están rotas. En ese sentido, los presos argumentan que no tienen “privacidad” y, por tal motivo, exigen “que sean arregladas cuanto antes”.

En el texto también incorporaron quejas relacionadas con la comida que pueden llevarles las visitas. “A veces dejan pasar solo galletitas y además hay mucha demora en el ingreso, lo cual nos quita tiempo para estar con ellas”, sostienen.

Por otro lado, los detenidos se quejan de que el baño de mujeres pierde agua, de manera “que se hacen charcos en la sala de visitas”, y manifiestan su descontento por la falta de luz en el baño de hombres y por las malas condiciones del colchón que utilizan para estar con sus parejas.

Al finalizar la nota, los 10 reclusos, les piden a las autoridades que “se pongan una mano en el corazón” cuando lean sus reclamos.