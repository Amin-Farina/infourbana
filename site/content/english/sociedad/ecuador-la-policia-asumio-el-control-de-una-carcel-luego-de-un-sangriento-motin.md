+++
author = ""
date = 2021-10-01T03:00:00Z
description = ""
image = "/images/ecuador_policia01-1.jpg"
image_webp = "/images/ecuador_policia01.jpg"
title = "ECUADOR: LA POLICÍA ASUMIÓ EL CONTROL DE UNA CARCEL LUEGO DE UN SANGRIENTO MOTÍN"

+++

#### **_El motín comenzó el martes, cuando reclusos de bandas rivales, con nexos con el narcotráfico mexicano, se enfrentaron con armas de fuego._**

Tres días después de un motín en una prisión de Guayaquil que dejó al menos 118 presos muertos, seis de ellos decapitados, la Policía asumió anoche el control de esa penitenciaría, anunciaron fuentes oficiales.

"Todo está tranquilo, ellos (los reclusos) están en las celdas. No es que los pabellones están tomados por ellos", expresó la comandante de la Policía, general Tannya Varela, en declaraciones a la prensa en la penitenciaría luego de liderar un operativo con la intervención de 900 agentes.

Los pabellones "no están tomados. Nosotros estamos ingresando normalmente", enfatizó, citada por la agencia de noticias AFP.

El motín comenzó el martes, cuando reclusos de bandas rivales, con nexos con el narcotráfico mexicano, se enfrentaron con armas de fuego.

Varela señaló que la Policía decomisó el viernes tres pistolas, 435 municiones, 25 armas blancas y tres artefactos explosivos. Dos policías también resultaron heridos en el amotinamiento.

Entretanto, la Defensoría del Pueblo precisó que "existen 118 personas fallecidas, 86 heridas, de ellas seis de gravedad y 80 leves".

El miércoles, el presidente Guillermo Lasso reportó 116 reclusos muertos y cerca de 80 heridos, y declaró el estado de excepción en el sistema penitenciario nacional, con lo que suspendió derechos a los presos.

Según el sitio local Primicias, el motín comenzó cuando presos de una banda celebraron el cumpleaños de uno de sus líderes detenidos, e hicieron alarde de tener el poder en la prisión, lo cual molestó a otras organizaciones rivales ubicadas en otros pabellones y desató los enfrentamientos.

Tanquetas y decenas de militares estuvieron apostados durante toda la jornada del viernes en los alrededores de la cárcel, donde centenares de familiares buscaban información sobre sus parientes.

"Es algo muy doloroso. (...) Dicen que hay personas que les han sacado la cabeza", dijo a la AFP Juana Pinto, que esperó impaciente conocer la suerte que corrió su hijo preso.

La Asamblea Nacional (Parlamento) de Ecuador aprobó el jueves una resolución para pedir la presencia de la ministra de Gobierno, Alexandra Vela, y del titular del Servicio de Atención a Privados de Libertad (SNAI), Bolívar Garzón, por la masacre ocurrida en el penal de Guayaquil.

Ni Vela ni Garzón forman parte de la gestión del presidente Guillermo Lasso desde la asunción del jefe del Estado, a fines de mayo: la ministra llegó al cargo a mediados de julio en reemplazo de César Monge, y Garzón asumió en el SNAI justamente el martes, en lugar del exasambleísta Fausto Cobo.

Según la resolución, las comparecencias deberán ser en un plazo máximo de ocho días, en una misma sesión plenaria.