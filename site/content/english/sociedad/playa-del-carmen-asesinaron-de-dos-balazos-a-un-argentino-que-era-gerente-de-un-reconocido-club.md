+++
author = ""
date = 2022-01-26T03:00:00Z
description = ""
image = "/images/awfnm367yzd4xfsw65nqtvrr5m.jpg"
image_webp = "/images/awfnm367yzd4xfsw65nqtvrr5m.jpg"
title = "PLAYA DEL CARMEN: ASESINARON DE DOS BALAZOS A UN ARGENTINO QUE ERA GERENTE DE UN RECONOCIDO CLUB"

+++

##### **_Se trata de Federico Mazzoni y fue ejecutado en el interior de su negocio el martes por la tarde. El ataque se da en la misma semana en la que mataron a dos turistas canadienses_**

Un argentino fue asesinado de dos disparos en la cabeza en Playa del Carmen, México. La víctima fue identificada como Federico Mazzoni, gerente de Mamita’s Beach Club, un reconocido club de playa de la zona.

Según la primera información que trascendió, el hombre fue acribillado en uno de los baños del lugar que administraba, este martes por la tarde.

Mazzoni sería una de las personas allegadas al empresario Jorge Marzuca Fuentes, dueño de uno de los clubes más conocidos de la zona de Quintana Roo y México.

Este es el segundo ataque armado registrado en Quintana Roo en menos de una semana. El pasado viernes 21 de enero sicarios ejecutaron a dos turistas canadienses con historial criminal, quienes fallecieron en el interior de un hotel en Xcaret.

La Fiscalía General del Estado de Quintana Roo (FGE) informó a través de un comunicado que ya iniciaron las labores de investigación en el caso. Personal policial se trasladó al lugar de los hechos para recolectar la evidencia correspondiente e interrogar a los testigos del crimen del argentino.

> “La #FGEQuintanaRoo informa que inició carpeta de investigación por el homicidio de un empleado de un restaurante en el Municipio de Solidaridad. Agentes de la #PDI y peritos están en el lugar recabando entrevistas y evidencias para identificar al o los participantes en estos hechos”, informó la dependencia a través de sus redes sociales.

Las fuerzas de seguridad aseguraron el perímetro y dieron inicio al operativo para dar con el asesino que ejecutó a Mazzoni. “Tenemos el homicidio de una persona por disparo de arma de fuego, la primera investigación señala que fueron dos sujetos los que ingresaron, estuvieron platicando con la ahora víctima y posteriormente huyeron. No hay probabilidad de que tenga relación con el ataque en el hotel Xcaret”, indicó el fiscal general de Quintana Roo, Oscar Montes de Oca.

Por su parte, la directiva de Mamita’s Beach Club publicó un comunicado en el que lamentaron los hechos ocurridos en sus instalaciones, pero no dieron más información sobre el caso. “Con mucho pesar, lamentamos el hecho registrado la tarde de este martes en nuestras instalaciones. Externamos nuestras condolencias y oraciones para familiares de nuestro colaborador”, expresó el club de playa.

Los dueños se encuentran “a la espera del resultado de las investigaciones y colaborando con las autoridades correspondientes para el total esclarecimiento” del crimen. “Grupo Mamitas no hará ningún otro comentario para no entorpecer la investigación de la Fiscalía del Estado”, aclaró el lugar.

Los investigadores analizan, además, la filmación de la cámara de seguridad de un hotel lindero al club de playa, que muestra como los presuntos responsables del crimen se escapan del lugar en motos de agua. Por el momento no hay detenidos.

Jorge Marzuca, director general de Mamita’s Group, había sido señalado por Jorge Luis Brisuela Guevara por haber participado en la filtración de documentos en los cuales se expuso la adjudicación directa de contratos para “carpas covid” por parte del gobernador Carlos Joaquín González.

Jorge Brisuela, alias El Venezolano, fue expuesto en una conferencia matutina del presidente de México, Andrés Manuel López Obrador, por ser amigo del gobernador Carlos Joaquín, quien le habría dado contratos por 44 millones de pesos para la instalación de centros de atención para pacientes con COVID-19 en distintos puntos del estado.