---
title: 'Hallaron fosfina en Venus: ¿Por qué es importante y cuántas son las chances
  de que haya vida?'
date: 2020-09-14T21:40:12.000+00:00
author: 
image_webp: "/images/descubren-indicios-de-vida-en_0_0_1200_747.jpg"
image: "/images/descubren-indicios-de-vida-en_0_0_1200_747.jpg"
description: Científicos constataron la presencia de este gas, que es considerado
  una huella de la vida o biomarcador.

---
_Científicos constataron la presencia de este gas, que es considerado una huella de la vida o biomarcador._

La **vida** en cualquiera de sus manifestaciones parecería ser un bien esquivo, al menos, en los reducidos límites de nuestro [Sistema Solar](https://www.clarin.com/tema/sistema-solar.html "sistema-solar"). Sin embargo, a sólo 40 millones de kilómetros, en [Venus](https://www.clarin.com/tema/venus.html "venus"), el planeta más cercano a la[ Tierra](https://www.clarin.com/tema/planeta-tierra.html "planeta-tierra"), un equipo de científicos asegura haber hallado **una huella de vida** a 55 kilómetros de altura, justo sobre la capa de nubes de ácido sulfúrico que calientan la superficie como si fuera una inmensa parrilla de piedra. Ese indicio es [un gas](https://www.clarin.com/sociedad/-indicios-vida-venus-encontraron-gas-existente-tierra_0_3_3buxE4w.html) conocido como **fosfano o fosfina (PH3)** que fue observado por telescopios terrestres.

El fosfano es una molécula formada por un átomo de fósforo y tres átomos de hidrógeno. Desde hace un tiempo, este compuesto es considerado **una huella de la vida o biomarcador**. Es decir, son sustancias asociadas con los seres vivos.

En la Tierra, este gas solo se produce industrialmente o por microbios que prosperan en entornos libres de oxígeno. Los astrónomos han especulado durante décadas que las nubes altas en Venus podrían ofrecer **un hogar para los microbios**, que flotan libres de la superficie abrasadora, pero necesitan tolerar una acidez muy alta.

“**No deberíamos apresurarnos** a afirmar que la fosfina es de origen biológico, es decir producida por seres vivos. Habría que descartar posibles fuentes de producción de fosfina originadas a partir de procesos abióticos, es decir aquellos donde los seres vivos no intervienen”, argumenta la astrobióloga Ximena Abrevaya, del Instituto de Astronomía y Física del Espacio (IAFE/Conicet).

![](https://gestion.fundaciondescubre.es/files/2020/09/Detectan-en-Venus-fosfina-un-gas-que-en-la-Tierra-producen-los-seres-vivos_1600104043.jpg)

En un estudio publicado este lunes en Nature Astronomy los científicos señalan que la cantidad de fosfina en Venus es **10.000 veces más alta** que la que podría producirse por métodos no biológicos.

“Lo primero, sería determinar si hay alguna forma de que se produzca de forma no biológica. Así, una vez descartada esta opción, **las posibilidades de que sea biológico aumentan significativamente**”, advierte Abrevaya.

El gas fue detectado mediante la observación de la atmósfera venusiana con la ayuda del radiotelescopio ALMA (Atacama Large Millimeter/submillimeter Array), situado en [Chile](https://www.clarin.com/tema/chile.html "chile") y James Clerk Maxwell (JCMT) en Hawái.

"Cuando obtuvimos los primeros indicios de fosfina en el espectro de Venus, **¡fue un shock!**", señaló Jane Greaves de la Universidad de Cardiff en el Reino Unido, quien descubrió por primera vez signos de este gas.

![](https://danielmarin.naukas.com/files/2014/07/Captura-de-pantalla-2014-07-22-a-las-23.48.56.png)

Este compuesto se halla también en planetas gigantes gaseosos del Sistema Solar, pero no es de origen biológico.

Desde hace años se sabe que los mayores planetas del sistema solar, Júpiter y Saturno, generan fosfina al unir un átomo de fósforo y tres de hidrógeno en sus capas internas, que están a **más de 500 grados**, en un proceso totalmente ajeno a la presencia de vida.

“Hace alrededor de diez años se detectó fosfina en Saturno y Júpiter. Si bien los procesos de formación de fosfina en estos planetas se deben a procesos químicos que ocurren bajo muy altas presiones y que no podríamos encontrar en **Venus**, es un buen ejemplo de cómo procesos no biológicos, es decir no vinculados a la presencia de vida, también pueden originar fosfina”, advierte Abrevaya.