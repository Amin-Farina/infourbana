+++
author = ""
date = 2021-12-23T03:00:00Z
description = ""
image = "/images/5eb862ef94331_900-1.jpg"
image_webp = "/images/5eb862ef94331_900.jpg"
title = "POR LAS AGUAS CLOACALES, DETECTARON QUE ÓMICRON TIENE UNA ALTA CIRCULACIÓN EN LA CIUDAD DE CÓRDOBA DESDE HACE 10 DÍAS"

+++

#### **_Lo determinó el monitoreo de aguas residuales que hacen investigadores del Conicet y la Universidad Nacional de Córdoba. La provincia registró una suba del 326% de los casos en las últimas dos semanas._**

Entre las personas que se contagian el coronavirus que causa la enfermedad COVID-19, el 35% pasa a excretarlo cuando va al baño. Eso sucede durante 17 días tanto en pacientes con síntomas o sin síntomas. Po esta razón, la evolución de los casos de COVID-19 se puede seguir también al tomar muestras de aguas residuales en las ciudades con servicios de cloacas.

Con esta metodología, investigadores del Conicet en el Instituto de Virología José María Vanella de la Facultad de Ciencias Médicas de la Universidad Nacional de Córdoba pudieron precisar cuándo se registraron los primeros casos con la variante Ómicron y avisaron que podía haber brotes en esa provincia.

En Córdoba, se registró una suba del 326% de los casos confirmados de COVID-19 durante las últimas dos semanas. En la mayoría de los casos, predomina la variante de preocupación Ómicron, que fue detectada en África tan solo el mes pasado y ya se propagó y detectó en 106 países.

Para reducir el riesgo de más contagios, desde hoy a la mañana la administración pública de Córdoba pasó al teletrabajo. “La disposición regirá desde mañana (por hoy) “y hasta nuevo aviso”, recordaron desde el Gobierno local y argumentaron: “La medida busca reducir al mínimo indispensable la presencia física de personas en cada uno de los ministerios y reparticiones, garantizando el estricto cumplimiento de los protocolos sanitarios”.

Córdoba es hoy la provincia más afectada por el crecimiento rápido de casos de COVID-19 por Ómicron. Está entre las 5 jurisdicciones con “riesgo epidemiológico alto” -donde más han aumentado los casos semanales-, junto con Tucumán, Ciudad de Buenos Aires, Neuquén y Río Negro. En ciudad de Buenos Aires, aumentaron el 110% durante los últimos 15 días. En Neuquén, los casos crecieron un 37%; en Tucumán, el 34%; y en Río Negro el 23% durante las últimas dos semanas.

El monitoreo de las aguas cloacales le ha sido útil a las autoridades sanitarias para tomar decisiones. “Tras el inicio de la pandemia en Argentina, empezamos a usar la epidemiología de aguas residuales. Nos permite tener un resumen de lo que está excretando la comunidad que está conectada a cloacas. tanto casos sintomáticos y asintomáticos”, dijo a Infobae Gisela Masachessi, doctora en Ciencias Biológicas e investigadora del Conicet.

El 6 de diciembre pasado, en las aguas residuales de Córdoba se empezó a detectar la variante Delta. “Indicaba que volvía a haber infectados”, señaló la científica. Entre marzo y junio pasado se había producido la segunda gran ola de la pandemia en el país y la metodología de monitoreo de aguas residuales también había permitido detectar a las variantes en circulación, como Gamma.

“El 13 de diciembre detectamos tanto las variantes Ómicron como Delta. Con los datos del monitoreo, una semana antes solo se detectaba Delta. Al tener en cuenta que Ómicron tiene mayor capacidad de transmisibilidad, el monitoreo de aguas residuales resultó clave: se avisó al Ministerio de Salud de Córdoba que los casos de COVID-19 iban a aumentar dos semanas antes”, informó Masachessi. “Las aguas residuales son como centinelas silenciosas. Al analizarlas, se consigue anticipar qué puede pasar en las próximas semanas en la curva de casos de COVID-19″, señaló.

Un día antes de que Ómicron se detectara en las aguas residuales, el Ministerio de Salud de la Nación informó que el Instituto Malbrán ha confirmado cuatro casos de variante Ómicron en la provincia de Córdoba. El caso índice fue persona que regresó de viaje el 2 de diciembre, procedente de Dubái, Emiratos Árabes Unidos, desembarcando en el Aeropuerto Internacional Ministro Pistarini de Ezeiza con test de antígeno negativo al arribo. La persona comenzó con síntomas el sábado 4 de diciembre, cuando ya se encontraba en la provincia de Córdoba.

La investigación en Córdoba se empezó el año pasado en el marco de la Unidad Coronavirus del Ministerio de Ciencia, Tecnología e Innovación de la Nación. La provincia de Córdoba fue una de las que comenzó a trabajar de manera inmediata para comprender cómo era la circulación del virus en la comunidad local. Con muestreos que iniciaron en mayo de 2020 en la capital provincial, los investigadores científicos en colaboración con el municipio local, instituciones de salud provinciales y cooperativas locales lograron predecir las dos olas que atravesó la provincia más un brote que se registró durante el verano pasado.

El grupo de Masachessi ya tenía experiencia en el estudio de virus que se excretan por vía entérica y que pueden ser detectados en efluentes cloacales. La ciudad de Córdoba tiene el 50% de la población conectada a la red cloacal. Por tanto, “la detección del virus en esa matriz podría ser un reflejo de lo que sucedía en la población”, argumentó la científica que contó con fondos de la Agencia Nacional de Promoción de la Investigación, el Desarrollo Tecnológico y la Innovación.

El uso de aguas residuales para detectar enfermedades a nivel poblacionales se ha usado antes de la pandemia del coronavirus. Es un enfoque analítico desde hace décadas que brinda información en tiempo real sobre la salud de distintas poblaciones. Por ejemplo, se aplicó en la detección de poliomielitis a nivel comunitario o la exposición de los individuos a químicos o patógenos.

En el caso del COVID-19, la huella genética del coronavirus se desprende de la materia fecal de individuos contagiados y se transfiere a las aguas residuales. Al medir la concentración de material viral en las muestras, se obtiene información respecto al alcance de la propagación del virus en una comunidad, incluidos casos asintomáticos, y se detectan cuáles son las variantes en circulación. Permite realizar un rastreo rápido y económico de la tendencia de la enfermedad a nivel poblacional.