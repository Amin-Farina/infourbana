+++
author = ""
date = 2022-01-09T03:00:00Z
description = ""
image = "/images/tamara-rosso-denuncio-que-balearon___4eli8eer3_640x361__1.jpg"
image_webp = "/images/tamara-rosso-denuncio-que-balearon___4eli8eer3_640x361__1.jpg"
title = "BALEAN EL FRENTE DE LA CASA DE UNA CONCEJALA DEL FRENTE DE TODOS Y DENUNCIA UN ATENTADO"

+++

##### **_Fuentes policiales aseguraron que el hecho ocurrió cerca de las 21 del pasado lunes en una vivienda de la localidad de Ostende, en la que vive Tamara Rosso, presidenta del bloque de concejales del FdT del partido de Pinamar, junto a su esposo y una hija de 12 años._**

La casa de la presidenta del bloque de Concejales del Frente de Todos (FdT) en el partido de Pinamar fue baleada por personas que huyeron en un auto, pero el ataque no provocó heridos, mientras que la edil denunció que se trató de un atentado y ahora cuenta con custodia policial.

Fuentes policiales aseguraron que el hecho ocurrió cerca de las 21 del pasado lunes en una vivienda de la localidad de Ostende, en la que vive Tamara Rosso, presidenta del bloque de concejales del FdT del partido de Pinamar, junto a su esposo y una hija de 12 años.

En ese momento, autores desconocidos a bordo de un vehículo dispararon contra el inmueble y huyeron.

La concejala explicó este jueves a Télam que en el momento del ataque se hallaba su hija con una amiga "a punto de ver una película, y escucharon un ruido muy fuerte".

"Yo en ese momento me encontraba en mi negocio junto a mi esposo, a unas cinco cuadras de mi casa, cuando la llamo a mi hija para ver cómo estaba y ahí me cuenta que había escuchado un ruido muy fuerte, que le dolía el oído y a los pocos minutos me cuenta que estaba el vidrio de la ventana roto", agregó.

"Aun si pensar en nada malo, mi esposo se va para casa y, luego de ver lo que había ocurrido, me cuenta e inmediatamente, conmocionados y en shock por el hecho, radicamos la denuncia en la comisaria de Ostende", explicó Rosso.

Para la concejala, el hecho fue "un atentado" y lo consideró como "violencia política".

"Estoy con custodia policial y espero que la justicia logre dar con los autores del hecho", dijo Rosso, quien agregó que "el proyectil entró por la ventana, atravesó todo el comedor y quedó incrustado en el marco de una puerta interna de la casa. Pasó a 20 centímetros de la cabeza de mi hija, fue realmente un milagro".

Una fuente de la investigación señaló que tanto el proyectil que se extrajo del marco de una puerta internada del domicilio como la vaina que se encontró en la calle corresponden a un calibre 9 milímetros.

El hecho es investigado por la Unidad Funcional de Instrucción (UFI) 4 de Pinamar, a cargo de Juan Pablo Calderón, del Departamento Judicial de Dolores.