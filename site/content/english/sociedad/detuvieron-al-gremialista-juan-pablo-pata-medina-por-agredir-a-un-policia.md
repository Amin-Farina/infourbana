+++
author = ""
date = 2022-02-22T03:00:00Z
description = ""
image = "/images/pmitlgayijahdpuypbgulaxxqu.jpg"
image_webp = "/images/pmitlgayijahdpuypbgulaxxqu.jpg"
title = "DETUVIERON AL GREMIALISTA JUAN PABLO “PATA” MEDINA POR AGREDIR A UN POLICÍA"

+++

#### **_El sindicalista, que se encontraba acompañado por familiares, se enfrentó con un grupo de personas que le gritaron “a la UOCRA no volvés más” frente Hospital Italiano de La Plata_**

El dirigente sindical Juan Pablo “Pata” Medina fue detenido durante la mañana de hoy en las inmediaciones del Hospital Italiano de La Plata, luego de enfrentarse con un grupo de personas que le gritaron “a la UOCRA no volvés más” y golpear a un efectivo de la policía bonaerense.

Según las primeras informaciones oficiales, el gremialista estaba acompañado por su mujer y su hija, y se dirigía al centro de salud para realizar un control médico, cuando fue interceptado por un grupo de personas, que lo agredieron y lo amenazaron. Se investiga si se trata de un nuevo hecho violento que se enmarca dentro de la interna sindical de la UOCRA en La Plata.

De acuerdo al parte oficial de la policía bonaerense, Medina, producto del enfrentamiento, debió ser atendido en el centro de salud, pero en ese momento le propinó un golpe de puño a uno de los efectivos policiales. Al salir del nosocomio volvió a agredir -de manera verbal- a personal policial, por lo que se dispuso su detención, también la de su hija y de otro hombre, no identificado. Fueron trasladados a la comisaría 4ta de La Plata e interviene en la causa el fiscal Marcelo Romero (UFI 6).

Medina se encuentra en prisión domiciliaria (pudo acudir al centro de salud por permiso judicial). El juez Alejandro Esmoris, del Tribunal Oral Federal N° 2 de La Plata, lo ordenó en agosto del año pasado por violar la prohibición de realizar actividades gremiales.

El dirigente sindical había sido detenido en septiembre de 2017 por orden del juez federal de Quilmes Luis Armella, que lo procesó por los delitos de “asociación ilícita”, “lavado de dinero” y “extorsión” y además le trabó un embargo por 200 millones de pesos. El miércoles que viene arrancará el juicio oral por esta causa. En febrero de 2020 fue beneficiado con la prisión domiciliaria, que cumplió en su casa de la localidad bonaerense de Ensenada, en calle 60 entre 9 bis y 11 de Villa del Plata.

Un año después, en febrero de 2021, fue excarcelado por el TOF 2 de La Plata. En ese momento, el juez le fijó una serie de restricciones como la prohibición de salida del país, la de acercarse a las víctimas de los hechos por los cuales se lo acusa, y también de participar de actividades gremiales en la UOCRA u otras seccionales.

César Albarracín, abogado de Medina, dijo que el sindicalista fue detenido por insultar a un policía. “Fuera de la clínica discutió con la policía por la situación que había vivido y porque no lo habían protegido. Yo estaba con él y los policías se pusieron picantes y Medina le dijo a uno ´vos que me miras así vigilante, hijo de puta, le pegaron a mi hija” y lo detuvieron por eso. Me parece gravísimo y lo vinculo con las denuncias que en la causa de la Gestapo”, señaló el letrado.

Más allá de la causa por los delitos de asociación ilícita, el dirigente gremial tiene otros expedientes en la Justicia. En noviembre del año pasado, la Suprema Corte de Justicia bonaerense revocó un fallo dictado por la Cámara de Casación provincial, que lo absolvió en una causa en la que había acordado en un juicio abreviado una condena de 11 meses de prisión de cumplimiento efectivo.

La acusación indicaba que el gremialista había obligado a obreros a hacer huelgas y el propio sindicalista había admitido los hechos. Sin embargo, Casación revocó esa condena, algo que fue revocado por la firma de los jueces de la Corte bonaerense Hilda Kogan, Sergio Torres, Luis Genoud y Daniel Soria.

Medina había pactado una condena en una causa que se había iniciado con la denuncia de la empresa Abes en 2017 por el delito de coacción agravada, ya que acusaban al sindicalista de cometer aprietes a empresarios de la construcción que levantaban edificios en el centro de La Plata. En aquella denuncia se señalaba que Medina había dispuesto la toma de obras en construcción de calle 58 entre 5 y 6; calle 8 entre 33 y 34; avenida 7 y 39; calle 47 entre 12 y 13 y 58 entre 10 y 11, en demanda de mejoras salariales.