+++
author = ""
date = 2022-02-24T03:00:00Z
description = ""
image = "/images/ebzl3e3vkndobo6ioh74sunvhu.jpg"
image_webp = "/images/ebzl3e3vkndobo6ioh74sunvhu.jpg"
title = "ARGENTINOS EN UCRANIA: ¿CUÁNTOS VIVEN EN EL PAÍS ATACADO POR RUSIA Y QUÉ MEDIDAS DISPUSO LA EMBAJADA?"

+++

##### **_Tras la ofensiva ordenada en las últimas horas por Vladimir Putin, el Embajada argentina en Ucrania desplegó un sistema de emergencia para atender a los ciudadanos argentinos que están viviendo de manera permanente en ese país._**

Fuentes oficiales indicaron que en Ucrania “**hay** **83 argentinos registrados permanente y unos 20 en tránsito**”. Asimismo, destacaron que se reforzaron las partidas de “pasaportes provisorios” para que la sede diplomática en la capital ucraniana tenga en caso de asistencia consular.

Más de 40 soldados ucranianos y una decena de civiles murieron en las primeras horas de la invasión lanzada por Rusia contra ese país, informó este jueves por la mañana un consejero del presidente de Ucrania.

En este sentido, las fuentes oficiales consultadas por este medio trataron de transmitir calma. _“La comunidad argentina está conectada a través de las redes oficiales y con un WhatsApp de Emergencia exclusivo para argentinos”_, indicaron.

Las mismas fuentes oficiales confirmaron que la Cancillería está en permanente contacto con la embajada argentina en Kiev, que “la Sección Consular de la Embajada está trabajando con normalidad y en comunicación permanente con la comunidad argentina”, añadieron.

En las últimas horas, la embajadora Elena Leticia Mikusinski aconsejó a través de las redes sociales de la sede diplomática a los connacionales abandonen de inmediato ese país.

Asimismo, recordaron “_la importancia de contar con la documentación argentina (DNI, pasaportes) y ucraniana (permisos de residencia si fueran residentes y no se encontraran transitoriamente en Ucrania) al día_”.

Por otro lado, se informó que las sedes diplomáticas en Polonia y en Rumania, dos países hacia donde huyen los ucranianos para escapar del conflicto bélico, “_están al tanto de esta situación y a disposición para prestar asistencia_”.

##### Cuál puede ser el impacto para la Argentina

La invasión militar que ordenó Putin tuvo como primera reacción en los mercados que el precio del barril de petróleo superara los 100 dólares por primera vez en más de 7 años.

Asimismo, las operaciones en el mercado de Chicago de referencia para las materias primas agropecuarias se volvieron frenéticas luego de que fuerzas rusas lanzaran el ataque contra Kiev. La soja subía 4,6% hasta superar largamente la barrera de los USD 600, el trigo 5,45% y el maíz 4,87% como resultado de los eventos.

La onda expansiva de la escalada del conflicto bélico puede impactar en distintos puntos de la economía argentina.