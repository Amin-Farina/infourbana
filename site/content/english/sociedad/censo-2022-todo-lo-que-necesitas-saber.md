+++
author = ""
date = 2022-03-14T03:00:00Z
description = ""
image = "/images/censo-2022.jpg"
image_webp = "/images/censo-2022.jpg"
title = "CENSO 2022: TODO LO QUE NECESITAS SABER "

+++
#### El Censo estará dividido en dos etapas, una el 16 de marzo y otra el 18 de mayo

Este año se realizará el Censo Nacional de Población, Hogares y Viviendas en la República Argentina, donde se obtendrán mediciones del número total de individuos al igual que información sobre sus condiciones de vida.

El Censo estará dividido en dos etapas. La primera tendrá lugar a partir del 16 de marzo, donde cada ciudadano podrá responder las preguntas del cuestionario censal desde cualquier dispositivo electrónico con acceso a internet mediante [**www.censo.gob.ar**](http://www.censo.gob.ar/)**.**

La segunda será el miércoles 18 de mayo, que a su vez será feriado nacional. Allí las y los censistas recorrerán todas las viviendas para pedirles a las personas el comprobante, el número que se les entrega luego de completar el censo de manera digital, o mismo solicitar que realicen la entrevista de manera presencial.

##### ¿A qué hora es el Censo 2022?

A partir de las 8:00 del miércoles 18 de mayo de 2022, una persona censista, debidamente acreditada, visitará cada una de las viviendas del país, hayan completado o no el cuestionario digital.

##### ¿Qué documentación se debe presentar?

Lo único que se le debe mostrar a los censistas será el comprobante que se entrega luego de completar el censo de forma digital o realizar la entrevista.

##### ¿Qué preguntan?

El cuestionario censal contiene 61 preguntas que indagan sobre los siguientes temas:

**Población**

* edad y sexo
* identidad de género
* lugar y fecha de nacimiento
* lugar de residencia 5 años antes del Censo
* nivel educativo
* cobertura de salud
* previsión social
* autorreconocimiento étnico (pueblos indígenas u originarios y afrodescendencia)
* situación laboral
* rama de actividad
* fecundidad

**Vivienda y hogar**

* materiales con los que se encuentra construida la vivienda
* condiciones sanitarias
* régimen de tenencia de la vivienda
* cantidad de habitaciones
* acceso a servicios de agua, luz y gas
* dificultad o limitación
* TIC (computadora, internet y teléfono)

##### ¿Es obligatorio?

Sí. Según el artículo 17 del Decreto 726/2020, todas las personas que habitan el territorio nacional tienen que responder las preguntas incluidas en el cuestionario censal.