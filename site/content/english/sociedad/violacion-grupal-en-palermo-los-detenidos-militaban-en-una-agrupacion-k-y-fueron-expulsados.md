+++
author = ""
date = 2022-03-02T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-03-02-at-10-03-12.jpeg"
image_webp = "/images/whatsapp-image-2022-03-02-at-10-03-12.jpeg"
title = "VIOLACIÓN GRUPAL EN PALERMO: LOS DETENIDOS MILITABAN EN UNA AGRUPACIÓN K Y FUERON EXPULSADOS"

+++

##### **_Mientras la Justicia espera el resultado de distintos peritajes para indagar en las próximas horas a los seis detenidos acusados de violar en grupo a una chica de 20 años en el barrio porteño de Palermo, la organización Lealtad Corriente Militante de Vicente López en la que participaban los jóvenes repudió los hechos y se puso a disposición de la víctima y de su familia._**

El comunicado emitido por la organización política de Vicente López, Lealtad Corriente Militante, fue difundido ayer por el concejal y referente de la organización Lucas Boyanovsky a través de su cuenta de Twitter: “Es nuestra obligación ser implacables ante cualquier forma de violencia por razones de géneros. Comparto el comunicado de Lealtad Vicente López”.

Y el texto agrega: “Somos implacables en nuestro posicionamiento, no permitiremos que alguien que haya ejercido una de las formas más extremas de la violencia de género como lo es la violencia sexual, continúe participando en actividades de nuestra organización”. De esta manera, la agrupación expulsó a Ignacio Retondo y Lautaro Ciongo Paciotti, dos de los involucrados en la violación grupal en Palermo.

“Es un delito gravísimo y nuestro rechazo es compromiso en la construcción de una organización y una sociedad libre de violencias”, apuntó en otro tuit Boyanovsky, que además se puso a disposición de la víctima.

Este martes por la mañana, todos los detenidos que se encontraban alojados en diferentes dependencias policiales de la Ciudad, esperaban ser trasladados para ser indagados por el Juzgado Criminal y Correccional número 21.

En tanto, la joven abusada fue dada de alta después de atravesar un protocolo forense para casos de violación y se encuentra acompañada por sus familiares, tras ser asistida por médicos y psicólogos del Hospital Rivadavia.

##### El momento en el que los vecinos golpearon y detuvieron a los atacantes

El hecho ocurrió el lunes por la tarde en Serrano al 1.300, adonde acudieron efectivos de la comisaría vecinal 14A por una denuncia de abuso sexual. Toda la secuencia quedó registrada en un breve video que grabó otro vecino del lugar.

“La chica estaba totalmente bajo el efecto de algún estupefaciente; no se podía mantener de pie”, expresó a TN Natalia Duarte, que observó desde su panadería a dos hombres fuera del vehículo y a otros cuatro en el interior.

##### ¿Quiénes son los detenidos por la aberrante violación grupal en Palermo?

Los seis detenidos fueron identificados por la policía como Ángel Pascual Ramos (23), Tomás Domínguez (21), Lautaro Pasotti (24), Ignacio Retondo (22), Alexis Cuzzoni (20) y Franco Lykan (24).

Al menos dos de los apresados tenían lesiones en el rostro como arañazos, por lo que los peritos analizaron las uñas de la víctima para ver si podía extraerse algún rastro orgánico para analizar y elaborar un patrón genético y poder vincular ese ADN con el de alguno de los detenidos.

Dentro del Volkswagen, los policías hallaron marihuana, dosis de LSD y secuestraron siete teléfonos celulares, que ahora también serán peritados.