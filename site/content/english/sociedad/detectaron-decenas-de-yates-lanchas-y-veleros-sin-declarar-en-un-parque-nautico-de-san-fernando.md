+++
author = ""
date = 2021-12-22T03:00:00Z
description = ""
image = "/images/ob3duainune2xglrmy4a5m7kbq.jpg"
image_webp = "/images/ob3duainune2xglrmy4a5m7kbq.jpg"
title = "DETECTARON DECENAS DE YATES, LANCHAS Y VELEROS SIN DECLARAR EN UN PARQUE NÁUTICO DE SAN FERNANDO"

+++

#### **_Equipos de fiscalización de la Agencia de Recaudación de la provincia de Buenos Aires (ARBA) detectaron, durante un operativo realizado en un importante amarradero de San Fernando, que 82 yates, lanchas y veleros no se encontraban registrados ante el fisco y, de esa manera, sus dueños evadían el pago del Impuesto a las Embarcaciones Deportivas._**

El director de ARBA, Cristian Girard, explicó que “estos controles en el sector náutico apuntan a regularizar la situación de quienes poseen embarcaciones de lujo sin declarar”, y resaltó que “queremos terminar con la especulación de ciertos sectores de la sociedad, de alto poder adquisitivo, que deciden no inscribir sus yates y evadir así el pago de impuestos”.

“Las guarderías tienen la obligación de informarnos las matrículas de todas las embarcaciones que amarran en el lugar. Esa información se cruza con la base de datos de ARBA para detectar las que no están registradas. En este caso, verificamos en un operativo presencial que, del total de yates, veleros y lanchas en infracción, había 19 con precios de mercado que van de U$S350 mil a U$S1,5 millones. Sus dueños nunca los declararon, lo que es un despropósito e injustificable. Y fueron intimados para que regularicen inmediatamente su situación”, subrayó Girard.

El director de ARBA, quien encabezó la labor de fiscalización en el parque náutico de San Fernando, sostuvo que “lamentablemente, existe una cultura tributaria débil y prácticas evasoras extendidas en ciertos sectores sociales con altos ingresos y acceso a estos bienes suntuarios. Por eso intensificamos la fiscalización, para cortar esa evasión y recuperar recursos genuinos que son fundamentales para financiar políticas públicas del Estado provincial”.

La Agencia de Recaudación envió intimaciones a cada titular de las embarcaciones sin declarar para notificar que procederá a dar el alta de oficio de esos bienes, de manera de cobrar el impuesto adeudado y las multas correspondientes.

A lo largo de este año, en las fiscalizaciones sobre el sector náutico ARBA detectó casi 3.000 embarcaciones que estaban sin declarar, y 259 de ellas ya fueron inscriptas al padrón.

Por otra parte, intimó a 37.403 contribuyentes que registraban deudas por $2.790 millones en concepto de Impuesto a las Embarcaciones Deportivas, y recuperó $488 millones por cuotas impagas.

La fiscalización durante 2021 también incluyó a responsables de amarres y guarderías náuticas: 297 recibieron intimaciones por no presentar declaraciones juradas de Ingresos Brutos o, directamente, por no haberse inscripto en ese tributo. En tanto que a otras 66 se les iniciaron sumarios por incumplir el régimen de información con ARBA.

Girard destacó que “nuestra tarea es mostrar una gestión eficiente, transparente, que tiene resultados concretos para cada bonaerense. Para eso trabajamos en el gobierno de Axel Kicillof. Con el aporte de las y los contribuyentes la Provincia lleva adelante políticas públicas que movilizan la rueda de la economía. Es un Estado muy presente y la gente ve que sus impuestos vuelven a la sociedad”.

En los últimos meses, a la par de los controles sobre las embarcaciones de lujo, la Agencia de Recaudación profundizó las fiscalizaciones sobre vehículos de alta gama, con el objetivo de continuar reduciendo la evasión y el incumplimiento fiscal en aquellos sectores de mayor capacidad patrimonial.