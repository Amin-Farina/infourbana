+++
author = ""
date = 2022-02-11T03:00:00Z
description = ""
image = "/images/como-recuperar-el-auto-frente-a-un-robo-1.jpg"
image_webp = "/images/como-recuperar-el-auto-frente-a-un-robo-1.jpg"
title = "QUISO VENDER EL AUTO EN REDES, LO SECUESTRARON Y LE ROBARON: “PERDÍ LAS GANAS DE SALIR ADELANTE”"

+++

#### **_Facundo Gómez Ovejero, de Tucumán, puso en venta su auto y lo publicó en las redes sociales para difundirlo. Dos delincuentes lo engañaron, se hicieron pasar por clientes, lo secuestraron y le robaron a punta de pistola._**

Tras el hecho, que ocurrió el martes, el joven confesó: “Perdí las ganas de salir adelante”.

##### Lo engañaron para robarle el auto que había publicado en las redes sociales

Gómez Ovejero publicó en Facebook un aviso con los datos de contacto y las fotos del auto que había puesto en venta. Una persona que estaba interesada lo contactó y quedaron en reunirse.

Ese día, alrededor de las 19.30, dos personas se acercaron hasta avenida Independencia al 2800 para ver el Volkswagen Bora que el joven había compartido.

Los supuestos interesados se subieron al vehículo con la intención de “probarlo”. Gómez Ovejero los acompañó, pero en el trayecto lo amenazaron a punta de pistola para que entregara el auto. “Lo había comprado con mucho esfuerzo. Perdí las ganas de salir adelante”, se lamentó en diálogo con La Gaceta.

> “Esta gente me había insistido para verlo a las 8 de la noche del día anterior, pero no me parecía adecuado. Me escribieron desde una cuenta de Facebook que tenía de nombre Sebastián Pérez”, explicó el chico.

Además, destacó que habitualmente utiliza las redes para vender y comprar pero nunca le había sucedido algo así. “No vi en qué vinieron porque ya me estaban esperando cuando llegué. Los dos estaban bien vestidos”, contó.

Según su relato, al subir al auto y luego de hacer algunas cuadras, sacaron un arma de alto calibre. “Era una ametralladora y me amenazaron todo el tiempo. En ningún momento me resistí. Me pidieron el celular pero les rogué que me lo devolvieran porque trabajo con eso, les dije que no me importaba el auto pero que necesitaba el teléfono”, detalló Gómez Ovejero.

“Me lo devolvieron y me hicieron bajar apuntándome con el arma. Yo pensé que me moría ahí”, expresó. El joven fue abandonado en avenida Jujuy al 2200, desde donde tomó un taxi para regresar a su hogar y luego realizó la denuncia en la comisaría 8°.

##### Ofrece una recomensa para quien aporte datos del auto

Con la intención de recuperar el vehículo, ahora ofrece una recompensa de $100.000, siempre y cuando se trate de datos certeros. “Estoy sin ganas de nada. Trabajo desde que tengo 12, 13 años. Sufro de ataques de pánico, ansiedad, tengo mis problemas y lo único que hago es trabajar”, dijo el chico angustiado.

Lo tenía muy cuidado, con la intención de venderlo y comprar algo mejor. Al final lo termino perdiendo. Pero no quiero rendirme”, sentenció. Quienes tengan información para aportar lo pueden hacer comunicándose con el celular 381 586-9131