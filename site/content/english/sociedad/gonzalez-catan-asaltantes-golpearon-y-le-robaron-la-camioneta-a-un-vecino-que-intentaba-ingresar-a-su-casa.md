+++
author = ""
date = 2021-06-08T03:00:00Z
description = ""
image = "/images/movil-policiajpg-1.jpg"
image_webp = "/images/movil-policiajpg.jpg"
title = "GONZALEZ CATÁN: ASALTANTES GOLPEARON Y LE ROBARON LA CAMIONETA A UN VECINO QUE INTENTABA INGRESAR A SU CASA"

+++

**_Un hombre fue asaltado por cuatro delincuentes que lo golpearon y le robaron la camioneta con la que entraba al garaje de su casa de la localidad bonaerense de González Catán, partido de La Matanza, informaron este miércoles fuentes policiales._**

El hecho, dado a conocer este miércoles, ocurrió el lunes a las 21.15 en una vivienda de la calle Balboa 5.300, cuando la víctima entraba con su camioneta Toyota Hilux gris al garaje y fue abordada por varios delincuentes armados que descendieron de un auto oscuro con armas en mano.

Los delincuentes golpearon a la víctima con un arma de fuego, le robaron las pertenencias y se subieron a su camioneta, con la que escaparon seguidos por el otro vehículo en el que habían llegado.

El hombre luego del robo y al ver que los delincuentes escapaban, pudo cerrar el portón del garaje y la puerta de acceso al patio para evitar que entraran a su casa.

La investigación se encuentra a cargo de la comisaría 2da. de González Catán, que procuraban dar con los vehículos y atrapar a los delincuentes.

En la causa, caratulada como “robo automotor y lesiones”, interviene la fiscalía de turno del Departamento Judicial de La Matanza, donde se analizan cámaras de seguridad vecinales en busca de imágenes de los asaltantes.