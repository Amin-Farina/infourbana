+++
author = ""
date = 2021-04-26T03:00:00Z
description = ""
image = "/images/6086b66462d8e_1004x565.jpg"
image_webp = "/images/6086b66462d8e_1004x565.jpg"
title = "POLICÍAS SE ESCONDIERON EN UN CAMIÓN DE RESIDUOS Y LOGRARON ATRAPAR UNA BANDA NARCO"

+++

**_Siete personas fueron detenidas por comercialización de estupefacientes. Agentes de la policía bonaerense, junto al Grupo Táctico Operativo y al Grupo de Apoyo Departamental, elaboraron un plan estratégico para lograr su captura._**

Siete personas fueron detenidas acusadas de integrar una banda dedicada a la venta de estupefacientes en el partido bonaerense de Almirante Brown luego de que policías ocultos en un camión de recolección de residuos municipal, las interceptara en plena operación en una plaza de ese distrito, informaron fuentes policiales.

Las detenciones fueron concretadas por efectivos de la comisaría 3ra. de dicha jurisdicción, en forma conjunta con Grupo Táctico Operativo (GTO) y Grupo de Apoyo Departamental (GAD), quienes primero procedieron a realizar cuatro allanamientos ordenados por el Juzgado de Garantías 4 de Lomas de Zamora, a pedido la Unidad Funcional de Instrucción (UFI) 14, del mencionado departamento judicial.

Tras diversas tareas investigativas y pruebas recolectadas, los policías lograron establecer que los delincuentes realizaban actividades ilícitas de compra y venta de estupefacientes en una plaza de la mencionada localidad, y allí fue donde aprehendieron a una parte de la banda.

Según informaron las fuentes a Télam, los policías arribaron al lugar de manera encubierta, ocultos en un camión de recolección de residuos de la Municipalidad de Almirante Brown, y lograron aprehender a los delincuentes durante una operación de venta de droga.

Según las fuentes, a los detenidos -de entre 17 y 30 años- les secuestraron 346 dosis de clorohidrato de cocaína, tres envoltorios de marihuana, tres balanzas de precisión, elementos de corte y fraccionamiento, tres teléfonos celulares y seis municiones calibre 9 milímetros. precisaron las fuentes.

Los apresados quedaron a disposición de la UFI 14 de Lomas de Zamora, que avaló lo actuado.