+++
author = ""
date = 2022-01-31T03:00:00Z
description = ""
image = "/images/bkpcaw43ffgphpvi3vkchfmtle.jpg"
image_webp = "/images/bkpcaw43ffgphpvi3vkchfmtle.jpg"
title = "UNA JOVEN DENUNCIÓ QUE FUE ABUSADA EN UN BOLICHE DE VILLA GESELL: ES EL SEGUNDO CASO EN 3 DÍAS"

+++

#### **_Ocurrió este sábado en el boliche “Dixit”. La fiscalía está analizando las cámaras de seguridad para identificar a los sospechosos._**

Una joven de 22 años denunció que fue abusada sexualmente dentro del boliche “Dixit” de la localidad balnearia de Villa Gesell. Se trata de la segunda denuncia en menos de 3 días.

Los investigadores intentan establecer si fue atacada por jóvenes de un mismo grupo o distintos. Hasta el momento no hay detenidos por el hecho.

Según lo que declaró la joven, ocurrió en la madrugada del sábado en el boliche ubicado en la calle Paseo 106 y Avenida 3. Cerca de las 2:00 ingresó al complejo con un grupo de amigas, pese a que en un primer momento la seguridad del lugar no le había permitido el acceso por estar en estado de ebriedad.

Siguiendo su relato, a las cuatro de la madrugada perdió a sus amigas y en una escalera cercana a los baños de la planta baja del boliche fue arrinconada por dos jóvenes, quienes comenzaron a manosearla y a insistirle para tener relaciones sexuales.

Luego de este episodio, se reencontró con su grupo pero inmediatamente volvió a perderlas y se puso a bailar en la pista principal del boliche con un grupo de desconocidos. En ese momento, dos hombres empezaron a manosearla mientras ella pedía que no lo hicieron.

Finalmente a las 6:00, la joven volvió a encontrarse con sus amigas y tras contarles lo que había sucedido, fueron a radicar la denuncia correspondiente en la comisaría 1° de Villa Gesell.

##### Detalles que recordó la víctima cuando hizo la denuncia

Cuando brindó su declaración, la joven describió a uno de los acusados de tez trigueña, aproximadamente de 1,70 metros de altura, cabello oscuro y “vestía una camisa negra y floreada”.

Incluso detalló que los que la manoseó en la pista de baile le pasó su usuario de Instagram y comenzaron a seguirse mutuamente.

La fiscal Verónica Zamboni, titular de la Unidad Funcional de Instrucción (UFI) 6 de Villa Gesell, ordenó que se realizara un examen físico a la joven y el mismo arrojó que “no presenta lesiones corporales ni compatibles con abuso sexual”.

El hecho quedó caratulado como “abuso sexual agravado” y la investigación está enfocada en determinar si fue atacada ambas veces por dos jóvenes pertenecientes a un mismo grupo o a diferentes. Además, la fiscal trabaja sobre los DVR de las cámaras de seguridad del boliche para identificar a los sospechosos.