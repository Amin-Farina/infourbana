+++
author = ""
date = 2022-01-17T03:00:00Z
description = ""
image = "/images/txlg5vawhjcdxkuz4554jots6q.jpg"
image_webp = "/images/txlg5vawhjcdxkuz4554jots6q.jpg"
title = "ROSARIO VIOLENTA: DOS SICARIOS MATARON A BALAZOS A UNA EMBARAZADA E HIRIERON A SU HIJITA DE 9 AÑOS"

+++

#### **_Ocurrió el domingo a la noche, alrededor de las 21.25, en la zona oeste de la ciudad. La menor se encuentra en estado reservado, internada en el Hospital de Niños._**

Una vez más las calles de Rosario son escenario de la violencia incontrolable. El domingo por la noche una mujer embarazada de 6 meses fue asesinada a balazos en su vivienda. En el ataque también recibió un disparo su hija de 9 años, que se encuentra estado reservado.

##### **¿Cómo ocurrió el violento crimen?**

El hecho tuvo lugar alrededor de las 21.25 en un edificio ubicado en Viamonte al 7300, en el extremo oeste de la ciudad y a pocas cuadras de avenida Circunvalación.

Según los primeros datos que trascendieron sobre la investigación gracias al testimonio de los vecinos, dos sujetos llegaron al departamento 4 de escalera del segundo piso, donde mencionaron el nombre de la mujer y sin decir más, efectuaron los disparos.

La víctima fatal del ataque fue identificada por la Policía como Débora Celeste Andino, de 31 años, quien tenía un embarazo de 6 meses. La mujer fue encontrada tendida en el suelo de su habitación, frente a la cama, donde presentaba un disparo de arma de fuego en el cráneo, que le ocasionó la muerte en el lugar, constatada por personal del Sies.

En tanto, la nena de 9 años recibió un balazo en el hombro derecho, de manera que fue trasladada en estado reservado al Hospital de Niños Víctor J. Vilela, donde se encuentra internada con pronóstico reservado.

Tras el crimen, en el lugar se hizo presente el Comando Radioeléctrico de la Unidad Regional II, que dio aviso al fiscal de Homicidios Dolosos de turno Ademar Bianchini. Este ordenó medidas investigativas a la División de Homicidios de la Agencia de Investigación Criminal (AIC), como la toma de testimonios, levantamiento de rastros y vainas servidas así como el relevamiento de cámaras en la zona.

Aunque aún es materia de investigación, según indicó el portal Rosario3 las primeras hipótesis indican que se trata de un homicidio por ajuste de cuentas. Es que el hermano de Débora, según fuentes de la investigación, está señalado como un presunto vendedor de droga en ese complejo de Fonavi del oeste de Rosario.

Cabe destacar que es el décimo asesinato que se registra en los primeros 16 días del año en el departamento Rosario, lo que equivale a una muerte cada 38 horas.