+++
author = ""
date = 2021-12-09T03:00:00Z
description = ""
image = "/images/kgyguipltfhapmytbvjt7e3ss4.jpg"
image_webp = "/images/kgyguipltfhapmytbvjt7e3ss4.jpg"
title = "INCIDENTES EN LA 9 DE JULIO ENTRE LA POLICÍA DE LA CIUDAD Y MANIFESTANTES DE RAÚL CASTELLS"

+++

#### **_El dirigente del Movimiento Independiente de Jubilados y Desocupados (MIJD), Raúl Castells, encabezaba una protesta en la Avenida 9 de Julio este jueves, frente al ministerio de Desarrollo Social de la Nación, y se descompensó en medio de un operativo policial que desalojó la zona para levantar los cortes de calle._**

Durante el operativo se produjeron incidentes, corridas y tiraron gases lacrimógenos para desplazar el corte de los militantes de la organización política. El reclamo apuntaba a la actualización de asignaciones y programas sociales.

“Nosotros lo queríamos era una reunión con ellos. Tenía que ver con las necesidades sociales que hay. Estamos a 15 días de la Noche Buena, la mishiadura (sic) que hay en los barrios. Estamos pidiendo atención social a las familias más humildes”, sostuvo Castells en diálogo con A24, en relación al pedido que reclaman ante la cartera que conduce Juan Zabaleta.

La tensión comenzó alrededor de las 11.30, cuando decenas de personas ocuparon la mitad de la avenida a la altura de la sede gubernamental. Los empujones y hechos de violencia se produjeron cuando el cordón policial de la Policía intentó limitar la marcha y garantizar la circulación en el recorrido del Metrobús.

A raíz del operativo, la manifestación fue desplazada hacia la plazoleta Jujuy, en frente de la oficina gubernamental, y la circulación quedó liberada al tránsito de forma total.

Durante los incidentes, Raúl Castells se descompensó por unos minutos. Más tarde, ingresó al ministerio de Desarrollo Social.

Según María Leiva, dirigente del MIJD, la reunión entre los funcionarios a cargo del ministro Juan Zabaleta y la organización política “estaba pactada”.