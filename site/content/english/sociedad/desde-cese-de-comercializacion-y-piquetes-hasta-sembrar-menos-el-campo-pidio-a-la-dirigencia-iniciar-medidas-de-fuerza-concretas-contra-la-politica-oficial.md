+++
author = ""
date = 2022-01-07T03:00:00Z
description = ""
image = "/images/u74nqftstjgj3jzgy6uxvfccmm-1.jpg"
image_webp = "/images/u74nqftstjgj3jzgy6uxvfccmm-1.jpg"
title = "DESDE CESE DE COMERCIALIZACIÓN Y PIQUETES HASTA SEMBRAR MENOS: EL CAMPO PIDIÓ A LA DIRIGENCIA INICIAR MEDIDAS DE FUERZA CONCRETAS CONTRA LA POLÍTICA OFICIAL"

+++

#### **_En una calurosa mañana en la ciudad de Armstrong, en la provincia de Santa Fe, con la presencia de unos 250 productores, las bases rurales solicitaron a la dirigencia de la Mesa de Enlace realizar medidas de fuerza concretas que expresen el malestar y la indignación del interior con la política del Gobierno nacional hacia el campo. “Hoy lo que recogemos de acá es bronca e indignación”, dijo el titular de CRA, Jorge Chemes._**

Sucedió al cabo de una semana en que se profundizó el malestar de dirigentes y productores con la mayor intervención del Gobierno en las exportaciones de carne vacuna, maíz y trigo, y la falta de medidas que promuevan la inversión y la producción. El campo también volvió a expresar que no es responsable de la suba de precios de los alimentos y trasladó a las autoridades nacionales la inquietud de resolver la inflación que aqueja al país desde hace años.

Una de las propuestas surgidas de la asamblea fue declarar el estado de alerta y movilización, cese de comercialización y piquetes, y en la próxima campaña de cultivos de invierno, que se empieza a definir en los próximos meses, bajar el nivel de uso de tecnología y de producción. Los productores reiteraron su “cansancio” con la realización de asambleas que no conducen a acciones concretas. “No van más las asambleas para hacer catarsis. Tenemos que accionar, porque el Gobierno nos quiere dividir y debilitar, además de perjudicarnos con sus políticas”, dijo un autoconvocado durante la reunión.

El sector reclama la eliminación total de las retenciones, apertura de las exportaciones y el desdoblamiento cambiario, y rechaza la conformación cualquier tipo de organismo que trabe la comercialización de su producción. Por otro lado, hubo un rechazo a los subsidios y compensaciones, y se pidieron medidas de emergencia para enfrentar la sequía que afecta a diferentes regiones productivas, que en el caso de Entre Ríos se mencionó en la reunión que el 80% del maíz sembrado se encuentra en un estado de regular a malo.

##### Asamblea en febrero

Ante el reclamo de la Asamblea, el presidente de la Federación Agraria Argentina, Carlos Achetoni, propuso realizar una nueva asamblea en febrero próximo en la localidad de Alcorta, en Santa Fe, lugar del recordado “Grito de Alcorta”, donde los pequeños productores realizaron una importante protesta en julio de 1912.“

Ahí tenemos que hacer la proclama de los puntos que vamos a ir a impulsar a la Casa de Gobierno y al Congreso, que son los lugares donde se tienen que brindar las herramientas que necesitamos para que nos dejen de pisotear. Vamos a demostrar que tenemos la capacidad de sacar a la gente de la pobreza y no la política, que llegó a la gente a una situación de crisis”, señaló el representante de los pequeños y medianos productores.

Además, el presidente de Confederaciones Rurales Argentinas, Jorge Chemes, dijo que hasta el momento lo que consiguió la Mesa de Enlace en relación a los beneficios para los productores, “ha sido muy poco”, y lo ejemplificó con el nuevo esquema de exportación de carne que presentó esta semana el Gobierno: el dirigente destacó la reapertura de la comercialización de carne de vaca a China, precisó que es mejor que lo que había hace tres meses atrás, “pero no estamos conformes”, sostuvo, y agregó: “El objetivo es la apertura total y a eso vamos”.

Si bien el titular de CRA trató de explicar a los productores presentes en la asamblea el accionar de la Mesa de Enlace y eso no convenció a los autoconvocados que seguían con su reclamo de medidas más contundentes, luego comentó: “Hoy lo que recogemos de acá es bronca e indignación. Y si realmente esto que hemos hecho hasta ahora no da resultado, no tenemos ningún reparo en cambiar el rumbo de las acciones a seguir.

Evidentemente tenemos que ir a más contundencia y más fuerza en el accionar. A eso vamos a tener que ir, porque lo hecho hasta ahora no dio resultado. El triunfo se consigue con gente. Habrá acciones donde vamos a necesitar mucha gente, porque hay muchos que están a favor del ´vayan y hagan´. Y eso lo tenemos que evitar”.

##### Críticas a la oposición

No solamente se escucharon voces críticas de los productores agropecuarios hacia el Gobierno nacional, sino también a la oposición.

En ese sentido, Juan Diego Echevehere, delegado de la Sociedad Rural Argentina en Entre Ríos y hermano del ex ministro de Agricultura durante la gestión de Mauricio Macri, al momento de hacer uso de la palaba en la asamblea, se preguntó “¿Dónde están el Gobierno y la oposición en este momento complicado, con una sequía que es durísima en varias zonas del país?”. En el caso de la mencionada provincia, los cultivos sembrados atraviesan una situación crítica, pero también la ganadería y la lechería.

Por otro lado, Juan Monín, productor de Santiago del Estero, sostuvo que “hay que apretar a la oposición a la que le hicimos una campaña gratis y ahora se fueron todos de veraneo”, a lo que el productor de Santa Fe, Gabriel Cingolani, sostuvo: “Queremos que los legisladores estén del lado del campo y no en Disneylandia”.

Entre los asistentes a la asamblea, estuvieron el diputado nacional por provincia de Buenos Aires por Juntos y también productor agropecuario, Pablo Torello, y el Senador Nacional por Entre Ríos por Juntos y ex presidente de la Federación Agraria de Entre Ríos, Alfredo De Angeli.