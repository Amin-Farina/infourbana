+++
author = ""
date = ""
description = ""
image = "/images/foto-choque.webp"
image_webp = "/images/foto-choque.webp"
title = "CHOQUE EN PALERMO: LOS HERIDOS QUE IBAN JUNTO A LAS VÍCTIMAS PIDEN AYUDA PARA QUE SUS FAMILIAS PUEDAN VIAJAR DESDE VENEZUELA"

+++
#### Jeins Mora y Dakha Curvelo estaban en el Ford Ka que recibió de lleno el impacto del BMW. Esperan que la embajada de su país colabore para que sus madres puedan acompañarlos

Jeins y Dakha iban el domingo por la noche junto a otras cuatro personas a bordo del Ford Ka blanco que recibió de lleno el impacto del BMW M3 negro que circulaba a 150 kilómetros por hora. Juan Carlos Márquez Meneses, de 23 años y su sobrina Jenismar Márquez, de 15, murieron tras el siniestro vial.

Juan -que también era oriundo de Venezuela- estaba sentado en el asiento del conductor del Ford K que quedó completamente destruido. Bomberos tuvieron que cortar los hierros con herramientas hidráulicas para liberarlo. A su lado, en el asiento del acompañante, iba Marianny, su novia, que fue trasladada al Hospital Pirovano.

Juan había llegado al país hace cuatro años. A fines del 2019 obtuvo su título en la Escuela de Pastelería Profesional en Buenos Aires y continuaba sus estudios para recibirse de chef. Actualmente, trabajaba en una de las sucursales de la cadena de Maru Botana.

  
Detrás de Juan iban su mejor amigo, que fue trasladado al Hospital Fernández, Jenismar con su papá y la hermana de Marianny. Según pudo saber Infobae de fuentes cercanas a las víctimas, los seis volvían de una salida en Palermo y se dirigían a la localidad bonaerense de Gerli, donde vive la cuñada del pastelero fallecido.

Al volante del BMW iba el empresario aduanero Roberto Patricelli, de 57 años, que está detenidoy este martes era indagado por el fiscal del caso.

Las pruebas que complican al acusado son, además de las imágenes fílmicas a las que accedió este medio del momento del accidente, la pericia realizada por el Cuerpo de Investigaciones Judiciales que determinó que Patricelli manejaba a 150 kilómetros por hora, más del doble de la velocidad permitida en esa avenida; y el test de alcoholemia realizado en el momento junto con una extracción de sangre. El alcoholímetro marcó 0,51 gramos de alcohol por litro de sangre, un decimal por encima del máximo permitido en la Ciudad.

Diego Olmedo, uno de los dos abogados defensores del empresario, reveló esta mañana que junto a Patricelli viajaban su hija y su hijastra y aseguró que el empresario incluso le negó haber bebido antes de conducir. “Me gustaría ver el análisis de sangre, que le extrajeron en el momento. Roberto me dijo que no había tomado nada. No había cenado todavía”, señaló.

En las imágenes que llegaron a Infobae se ve de distintos ángulos cómo perdió el control del BMW, que iba a altísima velocidad, e impactó de lleno contra el Ford Ka blanco, que esperaba que abra el semáforo en fila junto a otros vehículos: algunos autos se salvaron del impacto, pero el coche en el que iba el joven pastelero y su sobrina de 15 terminó volcado sobre la calzada.

Los otros vehículos damnificados son un Peugeot 207 en el que se encontraban un hombre y una mujer; y una camioneta Toyota RAV4 en la que viajaban dos chicas que descendieron por sus propios medios.

Además de las dos víctimas fatales, otras 11 personas resultaron heridas. Según fuentes del Ministerio de Salud, quedan dos víctimas internadas en el Hospital Fernández: un varón de 25 años con traumatismo de cráneo leve, que incluye una fractura, y otro hombre de 39 años se encuentra en terapia intensiva con traumatismo de cráneo grave. Su estado es crítico.