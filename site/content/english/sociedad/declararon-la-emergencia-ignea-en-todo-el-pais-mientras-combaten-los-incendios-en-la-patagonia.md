+++
author = ""
date = 2021-12-29T03:00:00Z
description = ""
image = "/images/zgxbxwe6jnhm7ixihvtexh6nkm.jpg"
image_webp = "/images/zgxbxwe6jnhm7ixihvtexh6nkm.jpg"
title = "DECLARARON LA EMERGENCIA ÍGNEA EN TODO EL PAÍS MIENTRAS COMBATEN LOS INCENDIOS EN LA PATAGONIA"

+++

#### **_"Estamos ante una situación que necesita obligatoriamente de una coordinación de esfuerzos y una conjugación de voluntades para poder enfrentar un panorama muy adverso; la temporada que viene en la Patagonia es complejísima”, argumentó el secretario de Control y Monitoreo Ambiental, Sergio Federovisky._**

El Consejo Federal de Medio Ambiente (Cofema) y el Ministerio de Ambiente y Desarrollo Sostenible, declararon este martes la "emergencia ígnea en todo el territorio nacional" por un un año, en función del riesgo extremo de incendios de bosques y pastizales, mientras unos 300 brigadistas y 17 medios aéreos trabajan denodadamente para controlar los incendios en la Patagonia, en zonas del Parque Nacional Nahuel Huapi, la Comarca Andina y Aluminé.

El secretario de Control y Monitoreo Ambiental, Sergio Federovisky, consideró que hay "un escenario que puede tender a repetirse de manera recurrente en el tiempo, es decir, un periodo de sequías prolongadas, de corrimiento de las temporadas secas en cada estación y de temporadas sin lluvias".

"Muy probablemente, como resultado del cambio climático, puedan presentarse temporadas de fuego a lo largo de todo el año, situación que implica un abordaje muy diferente al que veníamos teniendo hasta ahora”, advirtió.

El funcionario explicó que, en este contexto, será necesario “trabajar con un énfasis diferente y más profundo en las políticas de prevención. Las políticas del combate contra el fuego en situaciones climática adversas y con tanto material combustible en el suelo, resultan ineficientes más allá de todos los recursos a disposición que se pongan”.

Y agregó que “la necesidad de trabajar en la prevención es esencial, entendiendo por esta acción la reducción de las condiciones que favorecen la llegada y la posterior propagación de los incendios una vez que la temporada de fuego ya está lanzada”.

Respecto a la tarea realizada desde el Estado, Federovisky dijo que se hizo "un esfuerzo colosal en materia institucional para recuperar el Servicio Nacional del Manejo del Fuego para el Ministerio de Ambiente y para dotar a ese sistema de los recursos económicos que jamás tuvo ni tendría mediante los mecanismos habituales de presupuesto oficial”.

“La declaración de la emergencia ígnea es importante porque estamos ante una situación que necesita obligatoriamente de una coordinación de esfuerzos y una conjugación de voluntades para poder enfrentar un panorama muy adverso; la temporada que viene en la Patagonia es complejísima”, finalizó el secretario.

Mientras tanto, en la Patagonia unos 200 brigadistas y una docena medios aéreos trabajaban para controlar los incendios en zonas del Parque Nacional Nahuel Huapi, la Comarca Andina y Aluminé, con el foco "más complejo" ubicado en los lagos Martin y Steffen, cerca de Bariloche, informó a Télam Federovisky, quien precisó además que hay focos activos en siete provincias.

"Siempre es preocupante que haya incendios y estamos en un momento climático muy adverso por una sequía muy pronunciada y extendida", dijo Federovisky, quien atribuyó a esa situación "que en esta época tengamos una proliferación de incendios en distintas áreas que no es lo habitual para esta temporada".

Esta tarde 10 brigadistas provenientes de Santiago del Estero y 20 de la provincia de Buenos Aires llegarón a Neuquén y se suman a los 80 que están en ese territorio junto a cuatro aviones hidrantes y cuatro helicópteros, informó el Ministerio de Ambiente

En los focos activos de Bariloche trabajan 133 brigadistas y operan tres aviones y tres helicópteros, mientras que en los focos de Cushamen y Tehuelches, en la provincia de Chubut, se desplegaron 49 brigadistas y se enviaron dos aviones hidrantes y un helicóptero.

Voceros de la provincia de Río Negro destacaron por su parte que "sólo operan los helicópteros debido a las condiciones de visibilidad dado por la generación de humo en el fondo del valle de los Lagos Martin y Steffen"

A los focos ígneos de Río Negro, Neuquén y Chubut, se suman incendios en Santa Fe, San Luis, Formosa y Misiones.

En el sur del país, el foco "más complejo" es el de los lagos Martin y Steffen en el departamento de Bariloche, que "todavía no ha podido ser controlado ni direccionado", por lo que presenta "un grado de dificultad muy alto", precisó Federovisky.

"Los otros dos, el de Aluminé y la Comarca, en Lago Puelo, están relativamente contenidos", añadió.

El Ministerio de Ambiente y Desarrollo Sostenible, a través del Servicio Nacional de Manejo del Fuego (SNMF), continúa con el envío de recursos para combatir los incendios forestales en la Patagonia, adonde arribarán otros 10 brigadistas provenientes de Santiago del Estero y 20 de la provincia de Buenos Aires, que se suman a los 80 que ya están prestando servicio en Neuquén. Además, en esta provincia operan cuatro aviones hidrantes y cuatro helicópteros dispuestos por Nación.

En los focos activos de Bariloche trabajan 133 brigadistas y operan tres aviones y tres helicópteros enviados por el SNMF; en Cushamen y Tehuelches, en la provincia de Chubut, Ambiente desplegó 49 brigadistas y envió dos aviones hidrantes y un helicóptero.

En Río Negro también se encuentra la directora de operaciones del Servicio Nacional del Manejo del Fuego (SNMF), Lorena Ojeda, y el coordinador de la Regional Patagonia, Ariel Amthauer.

En tanto, en Neuquén, están trabajando en conjunto 80 brigadistas nacionales del SNMF y de la Administración de Parques Nacionales con al apoyo aéreo de cuatro aviones hidrantes y cuatro helicópteros con helibalde.

"Tenemos más de 200 brigadistas trabajando, por lo tanto tenemos los recursos suficientes pero las condiciones para operar son muy malas y esto hace muy difícil atacar el fuego por aire principalmente por el humo que hace bastante complejo poder volar", explicó Federovisky a Télam.

Respecto a las orígenes de estos incendios, el viceministro detalló que "fueron iniciados durante tormentas eléctricas sin precipitaciones, es decir rayos que cayeron sobre áreas de forestación nativa y produjeron las primeras chispas a través de las cuales después se propagó el incendio".

"La condición de sequía hace que hoy todo arda con una facilidad notable. Por suerte hasta el momento la mayor parte de los incendios no han sido intencionales", subrayó.

En cuanto al peligro para poblaciones cerca de los focos activos, el secretario de Control y Monitoreo Ambiental indicó que "se evacuó preventivamente a un puñado de habitantes de comunidades mapuches, pero no fue necesario más porque no hay por el momento amenaza a zonas urbanas".

Los incendios forestales traen aparejada la inevitable pérdida de bosque nativo y biodiversidad, lo que representa una amenaza para la riqueza forestal, la flora y la fauna, perturbando la ecología y el ambiente en general.

Federovisky se refirió a los incendios próximos a las ciudades de Santa Fe y Rosario que afectan zonas de islas, la costa de ríos y bañados, y descampados.

"Tenemos ahí todavía una persistencia muy importante", señaló, e indicó que las causas son derivadas de "la sequía extrema, la bajante del Río Paraná y la aparición de inescrupulosos que utilizan el fuego en una circunstancia que no debieran utilizarlo".

En este sentido, aseguró que "la mayor parte de ellos son intencionales y son iniciados por quienes quieren sacar provecho de la situación de sequía para quemar sus pastizales o provocar un avance de la frontera agropecuaria".

En tanto, el director de Protección Civil de la provincia, Roberto Rioja, informó que habrá "tres o cuatro días muy complicados, con altas temperaturas".

En el norte del país, la localidad formoseña de Pilcomayo vive una situación similar con incendios de pastizales favorecidos por "las altísimas temperaturas", dijo Federovisky.

También en Misiones, un incendio en la reserva del Zaimán avanzó esta mañana sobre unas viviendas ubicadas en las afueras de Posadas y cubrió de humo gran parte de la capital hasta que pudo ser controlado por los bomberos, y el Gobierno provincial advirtió sobre "el índice de peligrosidad" de que se generen nuevos focos por altas temperaturas, falta de lluvias y vientos.

La séptima provincia con focos ígneos es San Luis, con el fuego en la zona de Junín, según el reporte diario de SNMF.

Federovisky aconsejó, que de cara a una Patagonia "repleta de turistas y con una situación climática desfavorable hay que pedir que hacer fuego".

"Y si es absolutamente imprescindible, debe hacerse en las áreas únicamente habilitadas y tomar el recaudo de apagar hasta la última chispa", concluyó.