+++
author = ""
date = 2021-11-05T03:00:00Z
description = ""
image = "/images/75472711_521958318384674_1543669009197563904_njpg-1.jpg"
image_webp = "/images/75472711_521958318384674_1543669009197563904_njpg.jpg"
title = "\"SE FUE A HACER UN MANDADO Y ME LO DEVOLVIERON EN UN CAJÓN\": EL DOLOR DEL PADRE DEL "

+++

##### **_Se trata de Isaac Muñoz, el joven de 23 años que recibió un disparó fatal el miércoles al mediodía a manos de dos delincuentes que le robaron la moto. Ocurrió en Capitán Bermúdez, departamento de San Lorenzo_**

“Se fue a hacer un mandado y ahora me lo entregan en un cajón”, expresó con dolor el papá de Isaac Muñoz, el joven peluquero de 23 años que fue asesinado por motochorros, el miércoles al mediodía cuando se dirigía en moto a comprar facturas para compartir en familia. Por el crimen hay cinco detenidos. Ocurrió en Capitán Bermúdez, unos 10 kilómetros al norte de Rosario, en el departamento de San Lorenzo, Santa Fe.

El joven fallecido circulaba en su Honda Titán 150 cc. por barrio Villa Margarita de la citada localidad cuando dos delincuentes que iban en otra moto, en el marco de un asalto le dispararon un balazo que le atravesó el pecho. El muchacho cayó herido a pocos metros del lugar donde lo habían cruzado, y pese a que estaba malherido en el piso, los delincuentes lo golpearon, le robaron la moto y huyeron.

Mientras esperaban la llegada de la ambulancia, que tardó unos 15 minutos, un odontólogo que estaba a pocos metros de donde ocurrió el hecho, intentó auxiliarlo para frenar la hemorragia pero fue en vano ya que la víctima falleció por la gravedad de la herida.

El papá del peluquero comentó con tristeza que se enteró por un vecino sobre el robo. “Cuando llegamos ahí ya estaba muerto”, relató con conmoción y remarcó: ““Todavía no caigo”, sobre la reciente perdida de su hijo. En esa línea, Marisol, la hermana de Isaac, lo recordó como un “chico laburante, que alquilaba su departamento, que quería progresar e instalar otras peluquerías en Bermúdez”. “Era un chico lleno de sueños y de proyectos”, se lamentó.

La investigación que conduce el fiscal Juan Carlos Ledesma llevó a la policía realizar varios allanamientos en las horas posteriores al homicidio. Además de la captura de 5 sospechosos, encontraron partes de una moto idéntica a la que le robaron al joven.

Un elemento ser clave para la causa es un buzo amarillo, prenda que vestía uno de los asaltantes que quedó registrado en el material recopilado por las cámaras de video.

Por su parte, el ministro de Seguridad de Santa Fe, Jorge Lagna, sostuvo que hubo una “reacción inmediata” frente al episodio. “Nos ayudó mucho el sistema de videovigilancia de Capitán Bermúdez”, destacó sobre las medidas para esclarecer el hecho.

Todavía resta confirmar si entre los sospechosos está el autor del disparo. El ministro de Seguridad santafesino consideró “probable” que los detenidos estén involucrados en otros hechos similares.