+++
author = ""
date = 2022-02-09T03:00:00Z
description = ""
image = "/images/puerta-8jpg-1.jpg"
image_webp = "/images/puerta-8jpg.jpg"
title = "UN INVESTIGACIÓN PRIVADA DETERMINÓ QUE LA DROGA ENVENENADA TENÍA UN PRECURSOR DEL FENTANILO"

+++

##### **_Aseguraron que la cocaína adulterada que provocó la muerte de 24 personas en el Conurbano bonaerense tenía rastros de piperidina, un precursor del fentanilo._** 

El resultado se obtuvo a partir de un examen realizado en un instituto privado, y no a la investigación que llevan adelante las autoridades bonaerenses.

La piperidina fue hallada en una muestra de droga envenenada obtenida por el doctor Rodrigo Salemi, quien aseguró que la sustancia es de origen asiático y está prohibida en la Argentina.

> “Nosotros lo que hicimos fue comprar una muestra que habíamos obtenido a través de un abogado en el programa ‘Todo en Uno’ (América) que conduce Rolando Graña y nos comprometimos a analizarla. Lo hicimos en un laboratorio privado mediante un cromatógrafo lineal de alta eficacia”, detalló el especialista en diálogo con radio Rivadavia.

Al comparar una pequeña porción de droga adulterada con el fentanilo a través de diversos procesos científicos, llegaron a la conclusión que la muestra contenía piperidina.

> “Lo que se encontró es un precursor del fentanilo. Pero no es el fentanilo comercial que nosotros conocemos. Este es un fentanilo de muy baja calidad”, afirmó el médico.

Salemi informó que la piperidina es sintética. “No es la sustancia que se busca en Google y dice que se utiliza como veneno de hormigas”, diferenció.

Pese a que aún se esperan resultados oficiales de la pericia que realiza la Procuración, el ministro Berni avaló la investigación privada encabezada por Salemi y ratificó la presencia de piperidina en las dosis de droga adulterada.

Según comentó el funcionario a los canales C5N y A24, “la sustancia encontrada es un precursor del fentanilo”. “El fentanilo es una droga que se clasifica dentro de los opioides. Esos opioides son naturales, como la heroína, o sintéticos, que se hacen en un laboratorio. Lo que es natural tiene siempre la misma fórmula, pero los sintéticos se pueden cambiar la fórmula. Y si se hace de manera casera, son fórmulas extremadamente complejas. Y si uno se equivoca o calcula mal el tiempo de una reacción a otra, puede obtener una cosa parecida al fentanilo, que no es fentanilo”, dijo.

Expertos toxicólogos consultados por Infobae habían adelantado que la droga envenenada contendría un cóctel de derivados de opiáceos, entre ellos sustancias vinculados al fentanilo, la sustancia mortal que avanza desde China.

##### A la espera de la pericia oficial

Los peritos -que no dependen de la Policía sino de la Procuración bonaerense- abocados a analizar las muestras de la cocaína que causó 24 muertes y más de 80 intoxicados en los partidos bonaerenses de San Martín, Tres de Febrero y Hurlingham aún no pudieron identificar la sustancia de corte que se usó para contaminar los estupefacientes.

Altas fuentes en la Procuración bonaerense niegan que la piperidina haya sido encontrada en el estudio oficial con las muestras incautadas en Puerta 8 y los análisis de sangre y orina a las víctimas. Es decir, el testeo por la sustancia, que fue expresamente buscada, dio negativo en el análisis de la Justicia.

Las fuentes consultadas por Infobae, con acceso al expediente, objetan la cadena de custodia y el origen de la muestra empleada en el estudio anunciado por Salemi y repetido por Berni, así como su resultado, que no fue notificado oficialmente en el expediente de la UFI N°16 de San Martín que investiga los homicidios de 24 personas.

También, en el curso del fin de semana, un resultado inicial dio negativo para fentanilo: un segundo estudio se realizaba sobre las muestras para descartar un falso resultado, ya que es una sustancia infrecuente en los peritajes toxicológicos realizados en Argentina. Su uso clandestino en la historia reciente estuvo reservado a actividades ilegales de anestesistas y robos en hospitales.

Los laboratorios donde se están analizando los estupefacientes secuestrados son dos: uno se encuentra en la localidad de Munro y el otro en La Plata. Paralelamente, el Ministerio de Salud de la provincia hace estudios sobre las muestras de sangre y orina que se les tomaron a los intoxicados.