+++
author = ""
date = 2021-09-29T03:00:00Z
description = ""
image = "/images/ferro_1.jpg"
image_webp = "/images/ferro_1.jpg"
title = "DERRUMBE EN EL ESTADIO DE FERRO"

+++
#### **_Las impactantes imágenes del derrumbe de un sector de la nueva tribuna que construye Ferro Carril Oeste en su estadio del barrio porteño de Caballito causaron conmoción y sorpresa._**

La caída se dio cuando la estructura de madera iba a ser rellenada con hormigón y el accidente provocó heridas leves a seis trabajadores que fueron trasladados por el Servicio Ambulatorio Médico de Emergencia (SAME) a los hospitales Álvarez y Durand, informaron fuentes policiales y de la institución.

Minutos más tarde se conoció que el lugar del derrumbe iba a recibir al presidente Alberto Fernández el próximo sábado en medio de un acto convocado por el Movimiento Evita y Barrios de Pie bajo la consigna: “Por la Unidad y la Victoria”. El objetivo, según habían indicado era llevar adelante un “plenario nacional de la militancia de cara a las elecciones del 14 de noviembre”.

Los organizadores del encuentro aseguraron que se trabaja para ver qué pasará con el acto. La principal opción es que el mismo se traslade a otro lugar, aunque no se descarta una postergación de la fecha. “Ahora estamos tratando de conseguir lugar para hacerlo este sábado”, dijeron a este medio.

En la convocatoria se indicaba que “el plenario, que se realizará en el estadio de Ferrocarril Oeste desde las 15 horas, el presidente Alberto Fernández será el principal orador”.

Además indicaban que las organizaciones convocantes realizarían actos similares simultáneamente en todas las provincias argentinas. “De esta forma se dará comienzo formalmente a la campaña para las elecciones legislativas de noviembre. Cabe acotar que las organizaciones populares ya se encuentran recorriendo los barrios, hablando con los vecinos y vecinas y llevando la propuesta a cada barrio de la Argentina con la intención de revertir el resultado de la última PASO”, cerraba la invitación.

Sin embargo, desde Presidencia dijeron a este medio que no está confirmada la presencia de Alberto Fernández en el evento, aunque tampoco se descarta que concurra.

#### El derrumbe

“Lo que se cayó es un sector de madera que se estaba rellenando con hormigón en la tribuna que se está construyendo sobre la calle Avellaneda en nuestro estadio”, dijo a la agencia oficial Télam el presidente de Ferro, Daniel Pandolfi.

Por el derrumbe seis trabajadores sufrieron golpes, por lo que fueron trasladados de manera preventiva por el SAME los hospitales Álvarez y Durand, mientras otros dos fueron atendidos en el lugar, indicaron las fuentes policiales. Además, los directivos del club señalaron que los trabajadores trasladados “se encuentran en observación”.

En el derrumbe trabajaron al menos tres dotaciones de bomberos que llegaron hasta el club situado en la avenida Avellaneda y Federico García Lorca, donde vallaron y cortaron la calle para impedir el acceso de personas.

El sector de la tribuna que se derrumbó es de madera y estaba siendo llenado con hormigón desde la calle por una grúa rellenadora que posee un brazo para cumplir con esa obra.

La tribuna que construye el club de Caballito está en su última fase, ya tiene terminados siete cuerpos de cemento y dos bandejas y el sector que se derrumbó corresponde al último cuerpo de la tribuna que no posee bandeja superior, indicaron desde el club.

La obra fue iniciada hace más de un año y medio y se estima que se completaría en los primeros meses del 2022 cuando podría ser inaugurada, según informó Pandolfi.

Pandolfi afirmó que “entre 10 y 11 personas” trabajaban en la obra en el club en el momento del derrumbe, mientras unos 350 empleados lo hacían en las instalaciones.