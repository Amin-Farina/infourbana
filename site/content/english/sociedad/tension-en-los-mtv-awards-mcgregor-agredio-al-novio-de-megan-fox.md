+++
author = ""
date = 2021-09-13T03:00:00Z
description = ""
image = "/images/611700276_208104333_1024x576-1.jpg"
image_webp = "/images/611700276_208104333_1024x576.jpg"
title = "TENSIÓN EN LOS MTV AWARDS: MCGREGOR AGREDIÓ AL NOVIO DE MEGAN FOX"

+++

##### **_Conor McGregor continúa dando de qué hablar no solo en lo que respecta a lo deportivo sino también a lo mediático después de su última aparición pública en el prestigioso evento de los MTV Video Music Awards en el Barcklays Center de Nueva York, un pabellón deportivo de múltiples usos que el peleador conoce muy bien por las veladas que realiza la UFC._**

Fue allí en donde el luchador irlandés protagonizó un nuevo escándalo al discutir públicamente con el rapero Colson Baker, más conocido como Machine Gun Kelly, en plena alfombra roja. Los guardaespaldas tuvieron que actuar rápidamente para separarlos y que el altercado no pasara a mayores.

Según informó el portal norteamericano TMZ, la discusión habría empezado cuando la estrella de UFC se acercó al cantante y a su novia, Megan Fox, para pedirles una foto, sin embargo un comentario del músico provocó la reacción de The Notorious, quien no dudó en increparlo.

Los fanáticos no tardaron en prender las cámaras de sus celulares para grabar lo que estaba ocurriendo y hacerlo viral rápidamente. En las imágenes se pudo ver al luchador, con un traje rosa, arrojando el contenido de un vaso que estaba tomando hacia donde estaba Kelly, vestido de rojo.

Rápidamente el personal de seguridad se metió en la escena para separarlos, sobre todo a McGregor, que se lo pudo ver más furioso y predispuesto a pelear. Incluso un testigo aseguró a TMZ que, “se pelearon, luego todo se calmó y Conor fue por él de nuevo”.

Según indicó la misma fuente, algunos fotógrafos “no estaban contentos” y estaban “diciendo que iban a boicotear a Conor porque estaban preocupados de que MGK y Megan no caminaran por la alfombra roja”.

En los videos en cuestión también apareció Megan Fox en varias oportunidades tratando de separar a su novio del tumulto. “No podemos decir nada”, declaró la actriz a la revista Variety al ser consultada por el incidente.

“No pasó absolutamente nada”, aseguró por su parte el luchador de MMA, en diálogo con Entertainment Tonight. “Son solo rumores. ni siquiera conozco a ese chico. Seré honesto, solo son rumores.Solo peleo con luchadores de verdad. Gente que pelea de verdad. Realmente no peleo con raperos. No lo conozco, no sé nada de él, excepto que está con Megan Fox”, agregó.

Mientras que Machine Gun Kelly, que ganó en la categoría “Mejor Alternativo” por su canción “My Ex’s Best Friend”, prefirió no realizar declaraciones, el representante de McGregor solo se limitó a decir a Page Six que “Conor solo pelea contra peleadores”.