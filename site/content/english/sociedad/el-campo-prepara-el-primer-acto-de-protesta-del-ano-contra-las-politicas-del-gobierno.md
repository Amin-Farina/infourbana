+++
author = ""
date = 2022-01-01T03:00:00Z
description = ""
image = "/images/awubrvrc45du7agyc4qlizzenu.jpg"
image_webp = "/images/awubrvrc45du7agyc4qlizzenu.jpg"
title = "EL CAMPO PREPARA EL PRIMER ACTO DE PROTESTA DEL AÑO CONTRA LAS POLÍTICAS DEL GOBIERNO"

+++

#### **_El sábado 8 de enero se realizará una asamblea de productores en Cañada de Gómez, Santa Fe, a la que fue invitada la Mesa de Enlace_**

En el inicio de 2022, vale tener en cuenta dos aspectos del mundo agropecuario. En primer lugar, desde el ámbito privado proyectan otro año de fuerte aporte a la economía nacional, vía la cosecha de granos y las exportaciones agroindustriales, condicionadas a la situación climática, dada la escasez de precipitaciones en varias zonas del país. Por otro lado, seguirá muy vigente el reclamo de cambios en las políticas hacia el sector.

En cuanto a lo primero, la Bolsa de Comercio de Rosario proyectó para la actual campaña agrícola una siembra récord de 38,8 millones de hectáreas, que llevaría a una cosecha total de 144,5 millones de toneladas. Un contexto de menores precios internacionales que en 2021 se compensaría con un mayor volumen, que permitiría exportar unas 104 millones de toneladas, con un aporte de 38.400 millones de dólares.

Pero más allá de su ímpetu y potencia productiva, en el interior hay un gran malestar con las políticas del Gobierno de Alberto Fernández hacia el sector: alta presión impositiva, desdoblamiento cambiario e intervención en los mercados de carne vacuna, maíz y trigo, con el alegado objetivo de hacer bajar los precios de los alimentos, algo que no se consiguió porque no se resuelve la inflación ni se ordena su macroeconomía. Diferentes informes precisaron la baja incidencia del sector primario en el precio de los alimentos, en donde sí pesa, y mucho, la carga impositiva.

En el marco de una relación cada vez más distante entre las autoridades nacionales y la dirigencia agraria de la Mesa de Enlace, el sábado 8 de enero se realizará la primera asamblea de productores del año. Será a las 9:30hs en las instalaciones del Club Atletico Newell´s Old Boys de Cañada de Gómez, en la provincia de Santa Fe. Será al aire libre, con los protocolos correspondientes de prevención contra el coronavirus, uso de tapabocas y distanciamiento social.

Si bien hasta el momento no confirmó su asistencia, a la reunión fue invitada la Mesa de Enlace nacional y también provincial. El objetivo del encuentro, organizado por productores autoconvocados (muchos de ellos estuvieron al frente del multitudinario acto rural del 9 de julio de 2020 en San Nicolás), es acercar a la dirigencia con sus representados y empezar a definir los pasos a seguir. No se descarta que la asamblea sea la semilla de otras reuniones en zonas productivas del país.

Por otro lado, se decidió suspender otra Asamblea que se iba a realizar también en Santa Fe: productores autoconvocados planeaban reunirse en la localidad de Wheelwright, pero por el aumento de casos de coronavirus el encuentro se postergó hasta nuevo aviso.

##### Encuentro

Iván Castellaro, productor autoconvocado del distrito santafesino de Armstrong y uno de los organizadores de la reunión en Cañada de Gómez, dijo que “acá en nuestra zona nosotros trabajamos siempre mancomunadamente con las entidades, que son nuestros pares, nuestros amigos, y lo venimos haciendo desde hace tiempo muy bien. Es por eso que surge la necesidad de tener una charla con los presidentes de la Mesa de Enlace y que se puedan acercar a los productores, ya que consideramos que los dirigentes son los interlocutores válidos. 

Estamos frente a un Gobierno que no escucha y que no tiene interés de apostar a la producción, y a los dirigentes se les hace muy difícil negociar cosas que luego no se respetan, porque los funcionarios no cumplen sus promesas”.

Además, Castellaro se refirió a la situación actual de los productores: “Desde hace dos años el Gobierno viene pegándole a todo el sector productivo. No hay actividad agropecuaria que sea rentable: hoy la lechería está pasando por un momento complejo, lo mismo pasa con las economías regionales, y con un desdoblamiento cambiario que es peor que las retenciones, porque compramos los insumos a un dólar de 200/210 pesos y cobramos un dólar de 60 pesos. A todo esto, de cada 100 pesos que genera una hectárea agrícola, casi 70 pesos se los lleva el Estado sin invertir un peso. Esto es inviable y si seguimos así en unos años no queda un productor en pié”, señaló.

Por último, el productor habló de la importancia de la unidad del sector productivo para revertir la difícil situación, y trabajar para que el país logre una mayor producción, de la mano de una nueva política hacia el sector agropecuaria que promueva la inversión y la confianza. “Cuando apostamos a un país previsible y a futuro, necesitamos una menor presión impositiva, un único tipo de cambio, entre otras medidas”, concluyó Iván Castellaro.