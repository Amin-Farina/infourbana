+++
author = ""
date = 2022-01-28T03:00:00Z
description = ""
image = "/images/winim-2jpg-1.jpg"
image_webp = "/images/winim-2jpg.jpg"
title = "JOVENES EMPRENDEDORES Y COMIDA BARATA: DEJARON SUS TRABAJOS, JUNTARON US$100.000 Y LANZARON UN NEGOCIO EXITOSO"

+++

#### **_Tres emprendedores crearon una startup que ayuda a los locales gastronómicos a vender el excedente de alimentos que tienen a diario_**

Tres socios, un propósito: disminuir el excedente de alimentos que se descartan. En la Argentina se tiran por año 16 millones de toneladas de comida en buen estado. Esto genera la misma cantidad de gases de efecto invernadero que todos los autos de la Ciudad de Buenos Aires.

Además, los desperdicios anuales alcanzan como para alimentar a 3,5 millones de personas. En el caso de Latinoamérica, el desperdicio es de 127 millones de toneladas.

Esta situación inspiró a Santiago López Silveyra, Federico Broggi y Santiago Guglielmetti, tres jóvenes amigos, a fundar Winim. Con una inversión inicial de US$100.000, crearon una startup que ayuda a los locales gastronómicos a vender el excedente de comida diaria. A través de una aplicación de celular, que funciona como un market place (una plataforma de comercio online), los locales y empresas alimenticias adheridas revenden los productos que les sobran en el día o que tienen en sobre stock.

En 2019 salvaron 6.000 productos, en 2020, 60.000; en 2021, 150.000. Son 20.000 platos por mes, aproximadamente. Desde enero 2020 registran más de 200.000 kilos de comida salvada. Es el equivalente a 500.000 kilos de CO2, lo mismo que emite un avión que da 50 veces la vuelta al mundo.

El proyecto surgió en 2019, mientras terminaban de cursar una maestría y buscaban desarrollar un proyecto de impacto positivo. “Con mis socios queríamos hacer algo para generar un cambio, no solo ambiental, sino también en las personas. Veíamos que en Europa este modelo de negocio es una tendencia desde hace tiempo, pero en la Argentina todavía no estaba desarrollado. Cuando terminamos la maestría, dejamos nuestros trabajos en empresas multinacionales para dedicarnos de lleno a emprender”, comentó López Silveyra, licenciado en Economía Empresarial.

El negocio funciona como un modelo de triple impacto. Los comercios y empresas asociadas a la aplicación generan un ingreso extra con lo que antes era pérdida. Los usuarios consiguen productos de muy buena calidad con descuentos de entre el 50 y 60 por ciento. Como resultado, se evita el desperdicio de alimento.

“Somos un market place donde la gente no solo consigue comida a bajo costo; también está aportando a una causa mayor, colaborando con la reducción del impacto ambiental”, contó López Silveyra.

La app funciona únicamente en CABA en formato de delivery y take away, y abarcan desde restaurantes y pequeños comercios hasta empresas productoras de alimentos. Cuentan con 600 restaurantes inscriptos, entre 350 y 400 activos, y 8000 usuarios que utilizan la app todos los meses. Adherirse no tiene costo, pero se cobra comisión por plato vendido.

Entre los proyectos futuros, el próximo paso es internacional. En abril, van a abrir operaciones en Colombia (Bogotá y Barranquilla). “Queremos seguir difundiendo y generando consciencia sobre el cuidado del medio ambiente y la sustentabilidad. Son regiones donde hay mucho para hacer”, explicó López Silveyra.