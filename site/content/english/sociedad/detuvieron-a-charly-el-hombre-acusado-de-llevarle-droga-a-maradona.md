+++
author = ""
date = 2021-09-03T03:00:00Z
description = ""
image = "/images/1630689819890f1280x720-459382_591057_5033-1.jpg"
image_webp = "/images/1630689819890f1280x720-459382_591057_5033.jpg"
title = "DETUVIERON A \"CHARLY\", EL HOMBRE ACUSADO DE LLEVARLE DROGA A MARADONA"

+++

A mediados de 2020, Carlos Orlando Ibáñez vivó un tiempo con Diego Maradona en la casa de Brandsen. Pero desapareció: señalado como la persona que le daba alcohol y marihuana por la familia y numerosos testigos en la causa que investiga la muerte del ídolo, “Charly” -el apodo con el que lo conocían- era buscado por robo agravado por uso de arma y fue detenido este jueves por la noche.

Ibáñez, que estaba prófugo desde noviembre de 2017, ingresaba al country en el que vivía el por entonces DT de Gimnasia, con un DNI falsificado. Según indicaron los familiares de Maradona, llegó al entorno de la mano de Rocío Oliva, a mediados de 2018. Incluso, en los audios se puede notar la preocupación de Leopoldo Luque sobre la mala influencia de “Charly” en el astro.

Hay diferentes versiones que explican por qué Ibáñez dejó la casa del ídolo: desde la participación de un robo dentro del country hasta un fuerte enojo de Verónica Ojeda por el descontrol en la casa del country, que incluían pequeños robos. En ese momento, no solo le facilitaba alcohol y marihuana, si no también que le facilitaría el acceso ”a demanda” de la medicación psiquiátrica que le era recetada al excapitán de la Selección.

Aunque “Charly”, de 27 años, no está imputado en el caso que investiga la muerte del Diez, tenía un pedido de captura ordenado por Horacio Alejandro Lago, titular del Juzgado de Garantías N°1 de Morón, por el delito de robo agravado por el uso de arma de fuego, indicaron fuentes policiales

Este jueves por la noche, fue detenido por personal de la comisaría 3ra. de San Isidro, tras un llamado al 911, que alertaba sobre cuatro sospechosos que estaban arriba de un Peugeot 307 en el cruce de las calles Junín y Azopardo de la localidad de Boulogne. El exasistente de Diego llevaba una licencia de conducir trucha con el nombre de Federico Emiliano Quintans.

Ante el arribo del patrullero, uno de los cuatro sospechosos intentó refugiarse en una casa cercana, pero fue reducido.

Según las fuentes, los policías comenzaron a ser agredidos a piedrazos por vecinos de la zona, por lo que se dispuso trasladar el auto y a los cuatro sospechosos a la comisaría.

Consultada la fiscal Paula Hertrig, de la Unidad Funcional de Instrucción (UFI) descentralizada de Boulogne, dispuso la identificación de los cuatro demorados y la requisa del vehículo.

En el baúl del rodado, la policía encontró un bolso rectangular de tela color negro, en cuyo interior había dos equipos de comunicación tipo handy, pero al revisar el torpedo del auto, hallaron un habitáculo oculto en el sitio donde va el estéreo, donde había una pistola calibre .9 milímetros marca Pietro Beretta, con numeración limada.

La policía luego identificó a los apresados y determinó que uno de ellos era el fugitivo Ibáñez, mientras que los otros tres eran dos jóvenes de 20 y 18 años y un adolescente de 14.

Los tres mayores de edad, entre ellos Ibáñez, quedó aprehendido imputados del delito de “tenencia ilegal de arma de guerra”, mientras que por el adolescente se le dio intervención a la Fiscalía del Fuero Penal Juvenil de San Isidro.