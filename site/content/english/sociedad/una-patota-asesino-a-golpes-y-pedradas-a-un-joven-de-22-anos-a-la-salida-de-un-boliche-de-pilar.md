+++
author = ""
date = 2022-01-17T03:00:00Z
description = ""
image = "/images/gy2ydi3gqnemnk3szpzpvsynhi.jpg"
image_webp = "/images/gy2ydi3gqnemnk3szpzpvsynhi.jpg"
title = "UNA PATOTA ASESINÓ A GOLPES Y PEDRADAS A UN JOVEN DE 22 AÑOS A LA SALIDA DE UN BOLICHE DE PILAR"

+++

#### **_Un joven de 22 años murió en las últimas horas en el Hospital Sanguinetti, donde se encontraba internado tras haber sido brutalmente atacado por tres hombres y dos mujeres a la salida de un boliche de Pilar. Por el hecho hay un detenido y buscan al resto de los agresores._**

La víctima, Brian Cuitiño, había ido a bailar con su hermana de 19 años. De acuerdo al relato de los testigos y por causas que aún se desconocen, dentro del local los jóvenes habrían tenido una discusión con otro grupo y decidieron volverse a su casa. Sin embargo, en la calle se desató el trágico ataque.

En base a la reconstrucción, al salir del boliche Napoleón la hermana de Brian se fue a buscar el auto que habían dejado en un estacionamiento y él siguió caminando por la calle Las Magnolias hacia la ruta 8. En ese trayecto fue que lo interceptó un Ford Focus gris, del cual bajaron dos mujeres y tres sujetos.

El grupo arremetió a golpes y patadas contra el joven y lo tiró al piso, pero aún cuando se encontraba indefenso no dejaron de golpearlo. De hecho, las dos mujeres le tiraron pedradas en la cabeza hasta que quedó inconsciente. Brian fue trasladado de urgencia al Hospital Sanguinetti, donde finalmente murió este domingo por la tarde pese al esfuerzo de los médicos por salvarlo.

##### La investigación por el crimen de Brian Cuitiño

Horas después, el relevamiento de los videos registrados por las cámaras de seguridad de la zona permitió identificar el auto en el que se movilizaba la patota y después a su dueño, un joven de 22 años llamado Lucas Castillo.

Aunque el sospechoso no estaba en su casa cuando los efectivos fueron a buscarlo, los medios locales indicaron que en el lugar incautaron un pantalón de jean azul con agujeros, una remera de color azul y un par de zapatillas Nike de color negro con vivos naranjas, la misma ropa que de acuerdo a los videos llevaba puesta al momento del hecho.

Finalmente, el joven fue detenido en su lugar de trabajo, una empresa ubicada en la intersección de las calles Ingeniero Eifel y Avenida Constituyentes, de la localidad de Garín. La causa es investigada por el fiscal Gonzalo Agüero, que busca ahora dar con el resto de los agresores.