+++
author = ""
date = 2022-01-20T03:00:00Z
description = ""
image = "/images/171129-policialtablada-11364106794.jpg"
image_webp = "/images/171129-policialtablada-11364106794.jpg"
title = "LAFERRERE: ACRIBILLARON A UN POLICÍA PARA ROBARLE SU ARMA Y LA CAMIONETA"

+++

#### **_La víctima murió en medio de la operación a la que era sometido tras ser herido por los delincuentes._**

Un efectivo retirado de la Policía Federal fue asesinado en la localidad bonaerense de Gregorio de Laferrere en el marco de un asalto, en el que le robaron su camioneta y el arma reglamentaria con la que intentó defenderse, aunque no llegó a efectuar ningún disparo.

El hecho ocurrió el jueves por la noche alrededor de las 23.30. La víctima murió en medio de la operación a la que fue sometido de urgencia, tras las heridas de gravedad originadas en el enfrentamiento con los delincuentes.

Según registró una cámara de seguridad, la víctima identificada como Félix Emanuel Costa (64) entraba su camioneta en un domicilio ubicado en calle Del Tejar al 3.500, entre Zinny y Vírgenes, de la mencionada localidad de La Matanza, cuando fue abordado por tres delincuentes.

Aparentemente, los ladrones al detectar que el hombre estaba armado, y que intentó defenderse, lo balearon. Cuando Costa cayó herido en el piso también le quitaron el revólver. Luego escaparon a bordo del vehículo robado.

Un vecino cargó al policía herido en su auto y lo trasladó de urgencia al hospital Simplemente Evita donde la víctima murió en medio de la intervención de urgencia a la que fue sometido.

En tanto los delincuentes se dieron a la fuga por calle Vírgenes en dirección a Villa Unión.

Tras un llamado al 911, un patrullero de la comisaría 2ª de González Catán se presentó en el lugar.

El fiscal Federico Medone, a cargo de la investigación, ordenó revisar cámaras de seguridad de la zona para intentar dar con los atacantes.