+++
author = ""
date = ""
description = ""
image = "/images/glvrgphln5ejtmh5ch7prvnhqm.jpg"
image_webp = "/images/glvrgphln5ejtmh5ch7prvnhqm.jpg"
title = "SAN FERNANDO: DESPUÉS DE 10 HORAS DE TRABAJO, LOS BOMBEROS LOGRARON CONTROLAR EL INCENDIO EN UNA PLANTA DE BIMBO"

+++

**_Un incendio de enormes magnitudes en una planta panificadora de Bimbo ubicada en el partido boanerense de San Fernando logró ser controlado este lunes, tras más de 10 horas de trabajo de una veintena de dotaciones de bomberos, sin que se registraran heridos, informaron fuentes municipales._**

"El fuego está controlado y no hay heridos", dijo el jefe de bomberos de San Fernando, Nicolás Verón, aunque aclaró que todavía tendrán que "trabajar muchas horas y varios días" en el lugar.

El incendio se produjo el domingo pasadas las 21 horas, en la intersección de las calles de las calles Uruguay y Emilio Zola en San Fernando.

A esa hora el portero de la panificadora dio aviso a los bomberos quienes, con el aporte de otros municipios, precisaron de más de 20 dotaciones para poder controlarlo.

Verón aseguró que se desconocen las causa del incendio y en declaraciones al canal Telefe agregó que el fuego "está controlado pero hay que seguir trabajando".

Por otra parte, Verón, manifestó entrevistado por C5N que "los peritos tendrán que averiguar lo que pasó, nosotros tenemos para trabajar muchas horas y varios días", dijo Verón y añadió que "no hay heridos ni bomberos ni personal del lugar".

En tanto, indicó que todavía no se pudo ingresar a la planta. "Estamos en eso, necesitamos maquinaria pesada para ingresar a lo que es el centro", dijo.

Respecto a lo sucedido durante la noche del domingo, relató que, pasadas las 21, recibieron el llamado de la portería de la empresa advirtiendo sobre el incendio.

En ese momento, siguió Verón, los empleados, que eran "12 muchachos que estaban de turno, empezaron a sentir olor a quemado y humo".

Según se informó, el siniestro comenzó cerca de 21 horas en el edificio ubicado en la intersección de las calles Uruguay y Emilio Zola de San Fernando, que pertenece a una planta panificadora de la multinacional Bimbo.

Además, de los Bomberos de San Fernando (que fueron los primeros en llegar) en el lugar trabajan dotaciones de otras localidades como Tigre, Pacheco, Garín y San Isidro.

"Cuando llegaron las primeras dotaciones por la gran cantidad de humo costó ingresar por la magnitud del depósito y la mercadería que había adentro", especificó Verón y añadió: "Al tener material refrigerado tienen grandes cantidades de amoníaco, se trabajó mucho en controlar que el fuego no llegue ahí".

Asimismo, detalló que "se derrumbó una parte del techo del depósito que es inmenso".

En cuanto, a las industrias linderas y ante la preocupación por la cercanía de una estación de servicio, Verón sostuvo que "es una zona industrial, no se va a evacuar" y señaló que "hay mucha distancia a la estación de servicio".