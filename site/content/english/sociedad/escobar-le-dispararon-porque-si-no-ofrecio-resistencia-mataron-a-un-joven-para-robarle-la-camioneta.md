+++
author = ""
date = 2021-04-09T03:00:00Z
description = ""
image = "/images/comisaria.jpg"
image_webp = "/images/comisaria.jpg"
title = "ESCOBAR: “LE DISPARARON PORQUE SÍ, NO OFRECIÓ RESISTENCIA\": MATARON A UN JOVEN PARA ROBARLE LA CAMIONETA"

+++

**_La víctima fue abordada por dos motochorros cuando llegaba a su domicilio. “Ya había entregado el vehículo”, indicaron fuentes del caso._**

Un joven de 26 años fue asesinado de un balazo en el pecho al ser asaltado por dos motochorros que le robaron la camioneta cuando llegaba a su casa en el partido bonaerense de Escobar.

El hecho ocurrió el miércoles por la noche frente a una casa ubicada en la calle Las Margaritas al 2600, entre Las Orquídeas y Los Tulipanes, en el barrio Luchetti, en dicho partido del noreste de la provincia de Buenos Aires. Allí vivía la víctima, identificada como José Luis Martínez Caba, de nacionalidad boliviana.

El joven regresaba solo a su domicilio a bordo de su camioneta Renault Oroch blanca cuando fue interceptado por dos delincuentes que se movilizaban en una moto y con los cascos colocados.

Según voceros de la investigación los ladrones amenazaron a Martínez con armas de fuego, tras lo cual le exigieron la entrega del vehículo. Una vez que descendió de la misma recibió un balazo en el pecho que lo dejó herido en la vía pública.

“Le dispararon porque sí, porque ya había entregado voluntariamente el vehículo sin ofrecer resistencia”, explicó a Télam una fuente judicial que sostuvo que la principal hipótesis es que los delincuentes buscaban un vehículo para robar ya que la víctima tenía en su poder otros objetos de gran valor, como dinero en efectivo. Escaparon con la camioneta y la moto que, según un testigo presencial, era una Tornado negra que no llevaba chapa patente.

Luego del asalto, la víctima fue trasladada al Hospital Erill, de Escobar, donde murió alrededor de las 23.40. El personal policial, alertado de lo ocurrido por un llamado al 911, inició las diligencias de rigor con la finalidad de localizar a los motochorros y los rodados en los que huyeron.

A partir de la investigación creen que los delincuentes serían de la zona y que habrían cometido otros robos similares en Garín y San Fernando, donde viven, agregaron los informantes. “Hay varias líneas investigativas y las vamos a seguir hasta que sean atrapados. Tenemos cámaras de seguridad con las que se está trabajando para tener imágenes claras”, señaló la fuente consultada.

Una de las pistas que surgió del análisis de esas imágenes indica que los delincuentes huyeron hacia la Panamericana, en dirección a la Ciudad Autónoma de Buenos Aires.

El hecho, caratulado inicialmente como “homicidio en ocasión de robo”, es investigado por la fiscal Paula Gaggioti, de la Unidad Funcional de Instrucción descentralizada de Escobar en turno, quien aguarda para las próximas horas los resultados preliminares de una serie de peritajes, entre ellos, de la autopsia al cuerpo de la víctima.

Estimaron que la necropsia se llevará a cabo recién hoy en la Morgue Judicial de Zárate-Campana, aunque podría demorarse por falta de médicos forenses disponibles. A su vez, los peritos no hallaron ningún rastro balísticos en la escena del crimen, por lo que creen que los delincuentes se llevaron la vaina servida producto del disparo o utilizaron un revólver.

Semanas atrás, y por un hecho con algunas similitudes, la provincia de Buenos Aires volvió a teñirse de tragedia: un joven de 20 años fue asesinado de un disparo en el pecho durante un intento de asalto de su auto de alta gama en Lomas del Mirador.