+++
author = ""
date = ""
description = ""
image = "/images/21_421141fotogrande-1.png"
image_webp = "/images/21_421141fotogrande.png"
title = "DESALOJAN DE LA GENERAL PAZ A UN GRUPO DE VECINOS QUE EXIGÍAN MAYOR SEGURIDAD EN EL BARRIO MUGICA"

+++
**_La Prefectura Naval, con apoyo de a Policía de la Ciudad, desalojó la protesta tras varias horas de conflicto. Hubo quema de neumáticos y disparos._**

Un verdadero momento de tensión se vivió este lunes por la noche, cuando un grupo de al menos 50 personas que serían vecinos del barrio Padre Mugica cortaron ambos sentidos de la General Paz, a la altura de Villa Lugano, para reclamar más seguridad y fueron desalojados por efectivos de la Prefectura Naval Argentina.

Los manifestantes se concentraron en la intersección de esta avenida con la calle Castañares y comenzaron a prender fuego varios neumáticos para impedir la circulación del tránsito, afectando también a la colectora, según informaron diferentes fuentes.

Ante esta situación, efectivos de la mencionada fuerza, que cuenta con una pequeña base de operaciones en Mataderos, a pocos kilómetros del lugar donde se estaban produciendo los disturbios, se trasladaron hasta allí para desalojar la protesta.

Los uniformados dispararon balas de goma para tratar de desalentar el accionar de quienes se encontraban interrumpiendo el paso de los vehículos.

La situación se fue haciendo cada vez más tensa con el correr de las horas y en un momento los manifestantes produjeron algunos destrozos sobre la colectora, a la altura del barrio INTA, situado debajo de la conexión de la avenida General Paz con la Autopista Dellepiane, en el límite entre la provincia de Buenos Aires y la Capital Federal.

Al lugar llegaron también algunos patrulleros de la Policía de la Ciudad que prestaron apoyo y entre ambas fuerzas comenzaron a dialogar con quienes estaban encabezando la protesta y luego de varias horas de conflicto, el piquete finalizó.

Los efectivos porteños comenzaron entonces un operativo para reanudar el tránsito y dispusieron de algunos patrulleros que permanecían sobre la colectora para controlar que no surjan nuevos cortes.

Los manifestantes eran vecinos del Barrio Padre Mugica (ex Villa 31) que reclaman una mayor presencia de las fuerzas de seguridad en ese asentamiento, luego de un importante enfrentamiento que hubo entre dos bandas de delincuentes que se estarían disputando el control de la zona.

Hace poco más de un mes, la Policía de la Ciudad detuvo en la Villa 31 bis a Margarita Morales Huincho, 43 años, con una orden de un juzgado correccional porteño por una vieja causa por homicidio con fecha de 2017.

Según detectives judiciales, esta mujer fue amante y lugarteniente de César Morán de la Cruz, alias “El Loco” y “Mata por Gusto”, uno de los criminales más peligrosos de este lugar, con fuerte presencia en el Playón Este, “La Casa del Pueblo”, donde fue capturado su principal cómplice, Juan Honorio Inga Arredondo, alias “Piedrita, condenado a 20 años de cárcel por dos homicidios.

En los registros de Comodoro Py, Margarita aparece como vinculada a un negocio paralelo de la banda: los préstamos usureros. Los investigadores de la Justicia federal, acostumbrados a seguir a la banda de Morán de la Cruz, con una histórica causa principal en la Fiscalía Federal N°1 que fue del fallecido Jorge Di Lello, la ubican entre las encargadas de llamar a los deudores para que paguen.

Sin embargo, otra fuentes aseguraron en aquel momento que la víctima por la cual pidieron el arresto de Margarita murió baleada por un ajuste de cuentas netamente vinculado a una deuda de drogas. En su casa se encontraron cuatro kilos de cocaína, medio de pasta base y marihuana, además de un chaleco antibalas, una pistola calibre .22 con silenciador, municiones y un juego de esposas.

Si bien no se sabe si este hecho está relacionado con el conflicto entre delincuentes que desató la protesta de este lunes, lo cierto es que el Barrio Mugica todavía sigue siendo un territorio en el que narcotraficantes se disputan el poder.