+++
author = ""
date = 2022-01-04T03:00:00Z
description = ""
image = "/images/xker4wjicjfatkju3avpgdp4ge.jpg"
image_webp = "/images/xker4wjicjfatkju3avpgdp4ge.jpg"
title = "POLÉMICA EN CHUBUT: ENCONTRARON A UNA JUEZA A LOS BESOS CON UN PRESO CONDENADO A PERPETUA"

+++

#### **_El Superior Tribunal de Justicia de esa provincia ordenó revisar la actuación de la magistrada de Comodoro Rivadavia._**

El Superior Tribunal de Justicia de Chubut ordenó iniciar actuaciones sumarias administrativas contra una jueza penal de Comodoro Rivadavia a partir de la viralización de un video en el que se la registró, en una sala de visitas de la cárcel de Trelew, tomando mate y besándose con un preso al que había condenado a prisión perpetua.

A través de un comunicado oficial, el Superior Tribunal informó que la jueza Mariel Suárez habría incurrido en “conductas inadecuadas” con el preso Cristian “Mai” Bustos. El hecho ocurrió el 29 de diciembre pasado en el Instituto Penitenciario Provincial (IPP) de Trelew.

La jueza integró el Tribunal que hace dos semanas lo condenó en Esquel por asesinar al policía Leandro “Tito” Roberts en la localidad de Corcovado, crimen ocurrido en 2009. Suárez votó en disidencia y no apoyó la condena a perpetua.

Los hechos investigados ocurrieron en un aula del Instituto Penitenciario Provincial (IPP) de Trelew. Suárez integró el Tribunal que el 22 de diciembre pasado condenó a Bustos en un juicio realizado en Esquel. De hecho, Suárez votó en disidencia la condena a perpetua, solicitando una pena menor.

“A raíz de una comunicación formal dirigida a los ministros de la feria se tomó conocimiento de un encuentro requerido por una jueza penal de la circunscripción Comodoro Rivadavia y un recluso alojado en un centro penitenciario, considerado de alta peligrosidad y recientemente condenado en el marco de un juicio oral y público”, informó a través de un comunicado el Superior Tribunal de Justicia.

El encuentro entre la jueza y el preso fue denunciado por un oficial del Instituto Penitenciario a sus superiores en un escrito que fue difundido a la prensa local, en el que se indica que a las 16.40 del 29 de diciembre la jueza Suárez concurrió a la dependencia para entrevistarse con el condenado Bustos. El oficial describió que el sector de encuentro posee cámaras que permiten ver qué sucede, aunque no se almacenan imágenes, y que en determinado momento observó que la jueza y el recluso “comienzan a tener contacto físico, como abrazos, besos (en la boca) siendo reiterado este tipo de comportamiento en ambos”, según el escrito.

#### El caso

El inusual caso ocurrió el miércoles pasado 29 de diciembre dentro del Instituto Penitenciario Provincial (IPP) ubicado entre Trelew y Puerto Madryn, “en horas de la tarde”, agrega el comunicado oficial del Superior Tribunal.“A raíz de una comunicación formal dirigida a los ministros en feria se tomó conocimiento de un encuentro requerido por una jueza penal de la circunscripción de Comodoro Rivadavia y un recluso alojado en dicho centro penitenciario, considerado de alta peligrosidad y recientemente condenado en el marco de un juicio oral y público sustanciado en la ciudad de Esquel”, detalla el parte oficial del Poder Judicial.“

De los datos comunicados desde el IPP surge que la jueza habría incurrido en conductas inadecuadas para un magistrado. Las actuaciones se dirigen a dilucidar las circunstancias de dicha reunión entre una magistrada y un condenado, el tenor del encuentro, su extensión en el tiempo y las características del mismo, que puedan implicar violaciones de la ley de Ética Pública y/o al Reglamento Interno General del Poder Judicial”, concluye el comunicado del Superior.

El 23 de diciembre pasado se conoció el veredicto por el juicio a Cristian “Mai” Bustos, acusado de haber matado al policía Leandro “Tito” Roberts el 8 de marzo del año 2009 en Corcovado, y haber causado graves heridas al oficial Luis Cañumir.