+++
author = ""
date = 2021-04-20T03:00:00Z
description = ""
image = "/images/analia-maldonado-y-samuel-llanos-1.jpg"
image_webp = "/images/analia-maldonado-y-samuel-llanos.jpg"
title = "HALLARON UN CUERPO CALCINADO E INVESTIGAN SI ES EL DE UNA MUJER QUE DESAPARECIÓ JUNTO A SU PAREJA"

+++

**_Analía Maldonado fue vista por última vez el domingo junto a su pareja, un hombre con antecedentes de violencia y abuso que permanece desaparecido._**

Un cuerpo calcinado fue hallado esa mañana adentro de una bolsa en un camino rural de la localidad bonaerense de San Emilio, de la ciudad de Los Toldos, e investigan si se trata de una mujer que despareció junto a su pareja el domingo pasado.

El cuerpo fue hallado calcinado esta mañana durante los rastrillajes que se realizaban en busca de la pareja en un camino rural ubicado a 5 kilómetros de la localidad de Los Toldos y se investiga si se trata de Analía Maldonado (40), de quien no se tienen noticias desde el pasado domingo tras una discusión que mantuvo con su pareja, Samuel Llanos, un fisicoculturista con antecedentes que es intensamente buscado.

Maldonado y Llano estaban en pareja desde hace aproximadamente un año y lo último que se supo de ellos antes de que se les perdiera el rastro fue que mantuvieron una fuerte discusión.

Si bien no se encontraron acusaciones previas por violencia de género de Maldonado contra el fisicoculturista, Llano cuenta con al menos cinco causas por denuncias realizadas por parejas anteriores entre los años 2009 y 2020, entre ellas una por lesiones graves contra el hijo de una de estas mujeres.

Los antecedentes del hombre, sumado a la declaración de los testigos que refirieron haber escuchado una violenta pelea entre ellos “el sábado por la noche o el domingo por la mañana” en la casa que compartían, escalaron la preocupación alrededor del cuerpo calcinado que encontró la policía adentro de una bolsa en un camino rural de la zona.

Anoche, un grupo de vecinos de esa ciudad bonaerense se movilizó hacia la Estación de la Policía Comunal reclamando por lo aparición de ambos y que se investigue un posible femicidio. De acuerdo a la información difundida por el Diario Democracia, se cree que Llanos podría circular en un auto Nissan de color blanco, con patente ITA 913.