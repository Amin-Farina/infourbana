+++
author = ""
date = 2022-03-11T03:00:00Z
description = ""
image = "/images/foto-violador.webp"
image_webp = "/images/foto-violador.webp"
title = "\"ME AGARRE LA CABEZA, NO SABÍA LO QUE ESTABA PASANDO\": DECLARA LYKAN OTRO DE LOS IMPUTADOS POR LA VIOLACIÓN DE PALERMO"

+++
#### Fue indagado Franco Lykan, el segundo de los seis acusados. También se declaró inocente y cuenta su versión del acontecimiento. 

Franco Lykan fue el cuarto en ser convocado en el nuevo turno de declaraciones indagatorias realizadas ayer a los seis detenidos por la violación grupal de Palermo. Al igual que su amigo Ignacio Retondo, decidió hablar. En su declaración, contó cómo comenzó la noche que terminó con el abuso sexual, de que manera conocieron a la víctima y que vinculación lo une con el resto de los acusados.

En su declaración, vía Zoom, frente al fiscal Rosende y al juez Fernández, relató todo lo que sucedió esa madrugada. Contó que se encontró con sus amigos Retondo y Ciongo Pasotti la noche anterior en Villa Adelina y decidieron ir a Palermo.

“Fuimos a la puerta del boliche Ro Tecno Bar, tomando vino y tocando la guitarra. Mientras, se acercaban otras personas para unirse a los cantos. Preguntaron en “Ro” el precio de la cerveza, dijeron no, porque no tenían tanta plata y deciden ir a Plaza Serrano. Ahí se encuentran con otro grupo de gente, que también tenía guitarra. Eran bastantes personas en lo mismo, en la música. Aclara que siempre se manejó con Retondo y con Pasotti”, comenzó la transcripción.

Luego siguió con su relato, contando que mezcló alcohol con pastillas: “Ahí se toma la decisión de tomar otro vino. Agregado a ese vino, el dicente dice ‘qué ganas de tomarme un clona’ porque tiene, en general, ataques de ansiedad, lo consume, de 1 miligramo con el vino. Le hace un efecto somnífero, agregado a lo que ya había consumido, cerveza y vino. Recuerda que era de día y ya se empezaba a sentir mal, estaba quedándose casi dormido, inconsciente”.

Lykan aseguró estar dormido al momento del abuso: “Ahí va con Ciongo y le dice si le abre el auto que necesitaba dormir. Ahí intenta abrir, se logra abrir y entra. Se queda ahí desmayado, serian entre la 13:00 y 13:30 horas, se sentía mal, tenía ganas de dormir. Después de descansar, se despierta de golpe y ve que afuera era un tumulto de gente. Cuando abre la puerta lo ve a Retondo con el ojo todo hinchado, le pregunta y éste le dice que le habían pegado una patada en la cara. Ve a la persona que lo quería atacar y lo defiende, estaba conmocionado. Decide cruzar la calle en una esquina, se agarra la cabeza, no sabía lo que estaba pasando, ahí viene un oficial y lo esposa al lado del auto”.

Cuando el fiscal Eduardo Rosende le preguntó sobre la víctima, Lykan aseguró que “ni siquiera la registró”.

“El dicente asegura que en ningún momento tuvo contacto con la chica. Si le preguntan cómo estaba vestida, no lo recuerda porque no la registró, era una persona más del grupo, no le dio importancia. A la única persona que le dio importancia era a una persona de origen canadiense, estuvieron hablando en inglés casi toda la mañana y mediodía, fue el único contacto”, sostiene la declaración.

En sus cuestionamientos, el fiscal Rosende quiso hacer hincapié en lo que sucedió en el auto. Le preguntó a Lykan si al momento de ingresar al auto para dormir estaba con más gente o sólo. El detenido respondió que “entró solo, al asiento de atrás, se acostó, cerró los ojos y se despertó con todo ese problemón, esa situación. No tiene registro del ingreso de alguna otra persona, estaba inconsciente”.

Oriundo de Carapachay, las pruebas lo complican. En las pericias químicas le encontraron rastros de semen en la remera, bóxer, pantalón y medias que vestía al momento del hecho. Al igual que Ángel Pascual Ramos (23), Steven Alexis Cuzzoni (20), Thomas Domínguez (21), Lautaro Ciongo Pasotti (24) e Ignacio Retondo (22), continuará detenido en una comisaría porteña. En las próximos días el juez Marcos Fernández deberá decidir si les dicta la prisión preventiva. También se le halló semen a la víctima, tanto en la ropa como en su cuerpo: lo que diga la pericia de ADN será clave.