+++
author = ""
date = 2021-10-28T03:00:00Z
description = ""
image = "/images/syydkjohhfdgha4w5z3wdvmvpm.jpg"
image_webp = "/images/syydkjohhfdgha4w5z3wdvmvpm.jpg"
title = "ERA TRAPITO EN PALERMO Y ESTABA PRÓFUGO POR ROBO"

+++

##### **_Los trapitos que pululan por la zona de Palermo entre discotecas, locales de ropa y restaurantes o en eventos masivos en estadios para cobrar cientos de pesos por “cuidar” de manera extorsiva a vehículos en un espacio público pueden esconder otras historias._**

Esta semana, el Departamento Contravenciones y Faltas y la División Operaciones Especiales Requeridas por el Ministerio Público de la Policía de la Ciudad capturaron a uno que operaba en la zona de Malabia y Costa Rica, en las inmediaciones de Plaza Armenia, por una vieja cuenta pendiente.

Carlos Javier Lauría, de 27 años, con domicilio también en la zona de Palermo, había trabajado para una empresa gastronómica a mediados de la década pasada, luego en un supermercado chino. Pero un año antes, había registrado una causa por tentativa de robo en un juzgado porteño. Su pedido de captura seguía vigente, emitido por el Tribunal Oral Criminal N°1. Al encontrarlo, notificaron al tribunal que ratificó el pedido y fue trasladado.

Hay otros episodios violentos relacionados a trapitos en la historia reciente. En noviembre de 2020 en la ciudad de Córdoba, otro cuidacoches fue acusado de haber disparado un tiro a cuatro integrantes de un auto con los que había discutido previamente.

El hecho ocurrió en el cruce de las calles Derqui y Buenos Aires, en Nueva Córdoba. El conductor de nombre Ramiro, de un vehículo cuyo modelo y marca no fueron revelados, estacionó el auto en ese cruce en el inicio de la noche del sábado. Estaba acompañado por tres amigos. Después de bajar, le dio dinero a un cuidacoches que se encontraba en la cuadra.

Sin embargo, al regresar al auto, ya en la madrugada, los cuatro amigos se encontraron con otro cuidacoches, un “naranjita”, como le dicen en la jerga de la capital cordobesa por las pecheras reflectoras que llevan. El joven les exigió el nuevo pago del canon informal, a lo que Ramiro y sus colegas se negaron.

Los amigos del conductor relataron que después de intercambiar insultos y amenazas leves, el cuidacoches sacó un arma que llevaba al cinto y efectuó un disparo contra el conductor.

La bala ingresó en el brazo izquierdo de Ramiro, luego el proyectil siguió su trayecto hasta la pierna derecha y finalizó impactando contra la palanca de cambios del auto. Inmediatamente después, el cuidacoches se dio a la fuga y desapareció de la escena.

Policías memoriosos recuerdan el caso de Pablo José Bistmans, que operaba en la zona del Hipódromo. Acumuló 29 infracciones en los primeros nueve meses de 2016. Fueron, por ejemplo, cinco en el mes de febrero, otras siete en el mes de marzo, el mes pico en su resumen. Ninguna infracción fue registrada en enero, lo que serían unas aparentes vacaciones..

Con domicilio en Flores, Bistmans está registrado ante la AFIP en los rubros de transporte automotor con remises y taxis, pero todos sus encuentros con los efectivos de la N°31 fueron por violar el artículo número 79 del Código Contravencional porteño, “cuidar coches sin autorización legal”, una infracción en los papeles castigada con trabajo comunitario y multas.

También hicieron su vuelta al Monumental en el último superclásico del mes pasado. 30 infracciones fueron levantadas a cuidacoches según datos del Ministerio Público Fiscal porteño.