+++
author = ""
date = 2021-05-06T03:00:00Z
description = ""
image = "/images/patrullero-b.jpg"
image_webp = "/images/patrullero-b.jpg"
title = "MORENO: 4 DELINCUENTES ENTRARON A ROBAR UNA ROTISERÍA Y 1 DE ELLOS FUE ASESINADO A GOLPES POR LOS DUEÑOS DEL LOCAL"

+++

**_Cuatro delincuentes ingresaron a robar una rotisería en la localidad bonaerense de Moreno pero se toparon con los dueños del local que los enfrentaron a golpes. Uno de los asaltantes murió en el lugar. Aún se investiga si la muerte se produjo por la golpiza, aunque no se descarta una falla cardíaca._**

El hecho se desencadenó en la noche del pasado martes. Alrededor de las 22.30, en un comercio que funciona dentro de una vivienda ubicada en la calle Don Segundo Sombra, casi en la intersección con San Juan Bautista, de dicha localidad de la zona oeste del conurbano, cuatro hombres armados ingresaron al lugar con la intención de robar.

Dentro de la propiedad, los asaltantes quisieron entrar a una habitación en la que se encontraba la hija del dueño de la rotisería. Fue en ese momento que otros cuatro hombres que estaban en el local se enfrentaron a golpes con los delincuentes.

Según informaron fuentes judiciales a la Agencia Télam, uno de los ladrones llegó a disparar al menos una vez pero no hirió a nadie. En ese contexto, decidieron huir por el fondo de la propiedad donde se vieron obligados a saltar una reja. Sin embargo, uno de ellos no logró escapar: fue reducido por los dueños de la rotisería quienes comenzaron a pegarle para quitarle el arma de fuego que empuñaba y se encontraba cargada.

Los golpes continuaron por varios segundos hasta que en un momento el asaltante dejó de resistirse. No se movía. Ya había muerto en el lugar.

Por su parte, los otros tres cómplices escaparon corriendo y disparando tiros hacia la propiedad. Tras denunciar lo sucedido al 911, llegaron efectivos de la comisaría quinta de Moreno, quienes realizaron las primeras actuaciones en el sitio y constataron la muerte del delincuente.

La víctima fatal fue identificada como Alan Ledesma, era un joven de 24 años. Posteriormente, su cuerpo fue trasladado a la morgue judicial para realizarle la autopsia. Hasta el momento, fuentes de la investigación indicaron que no se pudo determinar la causal de la muerte: los forenses establecieron que Ledesma sufrió una acumulación de golpes, pero ninguno de ellos habría sido lo suficientemente contundente como para quitarle la vida.

Por ese motivo, se aguarda el resultado de más exámenes complementarios que constaten si murió a raíz de la golpiza o por alguna afección en su salud. De esta manera, no descartan que se trate de una falla cardíaca.

Interviene en la causa el fiscal Federico Soñora, a cargo de la Unidad Funcional de Instrucción (UFI) 4 del Departamento Judicial de Moreno-General Rodríguez, quien al momento no adoptó temperamento legal alguno para los propietarios de la rotisería, mientras espera el resultado de distintas pericias con el objetivo de avanzar en la investigación.