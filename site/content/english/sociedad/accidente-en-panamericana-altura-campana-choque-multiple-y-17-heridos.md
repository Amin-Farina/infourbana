+++
author = ""
date = 2021-11-01T03:00:00Z
description = ""
image = "/images/v7vr6z754fchjebmsluc5dii64.jpg"
image_webp = "/images/v7vr6z754fchjebmsluc5dii64.jpg"
title = "ACCIDENTE EN PANAMERICANA, ALTURA CAMPANA: CHOQUE MULTIPLE Y 17 HERIDOS"

+++
##### 

##### **_Un camión hizo una maniobra forzada para evitar a una moto y recibió el impacto de un micro camión. Cuatro vehículos participaron del siniestro. Ocurrió en el ramal Campana, kilómetro 72._**

Según las primeras informaciones, un camión intentó esquivar a una moto que estaba detenida en la Panamericana y por la maniobra recibió el impacto de un micro. En la colisión también se vio involucrado un auto. Todos los heridos por el siniestro fueron trasladados al Hospital Municipal de Campana.

Un motociclista se cayó al perder el control por un pozo, el camión que venía detrás quiso esquivarla y recibió el impacto de un micro que trasladaba desde Benavidez empleados de una reconocida automotriz.