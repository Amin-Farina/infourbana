+++
author = ""
date = 2021-04-08T03:00:00Z
description = ""
image = "/images/ht3rtpt4lrgfnhpcjizcuykwr4.jpg"
image_webp = "/images/ht3rtpt4lrgfnhpcjizcuykwr4.jpg"
title = "SE FUE DE FRANCIA Y PUSO UN HOSTEL EN JUJUY "

+++

**_Rémy Rassé es un artista plástico que se encandiló con los colores del norte argentino, donde conoció a su mujer y echó raíces. Construyeron una casa de adobe, un taller y un hostel. Dijo que ama las costumbres y la libertad que hay en el país: “Prefiero la vida argentina”._**

“Si no te gusta la aventura, la inestabilidad y los imprevistos, no vengas a vivir a la Argentina”, dice Rémy Rasse de 58 años, un artista plástico francés que eligió cambiar su vida en Niza, Francia, por Jujuy.

Vive en el corazón de la Quebrada de Humahuaca, cerca del pueblo de Huacalera, desde hace más de un década junto a su mujer Analía, también de 58 años. Confesó que encontró ahí en ese pueblito andino ubicado a 2641 metros de altura su lugar en el mundo: allí instaló su finca, su taller de artista y su hostel.

Rémy nació en el sur de Francia: es Licenciado en Bellas Artes y en Ordenamiento Territorial y Ecología. Su esposa Analía es nacida en Jujuy y directora audiovisual. A pesar de estar a más de once mil kilómetros de distancia, sus caminos se cruzaron, dando vida a una relación de amor, arte y profesión.

Enamorado del norte

Mientras cursaba su último año de facultad de Bellas Artes de París, Rémy decidió hacer un viaje para “aprender el conocimiento adquirido”. “Eso hacen los japoneses: se aíslan para incorporar lo nuevo. Presenté mi último proyecto y volé”, le cuenta a Infobae en su relato por saber cómo comenzó su excursión por Argentina, el inicio de todo.

Planeó hacer una expedición por la Cordillera a caballo junto a su hermano. “Primero visitamos la Patagonia chilena y también el noroeste argentino. Recorrimos 3.000 kilómetros en seis meses. En ese viaje descubrí el arte pre hispánico, primitivo, los colores, los murales y la belleza de los paisajes naturales. Pensé que era un lugar al tenía que volver”, destacó.

Se recibió y cumplió con su anhelo. Regresó en 1993 a Jujuy, esta vez para para llevar adelante un proyecto humanitario que le permitiría unir sus vocaciones: la agricultura y el arte. “Consistía en brindar talleres de pintura para niños y renovación de semillas para los agricultores de un pueblo ubicado en el punto tripartito donde se unen Argentina, Bolivia y Chile: El Angosto”, narró. Su trabajo aún se puede ver en las escuelas.

El destino la llevó a cruzarse con Analía, quien como estudiante de cine hacía su propio documental sobre los buscadores de oro. Empezaron a salir, se casaron y hace casi veinte años que están juntos. Se instalaron un tiempo en Francia, más precisamente sobre la Costa Azul, donde la familia de Remy posee un campo con vista a la montaña. “Me gusta mucho la vida alejada de la ciudad, su tranquilidad, su ritmo”, explicó.

#### Emigrar muy lejos de casa

En una decisión de a dos, apostaron por el norte argentino, compraron un terreno e idearon la casa de sus vidas. “Nunca perdimos contacto con la Argentina, ya que era el destino obligado para pasar las vacaciones y los encuentros con la gente querida, y sobre todo el arte”, contó.

En 2004 pusieron manos a la obra e hicieron su casa desde cero en una amplia finca de tres hectáreas con vista a los cerros Huacaleros. “Está hecha a mano, de adobe, y por dentro toda la decoración está realizada artesanalmente, desde las fuente del jardín hasta los cuadritos del baño”. La nombraron Solar de Trópico, en honor a que en el pequeño pueblo hay un monolito dedicado al Trópico de Capricornio, porque por esos lares atraviesa el trópico del hemisferio sur.

La palabra solar se utilizaba en la época colonial para describir el terreno de una propiedad de menos de diez hectáreas con un patio florido. La palabra trópico obedece a la línea geográfica del trópico de Capricornio pero también en homenaje a los artistas que más inspiran a Rémy: Vincent Van Gogh y Paul Gauguin. “Un lugar así solo se puede hacer en el norte argentino. Tardamos casi tres años en terminarla porque acá no se corre, hay tiempo para crear, por suerte se vive a otro ritmo. Para un artista eso es impagable…”

En su casa, hicieron el taller de artista y años más tarde un hostel a pedido de los visitantes. “Hicimos tres habitaciones más y empezamos a recibir turistas, sobre todo europeos”, remarcó. Remy y Analía son anfitriones de lujo y su fusión cosmopolita le agrega un valor a la experiencia, ya que intercambian costumbres, comparten el arte y también cocinan. “Hacemos desde humitas hasta el verdadero ratatouille francés. Acá la gente viene a desconectarse: no tenemos tele, sí wifi. La idea es que miren por la ventana y que salgan”.

Hasta que llegó la pandemia, donde estuvieron un año cerrados. Aprovecharon ese tiempo para hacer arreglos, pintar y seguir creando: “Tenemos respaldo pero fueron meses duros”. En Semana Santa pudieron hospedar turistas locales: “Es lindo recibir gente y mostrarles nuestra pasión por este país”.

\-¿Estas enamorado de la Argentina?

\-Claro, amo su libertad. Caminar por un sendero y no cruzarte con nadie es tan grande y tan diverso. También me gusta la gente, los caballos, las costumbres de reunirse a compartir un asado, tomar vino y hablar…

\-¿Cuáles son las cosas que no te gustan del país?

\-Pocas cosas… No sé, la inflación tal vez. Es el peor mal.

\-¿Creés que hay una idealización de la vida europea?

\-No es para todos. En Francia tenés la vida planificada: ya sabés qué va a pasar de acá a diez años. No digo que sea malo, pero a mí no me gusta la monotonía, o porque te hace desarrollar no la creatividad. La rigidez atenta contra eso... Prefiero esta vida argentina.