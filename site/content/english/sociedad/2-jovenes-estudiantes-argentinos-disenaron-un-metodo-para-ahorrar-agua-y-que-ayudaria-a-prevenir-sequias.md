+++
author = ""
date = 2022-02-23T03:00:00Z
description = ""
image = "/images/jtscv6wrund7hiofn45z5do6ra.jpeg"
image_webp = "/images/jtscv6wrund7hiofn45z5do6ra.jpeg"
title = "2 JÓVENES ESTUDIANTES ARGENTINOS DISEÑARON UN MÉTODO PARA AHORRAR AGUA Y QUE AYUDARÍA A PREVENIR SEQUÍAS"

+++

##### **_Axel Córdoba (24) y Denis Álvarez (23), a punto de terminar la carrera de Geología en la Universidad de Comahue, crearon un “polvo granular” que convierte el agua en un sólido e incorpora nutrientes para el crecimiento de las plantas. Aseguran que se puede regar con la mitad de agua_**

La preocupación por el incremento de zonas afectadas por la sequía no dejaba dormir a Axel Córdoba ni a Denis Álvarez, amigos y compañeros de cursada en la carrera de Geología. Desde el año 2019, el tema comenzó a ocupar a ambos.

Los dos sintieron que unir sus deseos para encontrar una solución pronta y posible era casi una urgencia. Gracias a los conocimientos de las materias Suelos, Hidrología y Geología Ambiental, a mediados de 2021, idearon un proyecto que aseguran es único en el mundo: un polvo granular capaz de convertir el agua líquida en un sólido e incorporar nutrientes esenciales para el crecimiento de las plantas. El producto, Hydroplus, se hidrata con el agua de riego y la proporciona lentamente conforme la planta la necesite.

> “Desde el año 2019, las sequías se han agudizado y extendido en Argentina. Para el 2050, al menos una de cada cuatro personas correrá el riesgo de vivir en un país afectado por la escasez crónica o reiterada de agua potable. Actualmente, más del 70% del agua potable es utilizada para la agricultura y de manera doméstica como regar nuestras plantas. Con los sistemas de riego actuales, mucha de esta agua se pierde por infiltración y/o evaporación, lo que constituye un enorme problema y un claro desperdicio de este escaso recurso. Lo que creamos hace que se use el 100 % del agua”, aseguran los jóvenes emprendedores.

##### Un proyecto que puede ser revolucionario

Axel nació en Tucumán. Dice que siempre le gustó leer libros de ciencias, lo maravillaron desde niño. Siendo adolescente comenzó a sentir interés por la biología, la física y la química. Por eso, cuando supo que la Geología incorporaba todas las ciencias y que además sabría cómo funciona el Planeta Tierra, su dinámica y cómo utilizar de manera eficiente y sustentable los recursos naturales, la eligió como carrera.

En 2016, se mudó con su familia a la localidad de General Fernández Oro, en Río Negro, y comenzó a estudiar Geología en la Universidad Nacional del Comahue, en Neuquén. Allí conoció a Denis Álvarez con quien inició una amistad y más tarde el proyecto que promete revolucionar el tratamiento de los suelos y el uso responsable del agua potable.

“Nos preocupaba la escasez de agua y comenzamos a buscar la manera de encontrar una solución utilizando los conocimientos que ya teníamos sobre el suelo y geología. Empezamos a tirar ideas y a ver cómo podíamos ahorrar agua y, a la vez, generar conciencia ambiental. Así surgió la idea de un producto que tenga que ver con el agua, el riego y el suelo porque aunque ya existen diferentes propuestas en el ámbito de la jardinería y agricultura para mejorar el desarrollo de las plantas y cultivos, éstos tienen químicos contaminantes o hay métodos para disminuir el riego como el de goteo u otros absorbentes que no aportan nutrientes ni son biodegradables”, explica con detalle Axel sobre el desarrollo que llevan adelante.

A mediados de 2021, luego de un tiempo exclusivo de investigación comenzaron a desarrollar un prototipo que fue probado en sus propios jardines y, más adelante, lo ofrecieron a los viveros locales con el aviso que es de larga vida. En noviembre pasado el prototipo —que tiene como materia prima poliacrilato de potasio y complejo húmicos— quedó listo para ser utilizado.

Por ahora, Axel y Denis preparan bolsas de 250 gramos para lanzar al mercado el próximo 1° de marzo por $450. Apuntan iniciar en los hogares y en viveros para luego abrirse paso en frente a las compañías agrícolas, vitivinícolas y las que acepten probar su producto. Más adelante, serán bolsas de 25 kilos más concentrado para cultivos más grandes “porque allí necesitan más riego”.

El polvo granular que crearon promete seguridad medioambiental por ser biodegradable y no tóxico; al entrar en contacto con el agua, se expande y “puede llegar a absorber más de 15 veces su peso en agua”, explican los futuros geólogos.

“Solo 18 gramos de hydroplus absorbe 300ml de agua, por lo que se recomienda esa dosificación por cada litro de sustrato. Su uso es simple: una cucharadita del producto se hidrata en medio litro de agua y se lo coloca en la raíz de las plantas o se realiza un surco alrededor de ella, luego se lo tapa con un poco más de tierra. Esto la mantiene hidratada sin necesidad de riego o lluvia constante. La retención de agua en el suelo, permite que la planta tome agua cada vez que lo necesita y cuando se deshidrata totalmente vuelve a su estado original. Puede rehidratarse de esa manera durante unos 5 años, el tiempo estimado de durabilidad del producto”, explicó Axel.

El granulado, cuenta, también incorpora elementos nutritivos a la planta porque utilizan “productos 100 % naturales que van a colaborar en las primeras etapas de crecimiento de la planta, incrementando su rendimiento en el caso de los cultivos”.

“Esto permite obtener un riego más eficiente. Lo que buscamos es crear una cultura de apreciación y optimización del uso del agua, ya que uno de los grandes retos de las ciudades y los campos es reducir el riesgo de pérdidas, aumentar la productividad y brindarle a la planta una nutrición eficiente y sustentable. Así, contribuimos al ahorro de agua y cuidado del medio ambiente desde nuestros jardines y huertas hasta los grandes campos de cultivo”, explica Axel y dice que hasta el momento lo probaron en cultivos de tomates y paltas con resultados satisfactorios.

La aspiración máxima de los jóvenes es hacer convenios con municipios para que lo apliquen en programas de forestación o lugares donde debido a la sequia o estructura del suelo los árboles no pueden crecer.

“Desde la Corporación Vitivinícola Argentina nos dijeron que querían hacer pruebas con el producto en La Rioja porque allí hay cultivos comprometidos y ya anticipan escases de alimentos para 2023″, adelanta con orgullo sobre el proyecto que ya recibió cuatro premios en el país, lo que los ayudó en su etapa de desarrollo tanto con financiamiento como con asesoramiento.

A modo de crear conciencia sobre el uso del agua, sobre todo en estos momentos, concluye: “Cada gota cuenta y podemos hacer una diferencia de verdad desde nuestros hogares hasta los grandes campos de cultivos, por eso es tan importante optimizar el agua”.