+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/campeona.jpg"
image_webp = "/images/campeona.jpg"
title = "CAMPEONA ARGENTINA DE 800 METROS CORRIÓ A UN LADRÓN Y LO ATRAPÓ"

+++

**_La atleta argentina Evangelina Thomas vivió un feo momento después de que descubriera que un delincuente había robado algunas de sus pertenencias tras llegar a su casa en la ciudad de Trelew._**

Afortunadamente, la campeona argentina de 800 metros, descubrió que el ladrón todavía estaba adentro de la vivienda por lo que intentó reducirlo y al escaparse lo persiguió durante dos cuadras hasta que lo atrapó.

La joven atleta contó con detalles qué fue lo que ocurrió: “Después de entrenar, tipo seis y cuarto de la tarde, me fui a mi casa y cuando llegué encontré todo dado vuelta. Mientras me hacía la idea de que había entrado a robarme, veo que una persona se escapa con una mochila por una ventana y ahí lo empecé a correr”.

“Me agarró una locura tremenda y no pensé en nada”, señaló que atleta argentina, que indicó que logró alcanzar al delincuente, le tiró una patada para frenarlo pero lo lo pudo agarrar y se cayó.

Pese a eso, Thomas se levantó y lo persiguió durante dos cuadras y lo logró alcanzar: “Me dijo que no le haga nada y me decía ‘tomá la mochila’”.

Acto seguido, la atleta contó que lo empezó a insultar y que el hombre la atacó con golpes “en el pecho y la cara”, hasta que llegaron dos personas y la ayudaron.

“Por suerte llegaron dos personas y me ayudaron a reducirlo hasta que llegó la policía. Me dio mucha impotencia la situación y se que no tendría que haber reaccionado así, pero la verdad es que no lo pensé”, indicó.

Thomas indicó que el hombre le había sacado una mochila con una notebook y un celular e indicó que ya está detenido: “Espero que lo tengan mucho tiempo ahí”, indicó en diálogo con TN Deportivo.

Más tarde, la atleta hizo un posteo en sus redes sociales en el que escribió: “Por favor no lo suelten más”, sobre una foto del suceso.