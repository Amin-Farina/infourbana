+++
author = ""
date = 2022-05-11T03:00:00Z
description = ""
image = "/images/mghrmik74rdtreur5czqqa2pl4.webp"
image_webp = "/images/mghrmik74rdtreur5czqqa2pl4.webp"
title = "ESTE ES EL PRÓFUGO POR MATAR A SU EX PAREJA EN EL GRAN ROSARIO: LA ENTERRÓ BAJO UNA CARPETA DE CONCRETO"

+++
#### Los investigadores creen que Gregorio Britez mató a golpes a Nora Escobar en la zona de Granadero Baigorria. Un amigo del sospechoso ya fue detenido. Los resultados de las pericias

En las últimas horas, el Ministerio Público de la Acusación difundió la foto de Gregorio Britez, de 52 años, alias “Yoyo”, prófugo por el femicidio de su ex pareja Nora Escobar en Granadero Baigorria.

La sospecha de violencia de género como contexto del crimen es clara: la hija de la víctima relató varios episodios en los que su madre fue golpeada por el hombre e incluso señaló que le había llegado a fracturar una mano y la nariz, según confirmaron fuentes del caso a Infobae.

El hecho fue descubierto este martes: el cuerpo de Nora fue enterrado en el patio de su casa debajo de una carpeta de concreto. Estaba desaparecida desde el 22 de abril, la fecha de su última comunicación telefónica con allegados, de acuerdo a la denuncia radicada por su hija ante la comisaría 24ª.

También, un amigo del presunto femicida fue detenido. Se trata de Alberto L., un hombre de 58 años que, según la información que tiene la fiscal de la causa, Marisol Fabbro, le prestó dinero a Britez, el sospechoso. Fue arrestado en su casa de La Cumbre al 1400, también en Granadero Baigorria. El hombre dijo conocer a Nora Escobar porque era la pareja de Gregorio. Alberto L. agregó que la última vez que vio a su amigo fue el 1° de mayo pasado, cuando fue a la casa a comer locro por el Día del Trabajador. En ese marco, según fuentes judiciales, admitió haberle prestado dinero en efectivo.

Ante la sospecha de presunto encubrimiento, la fiscal Fabbro formó causa penal contra Alberto L. y fue demorado.

En el marco de la investigación, la hija de Nora dijo ante la Policía que hacía un año que no tenía contacto con su madre. Añadió que a finales de abril se hicieron presentes los empleadores de su mamá (que era empleada doméstica) en la casa de su abuela y manifestaron no haberla visto más desde el 21 de abril pasado. La mujer señaló que su madre corría maratones y que incluso el 24 de abril iba a participar en una, pero como no asistió le llamó la atención.

En ese contexto, la hija de Nora explicó que se dirigió a la casa de su mamá y habló con Britez, quien le respondió que hacía tres meses que no veía a su madre porque se habían separado supuestamente por haberle encontrado mensajes con otro hombre.

La joven, según la presentación policial, comentó también que al ir al domicilio de Liniers de 1700 de Gregorio y su madre le llamó la atención que Nora le dejara a su perra, ya que ella era “muy apegada”, de acuerdo al relato que brindó. La hija de la víctima además recordó ante la Policía que tenía temor a que Gregorio le hiciera algo a Nora, ya que “anteriormente le había quebrado la mano y la nariz” en un episodio de golpes. Enfatizó que de ese hecho su madre hizo una denuncia, pero que luego volvió a salir con “Yoyo”.

_La Fiscalía Regional Segunda Circunscripción solicita la colaboración de la población para dar con Gregorio Brítez de 52 años sindicado por el homicidio de Nora Escobar. Ante cualquier información comunicarse al 911, dirigirse a la Unidad de Homicidios Dolosos de la Fiscalía (Sarmiento 2850) o contactarse a través de las redes sociales de la Fiscalía._