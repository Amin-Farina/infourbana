+++
author = ""
date = 2021-07-06T03:00:00Z
description = ""
image = "/images/22oa4fzubveove23yobvy53btm.jpg"
image_webp = "/images/22oa4fzubveove23yobvy53btm.jpg"
title = "LOMAS DE ZAMORA: LE ROBARON LA CAMIONETA  A UNA MUJER, LA ATROPELLARON Y ARRASTRARON 30 METROS PARA MATARLA"

+++

**_Ocurrió cerca de las 6:30 de ayer en la intersección de las calles Rivera y Espronceda, en inmediaciones de la feria La Salada. La víctima, de nacionalidad boliviana, tenía 45 años._**

Otra vez el conurbano bonaerense es escenario de un hecho violento de inseguridad. En esta oportunidad, una mujer murió al ser atropellada por el auto de los delincuentes que la asaltaron y que segundos antes le habían robado la camioneta en la que se trasladaba junto a su esposo y su hijo en la localidad de Ingeniero Budge, partido de Lomas de Zamora.

El hecho ocurrió cerca de las 6.30 de ayer en la intersección de las calles Rivera y Espronceda, en inmediaciones de la feria La Salada. Al llegar al lugar, efectivos de la comisaría 10º de Lomas de Zamora encontraron a Nancy Luisa Mamani de 45 años, de nacionalidad boliviana, tendida sobre el asfalto y malherida.

Junto a ella estaba su esposo, Jesús Contreras -también de 45 años-, quien relató a los policías que con su mujer y su hijo venían de Pergamino y se dirigían a la feria La Salada cuando se detuvieron en un semáforo en rojo con su camioneta Peugeot Expert blanca y fueron abordados por dos delincuentes armados.

Según confiaron fuentes policiales, el marido relató que los delincuentes los obligaron a bajar y les sustrajeron el rodado y, por causas que se investigan, otra parte de la banda que circulaba en un auto de apoyo atropelló a la mujer luego de que quedara enganchada en el vehículo. En segundos, Nancy fue arrastrada al menos 30 metros a lo largo de la calle Espronceda hasta que quedó tendida en el asfalto.

La secuencia quedó grabada por una cámara de seguridad. En el video se puede observar la brutalidad del episodio y cómo el auto se llevó a Mamani, para luego pasar por encima de ella con las cuatro ruedas. Su marido y el hijo corrieron a auxiliarla, pero la mujer no dio ninguna respuesta.

A los pocos minutos, la herida fue trasladada al Hospital Gandulfo de Lomas de Zamora, donde llegó con traumatismos en cráneo, piernas y pelvis, y si bien ingresó a quirófano y le practicaron maniobras de RCP, murió por la gravedad de las lesiones.

Según consignó la agencia Télam, fuentes de la investigación indicaron que la camioneta Peugeot Expert que había sido robada a las víctimas apareció abandonada en la calle Murature al 3000 de la vecina localidad de Villa Fiorito. Un jefe policial agregó que a unas cinco cuadras de allí, en Murature y Calingasta de la misma localidad del partido de Lomas de Zamora, también fue descartado un Volkswagen Fox blanco que se cree fue el auto con que atropellaron a a la víctima.

El hecho es investigado por el fiscal Nicolás Espejo, a cargo de la Unidad Funcional de Instrucción N°7 de Lomas de Zamora, quien calificó el expediente como homicidio en ocasión de robo, mientras aguardaba para las próximas horas los resultados de la autopsia para confirmar las causas de la muerte.

Entre tanto, la Policía Bonaerense releva en la zona las cámaras de seguridad en busca de más imágenes que les permitan identificar a los delincuentes.

Hacia fines del mes de abril también en la localidad de Ingeniero Budge, otro ciudadano de nacionalidad boliviana, Juan Yuri Conde Lozano, de 59 años, fue asesinado durante un asalto dentro de su local: recibió al menos tres puñaladas en el cuello a manos de un delincuente que luego del ataque se llevó el dinero de la recaudación.

El hecho ocurrió en un comercio ubicado sobre la calle Isaac Newton al 4000, donde además de dedicarse a la zapatería, la víctima vendía hilos y repuestos de máquina.

De acuerdo con el relato de testigos y otros comerciantes de la cuadra, del local salió a toda velocidad un joven vestido con una camiseta rosa del club Boca Juniors y con bermudas de color blanco, de entre 20 y 25 años.

El delincuente corrió por Newton en dirección a la Rivera Sur. Segundos después, cuando entraron a la zapatería, se encontraron una escena de terror: Juan estaba tirado en el suelo, boca abajo a pocos metros del baño y con el cuerpo sobre un charco de su sangre. Inmediatamente, los testigos alertaron al 911.

Al lugar llegaron efectivos del Comando Patrulla de Lomas de Zamora, quienes constataron la muerte de Conde Lozano y que presentaba por lo menos tres heridas de arma blanca en el cuello.