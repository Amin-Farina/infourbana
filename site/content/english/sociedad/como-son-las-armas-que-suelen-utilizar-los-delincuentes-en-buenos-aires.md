+++
author = ""
date = 2021-07-21T03:00:00Z
description = ""
image = "/images/456634-1.jpg"
image_webp = "/images/456634.jpg"
title = "¿CÓMO SON LAS ARMAS QUE SUELEN UTILIZAR LOS DELINCUENTES EN BUENOS AIRES?"

+++

Este lunes por la noche, efectivos de la DDI de San Martín y la SUB DDI de José C. Paz irrumpieron en una casa de la calle 3 de Febrero en el barrio Juan Vucetich del partido bonaerense de José C. Paz. Ahí, la fiscal Marisa Marino, de la UFI Nº 21 de Malvinas Argentinas y su colega Carlos Hermelo, de la UFI Nº 18, iban tras el mismo objetivo: Sergio Daniel Rodríguez, de 39 años, buscado en dos causas por los delitos de robo agravado por el uso de armas, tenencia ilegal de armas de guerra y de arma de uso civil condicional compartida.

“¿Qué pasa?”, preguntaba sorprendido uno de los hombres reducidos durante el allanamiento. Sin embargo, en el operativo no sólo se concretó la detención de Rodríguez y otros dos parientes suyos, sino que los elementos que encontraron agravaron considerablemente su situación ante la Justicia. Dentro de la casa fueron incautadas tres armas de distinto tipo y calibre, una gran cantidad de proyectiles y accesorios, que, según se investiga, podrían haber sido empleados en asaltos recientes en esa zona del Gran Buenos Aires.

El arsenal que los Rodríguez guardaban en esa casa estaba compuesto por un revólver de marca GHD, sin número de serie con 13 proyectiles calibre .32 S&W; un revolver sin marca visible, modelo 38 corto, de color plateado con cachas nacaradas de plástico, y una mini ametralladora.

Esta última se trata de una Intratec TEC-22, en realidad una pistola semiautomática de origen estadounidense, con un cargador con tres proyectiles de marca Super encamisados, accionada por retroceso, que dispara cartuchos calibre .22 long. Es muy común en Estados Unidos, considerada un arma asequible y fácil de manipular. Pero ciertamente es una rareza para la escena de las armas de fuego de la provincia, con un hampa acostumbrada a los viejos modelos argentinos PAM y FMK-3.

Durante el allanamiento en José C. Paz, los efectivos de la Bonaerense encontraron además un accesorio tipo silenciador para la mini ametralladora, un cargador de 9 milímetros de marca Bersa, cinco proyectiles de la marca FLB calibre 9mm, tres proyectiles marca CBC calibre .38, un proyectil de Fabricación Militar calibre .32, un proyectil de vaina plateada calibre .25, tres cartuchos de escopeta discriminados, dos de color transparente de marca CBC calibre .12 de posta de goma y uno de color azul calibre 12. Un verdadero arsenal por el que los tres hombres deberán responder ante la Justicia.

Unos 20 kilómetros más al Sur, pero también en la zona oeste del conurbano bonaerense, en el partido de Morón, personal de la Policía Bonaerense persiguió y se enfrentó tras el robo de un auto con tres delincuentes, que luego fueron aprehendidos y que iban armados, a diferencia del caso anterior, con un revólver de más de 70 años.

El enfrentamiento se produjo cuando dos efectivos de la Comisaría 1ra de Morón avistaron en el cruce de las calles Don Bosco y Agüero un Peugeot 208 gris con cuatro ocupantes que había sido robado algunos minutos antes y comenzaron una persecución para detenerlos.

En ese seguimiento, el auto de los asaltantes frenó para que descendiera un integrante de la banda y luego siguió con tres ocupantes hasta la esquina de Jorge De Kay y Victorino de la Plaza donde descienden los tres y disparan hacia los policías, que también respondieron con tiros e hirieron en una pierna a uno de los delincuentes, de 23 años, que luego fue detenido.

Otro de los ladrones, de 20 años de edad y con domicilio en La Tablada, cayó a 100 metros del lugar, cuando intentó refugiarse en una casa. Estaba armado con un revólver Tanque, calibre .32 largo, provisto de dos proyectiles, que en la década del 30 y 40 era fabricado por la empresa española “Ojanguren y Vidosa” y que hoy se ofertan como reliquias en anticuarios.

El tercer cómplice, de 18 años, también fue detenido a pocos metros del lugar del tiroteo. Dentro del auto, los efectivos encontraron también una pistola Bersa calibre .22 que fue secuestrada.

La UFIJ Nº 19 de La Matanza, a cargo de la fiscal Andrea Palin, dispuso la aprehensión de los delincuentes por los delitos de atentado y resistencia a la autoridad, tenencia ilegal de arma de fuego de uso civil sin autorización y violación de domicilio. También dispuso el secuestro de armas reglamentarias de los efectivos y la realización de peritajes a cargo de la Gendarmería, aunque no adoptó ningún temperamento para con los policías.

La semana pasada, en tanto, en un operativo en el que fueron detenidos dos de los presuntos ladrones que asesinaron a Héctor José Kuciukas -efectivo de la División Conductas Delictivas y Eventos Deportivos de la Policía de la Ciudad de Buenos Aires-, también fue secuestrado un pequeño arsenal presuntamente utilizado en robos.

Tanto en un domicilio ubicado en la localidad bonaerense de Monte Chingolo, partido de Lanús, como en otro en Wilde, en el partido de Quilmes, los efectivos encontraron: pistolones doble caño calibres 14, pistolas Bersa Thunder 9mm, pistolas calibre 22, revólveres y municiones que los delincuentes guardaban en mochilas y ocultas entre su ropa.

Los detectives encontraron también parte de los elementos que utilizaban para simular ser policías y concretar las distintas faenas, como chalecos antibalas y camperas con el escudo de la Policía Federal.