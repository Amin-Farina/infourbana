+++
author = ""
date = 2021-04-09T03:00:00Z
description = ""
image = "/images/como-afrontar-vuelta-trabajo-covid-normalidad-1597849906.jpg"
image_webp = "/images/como-afrontar-vuelta-trabajo-covid-normalidad-1597849906.jpg"
title = "QUIENES YA FUERON VACUNADOS, PODRÁN SER CONVOCADOS A TRABAJAR PRESENCIALMENTE"

+++

**_El Gobierno estableció que los empleadores “podrán convocar al retorno a la actividad laboral presencial a los trabajadores” que hayan recibido “al menos la primera dosis de cualquiera de las vacunas destinadas a generar inmunidad adquirida” contra el coronavirus. Así lo anunció a través de la Resolución Conjunta 4/2021, publicada este viernes en el Boletín Oficial._**

De esta forma, todas las personas en relación de dependencia que cumplan con esa condición podrán regresar a sus puestos “independientemente de la edad y la condición de riesgo, transcurridos 14 días de la inoculación”.

Esta norma también aplica para el personal de la salud con alto riesgo de exposición, aunque en este caso recién estarán autorizados a volver a sus puestos dos semanas después “de haber completado el esquema de vacunación en su totalidad”.

Las únicas excepciones a esta medida son: los empleados con inmunodeficiencias congénitas, asplenia funcional o anatómica (incluida anemia drepanocítica) y desnutrición grave; con VIH dependiendo del status (< de 350 CD4 o con carga viral detectable); con medicación inmunosupresora o corticoides en altas dosis (mayor a 2 mg/kg/día de metilprednisona o más de 20 mg/día o su equivalente por más de 14 días).

También quedan excluidos los pacientes con enfermedad oncohematológica hasta seis meses posteriores a la remisión completa; con tumor de órgano sólido en tratamiento, y trasplantados de órganos sólidos o de precursores hematopoyéticos.