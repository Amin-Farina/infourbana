+++
author = ""
date = 2021-02-17T03:00:00Z
description = ""
image = "/images/7ajcwbpg4zcmdbybrr2mnuzggi.jpg"
image_webp = "/images/7ajcwbpg4zcmdbybrr2mnuzggi.jpg"
title = "Robaron el anillo de Carlos Menem"

+++

La Justicia investiga el robo del histórico anillo del ex presidente Carlos Menem, quien murió este domingo a los 90 años luego de enfrentar diversos problemas de salud. Se trata de la joya de oro con una piedra de ónix que el ex mandatario usaba prácticamente todo el tiempo y que lo acompañó durante toda su presidencia y el tiempo que se desempeñó como senador nacional.

De acuerdo con los primeros datos de la investigación, que está cargo del fiscal Carlos Velarde, titular de la Fiscalía Nacional en lo Criminal Correccional Nº 42, y de la División de Robos y Hurtos de la Policía de la Ciudad, el robo habría ocurrido entre el 6 y 16 de diciembre en la casa que tenía el ex jefe de Estado en la calle Echeverría, en el barrio porteño de Belgrano, mientras estaba internado por primera vez en el Sanatorio Los Arcos.

Diego Storto, abogado de la familia Menem, fue quien confirmó la investigación penal en diálogo con el canal C5N. “Estamos muy avanzados en la investigación. Creo que se está yendo por buen camino”, dijo el letrado, que aseguró, además, que “es inminente” el allanamiento y detención de la o las personas involucradas en el robo. El expediente fue calificado como hurto y ya declararon 15 personas, entre enfermeros, asistentes y empleados del ex presidente.

El anillo en realidad no era el original que le había regalado su padre Saúl, respetando una tradición familiar árabe. De hecho, el hombre también le dio una joya de ese estilo a los hermanos del ex presidente. El que se conoció popularmente y que hoy es el objeto de la investigación judicial es una réplica, cuyo origen se dio irónicamente también durante un desafortunado episodio.

El 19 de diciembre de 1989, cuando asumió como mandatario de los argentinos, Menem perdió la joya. Fue a partir de este hecho que Zulemita, su hija, mandó a hacerle la réplica, que es la que usó durante todo el período como jefe de Estado y como senador nacional. Se trataba de una pieza con un gran valor emocional y había que reponerla. Todas las fotografías durante su máximo esplendor político son con aquel anillo. Era tanta la importancia que tenía la pieza para el ex presidente que en ella tenía grabados los nombres de sus cuatro hijos: Carlitos Junior, Zulemita, Nair y Máximo.

Prácticamente no se lo sacaba; solo cuando iba a dormir. En las noches Menem colocaba el anillo dentro del estuche de uno de los dos anteojos que usaba a diario, lo dejaba en su mesa de luz y al otro día volvía a ponérselo en su mano izquierda. Era un rutina casi irrenunciable que por la internación del diciembre pasado se vio abruptamente modificada.

La denuncia fue radicada por Zulemita el 17 de diciembre en la comisaría 14B, luego de que se descubriera que el anillo había desaparecido. Sucedió cuando Menem pidió que le trajeran sus anteojos, ya que quería ver un partido de fútbol. Cuando su enfermero personal le llevó los estuches con los anteojos desde la casa del barrio de Belgrano, notó que la joya no estaba. El anillo había sido utilizado por última vez el día 4 de diciembre del 2020 por el ex presidente, en una sesión legislativa.

Al momento de la internación, que comenzó el 15 de diciembre con el traslado del ex presidente al Sanatorio Los Arcos, en el domicilio porteño de Menem trabajaban, además de los custodios, tres enfermeros, dos empleados y una asistente. También ingresaron trabajadores que hicieron algunos arreglos en la vivienda.

“Descartamos cualquier tipo de conexión familiar y con la gente cercana que trabajó con él durante toda la vida, la Justicia está poniendo la lupa sobre algunas personas que durante la investigación quedaron más al descubierto probatoriamente, que fueron llamadas a declarar y se notó que falsearon su declaración”, expresó el letrado.

Emilio Méndez Villanueva es uno de los enfermeros que cuidaba a Menem los siete días de la semana. Había iniciado sus vacaciones el 5 de diciembre y fue reemplazado por otros tres por una empresa de medicina prepaga, que atendían al ex presidente en su domicilio. Según indicó el canal TN, previo a notar que faltaba el anillo, el profesional de la salud se había dado cuenta de que él mismo había sufrido un robo en el domicilio: el de su teléfono celular.

Según trascendió, mientras Menem estaba en el sanatorio del barrio de Palermo, el enfermero fue hasta la casa de la calle Echeverría y notó que su teléfono había desaparecido. Cuando la familia del ex presidente se percató de que la joya también había sido robada, Méndez Villanueva ahí sí reveló lo que había pasado con el celular. Ahora, la Justicia deberá determinar si ambos hurtos tienen algún tipo de relación. Los que se sabe hasta el momento -según la señal de noticias- es que uno de los empleados que trabajaba en la casa de Menem tiene antecedentes por robo.

Al respecto, el abogado que representa a Zulemita, explicó esta mañana que la denuncia no se había dado a conocer hasta este momento por el estado de salud delicado en el que se encontraba el ex presidente, pero que la causa “está avanzando” y “está yendo por buen camino”. Según dijo un asistente de Menem al enfermero, la última vez que vio el anillo en la mesa de luz fue el 13 de diciembre. Eso reduce el período del presunto robo a tres días.

Tras la denuncia, la Policía de la Ciudad inició las investigaciones para determinar los domicilios de los nuevos enfermeros (dos mujeres y un hombre). Además, por orden del fiscal interviniente, el 12 de febrero se realizaron tres allanamientos: dos en la Ciudad y uno en la Provincia, con resultados negativos.

La Policía de la Ciudad también solicitó las imágenes de las cámaras de seguridad del domicilio de Menem entre el 6 y el 17 de diciembre, pero en esas fechas las cámaras no funcionaron.