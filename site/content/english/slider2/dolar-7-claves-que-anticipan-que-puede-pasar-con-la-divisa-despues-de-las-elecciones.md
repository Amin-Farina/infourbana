+++
author = ""
date = 2021-08-30T03:00:00Z
description = ""
image = "/images/el-socialismo-puede-esperar-los-dolares-vuelven-a-estar-de-moda-entre-la-izquierda-latinoamericana-1.jpg"
image_webp = "/images/el-socialismo-puede-esperar-los-dolares-vuelven-a-estar-de-moda-entre-la-izquierda-latinoamericana.jpg"
title = "DÓLAR: 7 CLAVES QUE ANTICIPAN QUÉ PUEDE PASAR CON LA DIVISA DESPUÉS DE LAS ELECCIONES"

+++

**_Según un informe de GMA Capital, no habrá cambios con el dólar “hasta las elecciones” porque “no es novedad que atrasar el tipo de cambio es deporte nacional durante la contienda electoral”. Con dicho objetivo político se fuerza esa estrategia._**

Sin embargo, el escenario pude necesariamente virar una vez pasados los comicios del 14 de noviembre y, “mirando los años electorales con cepo cambiario, los seis meses posteriores a los comicios, los movimientos del dólar oficial representaron, en promedio, 72% del cambio de los últimos 12 meses”.

La gran pregunta que tiene el mercado es qué sucederá con el tipo de cambio después de las elecciones. Más concretamente, cómo será la política cambiaria: ¿será un salto discreto del dólar, o una aceleración del “crawling peg”?

El informe, elaborado por Nery Persichini y Melina Eidner, desarrolla siete factores que hablan de la dinámica cambiaria poselecciones y demuestra que las expectativas del mercado apuntan a un dólar más alto:

1) La situación internacional de la soja. Con la cuenta financiera cerrada a los movimientos de capitales, la única entrada genuina de divisas ocurre a través del canal comercial, especialmente con las exportaciones del agro, indicó pero “el milagro del ‘yuyo’ por encima de USD 500 la tonelada, que permitió que este año el efecto precio más que compensara el impacto en la cantidad, difícilmente pueda repetirse en 2022″, advirtió.

Y subrayó: “Con menos dólares comerciales a la vista, al Central se le dificultaría captar divisas, especialmente en un año en el cual los pagos de deuda en moneda dura suman más de USD 28.000 millones o 2,7 veces las reservas netas actuales”.

2) La estacionalidad de las intervenciones cambiarias del BCRA. Gracias a la performance récord del agro, de cada USD 100 que el BCRA compró en el MULC, USD 35 engrosaron reservas en lo que va del 2021 sin contabilizar el ingreso de los DEG del FMI.

“La mayor presión se vio durante agosto y octubre, que son meses en los cuales históricamente se votó. No obstante, esta dinámica se agudiza si se toman únicamente en cuenta los años impares con cepo cambiario”, agregó GMA.

3) Los rendimientos de los bonos dollar-linked. “Un termómetro elocuente del dólar más allá de las elecciones es el de los bonos dollar-linked. Las tasas de esta curva con USD 4.300 millones de valor nominal se mueven al compás de las expectativas de mercado. Cuando las tasas se vuelven negativas es sinónimo de un crecimiento en la demanda de cobertura”, puntualizó el análisis que, según las principales emisiones del mercado (T2V1, TV22 y el T2V2) “pronostica mayores tensiones cambiarias que en el peor momento de la historia reciente de los dollar-linked y de la brecha contra el contado con liquidación”.

4) Las relaciones de arbitraje entre bonos en pesos y los atados al tipo de cambio oficial. “Otra bandera roja de que el dólar difícilmente se comporte como hasta ahora una vez atravesado el Rubicón de los comicios es la comparación entre títulos en pesos y los mencionados dollar-linked”, aseveró el estudio. Recalcó que “todo el ajuste que no se vea en el dólar oficial en 2021, según los precios de los bonos, tendría lugar en 2022. Para colmo, esto no solo implica rendimientos dollar-linked más negativos, sino también tasas en pesos más altas. Esto, lejos de ser un hecho anecdótico, le pondría obstáculos al Tesoro a la hora de cumplir con su programa financiero”.

5) Las tasas implícitas en futuros. GMA consideró que “el costo de la cobertura post noviembre es otra muestra del cambio de paradigma cambiario que el mercado está barajando”. Precisó que “el cambio en las posiciones desde noviembre a diciembre muestra una variación mensual de 5,1%, que alcanza 81,6% anualizada. La dinámica de enero sería aún más acelerada: 5,4% mensual y 87,8% anualizada”.

6) El hecho de que 2021 sería el tercer año de mayor apreciación real en 16 años. En este escenario, GMA Capital aseguró que “un cambio de estrategia es impensado a esta altura del partido”, con lo cual “la marcha (hacia las elecciones) continuaría al compás de un ritmo de devaluación mensual que viene desacelerándose sistemáticamente desde enero: pasó de ser 3,9% en el primer mes del año a viajar por debajo de 1% en agosto”.

7) La historia reciente de los años impares centrada en el evento electoral. En ese sentido, GMA afirmó que “si se excluye 2015, ya que en diciembre se desmanteló el cepo anterior, casi 2/3 del incremento del tipo de cambio mayorista se dio en el semestre pos-elecciones”.