+++
author = ""
date = 2021-06-16T03:00:00Z
description = ""
image = "/images/entrenamiento-de-la-seleccion-de-argentina-de-futbol_45471593.jpg"
image_webp = "/images/entrenamiento-de-la-seleccion-de-argentina-de-futbol_45471593.jpg"
title = "ARGENTINA ENTRENA Y SCALONI DISEÑA CÓMO JUGARLE A URUGUAY"

+++

**_Luego del debut con empate ante Colombia, el seleccionado nacional prepara su segundo duelo del torneo continental de selecciones, mientras el entrenador Lionel Scaloni piensa en qué equipo parar en cancha._**

Este miércoles, la Argentina realizó un nuevo entrenamiento con miras a la segunda fecha del Grupo A de la Copa América 2021 que se disputa en Brasil que será ante el seleccionado "Celeste", equipo que arribó este miércoles a tierras "verdeamarelhas".

El conjunto "albiceleste", desde las 17 y en el predio de la AFA que tiene en Ezeiza, realizó un nuevo entrenamiento a las órdenes del cuerpo técnico para trabajar el equipo de cara al juego del viernes desde las 21 (hora argentina) ante Uruguay en el estadio Mané Garrincha de Brasilia.

"Otro día de intensos trabajos para preparar el próximo partido ante Uruguay. ¡Seguimos firmes y unidos!", indicó la publicación de las redes sociales de la Selección”.

Mientras tanto en lo deportivo, Scaloni y sus dirigidos analizan si en defensa seguirá Lucas Martínez Quarta o bien regresará - en lo que será su debut en el torneo- de Cristian Romero, tras una lesión durante el duelo ante Colombia en Barranquilla por Eliminatorias Sudamericanas.