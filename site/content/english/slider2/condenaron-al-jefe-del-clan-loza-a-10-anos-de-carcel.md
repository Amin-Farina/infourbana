+++
author = ""
date = 2021-12-03T03:00:00Z
description = ""
image = "/images/clan-loza-1.jpg"
image_webp = "/images/clan-loza.jpg"
title = "CONDENARON AL JEFE DEL \"CLAN LOZA\" A 10 AÑOS DE CÁRCEL"

+++

#### **_Se trata de Erwin Raúl Loza, a quien el Tribunal Oral en lo Penal Económico (TOPE) 3 lo acusó ser el jefe de una "asociación ilícita" para la comercialización y el contrabando de drogas desde Argentina hacia España entre 2008 y 2018._**

El líder del denominado "Clan Loza", una estructura criminal dedicada al lavado de activos con ganancias provenientes de la comercialización y el contrabando de drogas desde Argentina hacia España entre 2008 y 2018, fue condenado este jueves a 10 años de prisión y a pagar una multa de 378 millones de pesos.

Se trata de Erwin Raúl Loza, a quien el Tribunal Oral en lo Penal Económico (TOPE) 3 lo consideró "jefe" de una "asociación ilícita", mientras que William Oscar Weston Millones recibió la pena de 8 años de cárcel como "organizador" de la misma banda.

En total fueron 10 las personas condenadas por los delitos atribuidos al "Clan Loza", que abarcaron desde la figura de asociación ilícita hasta lavado de activos y contrabando de divisas, según el veredicto que leyó el juez Luis Imas y que los imputados escucharon a través de la plataforma virtual Zoom.

La lectura del veredicto estaba prevista para las 14.30, pero se demoró porque los jueces Imas, Karina Perrilli y Jorge Zabala precisaron algunos minutos más para afinar los detalles del fallo cuyos fundamentos se darán a conocer el 2 de marzo del 2022.

Pasadas las 16, cuando finalmente se dio inicio a la lectura del veredicto, el acto procesal debió interrumpirse porque se cayó la conexión con el penal de Marcos Paz donde están detenidos siete de los imputados.

Además de los dos cabecillas, Gerardo Cesar Cuccione fue condenado a 7 años de prisión, Estela Mari Gallo, esposa de Erwin Loza; y Gonzalo Loza a 6; Clara Luz Fernández a 5; Juan Carlos Fernández y Américo Alfredo Santi a 4 y medio; y Javier Silveira López y Alan Loza a 4.

El juicio se llevó a cabo de de manera semipresencial: siete de los acusados siguieron las audiencias desde la cárcel de Marcos Paz, una desde el penal de Ezeiza y dos desde sus casas, ya que fueron beneficiados con el arresto domiciliario.

La acusación estuvo a cargo del fiscal general en lo Penal Económico, Gabriel Pérez Barberá, y el auxiliar fiscal de la Procuraduría de Narcocriminalidad (Procunar), Matías Álvarez, quienes durante los alegatos habían pedido el decomiso de todos los bienes adquiridos por la organización con dinero proveniente supuestamente del lavado.

Entre esos bienes, que este jueves fueron reseñados por el tribunal, hay inmuebles, vehículos de alta gama, entre ellos una Ferrari que perteneció a Diego Armando Maradona en la década del '90, como así también dinero en efectivo que fue hallado en cuentas bancarias, maquinarias, relojes de alta gama y joyas, entre otros elementos.

De acuerdo con la investigación desarrollada por los fiscales, la estructura criminal adquiría sustancias ilícitas -principalmente clorhidrato de cocaína- en la Argentina y otros países de Sudamérica, y los transportaba, por vía marítima o aérea, a España, "donde violaba los controles aduaneros para ingresar la mercadería ilícita, que era distribuida para su comercialización en distintos puntos de Europa".

"Las ganancias generadas por la venta de la droga eran transportadas a la Argentina, donde se ponían en circulación mediante la compra de muebles e inmuebles, para darles apariencia lícita", dijeron los fiscales en su alegato.

"La actuación de esta asociación criminal tuvo impacto también en otros países como Italia, Irlanda, Inglaterra, Bolivia, Colombia y Perú", detalló el fiscal Álvarez, quien agregó que la estructura delictiva "presentó vinculaciones con otras células criminales asentadas en esos estados como también en España o Argentina" y que esos vínculos "se mantuvieron en las distintas etapas de la actividad narcocriminal".

Por su parte, Erwin, apodado "El Nene", de 43 años, fue apresado junto a otras 37 personas en la "Operación Cambalache" realizada en diciembre de 2018 tras una larga investigación.