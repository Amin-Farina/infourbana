+++
author = ""
date = 2021-11-30T03:00:00Z
description = ""
image = "/images/industria-automotriz-1140x570-1.jpg"
image_webp = "/images/industria-automotriz-1140x570.jpg"
title = "¿ESCASEZ DE AUTOS? EL PATENTAMIENTO DE 0KM CAYÓ MÁS DE 18% INTERANUAL EN NOVIEMBRE POR LA FALTA DE OFERTA"

+++

#### **_Según la Asociación de Concesionarias de Automotores (Acara) se liberaron en los últimos dos meses solo entre 9 mil y 10 mil unidades importadas cuando se podrían haber entregado 15 mil por mes. Cuáles son los modelos más vendidos_**

La cantidad de autos patentados durante noviembre alcanzó las 28.360 unidades, lo que arrojó un descenso del 18,7% interanual y un aumento del 1% respecto a octubre, según el último reporte de la Asociación de Concesionarias de Automotores (Acara).

De esta manera, en los once meses acumulados del 2021 se patentaron 363.931 unidades, equivalentes a una mejora del 13% frente al mismo período de 2020, cuando se registraron 321.893 vehículos.

La proyección para el cierre del año es de unos 380.000 vehículos 0 Km patentados, con lo que estiman que 2022 tendrá un piso de ventas de 400.000 unidades.

De acuerdo con el reporte de Acara, en noviembre se vendieron 28.360 unidades, lo que representó una baja del 18,7% interanual, ya que en noviembre de 2020 se habían registrado 34.887 unidades.

“Si la comparación es con su antecesor mes, se observa una suba del 1%, ya que en octubre de este año se patentaron 28.085 unidades”, indicó la entidad.

Tras darse a conocer las cifras, el presidente de Acara, Ricardo Salomé, dijo que en el sector “nos ilusionamos con poder crecer el año que viene y establecer un piso de 400.000 vehículos para todo el 2022″.

“Ojalá así sea porque somos una fuente de ocupación de empleo muy importante en el país: todo el sector incluye 176.000 empleos directos, y si tomamos el indirecto esa cifra crece tres veces más”, agregó el titular de Acara.

No obstante, Salomé lamentó que “se han liberado en los últimos dos meses solo entre nueve y diez mil unidades importadas. Creemos que se podrían haber liberado 15.000 por mes ya que vamos a terminar el año solo con aproximadamente 1.130 millones de dólares de déficit anual del sector”.

“Es un número que si se distribuye en doce meses no es significativo, más si tenemos en cuenta que la demanda ha sido muy fuerte. De esta forma, vamos a cerrar el año arañando los 380.000 vehículos patentados, pero con la frustración de que podríamos haber patentado 450.000 unidades”, expresó el directivo.

Por su parte, Rubén Beato, secretario General de la entidad, dijo que “pese a la coyuntura, esta es una industria que está en el buen camino, los concesionarios están acompañando y el objetivo es integrarse al mundo a través del Proyecto 2030, donde hasta se han presentado leyes en el Congreso Nacional para ir a un escenario de movilidad sustentable y acompañando las tendencias futuras”.