+++
author = ""
date = 2021-07-04T03:00:00Z
description = ""
image = ""
image_webp = ""
title = "SOMETIERON AL PAPA FRANCISCO A UNA INTERVENCIÓN QUIRÚRGICA"

+++

**_El papa Francisco, de 84 años, es operado hoy de una "estenosis diverticular" en el colon en un centro asistencial de Roma, en el marco de una intervención programada, informó a Télam el vocero papal Matteo Bruni._**

La operación se lleva adelante en el Policlínico A. Gemelli de la capital italiana, detalló Bruni.

"La cirugía será realizada por el Prof. Sergio Alfieri", agregó un comunicado vaticano.Según la Santa Sede, "al finalizar la cirugía, se emitirá un nuevo boletín médico" con el estado de salud de Jorge Bergoglio.

Antes de la internación, Francisco encabezó sin problemas el tradicional Ángelus desde el Palacio Apostólico y anunció su viaje de septiembre a Budapest y cuatro ciudades de Eslovaquia.

En el Policlinico Gemelli el Papa tiene una habitación disponible en el piso 10, la misma en la que, en 1981 el entonces papa Juan Pablo II se recuperó del atentado sufrido en Plaza San Pedro.

Durante julio, el Papa tiene suspendidas las audiencias generales de los miércoles, como todos los veranos, y solo mantendrá los Ángelus dominicales.

Esta es la segunda operación a la que se somete el pontífice desde que fue elegido en 2013, luego de que en 2019 fuera operado de cataratas en el hospital Pio XI de la capital italiana.

Hoy es la primera vez que el Papa va a l Gemelli, luego de que, el 27 de junio de 2014, se cancelara a última hora la visita que debía realizar al Hospital Universitario para celebrar la misa con los enfermos y sus familiares por una indisposición inesperada.