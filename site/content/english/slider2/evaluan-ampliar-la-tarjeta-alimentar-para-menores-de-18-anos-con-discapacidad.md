+++
author = ""
date = 2021-04-20T03:00:00Z
description = ""
image = "/images/1584903196_102186_1584903529_noticia_normal.jpg"
image_webp = "/images/1584903196_102186_1584903529_noticia_normal.jpg"
title = "EVALUAN AMPLIAR LA TARJETA ALIMENTAR PARA MENORES DE 18 AÑOS CON DISCAPACIDAD"

+++

**_El ministro de Desarrollo Social, Daniel Arroyo, afirmó que desde el gobierno nacional, están “apuntando a llegar al conjunto de las personas que tienen discapacidad y se ven afectadas para insertarse laboralmente, y más en esta pandemia”._**

El ministro de Desarrollo Social, Daniel Arroyo, aseguró que el Gobierno nacional evalúa ampliar el alcance de la Tarjeta Alimentar, para incluir a personas con discapacidad menores de 18 años.

“Estamos trabajando para incorporar a las personas con discapacidad hasta 18 años en la Tarjeta Alimentar”, aseguró el funcionario a la radio FM Metro.

En ese marco, explicó que “la Tarjeta Alimentar hoy es para madres con chicos menores de 6 años, para mujeres a partir del tercer mes de embarazo y para personas con discapacidad que tienen una asignación”.

En este sentido, afirmó que, desde el gobierno nacional, están “apuntando a llegar al conjunto de las personas que tienen discapacidad y se ven afectadas para insertarse laboralmente, y más en esta pandemia”.

En otro orden, confirmó que el próximo 27 de abril se reunirá el Consejo del Salario, de donde surgirá un incremento del monto que cobran quienes participan del plan Potenciar Trabajo.

“Hay 870.000 personas en el plan Potenciar Trabajo, en el cual el Estado paga la mitad del salario mínimo. Hoy el salario mínimo son 21.600 pesos y un plan del Potenciar Trabajo son 10.800 pesos. A partir de que suba el salario mínimo habrá también un incremento para este plan”, manifestó Arroyo.

Al repasar el abanico de ayudas sociales en el marco de la segunda ola de coronavirus, Arroyo dijo que “el bono de 15.000 pesos” que se dispuso durante 15 días en el marco de las nuevas restricciones “llega a un millón de familias”.

“También está el Repro II, que lo paga el Ministerio de Trabajo sobre los salarios del sector privado, y alcanza sobre todo a gastronomía, turismo y eventos culturales, los sectores más afectados, y son 18.000 pesos” por trabajador, amplió el funcionario.

Sobre ese programa, Arroyo dijo que ahora se contemplará también para los pequeños emprendedores, que anteriormente no estaban alcanzados por el beneficio.

En el plano alimentario, el funcionario recordó que “hay 10 millones de personas que reciben asistencia alimentaria” y que “se aumentó un 50% el monto de la Tarjeta Alimentar, que llega a un millón de familias, y además se reforzaron las partidas para comedores y merenderos”.