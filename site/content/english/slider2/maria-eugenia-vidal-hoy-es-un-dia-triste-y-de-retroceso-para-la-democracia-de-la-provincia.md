+++
author = ""
date = 2021-12-29T03:00:00Z
description = ""
image = "/images/vidal-me.jpg"
image_webp = "/images/vidal-me.jpg"
title = "MARÍA EUGENIA VIDAL: “HOY ES UN DÍA TRISTE Y DE RETROCESO PARA LA DEMOCRACIA DE LA PROVINCIA”"

+++

#### **_La ex gobernadora habló con Infobae sobre el proyecto aprobado por la Legislatura bonaerense que permitirá la reelección de unos 90 intendentes en 2023. Lamentó que ahora se habilitará a los jefes comunales a quedarse 12 años en el poder._**

Entre profundas diferencias en la coalición opositora, María Eugenia Vidal fue contundente. Pocos minutos después de que el kirchnerismo y un sector de Juntos votaron en la Legislatura bonaerense el proyecto que les permitirá a unos 90 intendentes volver a presentarse en 2023, la ex gobernadora dijo:

> “Hoy es un día triste y de retroceso para la democracia de la provincia. Volvimos atrás con una ley limitaba la elección de un intendente a dos mandatos y obtuvieron uno más, habilitándolos a quedarse 12 años en el poder”.

Para la diputada nacional de Juntos, “los argumentos sobre los abusos de algunos intendentes en la reglamentación de la ley se resolvían derogando la reglamentación, no habilitando un mandato nuevo para todos”.

Aun así, Vidal rescató “la actitud de diputados y senadores de distintos partidos que votaron en contra, defendiendo con coherencia lo que es mejor para la gente”. “Aunque mi posición no fue la que prevaleció -agregó-, esto para mí nunca se trató de ganar o perder sino de defender mis convicciones, sin especulaciones y en coherencia con lo que siempre dije e hice”, dijo.

El proyecto aprobado por la Legislatura provincial fue el resultado de negociaciones entre el peronismo y un sector de Juntos por el Cambio, que se pusieron de acuerdo en modificar la ley sancionada en 2016, a comienzos del gobierno de Vidal, con el aval del Frente Renovador de Sergio Massa. El decreto de reglamentación de esa norma permitió que una veintena de intendentes del Frente de Todos encontraran una interpretación que les permitirá sortear la prohibición de tener más de dos reelecciones consecutivas.

La ley N° 14836 les impide una nueva reelección a diputados, senadores, concejales y consejeros escolares de la provincia de Buenos Aires que, “habiendo sido reelectos en el mismo cargo para un segundo mandato consecutivo, hayan asumido sus funciones y ejercido por más de dos años, continuos o alternados”. Ese párrafo ocasionó una oleada de pedidos de licencia de intendentes del peronismo antes de cumplir dos años y un día de su segundo mandato, algo que les permitirá postularse de nuevo en 2023.

Para el PRO, el rechazo a la reelección indefinida que impulsó Vidal en su momento parecía una bandera ética indiscutible, pero, sin embargo, hubo muchos intendentes del espacio que se quejaron porque la situación legal terminaba favoreciendo a sus colegas peronistas que no tenían ningún problema en pedir licencia para desempeñarse en el gobierno de Axel Kicillof, dejarles el cargo a parientes o amigos y esperar la posibilidad de volver a presentarse para el mismo puesto dentro de dos años.

La conducción nacional del PRO rechazó el 10 de diciembre cualquier norma que ponga en vigor nuevamente la reelección indefinida, pero la veintena de intendentes del espacio ya se habían pronunciado en favor de acompañar la sanción de una norma que les permitiera ser reelegidos en 2023, como el proyecto presentado por el diputado provincial del Frente de Todos Walter Abarca y que modificaba el artículo que impedía la reelección indefinida de los intendentes. La elección del intendente municipal, afirmaba, debe estar “determinada únicamente por el voto popular, universal, igual, secreto, libre y obligatorio de los vecinos del Municipio”.

De todas formas, las fisuras en Juntos por el Cambio se extendieron porque la UCR también estaba de acuerdo en permitir la continuidad de los jefes comunales. La propuesta que se terminó imponiendo surgió del senador bonaerense de Juntos Juan Pablo Allan, quien planteó la modificación del artículo 7 de la ley 14.836 que limita las reelecciones indefinidas con este cambio: “Los mandatos de intendentes, concejales, consejeros escolares, diputados y senadores que se hayan iniciado como resultado de las elecciones del año 2017, 2019 y 2021 serán considerados como primer período a los efectos de la aplicación de la presente ley”. Con este párrafo se habilitará a los jefes comunales que hayan sido elegidos en 2015 y reelegidos en 2019 a competir por un tercer período.

El proyecto de Allan fue aprobado esta tarde en el Senado provincial, con la oposición del Frente Renovador en ambas cámaras y el voto dividido de Juntos, una postura que profundizará las diferencias internas. Para confirmar la polémica que se abrió, la jefa del PRO, Patricia Bullrich, se diferenció de Vidal: “Si no derogás esa ley y dejás esa reglamentación, te van a seguir haciendo la jodita de irse”, dijo a TN. “Lo primero que se debería derogar es el decreto reglamentario”, sostuvo.

“La ley de 2016 decía que eran dos mandatos consecutivos. Eso es razonable -señaló Bullrich-. Luego, en 2019, se planteó que si renunciabas o pedías una licencia dos años antes no se tomaba como mandato. Entonces, ahí hay una trampa”. La ex ministra de Seguridad consideró que “por eso, se escaparon 20 intendentes del justicialismo y el kirchnerismo que se fueron para ser reelectos”. “Se generó una situación confusa -agregó-. La reglamentación dio lugar a esa trampa”.

En la visión de un senador bonaerense de Juntos, lo que se votó en la Legislatura tuvo un sentido claro: “Ratificar vehementemente que estamos en contra de las reelecciones indefinidas. Hubo que modificar parte de la ley porque el atajo había hecho que la utilización de la misma a favor de los intendentes kirchneristas pusiera al frente de Juntos en inferioridad de condiciones para la próxima elección. Entendemos que no es la mejor salida de todas, pero había que encontrarle una solución política al tema”.

En la vereda de enfrente, el grupo de legisladores bonaerenses que responde a Vidal votó en contra de extender otro mandato a intendentes que fueron elegidos en 2015 y reelegidos en 2019 y afirmó: “El valor de la palabra se defiende y se cuida porque el compromiso asumido con los bonaerenses es mucho más importante que cualquier interés partidario o personal”.

“Entendemos que la Ley 14.836 impulsada por la ex gobernadora María Eugenia Vidal en 2016 era clara y significaba un verdadero punto de inflexión para la democracia, poniéndole fin a las elecciones indefinidas y a los políticos eternos. Por eso votamos en contra de cualquier modificación que cambie la esencia de la ley”, manifestaron los legisladores de Juntos Alex Campbell, Juan Carrara, Johanna Panebianco, Santiago Passaglia, Anastasia Peralta Ramos, Matías Ranzini, Noelia Ruiz y Sergio Siciliano.