+++
author = ""
date = 2021-04-23T03:00:00Z
description = ""
image = "/images/5f90b99cde931_1004x565.jpg"
image_webp = "/images/5f90b99cde931_1004x565.jpg"
title = "MILITANTES DE JUAN GRABOIS PODRÍAN IR A JUICIO POR LA USURPACIÓN DEL CAMPO DE LUIS ECHEVEHERE "

+++

**_Dolores Etchevehere, su abogado Facundo Taboada y el ingeniero agrónomo Lautaro Leveratto Lazzarini eran los tres apuntados por la fiscalía, pero la querella del ex ministro reclamó que también se indague a los integrantes del Grupo Artigas._**

La Fiscalía pretendía cerrar la investigación por la usurpación de Casa Nueva, el campo de Luis Miguel Etchevehere, apuntando a tres personas: Dolores, la hermana del ex funcionario; su abogado Facundo Taboada; y el ingeniero agrónomo Lautaro Leveratto Lazzarini. Sin embargo, la querella particular del ex ministro reclamó que se apunte también a 30 integrantes del denominado Grupo Artigas, que respondía al dirigente social Juan Grabois.

Desde el 15 de octubre del año pasado y durante dos semanas, el país estuvo en vilo por la toma de la estancia Casa Nueva en la zona del Quebracho, un paraje rural ubicado 130 kilómetros al norte de Paraná, la capital entrerriana. Un grupo integrado por militantes que respondían a Juan Grabois irrumpió y se asentó para llevar adelante un proyecto agroecológico. Al frente de estas 40 personas iban Dolores Etchevehere, Taboada y Lazzarini. Se autodenominaron “Proyecto Artigas”, apelando a la tradición del caudillo y a sus ideas sobre la reforma agraria.

El hecho provocó una airada reacción de los sectores ruralistas que veían en el incidente un ataque a la propiedad privada y temían que se generara un efecto dominó sobre otros campos. La Justicia actuó y, tras un primer fallo adverso, Luis Miguel Etchevehere, sus hermanos y su madre Leonor lograron que en los Tribunales se escuche su reclamo y se ordene el desalojo del campo.

El “Proyecto Artigas” se dispersó. La mayoría de los militantes que se habían intrusado para comenzar a sembrar perejil y otras plantas volvió al conurbano bonaerense, de donde habían partido. Pero la causa penal que se había abierto en el Juzgado de Garantías de la ciudad de La Paz prosiguió.

#### Seis meses más tarde

En el legajo de investigación abierto quedaron las indagatorias a las que fueron sometidos Dolores Etchevehere, Taboada y Leveratto Lazzarini y la identificación de una treintena de personas que estuvieron dentro del campo. Y poco más.

La Fiscalía interviniente, a cargo del Dr. Oscar Sobko y de la Dra. Constanza Bessa, tiene la intención de pedir la elevación de la causa a juicio. Si es que no existe la posibilidad de que el problema se resuelva a través de un juicio abreviado, un mecanismo que prevé la legislación entrerriana para evitar que las causas lleguen a debate.

“Nosotros vamos contra quien efectivamente podemos probar el dolo”, indicó Sobko a Infobae al momento de justificar que la acusación se enfoque sólo en tres personas y no en la totalidad de los que participaron del copamiento de la estancia.

El planteo para que la causa sea llevada a juicio oral y público ya está bosquejada, casi lista para ser elevada al juez de Garantías de La Paz, Walter Carballo.

No obstante, la querella particular no está conforme con el planteo y requerirá que se impute y se tome declaración indagatoria a todos los integrantes del grupo de Grabois.

#### Las razones

Este jueves por la tarde Rubén Pagliotto, el abogado de Luis Miguel Etchevehere y su familia, presentó un requerimiento en ese sentido ante la Fiscalía.

“Este caso no puede concluirse con la imputación y remisión a juicio de sólo tres de ellos, aunque se trate de quienes ostentaban el liderazgo del grupo de ocupantes clandestinos, sin perjuicio de que quien claramente lideraba el grupo y hacía gala de ello en cuanto medio le acercaba un micrófono o cámara fue el abogado capitalino Juan Grabois”, planteó el letrado.

Circunscribir la comisión del delito a Dolores, Taboada y Leveratto “es, cuanto menos, un acto asaz irrazonable, incompleto, injusto y que, de forma velada aunque grosera, consagra la impunidad, desembarazando sin justificativo alguno a quienes con plena conciencia de lo que hacían, despojaron a Las Margaritas SA (la empresa de los Etchevehere dueña del predio) de unos los bienes que integran su capital social”, precisó Pagliotto.

Asimismo, remarcó que los usurpadores “perpetraron hechos de intimidación, amenazas y de restricción de la libertad al personal del establecimiento como también daños y destrozos en la propiedad, sin olvidar que alteraron y resintieron la operatividad normal de la unidad agropecuaria”.

El letrado requirió el listado de las personas que integraron el Grupo Artigas, que fueron identificadas por la Policía provincial durante el procedimiento de desalojo, y que consta en la causa. El objetivo es que sean imputados, indagados y llevados a juicio para determinar su grado de participación en los hechos ilícitos que se investigan.

Si la Fiscalía da curso al planteo, algo que es de esperar, la nómina será enviada a la querella, que solicitará que se los impute y se los indague. El trámite demandará un tiempo más, postergando el juicio.