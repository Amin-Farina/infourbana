+++
author = ""
date = 2021-07-29T03:00:00Z
description = ""
image = "/images/604e5d5657258nancypazosydiegosantilli.jpg"
image_webp = "/images/604e5d5657258nancypazosydiegosantilli.jpg"
title = "NANCY PAZOS SOBRE SANTILLI: \"LE SALE BÁRBARO HACERSE LA VÍCTIMA\""

+++

**_En una entrevista radial que mantuvo con la precandidata a diputada nacional bonaerense por el Frente de Todos, Victoria Tolosa Paz, la periodista Nancy Pazos la aconsejó sobre las formas que debía manejar con su ex marido y candidato de Juntos en Buenos Aires, Diego Santilli. En esa línea, manifestó que "le sale bárbaro hacerse la víctima”._**

“Vas a tener que ir moderando un poco tus formas, a ver si terminás siendo la villana de la película vos también”, manifestó Pazos en un pasaje de la charla con Tolosa Paz, en su programa Ruleta Rusa en FM Rock & Pop.

Con consejos que oscilaban entre el tono del chiste, la ironía y situaciones de la realidad, la periodista que estuvo casada con Santilli dijo que “una de las características” del vicejefe de Gobierno porteño “es que le sale bárbaro hacerse la víctima”.

Luego de que Tolosa Paz recordara su rol como coordinadora de la unidad de ministros de la cartera de Desarrollo Social dirigida Alicia Kirchner, Pazos contó un recuerdo propio.

“Lindos momentos esos, recuerdo perfectamente cuando mi exmarido asume el Ministerio de Espacio Público de la Ciudad. Después del acto de asunción vamos a reconocer el despacho. Es un momento icónico, que uno recuerda de por vida”, dijo la conductora radial. 

Luego de comentar la diferencia de edad entre Tolosa Paz y su marido, José "Pepe" Albistur, Pazos sostuvo que está mal asociar a las mujeres con sus parejas dentro de la política.

“Las relaciones parentales entre la gente que se dedica a lo mismo es como decirme a mí ‘la ex de Santilli’. No, boludo. Era yo antes que él. Es una cosa muy machista, porque a mí todo el tiempo me preguntan y después me dicen ‘soltalo’. Me preguntan y yo contesto porque no le hago asco a nada, ni siquiera a contestar sobre Santilli”, aceleró la periodista.

> “Vas a tener que ir moderando un poco tus formas"

De esa manera aconsejó Nancy Pazos a Victoria Tolosa Paz sobre el tono que debería mantener con Diego Santilli, en caso de que el precandidato de Juntos triunfe en la interna contra Facundo Manes.

“Cuando estuvimos juntos se terminó yendo de casa, fue todo un desastre. Pasó hace un montón de tiempo, y ya está todo más que bien. Pero fui abandonada, dejada, me engañó con otra, se fue, se casó al poco tiempo, y yo terminé siendo la villana de la historia, lo cual es una cosa increíble, pero tiene esa capacidad”, develó la conductora de Ruleta Rusa.

Luego, Tolosa Paz expresó su tono aguerrido para hacer política y habló sobre el enroque de Santilli con la ex gobernadora bonaerense María Eugenia Vidal, que se postuló en la Ciudad de Buenos Aires como precandidata a diputada nacional. 

“Ninguno de los dos puede sostener el proyecto político de Mauricio Macri", dijo la ex precandidata a intendenta en La Plata.  Con ese plafón, Pazos hizo otro linkeo con Santilli. “Igual, te digo una cosa: no sabés lo bien que sale de todas las situaciones. Ustedes también tenían uno parecido, que era Scioli, que hace como ‘ole’ a muchas de las cosas. Veremos ahí la capacidad de ustedes”, manifestó la periodista.

> "La próxima hagamos al revés: vos me preguntás a mí"

Con tono jocoso, Nancy Pazos le ofreció a Tolosa Paz intercambiar roles para diseccionar más información sobre Santilli. “Yo que vos, la próxima hagamos al revés: vos me preguntás a mí, tenés que aprender mucho de mí, sino no te va a ir bien”, dijo la periodista.

En ese marco, la precandidata siguió con la humorada y le dijo a la conductora juntarse para hacer “un análisis pormenorizado” de Santilli.

Retomando el terreno político, Tolosa Paz criticó a los opositores. “Nuestros ciudadanos y ciudadanas no son tontos, por más que nos quieran vender una campaña de marketing: el pelo rojo del Colo... Discutamos ideas, cómo sale la Provincia adelante”, sostuvo.

> "No está con Alberto Fernández porque se llevaban mal en lo personal"

Finalmente, Pazos habló sobre la formación política de su ex pareja en la década del '90. “Yo tengo la misma edad, pero había sido formada en política previamente. No está con Alberto Fernández porque se llevaban mal en lo personal, y se pelearon en un cierre de listas. Por eso no terminó siendo kirchnerista, te lo puedo asegurar. Hay una camada de dirigentes políticos muy formados en la década del 90. Lo tienen ustedes también, Sergio Tomás podría haber estado del otro lado”, dijo la periodista.

En ese marco, Tolosa Paz llamó al precandidato impulsado por Horacio Rodríguez Larreta a debatir y a dejar de lado el marketing. “Lo invito a Santilli a que discutamos la Provincia, en lugar de que me tuitee el color de pelo, que lo veo todos los días. Dejemos de lado el globo, el color de pelo, pongámosle contenido”, concluyó.