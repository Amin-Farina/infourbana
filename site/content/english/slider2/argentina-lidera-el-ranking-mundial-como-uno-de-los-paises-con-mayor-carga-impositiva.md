+++
author = ""
date = 2021-06-30T03:00:00Z
description = ""
image = "/images/carga-impositiva-2-696x437.jpg"
image_webp = "/images/carga-impositiva-2-696x437-1.jpg"
title = "ARGENTINA LIDERA EL RANKING MUNDIAL DE LOS PAÍSES CON MAYOR CARGA IMPOSITIVA"

+++

**_La Argentina también está en categoría única o de “standalone” en materia de la alta carga tributaria sobre el sector formal de la economía, que termina contribuyendo en la generación de mayor inflación y menor inversión._**

Esto se agravará con el aumento del impuesto a las Ganancias para las sociedades al 35%, que da marcha atrás con la reducción que se había adoptado durante el gobierno de Mauricio Macri. Así lo explicaron dos expertos en impuestos, que enfatizaron que la Argentina ocupa el lugar más alto en el ranking de presión impositiva real, tal como lo indica el informe Doing Business del Banco Mundial.

Los perjuicios de esta altísima carga se reflejan en una menor inversión privada y precios más altos para los consumidores, indicaron los expertos Matías Olivero Vila (socio del departamento de impuestos del estudio Bruchou) y Fernando Guntern (gerente corporativo de impuestos de Arcor y presidente del departamento de política tributaria de COPAL), en el panel virtual del Bruchou Legal Week, dedicado a la carga fiscal formal que impacta a los contribuyentes.

Olivero Vila sostuvo que en el gobierno de Macri “no había controles cambiarios pero elevados impuestos y no hubo lluvia de inversiones., pero si bien en la gestión anterior se partía del diagnóstico de tener elevados impuestos, ¿hasta qué punto se actuó en consecuencia?”.

“Hay que hacer distinción entre presión y carga fiscales en el sector formal, es decir, el que paga impuestos. El primer concepto es de la recaudación sobre el PBI, mientras que lo segundo es cuántos impuestos pagan los que pagan impuestos”, indicó el abogado experto en impuestos.

“En el Doing Business 2020 del Banco Mundial, que estudia a 190 países, el sistema tributario argentino está ubicado en el puesto 170. Respecto del resto de América latina, lo que nos tira para abajo es la tasa total de imposición, porque Argentina tiene el 106% de imposición total sobre la utilidad, es el más alto sin contar en cuenta jurisdicciones de un millón de habitantes, como las Islas Comoras. Es decir, que Argentina es el país con la carga fiscal más elevada del mundo”, detalló.

“Este porcentaje es más del doble que el promedio regional y más alto que Brasil. Estamos en standalone también en materia fiscal”, sentenció el experto de Bruchou, quien advirtió que “esto genera falta de competitividad del sistema tributario argentino para las empresas, como de los mayores precios que pagan millones de argentinos, porque no es que las empresas pagan más, sino que los precios tienen que subir para absorber esta carga tan elevada”.

Por su parte, Guntern de Arcor y Copal indicó que el componente fiscal de los bienes de consumo, en alimentos y bebidas, es muy alto, tal como lo demostró un estudio que realizó el IARAF. “Hubo una tendencia a la baja entre 2016 y 2018, pero en 2019 con la crisis, el cambio de gestión y la pandemia, los cambios quedaron frezados o se revirtieron. Antes, se incrementó el pago a cuenta total del impuesto al cheque, cambios en el mínimo no imponible, unificación de alícuotas de las contribuciones a la seguridad social y se redujo en forma gradual el impuesto a las Ganancias para utilidades no distribuidas, pero todos esos cambios o se frenaron, o se revirtieron”, explicó. El caso más claro es el de la tasa de ganancias para las sociedades.

Otro paso atrás fue la virtual eliminación del Consenso Fiscal diseñado en 2018, que había beneficiado a las industrias hasta 2020, señaló. En este sentido, Olivero Vila dijo que el ejemplo de la alta carga impositiva “en alimentos y bebidas se puede extender a otros casos, como la ropa, donde el principal componente es el pago de impuesto; la alta carga impositiva no es un problema de las empresas, sino de los 45 millones de personas que viven en el país”.

“Hubo países con condiciones económicas peores que la Argentina que supieron salir a tomar aire y reducir la carga fiscal sobre el sector formal, como Sierra Leona, o el Congo, mientras que la Argentina sigue teniendo una imposición total superior al 100 de carga tributaria formal”, precisó el abogado. Sobre la afirmación de que cuando se bajan impuestos no vienen más inversiones, en referencia al gobierno de Macri, Olivero aclaró que “se hicieron intentos de bajar impuestos, pero, pero se quedaron a mitad de camino; Argentina pasó de terapia intensiva a intermedia y no supo competir”.

Cuando se le preguntó al gerente de impuestos de Copal qué impuestos se deberían eliminar, explicó que el 90% de la carga impositiva concentra sobre 11 tributos, aunque en los tres niveles del estado hay más de 100.

En cambio, consideró que la reducción del IVA a ciertos productos de la canasta básica no funciona, porque no se aplica en todos los eslabones de la cadena y por lo tanto no se refleja en una reducción de los precios al consumidor. Por esto, sugirió universalizar el mecanismo de devolución del IVA -aplicado actualmente a los sectores de menores recursos- a través de los mecanismos bancarios, de modo tal de contribuir también a una mayor formalización de la economía.

Además, recordó que “hay una elevada carga administrativa para tener que interpretar y cumplir, lo que lleva a deber tener un equipo muy preparado y un aumento del costo financiero”. A esto se suma el problema de tener “saldos a favor del fisco por muchos años sin poder ser utilizados, lo que se transforma en un perjuicio financiero, por eso estamos trabajando con las cámaras a nivel nacional y provincial para que se use la cláusula que quedó vigente del consenso fiscal para usar los saldos a favor en forma armónica en todo el país”.

Olivero Vila también mencionó el inconveniente de no poder aplicar el ajuste por inflación, debido a que “hubo normas de la gestión anterior y de esta que fueron en contra de la jurisprudencia de la Corte sobre la inconstitucionalidad” de la falta de ajuste. “Al final del día, todo esto significa un costo mayor en el precio del consumidor”, se lamentó.

Una y otra vez, Olivero Vila se refirió a la “excepcionalidad” argentina en materia de impuestos y en este sentido mencionó los derechos de exportación y el nuevo impuesto a la riqueza, o “Aporte Solidario”, que “solo se aplicó en la Argentina durante la pandemia, aunque se debatió en muchos países, y acá se aplicó con una alícuota que duplica la sugerida por Oxfam, la organización que propuso esta tasa a nivel internacional. Otra vez, standalone”. A esto se suma la iniciativa de aumentar nuevamente la alícuota del impuesto Bienes personales, cuya tasa es más reducida cuando “se la compara con otros países” con tributos al patrimonio.

Frente a este cuadro crítico, Guntern dijo que el sector privado no baja los brazos y dialoga con el Estado para que el impuesto al cheque vuelva a tomarse como pago a cuenta en forma considerable -y no solo a cuenta de Ganancias, dado el contexto recesivo- y para implementar diferentes regímenes de promoción de las inversiones. Se podría empezar con algo básico como que haya un solo domicilio fiscal electrónico en todo el país, ya que actualmente hay uno por provincia más el nacional, o sea, 25 por cada contribuyente.

En tanto, Olivero Vila señaló que “si bien el camino judicial es arduo, riesgoso y largo, al final hubo varios precedentes de la Corte que fueron favorables, como se mostró en ajuste de inflación o derechos de exportación”, en diferentes expedientes. Claro está que, en estos casos, la reparación para los contribuyentes es muy lenta y parcial, suficiente como para desalentar a la mayoría a seguir ese sendero.