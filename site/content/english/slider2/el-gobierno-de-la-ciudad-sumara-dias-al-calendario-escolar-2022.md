+++
author = ""
date = 2021-10-13T03:00:00Z
description = ""
image = "/images/tmb1_horacio-rodriguez-larreta-visita-establecimiento-escolar-foto-gobierno-ciudad-de-buenos-aires-893960-182859.jpg"
image_webp = "/images/tmb1_horacio-rodriguez-larreta-visita-establecimiento-escolar-foto-gobierno-ciudad-de-buenos-aires-893960-182859.jpg"
title = "EL GOBIERNO DE LA CIUDAD SUMARÁ DÍAS AL CALENDARIO ESCOLAR 2022"

+++

#### **_El gobierno porteño anunciará el miércoles por la mañana el calendario escolar para el próximo ciclo lectivo. Los alumnos porteños volverán a las aulas el 21 de febrero y tendrán 192 días de clase; doce más de lo que contempla la normativa obligatoria._**

Mañana el jefe de Gobierno porteño, Horacio Rodríguez Larreta, anunciará junto a su ministra de Educación, Soledad Acuña, el nuevo cronograma que tiene como objetivo recuperar parte del tiempo escolar que se perdió durante la pandemia. Además de la extensión del calendario, la presentación incluirá otros ítems como horas libres, jornada extendida, clases los sábados y escuelas de verano e invierno.

De acuerdo con la agenda educativa definida para el 2022, los docentes regresarán a las escuelas el lunes 7 de febrero. Todos los maestros deberán asistir a jornadas de formación que serán “masivas, presenciales y obligatorias”. Por primera vez se sumarán los profesores de escuelas privadas, por lo que confluirán un total de 24 mil educadores.

Por su parte, los chicos se reincorporarán el 21 de febrero y el ciclo lectivo terminará recién el 21 de diciembre, cerca de las fiestas. Si bien aún no está confirmadas las fechas, las vacaciones de invierno se suelen ubicar en las últimas dos semanas de julio.

Al margen del calendario, mañana las autoridades de la Ciudad anunciarán el programa “Aprovechar cada hora libre”. Según los cálculos oficiales, la sumatoria de horas libres en un año históricamente implica la pérdida de entre 3 y 5 días de clases en secundaria. “El objetivo de este programa es el uso eficiente del tiempo para transformarlo en una nueva instancia de aprendizaje”, explicaron las fuentes porteñas.

La propuesta apunta a “fortalecer” la figura del preceptor para que pueda acompañar la trayectoria de los estudiantes. Con esa intención, 2.600 preceptores serán capacitados para que puedan detectar posibles problemas de aprendizaje, para que trabajen en el aspecto socio-vincular de los alumnos, para que refuercen el clima escolar y el bienestar emocional de los chicos.

#### JORNADAS EXTENDIDAS

Los alumnos de sexto y séptimo grado de primaria de todas las escuelas de gestión estatal de jornada simple van a contar con una jornada extendida obligatoria. Eso significa que 21.500 alumnos van a sumar el equivalente a 90 días de clase al año, según los cálculos oficiales.

#### CLASES LOS SÁBADOS

Los Centros de Acompañamiento a las Trayectorias Escolares (CATE) se implementaron en 2020 para reforzar los contenidos de los alumnos de primaria y secundaria por medio de clases extra los sábados. Ahora, en 2022, la asistencia será obligatoria para todos los estudiantes de primer año de secundaria que se encuentren en promoción acompañada, es decir, para los que pasaron de año adeudando algunas materias.

#### ESCUELA EN INVIERNO Y VERANO

Se llevarán adelante otra vez la escuela de verano y de invierno. Estas instancias tienen el propósito de intensificar la enseñanza de los contenidos priorizados durante los recesos escolares para aquellos alumnos que lo necesiten. En 2021, 18 mil chicos asistieron a clases durante el receso invernal, informaron.