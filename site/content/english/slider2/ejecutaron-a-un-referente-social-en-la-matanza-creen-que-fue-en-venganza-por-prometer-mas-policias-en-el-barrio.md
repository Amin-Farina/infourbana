+++
author = ""
date = 2021-11-10T03:00:00Z
description = ""
image = "/images/34fthmbtbzf2vdsflmu5eitody.jpeg"
image_webp = "/images/34fthmbtbzf2vdsflmu5eitody.jpeg"
title = "EJECUTARON A UN REFERENTE SOCIAL EN LA MATANZA: CREEN QUE FUE EN VENGANZA POR PROMETER MÁS POLICÍAS EN EL BARRIO"

+++

##### **_Mientras familiares y amigos de Roberto Sabo (48) reclamaban Justicia el domingo pasado frente a la comisaría 2° de Ramos Mejía por el crimen del kiosquero, e incluso el ministro de Seguridad Bonaerense, Sergio Berni, se hacía presente en el lugar; a pocos kilómetros de allí, también en el partido de La Matanza, mataban a otro vecino. René Mendoza Parra, de 78 años y nacionalidad boliviana, era referente social del barrio Fátima, de González Catán._** 

Lo asesinaron a balazos dos hombres en un ataque relacionado con su labor comunitaria y su lucha contra la inseguridad y el avance narco en el lugar.

Según informaron fuentes judiciales a Infobae, el crimen de Parra ocurrió en el patio su casa, ubicada en el cruce de Buenos Aires y Tarija, cuando dos hombres de nacionalidad paraguaya, llegaron y tocaron el timbre. En ese momento, Parra salió y fue ahí donde lo balearon: le dispararon 14 veces. Luego, los sospechosos escaparon.

La investigación por el crimen de Parra quedó en manos del fiscal Federico Medone, titular de la UFI Temática Homicidios de La Matanza, el mismo funcionario judicial que llevó adelante la investigación que terminó con los asesinos de Sabo detenidos: Leandro Daniel Suárez (29) y una adolescente de 15 años.

De acuerdo a las primeras informaciones, pocas horas antes del crimen, Parra y a los vecinos del barrio habían tenido una reunión en la que los residentes de la zona le reclamaban la necesidad de más presencia policial. Puntualmente, en el cruce de las calles Calderón de la barca y Tarija, a pocos metros del río Matanza, una zona controlado por el narcotráfico, donde los crímenes por ajustes de cuentas se repiten.

Ante el reclamo de los vecinos, Parra prometió que iba a haber más policías en la zona. Por estas horas, la investigación parte desde esa reunión. Se sospecha que narcos del lugar fueron alertados sobre la promesa del referente social, y en venganza lo asesinaron.

Sin embargo, para los investigadores, un crimen en esa zona lamentablemente no es novedad. Lo describen como un territorio donde la vida vale poco y los homicidios relacionados con ajustes de cuentas, vinculadas a mezquindades entre tranzas, abundan.

De acuerdo a los investigadores, acostumbrados a inmiscuirse en esa zona de la vera del río Matanza que atraviesa tres localidades (Laferrere, Virrey del Pino y González Catán), hasta enero pasado había al menos un homicidio por mes o más, motivados por ajustes de cuentas de las guerras entre pequeñas facciones narcos. Dicen que no hay un capo en el territorio, un líder, solo cowboys del paco bonaerense, principalmente de nacionalidad paraguaya, disparándose entre sí.

Sin ir más lejos, el viernes pasado, un grupo de agentes de la Policía Bonaerense se encontraba abocado a la búsqueda de un menor de 12 años que se encuentra desaparecido. En medio del rastrillaje, los agentes recibieron el dato de que Chávez podía estar a la vera del río Matanza, a la altura de la localidad de Gregorio Laferrere, en una zona rodeada de maleza y se dirigieron hacia el lugar. Una vez allí, se introdujeron entre los matorrales por un sendero para encontrar al chico. En ese momento, los efectivos escucharon: “La Policía, la Policía, viene la Policía”, y cuatro hombres comenzaron a dispararles mientras corrían.

De inmediato, se inició un feroz tiroteo que acabó cuando tres de los hombres escaparon y uno de ellos cayó al suelo herido bala. En ese instante, los policías se acercaron, le quitaron el arma y lo identificaron: Mario Fabián Estigarribia, de nacionalidad paraguaya. Estigarribia tuvo que ser trasladado a un hospital de la zona, pero falleció antes de ingresar al centro médico.

El 13 de enero pasado, Federico Julio López y su compañero, ambos policías de la Superintendencia de Drogas Peligrosas de la PFA, se entreveraron encubiertos en un caminito a la vera del río Matanza, en el borde del Barrio Fátima de González Catán, cerca de donde mataron a Estigarribia y a Parra. La UFI temática de drogas de La Matanza, a cargo de la fiscal Julia Panzoni, les había enviado un oficio a comienzos de ese mes para entrar en el territorio e investigarlo. López y su compañero llegaron a bordo de un Chevrolet Corsa gris al asentamiento de casas bajas, rodeado de vegetación. A los pocos segundos, salieron del lugar heridos de bala.

López recibió un impacto de lleno en el cráneo, cuando un grupo de hombres con armas en las manos les gritó, como si ellos mismos fueran los policías, y tiraron. Fue trasladado de urgencia al hospital Churruca poco después. El agente continúa internado aunque fuera de peligro, según confirmaron fuentes judiciales. Su compañero terminó ileso. La investigación está a cargo de la fiscal Andrea Palín, titular de la UFI Nº9 de La Matanza, quien había arrestado a uno de los sospechosos por el ataque y lo acusó por homicidio en grado de tentativa.

Meses después del ataque al policía López, la Superintendencia de Drogas Peligrosas de la PFA realizó 23 allanamientos en la zona y detuvieron al hombre señalado como el tirador apodado “Bodoque” junto a otros 10 sospechosos. Los detectives llegaron a él a partir de una fotografía que habían logrado captar. Así hallaron a alias Bodoque, de nacionalidad paraguaya, en el barrio y lo arrestaron. Según indicaron, el sospechoso sería uno de los hombres “pesados” dentro de la zona en el negocio del narcomenudeo.

En los operativos, ordenados por la fiscal Panzoni, que contaron con helicópteros volando la zona y agentes a bordo de lanchas, secuestraron cerca de 40 panes de marihuana, varios kilos de cocaína fraccionada lista para la venta, balanzas, armas y granadas. Por otro lado, según indicaron fuentes del caso, se llegó hasta el lugar con topadoras para derribar los bunkers asentados en la zona, donde se concretaba la venta de drogas.

Sin embargo, el foco de los detectives sobre esa zona comenzó algunos años antes. En diciembre de 2019, la Policía Bonaerense detuvo en Laferrere a diez personas que integraban la organización, que en ese momento las crónicas policiales llamaron: “La banda del Carancho paraguayo” o del “Zar de la costa”, por el apodo del presunto líder que habían arrestado.

Se trataba de José Ruiz Díaz Cabaña, alias “El Carancho”, un narco de poca monta que organizaba la venta en ese territorio para Hernán Darío Escurra alias “El Nono”, un barra del club Laferrere también detenido en noviembre pasado.

“El Carancho” además cargaba con causas de homicidio. Según una investigación judicial a cargo de la fiscal Panzoni, la gente que trabajaba para él se metía dentro del monte, armaba carpas y vendía drogas. Su vía de escape era particular, habían instalado una tirolesa para cruzar el río hacia el margen del partido de Ezeiza. Así, comenzaron las disputas territoriales a sangre y fuego.

Una de las víctimas fue Alexis Maximiliano Zacarías, de 29 años, ejecutado de un tiro en la cabeza: “El Carancho” Cabaña fue imputado por el crimen. Los casos en esa zona tienen grandes similitudes. El 24 de julio de 2019, por ejemplo, la Policía Bonaerense tras un llamado al 911 en González Catán halló un cuerpo envuelto en un colchón a la vera del río; fue identificado como Emilio Guchea, de 19 años.

Algo parecido sucedió pocos días después cuando los agentes encontraron el cuerpo de otro joven, Nahuel Ramírez. También enrollado en sábanas y a la vera de un arroyo en Laferrere. Otro caso idéntico fue el homicidio de Luis Rojas, de 25 años, registrado el 25 de octubre de 2019.

“Tu marido está en el fondo del 38 tirado, lo mataron”. Así le comunicaron a la esposa de Rojas que habían asesinado a su pareja. El cuerpo estaba envuelto en frazadas y con un tiro en la cabeza. Aún no hay detenidos por el caso. De esta manera, los casos en la zona se acumulan.