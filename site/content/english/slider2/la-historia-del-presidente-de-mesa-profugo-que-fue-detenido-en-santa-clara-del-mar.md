+++
author = ""
date = 2021-09-13T03:00:00Z
description = ""
image = "/images/qzsc27juo5dz5elgag6xewc25a.jpg"
image_webp = "/images/qzsc27juo5dz5elgag6xewc25a.jpg"
title = "LA HISTORIA DEL PRESIDENTE DE MESA PRÓFUGO QUE FUE DETENIDO EN SANTA CLARA DEL MAR"

+++

#### **_José F. A., de 37 años, fue arrestado mientras ejercía como autoridad de mesa durante las PASO. Una fiscalía de Salta lo buscaba por haber supuestamente abusado de su ex pareja._**

Este domingo por la mañana, apenas abiertos los comicios, efectivos de la Policía Bonaerense ingresaron a la Escuela 503 de Santa Clara del Mar, en el partido de Mar Chiquita y escoltaron hacia afuera a uno de los hombres que ejercía como presidente de mesa: ahí mismo lo esposaron y lo detuvieron. José F. A., de 37 años, no se resistió. Sobre su cabeza pesaba una orden de detención y pedido de captura nacional dispuesto por el Juzgado de Garantías N° 2 de Salta desde hace poco más de 20 días.

Aún así, en circunstancias que aún se investigan, el hombre -con último domicilio registrado en la localidad bonaerense de Villa Celina- no sólo se presentó a votar a la escuela, sino también a presidir la mesa 0054.

Si bien desde un primer momento se especuló con que podría haber sido una estrategia exitosa para atraparlo, una fuente judicial con acceso a la causa confirmó a Infobae que no fue así: la captura del hombre fue una simple casualidad.

José F. comenzó a ser investigado en las últimas semanas. Su ex pareja quien lo llevó a la Justicia de Salta hace poco más de un mes: la mujer denunció que había sido víctima de violencia de género durante el tiempo que mantuvieron una relación y lo acusó de haber abusado sexualmente de ella.

Por las características de las denuncia, la fiscal Cecilia Flores Toranzos, titular de la Fiscalía Penal N°3 de la Unidad de Delitos contra la Integridad Sexual de Salta, que quedó a cargo de la investigación, solicitó al juzgado la detención del hombre, que fue otorgada recién el 20 de agosto pasado, por temor a que entorpezca la causa o interfiera en las declaraciones de testigos.

Para los investigadores todavía es una incógnita cómo José F. terminó en la Costa Atlántica bonaerense. El hombre -con trabajos recientes en cadenas de venta de electrodomésticos y supermercados- vivió de forma intermitente entre las ciudades salteñas de Cerrillos y Tartagal, los monoblocks de Villa Madero y el partido de La Matanza.

Este domingo, sin embargo, el personal del Equipo de Prevención Ciudadana de Mar Chiquita lo capturó a varios kilómetros de todos esos puntos. Así, un hombre de 59 años que estaba en la escuela debió tomar su lugar como autoridad en la mesa.

En los próximos días, una vez terminados los trámites correspondientes, el imputado será trasladado de nuevo a Salta, donde deberá prestar declaración ante la fiscal Flores Toranzos.

No fue el único detenido en la jornada de votación. En el partido bonaerense de Villarino, un hombre de 47 años con pedido de paradero por estafas ordenado por la justicia federal de Río Negro fue detenido el domingo por la tarde en un establecimiento educativo de la localidad de Pedro Luro.

Se trata de Guido Yevara, que fue apresado por la Policía alrededor de las 15.15 horas cuando intentó emitir su voto en la Escuela N°33 “Nicolás Marino”, ubicada en la calle 12 y 7 de esa localidad del sur de la provincia de Buenos Aires.

Inmediatamente se comprobó que el hombre tenía un pedido de paradero activo en una causa en la que interviene el Juzgado Federal N° 1 con asiento en Viedma, capital de Río Negro, por lo que fue aprehendido y trasladado a la seccional local.