+++
author = ""
date = 2021-05-12T03:00:00Z
description = ""
image = "/images/hagay_sobol.jpg"
image_webp = "/images/hagay_sobol.jpg"
title = "ALBERTO FERNÁNDEZ: \"LA POSTURA DE ARGENTINA SOBRE EL CONFLICTO ENTRE ISRAEL Y PALESTINA RESPETA LA POSICIÓN DE LA ONU\""

+++

**_“La posición de Argentina sobre el conflicto entre Israel y Palestina respeta la posición de Naciones Unidas”, dijo Alberto Fernández a Infobae antes de partir a un encuentro cerrado con representantes de empresa francesas que invierten en el país. “Nosotros leímos el comunicado de la ONU, y sobre su posición internacional escribimos la nuestra”, agregó el jefe de Estado._**

Y completó Alberto Fernández: “Nuestra solidaridad a los familiares que sufren el conflicto en Medio Oriente”.

Al lado del Presidente estaba el ministro de Relaciones Exteriores, Felipe Solá, quien explicó el proceso formal seguido por la Cancillería para emitir un comunicado oficial que fue cuestionado por la representación diplomática de Israel en la Argentina, la comunidad judía en el país y la mayoría de los dirigentes de la oposición.

“La primera información del conflicto me la dio el secretario de Culto (Guillermo Oliveri) y sobre eso empezamos a trabajar”, aseguró Solá.

\-¿Y después qué hicieron?-, preguntó este enviado especial.

\-Llamé a nuestra embajadora en Naciones Unidas (María del Carmen Squeff). Y me envió un texto con la posición oficial de las Naciones Unidas. Te lo voy a pasar...-, contestó el canciller.

Un segundo más tarde, Solá tomó su celular, buscó el WhatsApp de la embajadora en la ONU, y puso reenviar al mensaje recibido -ayer- desde New York.

Ese mensaje de WhatsApp, tiene dos partes: un texto de la embajadora Squeff, y la copia exacta de un cable redactado por la agencia española EFE.

“Guterres pide un cese inmediato de la escalada entre israelíes y palestinos”, escribió Squeff al canciller argentino.

Y a continuación, la embajadora pegó el cable de EFE, que dice:

Naciones Unidas, 11 may (EFE).- El secretario general de la ONU, António Guterres, exigió este martes un “cese inmediato” de la escalada violenta entre israelíes y palestinos, que ya ha dejado víctimas mortales en ambos bandos. Según su portavoz, Guterres está “profundamente triste por conocer el creciente número de víctimas, incluidos niños, por los ataques aéreos israelíes en Gaza y las muertes israelíes por cohetes” lanzados desde la franja. ”Las fuerzas de seguridad israelíes tienen que ejercer máxima contención y calibrar su uso de la fuerza”, señaló el portavoz, Stéphane Dujarric, que añadió que el “lanzamiento indiscriminado de cohetes y morteros hacia centros de población israelíes es inaceptable”, en referencia a los ataques llevados a cabo desde Gaza.

En otros párrafos del cable, la agencia EFE añade:

“Dujarric dijo que la ONU está trabajando con todas las partes relevantes para reducir la tensión de forma urgente. Este martes (por ayer), la Oficina de Derechos Humanos de la ONU consideró que Israel ha utilizado la fuerza de forma innecesaria y desproporcionada contra manifestantes palestinos, quienes protestan desde hace algunas semanas contra el desahucio de familias palestinas de un barrio de Jerusalén Este ocupada.”

Y completó EFE desde Naciones Unidas, citando a la Oficina de Derechos Humanos de la ONU:

“Las Fuerzas de Seguridad Israelí (Sic) deben permitir y garantizar el ejercicio de las libertades de expresión, asociación y reunión. Ninguna fuerza debe ser utilizada contra aquellos que ejercen estos derechos pacíficamente”, sostuvo el portavoz de la Oficina, Rupert Colville. ”Cuando el uso de la fuerza es necesario debe hacerse respetando plenamente las normas internacionales de los derechos humanos, que prohíben el uso innecesario y desproporcionado de la fuerza. Esto no ha sido el caso en los últimos días”, agregó.

Cuando este enviado especial terminó de leer el cable de EFE, Solá añadió: “Nos mantenemos alineados con el derecho internacional, y si la postura de la ONU cambia, nosotros también cambiaremos para seguir sus pasos en el conflicto”.

\-¿Habló con la representación diplomática de Israel en la Argentina?

\-No. Sólo leí su comunicado en Twitter.

El comunicado argentino basado en la posición de las Naciones Unidas, y que provocó el cuestionamiento de la embajada israelí en Buenos Aires, decía textualmente:

“La República Argentina expresa su honda preocupación por el dramático agravamiento de la situación en Israel y Palestina, el uso desproporcionado de la fuerza por parte de unidades de seguridad israelíes ante protestas por posibles desalojos de familias palestinas de sus hogares en los barrios de Sheikh Jarrah y Silwan, así como por la respuesta a través del lanzamiento de misiles y artefactos incendiarios desde la Franja de Gaza”.

Israel considera que la respuesta no fue desproporcionada y que Hamas es una organización terrorista que opera sin límites desde la Franja de Gaza. En Tel Aviv estaban sorprendidos por la posición de la cancillería argentina, que planteó una simetría entre una organización terrorista que siembra la muerte con un estado democrático que ejerce su derecho internacional a proteger a sus ciudadanos y su territorio.

“Están atacando a nuestro país, hay muchos heridos, la situación es preocupante, es grave. Nosotros hicimos todo lo que pudimos para no llegar a esto. Este comunicado lo vemos con preocupación y no expresa la buena relación que existe entre nuestros países”, sostuvo la embajadora de Israel en la Argentina, Galit Ronen, ante una consulta del periodista Luis Novaresio.

“Las relaciones se mantendrán como hasta ahora. No hay que alarmarse. Nosotros seguimos la perspectiva internacional de la ONU”, reiteró Solá antes de acompañar a Alberto Fernández en su primera actividad oficial en París.

La próxima para de la comitiva argentina será Italia. Hay expectativa por un posible encuentro entre el Presidente y el papa Francisco. También por una reunión con la directora del FMI. Por lo pronto, Fernández ya decidió quedarse 24 horas más de lo que había previsto inicialmente y despegará rumbo a Buenos Aires el sábado al mediodía.