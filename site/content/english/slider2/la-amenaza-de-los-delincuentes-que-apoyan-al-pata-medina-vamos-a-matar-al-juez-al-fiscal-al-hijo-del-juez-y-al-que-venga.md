+++
author = ""
date = 2021-08-26T03:00:00Z
description = ""
image = "/images/las-amenazas-las-hace-un-grupo-que-dice-apoyar-al-pata-medina-1.jpg"
image_webp = "/images/las-amenazas-las-hace-un-grupo-que-dice-apoyar-al-pata-medina.jpg"
title = "LA AMENAZA DE LOS DELINCUENTES QUE APOYAN AL PATA MEDINA: \"VAMOS A MATAR AL JUEZ, AL FISCAL, AL HIJO DEL JUEZ Y AL QUE VENGA\" "

+++

**_Circuló un video donde se muestran amenazantes, enfundando ametralladoras y utilizando máscaras para ocultar su identidad, mientras la patota expresaba su respaldo explícito hacia el dirigente sindical y a su hijo, Cristian “Puly” Medina._**

“Lo vamos a matar al juez al fiscal, al hijo del juez y al que venga”. Un grupo de hombres que dicen apoyar al sindicalista de la UOCRA, Juan Pablo “Pata” Medina, difundió ese mensaje intimidatoria con un video casero. 

El objetivo del grupo es claro: amenazar con represalias al juez y el fiscal de la causa que están a cargo de definir la libertad del gremialista por incumplir una prisión domiciliaria.

“Nosotros queremos arrancar diciendo que si mañana de nuevo van a tomar medidas contra nuestro líder... Acá estamos, pero de verdad. Nosotros estamos con vos Pata, “Puly”. ¡Cien por ciento!”, dice el cabecilla del grupo en el video que se viralizó en las redes sociales.

De inmediato, y en medio de una puesta en escena de la patota cargando sus ametralladoras, el portavoz advierte: 

> “Lo vamos a matar al juez al fiscal, al hijo del juez y al que venga. Acá está la banda del “Puly” y del “Pata”. ¡Ortiba! ¡La concha bien puta de su madre! ¡Manga de giles!”. “¿Quieren guerra? ¡Guerra van a tener!”, replica otra de las voces del grupo.

El video comenzó a circular ante la posibilidad de que el “Pata” Medina pueda volver a prisión. El dirigente sindical, que hasta 2017 encabezó la seccional de la Uocra de La Plata, quedó en una situación judicial complicada luego de encabezar una marcha este lunes en la capital de la provincia de Buenos Aires junto a su hijo “Puly” Medina, también imputado en la causa por asociación ilícita, lavado de dinero y extorsión que involucra al gremialista y que tramita en el tribunal Oral Federal 2 de La Plata que aún no le fijó fecha.

Este jueves, “Pata” Medina fue citado a una audiencia oral por videoconferencia para explicar por qué lideró una manifestación en La Plata, a pesar de las restricciones que le dictó la Justicia.

El cabecilla del sindicato constructor fue excarcelado en febrero pasado porque llevaba preso tres años, pero está a la espera del juicio. En el fallo que le concedió la libertad, el juez del Tribunal 2, Alejandro Esmoris, le fijó una serie de condiciones, como la prohibición de salida del país, acercarse a las víctimas de los hechos que se le imputan y también la de participar de actividades gremiales en la UOCRA u otras seccionales.

Luego de la audiencia de este jueves ante la Justicia, el fiscal Marcelo Molina elevará su opinión al juez, quien deberá resolver si revoca la excarcelación del “Pata” Medina y su hijo y vuelve a ordenar su detención, o si los argumentos del sindicalista son satisfactorios y puede esperar el juicio en libertad.

Tanto Medina como David Emiliano García (su cuñado) estaban detenidos desde el 26 de septiembre de 2017 por orden del juez federal de Quilmes Luis Armella, que lo procesó por los delitos de “asociación ilícita”, “lavado de dinero” y “extorsión” a empresarios de la construcción. El icónico dirigente platense fue apresado entonces en su casa de Punta Lara luego de mantenerse atrincherado durante varias horas en la sede de la seccional, donde se blindó con una tensa concentración de trabajadores de la construcción.

En el mes de mayo, “Pata” Medina reapareció en la vida política en un acto del Frente de Todos en Ensenada. En ese marco, el ex titular de la UOCRA dijo públicamente que fue en apoyo del gobierno nacional, pero en realidad buscó hacer visible un pedido al presidente Alberto Fernández y la vicepresidenta Cristina Kirchner. El petitorio por escrito daba cuenta de la “gravísima persecución que he padecido como dirigente gremial y líder sindical”, donde alegó que las causas judiciales en su contra “se encuentran en su mayoría cerradas por inexistencia de delito”.

El dirigente gremial llegó hasta las vallas de ese mitín político, pero no pudo pasar. Fieles a su impronta amenazante, sus seguidores forcejearon con la Policía bonaerense. “Acá estamos en Ensenada, dejen entrar al Pata”, gritó su base de apoyo.

En ese momento, Medina estaba en condiciones de participar de esa actividad política. El impedimento alcanzaba a actividades o reuniones de la Uocra. El expediente de fondo por la causa de “lavado de activos y asociación ilícita” ya fue elevado a juicio oral, pero todavía no tiene fecha para comenzar.