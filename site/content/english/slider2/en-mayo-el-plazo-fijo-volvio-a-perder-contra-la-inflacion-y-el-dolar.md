+++
author = ""
date = 2021-06-02T03:00:00Z
description = ""
image = "/images/pesos-1024x584-1.png"
image_webp = "/images/pesos-1024x584.png"
title = "EN MAYO, EL PLAZO FIJO VOLVIÓ A PERDER CONTRA LA INFLACIÓN Y EL DÓLAR"

+++

**_En un contexto de elevada inflación y aumento del dólar libre, el plazo fijo es un instrumento cada vez menos atractivo para los ahorristas._** 

Hoy los bancos pagan una tasa prácticamente uniforme del 37% anual en pesos que no alcanza para ganarle o empatarle a la inflación pero ahora tampoco para seguirle el tren al dólar en el mercado libre. Por lo tanto, los ahorristas que se vuelcan por esta opción están perdiendo mes a mes poder adquisitivo y cada vez tienen menos patrimonio medido en moneda dura.

El Banco Central dispuso una tasa mínima de 37% para el sistema financiero, pero casi ninguna entidad se sale de la regla. Los bancos sufren una fuerte reducción de su rentabilidad ante la disminución del crédito al sector privado, al tiempo que las tasas que pagan las colocaciones en Leliq tampoco son significativamente mejores (38% anual). Los balances bancarios del primer trimestre mostraron las ganancias más bajas de los últimos diez años.

Tampoco hay un esfuerzo de las entidades por publicitar los plazos fijos ajustados por UVA, que al menos permiten mantener poder adquisitivo ya que deben ajustarse por inflación. Esta opción viene presentando un incremento firme, aunque desde niveles muy bajos. El stock apenas llega a $ 169.000 millones, equivalente al 4,3% de las colocaciones tradicionales a plazo. Si bien los plazos fijos UVA deben realizarse a 90 días como mínimo, también pueden precancelarse a partir de los 30 días, aunque en ese caso ya se paga la tasa de una colocación tradicional.

La tasa de plazo fijo tradicional quedó lejos de la inflación de los últimos meses e incluso del 3,8% que se espera para mayo, siendo la segunda caída consecutiva que registrará el índice oficial. El rendimiento de 37% anual pierde además por goleada contra el nivel anualizado del 60% que muestra el dato de inflación de los últimos seis meses. Y también queda muy por debajo de la estimación del 50% que se espera para los próximos doce meses, según la última encuesta de expectativas de inflación de la Universidad Di Tella.

El Gobierno, sin embargo, descansaba en la estabilidad del dólar para sostener el volumen de plazo fijo en pesos. Pero esto cambió en los últimos dos meses. En abril, el dólar libre subió casi 7%, al igual que el “contado con liquidación”, y en marzo la suba superó el 6%. Esto significa que el rendimiento de quedarse en pesos se ubicó bien por debajo de lo que hubiera dejado una inversión en dólares en el mismo período.

Los últimos números del Banco Central empiezan a reflejar este “cansancio” de los ahorristas. Mientras que en los meses anteriores el aumento del volumen de plazo fijo en pesos se ubicaba en torno a los $ 120.000 millones mensuales, en mayo todo indica que el stock permanecerá estancado o con una suba inferior a los $ 20.000 millones.

El escaso volumen o casi nulo volumen de aumento de los plazos fijos tradicionales, no obstante, también está afectado por la decisión del Gobierno de obligar a los fondos comunes a reducir su tenencia en fondos comunes de dinero (cortísimo plazo) para inducirlos a comprar bonos del Tesoro. Según calculó la economista Paula Gándara, de AdCap Securities, durante el último mes se liquidaron unos $ 36.000 millones, con el consiguiente impacto negativo en el volumen de plazos fijos.

Pero el desarme de posiciones inducido para los fondos comunes es sólo una parte de la explicación. Claramente, en la medida que la tasa para el ahorrista se vuelva cada vez más negativa, menor será el interés por permanecer con una colocación en pesos. Esto impactará tarde o temprano sobre el dólar y por ende en los niveles de inflación.

Ahora el BCRA autorizó a los bancos a vender parte de sus encajes de Leliq para pasarse a letras emitidas por el Tesoro. La diferencia es que tendrán tres puntos más de tasa (41% a 38%), pero además dejarán de pagar Ingresos Brutos de la Ciudad de Buenos Aires por los intereses. Esto representa un rendimiento adicional de otros tres puntos.

La duda es si esta mejora de la rentabilidad que tendrán los bancos se traducirá también en tasas más altas para los ahorristas. Esto podría ocurrir si se confirma la tendencia a una desaceleración mayor o incluso estancamiento en el nivel de plazos fijos en pesos en un contexto de elevada inflación.

Para el titular del BCRA, Miguel Pesce, mantener tasas reales tan negativas (es decir por debajo de la inflación mensual y proyectada) es jugar con fuego. El peligro latente es que parte de los sueldos que se pagarán en los próximos días y sobre todo el medio aguinaldo aumente la presión dolarizadora, al reducirse las opciones atractivas para permanecer en moneda local.