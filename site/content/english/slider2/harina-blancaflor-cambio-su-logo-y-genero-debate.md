+++
author = ""
date = 2021-05-25T03:00:00Z
description = ""
image = "/images/harina-blancaflor-logo-g20210525-1177706-1.jpg"
image_webp = "/images/harina-blancaflor-logo-g20210525-1177706.jpg"
title = "HARINA BLANCAFLOR CAMBIÓ SU LOGO Y GENERÓ DEBATE"

+++

**_La ilustración de una cocinera negra en su paquete de harina leudante fue eliminada ante posibles críticas por racismo; miles de usuarios se sumaron a la discusión en Twitter._**

La cocinera negra que acompañó a la marca de harinas Blancaflor desde sus inicios fue eliminada en un rebranding reciente de la marca. La acción generó comentarios contrapuestos en Twitter, entre aquellos que celebraron la iniciativa, que evoca un pasado racista, y los que lamentaron la pérdida de un símbolo de su infancia y juventud.

“Blancaflor es la marca pionera que creó la famosa harina leudante aportándole practicidad al amasado casero. Con una trayectoria de más de 60 años, sigue siendo la aliada indispensable de quienes disfrutan de amasar con un producto único”, describe Molinos, el holding alimenticio del grupo Pérez Companc.

En una decisión de marketing, Blancaflor modificó el diseño de su paquete celeste de harina leudante y modernizó su histórico packaging. Quitó a la cocinera negra, que se encontraba sobre un fondo rojo, y ubicó a su tradicional logotipo dentro de un palo de amasar. También, ilustró con una foto, de manos batiendo, y dejó de lado la ilustración original.

Bajo un nuevo paradigma, la representación de la mujer negra, asociada a un pasado colonia, con camisa, sombrero de cocinero, aros blancos y delantal rojo, podía llegar a ser un problema para la marca. “La nueva normalidad deconstruida de Blancaflor quitando a la africana esclava que amasaba”, escribió el usuario de Twitter @amundsenroald11, quien posteó la foto original comparando los dos paquetes, el viejo y el nuevo y luego provocó una ola de repercusiones.

El posteo original tuvo al menos 21.000 likes y más de 1200 retuits. Sin embargo, hubo algunos usuarios que lamentaron la modificación y despidieron con nostalgia a la histórica cocinera. “Buscan la destrucción de las tradiciones para que a las nuevas generaciones les parezca normal las barbaridades que proponen. Blancaflor me recuerda a mi madre y por eso la compro, todas las tardes amasaba cosas ricas para la merienda de sus 6 hijos. Jamás vi una esclava negra”, expresó otra usuaria.

Algunos de los comentarios más destacados fueron: “Hasta la negrita de Blancaflor se quedó sin laburo con Alberto”; “Había casos afuera y tenía que llegar acá en algún momento. Blancaflor cambia el logo para no ser criticada por racismo. Lindo debate”; “Si Blancaflor no pone de nuevo a la negrita, no la compro más”.

Otros destacaron cómo la campaña terminó ubicando a la marca entre las tendencias del país en Twitter y generando una gran repercusión, tras la decisión. “Estamos discutiendo sobre la imagen de Blancaflor, el publicista logró su cometido”, reflexionó otro usuario.