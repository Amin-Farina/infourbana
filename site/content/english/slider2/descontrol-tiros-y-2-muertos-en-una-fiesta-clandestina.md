+++
author = ""
date = 2021-09-01T03:00:00Z
description = ""
image = "/images/fiesta_1-jpg_543804098.jpg"
image_webp = "/images/fiesta_1-jpg_543804098.jpg"
title = "DESCONTROL, TIROS Y 2 MUERTOS EN UNA FIESTA CLANDESTINA"

+++

#### **_Ocurrió en Esteban Echeverría. Por el violento episodio hay un oficial de la Policía de la Ciudad detenido, quien se negó a declarar y seguirá preso._**

Una fiesta que había sido convocada por las redes sociales en una casa de Esteban Echeverría terminó a los tiros en plena madrugada y el desenlace fue trágico: dos personas murieron y otras tres resultaron heridas.

Un llamado al 911 llevó a los efectivos de la comisaría 3ra. de esa localidad bonaerense al domicilio de la calle Vía Monti al 1800 donde se llevó a cabo la reunión. Allí, en la vereda de enfrente, se toparon con un joven inconsciente tirado en el piso junto a una moto Honda Tornado roja y blanca y sin patente. Tenía un balazo en el abdomen.

A los pocos metros, en el patio delantero de una vivienda lindera los efectivos encontraron a otro herido de bala, un joven oficial de la Policía de la Ciudad de 21 años identificado como Camilo Farías que portaba su pistola reglamentaria.

En su declaración, este efectivo manifestó que había sufrido un intento de robo y que, al intentar evitar que se llevaran su moto, se desató el tiroteo en el que él mismo resultó herido, así como también otros dos sospechosos. Sin embargo, la versión del policía perdió fuerza después de que varios testigos aseguraran que no existió ningún asalto sino que en realidad se trató de una pelea por otros motivos.

Según indicó una fuente cercana a la causa a Télam, Elian Jeremías Bahamonde, el primer joven herido con el que se cruzó el patrullero, había asistido a la fiesta acompañado por su amigo policía, Farías, y otras cuatro personas.

En medio de la noche y tras un consumo excesivo de alcohol, Bahamonde orinó en la cama y sobre el televisor del dueño de la casa y tanto él como el oficial fueron echados del lugar. Los jóvenes se fueron, pero solo fue una pausa en el conflicto ya que ambos volvieron poco después en una Honda roja dispuestos a tomar revancha a los tiros.

Yahir Ayala, de 21 años, fue la primera víctima fatal de la noche. Lo trasladaron al Ballestrini, donde los médicos constataron su muerte producto de un disparo en el pecho. En tanto, otros dos jóvenes que participaban de la reunión también resultaron heridos y permanecen en el Santamarina.

A su vez, el policía y su amigo también fueron baleados en el enfrentamiento por lo que ahora la Justicia investiga quiénes dispararon contra ellos desde el interior de la casa, debido a que en la escena solo se secuestró la pistola reglamentaria del efectivo.

Behamonde, el sujeto que desató la pelea y recibió un balazo en el abdomen, murió horas después en el hospital por lo que el fiscal Fernando Semisa, de la Unidad Funcional de Instrucción (UFI) 4 descentralizada de Esteban Echeverría, ordenó la autopsia.

Por su parte, Farías, quien presta servicios en la Comisaría Vecinal 4A de la Policía de la Ciudad, fue intervenido quirúrgicamente y quedó internado fuera de peligro. Pese a esto, estuvo en condiciones de ser indagado pero se negó a declarar y seguirá preso.

El instructor judicial le imputó a al oficial el delito de “homicidio cometido con arma de fuego en concurso real con homicidio simple cometido con arma de fuego en tentativa reiterada en dos hechos”, y en las próximas horas pedirá formalmente su detención al Juzgado de Garantías interviniente.