+++
author = ""
date = 2022-02-10T03:00:00Z
description = ""
image = "/images/jf-17-thunder-paf.jpg"
image_webp = "/images/jf-17-thunder-paf.jpg"
title = "ARGENTINA ENVIARÁ UNA DELEGACIÓN MILITAR A CHINA PARA AVANZAR EN LA COMPRA DE AVIONES DE COMBATE"

+++

##### **_Una delegación militar argentina viajará a China en el mes de marzo como parte de un entendimiento de cooperación bilateral en materia de defensa que, entre otros temas, contempla la posible compra de doce aviones de combate chinos. Como había adelantado Infobae, el proyecto del Presupuesto 2022 incluyó una partida de más de USD 600 millones para la adquisición de 12 aeronaves supersónicas JF-17 Thunder-Bloque III._**

Según pudo saber este medio, Argentina analiza cinco ofertas en total para la compra de aviones, entre ellas, la de China, motivo por el cual ya viajaron dos veces oficiales de la Fuerza Armada y programa una más para el próximo mes.

Además, el embajador argentino en Beijing, Sabino Vaca Narvaja, mantuvo encuentros con la Corporación Nacional de Importación y Exportación de Aero-Tecnología de Chin (CATIC), reunión que fue calificada como parte de la “estrategia integral” de cooperación en materia de defensa y considerada un “hito”.

Cabe recordar que el proyecto del Presupuesto 2022 incluyó una partida de más de USD 600 millones para la compra de 12 aviones supersónicos JF-17 Thunder-Bloque III, de origen chino. El Ministerio de Economía contempló además la erogación de USD 20 millones extra para infraestructura complementaria, entre ellos los simuladores de vuelo.

Por otro lado, el Gobierno prevé incorporar en 2022 vehículos 8x8 para el Ejército, un buque polar para las campañas antárticas y helicópteros de rescate. Sin embargo, en ninguna de las estimaciones precisó detalles de origen, marca y modelo del equipamiento que se incorporará. Es lo que corresponde cuando se imputa una partida presupuestaria para material que aún no fue producto de una licitación o compulsa internacional. Por eso, resultó llamativo que en el caso de los aviones apareciera un empresa china como vendedora cuando aún no se realizó el procedimiento legal para avanzar con la compra pública y escoger al mejor oferente.

El próximo viaje se realizará luego de la reunión entre Alberto Fernández y el mandatario chino Xi Jinping, durante su gira a principios de mes de febrero. China está interesada en proporcionar a Argentina el moderno JF-17, ya que podría abrir la puerta a futuros acuerdos de armas con otros países de la región. Argentina había acordado previamente con China comprar una gama de sistemas militares en 2015. El contrato, por un valor estimado de mil millones de dólares, incluía buques de guerra, vehículos blindados y aviones de combate. El entonces Ministro de Defensa argentino, Agustín Rossi, confirmó ese mismo año que el JF-17 estaría en la lista de la compra.

Estos acuerdos se cerraron durante las administraciones de Cristina Kirchner (2008-2015), solo para ser cancelados por el gobierno conservador de Mauricio Macri después de que llegó al poder en diciembre de 2015. Desde entonces, los acuerdos de armas fueron revividos por el actual gobierno.

Vaca Narvaja también se reunió con Jiao Kaihe, presidente de China North Industries Group Corporation Limited (NORINCO), empresa estatal china con la que Buenos Aires trabaja en un proyecto que incluye la posible adquisición de 88 vehículos blindados así como la constitución de un unidad productiva en Argentina.

El gobierno argentino quiere que NORINCO comparta tecnologías y desarrolle con ellos una unidad de fabricación de vehículos de doble uso. Las negociaciones aún están en curso. Vaca Narvaja argumentó que la colaboración con China debe incluir “vínculos productivos para el sistema militar argentino” como principio fundamental.