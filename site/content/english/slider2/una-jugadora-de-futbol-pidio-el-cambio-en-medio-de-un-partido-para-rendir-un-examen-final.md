+++
author = ""
date = 2021-06-27T03:00:00Z
description = ""
image = "/images/ka6ynxex5jhgpi3dctuityjt5a.jpg"
image_webp = "/images/ka6ynxex5jhgpi3dctuityjt5a.jpg"
title = "UNA JUGADORA DE FUTBOL PIDIÓ EL CAMBIO EN MEDIO DE UN PARTIDO PARA RENDIR UN EXAMEN FINAL"

+++

**_Daniela Palma es jugadora de Quilmes y salió en el primer tiempo para dar un examen de Informática. Lo hizo al costado de la cancha, a pesar del frío, y con los botines puestos._**

Daniela Palma es todo un ejemplo del esfuerzo que hoy hacen las mujeres para jugar al fútbol. Estudian, trabajan y entrenan. A veces, los horarios juegan en contra y se mezcla todo, convirtiendo una cancha de fútbol en una mesa de examen al mismo tiempo. Eso pasó el sábado con la futbolista de Quilmes, que pidió el cambio a los 40 minutos del primer tiempo para rendir una materia de la carrera de Kinesiología y Fisiatría en la Universidad Nacional Arturo Jauretche.

La mediocampista central de del Cervecero tiene 27 años y le encanta jugar al fútbol. En Primera C todo es absolutamente amateur. No gana un peso. Todo es a pulmón soñando con, tal vez en un futuro, firmar un contrato profesional. Por ahora eso está lejos y Daniela no quiere desatender sus estudios. Por eso rindió al costado de la cancha en medio del partido que su equipo le ganó 3 a 0 a Argentino de Quilmes, durante un amistoso jugado el sábado por la tarde.

Palma habló en la semana con su entrenador (Fernando Chiappino) y acordaron que jugaría el primer tiempo, pero le advirtió que a las 17 tenía que estar sentada con la computadora para rendir de manera virtual el examen final de Informática que le permitiría promocionar la materia. Incluso, intentó sin éxito adelantar el turno de la evaluación, pero el titular de la Cátedra “no le dio pelota”.

A los 40 minutos del primer tiempo, la joven que además trabaja todos los días de 7 a 16 como niñera, pidió el cambio. Salió y se puso un pantalón largo y una campera. Todavía con los botines puestos, prendió la computadora y rindió el examen. La foto de @DeportesenFM se viralizó y recibió muchas felicitaciones en Twitter por su decisión de jugar y cumplir con el estudio al mismo tiempo.

Palma es fanática del fútbol y los deportes y uno de sus sueños es trabajar como kinesióloga en Quilmes, a donde llegó en 2020 cuando el club formó su equipo femenino. Anteriormente jugó en Defensa y Justicia y cuando era chica practicó hockey durante muchos años.

Quilmes juega en el torneo de la C del fútbol femenino que organiza la AFA y el campeonato todavía no tiene fecha de inicio. El ascenso lleva más de un año sin jugarse y por ahora no está claro cuándo se reanudará. Mientras tanto, Daniela se entrena, trabaja y juega amistosos con sus compañeras. Todo eso sin descuidar la facultad.