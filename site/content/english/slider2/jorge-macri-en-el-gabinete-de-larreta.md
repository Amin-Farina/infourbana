+++
author = ""
date = 2021-10-31T03:00:00Z
description = ""
image = "/images/acuerdo_orrego-larreta-jorge_macri-1.jpg"
image_webp = "/images/acuerdo_orrego-larreta-jorge_macri.jpg"
title = "¿JORGE MACRI EN EL GABINETE DE LARRETA?"

+++

##### **_Desde su entorno, aseguraron que no será jefe de Gabinete. El intendente de Vicente López ocuparía lo que sería el ministerio del AMBA. La jugada se proyecta en también en función de su 2023._**

De acuerdo a La Política Online, desde el gobierno de la Ciudad de Buenos Aires aseguran que jefe comunal está cada vez más cerca de integrar el Gabinete de Horacio Rodríguez Larreta.

Desembacaría como ministro o secretario de Area Metropolitana de Buenos Aires, cargo que se crearía especialmente para el intendente de Vicente López luego de que esté se bajase de la potencial interna contra Diego Santilli y en función de lo que pueda pasar en 2023, ya que de no ser candidato para la provincia de Buenos Aires el jefe comunal podría regresar a sus pagos dada la letra chica de la Ley provincial 14.515.

Según el medio mencionado, Larreta estaría afable a sacarle la jefatura de Gabinete a Felipe Miguel, uno de sus funcionarios más cercanos.

Cabe recordar que el primo del ex presidente se había postulado directamente ante Larreta para reemplazar a Miguel, con una frase impactante: "Vos necesitás un Manzur", había revelado LPO.