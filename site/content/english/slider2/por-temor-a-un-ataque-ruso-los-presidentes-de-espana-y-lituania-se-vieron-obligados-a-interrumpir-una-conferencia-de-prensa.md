+++
author = ""
date = 2021-07-08T03:00:00Z
description = ""
image = "/images/evacuacion-sanchez-lituania-otan-1.jpg"
image_webp = "/images/evacuacion-sanchez-lituania-otan.jpg"
title = "POR TEMOR A UN ATAQUE RUSO, LOS PRESIDENTES DE ESPAÑA Y LITUANIA SE VIERON OBLIGADOS A INTERRUMPIR UNA CONFERENCIA DE PRENSA"

+++

**_En medio del evento, surgieron las voces de los pilotos a la carrera en el hangar donde se encontraban sus aviones ante la presencia de un posible enemigo en el espacio aéreo._**

Dos cazas rusos que volaban sin advertir de su presencia en el mar Báltico causaron este jueves la alerta que interrumpió este jueves el acto que estaba protagonizando el presidente del Gobierno español, Pedro Sánchez, junto con el presidente de Lituania, Gitanas Nauseda, en la base de la OTAN en Siauliai (Lituania).

“Esta mañana dos cazas Eurofighter de la OTAN despegaron para identificar a dos aviones que volaban en el mar Báltico. Identificaron a dos SU-24 que se dirigían al noreste”, indicaron a Efe fuentes de la Alianza Atlántica.

Estos dos aviones de ataque rusos “no enviaron un plan de vuelo, no tenían sus transpondedores encendidos y no hablaron con los controladores del tráfico aéreo”, agregaron.

Sánchez, en la última jornada de su gira báltica, inició su agenda en Lituania visitando esa base en la que existe un contingente español que participa en la misión de policía aérea báltica frente a posibles amenazas rusas.

Sánchez se reunió en las instalaciones militares con Nauseda y tras su encuentro tenían previsto sendos discursos ante los militares.

El presidente lituano fue el primero en tomar la palabra en uno de los hangares de la base y delante de uno de los siete Eurofighter españoles que están destacados en la base hasta finales de agosto.

Su discurso fue interrumpido por las voces de los pilotos que accedieron a la carrera al hangar anunciando una alarma ante la presencia de un avión no identificado y subiendo a los cazas para despegar en misión identificativa.

La situación generó unos momentos de confusión debido a que estaba previsto un simulacro de alarma durante la visita de Sánchez, pero tanto el destacamento español como fuentes del Gobierno aseguraron que se trataba de una alerta real.

Protegidos con tapones en los oídos, Sánchez y el presidente de Lituania observaron todos los movimientos previos al despegue de dos aviones españoles para proceder a la identificación de los aviones.

En torno a media hora después del despegue de los cazas continuó el acto y Sánchez destacó la importancia de la misión del contingente español como dijo que se había podido comprobar poco antes.

“Hemos visto un caso real de lo que justifica la presencia de las tropas españolas en Lituania”, comentó el jefe de Gobierno.

“Esto demuestra una vez más la importancia de la misión de patrulla aérea de la OTAN, que funciona desde hace 60 años para mantener nuestros cielos seguros”, indicó por su parte la portavoz aliada, Oana Lungescu.

Asimismo, aseguró que también “muestra las habilidades de nuestros pilotos y la estrecha coordinación entre aliados de la OTAN”.

A través de toda Europa, los aviones de combate de la OTAN están permanentemente de servicio y preparados para despegar rápidamente en caso de que se detecten vuelos sospechosos o no anunciados cercanos al espacio aéreo aliado.

En este servicio están incluidas las patrullas aéreas de la OTAN, en la que destacamentos de aviones entran y salen de países aliados para contribuir a la protección de sus cielos.

En 2020, las fuerzas aéreas de la OTAN se movilizaron más de 400 veces para interceptar aviones desconocidos, la mayor parte de ellos de Rusia, que se acercaban al espacio aéreo de la Alianza.