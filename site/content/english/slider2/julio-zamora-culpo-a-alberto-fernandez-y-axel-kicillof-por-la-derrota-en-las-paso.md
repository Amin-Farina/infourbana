+++
author = ""
date = 2021-09-20T03:00:00Z
description = ""
image = "/images/julio-zamora-428-3.jpeg"
image_webp = "/images/julio-zamora-428-2.jpeg"
title = "JULIO ZAMORA CULPÓ A ALBERTO FERNÁNDEZ Y AXEL KICILLOF POR LA DERROTA EN LAS PASO"

+++

#### **_Julio Zamora, jefe comunal de Tigre, habló luego de la semana más crítica del Gobierno Nacional. “El pronóstico ganador nos llevó a perder votos, nos confiamos”, admitió. Allí, el oficialismo perdió por seis puntos frente a Juntos por el Cambio_**

La herida sigue abierta en el Frente de Todos; continúan los reproches, los cuestionamientos y hay quienes siguen preguntándose cómo no se previó tamaña derrota. A una semana de las PASO, luego de los días más críticos de la gestión de Alberto Fernández en la que su autoridad fue jaqueada por Cristina Kirchner y se resolvió una renovación profunda y estrepitosa del Gabinete Nacional, un intendente del oficialismo culpó al Presidente y al gobernador bonaerense Axel Kicillof por la derrota electoral en manos de Juntos por el Cambio.

Se trata del jefe comunal de Tigre, Julio Zamora, quien se mostró molesto - con “bronca”- al considerar que su gestión a nivel municipal fue buena pero el arrastre negativo de parte de Nación, producto del manejo de la pandemia, llevó al FdT a perder por seis puntos de diferencia. Allí la lista oficial fue encabezada por Gisela Zamora, esposa del intendente, obtuvo el 32,06% de los votos, mientras que Juntos por el Cambio cosechó el 38% entre las dos nóminas que presentó: Segundo Cernadas, candidato referenciado en Diego Santilli que se impuso en la interna al ex diputado nacional Nicolás Massot. Es la primera vez en años que el oficialismo en Tigre no alcanza el 35% de los votos.

“Perdimos veinte puntos con una buena gestión, obras, iluminación, infraestructura en salud y en educación, da bronca”, lamentó Julio Zamora. En diálogo con la periodista Patricia Chaina, para Página 12, el jefe comunal admitió que “el pronóstico ganador nos llevó a perder votos, nos confiamos”. En efecto, la sensación es compartida por el amplio espectro oficialista. Incluso los boca de urna del kirchnerismo daban a Victoria Tolosa Paz triunfando por más de 6 puntos. Si bien la precandidata a diputada nacional por la provincia de Buenos Aires fue la más votada (2.788.022), entre Santilli (1.895.769) y Facundo Manes (1.254.220), Juntos venció por más de 4 puntos al Frente de Todos. En líneas generales, en el oficialismo atribuyen la respuesta de la gente al enojo por la situación económica. En Tigre, de 338.000 votos, la lista oficialista retuvo 68 mil mientras que el principal espacio opositor llegó a 78.000. El resto de las fuerzan se quedaron con 50 mil, mientras que entre los en blanco o nulos hubo 16.000. Un dato no menor, el ausentismo (125 mil personas) fue marcado en los barrios populares.

“Hay disconformidad a nivel país producto de la pandemia y hay responsabilidad a nivel provincia”, analizó Zamora, quien insistió que de su parte el gobierno local tejió “una trama a cuatro manos, con obras que unen los barrios, con iluminación, asfalto, cloacas, agua”. Pero no alcanzó. En el municipio también perciben con preocupación el avance del PRO en una localidad caracterizada por el aumento de los barrios cerrados. Por ejemplo, en Nordelta votan 40 mil personas; allí el oficialismo perdió. Mientras que en Benavides, El Talar, Rojas, Almirante Brown, donde no hay countries, y votan alrededor de 60.000 personas, el resultado fue positivo.

En medio de la crisis política e institucional del Gobierno, antes de las críticas que emitió este domingo, Zamora se había puesto del lado del mandatario. “En este tiempo difícil, quiero expresar mi apoyo a nuestro presidente. Bajo su conducción, vamos a seguir trabajando unidos para la reconstrucción de la Argentina”, manifestó el pasado miércoles en sus redes sociales.

Días atrás, otro funcionario del kirchnerismo remarcó luego de la derrota del pasado domingo que “hay que buscar responsables de cara hacia adentro, no hacia afuera”. Fue Sergio Berni, el ministro de Seguridad de Kicillof, quien en reiteradas ocasiones cuestionó la gestión de Alberto Fernández, quien el pasado viernes dijo: ”A veces uno reclama que los funcionarios estén más en la calle; necesitamos más funcionarios con la vocación de escuchar, resolver e involucrarse más”.