+++
author = ""
date = 2022-01-12T03:00:00Z
description = ""
image = "/images/ijx7r2ae4fcmdno4fytzbawomu.jpg"
image_webp = "/images/ijx7r2ae4fcmdno4fytzbawomu.jpg"
title = "EL EX TITULAR DEL MINISTERIO DE SALUD, GINÉS GONZÁLEZ GARCÍA RECORDÓ SU SALIDA DEL GOBIERNO: “NO HICE NADA INCORRECTO, QUIZÁS COMETÍ ALGUNA ESTUPIDEZ”"

+++

##### **_El ex ministro de Salud Ginés González García volvió a recordar hoy su salida del Gobierno del Frente de Todos envuelto en la polémica que significó el vacunatorio VIP, que benefició a un grupo de funcionarios cuando todavía no se había expandido la inoculación contra el coronavirus en todo el país._**

> “Fue un dolor grande, algo difícil de ser superado; pero (estoy) con la tranquilidad de que no hice ninguna cosa que no estuviera habilitada, no hice nada incorrecto y eso se lo planteé a la Justicia. Quizá cometí alguna estupidez al admitir que 10 casos, que se negaron a ser vacunados en el Hospital Posadas, aparecieron en el ministerio, pero un día que yo no estaba, una cosa rara”, recordó.

Además, y en diálogo con radio Delta, aseguró que aquel episodio “no pudo haber sido fortuito”. “A la distancia pienso, y por ahí me lo dicen mucho, que fue una emboscada, una cama, pero ya pasó, yo no tengo pruebas, pero es raro, porque no soy ni amigo de (Horacio) Verbitsky, cosa que él dijo, y segundo no se vacunó porque era amigo mío, se vacunó porque tenía 79 años y estaba entre los que se vacunaban en ese momento, en esa edad. Es difícil de explicar, y después fue usado como bandera de combate por la oposición”, agregó.

El escándalo por el vacunatorio VIP comenzó el 19 de febrero de 2021, cuando Verbitsky relató en un programa de radio cómo le facilitaron el acceso a la vacuna fuera del protocolo vigente. “Llamé a mi viejo amigo Ginés González García”, reconoció el periodista en aquel momento. En virtud de eso fue convocado a vacunarse en el Ministerio de Salud y se descubrió que otros allegados al poder habían tenido ese privilegio.

Varios meses más tarde, el entonces Ministro de Salud admitió que existió un contacto telefónico con el periodista mucho antes de que estallara la polémica: “Me dijo ‘se ha muerto un pariente mío, estoy muy asustado’ y yo le pedí que esperara”.

Consultado sobre la forma que dejó el Gobierno, cuando el Alberto Fernández oficializó de manera pública que le había pedido la renuncia, planteó: “No me pareció justificable lo que pasó conmigo, y se lo dije, en última instancia, creo que si uno quiere conducir tiene que bancar”. “Hubiera querido hablar, decir la verdad”, señaló.

> “Me dijeron que renuncie, y soy un tipo disciplinado, soy peronista, pero no había pasado lo mismo con casos peores que el mío, de algunos miembros del Ejecutivo; ya pasó, no tiene arreglo, el daño no es judicial porque estoy convencido de que no habrá inconvenientes, tengo daño con parte de la sociedad y eso no me gusta, eso es feo, no soy un héroe o mártir, pero me he dedicado al servicio del Estado gran parte de mi vida, como embajador, como ministro”, completó.

Los imputados por la causa judicial que se inició fueron, además del ex ministro, su sobrino y ex jefe de Gabinete del Ministerio de Salud, Lisandro Bonelli. También se imputó a a Alejandro Collia y Marcelo Guillé. Guillé fue secretario privado de González García y realizó llamados para convocar a los vacunados VIP al ministerio de Salud. En tanto, Collia, que era funcionario de González García, continúa -en la gestión de Carla Vizzotti- como subsecretario de Gestión de Servicios e Institutos del ministerio de Salud de la Nación.

Por otro lado, Ginés González García se refirió a la situación de la pandemia actual y la multiplicación de los casos por la irrupción de la variante Ómicron. “Antes había más respeto y miedo, hoy se perdió mucho, sobre todo en los jóvenes”. Además, destacó: “Se podrían haber hecho las cosas mejor, pero hoy Argentina está rankeada como uno de los países con mejores resultados en el mundo”.