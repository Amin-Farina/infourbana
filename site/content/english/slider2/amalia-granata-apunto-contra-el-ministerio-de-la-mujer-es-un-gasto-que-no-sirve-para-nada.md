+++
author = ""
date = 2022-03-04T03:00:00Z
description = ""
image = "/images/qvykqrzapbav7jor4hllbcjc3i.jpg"
image_webp = "/images/qvykqrzapbav7jor4hllbcjc3i.jpg"
title = "AMALIA GRANATA APUNTÓ CONTRA EL MINISTERIO DE LA MUJER: “ES UN GASTO QUE NO SIRVE PARA NADA”"

+++

##### **_La diputada se manifestó tras los dichos de la ministra, Elizabeth Gómez Alcorta, sobre el caso de violación en Palermo y pidió el cierre del organismo_**

Fiel a su estilo crítico y provocador, la diputada provincial de Santa Fe, Amalia Granata volvió a hacer duras declaraciones contra distintos organismos de gobierno. En ese contexto, apuntó a los dichos de la ministra Elizabeth Gómez Alcorta, titular de la cartera de Mujeres, Géneros y Diversidad en relación al caso de violación en Palermo.

“Hay que cerrar el Ministerio de la Mujer y el INADI” afirmó la legisladora al considerar que “no sirven para nada y lo usan para mantener la militancia”. Y apuntó directamente a la ministra, quien había hecho declaraciones en sus redes sociales que se prestaron a diferentes interpretaciones por algunos referentes de distintos espacios.

“No son monstruos, son varones socializados en esta sociedad”, había escrito Gómez Alcorta al referirse a los acusados de la violación grupal a una joven en el barrio porteño de Palermo ocurrida el lunes. “Es tu hermano, tu vecino, tu papá, tu hijo, tu amigo, tu compañero de trabajo. No es una bestia, no es un animal, no es una manada ni sus instintos son irrefrenables. Ninguno de los hechos que nos horrorizan son aislados. Todos y cada uno responden a la misma matriz cultural”, aseguró desde su cuenta personal.

“Esas mujeres como Gómez Alcorta dicen pavadas para distraernos y sacar el enfoque de su incapacidad de manejar su Ministerio que es un gasto para el Estado, solamente un gasto y ninguna solución”, sostuvo Granata en declaraciones al programa Pan y Circo que conduce el periodista Jonatan Viale por Radio Rivadavia. Además, añadió que “Alcorta habla de que son víctimas de la sociedad, pero estos chicos por su edad fueron criados durante los gobiernos kirchneristas ¿Quiénes gobernaron durante todos esos años? Háganse cargo de algo”.

Fue entonces cuando recordó que estuvo “en contra” cuando se creó un Ministerio igual en su provincia, Santa Fe, porque “es un gasto innecesario, creado para mantener militancia con cargos, ñoquis, tan simple como eso”. En ese momento, aclaró que presentó un pedido para saber de dónde sacaron la plata para mantenerlo porque “fue creado luego del Presupuesto. Hay 127 cargos en el Ministerio y quieren agregar 30 cargos más”.

Al respecto, hizo una comparación de la situación con un episodio casero: “si en tu casa tenés una pileta y no la usás, solo te produce gastos para mantenerla y en algún momento la tenés que clausurar, vaciarla y taparla. Y son gastos que salen de la gente, del bolsillo del que va a laburar, que se levanta a las 4, 5 o 6 de la mañana, que paga sus impuestos, que trata de estar al día. Que va a esperar el colectivo con miedo a que le roben y lo maten. Vivimos en un país donde nada funciona”.

Sobre el final de la entrevista, la ex mediática quiso dejar un mensaje personal. “Si mañana me violan a mí, estas mujeres van a estar mudas, no van decir nada porque yo pienso diferente a ellas. No es un Ministerio para todas, es un ministerio para la militancia”, arriesgó y juró que si dependiese de ella, cerraría tanto el ministerio nacional como provincial. “Hay que animarse a hacerlo, la gente tiene miedo de decir lo que piensa. Hay que terminar con el relato y esto que no sirve para nada, hay que salir adelante con la educación. Entonces esa plata que es mucha por mes, porque hay muchos ñoquis, tiene que ir para la educación”, aseguró.