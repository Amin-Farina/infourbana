+++
author = ""
date = 2021-10-15T03:00:00Z
description = ""
image = "/images/cra-impuesto-riqueza-maximo-kirchner-650x404.jpg"
image_webp = "/images/cra-impuesto-riqueza-maximo-kirchner-650x404.jpg"
title = "¿SIRVIÓ DE ALGO EL IMPUESTO A LA RIQUEZA?"

+++

#### **_Las cifras del presupuesto muestran una muy baja ejecución en áreas sensibles como la construcción de viviendas populares, educación y la explotación de Vaca Muerta, que habían sido definidas como prioritarias en la ley que dio origen al tributo._**

El Gobierno utilizó un muy bajo porcentaje de los recursos que recaudó con el nuevo impuesto a las grandes fortunas, que provocó la salida de números contribuyentes importantes del país, por la excesiva carga fiscal.

La alícuota más alta llegó al 5,25% del patrimonio y buena parte de los contribuyentes pagó, aunque algunos otros pocos decidieron recurrir a la Justicia por considerar que era confiscatorio y algunos recibieron una sentencia favorable tanto en primera como en segunda instancia. Sobre todo, la carga patrimonial se duplicó con el pago de Bienes Personales, que está fijado sobre el mismo hecho imponible que este impuesto de “emergencia” que se usó poco para los fines previstos, según las propias cifras oficiales. Hasta mayo la AFIP había recaudado $ 230.000 millones por este polémico tributo y luego entraron sumas más acotadas en cuotas.

La ley marcaba que la recaudación del denominado “aporte solidario” se destinaría a comprar equipamiento de salud para atender la pandemia, apoyar a las PyMEs con subsidios y créditos, urbanizar los barrios populares con obras que empleen a los vecinos de cada barrio, hacer obras y equipar a YPF para producir y envasar gas natural y financiar un relanzamiento del plan Progresar para que los jóvenes puedan seguir estudiando.

En particular, la ejecución fue muy baja en educación, en viviendas populares y en Vaca Muerta, el proyecto estratégico de energía más importante del país. En el caso de Vaca Muerta, la ejecución fue nula, según las fuentes oficiales. Ahora, el dinero se utilizará para financiar parte de la construcción del gasoducto Néstor Kirchner anunciado ayer.

Al respecto, el secretario de Energía, Darío Martínez, señaló que “es prioritario la construcción de gasoducto Néstor Kirchner. Estamos analizando la mejor herramienta para utilizar los fondos del aporte solidario que esta en manos de IEASA para financiar esta obra estratégica para el desarrollo de la matriz energética de nuestro país; el aporte solidario debería permitir disponer de aproximadamente 500 millones de dólares para el desarrollo de proyectos energéticos en el país”.

“Con esta política estamos dando pasos previsibles para garantizar el gas que los argentinos y las argentinas necesitan para su desarrollo, en un contexto mundial en dónde los países centrales deben enfrentar un invierno con precios altos y analizan medidas para minimizar el impacto en los hogares y la industria. Miramos el mundo para tomar las mejores definiciones, para volvernos competitivos y realmente ser parte de un mapa mundial complejo. Así nace el gasoducto y ahí está el resultado exitoso y no azaroso del plan gas”, indicó el funcionario.

“También, contribuimos eficientemente en mantener una matriz energética más limpia con vistas a las transiciones energéticas y la disminución de la emisión de gases del efecto invernadero. El gas producido en argentina por trabajadores argentinos es el primer vehículo de esa transición energética justa, que nos permitirá ahorrar divisas producto de la sustitución de importaciones”, aclaró.

En Educación -gestión de Nicolás Trotta- solo se ejecutó el 11% de los fondos destinados a las Becas Progresar: 5000 millones sobre 45.000 millones, de acuerdo a las cifras del presupuesto analizadas por Asociación Argentina de Presupuesto y Administración Financiera Pública (ASAP).

También la ejecución del Fondo de Integración Social Urbana (FISU), que gestiona el Ministerio de Desarrollo Social, fue muy escasa, pese a que la mayor parte del impuesto se recaudó a fines de marzo –luego un remanente se cobró en cinco cuotas- de la siguiente manera: se recaudaron $ 50 mil millones, pero tienen proyectos por sólo 9 mil millones y han pagado de modo efectivo al 30 de setiembre 2 mil millones, según cifras oficiales.

El saldo a ejecutar del FISU es de 41 mil millones de pesos. Y del monto ejecutado, aún quedan por abonar proyectos ya aprobados por una suma de $ 7 mil millones, según se desprende de las cifras tomadas del presupuesto nacional.

Cabe recordar que el Fondo de Integración Socio Urbana fue creado por el Decreto N° 819/2019 a partir de la Ley Nº 27.453 y tenía como objetivo principal “el financiamiento de proyectos de integración socio urbana para los Barrios Populares que se encuentren inscriptos en el registro Nacional de Barrios Populares y la creación de lotes con servicios”.

A priori, el Gobierno afirmaba que “el financiamiento de estas obras propicia el acceso a los servicios básicos, la mejora y ampliación del equipamiento social y de la infraestructura, el tratamiento de los espacios públicos, la eliminación de barreras urbanas y la mejora en la accesibilidad, la conectividad y la mitigación de riesgos ambientales. Dichas acciones deberán ser integrales, participativas y con enfoque de género y diversidad”.

En el caso de las pymes, se ejecutaron $ 40.000 millones sobre $ 45.000 y en Salud, para insumos médicos, $ 44.311 sobre $ 45.009 millones, según las cifras oficiales analizadas por ASAP.

##### **CRÍTICA DE LOS EXPERTOS**

Con este pobre balance en la mayoría de los rubros, los tributaristas ratificaron que el perjuicio fue claramente mayor que el supuesto beneficio que formuló en términos retóricos el Gobierno, sin el correspondiente reflejo en la realidad.

Al respecto, Horacio Cardozo expresó que “el desatino del impuesto es palpable y medible. En los dos últimos años (desde que se empezó a hablar de el) y por culpa de este impuesto muchos empresarios se han ido a residencias fiscales más amigables y muchos más seguirán ese camino”.

“Pero no solo eso, sino que frente a la sensación de que la única estrategia del gobierno es esquilmar a los que algo tienen o más tienen, muchos han dejado de invertir pues estas medidas generan más daños que beneficios”.

“Esto no significa renegar de que los que más tienen deben contribuir en mayor medida, lo que es una medida sabia, pero no como lo ha planteado este gobierno en cuanto a que los empresarios egoístas y avaros deben contribuir, si no dentro de un proyecto más amplio y de crecimiento y mejora de las condiciones económicas”.

“Estimo que pasa algo parecido con el aumento de la alícuota del IVA y otros impuestos, está demostrado que a determinado aumento no se genera una mayor recaudación, esto es que no tiene sentido seguir aumentando la alícuota, pues a partir de cierto momento cada vez se recauda menos”.

“El daño provocado es inmenso, daño a la confianza de los que más tienen y que por ende tienen más posibilidades de sacar adelante la economía generando trabajo. Varios de los que me han consultado en su momento por el impuesto a la riqueza, hoy han mudado su residencia fiscal”.

Por su parte, César Litvin dijo que “los legisladores, al momento de aprobar un nuevo impuesto deben conocer, de manera incuestionable, los efectos económicos adversos que estos generan. En el caso particular del Aporte Solidario, un verdadero impuesto aunque fue disfrazado de Aporte, ha generado una reacción social importante en los contribuyentes obligados al pago , que son también los mismos, que tienen posibilidades de realizar inversiones productivas , generadora de empleo privado”.

“Esta reacción tiene sustento en el efecto confiscatorio que pudo observarse en gran parte de los casos donde el impuesto ha absorbido una parte sustancial de la renta y en otros casos el gravamen supero holgadamente la renta de esos bienes alcanzados por el tributo”.

“El panorama adquiere características absurdas cuando se suma el efecto del impuesto sobre los bienes personales al ASE. La Justicia tiene un enorme desafío: poner en el lugar que se merece las garantías constitucionales, que actúan como limite al avance del Estado sobre el derecho de propiedad privada; de lo contrario se estaría naturalizando lo incorrecto y el contribuyente queda sin protección ante la voracidad del Fisco”.

“Este impuesto ha sido generador de más pobreza porque produce la huida de capitales y emprendedores con capacidad de consumir y fundamentalmente de invertir y de este modo afecta la actividad económica en una coyuntura donde debe recorrerse el camino inverso. En efecto, los países modernos están apuntando a sistemas tributarios competitivos y estables para seducir capitales y combatir el desempleo en época de pos pandemia”, aclaró.

En tanto, Iván Sasovsky dijo que “al final no sirvió de nada establecer un impuesto de este estilo, ya que los fondos recaudados se terminaron licuando con el incremento desmesurado del gasto que se llevó adelante en los últimos meses”.

“La prueba fehaciente de que fue insuficiente está en el hecho de que varios actores del oficialismo plantearon la necesidad de que deje de ser excepcional y que se aplique mientras dure las medidas de prevención sanitaria”.

“Les sacaron a los privados una suma de dinero que podían haber sido invertidos en la generación de riqueza y puestos de trabajo, cosa que no hace el Estado (en este caso, solo fue para cubrir gastos)”.

“Para peor, el impacto de la expansión del gasto y de la emisión correspondiente para sustentarlo, solo se verá reflejada en una mayor inflación y terminará repercutiendo en lo que menos tienen”, concluyó el experto.