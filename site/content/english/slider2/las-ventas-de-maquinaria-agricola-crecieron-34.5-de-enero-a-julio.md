+++
author = ""
date = 2021-08-23T03:00:00Z
description = ""
image = "/images/massey-ferguson-690x460.jpg"
image_webp = "/images/massey-ferguson-690x460.jpg"
title = "LAS VENTAS DE MAQUINARIA AGRÍCOLA CRECIERON 34.5% DE ENERO A JULIO"

+++

**_Hubo 3.637 patentamientos durante los primeros 7 meses del año. Los tractores experimentaron un alza del 50,2% interanual, y las pulverizadoras del 13,7%. En contraposición, las cosechadoras cayeron casi 17%._**

Las ventas de maquinaria agrícola subieron 34,5% interanual durante los primeros 7 meses de este año, al llegar a las 3.637 unidades patentadas frente a las 2.705 de igual periodo del 2020. Así lo informó la Asociación de Automotores de la República Argentina (ACARA) al analizar las comercializaciones de tractores, cosechadoras y pulverizadoras, que tuvieron comportamientos disímiles.

El incremento fue apuntalado fuertemente por los tractores, que en lo que va del 2021 tuvieron un crecimiento del 50,2%, con 2.880 patentamientos frente a los anteriores 1.918. Asimismo, alcanzaron un repunte del 11% en julio, comparado con igual mes del año pasado, al pasar de las 520 unidades a las 554. No obstante, si la comparación es intermensual, hubo una merma del 4% frente a los 577 de junio.

Si se analiza de enero a julio inclusive, puede verse que las cosechadoras no corrieron la misma suerte. Ya que tuvieron una caída del 16,9% interanual, con 375 ventas contra las 451 del mismo lapso del 2020, siguiendo la tendencia negativa de una industria muy por debajo de la media histórica de 800 unidades de los últimos años, señalaron desde la entidad.

Además, si se contrapone julio de este año con igual mes del anterior, el declive de las cosechadoras llega al 35%, ya que bajaron de 97 a las recientes 72. Aunque si la equiparación es intermensual, ahí sí el rubro muestra una mejora del 14% frente a las 63 transacciones registradas en junio.

En tanto, con 382 ventas, las pulverizadoras mostraron un incremento del 13,7% interanual de enero a julio, ya que el año pasado habían sido de 336 en ese mismo intervalo. En cambio, si la contraposición se hace solamente con julio del 2020, hubo una caída del 27,7% interanual, al pasar de 92 a 73. Mientras que, en términos intermensuales, se logró un ascenso del 28% sobre las 57 operaciones de junio, según reportó la División de Maquinaria Agrícola de ACARA.

En el marco de este informe, el presidente de la entidad, Ricardo Salomé, aseguró que “al igual que lo sucedido en el mercado de autos y motos, julio fue un buen mes para este sector y observamos cómo muchos están aprovechando para poder renovar sus maquinarias, que en el caso del sector agropecuario es un verdadero capital de trabajo”.

El ejecutivo puntualizó que debe apuntarse a la conformación de una “oferta acorde”, ya que existe una demanda que tiene la totalidad de la producción reservada para los próximos meses. Eso, señaló, denota que la actividad se mantendrá con una buena performance durante lo que queda del año.

Al respecto, evaluó: “Es un síntoma alentador, ya que en nuestro país el campo es un sector dinamizador como pocos y cuando comienza a levantar su ritmo, muchas industrias afines también lo hacen, algo que también observamos si analizamos el crecimiento que viene mostrando la venta de pick ups y otros vehículos de uso intensivo”.