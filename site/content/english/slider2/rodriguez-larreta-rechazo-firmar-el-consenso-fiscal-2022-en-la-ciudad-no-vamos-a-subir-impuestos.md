+++
author = ""
date = 2021-12-23T03:00:00Z
description = ""
image = "/images/horacio-rodriguez-larreta___krtqutogq_1200x630__1.jpg"
image_webp = "/images/horacio-rodriguez-larreta___krtqutogq_1200x630__1.jpg"
title = "RODRÍGUEZ LARRETA RECHAZÓ FIRMAR EL CONSENSO FISCAL 2022: “EN LA CIUDAD NO VAMOS A SUBIR IMPUESTOS”"

+++

#### **_El jefe de Gobierno porteño, Horacio Rodríguez Larreta, adelantó hoy su rechazo al proyecto de pacto fiscal 2022 que impulsa la Casa Rosada y que buscar consensuar con los gobernadores. “En la Ciudad no vamos a aumentar impuestos, por eso no lo vamos a firmar”, afirmó el mandatario, luego de una realizar una recorrida por el centro de testeo contra el COVID-19 en Costa Salguero._**

La postura de Larreta corresponde al borrador de la iniciativa que comenzó a circular durante la noche de ayer y que, entre otros puntos, habilita la posibilidad de que las provincias suban impuestos. Y se condice con el rechazo al pacto fiscal del año pasado, cuando la ciudad de Buenos Aires rechazo su firma porque la contrapartida de esa adhesión era desistir del juicio contra el Estado nacional por la quita de la coparticipación que anunció el presidente Alberto Fernández en medio de la pandemia.

El nuevo consenso fiscal que propone el Gobierno dispone en unos de sus apartados que las provincias y la CABA, dentro del transcurso del año 2022, “procurarán legislar un impuesto a todo aumento de riqueza obtenido a título gratuito como consecuencia de una transmisión o acto de esa naturaleza, que comprenda a bienes situados en su territorio y/o beneficie a personas humanas o jurídicas domiciliadas en el mismo, y aplicarán alícuotas marginales crecientes a medida que aumenta el monto transmitido a fin de otorgar progresividad al tributo”.

“El mismo alcanzará el enriquecimiento que se obtenga en virtud de toda transmisión a título gratuito, incluyendo: Las herencias; los legados; las donaciones; los anticipos de herencia; cualquier otra transmisión que implique un enriquecimiento patrimonial a título gratuito”, se completa en el borrador del proyecto.Este punto es rechazado por Juntos por el Cambio.

El economista y diputado nacional Luciano Laspina se manifestó a través de las redes sociales: “El inminente ‘Pacto Fiscal 202′ delegará en las provincias la creación de un nuevo ’Impuesto a la Herencia’. Además, permitirá aumentos en los topes de Ingresos Brutos en varios rubros. La voracidad impositiva no tiene límites”, aseguró. El texto también incluye una alícuota máxima del 3,5% a la transferencia de inmuebles, del 3% a la transferencia de automotores y del 2% a los restantes actos, contratos y operaciones alcanzadas por este tributo en general. Y establece alícuotas topes de Ingresos Brutos: las más altas son para los servicios financieros e intermediación financiera con un 9%.Sobre la iniciativa también se refirió la portavoz presidencial, Gabriela Cerruti. “El lunes habrá una reunión del Presidente con los gobernadores donde se va a conversar sobre el consenso fiscal”, adelantó.

El Jefe de Estado y los mandatarios provinciales acordaron avanzar con este tema el pasado martes en la quinta de Olivos, cuando Alberto Fernández prometió a los gobernadores oficialistas que no se frenarán las obras en las provincias por el rechazo opositor al Presupuesto 2022. Ese día, según informaron fuentes oficiales, fueron convocados a la “reunión de trabajo” exclusivamente los gobernadores cercanos al Frente de Todos: Axel Kicillof (Buenos Aires); Raúl Jalil (Catamarca); Jorge Capitanich (Chaco); Gustavo Bordet (Entre Ríos); Gildo Insfrán (Formosa); Sergio Ziliotto (La Pampa); Ricardo Quintela (La Rioja); Sergio Uñac (San Juan); Alicia Kirchner (Santa Cruz); Omar Perotti (Santa Fe); Gerardo Zamora (Santiago del Estero); Gustavo Melella (Tierra del Fuego); y Osvaldo Jaldo (Tucumán).