+++
author = ""
date = 2021-12-08T03:00:00Z
description = ""
image = "/images/carlos-raimundi-la-juventud-radical-defensor-maduro-1.jpg"
image_webp = "/images/carlos-raimundi-la-juventud-radical-defensor-maduro.jpg"
title = "NICARAGUA: ARGENTINA SE ABSTUVO OTRA VEZ DE CONDENAR LAS VIOLACIONES A LOS DERECHOS HUMANOS DEL RÉGIMEN DE DANIEL ORTEGA"

+++

#### **_El gobierno de Alberto Fernández se sumó a México, Bolivia, Honduras y otros cuatro países que no apoyaron la resolución impulsada por Estados Unidos, Canadá, Brasil y 22 países más que exigieron la inmediata liberación de los presos políticos y unas elecciones limpias y libres._**

El gobierno de Alberto Fernández se abstuvo otra vez de condenar graves las violaciones a los derechos humanos del régimen que encabeza Daniel Ortega en Nicaragua, en una votación clave de la Organización de Estados Americanos (OEA), que declaró que en ese país no se cumple la Carta Democrática y, al mismo tiempo, exigió la libertad inmediata de los presos políticos y la realización de elecciones limpias, libres y transparentes.

El embajador Carlos Raimundi fue el responsable de justificar la decisión de la Casa Rosada de no acompañar el voto mayoritario del continente contra la autocracia nicaragüense para que habilite el ingreso de una misión diplomática de la OEA para iniciar un diálogo sobre reformas electorales y la convocatoria de nuevas elecciones.

El principal argumento del voto fue la crítica a la actuación del organismo continental en la crisis política que derivó en la salida de Bolivia de Evo Morales. Argentina quedó así del lado de Bolivia, México, Belice, Honduras, Saint Kittis y Nevis, San Vicente y las Granadinas y Santa Lucía que se abstuvieron, frente a una mayoría de 25 votos que votaron a favor. Nicaragua votó en contra.

Estados Unidos, Canadá, Brasil, Uruguay, Chile, Colombia, Paraguay, Perú, Costa Rica, Venezuela (la representación reconocida es la de Juan Guaidó), fueron algunos de los países clave, del total, que impulsaron la resolución, de 34 Estados que integran la OEA.

“Argentina reitera su compromiso irrenunciable con los derechos humanos, que son principios superiores y fundamentales de su política exterior. Argentina ha expresado su preocupación por la detención de personalidades políticas de Nicaragua con su voto en organismos internacionales. Argentina ha repuesto a su embajador en Managua y está convencida de que los informes en el lugar son mucho más completos y precisos que los que se realizan desde fuera de la situación”, inició el discurso Raimundi.

Y agregó: “Argentina rechaza la aplicación de sanciones y cualquier otra medida que pueda exacerbar un conflicto o agravar la situación de los ciudadanos y ciudadanas, generando con esto tensiones adicionales que inclusive pueden ser utilizadas para una mayor intervención externa. Argentina nunca dará por agotados los caminos para restablecer el diálogo con el gobierno y con el pueblo de Nicaragua, pero indudablemente para hacerlo hay que contar con una credibilidad y una legitimidad que la actual conducción de la OEA no tiene, tal cual lo ha expresado entre otros episodios, no el único y quizá el más grave, por su rol en el golpe de estado en Bolivia, por todas estas razones es que justificamos nuestro voto”.

En concreto, la Organización de Estados Americanos (OEA) aprobó una resolución para pedir Ortega que deje entrar al país centroamericano a una misión diplomática para iniciar un diálogo sobre reformas electorales y la convocatoria de nuevas elecciones. La resolución se aprobó con el voto a favor de 25 de los 34 miembros activos de la OEA (Cuba pertenece al organismo pero no participa en este desde 1962), ocho países se abstuvieron, incluidos México, Argentina, Bolivia y Honduras, mientras que Nicaragua fue la única en votar en contra.

La iniciativa insta al Gobierno de Ortega a que “con carácter urgente y como primera medida” ponga en libertad a todos los “presos políticos” y acepte una misión de “buenos oficios” de alto nivel que debe ser autorizada por el Consejo Permanente del organismo.

El mandato de la misión diplomática será llegar a un acuerdo sobre cómo lograr tres objetivos, empezando por una reforma electoral integral. El segundo objetivo de la misión diplomática será revocar todas las leyes que restringen la participación política y limitan los derechos humanos en Nicaragua. Y el tercero será iniciar un diálogo con todos los partidos políticos y otros actores en Nicaragua con el objetivo de celebrar elecciones presidenciales y parlamentarias “tempranas” que sean libres, imparciales y transparentes, con observación internacional “creíble”.

Ortega se impuso de manera fraudulenta el 7 de noviembre en unas elecciones en las que no participaron sus rivales políticos porque, en los meses anteriores, las autoridades disolvieron tres partidos políticos y arrestaron a más de una treintena de dirigentes opositores, entre ellos siete aspirantes presidenciales, incluida Cristiana Chamorro.

En reacción, la Asamblea General de la OEA, el foro político más importante del organismo, aprobó una resolución en la que aseguraba que esos comicios carecieron de “legitimidad democrática” y no fueron ni libres, ni justos, ni transparentes. Acto seguido, el Gobierno de Ortega acusó de “injerencia” a la OEA y anunció que tenía intención de salir del organismo para lo que denunció la Carta de la OEA, su documento fundacional firmado en 1948. Según el reglamento del organismo, cualquier país que denuncia la Carta de la OEA tiene que esperar dos años para que la retirada se haga efectiva.

#### Incumplimiento de la Carta Democrática

Más allá de la misión diplomática, el otro punto clave de la resolución es que resuelve que “Nicaragua no está cumpliendo los compromisos asumidos en la Carta Democrática Interamericana”, aprobada en 2001 para fortalecer y preservar las democracias en el continente americano. Para “restablecer” el cumplimiento de la Carta Democrática Interamericana, la resolución pide al secretario general de la OEA, Luis Almagro, que solicite “con urgencia” una reunión con el Gobierno de Ortega para que permita la entrada al país de la misión diplomática.

Almagro deberá informar a la OEA de sus gestiones a más tardar el 17 de diciembre, según establece la resolución.

La Carta Democrática es un instrumento jurídico que, en sus artículos 20 y 21, contempla trámites diplomáticos contra un Estado miembro donde haya “una alteración del orden constitucional” y, de fracasar esas gestiones, allana el proceso para su suspensión, con lo que dejaría de participar en los programas del organismo. Para aprobar la suspensión, la mayor forma de sanción que tiene la OEA, son necesarios 24 votos, es decir, dos tercios de los 34 países que son miembros activos del organismo. En sus 70 años de historia, la OEA solo ha suspendido a dos Estados: Cuba y Honduras.