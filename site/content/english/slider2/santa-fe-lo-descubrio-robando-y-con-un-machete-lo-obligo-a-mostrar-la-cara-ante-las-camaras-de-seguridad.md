+++
author = ""
date = 2021-12-13T03:00:00Z
description = ""
image = "/images/un-hombre-atrapo-a-un-ladron-lo-puso-de-rodillas-frente-a-un-camara-de-seguridad-y-lo-azoto-con-un-machete-1282920-1.jpg"
image_webp = "/images/un-hombre-atrapo-a-un-ladron-lo-puso-de-rodillas-frente-a-un-camara-de-seguridad-y-lo-azoto-con-un-machete-1282920.jpg"
title = "SANTA FE: LO DESCUBRIÓ ROBANDO Y CON UN MACHETE LO OBLIGÓ A MOSTRAR LA CARA ANTE LAS CÁMARAS DE SEGURIDAD"

+++

#### **_En la provincia de Santa Fe, un hombre descubrió a un joven intentando robar un auto y con un machete lo corrió por la calle hasta capturarlo y obligarlo a pedir perdón y mostrar la cara ante una cámara de seguridad que registró todo el episodio._**

El hecho ocurrió en la madrugada del sábado pasado, en la ciudad de Esperanza, cabecera del departamento Las Colonias, ubicada a unos 38 kilómetros de la capital, donde un delincuente que circulaba en bicicleta por una zona poco transitada vio el vehículo estacionado y decidió cometer el ilícito.

Según se puede apreciar en un primer video grabado por la cámara de seguridad del lugar, al notar que no venía nadie el ladrón frenó, fue caminando hasta el rodado y, sin mayores dificultades, abrió la puerta del lado del conductor, sin darse cuenta de que estaba siendo observado por un vecino del barrio.

Sin dudarlo, el hombre que estaba mirando el accionar de este joven salió de una casa situada en la vereda de enfrente y, sin remera y con un machete en la mano, se fue acerando sigilosamente hasta la parte trasera del auto que estaba siendo asaltado.

Mientras aparentemente el joven seguía intentando encender el vehículo para darse a la fuga, el adulto lo sorprendió dándole un golpe en la espalda con el mango del arma blanca, tras lo cual el ladrón se asustó y comenzó a correr para escapar de allí.

No obstante, el hombre lo persiguió durante algunos metros y logró atraparlo. Cuando lo hizo, lo tomó del cuello del buzo y por la fuerza lo llevó hasta quedar frente a la cámara de seguridad, donde lo obligó a pedir perdón y a mostrar la cara ante el lente para que quede registrada, por lo que se interpreta de las imágenes de una segunda filmación que circuló luego en las redes sociales.

Minutos después salió también a la vereda una mujer que buscó la bicicleta del delincuente y la puso a resguardo, mientras el sujeto del machete le continuó gritando y pegando periódicamente al muchacho, que estaba arrodillado y juntaba las palmas de las manos en señal de arrepentimiento.

De acuerdo con lo que informó el medio local Uno, el hombre era el dueño del auto en cuestión y, tras algunos minutos de tensión y enojo, le pidió al ladrón que juntara todas las cosas que había arrojado mientras intentaba huir y luego lo dejó ir sin dar aviso a la Policía, por lo que no hubo una denuncia ni intervino ninguna fiscalía por este hecho.

En julio pasado, dos grupos de vecinos en los partidos bonaerenses de Pilar y La Matanza protagonizaron dos repudiables intentos de linchamiento a presuntos ladrones.

El primero de ellos ocurrió en el barrio San Jorge del municipio de Pilar, donde varias personas atacaron a dos presuntos delincuentes que habrían intentado robar las pertenencias de unos jóvenes que se habían juntado a jugar al fútbol.

En el video se puede ver el momento en que un vecino tomó por la fuerza a uno de los asaltantes y le sacó una por una las prendas de vestir y el calzado. Cuando quedó completamente desnudo, lo dejaron irse. Su cómplice, en tanto, intentó escapar, pero fue golpeado por los vecinos que lo corrieron y lograron tirarlo al piso.

El segundo hecho ocurrió en la localidad de Isidro Casanova, cuando un adolescente de 16 años fue reducido, golpeado y atado a un poste por vecinos que lo reconocieron y acusaron de haber robado la semana anterior en dos comercios de la zona.