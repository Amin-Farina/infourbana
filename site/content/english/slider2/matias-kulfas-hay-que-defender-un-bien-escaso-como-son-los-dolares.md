+++
author = ""
date = 2021-07-14T03:00:00Z
description = ""
image = "/images/5f2006910bce1_1004x565-1.jpg"
image_webp = "/images/5f2006910bce1_1004x565.jpg"
title = "MATÍAS KULFAS: \"HAY QUE DEFENDER UN BIEN ESCASO COMO SON LOS DÓLARES\""

+++

**_El ministro de Desarrollo Productivo Matías Kulfas defendió hoy la aplicación de nuevas medidas restrictivas en el mercado de cambio y afirmó que tienen como objetivo “administrar un bien escaso como son los dólares”._**

Al finalizar la reunión de gabinete económico que tuvo lugar en la Casa de Gobierno, el titular de la cartera productiva afirmó que las decisiones del Banco Central y de la Comisión Nacional de Valores (CNV) para limitar el mercado de contado con liquidación “son diferentes medidas que se han implementado con un objetivo claro que es administrar de manera eficiente un bien escaso que son los dólares”, mencionó. En realidad, el mundo transita una muy abundante liquidez y la denominada restricción externa no se visualiza, a diferencia de lo que ocurría décadas atrás.

El Gobierno decidió en los últimos días poner en marcha nuevas restricciones cambiarias con las que busca contener la falta de dólares en estos meses preelectorales en los que se esperan fuertes presiones sobre la divisa norteamericana.

En ese sentido, por un lado la CNV definió nuevas trabas para operar con bonos. Específicamente, se definieron cupos nominales por semana estrictos de compra de bonos. El Banco Central, por su parte, prohibió que empresas que tienen varios CUIT puedan utilizarlos de forma tal que con una de esas identificaciones accedan al Mercado Único Libre de Cambios (MULC) y con otra al contado con liquidación.

Kulfas mencionó este miércoles que las medidas tienen como objetivo “poder lograr un equilibrio en mercado de cambios. Hay que recordar que iniciamos el gobierno con un gran desequilibrio y buscamos restablecerlo”, dijo. Y agregó que además de este tipo de medidas apuntan a otras “para la economía real, para que la economía produzca más y que ahorre divisas por importaciones y que pueda aumentar las exportaciones”, concluyó.

Por otra parte, el ministro de Desarrollo Productivo mencionó que la economía va a ser uno de los ejes de la campaña electoral de los próximos meses. “La campaña va a estar centrada en varios temas y uno de esos va a ser la economía. Tenemos un objetivo de reactivación y seguimos en esa línea”, apuntó el funcionario.

“La pandemia golpeó a la economía de todo el mundo pero eso no nos hizo resignar nuestro objetivo. La política industrial está dando resultados y el sector ya produce más que en el último año de Mauricio Macri, crece la construcción y la economía del conocimiento, hubo una buena campaña agroiundustrial. Y esto convive con sectores golpeados” por la pandemia, dijo Kulfas.

En ese sentido, apuntó que “con este avance tan fuerte de la vacunación” el Gobierno espera poder “salir de las restricciones” y que “en los últimos meses del año tengamos una recuperación con todos los sectores de la economía”, enfatizó el ministro.

“Tenemos una economía que funciona a dos velocidades: sectores creciendo y recuperándose, como el sector industrial que muestra un fuerte crecimiento de acuerdo a la demanda de energía. Son sectores en expansión. Y después hay otros golpeados, por eso mantenemos la ayuda con el Repro”, mencionó Kulfas.

El último pago de Repro, que tuvo lugar en los últimos días, asistió a unas 31.500 empresas del sector privado para que puedan cumplir con el pago de sueldos. En total, según estimaciones oficiales, fueron alcanzados 556.000 trabajadores. De ese total, 490.000 fueron empleados en relación de dependencia y otros 70.600, independientes. Implicó para el Estado un gasto de $10.400 millones. La inscripción para el Repro de julio comenzará en los próximos días, anticipó Kulfas.

Respecto a la inflación, el ministro de Desarrollo Productivo afirmó que “en este tiempo hemos recibido un shock de precios internacionales fuerte que impactó en los precios internos, aunque trajo mayores reservas al Banco Central. Tenemos tres meses de caída de la inflación y veremos el dato de junio, pero seguimos trabajando para que haya una tendencia a la baja”, comentó.

Por último, sobre la negociación con el sector de la carne tras el conflicto por el cierre de exportaciones, Kulfas dijo que el Gobierno trabaja “con un sector exportador que va normalizándose con el nuevo esquema que planteamos, lo que buscamos es una armonía para que pueda funcionar el mercado exportador pero protegiendo la mesa de los argentinos”, aseguró, y concluyó que entre fines de junio y principios de julio “vimos una moderación de los precios de la carne”.