+++
author = ""
date = 2021-10-11T03:00:00Z
description = ""
image = "/images/v3uudjgz3vbppo2kigfmycbftq.jpg"
image_webp = "/images/v3uudjgz3vbppo2kigfmycbftq.jpg"
title = "ANÍBAL FERNÁNDEZ BUSCÓ JUSTIFICAR SU AMENAZA A NIK: \"ÉL VIVE AGRAVIÁNDONOS Y NADIE LE DICE NADA\""

+++

##### **_El ministro de Seguridad, Aníbal Fernández, pidió disculpas al dibujante Nik después de contestarle un tuit con un mensaje amenazante en el que daba a conocer la escuela a la que iban sus hijas. “Él vive agraviándonos y nadie le dice nada, pero si lo tomó como una amenaza le pido perdón”, declaró._**

El funcionario buscó minimizar el escándalo. “La redacción es muy clarita. No dice otra cosa que un debate que estaba manteniendo con él por el tema de las subvenciones. Ese debate se agota ahí mismo”, sostuvo Fernández.

Según dijo, luego le avisaron “que él habría dicho en algún lado que sentía que era una amenaza a sus hijos, eso ya sería muy grave”. “Con lo cual no se me caen los anillos, me parece que es obligatorio de uno tener que aclarar estas cosas. Yo jamás me metería con los hijos de nadie. Para mi los hijos, las casas, las mujeres son templos, uno no se mete con esa cosa”, afirmó Fernández.

El funcionario negó que sus mensajes hayan tenido un tono amenazante: “si él lo tomó así, un caballero pide disculpas”, sostuvo e insistió en que “es honesta su disculpa” y que también le mandó un mensaje privado.

El ministro de Seguridad también indicó que “no es la primera vez que debatió con Nik” y que el dibujante “lo ha insultado varias veces”. “Él vive agraviándonos y después borra los mensajes”, aseguró sobre el humorista.

Fernández también desestimó con una ironía los pedidos de renuncia que le llegaron desde la oposición: “Yo le pediría la renuncia a tantos”.

Al concluir la charla, Fernández le resto importancia al asunto: “estamos interpretando un tuit entre todos, cuando la realidad lo que hay que discutir son políticas más preocupantes respecto a lo que nos sucede a nosotros”.