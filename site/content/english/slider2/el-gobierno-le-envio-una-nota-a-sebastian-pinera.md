+++
author = ""
date = 2021-09-06T03:00:00Z
description = ""
image = "/images/sebastian-pinera.jpg"
image_webp = "/images/sebastian-pinera.jpg"
title = "EL GOBIERNO LE ENVIÓ UNA NOTA A SEBASTIÁN PIÑERA"

+++

##### **_La Argentina le advirtió a la administración del país vecino que la “actualización” de los límites de su plataforma continental desconoce tratados._**

En un nuevo capítulo de la tensión de la Argentina con Chile por la plataforma continental, el Gobierno le envió una nota a la administración de Sebastián Piñera en rechazo de lo que llamó la “vocación expansiva” del país vecino. La Argentina advirtió que la “actualización” de los límites de Chile es contraria al Tratado de Paz y Amistad de 1984 y a la Convención de las Naciones Unidas sobre el Derecho del Mar de 1982.

Cancillería expresó su “preocupación” por la “pretensión” del país limítrofe “de proyectar plataforma continental al Este del meridiano 67º 16, 0″. Advirtió que de esta manera se avanzaría sobre la plataforma continental argentina y se superpondría “con una gran extensión de fondos marinos y oceánicos que forman parte del Patrimonio Común de la Humanidad”.

El ministerio de Relaciones Exteriores criticó lo que llamó “una vocación expansiva que la Argentina se ve obligada a rechazar”. Planteó que el reclamo es “manifiestamente extemporáneo y contradictorio con la conducta de Chile previa a mayo de 2020″ y que “desconoce lo dispuesto por las normas internacionales aplicables cuya interpretación de buena fe es exigida por el derecho internacional”.

En la nota el organismo a cargo de Felipe Solá mencionó las presentaciones ante la Comisión de Límites de la Plataforma Continental (CLPC) hace más de diez años. Cancillería explicó que se determinó que la demarcación del límite de la plataforma continental argentina “guarda plena conformidad con las normas de la Convención de las Naciones Unidas sobre el Derecho del Mar de 1982 y el Tratado de Paz y Amistad Argentino-Chileno de 1984″.

#### Felipe Solá adelantó que pueden “llegar a recurrir a un tribunal” por la plataforma continental

La semana pasada el canciller Felipe Solá expuso ante la Comisión de Relaciones Exteriores y Culto del Senado junto al secretario de Malvinas, Antártida y Atlántico Sur Daniel Filmus. Adelantó que pueden “llegar a recurrir a un tribunal” para resolver las diferencias con el país vecino.

Planteó que “no hay otra manera” de resolver la controversia con Chile que no sea las de la vía de las negociaciones y que “no existe una tercera manera”. Mencionó que hubo “varias controversias limítrofes” entre los dos países que fueron “laudadas de distintas maneras”.

#### La medida que generó la tensión con Chile

Las diferencias surgieron por la actualización que hizo Chile a través de dos decretos de los espacios de soberanía marítima en la Zona Austral, con la proyección de la plataforma continental a partir de las 200 millas náuticas desde las Islas Diego Ramírez. El Gobierno acusó a la administración de Piñera de “pretender apropiarse de una parte de la plataforma continental argentina”.

El 11 de marzo de 2016 y 17 de marzo de 2017 la Comisión de Límites de la Plataforma Continental (CLPC), organismo creado por la Convención de las Naciones Unidas sobre el Derecho del Mar (CONVEMAR), aprobó las Recomendaciones sobre la presentación argentina del límite exterior de la plataforma continental realizada el 21 de abril de 2009.

En agosto del año pasado el gobierno de Alberto Fernández promulgó la ley vinculada con los Espacios Marítimos e Insulares, que define las coordenadas del límite exterior de la plataforma continental Argentina e insular más allá de las 200 millas. De acuerdo al Ejecutivo la delimitación decretada por Chile incluye áreas marítimas que fueron oficializadas como propias por parte de la Argentina ante la Organización de las Naciones Unidas (ONU).