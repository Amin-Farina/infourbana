+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/campo-vs-yegua.jpg"
image_webp = "/images/campo-vs-yegua.jpg"
title = "EL CAMPO IRÁ A PARO DESDE EL JUEVES EN RECHAZO AL CEPO A LA CARNE"

+++

**_Las cuatro entidades agropecuarias que conforman la mesa de enlace decidieron lanzar un cese de comercialización desde este jueves, en rechazo el cierre de las exportaciones de carne vacuna, por 30 días, anunciada por el Gobierno._**

Así lo definieron los titulares de Sociedad Rural, Confederaciones Rurales; Coninagro y Federación Agraria (FAA) que realizaron un encuentro virtual de urgencia en medio del creciente malestar entre los productores ante lo que muchos consideran “una declaración de guerra” hacia el sector.

Con varias entidades regionales en estado de alerta y movilización, los referentes nacionales de los productores salieron con un paro comercial ante lo que consideran una “nefasta” decisión que, a su criterio, no solo no contendrá los precios internos sino que en el mediano y largo plazo derivará en menor oferta productiva y, por ende, alza en los valores al consumidor.

Según pudo saber Tn.com.ar, la industria frigorífica de exportación mantiene este martes contactos con distintos funcionarios de Poder Ejecutivo con el objetivo de encontrar mecanismos que permitan mantener abierta la actividad y cumplir con los compromisos asumidos con el exterior.

En la noche del lunes, apenas conocida la decisión oficial, desde CRA se alertó que el cierre de ventas al exterior es una “marcha atrás para el desarrollo y crecimiento de la ganadería” y que deja a los productores en “camino a un cese de comercialización”.

Daniel Pelegrina, presidente de SRA, afirmó, a su turno que “el cierre de exportaciones de carne por 30 días es un error y un paso atrás en todo sentido. Causará un daño irreparable a un sector productivo que ha demostrado que genera empleo y actividad en todo el territorio nacional. La decisión destruye la imagen de Argentina como proveedor confiable y volveremos a regalarle los mercados a nuestros principales competidores”.

Para Carlos Achetoni, titular de FAA, la decisión del Gobierno un “descontento muy fuerte” en el sector y llegan “pedidos de distintos puntos del país para que se tomen medidas” en rechazo de la medida oficial.