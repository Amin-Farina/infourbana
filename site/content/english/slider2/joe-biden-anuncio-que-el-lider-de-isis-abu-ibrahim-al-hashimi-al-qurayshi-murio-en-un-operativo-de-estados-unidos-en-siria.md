+++
author = ""
date = 2022-02-03T03:00:00Z
description = ""
image = "/images/do4tdmbap5gcban3wunqzrcuv4.jpg"
image_webp = "/images/do4tdmbap5gcban3wunqzrcuv4.jpg"
title = "JOE BIDEN ANUNCIÓ QUE EL LÍDER DE ISIS, ABU IBRAHIM AL-HASHIMI AL QURAYSHI, MURIÓ EN UN OPERATIVO DE ESTADOS UNIDOS EN SIRIA"

+++

##### **_Lo informó el mandatario esta mañana a través de Twitter; según un alto funcionario de la Casa Blanca, el terrorista se inmoló ante la llegada de las fuerzas especiales_**

Las fuerzas especiales de Estados Unidos lanzaron una ofensiva contra jihadistas en el noroeste Siria, una operación antiterrorista que fue calificada como exitosa, en la que murió el líder del Estado Islámico (ISIS, por sus siglas en inglés), Abu Ibrahim al-Hashimi al- Qurayshi, informó hoy el presidente Joe Bien.

El operativo dejó además 13 muertos, entre ellos seis niños y cuatro mujeres, según señalaron los servicios de emergencias que acudieron al lugar.

> “Anoche, bajo mi dirección, las fuerzas militares estadounidenses llevaron a cabo con éxito una operación antiterrorista. Gracias a la valentía de nuestras Fuerzas Armadas, hemos eliminado del campo de batalla a Abu Ibrahim al-Hashimi al Qurayshi, el líder de ISIS”, informó el presidente de Estados Unidos, Joe Biden, en su cuenta de Twitter, y señaló que más tarde dará una conferencia de prensa para dar detalles del operativo.

Este fue el mayor operativo en la provincia de Idlib -controlada por rebeldes-, desde el asalto estadounidense que acabó con la vida del líder del grupo extremista Estado Islámico, Abu Bakr al-Baghdadi, en 2019, durante la presidencia de Donald Trump. Tras su asesinato, el 31 de octubre de ese año, al-Qurashi asumió el cargo al frente de ISIS.

Según el diario estadounidense The New York Times, un alto funcionario de la administración de Biden afirmó que el terrorista se inmoló tras detonar una bomba que habría terminado con su vida y la de su familia.

“Poco se sabe sobre al-Qurayshi, quien sucedió a al-Baghdadi, o la estructura de mando superior de ISIS. Pero los analistas dijeron que la muerte del líder del Estado Islámico fue un golpe significativo para el grupo terrorista”, agrega el diario.

Biden señaló en el comunicado que las fuerzas estadounidenses implicadas en la redada volvieron a sus bases sin sufrir bajas, antes de agregar que próximamente realizará declaraciones públicas para dar detalles sobre la operación. “Que Dios proteja a nuestras tropas”, remarcó.

“La misión fue un éxito”, afirmó el secretario de Prensa del Pentágono, John Kirby, en un breve comunicado. “No hubo víctimas estadounidenses. Se ofrecerá más información a medida que esté disponible”.

ISIS, que intenta resurgir, realizó una serie de ataques en la región, entre ellos un asalto de 10 días a fines del mes pasado para tomar una prisión.

##### El operativo

Los efectivos estadounidenses aterrizaron en helicópteros y asaltaron una vivienda en un rincón de Siria en poder de los rebeldes y se enfrentaron durante dos horas con hombres armados, dijeron testigos. Según sus relatos, hubo continuos disparos y explosiones que despertaron a la tranquila localidad de Atmeh, cerca de la frontera turca, una zona repleta de campos para desplazados por la guerra civil siria.

Idlib está controlada en su mayoría por combatientes respaldados por Turquía, pero también es un feudo de Al-Qaeda y varios de sus altos cargos residen allí. Otros insurgentes, incluyendo miembros de la milicia rival Estado Islámico, también se han refugiado en la región.

“Los primeros momentos fueron aterradores, nadie sabía qué estaba pasando”, dijo Jamil el Deddo, residente en un campo de refugiados próximo. “Todos estábamos preocupados porque pudiese ser la aviación siria, lo que nos recordó las bombas de barril que solían arrojarnos”, agregó, en referencia a los contenedores de cargados de explosivos empleados por las fuerzas del presidente Bashar al-Assad contra los opositores durante el conflicto civil.

La planta superior de la vivienda en donde transcurrieron los hechos quedó prácticamente arrasada tras la incursión, que derribó el tejado y las paredes. Se podían ver manchas de sangre en las paredes y en el piso de la estructura que quedó en pie, con un dormitorio destrozado con una cuna de madera en el suelo.

Por el momento, el Pentágono no dio detalles sobre el número de víctimas. Según el Observatorio Sirio para los Derechos Humanos, un grupo opositor al gobierno sirio con sede en Gran Bretaña que monitorea el conflicto, el operativo dejó 13 fallecidos, entre ellos cuatro niños y dos mujeres.

En tanto, la Defensa Civil Siria, un grupo de emergencias gestionado por la oposición llamado también Cascos Blancos, contabilizó 13 personas fallecidas en los bombardeos y enfrentamientos posteriores al asalto. Su recuento incluía seis menores y cuatro mujeres.

El Fondo de las Naciones Unidas para la Infancia (Unicef) declaró que al menos seis niños fueron asesinados por “la fuerte violencia” del operativo.

Omar Saleh, residente de una casa cercana, contó que sus puertas y ventanas comenzaron a vibrar con el sonido de los aviones volando bajo cerca de la 1 de la madrugada. Después, escuchó a un hombre que hablaba árabe con acento iraquí o saudí, pedir a las mujeres, a través de un altavoz, que se rindieran o se marcharan del lugar.

“Esto se prolongó 45 minutos. No hubo respuesta. Entonces, comenzaron los disparos de ametralladoras”, añadió Saleh, señalando que esa situación duró dos horas mientras los aviones sobrevolaban la zona a baja altura.

Un funcionario estadounidense señaló que uno de los helicópteros del operativo sufrió un problema mecánico y tuvo que ser volado en tierra.

La operación se produjo en un momento en el que Estado Islámico estaba reafirmándose, perpetrando algunos de sus mayores ataques desde su derrota en 2019. En las últimas semanas y meses, la milicia radical lanzó una serie de operaciones en la región, incluyendo un asalto de 10 días para tomar una prisión en el noreste de Siria, con al menos 3000 detenidos del grupo, a finales de enero.