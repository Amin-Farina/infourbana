+++
author = ""
date = 2021-06-08T03:00:00Z
description = ""
image = "/images/controles-precios-maximos-san-rafael-1-1.jpeg"
image_webp = "/images/controles-precios-maximos-san-rafael-1.jpeg"
title = "EL GOBIERNO PRESENTARÁ UNA CANASTA DE 70 PRODUCTOS CON PRECIOS CONGELADOS "

+++

En medio de la aceleración de la inflación que se desató en marzo y no logró morigerarse en los meses siguientes el Gobierno presentará esta tarde una nueva canasta de 70 productos esenciales a precios acordados con 24 empresas. El nuevo programa se llamará “Super Cerca” y tendrá como particularidad que los productos estarán etiquetados como un intento por frenar las remarcaciones.

Según un comunicado oficial, este martes a las 16 horas el ministro de Desarrollo Productivo, Matias Kulfas, y la secretaria de Comercio Interior, Paula Español, ofrecerán una conferencia de prensa para el lanzamiento del programa Súper Cerca.

Durante la conferencia se va a anunciar un acuerdo de precios fijos por seis meses e impresos en los envases.

“Esta primera etapa abarcará a 70 productos que se van a comercializar en los comercios de cercanía de todo el país. Es un acuerdo con 24 empresas del sector, con el objetivo de cuidar la canasta familiar”, detallaron voceros de Desarrollo Productivo.

El anuncio será en la Sala de Conferencia de Casa Rosada.

El Índice de Precios al Consumidor (IPC) que elabora el Indec aceleró a 4,8% mensual en marzo pasado, por encima de las previsiones más pesimistas, y el pico en el avance del nivel general de precios se mantuvo en los meses siguientes. En abril, el propio Indec registró un 4,1% mensual mientras que el dato de mayo todavía no se conoce pero se estima en el sector privado en torno al 3,8%.

Con el dato oficial de abril, la inflación interanual aceleró al 46,3% y las previsiones privadas para 2021 rondan el 50%.

Con medidas tradicionales como suba de tasas descartadas, el Gobierno reacciono principalmente con una desaceleración del ritmo de deslizamiento del dólar oficial pero si la medida tiene efecto llegará con el tradicional lag de al menos 6 meses de las decisiones de política monetaria. En el mientras tanto, distintas áreas del Gobierno buscan acuerdos de precios y regulaciones que frenen el avance aunque sea en el corto plazo. O que generen canastas alternativas para las familias.

El más reciente fue el cierre de las exportaciones de carne en un intento por acordar con los frigoríficos un freno a los avances de precios del sector. El precio de la carne avanzó 6,1% en mayo según el último informe del Instituto de Promoción de la Carne Vacuna Argentina (IPCVA) y en términos interanuales sube 75%.

Hoy se termina el programa Precios Máximos, el más resistido por los empresarios del comercio minorista. El programa que comenzó en los primeros días de la pandemia y que implicó dejar fijos los precios de miles de productos de distintos rubros, desde bienes de primera necesidad hasta otros fuera de la canasta básica, lleva más de un año y tres meses en vigencia.

En el sector privado veían la negociación alrededor de Super Cerca como una superación de ese programa, aunque el final de Precios Máximos no fue oficializado.

Según pudo reconstruir en base a fuentes privadas y oficiales, en las últimas semanas las dos partes avanzaron en prácticamente todos los aspectos del nuevo acuerdo de precios, incluyendo qué productos estarán en las góndolas, la señalización obligatoria con el valor acordado en los envases de cada producto, la duración y presencia en superficies comerciales.

La nueva canasta tendrá alimentos básicos, tanto de primeras como de segundas marcas. Estarán incluidos artículos como fideos, arroz, aceite, lácteos, polenta y yerba, entre otros. Y en el rubro de limpieza, otros como de higiene y cuidado personal, lavandinas o jabones.

La intención oficial es que los 70 productos se ofrezcan en supermercados chinos y comercios barriales de cercanía, para conseguir una “capilaridad” mayor que Precios Cuidados, el esquema más representativo entre los vigentes para mantener a raya los valores en las góndolas, que está presente en supermercados y que representa el 35% del consumo, según estimaciones oficiales.