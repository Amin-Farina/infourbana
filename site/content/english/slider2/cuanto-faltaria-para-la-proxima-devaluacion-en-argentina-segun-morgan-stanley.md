+++
author = ""
date = 2021-06-22T03:00:00Z
description = ""
image = "/images/devalueta.jpg"
image_webp = "/images/devalueta.jpg"
title = "¿CUÁNTO FALTARÍA PARA LA PRÓXIMA DEVALUACIÓN EN ARGENTINA SEGÚN MORGAN STANLEY? "

+++

**_El banco de inversión Morgan Stanley estimó que la Argentina podría concretar una devaluación más acelerada del peso a inicios de 2022, tras las elecciones legislativas._**

Sin mencionar un porcentaje, la entidad financiera cree que se podría repetir la estrategia de 2014, cuando el ex titular el Banco Central, Juan Carlos Fábrega, permitió un ajuste del 15% en el tipo de cambio apenas tres días.

En un reporte difundido este lunes, Morgan Stanley indicó: "Con las elecciones (legislativas) aplazadas hasta mediados de noviembre, ya no vemos un ajuste único del tipo de cambio oficial este año".

"Sospechamos que es una decisión política difícil de tomar, ya que un movimiento único del tipo de cambio en noviembre o diciembre se traduciría en un pico de inflación antes de las vacaciones de fin de año, generalmente un período sensible desde una perspectiva social", según detalla en el documento.

Por otro lado, desde Morgan Stanley añadieron: "Nuevamente, es el buen desempeño en la primera mitad del año lo que nos lleva a pensar que la estrategia de confusión podría extenderse hasta principios de 2022, sin una gran sorpresa negativa en la dinámica de Covid-19 o una reversión considerable en las condiciones de liquidez global".

En ese sentido, el documento consideró que, al igual que en 2014, Morgan Stanley espera cualquier movimiento único del tipo de cambio oficial se produzca a principios de 2022, antes de un eventual programa del FMI. 

"La magnitud del movimiento es difícil de predecir, pero es razonable esperar algo similar al de 2014", detalló.

El informe -firmado por los economistas Fernando Sedano y Lucas Almeida- señaló además que "continuamos viendo un programa con el FMI (en algún momento entre fines del primer trimestre o comienzo del segundo de 2022), que aborde algunos problemas macro y ataque los problemas estructurales".

"Sospechamos que al FMI le gustaría ver una brecha cambiaria más pequeña para cuando entre en vigor un nuevo programa. Además, que aumenten las reservas y que haya una reducción gradual de los controles al capital y a las importaciones deberían ser características clave del eventual programa de facilidades extendidas", añade el documento del banco de inversión.

Morgan Stanley sostuvo también: "Esperamos que las autoridades estén de acuerdo en la necesidad de consolidar las cuentas fiscales, aunque es poco probable que esto se logre con una reducción considerable del gasto, ya que este Gobierno es propenso a una mayor intervención estatal". 

"Dada la necesidad de reducir la brecha fiscal, no esperamos una relajación de la carga fiscal del país, lo que no augura nada bueno para la inversión".

Asimismo, el informe consigna que "cuanto más sólida sea la posición de las reservas, menos apremiante será el acuerdo del FMI para las autoridades". 

Desde Morgan Stanley también observaron que el Gobierno cree que la confianza del mercado no se recuperará ni siquiera bajo un marco macroeconómico sellado por el FMI y, por lo tanto, una menor independencia de las políticas, bajo un programa del organismo, "es de hecho una alternativa más riesgosa", para el banco.

"Si bien creemos que habrá un acuerdo y un programa blando, no podemos descartar la posibilidad de que no haya acuerdo y la Argentina entre en mora con el FMI. Este sería el escenario de autarquía, en el que el Gobierno, con un colchón de reservas de divisas mayor al esperado, intenta profundizar las políticas poco ortodoxas y posponer un reequilibrio macroeconómico. Consideramos esto arriesgado, ya que las reservas de divisas han mejorado, pero no a niveles que justificarían el éxito en la autarquía", concluyó el banco.