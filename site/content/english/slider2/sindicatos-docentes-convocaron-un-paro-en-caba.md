+++
author = ""
date = 2021-04-14T03:00:00Z
description = ""
image = "/images/ctera.jpg"
image_webp = "/images/ctera.jpg"
title = "SINDICATOS DOCENTES CONVOCARON UN PARO EN CABA"

+++

Sindicatos docentes de la Ciudad de Buenos Aires convocaron hoy a un paro de actividades para exigir la suspensión “temporal” de las clases presenciales en las escuelas por el crecimiento exponencial de casos de coronavirus y la alarma en la ocupación de las camas de terapia intensiva.

La medida de fuerza fue impulsada por Ademys, a la que horas después se le sumó UTE-Ctera. Se trata de los dos gremios con mayor cantidad de afiliados en la Capital Federal, por lo que se espera que efectivamente no haya presencia en las aulas porteñas en muchos establecimientos.

“Exigimos a Horacio Rodríguez Larreta la suspensión temporal de la presencialidad en CABA. La salud y la vida son prioridad”, planteó UTE-CTERA en la convocatoria al paro de este miércoles.

Las organizaciones gremiales vinculan la suba de los casos a la apertura de las escuelas, y sostienen que aunque haya un respeto de los protocolos en los establecimientos, la asistencia a clases presenciales genera que un millón más de personas estén en la calle, lo que implica una mayor presión sobre el transporte público.

El secretario gremial de CTERA y secretario general adjunto de UTE, Eduardo López, señaló que el reclamo de “la suspensión temporal de las clases presenciales en la ciudad de Buenos Aires hasta que deje de crecer esta locura de contagios y haya camas (de terapia intensiva) en la Ciudad”. “Lo mejor es la presencialidad, sin dudas, pero en pandemia fue mejor la experiencia a distancia del año pasado, los chicos estudian más, se contagian menos y hay más camas”, comentó el dirigente sindical.

La convocatoria a la huelga se enmarca en un reclamo generalizado realizado por la Confederación de Trabajadores de la Educación de la República Argentina (Ctera), que solicitó al Gobierno nacional y a las carteras educativas provinciales “la suspensión temporal de la presencialidad en aquellos distritos donde el aumento de casos ha sido exponencial en los últimos 14 días”. El ministro de Educación de la Nación, Nicolás Trotta, aseguró durante la semana que se mantendrá la modalidad presencial y que los contagios no se producen en las instituciones escolares.

En la Ciudad, las escuelas acumularon más de dos mil contagiados entre docentes, no docentes y alumnos. Sin embargo, el gobierno porteño mantiene una postura firme en relación a la presencialidad. El 17 de abril se cumplirán dos meses desde el inicio del ciclo lectivo. Los nuevos datos de contagios, revelan un aumento de los casos en sintonía con el rebrote general de la segunda ola, pero una muy baja positivización que, según el gobierno porteño, indica que en los colegios no se está reproduciendo el coronavirus.

En comparación con el primer mes, solo se había contagiado el 0,17% de los docentes y alumnos, entre el 17 de marzo y el 12 de abril se infectó el 0,71% de la comunidad educativa, en línea con la curva ascendente en la Ciudad. De los casos que se aislaron preventivamente por contacto estrecho dentro de una misma burbuja, dio positivo posteriormente solo el 0,012 por ciento.

Hay una coincidencia entre la gestión de Horacio Rodríguez Larreta y de Alberto Fernández. Los funcionarios a cargo de las carteras educativas concluyen que los casos se generan fuera de las aulas. La diferencia es que el ministro Nicolás Trotta exige que se “restrinjan aquellas actividades que no son prioritarias” para que se mantengan las clases presenciales.

“La escuela o el trabajo con ámbitos en los que el cumplimiento de los protocolos ha demostrado que se puede reducir el índice de contagios; las actividades sociales que hacemos después de la escuela o el trabajo son las que nos exponen a mayores riesgos de contagio”, señaló el ministro de Educación.

En UTE-Ctera y Ademys la relación con el gobierno de la Ciudad es de abierta confrontación. Consideran que, bajo la modalidad de clases a distancia, “no había colapsado el sistema de salud de la Ciudad” en comparación con la actualidad, que “no hay camas”; y que tampoco se cumplen debidamente los protocolos en las escuelas. Además, apuntan un fenómeno en crecimiento, donde cada vez se detectan más interrupciones de las clases presenciales por los “aislamientos de las burbujas”.

En la provincia de Buenos Aires, la situación es diferente. Con protocolos y requisitos más flexibles para que se implemente la presenciales, los sindicatos docentes mantienen los canales institucionales abiertos con el gobernador Axel Kicillof. Sin embargo, la Unión de Docentes de la provincia de Buenos Aires (Udocba), uno de los gremios que integra el Frente de Unidad Docente (FUDB), ya convocó a un paro de 24 horas para este miércoles en reclamo de la suspensión de las clases presenciales.

El jueves habrá una reunión convocada por el gobierno bonaerense, junto a que un comité de crisis integrado por autoridades sanitarias, educativas y sindicatos docentes, donde se evaluará si en los distritos más afectados por el coronavirus se suspenden o reducen las clases presenciales.