+++
author = ""
date = 2022-02-15T03:00:00Z
description = ""
image = "/images/1644920481973mayans.jpg"
image_webp = "/images/1644920481973mayans.jpg"
title = "GRIETA OFICIALISTA: JOSÉ MAYANS, JEFE DEL BLOQUE DEL FDT EN EL SENADO ADVIRTIÓ QUE NO VOTARÁ EL ACUERDO CON EL FONDO A LIBRO CERRADO"

+++

##### **_Luego de la renuncia de Máximo Kirchner, en disidencia a la negociación con el Fondo, José Mayans también planteó reparos y le exigió a Guzmán conocer “los detalles del acuerdo para saber en qué compromiso vamos a meter al país”_**

El Frente de Todos se encuentra ante el desafío de subsistir con la menor cantidad de heridas posibles debido a las diferencias que produce al interior de la coalición el la negociación con el Fondo Monetario Internacional. Luego de la renuncia de Máximo Kirchner a la presidencia del bloque en Diputados, ahora fue José Mayans, el titular de la bancada oficialista en el Senado, quien puso reparos al apoyo del acuerdo.

Alberto Fernández continúa diagramando la estrategia para que con un amplio consenso el Congreso apruebe la Ley de Fortalecimiento de la Sostenibilidad de la Deuda Pública. El ministro de Economía, Martín Guzmán, se encarga de definir la letra chica con el staff del FMI, mientras el presidente de la Cámara Baja, Sergio Massa, busca acercar posiciones en el oficialismo y mantiene el diálogo con la oposición.

En los próximos días el Poder Ejecutivo deberá definir por donde ingresa el acuerdo al Congreso. “Es más conveniente que el debate se inicie en Diputados por las circunstancias y las visiones que hay con respecto al endeudamiento público”, señaló Mayans, quien advirtió que “el FMI tiene requerimientos para el Banco Central, sobre el déficit, pide la suba de tarifas y la elevación de las tasas” y “habrá que ver cómo impacta eso en la economía”.

En diálogo con El Destape Radio, el presidente del bloque de senadores del Frente de Todos reveló que le pidió a Sergio Chodos, representante argentino ante el Fondo y a Guzmán “los detalles del acuerdo para saber en qué compromiso vamos a meter al país”. Mayans aclaró que “confía” en el ministro y en su “buena fe”, pero “el pueblo argentino nos votó como representantes para hacer las cosas bien, no meternos en algo que no vamos a poder soportar”.

En este marco, el senador expresó que en el Frente de Todos “hay disidencias sobre si el compromiso es sostenible” y manifestó que la postura que adoptará la coalición oficialista en la votación se definirá “cuando nos reunamos”.

Además “hay que discutir el monitoreo trimestral que quiere hacer el Fondo Monetario”. “Después de lo que hicieron, quieren someternos a un monitoreo trimestral para que ellos conduzcan prácticamente la economía del país”, cargó contra el organismo multilateral de crédito.

> “Argentina no puede afrontar el compromiso de los dos pagos de 20 mil millones de dólares”, advirtió el legislador que señaló que “hubo irresponsabilidad tanto de parte del FMI como de parte de Mauricio Macri”. “El nivel de deuda que dejó el Gobierno de Macri fue desastroso, deja una deuda de 320 mil millones de dólares más una proyección de 100 mil millones de intereses”, agregó.

“Hay una deuda que es ilegítima e ilegal porque no ha pasado por el Congreso”, continuó criticando a la gestión de Cambiemos: “Esa plata entró y salió del país, y ahora todo el mundo tendrá que pagar la ineptitud y la impericia del Gobierno de Macri”.

En ese sentido apuntó contra “la irresponsabilidad” de la oposición. “En las últimas sesiones, Juntos por el Cambio lo único que hizo fue dejarnos sin quórum, es difícil trabajar así; el país está sin presupuesto y sin previsión”.

Los reparos que marcó Mayans sucedieron en la misma jornada que un grupo de senadores amenazó con abandonar el bloque del Frente de Todos. Son el entrerriano Edgardo Kueider, el correntino Camau Espínola, el jujeño Guillermo Snopek y el salteño Sergio “Oso” Leavy, quienes se quejaron públicamente por la falta de discusión interna en la bancada que preside Mayans y que conduce Cristina Kirchner.

> “El costo de no pagarle al FMI significa entrar en default y eso tendrá un impacto mayor desde el punto de vista social; el gesto de Máximo Kirchner generó temor en los mercados y puso tensión nuevamente sobre el dólar”, precisó el senador Kueider en diálogo con Radio Mitre.

“Cuando el Presidente envíe al Congreso el acuerdo con el FMI, tenemos que aprobarlo y no permitir que la Argentina caiga en una situación de caos e incendio económico”, sentó posición el legislador.

Pese a este panorama de discrepancias internas, en el Frente de Todos estiman que la votación se llevará a cabo en los primeros días de marzo. Más allá de la voluntad de los legisladores, la fecha del debate está atada a la aprobación del board del FMI.