+++
author = ""
date = 2022-01-31T03:00:00Z
description = ""
image = "/images/arton161655-1.jpg"
image_webp = "/images/arton161655.jpg"
title = "UN LADRÓN QUISO ASALTAR A DOS JUBILADOS, DISPARÓ SU ARMA POR ERROR Y SE MATÓ"

+++

##### **_Un ladrón intentó este domingo asaltar a una pareja de jubilados en Villa Bosch, en el partido bonaerense de Tres de Febrero. El atraco terminó en tragedia para el delincuente, quien disparó accidentalmente el arma que usaba para robar y murió._**

Según informaron portales de noticias locales, el hecho ocurrió hoy a la una de la madrugada, cuando la pareja regresaba a su casa en un Ford Focus. En el momento en el que se disponían a ingresar a su domicilio, en la calle 17 de Agosto al 1600, la pareja -de 79 y 78 años- fue abordada por dos ladrones que se habían escondido en el patio delantero de la vivienda.

El asalto derivó en un forcejo entre el jubilado y uno de los delincuentes, quien comenzó a propinarle culatazos en la cabeza del hombre con su arma hasta que la pistola se disparó accidentalmente y el ladrón terminó con un balazo en su pierna izquierda.

Acto seguido, los dos delincuentes se dieron a la fuga, pero tras recorrer unos 70 metros el malhechor baleado se desplomó en la calzada y murió. El cómplice, hasta este momento, se mantiene prófugo.

Efectivos de la comisaría de Villa Bosch y del Comando de Patrullas de Tres de Febrero se hicieron presentes en el lugar. Según informaron portales de noticias locales, los peritos de la Delegación San Martín de la Policía Científica determinaron que el delincuente falleció debido a que la bala le seccionó la arteria femoral.

Los efectivos policiales hallaron en la escena del hecho una pistola Llama modelo Max, calibre 40, y una mochila en cuyo interior había un revólver Smith & Wesson calibre 32 largo. En el patio delantero de la casa de las víctimas, además, se encontró una escopeta Rexio calibre 12/70 sin numeración, cargada con dos cartuchos con postas de goma y tres con munición de guerra.

El jubilado agredido, en tanto, debió ser asistido por las heridas que recibió, producto del forcejeo con el delincuente. Tras las curaciones, se encuentra fuera de peligro, según se informó.

La causa fue caratulada como “homicidio y robo agravado por el uso de arma de fuego en grado de tentativa” y encabeza la investigación el fiscal Fabricio Donato Ióvine, de la Unidad Funcional de Instrucción (UFI) N°1 de San Martín.