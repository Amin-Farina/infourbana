+++
author = ""
date = 2021-10-20T03:00:00Z
description = ""
image = "/images/precios_congelados_x1x_crop1634693050415-jpg_1207600405.jpg"
image_webp = "/images/precios_congelados_x1x_crop1634693050415-jpg_1207600405.jpg"
title = "PRECIOS CONGELADOS POR DECRETO: LAS EXIGENCIAS DEL GOBIERNO A LAS EMPRESAS Y COMERCIANTES"

+++

#### **_Finalmente, el congelamiento de precios se regirá por una resolución, luego de que ayer no hubiera acuerdo entre el Gobierno y los fabricantes de alimentos, que denunciaron que “no hay condiciones para acordar” y que la inflación no sube por sus productos._**

Tras la tensión generada entre las partes luego de un segundo encuentro con las empresas y una segunda lista de 1.650 productos (la primera había sido de 1245), se concretó ayer por la tarde una reunión de urgencia entre el titular de Coordinadora de Industrias de Productos Alimenticios (Copal), Daniel Funes de Rioja; el director ejecutivo de la Asociación de Supermercados Unidos, Juan Vasco Martínez, y el secretario de Comercio Interior, Roberto Feletti, con el objetivo de reencausar la negociación.

Pero el clima no fue el mejor. El funcionario estaba molesto por el comunicado que Copal había enviado a los medios horas antes, y no hubo acuerdo. Feletti les informó que al Gobierno le urgía emitir una resolución, aunque les aseguró que continuaría con el diálogo. Finalmente, los artículos a precio congelado serán 1.432, número al que se llegó luego de considerar correcciones que le hicieron llegar algunas firmas, pero se trató de apenas un tibio retoque. Esta situación tensó más aún la relación de los empresarios de consumo masivo con Feletti, quien recibió advertencias de Funes de Rioja sobre una posible judicialización de la norma.

Según los empresarios, la nueva lista contempla valores que son mucho más bajos que los que regían el 1° de octubre. Así lo advirtieron diferentes miembros de la Copal y de las cámaras que agrupan a firmas de cosmética, higiene personal y limpieza.

El listado oficial corrobora que los precios de los 654 ítems que son parte del programa Precios Cuidados mantienen el precio acordado hace tres meses, por lo que no incorpora los incrementos de octubre concedidos por la ex titular de Comercio Interior, Paula Español. Además, incluye los 70 que integran “Súper Cerca” y los otros 708 son nuevos y contemplan reducciones de entre 10% y 20%, en la mayoría de los casos, pero con casos puntuales de rebajas de hasta el 50%. Es el caso, por ejemplo, del dulce de leche repostero marca La Serenísima de 400 gramos, que cuesta hoy en góndola $ 201,10 (precio en una cadena) y en la lista del Gobierno figura que debe venderse hasta enero a $116,37, es decir, 42% más barato y sólo $4 por encima del dulce de leche La Armonía, que está en Precios Cuidados.

También se detectaron diferencias importantes en el paquete de 50 unidades de mate cocido en saquitos marca Unión; en el té en saquitos Green Hills de 25 unidades, y en el litro de shampoo marca Plusbelle, en donde las diferencias entre el listado oficial y los precios actuales de góndola son de 40%, 33% y 40%, respectivamente. Son apenas meros ejemplos, que según las compañías se replican en muchos otros ítems elegidos por el Gobierno.

Hay otros, sin embargo, que tienen rebajas de entre 10 y 25%, los porcentajes que subas que el secretario Roberto Feletti anticipó que habían tenido muchos productos durante los primeros 18 días de octubre. “Hay vocación de acuerdo, pero firmeza en la necesidad de concretarlo porque la aceleración de los precios de la canasta básica es un tema que preocupa sumamente al Gobierno Nacional. Entre el 1° y el 18 de octubre, la canasta del supermercado aumentó 2,2%, con productos que registraron aumentos de entre 10 al 25%”, señaló Feletti luego del encuentro que mantuvo este lunes con las principales empresas de consumo masivo.

De los 20 ítems analizados por este medio, sólo dos reflejan bajas menores al 10%. Uno es el paquete de 1 kilo de sal gruesa marca Celusal, que muestra un recorte en el precio actual de góndola del 4%, y otro es la botella de 1,65 litros de agua sin gas Villa del Sur, donde el Gobierno propuso una reducción del 2% versus su valor vigente. A su vez, productos como la Coca-Cola regular de 2,25 litros debería bajar 15% su precio, mientras que la Coca-Cola light de 1,5 litro, 10%. Lo mismo que el queso crema Casancrem de 270 gramos, que hoy se comercializa a $254,60 y deberá bajar a $223,14. Y los ejemplos continúan.

El cuestionamiento del sector privado es, en primer lugar, que Comercio Interior está desacreditando los aumentos que la propia secretaría había otorgado recientemente, muchos de los cuales ya estaban aplicados; y por otro, que los precios plasmados en la lista son, en muchos casos, anteriores al 1° de octubre. Y Feletti planteó claramente que los valores debían retrotraerse a esa fecha.

“Esto pasa cuando cambian el esquema, lo que veníamos respetando de las pautas oficiales. Por eso, es necesario con tiempo sentarse y tener una negociación que termine de satisfacer a todos y sea en beneficio del consumidor. Si no, no es un acuerdo, es una imposición, que a su vez desconoce convenios como Precios cuidados, que estipulan subas trimestrales, y ya con márgenes negativos”, remarcaron en una alimenticia.

En otra, en tanto, coincidieron en que “si esto tuviera sentido de acuerdo, las empresas deberían haber podido proponer sus listas y eso no fue tenido en cuenta”, tal como quedó reflejado en el duro comunicado que emitió este martes Copal, antes de que su titular y presidente de la UIA, Daniel Funes de Rioja, fuera recibido nuevamente por Feletti, junto con el supermercadismo.

“Además -recalcó este empresario-, hay una diferencia de precios muy grande. Y eso es porque esa lista oficial desconoce las pautas que las empresas acordaron con Comercio cuando estaba Español. Es un retroceso y una situación delicada”.