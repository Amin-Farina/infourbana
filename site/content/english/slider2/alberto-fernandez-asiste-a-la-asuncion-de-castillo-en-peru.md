+++
author = ""
date = 2021-07-28T03:00:00Z
description = ""
image = "/images/6100c50207075-14-15_1004x565.jpg"
image_webp = "/images/6100c50207075-14-15_1004x565.jpg"
title = "ALBERTO FERNÁNDEZ ASISTE A LA ASUNCIÓN DE CASTILLO EN PERÚ"

+++

**_El presidente Alberto Fernández asistirá hoy en Perú a los actos de asunción del mandatario electo José Pedro Castillo Terrones, luego de haber arribado anoche a ese país._**

Junto a la primera dama Fabiola Yañez, el jefe de Estado fue recibido junto a la comitiva oficial en la ciudad de Lima por el embajador argentino en Perú, Enrique Vaca Narvaja.

Fernández participará hoy, en el Congreso Nacional del Perú, de la ceremonia de transmisión de mando, y mañana, en Pampa de la Quinua, del acto conmemorativo.

Además mantendrá durante su estadía diversos encuentros bilaterales con jefes de Estado y funcionarios de la región.

Como parte de la delegación oficial argentina viajaron el canciller Felipe Solá, y la ministra de Mujeres, Género y Diversidad, Elizabeth Gómez Alcorta; los secretarios General de la Presidencia, Julio Vitobello; de Asuntos Estratégicos, Gustavo Beliz, y de Comunicación y Prensa, Juan Pablo Biondi, y el diputado nacional Eduardo Valdés.