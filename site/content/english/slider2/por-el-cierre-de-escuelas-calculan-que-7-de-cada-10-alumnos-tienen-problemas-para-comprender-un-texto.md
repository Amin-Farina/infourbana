+++
author = ""
date = 2021-09-22T03:00:00Z
description = ""
image = "/images/5fa436b115e5d-1.jpeg"
image_webp = "/images/5fa436b115e5d.jpeg"
title = "POR EL CIERRE DE ESCUELAS, CALCULAN QUE 7 DE CADA 10 ALUMNOS TIENEN PROBLEMAS PARA COMPRENDER UN TEXTO"

+++

#### **_Surge de un informe de CIPPEC por el Día del Estudiante. Los chicos de secundaria no serían capaces de “identificar la idea central de un texto de longitud moderada”_**

El cierre de las escuelas durante 2020 y parte de 2021 no será gratuito. Lo que aún resta precisar es su costo. A la espera de evaluaciones que confirmen el impacto en los aprendizajes, las proyecciones son alarmantes: según un estudio de CIPPEC, 7 de cada 10 alumnos de secundaria tienen dificultades para comprender un texto a raíz de la pérdida de días de clase.

La publicación del informe coincide con el Día del Estudiante. La proyección de CIPPEC retoma una simulación que hizo el Banco Mundial a partir de los resultados en las pruebas PISA. Antes de la pandemia, el 52% de los chicos argentinos de 15 años reflejaba bajos rendimientos en lectura. Ahora ese guarismo ascendería al 73%.

De los tres escenarios que despliega la proyección, Argentina se encuentra en el peor de todos: en el pesimista, que implica un cierre escolar de 13 meses. Los otras dos categorías, optimista e intermedio, toman suspensión de presencialidad por 7 y 10 meses respectivamente. A ese punto se le suman las estrategias de mitigación, es decir cuán efectivas resultaron los mecanismos para dar clases a distancia. Allí también hay tres niveles: alto, intermedio y bajo.

El documento de CIPPEC ubica a la Argentina en el escenario pesimista por dos motivos. Al igual que buena parte de la región, tuvo un cierre escolar muy extenso que se prolongó incluso en la primera mitad de 2021. En segundo lugar, porque muchos estudiantes apenas pudieron sostener un vínculo con la escuela a distancia.

“Incluso en 2021 hubo semanas en las que 9 de cada 10 estudiantes no asistieron a la escuela de manera presencial. En 2020, 4 de cada 10 estudiantes del nivel secundario tuvieron una vinculación débil con la escuela, caracterizada por, como máximo, dos actividades por semana sin devolución del docente, o una sola actividad semanal con supervisión docente”, advierte el informe.

El resultado: 7 de cada 10 chicos estarían por debajo del mínimo en lectura. Es decir, no serían capaces de “identificar la idea central de un texto de longitud moderada, encontrar información siguiendo criterios explícitos, aunque a veces complejos, y reflexionar sobre el propósito y la forma de los textos cuando se les indica”.

La pérdida de aprendizajes no será el único problema que traerá el cierre escolar. El más urgente posiblemente sea el del abandono. Los autores del informe utilizaron una estimación del Banco Interamericano de Desarrollo que advierte un retraso de casi una década a raíz de la pandemia en el acceso a la escuela secundaria.

Según el cálculo, el escenario “normal”, es decir sin pandemia, hubiera arrojado un 18% de chicos de entre 15 y 17 años fuera de la escuela. Producto del cierre educativo, esa cifra ascendió al 22% en 2020.

Una de las razones del aumento del abandono, explica el informe, es que se disparó la cantidad de adolescentes que deben trabajar, ya sea en el hogar o fuera, para subsanar la pérdida de ingresos de su familia. A eso se le suma también el impacto en la salud psicológica de los jóvenes.

Juan Cruz Perusia, investigador principal del programa de Educación de CIPPEC y coautor del documento, incentivó el uso de “sistemas de alerta temprana” para preservar las trayectorias escolares. “Los sistemas de información educativa basados en datos individualizados de estudiantes son esenciales para la gestión de los principales desafíos del sector educativo en la pospandemia. En Argentina se debe acelerar y profundizar el desarrollo de este tipo de información”, señaló.

Hasta el momento, el país avanzó mucho más en lo formal que en lo operativo. Argentina aún no tiene una base de estudiantes con nombre y apellido a nivel nacional. El Sistema Integral de Información Digital Educativa (Sinide), que el Consejo Federal aprobó en 2014, todavía no cumple esa función. En 2018, también se aprobó la cédula escolar nacional (CEN), la cual tampoco se consolidó. A excepción de algunas jurisdicciones, no hay información nominalizada de los estudiantes, lo cual dificulta la tarea de volver a traerlos a la escuela.

Alejandra Cardini, directora del programa de Educación de CIPPEC, describió el escenario -cada vez más desigual- que atravesará la escuela argentina. “El regreso a la presencialidad educativa plena en la pospandemia será en condiciones educativas, económicas y sociales deterioradas por la crisis del Covid-19. Las desigualdades estructurales a nivel socioeconómico y territorial, y su profundización a partir de la pandemia, hacen que ser estudiante hoy en la Argentina refleje experiencias muy distintas. Esto interroga la capacidad de nuestro sistema educativo, de las políticas públicas y de los sectores dirigentes para ofrecer más y mejores oportunidades de desarrollo individual y colectivo”.