+++
author = ""
date = 2021-07-20T03:00:00Z
description = ""
image = "/images/kreplak-vacuna-provincia-1105927-1.jpg"
image_webp = "/images/kreplak-vacuna-provincia-1105927.jpg"
title = "EL VICEMINISTRO DE SALUD DE PBA DESESTIMÓ EL RETORNO A LA PRESENCIALIDAD ESCOLAR COMPLETA"

+++

**_El viceministro de Salud de la provincia de Buenos Aires, Nicolas Kreplak rechazó este martes la presencialidad completa en las escuelas del distrito y consideró que "no es algo adecuado", sobre todo en poblaciones en las cuales no se completó en su totalidad el proceso de vacunación contra el coronavirus._**

"Reducir la distancia entre los niños teniendo uno al lado de otro en pupitres no me parece adecuado. Es más, me parece peligroso. No se puede todavía tomar esas medidas de flexibilización, sobre todo en poblaciones que no han sido vacunadas en su totalidad", señaló Kreplak en declaraciones a Radio La Red.

Y en ese sentido, el viceministro agregó: "Sabemos perfectamente bien que en lugares cerrados hay que mantener el distanciamiento. Esto es lo más importante junto con la ventilación y un conjunto de otras medidas".

Las declaraciones de Kreplak se dan en relación a la decisión de La Ciudad de Buenos Aires de reanudar a partir de agosto y en forma gradual la presencialidad total en las escuelas, tras la implementación de clases virtuales por la pandemia del coronavirus.

#### Las clases en CABA

La administración de Horacio Rodríguez Larreta anunció el lunes la implementación de un esquema escalonado que será obligatorio y en el que los alumnos secundarios serán los primeros en volver a las aulas.

El plan de regreso será en burbujas que abarcarán a la clase completa de alumnos y se aplicará en etapas entre el 4 y el 23 de agosto próximos, después del receso escolar por las vacaciones de invierno y las dos jornadas del 2 y el 3 para exámenes.

En tanto gremios docentes y legisladores de la oposición porteña rechazaron la decisión del Gobierno local para la vuelta a la presencialidad total de 700.000 estudiantes después de las vacaciones de invierno y consideraron la medida como "un marketing de campaña" de Rodríguez Larreta y "un anuncio electoralista alejado de la realidad", en medio de la pandemia de coronavirus.

"Durante la primera jornada del receso y tras un cuatrimestre protagonizado por la lucha y la resistencia de la comunidad educativa, el jefe de Gobierno lleva al límite su negacionismo sanitario anunciando el regreso gradual a una situación educativa pre pandemia sin posibilidad de respetar el distanciamiento social establecido nacionalmente", señalaron desde la Unión de Trabajadores de la Educación (UTE).

Por su parte, la legisladora del Frente de Todos Lorena Pokoik consideró a la medida anunciada por el Gobierno de la Ciudad como "otro anuncio electoralista de Horacio Rodríguez Larreta alejado de la realidad".

"Las andanzas del jefe de Gobierno van en contra de todo el esfuerzo que millones de argentinos hicimos en pandemia. No se puede tirar todo por la borda, menos con la variante Delta dando vueltas en el mundo", dijo a través de su cuenta en Twitter.