+++
author = ""
date = 2021-07-12T03:00:00Z
description = ""
image = "/images/policia-1.jpeg"
image_webp = "/images/policia.jpeg"
title = "BALVANERA: 4 DETENIDOS POR TRÁFICO DE ÉXTASIS Y KETAMINA"

+++

**_Cuatro personas, tres de ellas de nacionalidad china y una argentina, fueron detenidas sospechadas de comercializar estupefacientes en un local donde se realizaba un karaoke en el barrio porteño de Balvanera, señalaron este domingo fuentes policiales._**

La Policía de la Ciudad, en coordinación con personal del Centro de Investigaciones Judiciales (CIJ) y la Agencia Gubernamental de Control (AGC) incautaron en el lugar 6 pastillas de éxtasis, 20 bolsitas con ketamina, tres pipetas artesanales con aceite de cannabis, cinco celulares y 3.690 pesos.

Dentro del lugar fueron detenidas cuatro personas, tres de nacionalidad china de 33, 38 y 40 años, y una mujer argentina de 29, indicaron.

En la causa intervino la Fiscalía Contravencional y de Faltas 11, que imputó a los cuatro involucrados por infracción a la Ley de Drogas 23.737.

En tanto, una de las personas chinas tenía una situación irregular de ingreso al país, añadieron las fuentes.