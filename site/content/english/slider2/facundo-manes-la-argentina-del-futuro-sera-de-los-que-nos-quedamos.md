+++
author = ""
date = 2022-03-01T03:00:00Z
description = ""
image = "/images/facundo-manes-1.jpg"
image_webp = "/images/facundo-manes.jpg"
title = "FACUNDO MANES: \"LA ARGENTINA DEL FUTURO SERÁ DE LOS QUE NOS QUEDAMOS\""

+++

##### **_El diputado nacional y neurocientífico fue el único representante de Juntos por el Cambio que se quedó en el recinto legislativo para escuchar el discurso del presidente Alberto Fernández, luego de que sus colegas del PRO abandonaron la sesión._**

El diputado nacional y neurocientífico Facundo Manes (Juntos por el Cambio) clamó que "la Argentina del futuro será de los que nos quedamos", luego de que sus colegas del PRO abandonaron la Asamblea Legislativa cuando escucharon críticas del presidente Alberto Fernández al gobierno de Mauricio Macri por la deuda externa y permaneció solo en su sector, rodeado de bancas vacías con banderas de Ucrania.

"Me preguntan por qué me quedé. Ya probamos con no escucharnos y así estamos", justificó Manes después de que su imagen, con la cara con gesto adusto en la banca, recorrió medios y redes sociales.

El médico de origen radical, que compitió y perdió en las PASO bonaerenses para candidatos a diputados nacionales con el macrista Diego Santilli, dijo en Twitter que no coincide "con muchas cosas del discurso" de Fernández, "pero si no lo escucho no puedo opinar".

"Más EMPATÍA y menos grieta = más desarrollo inclusivo", expresó Manes, contraponiéndose a sus socios del PRO en Juntos por el Cambio.

Y cerró su expresión con la frase "la Argentina del futuro será de los que nos quedamos".