+++
author = ""
date = 2021-08-13T03:00:00Z
description = ""
image = "/images/6112b3ac45c53_1004x565-1.jpg"
image_webp = "/images/6112b3ac45c53_1004x565.jpg"
title = "CAFIERO SOBRE EL CUMPLEAÑOS DE FABIOLA YAÑEZ EN OLIVOS: \"FUE UN DESCUIDO, NO DEBIÓ HABER PASADO\""

+++

**_El jefe de Gabinete Santiago Cafiero fue el primer funcionario en salir a hablar de la foto donde se observa a Alberto Fernández junto a otras 11 personas durante la fiesta en la Quinta de Olivos que organizaron para celebrar el cumpleaños de Fabiola Yañez, en plena cuarentena._**

“Se cometió un error, fue un descuido que no debió haber pasado”, dijo el funcionario.

En los últimos días se conocieron más derivaciones del escándalo de las “visitas VIP” a la residencia oficial de Olivos en medio de la cuarentena dura por la pandemia. El jueves, una foto en la que se ve al Presidente reunido junto a su pareja y un grupo de amigos, en una situación de esparcimiento, sin distanciamiento ni barbijo desató una fuerte polémica.

Ante esto, el Jefe de Gabinete señaló que “fue un error”, al ser consultado este viernes en diálogo con Radio 10. “No tendría que haber pasado, ahora para adelante nosotros podemos poner sobre la mesa todo lo que hicimos por la pandemia”, aseguró.

###### EL GOBIERNO ADMITIÓ LA FIESTA EN OLIVOS DURANTE LA CUARENTENA

En ese sentido, Cafiero defendió a Fernández y acusó a la oposición de “buscar escándalos”, de cara a las elecciones. “Ellos hacen política a partir de un discurso del odio, precisan esto para llevar la campaña. Buscando escándalos esconden lo que verdaderamente importa, esos planes económicos que ellos llevaron a cabo y tanto daño le hizo a los argentinos”, señaló.

Luego de que se diera a conocer la foto, los diputados Mario Negri, Luis Petri, Waldo Wolff y Cristian Ritondo impulsarán un pedido de juicio político contra el jefe de Estado, acompañados por otros integrantes de la oposición.

Para el jefe de ministros, están “desorientados”. Además, aseguró que no tuvieron un rol significativo para aportar durante la gestión de la pandemia.

“Han tenido un rol nulo, se oponían a todo, a las medidas de cuidado, después hasta incluso cuestionaban la existencia del virus, de la pandemia, luego la veracidad y eficiencia de las vacunas”, cuestionó.

En la misma línea, Cafiero apuntó contra el Jefe de Gobierno porteño Horacio Rodríguez Larreta, el expresidente Mauricio Macri y la precandidata a diputada por la Capital María Eugenia Vidal. “No dieron explicaciones en ningún momento cuando Macri viajaba por el mundo, volvía, no cumplía con las medidas. Tampoco lo hicieron cuando Larreta se fue en vuelo privado a Buzios y no participó entonces de protocolos que tenía que llevar como jefe de Gobierno, o Vidal también que había generado reuniones donde de ahí se contagiaron comensales que ella invitaba”, recordó.

Por eso, cuestionó la “doble vara” opositora. “Reconocemos que nos equivocamos, pero sabemos que los temas que le importan a la gente es cómo vamos a salir de la pandemia, cómo vamos a levantar a la economía, al salario, al trabajo”, puntualizó.