+++
author = ""
date = 2021-05-31T03:00:00Z
description = ""
image = "/images/g20.jpg"
image_webp = "/images/g20.jpg"
title = "DE TODOS LOS PAÍSES DEL G20, ARGENTINA PODRÍA SER EL PAÍS QUE MÁS TARDE EN RECUPERAR SU ECONOMÍA"

+++

La Organización para la Cooperación y el Desarrollo Económicos (OCDE) dijo este lunes en un informe que prevé para este año un crecimiento del 6,1 % en la economía de Argentina, aunque persisten factores que limitan la recuperación tras una caída del PIB del 9,9 % en 2020. El informe, además, estimó que el país va a ser el que más va a tardar en recuperar niveles de actividad económica previos a la pandemia entre los miembros del G20. La Argentina recién recuperaría lo perdido en 2026.

De acuerdo a su informe de perspectivas económicas, la OCDE pronostica además una expansión económica de Argentina del 1,8 % en 2022, que, de concretarse, dejará a la segunda mayor economía suramericana en niveles todavía por debajo de los que tenía antes de la irrupción de la pandemia de covid-19.

En una comparación de ritmos de recuperación que incluye a todos los miembros del G20, la Argentina es la que más va a tardar en recuperar los niveles previos a la pandemia. La comparativa estima el crecimiento del PBI de los miembros de ese grupo de países y cuánto tiempo va a tardar cada uno a alcanzar un PBI per cápita igual al que mostraba antes de la pandemia.

China, Turquía, Corea del Sur, Rusia y los Estados Unidos ya recuperaron lo perdido, según el informe. Japón, Alemania, India e Indonesia lo harán este año. De la regió, Brasil volverá a niveles de actividad pre-crisis en 2022, mientras que México en 2023. Argentina será la única que tardará 5 años, según la estimación.

“Los persistentes desequilibrios macroeconómicos y las nuevas restricciones a la movilidad pesarán sobre la demanda interna y limitarán la recuperación”, advierte el informe.

La OCDE observó que Argentina enfrenta una fuerte segunda ola de covid-19 y que la vacunación “avanza lentamente”, lo que ha obligado a endurecer las restricciones en abril y mayo.

Según el informe, se espera que Argentina logre la inmunidad colectiva en 2022.

Por otra parte, la OCDE advirtió que la inflación, que ronda el 40 % anual, permanecerá en niveles altos, “a pesar de la débil demanda interna y los estrictos controles de precios”.

Asimismo, observó que los problemas en el mercado laboral están afectando los ingresos de los hogares, frenando el consumo privado, que cayó un 13,1 % en 2020.

Para este año la recuperación del consumo privado proyectada por la OCDE es del 2,2 %, con un magro avance del 1 % previsto para 2022.

Además, la OCDE señaló que, aunque habrá una lenta recuperación del empleo, la alta informalidad laboral seguirá siendo un factor de preocupación.

En su informe, la OCDE destacó que el Gobierno de Alberto Fernández adoptó medidas “audaces y oportunas” para contener la pandemia y apoyar a hogares y empresas.

El organismo espera que el gasto relacionado con la pandemia sea retirado gradualmente, al tiempo que señaló que los ingresos más sólidos, en parte relacionados con los altos precios de las materias primas que exporta Argentina, han colaborado para mejorar ligeramente los resultados fiscales, que el año pasado cerró con un déficit primario del 6,5 % del PIB.

“Esto reducirá la necesidad para la financiación monetaria a corto plazo”, indica el informe, que marca que “trazar un camino a mediano plazo hacia la sostenibilidad fiscal ayudaría para apuntalar la confianza y reforzar la inversión”.

La OCDE advirtió que sus pronósticos para Argentina podrían cambiar si se produce una corrección desordenada de los desequilibrios macroeconómicos del país, una devaluación repentina del peso argentino, mayor inflación o bloqueos prolongados de la actividad económica como consecuencia de un empeoramiento de la emergencia sanitaria.

En sentido inverso, las proyecciones podrían mejorar si la campaña de vacunación nacional e internacional avanzara a mayor ritmo o si se produjera una recuperación más rápida en Brasil.

En su informe, la OCDE pronostica para este año un crecimiento del 3,7 % en el PIB de Brasil, que es el principal socio comercial de Argentina.

Para el organismo, también un tipo de cambio más competitivo en Argentina podría impulsar aún más las exportaciones del país.

Según el informe, las exportaciones de bienes y servicios de Argentina crecerían un 6,8 % este año y un 11,3 % en 2022.