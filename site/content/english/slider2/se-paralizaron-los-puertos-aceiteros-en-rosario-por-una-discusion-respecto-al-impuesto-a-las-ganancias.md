+++
author = ""
date = 2021-11-04T03:00:00Z
description = ""
image = "/images/img_0494.jpg"
image_webp = "/images/img_0494.jpg"
title = "¿PELIGRA LA CANDIDATURA DE CERNADAS?"

+++
###### FUENTE: INFOBAN

##### **_La Justicia hizo lugar a una medida cautelar planteada contra su candidatura a concejal. Provisoriamente no podrá integrar la lista de Juntos en Tigre._**

En una causa que miran con mucha atención un sinnúmero de dirigentes políticos de todos los sectores por las implicancias que podría tener a futuro sobre las pretensiones reeleccionistas, el Tribunal de Trabajo Nº 1 de San Isidro admitió la acción de amparo presentada luego del rechazo inicial de la Junta Electoral de la provincia de Buenos Aires a la impugnación de la candidatura de Segundo Cernadas.

Cabe recordar que la impugnación al dirigente de Juntos fue presentada previo a las PASO en virtud de que estaría incumpliendo la ley que limita las reelecciones en la provincia de Buenos Aires. Cernadas fue electo concejal en 2015, y al poco tiempo renunció a la banca para asumir al frente de la ANSeS de Tigre, y en 2017 nuevamente se presentó como candidato a concejal, cargo que busca conseguir una vez más en las próximas elecciones.

El Tribunal en una sentencia interlocutoria a la que accedió InfoBAN, resolvió por mayoría de sus integrantes revocar de manera cautelar la decisión de la Junta Electoral, que el 18 de octubre  oficializó la candidatura de Cernadas.

Asimismo ordenó y comunicó a la Junta Electoral competente que “provisoriamente el candidato a concejal Pedro Segundo Cernadas no podrá integrar la lista de postulantes a dicho cargo”.

La impugnación a la candidatura del actor fue presentada desde el sector de José Luis Espert, a través de Juan José Cervetto, quien encabeza la lista de concejales de Avanza Libertad en Tigre

La ley que estaría infringiendo Cernadas es la que impide las reelecciones indefinidas en el ámbito de la provincia de Buenos Aires, sancionada en 2016, la cual establece que los intendentes, concejales, consejeros escolares y legisladores elegidos para un cargo solo “podrán ser reelectos por un nuevo período”, pero que “si han sido reelectos no podrán ser elegidos en el mismo cargo, sino con intervalo de un período”.

El curso de la denuncia contra el dirigente de Juntos es mirada con suma atención, ya que podría significar una salida a futuro para sortear la limitación electiva en territorio bonaerense, y marcar un precedente que habilite un mecanismo a través de renuncias o licencias a los cargos para vulnerar la ley que estableció el fin de las reelecciones indefinidas.

##### La respuesta de Cernadas

Desde el Pro de Tigre indicaron que la Justicia Electoral ya dio vista al fallo y “lo rechazó por impracticable”. Ante la requisitoria de este medio sólo facilitaron el que sería el punto IX de tal resolución, donde se sostiene que “en esta instancia avanzada del proceso electoral, este Organismo de la Constitución carece de competencia para dar cumplimiento a la medida ordenada”, es decir, la cautelar del tribunal sanisidrense, que dejaba a Cernadas fuera de la lista de postulantes a concejales de Tigre en la lista de Juntos.

En contactos previos con la prensa, el ex presidente del HCD de Tigre había manifestado que se trata de “una operación política”, al considerar que “la ley es totalmente clara y dice que no puede haber una re-reelección por más de dos periodos, y que para que se compute uno de los dos tenés que haber estado como mínimo dos años en ese periodo", ya que en su primer mandato renunció a la banca a los meses de haber asumido.