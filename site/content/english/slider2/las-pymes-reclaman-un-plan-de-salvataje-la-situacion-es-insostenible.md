+++
author = ""
date = 2021-05-21T03:00:00Z
description = ""
image = "/images/5ef0ce6cf3065_1004x565-1.jpg"
image_webp = "/images/5ef0ce6cf3065_1004x565.jpg"
title = "LAS PYMES RECLAMAN UN PLAN DE SALVATAJE: \"¡LA SITUACIÓN ES INSOSTENIBLE!\""

+++

**_A través de un comunicado la CAME aseguró que la supervivencia de las pequeñas y medianas empresas depende del auxilio financiero que pueda darles el Gobierno. Las nuevas medidas regirán hasta el 31 de mayo._**

Tras las nuevas restricciones anunciadas por el gobierno nacional para mitigar la segunda ola de la pandemia de coronavirus la Confederación Argentina de la Mediana Empresa (CAME), reclamó este jueves un auxilio financiero, impositivo y previsional para evitar el colapso de las pymes.

“La supervivencia de las pymes se torna insostenible”, sostuvo el presidente de la entidad, Gerardo Díaz Beltrán que remarcó que hay personal que desde hace un año no puede trabajar por estar dentro de los grupos de riesgo.

Y añadió: “A eso se suma una drástica caída de las ventas (que en algunos rubros superó el 30% en la comparación interanual), el incremento de costos por la inflación y hasta la creación de nuevos impuestos, como la tasa “covid” que cobran algunos municipios”.

#### Un plan de contingencia

Para el titular de la Cámara que nuclea a las pequeñas y medianas empresas del país, “el Gobierno deberá plantear un plan de contingencia con suspensiones de todos los cargos impositivos, financieros, patronales y restablecer la Asistencia de Emergencia al Trabajo y la Producción (ATP)”. Además, Díaz Beltrán pidió que los gobernadores adopten medidas especiales para evitar el cierre de más empresas en todo el país.

La CAME recordó que las pymes movilizan el 70% del empleo privado del país y alertó que, por el impacto de la primera cuarentena estricta, la entidad registró “un cierre masivo de comercios que alcanzó a un 15.6% de los locales del país”. Ese porcentaje, según precisó, representa más de 90 mil comercios que cerraron sus puertas desde marzo de 2020. ”Hay sectores terriblemente afectados que ya no tienen margen para reconvertirse ni mantener la actividad”, concluyó Díaz Beltrán.