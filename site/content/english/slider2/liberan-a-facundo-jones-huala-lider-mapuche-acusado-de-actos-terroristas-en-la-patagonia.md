+++
author = ""
date = 2022-01-21T03:00:00Z
description = ""
image = "/images/facundo-jones-huala-08232018-01-361078-1.jpg"
image_webp = "/images/facundo-jones-huala-08232018-01-361078.jpg"
title = "LIBERAN A FACUNDO JONES HUALA, LÍDER MAPUCHE ACUSADO DE ACTOS TERRORISTAS EN LA PATAGONIA"

+++

##### **_La Justicia chilena le otorgó la libertad condicional al líder terrorista Facundo Jones Huala, acusado de cometer distintos actos como incendios, portación ilegal de armas, entre otros._**

Había sido condenado el 21 de diciembre de 2018 a nueve años de prisión por haber participado en 2013 del incendio de una propiedad.

##### Facundo Jones Huala recupera la libertad por decisión de la Justicia de Chile

Este viernes la justicia chilena dio lugar al recurso de amparo solicitado por la defensa del líder mapuche para que se le otorgue la libertad condicional.

El beneficio le había sido negado en septiembre cuando su abogada Karina Riquelme lo solicitó en función de una supuesta buena conducta en una prisión que es habitada por unos 400 detenidos, gran parte de ellos de origen mapuche.

De acuerdo con el fallo de la Cámara de Apelaciones de Temuco, entre varias cuestiones, se considera que Jones Huala, “registra cuatro bimestres de buena conducta”.

Además, aclara que la libertad condicional “no extingue ni modifica la duración de la pena”, sino que es un “modo particular de hacerla cumplir”, en libertad por el condenado y según las disposiciones que se dicten.

La medida lleva la firma de los jueces Carlos Gutiérrez Zavala y Luz Mónica Arancivia Mena.

##### La condena al terrorista Jones Huala

El Tribunal Oral en lo Penal de Valdivia dictó una condena de 9 años de prisión efectiva para Facundo Jones Huala en diciembre de 2018. Le otorgaron 6 años por el incendio de una propiedad y 3 años por tenencia ilegal de armas.

Fue acusado de ser uno de los tres hombres que el 9 de enero de 2013 amenazó con armas de fuego a la familia Riquelme que habitaba la finca. Según la investigación del caso, fueron sacados del lugar para posteriormente “arrojar una antorcha a la vivienda con la que le prendió fuego”, mientras reclamaban la devolución de tierras a las comunidades mapuche.

Alberto Riquelme y su hija Cristina fueron identificaron en su momento ropa, armas y otros objetos que utilizaron los atacantes enmascarados durante el hecho, que luego se secuestraron en la casa de la machi Millaray Huichala, donde estaba alojado Jones Huala.