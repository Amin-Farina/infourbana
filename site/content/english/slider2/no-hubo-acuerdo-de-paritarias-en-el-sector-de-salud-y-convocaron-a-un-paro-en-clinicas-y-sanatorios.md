+++
author = ""
date = 2021-07-23T03:00:00Z
description = ""
image = "/images/medicos-salud.png"
image_webp = "/images/medicos-salud.png"
title = "NO HUBO ACUERDO DE PARITARIAS EN EL SECTOR DE SALUD Y CONVOCARON A UN PARO EN CLÍNICAS Y SANATORIOS"

+++

**_El gremio reclama un ajuste salarial de 45%, pero las empresas afirman que no pueden afrontar ese aumento. Los turnos no urgentes fueron reprogramados. Las razones detrás del conflicto._**

El del sector de la sanidad es un conflicto en cadena. Por un lado, los prestadores argumentan que no pueden dar un aumento del 45% que reclama el gremio de la Sanidad, que lidera Héctor Daer, porque no tienen cubiertos sus costos. Por otro lado, las prepagas, que son financiadoras (y muchas veces, también prestadoras), aseguran que tienen “trabados” los aumentos y por eso acudieron a la Justicia para lograr que se recompongan sus márgenes.

Hoy venció la conciliación obligatoria que dictó el Ministerio de Trabajo y el sindicato que representa a los trabajadores de la salud ratificó que este viernes realizarán paros de cuatro horas por turno en las clínicas y sanatorios privados.

“Los empresarios tienen la obligación de pagarnos lo que corresponde y si no tienen recursos que los vayan a buscar”, dice la carta que firman Daer y Carlos West Ocampo que confirmó la medida.

El miércoles por la tarde la Federación Argentina de Prestadores de Salud confirmó que se reprogramarán los turnos programados no urgentes en todas las instituciones prestadoras del país.

El comunicado de la FAPS explica claramente lo que está pasando: “Los prestadores de salud no podemos garantizar la pretendida actualización de los salarios de alrededor del 45% porque las Obras Sociales y las empresas de Medicina Prepaga no trasladan los recursos suficientes para poder afrontar ese porcentaje”.

Y sigue: “Nuestros ingresos -que en definitiva son los recursos de los trabajadores- están estipulados por los aranceles que definen los financiadores (Obras Sociales, empresas de Medicina Prepaga, PAMI). Y si los financiadores no actualizan los aranceles que pagan, nuestros ingresos pierden día a día frente a los costos en alza”.

Mientras tanto, avanza en la Justicia la demanda de las prepagas para lograr aumentar las cuotas “al ritmo del aumento de sus costos”, tal como explican en el sector. A mediados de este mes hubo un fallo de la Justicia que suspendió la anulación de aumentos y, si el fallo se confirma, las prepagas podrán estar habilitadas pronto a aumentar un 26%.

El aumento que viene sería el cuarto en el año, luego de un 3,5%, un 4,5% y un 5,5% autorizado en el primer semestre de 2021. Hasta este momento, alegan las prepagas, todas las subas que se dieron se utilizaron para cubrir lo que faltaba de la paritaria 2019-2020.

Esta semana, la Unión Argentina de Salud (UAS), que nuclea al sector de la medicina privada, le solicitó al Gobierno que otorgue “una gratificación extraordinaria mensual, financiada y abonada desde el Estado en forma directa, mientras perdure la emergencia sanitaria del 50% del salario de Enfermera/o” a todo el personal en relación de dependencia, médicos con contrato de locación de servicios y residentes o alumnos de la carrera de especialización.

Asimismo, también pidió reformas a largo plazo, como exenciones fiscales y fondos provenientes del Estado Nacional para financiar tratamientos de alto costo de medicamentos, prótesis y órtesis, entre otros. Finalmente, solicitó “disponer una fórmula polinómica que contemple ajustes mensuales y automáticos de las cuotas de las Empresas de Medicina Prepaga, en virtud de la variación de los costos”.

Consultadas por este medio, fuentes de la Superintendencia de Seguros de Salud, a cargo de la decisión de otorgar o no el aumento a las prepagas, aseguraron que por el momento no se había citado a las empresas para conversar y que tampoco hay novedades respecto de la potencial suba.