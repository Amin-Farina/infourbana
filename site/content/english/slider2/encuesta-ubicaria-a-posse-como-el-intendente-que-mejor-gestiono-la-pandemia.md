+++
author = ""
date = 2021-09-08T03:00:00Z
description = ""
image = "/images/gustavo-posse-1.jpg"
image_webp = "/images/gustavo-posse-1.jpg"
title = "ENCUESTA UBICARÍA A POSSE COMO EL INTENDENTE QUE \"MEJOR GESTIONÓ LA PANDEMIA\""

+++

###### **_Un sondeo de opinión reveló que el intendente de San Isidro, Gustavo Posse, es el mandatario municipal del Conurbano bonaerense que mejor gestionó ante la crisis sanitaria generada por el coronavirus._**

En segundo lugar aparecen el jefe comunal de Tres de Febrero, Diego Valenzuela, con una valoración positiva cercana a los 70 puntos, y en tercer término, Juan Zabaleta, ex jefe comunal de Hurlingham, y actual ministro de Desarrollo Social de la Nación. Luego se ubican Ariel Sujarchuk (Escobar); Julio Zamora (Tigre); Jorge Macri (Vicente López) y Fernando Gray (Esteban Echeverría).

La puntuación de Posse en este relevamiento, que alcanzó el 79,3 por ciento de los votos, lo ubica al tope de este ranking, en base a evaluaciones efectuadas por los vecinos.

El estudio, que relevó 997 casos, fue realizado por la revista especializada en política nacional Comunas, tras indagar las opiniones de ciudadanos de 24 localidades del Conurbano, entre el 5 y el 10 de agosto pasado.

Los principales aspectos valorados estuvieron relacionados con el funcionamiento de los sistemas de salud, las políticas económicas, el manejo de las restricciones y posteriores habilitaciones de actividades, y el acceso a hisopados gratuitos.