+++
author = ""
date = 2021-12-17T03:00:00Z
description = ""
image = "/images/s4castpo5nfv7eimemsoklpenu.jpg"
image_webp = "/images/s4castpo5nfv7eimemsoklpenu.jpg"
title = "CHUBUT: MANIFESTANTES QUE SE OPONEN A LA LEY DE MEGAMINERÍA INCENDIARON LA CASA DE GOBIERNO Y OTROS EDIFICIOS PÚBLICOS"

+++

#### **_Al igual que en la madrugada del miércoles, este jueves volvieron a registrarse incidentes en la capital de Chubut en el marco de protestas contra la aprobación de la polémica ley de megaminería. Hasta largas horas de la noche, manifestantes destrozaron e incendiaron la Casa de Gobierno y otros edificios públicos._**

Los vecinos que reclamaron en el lugar demandaron que el gobernador de la provincia vete el proyecto, aunque Mariano Arcioni promulgó la ley en horas de la tarde de este jueves.

“Las instalaciones del Superior Tribunal de Justicia también están en llamas, al igual que la plaza de Rawson, la Casa de Gobierno y la Legislatura”, detalló la transmisión del canal local AZM TV. La periodista del canal de la Patagonia, detalló a continuación: “Arde Rawson, está todo incendiado”.

En las imágenes que se difundieron desde Chubut, se observaron importantes destrozos en la vía pública, pero sobre todo autos prendidos fuego en calles y avenidas de la zona céntrica de la ciudad capital. Según se informó son 16 los edificios públicos atacados por los manifestantes.

El miércoles, en una sesión sorpresiva, la Legislatura de la provincia aprobó el proyecto de zonificación minera resistido por varios sectores de la sociedad de esa provincia. El polémico proyecto, criticado por la Universidad Nacional de la Patagonia y por representantes del Conicet, fue aprobado por 14 votos afirmativos y 11 diputados provinciales que se pronunciaron de forma negativa. Hubo dos legisladores ausentes.

Horas después, un grupo de manifestantes y la policía de Rawson se enfrentaron con piedrazos y disparos de balas de goma en el marco de los primeros reclamos contra la decisión política. Este jueves hubo nuevos enfrentamientos entre las agrupaciones y las fuerzas de seguridad.

Puerto Madryn, Esquel y otras ciudades de la Patagonia fueron escenario de multitudinarias marchas en reclamo del la nueva ley de Zonificación de la Actividad Minera.

La iniciativa oficial permitirá la minería metalífera química, de plata, cobre y plomo, en algunas zonas de la provincia, como Telsen y Gastre, y es activamente resistida por buena parte de los movimientos sociales chubutenses. Se trata de la reglamentación de la Ley 5.001 que prohíbe la megaminería con uso de cianuro en la jurisdicción, pero que en uno de sus artículos permite que se determinen áreas en las que podría permitirse.

La directora del hospital Santa Teresita de Rawson, Paula Morales, informó a Télam que “esta madrugada (por ayer) se presentaron 11 pacientes con diversas heridas leves, la mayoría contusiones, algunos heridos de postas de goma y un paciente atendido por inhalación de gases”. “Del total de atendidos, seis eran efectivos policiales” explicó la médica.

Unos 300 manifestantes, en su mayoría jóvenes, permanecieron en señal de protesta desde ayer al mediodía en la esquina de Moreno y Vachina de Rawson, la ochava norte de Casa de Gobierno más próxima al despacho del gobernador Mariano Arcioni, quien viajó hoy a Comodoro Rivadavia, 400 km. al sur de esta capital.

Quienes se oponen al proyecto oficial, hecho casi a la medida de la empresa candiense Pan American Silver que espera hace 11 años explotar el proyecto Navidad en la meseta, sostienen que la compañía, que busca explotar plata y plomo del yacimiento, usará un método distinto al cianuro pero no menos nocivo: el xantato.

Para evitar las prohibiciones legales provinciales al uso de cianuro en la megaminería, las compañías aseguran que usarán un método más amigable con el ambiente: se trata de un proceso de flotación que usa reactivos y detergentes a los que se pegan los minerales. El xantato y la policriamida son dos de los reactivos rechazados por los vecinos por su potencial contaminante. Al reaccionar con la poliacrilamida, el xantato genera bisulfuro de carbono que es un compuesto del cual se puede obtener ácido sulfúrico. Aunque no existe una normativa que en la Argentina prohíba el uso de esta sustancia, hubo un proyecto en el Senado en 2015 que nunca prosperó.