+++
author = ""
date = 2021-10-04T03:00:00Z
description = ""
image = "/images/27dmv5ijnzh5zb6q5ppcytlbgm.jpeg"
image_webp = "/images/27dmv5ijnzh5zb6q5ppcytlbgm.jpeg"
title = "CONFLICTO EN RÍO NEGRO: LOS MAPUCHES INCENDIARON UN CAMPAMENTO DE VIALIDAD Y LAS AUTORIDADES LOCALES PIDIERON A NACIÓN EL ENVÍO DE FUERZAS FEDERALES"

+++

##### **_Tres personas encapuchadas atacaron el puesto y dejaron un mensaje con consignas sobre la lucha territorial de la comunidad indígena. La Gobernadora Arabela Carreras reclamó ayuda del gobierno nacional y anunció que radicará una denuncia por terrorismo_**

Personas con los rostros cubiertos irrumpieron en un campamento de Vialidad Provincial ubicado en el camino al cerro Catedral, a pocos kilómetros del centro de Bariloche, incendiaron un depósito y máquinas viales y dejaron maniatado al cuidador y su esposa.

Los atacantes atribuyeron el hecho a la lucha que las comunidades mapuches mantienen en la zona, en el marco de un plan de reivindicación territorial.

##### LOS HECHOS

El episodio ocurrió antes de las 23 horas. El cuidador y su esposa estaban en el campamento cuando fueron sorprendidos por tres hombres encapuchados, uno de los cuales portaba un arma de fuego.

De acuerdo a datos oficiales, el matrimonio fue maniatado en el interior de la vivienda: “somos mapuches” les anunciaron los atacantes.

Antes de rociar el depósito y las máquinas viales con combustible, los encapuchados sustrajeron teléfonos celulares y las llaves del vehículo particular del cuidador.

Bomberos de distintas dependencias trabajaron durante varias horas en el sitio para poder controlar las llamas, que provocaron cuantiosos daños.

Los encapuchados atribuyeron el atentado al plan de reivindicación territorial que comunidades mapuches mantienen en la Patagonia y apuntaron contra la Gobernadora rionegrina, fiscales y jueces.

“Los niños mapuches crecerán y vencerán en el weichan, la tierra no se vende, se defiende. Fuera winkaes terratenientes de wallace mapu” decía uno de los escritos, en el que había simbología mapuche.

Otro de los textos que dejaron en el lugar decía: “Ninguna agresión sin respuesta newentuaimun Lof Quenquentrew wintrange Pu Weichafe Libertad al Lonko Facundo Huala, Rafael Nahuel, Matias Catrileo, Pablo Marchant, Santiago Maldonado, presentes en el weichan. Los Weichafes de la R.A.M. Vengaremos a Inakayal”.

El episodio podría tener vinculación con la usurpación que la comunidad Lof Quemquentreu mantiene desde hace varios días en el paraje Cuesta del Ternero, sobre la ruta provincial 6, entre Bariloche y El Bolsón.

El predio, conocido como Tapera de los Álamos, fue restituido a sus propietarios a partir de una medida que dispuso el Ministerio Público Fiscal de Río Negro, aunque los encapuchados lograron reingresar. Desde ese momento la Policía de la provincia cuenta con una fuerte custodia, mientras que allegados a quienes mantienen la ocupación e integrantes de otras comunidades protagonizan constantes ataques a los efectivos de la fuerza rionegrina.

#### LA DENUNCIA

La mandataria rionegrina aseguró este lunes que mantuvo una comunicación con el Ministro de Seguridad de la Nación, Aníbal Fernández, a quien le solicitó la inmediata intervención de las fuerzas federales, ante los constantes episodios “que protagonizan estos grupos violentos”.

Anunció además que el Gobierno de Río Negro presentará ante la Justicia Federal una denuncia por “terrorismo”, en la que no sólo incluirá el reciente ataque a las dependencias de Vialidad Nacional, sino también el incendio que desconocidos provocaron en el flamante Centro de Información Turística de El Bolsón, ocurrido durante la madrugada del domingo.

“Le pedí a Nación la inmediata intervención de las fuerzas federales de seguridad para actuar en la región y prevenir nuevos hechos vandálicos” dijo Carreras, y aseguró que recibió el compromiso de acompañamiento.

El Gobierno rionegrino invocará el Art. 213 del Código Penal de la Nación, que indica que: “Se impondrá prisión o reclusión de CINCO a VEINTE años al que tomare parte de una asociación ilícita cuyo propósito sea, mediante la comisión de delitos, aterrorizar a la población u obligar a un gobierno o a una organización internacional a realizar un acto o abstenerse de hacerlo (...)”. La norma hace referencia al uso de armas y agentes químicos, encuadrando estos últimos a los combustibles utilizados para los hechos vandálicos de El Bolsón.

Señaló además que no avala “ningún tipo de violencia sin importar el sector del cual provenga”, y expresó su repudio a “cualquier tipo de ataque, porque esta no es la forma de resolver las diferencias con el Estado”.