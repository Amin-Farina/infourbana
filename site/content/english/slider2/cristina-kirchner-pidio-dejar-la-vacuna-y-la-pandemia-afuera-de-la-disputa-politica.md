+++
author = ""
date = 2021-06-15T03:00:00Z
description = ""
image = "/images/vieja-nefasta.jpeg"
image_webp = "/images/vieja-nefasta.jpeg"
title = "CRISTINA KIRCHNER PIDIÓ \"DEJAR LA VACUNA Y LA PANDEMIA AFUERA DE LA DISPUTA POLÍTICA\""

+++

**_La vicepresidenta de la Nación, Cristina Fernández de Kirchner, pidió este lunes a la dirigencia política que deje "la vacuna y la pandemia afuera de la disputa política" y abogó para que no se ponga en duda "lo que dice la ciencia", al encabezar junto al gobernador bonaerense, Axel Kicillof, un acto en el Hospital de Niños "Sor María Ludovica" de La Plata._**

"No podemos seguir discutiendo y envenenando a la gente con que la vacuna tal no sirve. En nombre de tanta gente que no se vacunó y hoy ya no está, y sus familiares la lloran, en nombre de los trabajadores de la salud, por favor dejemos a la vacuna y a la pandemia afuera de la disputa política", expresó la vicepresidenta.

Cristina Fernández, junto al gobernador bonaerense, Axel Kicillof, encabezó este lunes un acto en el Hospital de Niños "Sor María Ludovica" de La Plata, donde se reanudaron obras en el Pabellón de Salud Mental, que implicarán una inversión de $148.605.766.

"Entre todos podemos llegar a un acuerdo básico mínimo; hay cosas que no pueden ser ya objeto de discusión", afirmó Cristina al referirse a los cuestionamientos dirigidos hacia las vacunas y a las medidas de cuidado para frenar los contagios de coronavirus que, sin embargo, encuadró en situaciones de "irracionalidad" que se dan en todo el mundo.

La vicepresidenta pidió "no buscar divisiones ni entorpecer al otro".

"Contribuyamos a que haya menos contagios", pidió, al tiempo que resaltó que Argentina cuenta, desde hoy, con "20 millones de vacunas".

Cristina Fernández analizó que "con la última llegada de Astrazeneca llegamos a los 20 millones de vacunas, con todas las dificultades que vemos y son producto de la desigualdad, vemos países que han acaparado millones de vacunas a punto tal que tienen 8 vacunas por habitantes frente a otros que penan por vacunas".

"¿No sería mejor que todos unidos pidamos a esos países que sean solidarios y envíen esas vacunas al resto del mundo?", se preguntó la vicepresidenta.

Cristina hizo también un "llamado a los medios de comunicación" para que contribuyan a garantizar información veraz a la población y que no se ponga "en duda la palabra de los médicos y de la ciencia".

"Ayudemos a la verdadera libertad cuando estemos todos vacunados y podamos hacer lo que queremos", subrayó y evaluó que "debería haber un acuerdo tácito de la sociedad para hacerle caso a los médicos".

La epidemia de polio

En ese contexto, la vicepresidenta recordó que cuando niña asistía a este nosocomio pediátrico, centro de referencia de la provincia de Buenos Aires, para vacunarse y que allí incluso recibió su dosis de Sabin oral contra la poliomielitis.

"La campaña de polio fue después del derrocamiento de (Juan Domingo) Perón, imagínense si había grieta en el país, en el colegio vacunaban obligatoriamente, estaba prohibido hablar de Perón y Evita por ley, imagínense si había que luchar por las libertades en serio, la libertad de elegir, de votar a quien uno quiere", relató.

Explicó que, en ese contexto, "en mi casa la mayoría era peronista, salvo mi viejo, pero no se nos ocurrió decir ´no nos vamos a vacunar´, (decir eso) es producto de la irracionalidad, que no es patrimonio de Argentina, no es el juego de la democracia, hay algo que escapa y es la falta de razones".

Tras enviar un abrazo a todos y todas aquellas personas que han perdido un ser querido en esta pandemia, Cristina confió: "Vamos a salir de esto, con la vacuna vamos a salir, vamos a vacunar a todos y todas las argentinas".

El discurso de Kicillof

Por su parte, Kicillof también reclamó a la oposición que "deje de sembrar odio por los canales de televisión" y sostuvo: "Si quieren actuar en forma responsable ayuden con algo: consigan vacunas o anótense como voluntarios".

Kicillof expresó su emoción por estar inaugurando obras en un hospital que tiene 126 años de fundado y que sin embargo, al asumir su gestión, lo encontró "en estado de abandono en términos edilicios que no se merecen los y las trabajadoras y los niños y las niñas de la provincia de Buenos Aires".

Detalló que la obra inaugurada fue paralizada en agosto de 2019 por el Gobierno anterior de María Eugenia Vidal, lo que calificó como "una vergüenza"

"Muchas obras se empezaron y no se terminaron, otras estaban prácticamente terminadas y no se quisieron inaugurar porque las había empezado otro Gobierno. Los 5 hospitales que no quisieron inaugurar son una vergüenza", dijo Kicillof, quien precisó que "vinimos a terminar las asignaturas pendientes" de la administración de Vidal.

El mandatario bonaerense remarcó con ironía que "los que dejaron pudrir vacunas nos dicen cómo hay que vacunar; los que no terminaban hospitales nos quieran decir cómo hay que hacer con el sistema de salud, los que cerraron escuelas técnicas, escuelas en el Delta, nos vienen a decir lo que hay que hacer en educación".

"Cerrar escuelas es algo que manejan y nosotros no cerramos ningunas; es una falta de respeto a las maestras decir que no hubo clases", completó.

El mandatario cuestionó a la oposición por "venir a poner más angustia a una pandemia difícil, venir a inculcar odio, generar división".

"Lo único que pedimos es que, si quieren actuar de oposición responsable, ayuden con algo, que consigan vacunas, se pongan de voluntarios, que acompañen a la gente que sufre en vez de recorrer canales y Twitter llenando de odio a la gente, es una invitación para que colaboren, la mano está extendida para que ayuden porque hace falta", reclamó.

Finalmente, con emoción, se refirió a los trabajadores de salud: "Se podrá llenar de equipos, de edificios, inaugurar hospitales pero lo central son los trabajadores y trabajadoras de la salud", dijo y agregó: "hay que invertir en eso, este hospital ustedes lo van a usar para curarnos y salvarnos la vida, muchas gracias".

En el acto de inauguración estuvieron presentes la vicegobernadora bonaerense, Verónica Magario; y el ministro de Salud bonaerense, Daniel Gollan.

La obra inaugurada

En el primer y tercer piso inaugurados este lunes hay habitaciones compartidas de internación; en el 2° piso se desarrolla un área de internación especial y en la azotea hay un espacio para la colocación de los equipos para la instalación de climatización, como así también los tanques de reserva.

El nuevo edificio cuenta además con un subsuelo de 166 m2, la Planta Baja de 382 m2, un primer nivel de 457 m2 y un segundo y tercer nivel de 366 m2 cada uno. Se suma así una superficie cubierta total de 1.761 m2 de obra nueva provista de instalaciones de gases medicinales para internación