+++
author = ""
date = 2021-04-29T03:00:00Z
description = ""
image = "/images/j-biden.jpg"
image_webp = "/images/j-biden.jpg"
title = "JOE BIDEN: \"ES HORA DE QUE LAS EMPRESAS ESTADOUNIDENSES Y EL 1% MÁS RICO DE LOS ESTADOUNIDENSES PAGUEN LO QUE LES CORRESPONDE\""

+++

**_El presidente de Estados Unidos, Joe Biden, habló este miércoles por primera vez ante el Congreso, en un discurso en el que expresó su voluntad reformadora, en particular en materia fiscal._**

El presidente, luciendo una mascarilla, fue recibido con aplausos, mientras subía al escenario en su primer discurso en una sesión conjunta, en medio de una pandemia. El recinto estuvo con mucho de sus asientos vacíos, una muestra de las medidas de seguridad por el coronavirus: en lugar de las 1.600 personas que suelen asistir, el aforo fue de 200.

En vísperas de cumplir los primeros y simbólicos 100 días en el poder, Biden expuso su proyecto para las “familias estadounidenses”, que contiene una “inversión histórica” en educación y en la infancia, por un monto cercano a los 2 billones de dólares.

Esta es la primera vez que un presidente estadounidense dio su discurso ante una sesión bicameral flanqueada por dos mujeres, la vicepresidenta Kamala Harris y la presidenta de la Cámara Baja, Nancy Pelosi.

“Después de 100 días, he venido a decir que Estados Unidos está en movimiento de nuevo”, dijo al comienzo de su discurso.

El mandatario demócrata aseguró que había prometido 100 millones de vacunas en 100 días, “pero hemos dado 220 millones de vacunas en 100 días”, dijo.

“Ahora todo el mundo es elegible para vacunarse”, agregó. “Ahora mismo, de inmediato, vayan a vacunarse, estadounidenses”.

“Todos sabemos, la vida nos puede pegar duro”, dijo en otra parte del discurso, en el que recordó el duro momento que atraviesa el país por la pandemia de COVID-19. “Pero en Estados Unidos, nunca, nunca, nunca bajamos los brazos. Los estadounidenses siempre nos levantamos, eso es lo que estamos haciendo “.

Biden dijo que el sistema de salud debe ser más accesible a todos y prometió hacer un mejor control a los precios de las medicinas en los Estados Unidos, que aseguró es de los más altos del mundo.

Por otra parte, el mandatario demócrata prometió que este año su administración encontrará el camino para bajar a la mitad la pobreza infantil.

El mandatario también resaltó los logros alcanzados para hacer frente a la crisis sanitaria generada por el coronavirus: “Puedo decir que gracias a ustedes, el pueblo estadounidense, nuestro progreso en los últimos 100 días en contra de una de las peores pandemias de la historia ha sido uno de los mayores logros logísticos en la historia del país”.

En contraste a su antecesor Donald Trump, el mandatario demócrata reconoció el problema que enfrenta EEUU y el mundo por el cambio climático. “Por mucho tiempo, no utilizamos la principal palabra para enfrentar la crisis climática: cuando pienso en cambio climático, pienso en crear empleos”.

Sobre este tema, Biden aseguró que enfrentar el problema del cambio climático implica un cambio en la industria como es el paso a energías renovables.

“El American Jobs Plan (propuesta del presidente estadounidense para invertir $ 2 billones en infraestructura) pondrá a los ingenieros y obreros a trabajar en la construcción de edificios y viviendas con mayor eficiencia energética”, dijo Biden. “El American Jobs Plan ayudará a millones de personas a volver a sus trabajos y carreras”.

“Wall Street no construyó este país. Fue la clase media. Y los sindicatos construyeron a la clase media”, agregó.

Joe Biden aseguró que ha creado 1.300.000 empleos en 100 días, “más que ningún otro presidente en sus primeros 100 días”.

En otra parte del discurso, Joe Biden hizo énfasis en la necesidad de generar empleos en EEUU y en que el país recupere su centralidad en el mundo. “Estamos en competencia con China y otros países para ganar el siglo XXI”, dijo.

“Estados Unidos se enfrentará a las prácticas comerciales injustas que socavan a los trabajadores y las industrias estadounidenses”, dijo sobre la guerra comercial con el gigante asiático.

“Todas las inversiones en el American Jobs Plan se guiarán por un principio:” Compre productos estadounidenses “, agregó Biden.

El demócrata dijo que para ganar la competencia por el futuro, también necesitan invertir en las familias estadounidenses para que estas puedan progresar.

El plan de $ 1.8 billones incluye fondos para la educación preescolar universal, dos años de universidad gratuita y un programa nacional de cuidado infantil, entre otras medidas.

“Cuando esta nación universalizó 12 años de educación pública en el siglo pasado, nos convirtió en la nación mejor educada y mejor preparada del mundo”, dijo. “Doce años ya no son suficientes hoy para competir en el siglo XXI”, agregó.

Joe Biden también consideró que “ya es hora” de que las grandes corporaciones y los más ricos de Estados Unidos, que son un uno por ciento, “paguen su parte justa” de impuestos.

“Solo la parte justa”, reiteró Biden en su discurso, en el que señaló que las fortunas y las grandes empresas tienen que ayudar pagando con sus impuestos las inversiones públicas que su gobierno se propone acometer.

Biden señaló que un reciente estudio asegura que el 55 por ciento de las grandes empresas pagó “cero” impuestos federales el año pasado, y lograron 40.000 millones de dólares en beneficios, mientras que muchas evadieron impuestos o se acogieron a beneficios y deducciones por emplear a sus trabajadores en otros países. “Y eso no está bien”, denunció.

“No impondré ningún aumento de impuestos a las personas que ganen menos de $ 400,000 al año”, aclaró. “Es hora de que las empresas estadounidenses y el 1% más rico de los estadounidenses paguen lo que les corresponde”.

“La IRS (agencia impositiva) tendrá mano dura con los millonarios y billonarios que hacen trampa con sus impuestos”, agregó.

Biden instó a la oposición republicana durante su primer discurso a que se una a los demócratas para restringir el acceso a las armas.

”No quiero ser beligerante ni nada, pero necesitamos que más republicanos se unan a la inmensa mayoría de sus colegas demócratas para cerrar las lagunas legales e imponer verificaciones de antecedentes para comprar un arma”, dijo el presidente

En contraste con su antecesor, el mandatario estadounidense también se dirigió a la comunidad LGBTI. “Quiero que sepan que su presidente los respalda”, dijo.

En materia de política internacional, Biden dejó en claro a Putin que no desea una escalada del conflicto entre EEUU y Rusia. Sin embargo, advirtió que cualquier acción de Moscú tendrá consecuencias.

“Respondí de manera directa y proporcionada a la interferencia de Rusia en nuestras elecciones y a los ciberataques a nuestro gobierno y empresas”, dijo Biden, refiriéndose a las sanciones que impusieron a Rusia.

“Trabajaremos con nuestros aliados para responder a las amenazas que suponen Irán y Corea del Norte a través de la diplomacia y una férrea disuasión”, agregó.

También aseguró que EEUU se mantendrá firme en su compromiso para defender los derechos humanos en todo el mundo, también en China, quien considera esos intentos como una injerencia de Washington.

Por otra parte, Biden prometió que Estados Unidos tendrá un papel más activado para enfrentar la pandemia de COVID-19 en el mundo que ha dejado más de 3 millones de muertos. “EEUU hará un arsenal de vacunas para otros países”, afirmó.

También mencionó la retirada de las tropas estadounidenses de Afganistán, que estará completada para el próximo septiembre cuando se cumple el vigésimo aniversario de los ataques del 11-S, y subrayó que esta guerra nunca debería haber durado tanto.

Casi al cierre de su discurso, Biden instó a que el Legislativo apruebe este año una normativa que otorgue protección a los “soñadores”, jóvenes indocumentados llegados al país siendo menores.

El mandatario demócrata —que busca marcar una diferencia con respecto a la dura política migratoria adoptada por Donald Trump— también llamó a dar protección a los migrantes beneficiados con el Estatuto de Protección Temporal (TPS) para originarios de países que sufren catástrofes naturales o violencia política.

“Si creen que necesitamos una frontera segura, apruébenla. Si creen en una vía hacia la ciudadanía, apruébenla. Si realmente quieren resolver el problema, les he enviado el proyecto, ahora apruébenlo”, afirmó Biden.

El líder demócrata dijo que “durante más de 30 años los políticos han hablado sobre la reforma migratoria y no han hecho nada” y sostuvo que “ha llegado el momento de resolverlo”.