+++
author = ""
date = 2021-10-25T03:00:00Z
description = ""
image = "/images/55250-2.jpg"
image_webp = "/images/55250-1.jpg"
title = "EL GOBIERNO BUSCA AVANZAR CON UN PLAN DE RESCATE DE LA COOPERATIVA SANCOR"

+++

##### **_El Gobierno buscó este lunes "acercar posiciones" para avanzar en un rescate de la cooperativa Sancor, que tiene deudas millonarias, ​en una reunión en la Casa Rosada del jefe de Gabinete, Juan Manzur, con el ministro de Agricultura, Ganadería y Pesca, Julián Domínguez; y el secretario general de la Asociación de Trabajadores de la Industria Lechera de la República Argentina (Atilra), Héctor Ponce._**

"Hablamos sobre el rescate Sancor. Hace mucho tiempo que estamos viviendo este estado de situación y confiamos en la ayuda del Estado para rescatar una empresa que se ha constituido por mérito propio en referente de la lechería y de la actividad a nivel nacional. Estamos trabajando en ese sentido", explicó Ponce a los periodistas acreditados en la Casa Rosada.

Fuentes oficiales, en tanto, indicaron a Télam que la reunión convocada por Manzur tuvo el fin de "acercar posiciones" para encontrar una salida a la situación de Sancor, que tiene un importante pasivo, y plantas en Santa Fe y Córdoba.

Ponce explicó que Sancor "tiene aproximadamente 1500 trabajadores en forma directa y trabajo en forma indirecta en función de que hay distintas actividades que tienen que ver con la fabricación de productos lácteos".

"Hoy por hoy la situación tiene que ver con que nosotros entendemos que hay que salvar a la empresa y básicamente los puestos de trabajo. Este es el tema fundamental por el cual nosotros venimos", explicó acerca de la situación crítica de la cooperativa, que ya motivó el año pasado varias reuniones con funcionarios del Gobierno nacional.

Al respecto, ante una consulta de Télam, Ponce contó que fueron recibidos este lunes por Manzur y el ministro Domínguez y tienen "el compromiso de parte del Estado argentino de colaborar a los efectos de que la empresa continúe y los puestos de trabajo sigan preservándose".

"No gestionamos pero nos vemos en la obligación de recurrir a todas aquellas cuestiones que nos permitan de alguna forma tratar de encontrar alguna alternativa de solución y eso es lo que estamos buscando", insistió.

En tanto, el secretario general de Atilra, ante otra consulta sobre los controles de precios por parte de la Secretaría de Comercio Interior, evaluó que eso "no solo se ejerce sobre el precio final del producto terminado, sino en lo que queda de la cadena".

Sobre este punto, dio el ejemplo de lo que "ocurre en Europa", donde según dijo "se controla absolutamente todo, desde el inicio hasta la finalización del producto".

"No es un control de precios sino es simplemente que actividad pueda estar no controlada pero sí para todos estén en sintonía con lo que debe ser el control, para que en definitiva quien va a pagar tenga que pagar realmente lo que corresponde", agregó Ponce.