+++
author = ""
date = 2021-05-05T03:00:00Z
description = ""
image = "/images/arton115813.jpg"
image_webp = "/images/arton115813.jpg"
title = "DECANO DE LA FADU SE ALEJÓ DE SU CARGO, ACUSADO DE VIOLENCIA DE GÉNERO"

+++

**_El decano de la Facultad de Arquitectura, Diseño y Urbanismo (FADU) de la Universidad de Buenos Aires (UBA), Guillermo Cabrera, pidió licencia en su cargo hasta que se esclarezca una denuncia judicial en su contra por un hecho de violencia de género._**

Según trascendió, Cabrera habría tenido una relación extramatrimonial con una empleada del establecimiento. En un momento dado la mujer lo denunció por un acto violento. Los detalles de esa denuncia quedaron bajo reserva. Pero la fiscalía se comunicó con la institución para interiorizarse acerca de la situación laboral de los protagonistas. Eso llevó a que el Comité de Género de la Facultad tomara intervención.

De acuerdo a las consultas, la empleada prefirió que esa situación se canalizara solo por vía judicial, donde actualmente está en proceso de investigación. Su nombre y los detalles de su denuncia no se darán a conocer para preservar su identidad.

Frente a ello, Cabrera decidió tomar 90 días de licencia a la espera de que se esclarezca la situación. El pedido se formalizó a fines de marzo a través de una carta que se hizo pública.

“La solicitud está motivada en el hecho de haber sido notificado de una denuncia judicial que involucra a mi persona. A pesar de que tengo plena certeza de la falsedad de la misma, tomo esta decisión con el fin de preservar la institución a la que he brindado más de 30 años de trabajo, con compromiso y respeto, entendiendo que nuestra Facultad y Universidad están por encima de las personas”, planteó.

“Situaciones como estas pueden ser aprovechadas para especulaciones políticas fuera de los contextos que corresponden y es precisamente lo que creo mi deber evitar. Confío en la Justicia y me pondré a disposición de la misma para demostrar mi inocencia, poner a su alcance toda la información pertinente y aclarar todas las situaciones que me sean requeridas”, dijo en su misiva.

Hubo algunos integrantes del Consejo Directivo de la FADU que exigieron que se dieran a conocer los detalles de la denuncia. Pero no hubo más datos al respecto.

La licencia vence el día 23 de junio del 2021. De acuerdo a las consultas hechas por Infobae, Cabrera no volvería a su cargo hasta que no haya avances sobre la situación judicial. Así se lo hizo saber a sus colaboradores. La conducción de la FADU quedó en manos del vicedecano Carlos Venancio.

Más allá del rumbo que tomo la causa, la Facultad se encontraría impedida de tomar otra clase de medidas teniendo en cuenta que fue la víctima la que solicitó que la casa de estudios no tomara intervención.

En cuanto a Cabrera, inició su carrera docente en 1988. En 2010 fue Secretario Académico de la Facultad de Arquitectura, Diseño y Urbanismo de la Universidad de Buenos Aires. En 2017, se convirtió en decano, en la elección tomada por el Consejo Directivo, durante una sesión extraordinaria. Su cargo dura cuatro años.

Cabrera llegó al decanato auspiciado por su antecesor inmediato Luis Bruno, representando la continuidad de una coalición amplia donde confluyen sectores radicales, del peronismo y el kirchnerismo.