+++
author = ""
date = 2021-05-07T03:00:00Z
description = ""
image = "/images/canciller-claudia-blum-saudo-cuerpo-diplomatico-1-1.jpeg"
image_webp = "/images/canciller-claudia-blum-saudo-cuerpo-diplomatico-1.jpeg"
title = "DURA RESPUESTA DE LA CANCIELLER COLOMBIANA A  ALBERTO FERNÁNDEZ POR SUS DICHOS SOBRE LAS MANIFESTACIONES"

+++

**_La Cancillería de Colombia emitió un durísimo comunicado en respuesta a los dichos del presidente argentino Alberto Fernández quien por medio de su cuenta de Twitter había cuestionado el accionar de la administración de Iván Duque en la crisis desatada a partir de las múltiples manifestaciones que se dan en las principales ciudades colombianas. “Alimenta la polarización”, acusó la dependencia dirigida por Claudia Blum._**

“Cancillería, en nombre del Gob. de COL, rechaza firmemente las declaraciones del presidente Alberto Fernández, que desconocen que miles de colombianos han tenido, conforme a nuestro Estado de Derecho, todas las garantías para ejercer la protesta pacífica a lo largo y ancho del país”, comienza el mensaje emitido por la cuenta oficial de el ministerio de relaciones exteriores de Duque.

Fernández publicó su mención sobre la crisis colombiana en la noche del jueves: “Con preocupación observo la represión desatada ante las protestas sociales ocurridas en Colombia”, expresó el jefe de estado argentino en su cuenta de aquella red social. “Ruego porque el pueblo colombiano retome la paz social”, continuó y agregó que “instaba” al gobierno de Duque a que, “en resguardo de los derechos humanos, cese la singular violencia institucional que se ha ejercido”, lo que provocó la respuesta de Bogotá.

La cancillería colombiana continuó: “El Gobierno Nacional ha convocado y adelanta diálogos con todos los sectores del país. La institucionalidad democrática colombiana protege los derechos constitucionales de los colombianos y no será desprestigiada por este tipo de pronunciamientos que, además de ser una intromisión arbitraria, buscan alimentar la polarización que no contribuye a la convivencia y al consenso”.

La crisis que sacude Colombia fue el rechazo popular a la reforma tributaria impulsada por Duque. Pese a que el presidente anunció el retiro del proyecto impositivo y que estaba dispuesto a negociar uno nuevo con la oposición, el descontento de ciertos sectores continuó. Hechos vandálicos se viven en las principales ciudades colombianas con quemas de comercios y saqueos. Como resultado de la represión y excesos policiales que están bajo investigación ya son 26 las víctimas fatales.

La vicepresidenta Marta Lucía Ramírez respaldó las conclusiones que hizo el presidente ecuatoriano Lenin Moreno luego de una investigación hecha por parte de autoridades colombianas y ecuatorianas y aseguró que Nicolás Maduro pretende exportar su modelo al país vecino.

Ayer mismo el oficialista Partido Socialista Unido de Venezuela (PSUV) que dirige la dictadura venezolana emitió un comunicado condenando al gobierno de Duque. El PSUV dijo que “rechaza y condena al narcogobierno de Iván Duque y su mentor, Álvaro Uribe Vélez, que dieron la orden al Ejército y la Policía colombiana para masacrar al pueblo colombiano que hace uso de su legítimo derecho a la protesta pacífica contra medidas antipopulares y neoliberales”. El chavismo señaló además que el “hermano pueblo de Colombia sufre en sus calles el asesinato de sus hijos e hijas en manos de quienes manchan sus manos y conciencia con la sangre de quienes solo alzan su voz, pero reciben la descarga de fusiles”.

“Una briza (sic) bolivariana sopla en Colombia, un huracán bolivariano hará justicia y paz”, concluye el documento chavista.

#### Naciones Unidas en Colombia

En la mañana de este viernes, 7 de mayo, la Misión de Verificación de Naciones Unidas en Colombia emitió un comunicado frente a las jornadas de manifestaciones, que iniciaron el pasado 28 de abril, que iniciaron en protesta a la reforma tributaria propuesta por el gobierno de Duque.

En la misiva, la ONU en Colombia reconoce actos de violencia en los que se han perdido vidas humanas y, entre ellos, se destaca el uso desproporcionado de la fuerza por parte de las autoridades locales. Esto ya había sido denunciado por Marta Hurtado, portavoz del Alto Comisionado para los Derechos Humanos, quien denunció que policías abrieron fuego contra manifestantes en Cali, Valle del Cauca.

Finalmente, la ONU observa de manera positiva la agenda de diálogo que estableció el Gobierno nacional para llegar a consensos que desmonten el paro nacional. Sin embargo, la organización espera que las mismas cuenten con pluralidad y resultados. “Espera que estos se lleven a cabo de manera inclusiva y con vistas a obtener resultados concretos que puedan brindar una solución pacífica a la actual coyuntura”, aseveró.