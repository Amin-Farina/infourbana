+++
author = ""
date = 2022-02-21T03:00:00Z
description = ""
image = "/images/cabandie-y-valdez.jpg"
image_webp = "/images/cabandie-y-valdez.jpg"
title = "EL GOBERNADOR DE CORRIENTES DIJO QUE CABANDIÉ LO LLAMÓ EN MEDIO DEL DESASTRE PARA QUEJARSE POR LAS CRÍTICAS DE MACRI"

+++

##### **_Pareciera ser que la política no sabe de emergencias ni desastres naturales. Así quedaría demostrado en caso de confirmarse un dato revelado este lunes por el gobernador de Corrientes, Gustavo Valdés, integrante de Juntos por el Cambio._**

El mandatario de la provincia que se encuentra asediada por los incendios aseguró que en medio de la emergencia el ministro de Ambiente, Juan Cabandié, lo llamó para quejarse por las críticas del ex Presidente.

> “”Llamalo a Macri para que deje de tuitear en mi contra”; ¿existió ese pedido de Cabandié? ¿Era lo que más le preocupaba?”, preguntó el periodista Eduardo Feinmann en radio Mitre.

“No sé cómo se filtró eso, pero estaba preocupado por el posicionamiento político. Yo puedo controlar mi cuenta de Twitter, no la de los demás. Nosotros teníamos que trabajar para sofocar el fuego”, contestó Valdés.

El viernes pasado el ex Presidente cuestionó a Cabandié en redes sociales por no haber socorrido a tiempo a los correntinos con aviones hidrantes, vigías y equipamiento.

Quien salió al cruce del ex jefe de Estado no fue Cabandié sino su segundo, Sergio Federovisky: “Es falso, hay una docena de medios aéreos y más de 100 brigadistas en Corrientes y Misiones”.

“Lo que no recuerda es que en 2017 se descuartizó el plan de manejo del fuego y lo recibimos en 2020 sin presupuesto alguno”, agregó.

Independientemente del cruce tuitero, la relación política entre Valdés y Cabandié se deterioró hasta tal punto que el correntino prefiere otros interlocutores de la Casa Rosada. No le interesa hablar con él porque cree que no podrá llegar a buen puerto en ninguna de sus comunicaciones Le cayó muy mal la acusación que hizo el ministro al asegurar que el gobierno nacional había ofrecido ayuda en enero y que la provincia no la había aceptado.

“Ofrecimos recursos el 23 de enero y pidieron ayuda el 5 de febrero”, había dicho durante una entrevista. En la provincia lo desmienten. “El gobierno provincial viene alertando desde el mes de noviembre sobre el impacto de la sequía, la baja del río y el riesgo de incendios que generaban las altas temperaturas”, indicaron desde el Poder Ejecutivo correntino.

El gobernador fue protagonista en las últimas horas de una insólita polémica: fue cuestionado por haber pedido ayuda a los Estados Unidos para apagar los incendios. Este lunes explicó: “Lo pedimos porque tienen aviones de mayor porte, hasta un Boeing 747 con gran capacidad para transportar agua; que me digan cipayo o lo que quieran, pero todo lo que nos puedan mandar a nosotros nos sirve”, contestó.

En ese contexto, agradeció a intendentes, gobernadores y hasta al presidente de Brasil que recientemente han tenido gestos solidarios para colaborar con la batalla contra el fuego. También se refirió a las colectas realizadas por el club River Plate, el influencer Santiago Maratea y otras organizaciones civiles y particulares que se organizaron para colaborar.