+++
author = ""
date = 2021-08-18T03:00:00Z
description = ""
image = "/images/abjqv7pdjjahtd47jwebhjyxly.jpg"
image_webp = "/images/abjqv7pdjjahtd47jwebhjyxly.jpg"
title = "MORE RIAL INCURSIONA EN POLÍTICA ¿QUÉ PARTIDO POLÍTICO ELIGIÓ?"

+++

**_Aunque las listas de precandidatos para las PASO del 12 de septiembre ya están cerradas, este viernes Morena Rial anunció que estaba decidida a incursionar en política con la mira puesta en las elecciones de 2023. “Para cambiar las cosas, no queda más que hacerlo uno mismo”, aseguró en su cuenta de Instagram._** 

Luego de ese publicación se esperaba que la mediática contara cuál era el partido al que se sumaría y no faltaron especulaciones al respecto. Finalmente, el misterio terminó en la noche del martes.

El abogado Alejandro Cipolla reveló que el espacio que eligió junto a su defendida es el Partido Renovador Federal. “La verdad es que nos gustó que sea algo nuevo, que no venga del mundo de la política”, comentó. Y en se sentido, destacó a su referente: Martín Maganas, el empresario que está al frente de la productora de aceite Ecoliva.

Según explicó a este medio, el líder del partido comparte las mismas inquietudes que ellos. “Es referente de las PYMES. También nos importó que sea referente en una fundación que lucha contra la discriminación”, sostuvo. En ese sentido, ya había contado que su deseo es que los actos de discriminación en la Argentina constituyan un delito y no solo una contravención.

Además de este tema, Cipolla y More se mostraron en las redes sociales a favor de la legalización de la marihuana. “Al igual que Alexander Caniggia, ya dije públicamente mi posición. Es muy probable que esté presentado próximamente un proyecto de ley para que la tenencia no sea punible”, señaló el representante legal.

Por su parte, Maganas manifestó su felicidad por la adhesión de la famosa a su partido. “Nosotros desde nuestro espacio agradecemos que cualquiera se quiera sumar. En este caso More, con buenas ideas y ganas de hacer. A partir de acá hay que analizar cuál es la mejor alternativa para que todos construyamos algo juntos”, afirmó

Asimismo, valoró que figuras como Cinthia Fernández y Brian Lanzelotta aspiren a un cargo público: “Todos tienen derecho a participar gente con ganas y gestión nueva, porque los que están se equivocan todos los días, no tienen gestión privada ni una gestión exitosa. Amalia Granata tampoco tenía experiencia y hoy es diputada. ¿Por qué Morena no podría prepararse?”. Y concluyó: “No hay nada que le impida focalizarse en eso, y las buenas ideas e intenciones ya las tiene, es una buena persona y eso es lo que necesita la política”.