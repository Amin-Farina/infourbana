+++
author = ""
date = 2022-03-08T03:00:00Z
description = ""
image = "/images/migrantes-ucranianos.jpg"
image_webp = "/images/migrantes-ucranianos.jpg"
title = "ARGENTINA OTORGARÁ VISA HUMANITARIA A CIUDADANOS UCRANIANOS"

+++

#### **_La disposición otorga el estatus de protección temporal para garantizar el ingreso y permanencia en el país de ucranianos y familiares directos, con un plazo de permanencia de hasta 3 años en el país._**

A través de una resolución de la Dirección Nacional de Migraciones (DNM), organismo dependiente del Ministerio del Interior, el gobierno nacional autorizó el ingreso y la permanencia por razones humanitarias a la Argentina a ciudadanos ucranianos y sus familiares directos, independientemente de su nacionalidad, como respuesta a la crisis ocasionada por el conflicto bélico que atraviesa ese país.

La medida del ministerio a cargo de Wado De Pedro y de Migraciones, dirigida por Florencia Carignano, apunta a facilitar y garantizar la reunificación familiar de los ciudadanos ucranianos.

Además, otorga el estatus de protección temporal para garantizar el ingreso y permanencia en el país de ucranianos y familiares directos, con un plazo de permanencia de hasta 3 años en nuestro país. Vencido ese plazo, los beneficiarios pueden solicitar y acceder a la residencia definitiva en nuestro país.

Desde Migraciones resaltaron que la decisión "tiene por objeto brindar protección a aquellas personas que, no siendo refugiadas o asiladas, momentáneamente no pueden permanecer o retornar a su país, al tiempo que las personas beneficiadas estarán eximidas del pago de las tasas migratorias atendiendo la crisis que atraviesa Ucrania"