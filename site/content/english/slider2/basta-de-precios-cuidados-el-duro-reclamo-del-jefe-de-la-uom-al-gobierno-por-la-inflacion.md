+++
author = ""
date = 2022-01-24T03:00:00Z
description = ""
image = "/images/antonio-calo-ok-1.jpg"
image_webp = "/images/antonio-calo-ok.jpg"
title = "“BASTA DE PRECIOS CUIDADOS”: EL DURO RECLAMO DEL JEFE DE LA UOM AL GOBIERNO POR LA INFLACIÓN"

+++

#### **_El secretario general de la Unión Obrera Metalúrgica, Antonio Caló, pidió “tomar el toro por las astas y resolver el problema de fondo”, en relación a la suba de precios._**

Tras lograr un incremento adicional del 15% y cerrar la paritaria en 50,2% hasta abril -fecha en la que volverán a negociar-, el líder de la Unión Obrera Metalúrgica, Antonio Caló, pidió corregir la inflación.

> “Sacamos 50,2% de aumento. Podés sacar 60% pero la plata no alcanza nunca. Si no se corrige la inflación no va a arreglarse nunca”, aseguró Caló luego del acuerdo con los empresarios del sector.

En una entrevista radial con Radio Con Vos (FM 89.9), el secretario general de la UOM aseguró que “siendo optimista” cree que puedan terminar el 2022 “empatados con la inflación”.

Asimismo, fue muy duro con el gobierno, al que le pidió “agarrar el toro por las astas”. “Basta de precios cuidados. Hay que resolver el problema de fondo y ponerlo arriba de la mesa para corregir la inflación entre todos”, expresó.

Ya en octubre, Caló había sido duro con el aumento de la inflación cuando en una entrevista se preguntó por qué se generan incrementos de precios si “no hubo aumentos” en los últimos meses de los servicios públicos y los combustibles y consideró que “los formadores de precios de la Argentina no demuestran por qué aplican las subas”.

#### El acuerdo con el FMI y la negociación de Guzmán

Así como le pidió al gobierno corregir el “problema de fondo de la inflación”, Caló también apuntó contra el gobierno de Mauricio Macri por el acuerdo con el Fondo Monetario Internacional (FMI).

“El acuerdo tiene que llegar. El FMI le hizo un préstamo a Macri para gane las elecciones. Tiene que reconocer que se equivocó”, aseguró el gremialista, quien también opinó sobre las negociaciones actuales.

En ese sentido, se refirió a la gestión que lleva adelante el ministro de Economía, Martín Guzmán, en el acuerdo del pago de la deuda al FMI: “A Guzmán lo apoyamos desde siempre. Es un muchacho joven y tiene una formación profesional que no he visto en otros ministros”.

“Creo que lo tiene claro que no va a haber acuerdo sin el hambre de los argentinos. Para pagar hay que producir”, agregó Caló.

##### El diálogo con los empresarios y la vacunación

Antonio Caló, secretario general de la UOM, brindó detalles sobre la relación que llevan adelante con los empresarios: “Estamos discutiendo los salarios y más o menos nos ponemos de acuerdo. Con los empresarios tenemos un diálogo perfecto”.

“Y de hecho, invitamos a los empleados a que se vacunen: tenemos que tratar de cuidarnos entre todos. Si 80% se vacuna, ¿qué le pasa al 20% que no?”, agregó Caló sobre el riesgo epidemiológico que vive el sector.