+++
author = ""
date = 2021-05-03T03:00:00Z
description = ""
image = "/images/tarjetas-bbva-1024x682.jpg"
image_webp = "/images/tarjetas-bbva-1024x682.jpg"
title = "EL USO DE TARJETAS DE DÉBITO Y DEL AHORA 12 AUMENTÓ DURANTE LA PANDEMIA"

+++

**_De acuerdo con el indicador Índice Prisma Medios de Pago, en el primer trimestre el consumo con tarjeta de débito representó el 48,31% del volumen del uso de la misma, 4,5 puntos porcentuales mayor al 48,31% que significó en el mismo periodo de 2020._**

La pandemia de coronavirus disparó el uso de tarjetas de débito, que aumentaron 33% interanual en el primer trimestre del año, ganándole espacio al efectivo, y también la utilización del Ahora 12, que en todas sus modalidades representó el 57,8% del volumen de las compras en cuotas con tarjetas de crédito realizadas entre enero y marzo, destacó un informe privado.

"En el primer trimestre se aceleró el aumento del uso de tarjeta de débito y la tarjeta de crédito se estabilizó. La irrupción de la pandemia catapultó el uso de la tarjeta de débito, que creció 33% en la comparación anual, ganándole espacio al efectivo", destacó el director de Relaciones Institucionales de Prisma Medios de Pago, Julián Ballarino.

El Índice Prisma Medios de Pago se elabora en base la información del Instituto Nacional de Estadística y Censos (Indec), el Banco Central (BCRA), medios de pago, billeteras digitales, las terminales de cobro de tarjetas, cajeros automáticos, el pago electrónico de impuestos y servicios, y las transacciones a través de plataformas comerciales.

De acuerdo con este indicador, en el primer trimestre el consumo con tarjeta de débito representó el 48,31% del volumen del uso de la misma, cifra que resultó 4,5 puntos porcentuales mayor al 48,31% que significó en el mismo periodo de 2020.

Por otra parte, el informe destacó que los planes Ahora sostuvieron una importante participación en las compras con tarjeta de crédito.

"Al analizar el financiamiento elegido al pagar en cuotas se destaca que los Planes Ahora siguen siendo los preferidos por los consumidores para realizar compras de largo plazo", amplió Ballarino.

Asimismo, uno de cada cuatro pesos que se movieron en el uso de tarjetas de crédito fue motorizado por alguna de las opciones de los planes incluidos en el Ahora 12, de 3, 6, 12 y 18 cuotas.

Esto significó un crecimiento en cuanto a la utilización del programa de la Secretaría de Comercio Interior, dado que hace un año atrás esa proporción era de uno de cada cinco pesos.

De las cuatro modalidades, el Ahora 12 representó el 46,89% del volumen de compras realizadas con el programa, el Ahora 18 significó el 19,88%; el Ahora 6 el 17,22%; y el Ahora 3 el 16,01%.

El resultado del Indice Prisma coincidió con un estudio recientemente difundido por Red Link, sobre el uso de dinero electrónico.

Ese trabajo consignó que casi 40 millones de personas y empresas habían utilizado en marzo pasado dinero electrónico vía transacciones online o con tarjetas de crédito o débito en la Argentina, un 25,4% más que igual mes del año pasado.