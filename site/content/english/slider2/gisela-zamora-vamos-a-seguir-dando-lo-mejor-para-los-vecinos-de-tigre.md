+++
author = ""
date = 2021-09-10T03:00:00Z
description = ""
image = "/images/gisela-zamora-tigre-3-1.jpg"
image_webp = "/images/gisela-zamora-tigre-3.jpg"
title = "GISELA ZAMORA: \"VAMOS A SEGUIR DANDO LO MEJOR PARA LOS VECINOS DE TIGRE\""

+++

**_Gisela Zamora que encabeza la lista de precandidatos concejales del Frente de Todos en Tigre y busca su reelección para ocupar una banca en el HCD, realizó un balance de gestión y anticipó nuevas propuestas para el desarrollo del municipio._**

Durante las últimas dos semanas, Gisela Zamora encabezó junto al intendente Julio Zamora una serie de actos de campaña en distintas instituciones del distrito llevando propuestas  y repasando las acciones de gestión llevadas adelante por el municipio de Tigre en los últimos años.

“La verdad que el recibimiento de los vecinos es positivo, porque valoran el trabajo del municipio durante pandemia y nos cuentan que se sienten acompañados por la gestión”, dijo al ser entrevistada por el periodista Daniel Burone Risso.

En ese sentido, destacó que durante la pandemia “el intendente Julio Zamora hizo un trabajo de mucho compromiso para garantizar que todos accedan a la salud, y lo hizo con los tres hospitales diagnóstico inmediato que fueron inaugurados previo a la pandemia y se convirtieron en centros de Covid donde pudimos salvar cientos de vidas”.

Gisela Zamora contó que se está reforzando el sistema de salud del distrito “Con ampliaciones de los servicios en los centros de salud y colaborando con el hospital de  Pacheco”,  además contó que “en pocos días más vamos a estar recibiendo en el Hospital de Diagnóstico Inmediato el primer resonador magnético nuclear de partido”.

En materia de seguridad consideró que “Tigre es el mejor lugar para vivir, contamos con más de 2000 cámaras, tenemos los móviles del COT, los botones antipánico, los tótem en los espacios públicos y las aplicaciones de seguridad para celulares”.

Además recordó que en los últimos años “el municipio renovó 62 espacios públicos, donde la familia entera puede disfrutar de su tiempo libre, tanto los más chicos en los juegos o los jóvenes practicando deportes”.

En metería de inversión cultural, la concejala dio el ejemplo del teatro “Pepe Soriano” de Benavidez “se trata de un importante bien cultural que hace poquitos días reabrió sus puertas”, al tiempo que recordó “fue inaugurado en el 2019 previo a la pandemia, tuvo seis meses abiertas sus puertas y muchos vecinos pudieron asistir por primera vez a un teatro, después durante la pandemia tuvimos que reconvertirlo en un centro vacunatorio y miles de vecinos pudieron contar con el mejor lugar para recibir su vacuna”.

“Julio es un intendente muy cercano a la gente”, destacó en un tramo de la entrevista, al tiempo que destacó  “recorre continuamente los barrios, así que se lo puede encontrar en una plaza o recorriendo un centro de salud, es  un intendente con el que se puede charlar y plantearle las necesidades y no solamente queda en la escucha, sino que junto a su equipo busca las soluciones”.

“Por eso creo que Tigre tiene todo para vivir,  tiene seguridad,  Plazas,  estamos avanzando en materia de salud,  tenemos un centro universitario, un hospital materno, un centro de ojos y el odontológico, en definitiva encaramos un conjunto de políticas que se hacen que el vecino viva mejor”, detalló.

Respecto al sector gastronómico y turístico que sufrió seriamente los efectos de la cuarentena analizó: “la pandemia generó un momento difícil para la gastronomía y para todo el corredor turístico de Tigre, hoy estamos en una etapa distinta gracias a la vacunación y vemos que eso también es una gran oportunidad, porque somos uno de los municipios de cercanía elegidos para venir a disfrutar del Delta, del Puerto de Frutos o Villa la Ñata,  tenemos también una puerta gastronómica amplia y estamos trabajando para mejorar la oferta en materia hotelera”.

También adelantó algunos proyectos que se estudian en materia turística: “Tenemos un proyecto que estamos trabajando que tiene que ver con el corredor del Paseo Victoríca,  la ideas es empezar a convertirlo un lugar con oferta más nocturna”, y detalló “todas estas ideas las trabajamos con nuestros comerciantes que son los que proponen los proyectos, nosotros los estudiamos con el equipo con el que además estamos preparándonos para esta temporada que viene, creo que vamos a hacer uno de los destinos más elegidos de la provincia”.

Al ser consultada sobre su reelección como concejal de Tigre, contestó “En 2017 me eligieron concejal y desde ahí siempre trabajé junto a Julio Zamora en este proyecto, estuve en los momentos más importantes de Tigre, con la inauguración de los hospitales, con la renovación de los espacios, pero también estuve en el momento más difícil de la pandemia junto a los equipos de trabajo cuidando nuestros vecinos y vamos a seguir dando lo mejor, vamos a estar al lado de nuestro vecino haciendo de que ese lugar sea el mejor lugar para vivir”.