+++
author = ""
date = 2021-09-27T03:00:00Z
description = ""
image = "/images/lingotes-oro-dolares-1.jpg"
image_webp = "/images/lingotes-oro-dolares.jpg"
title = "¿EL BANCO CENTRAL TENDRÁ QUE VENDER SU ORO PARA CONTENER EL DÓLAR?"

+++

#### **_Analistas calculan que en pocos meses las reservas líquidas caerán a cero y la entidad tendrá que tomar decisiones difíciles. Un acuerdo con el FMI que postergue pagos de deuda podría evitar la caída._**

El Gobierno está pagando un costo muy alto por mantener a las cotizaciones paralelas del dólar lejos del centro de la escena en la carrera final hacia las elecciones de noviembre. Aunque no haya una corrida en marcha, las tensiones en los mercados de dólar financiero y libre se hacen sentir y hacen mella en las reservas internacionales del Banco Central. Las tenencias líquidas de la entidad que conduce Miguel Pesce van en descenso y, con fuertes pagos al FMI por delante, amenazan con llegar a cero en los próximos meses. Y, de llegar a ese punto, la autoridad monetaria podría verse forzada a hacer lo impensado: vender sus reservas de oro.

El análisis de distintos economistas está lejos de predecir que Pesce va a poner en el mercado los lingotes de oro del Banco Central. Lo que hacen es marcar una dinámica que, más temprano que tarde, va a necesitar de un cambio drástico. Tan sencillo como cerrar un acuerdo con el FMI.

El Banco Central vende reservas en el mercado cambiario formal para abastecer la demanda de importadores y ahorristas. Al mismo tiempo, también vende reservas en dólar contado con liquidación para mantener a raya esa cotización paralela. Y, por último, las usa para pagar vencimientos de deuda como los casi USD 1.900 millones que pagó al Fondo este mes. Todos esos usos no se pueden mantener por mucho tiempo más.

Un cálculo del economista Gabriel Rubinstein, ex representante de Roberto Lavagna en el Banco Central, estimó que la tendencia lleva a las reservas líquidas a cero tan pronto como en noviembre de este año. Y en ese punto, la entidad deberá buscar formas de utilizar el oro de sus reservas o buscar vías para utilizar los encajes de depósitos que los bancos están obligados a colocar en el Banco Central.

“Eso ya se usó en el pasado varias veces, con una resolución del Banco Central que no es del todo pública. El problema es el miedo que puede generar en los depositantes que exista la posibilidad del uso de encajes de de los depositantes. Es un temor que puede estar subyacente y que te puede generar una corrida contra los depósitos”, dijo Rubinstein.

“Es absolutamente necesario cerrar con el FMI, sin acuerdo sos boleta, porque la situación de reservas es muy crítica. Toda esta demora no tuvo sentido, se perdieron reservas con la idea de que es impopular. La situación es crítica ahora, lo va a ser mucho más aún. Toda esta demora costó reservas bajo la lógica de que un acuerdo es impopular”, comentó.

El análisis es similar entre otros especialistas. Con el plan “poner plata en el bolsillo” en marcha, la consultora Econviews de Miguel Kiguel estima que la demanda de dólares sólo podrá aumentar.

“Con más dinero en la calle no está escrito en ningún lado que la gente no vaya a comprar dólares con lo que llegue en forma directa o indirecta. Dicho de otro modo, es más probable que se agrande la demanda que la oferta de dólares en este escenario”, analizó la consultora en un informe.

“Con una demanda creciente y una oferta que escasea, el pronóstico es reservado. La dinámica de las reservas para las próximas semanas apunta a que cuando llegue el calorcito de diciembre el BCRA empezará a contar los lingotes de oro para empezar a venderlos. Lo único positivo de esta situación es que debería generar pocas dudas de los beneficios de ir a buscar un acuerdo con el Fondo Monetario Internacional. Sin programa, la corrección cambiara que a esta altura es inevitable carecería de anclas para evitar un over-shooting desestabilizante”, agregó el análisis.

“Nuestro cálculo de reservas internacionales netas se compone de a) las reservas líquidas, que son los dólares contantes y sonantes que se usan para sostener la cotización oficial, a través del MULC y la cotización paralela, a través del mercado de bonos; b) los Derechos Especiales de Giro (DEGs), que se usarán para el pago de los vencimientos de capital e intereses al FMI; y c) el Oro, cuya cotización varía”, abundó Econviews.

Según los cálculos de la consultora, las reservas internacionales netas en poder del BCRA son alrededor de USD 7.500 millones. De ellos, USD 3.100 millones son DEGs, otros USD 3.600 millones están colocados en oro y USD 800 millones son divisas líquidas.

“De acá diciembre la suma de los pagos de deuda con el FMI, con otros organismos internacionales y la intervención proyectada es de USD 3.571 millones. Lo que significa que para los primeros días de enero estaríamos empezando a vender oro para sostener el mercado de cambios, si no antes. Y esto no contempla las turbulencias políticas que podrían sucederse ante un resultado electoral similar o peor para el oficialismo. En la práctica se pueden usar dólares de encajes o agrandar el préstamos con el BIS, pero en esencia es como vender el oro a cuenta”, concluyó.