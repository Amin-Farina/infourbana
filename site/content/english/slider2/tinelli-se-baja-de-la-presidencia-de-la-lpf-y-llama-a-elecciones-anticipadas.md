+++
author = ""
date = 2022-01-10T03:00:00Z
description = ""
image = "/images/tinelli-declaraciones-clp_862x485.jpeg"
image_webp = "/images/tinelli-declaraciones-clp_862x485-1.jpeg"
title = "TINELLI SE BAJA DE LA PRESIDENCIA DE LA LPF Y LLAMA A ELECCIONES ANTICIPADAS"

+++

##### **_Mediante una nota, el empresario y animador televisivo puso a disposición su renuncia y fijó la fecha del 31 de marzo de 2022 para elegir a las autoridades que completarán el mandato vigente hasta marzo de 2024._**

Marcelo Tinelli puso a disposición su cargo de presidente de la Liga Profesional de Fútbol (LPF) y realizó un llamado a elecciones anticipadas el próximo 31 de marzo, en una carta dirigida a los 28 clubes que componen la Primera División del fútbol argentino.

El empresario y conductor televisivo concluyó así de forma precipitada su gestión, que tenía vencimiento en marzo de 2024 e ingresó seriamente en crisis frente a un planteo formal realizado por 15 instituciones, el 24 de diciembre pasado.

"Es tiempo de abordar temas esenciales. Y estoy convencido que para ello resulta indispensable estar más unidos que nunca como clubes de Primera División. Es por esta razón que, junto con los integrantes de la Mesa Directiva y del Consejo Directivo de la Liga, que acompañan esta nota firmando, invitamos a toda la dirigencia a iniciar conversaciones para buscar nuevas autoridades de la LPF", introduce la nota.

"Hemos fijado la fecha del 31 de marzo de 2022 para la elección de nuevas autoridades de la Liga, que completen el mandato vigente hasta marzo de 2024", plantea el escrito que lleva la firma de Tinelli, los tres vicepresidentes Cristian Malaspina (Argentinos Juniors), Hernán Arboleya (Lanús) y Mario Leito (Atlético Tucumán); el director general Eduardo Spinosa (Banfield) y el secretario general Sergio Rapisarda (Vélez), entre otros.

Los aspirantes a la presidencia para reemplazar a Tinelli tendrán tiempo de presentar su candidatura hasta el 1 de marzo, conforme el Reglamento Electoral de la Asociación del Fútbol Argentino (AFA).

"Creemos firmemente que este es el mejor camino y por eso debemos tomarnos este tiempo para encontrar los consensos y la unidad que el momento exige. Tal lo conversado con la mayoría de los clubes, permaneceré en el cargo hasta que se elijan nuevas autoridades. Confío en que este escenario desemboque en una lista de unidad con el máximo consenso posible", anheló.

##### El camino hacia la renuncia

El llamado a unidad de Tinelli se produce dos semanas después de denunciar un "golpe (institucional) artero" con el pedido de remoción exigido en la carta que divulgaron 15 clubes a horas de la Nochebuena.

Los representantes de Aldosivi de Mar del Plata, Lanús, Argentinos Juniors, Independiente, Gimnasia La Plata, Estudiantes de La Plata, Godoy Cruz de Mendoza, Rosario Central, Huracán, Defensa y Justicia, Arsenal, Colón de Santa Fe, Platense, Sarmiento y Central Córdoba de Santiago del Estero exigieron una reunión extraordinaria del Comité Ejecutivo en las 72 horas posteriores a la presentación para tratar un cambio en la presidencia de la LPF.

Tinelli respondió de inmediato con otra misiva en la que rechazó ese pedido y fijó la fecha del 11 de enero, cuando se sortee el nuevo campeonato 2022, para dialogar sobre la crisis política, algo que finalmente quedó abortado con el llamado a elecciones.

Al martes siguiente, 29 de diciembre, los 15 clubes firmantes más Boca Juniors, Racing Club y Barracas Central participaron de una reunión en las oficinas de Puerto Madero en la que se acordó pedirle a Tinelli que fije fecha de elecciones para una salida ordenada en términos institucionales.

El mensaje más fuerte en el encuentro de ese martes fue la presencia de Juan Román Riquelme, vicepresidente de Boca, con había tenido un roce con el titular saliente de la LPF antes del clásico con San Lorenzo del torneo pasado, que el "Xeneize" debió asumir con juveniles por motivos sanitarios tras un viaje a Brasil.

En una nota con Télam, el vicepresidente primero de la LPF, Cristian Malaspina, adelantó que el pedido de remoción impulsado por la dirigencia del fútbol argentino respondía a la nula relación entre Tinelli y el presidente de la AFA, Claudio "Chiqui" Tapia.

"La falta de gestión se está llevando puestos a todos los clubes. Tinelli debe reconocer que evidentemente no tiene tiempo ni ganas de conducir este barco", afirmó el titular de Argentinos Juniors, uno de los que podría presentarse en los próximos comicios.

Los clubes le exigían a Tinelli avances en temas relativos a la televisión, el juego de apuestas on line, el decreto 12-12 derogado por Mauricio Macri, que eliminó rebajas en las cargas impositivas, la seguridad y el tipo de cambio.

"Hasta que se determinen los nuevos líderes de la LPF -por consenso o elecciones-, la Liga seguirá funcionando en forma plena, con todo el equipo de trabajo enfocado en la organización e implementación para la disputa de los torneos y la defensa de los intereses comunes, aún en medio de una feroz pandemia que permanentemente le pone obstáculos al desarrollo y a la industria de nuestra Primera División", prometió Tinelli.