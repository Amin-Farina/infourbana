+++
author = ""
date = 2021-12-31T03:00:00Z
description = ""
image = "/images/5ef5cba48c860_900-1.jpg"
image_webp = "/images/5ef5cba48c860_900.jpg"
title = "AEROLÍNEAS ARGENTINAS ADVIRTIÓ QUE POR LA OLA DE CONTAGIOS PUEDE HABER SUSPENSIONES Y CANCELACIONES DE VUELOS"

+++

#### **_Pablo Ceriani, el presidente de la empresa, señaló que están realizando un esfuerzo muy grande para sostener las operaciones en plena temporada de verano_**

Aerolíneas Argentinas, informó que debido a la creciente suba de los contagios de Covid-19, la operación de sus vuelos puede sufrir cambios o reprogramaciones. Así lo transmitió Pablo Ceriani, presidente de la empresa, a través de su cuenta de Twitter.

“Debido a la nueva ola de contagios que estamos viviendo en todo el país, que también afecta al personal de Aerolíneas Argentinas, queremos informar que nuestras operaciones pueden sufrir demoras, modificaciones y/o cancelaciones”, señaló.

“Ante esta situación, estamos realizando un esfuerzo muy grande para sostener las operaciones en plena temporada de verano y que afecte lo menos posible los itinerarios de los vuelos. Les pedimos paciencia, cualquier novedad de sus vuelos será comunicada por los canales oficiales”, agregó.

Además, el titular de Aerolíneas Argentinas comentó que es la misma situación que están sufriendo todas las aerolíneas y que solamente el pasado 2 de enero, fueron cancelados 4.000 vuelos en todo el mundo.

Desde la empresa indicaron que aquellos pasajeros que puedan ser afectados recibirán una notificación vía el correo electrónico de contacto que hayan informado en su reserva con la modificación de su vuelo. Aerolíneas Argentinas, se encuentra realizando una importante operación de temporada de verano con 230 partidas diarias entre vuelos, nacionales, regionales e internacionales.

Hasta el momento, solo fue suspendido un vuelo a Bariloche con 170 pasajeros, que terminó saliendo un día después. Los empleados trabajan en burbujas, y si aparece un caso positivo, todo el grupo debe aislarse, lo que complica las operaciones. El área de mantenimiento es una de las que se vio afectada por los contagios. Los equipos de trabajo se van rearmando y ajustando a contra reloj para poder cumplir con los vuelos previstos.

Con todo, hasta el momento, la ola de contagios no tuvo un impacto muy fuerte en la caída de reservas.

El domingo se cancelaron más de 4.000 vuelos en todo el mundo, más de la mitad de ellos en Estados Unidos, debido a condiciones climáticas y un aumento de casos de coronavirus por la variante ómicron. Los vuelos cancelados hasta el domingo pasado, incluían más de 2.400 que entraban, salían u operaban dentro de Estados Unidos, según el sitio web FlightAware.com. A nivel mundial, más de 11.200 vuelos fueron atrasados.

Las fiestas de Navidad y Año Nuevo suelen ser una época de gran tráfico aéreo a nivel global, pero la rápida propagación de ómicron ha provocado un fuerte aumento de las infecciones por COVID-19, obligando a las aerolíneas a cancelar vuelos por aislamientos de sus pilotos y su personal de cabina.

Tripulantes de cabina, pilotos y personal de apoyo de las aerolíneas se mostraron reacios a trabajar horas extras en las vacaciones, pese a las ofertas de fuertes incentivos económicos. Muchos temían contraer el COVID-19 y no veían con buenos ojos la perspectiva de tener que lidiar con pasajeros indisciplinados, dijeron algunos sindicatos de aerolíneas.

A nivel mundial, según información de la agencia Reuters, en los meses previos a las vacaciones, las aerolíneas estuvieron buscando empleados, después de haber despedido a miles de personas en los últimos 18 meses, cuando la pandemia afectó a la industria.