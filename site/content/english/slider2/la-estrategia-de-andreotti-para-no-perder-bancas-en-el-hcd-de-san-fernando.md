+++
author = ""
date = 2021-08-08T03:00:00Z
description = ""
image = "/images/1440x810_45003176_2x-1.jpg"
image_webp = "/images/1440x810_45003176_2x.jpg"
title = "LA ESTRATEGIA DE ANDREOTTI PARA NO PERDER BANCAS EN EL HCD DE SAN FERNANDO"

+++

Siete son las bancas locales que pone en juego el Frente de Todos (FdT) en esta elección de medio término en San Fernando y hasta los más optimistas admiten que, aún en un escenario de victoria, será difícil para el oficialismo retener esos lugares en el Concejo Deliberante (HCD).  

El Andreotismo en San Fernando llega a esta elección habiendo sorteado un movimiento político más que relevante: se sumó al Frente de Todos (FdT) en el 2019 y realizó un cambio de mando en el ejecutivo municipal, de Luis a Juan Andreotti, sin que esto haya mermado la buena imagen que tienen los vecinos de la gestión.

Estos dos primeros años de mandato del joven intendente Juan Andreotti se han desarrollado en medio de un contexto mundial inédito: le tocó ser el rostro local en la administración de una pandemia. Y en lo político, también muestra un escenario novedoso: es el primer período desde que gobierna el Frente Renovador (FR) en San Fernando, que lo hace con los gobiernos municipal, provincial y nacional alineados en el mismo signo partidario. 

En un panorama de tanto cambio, lo que parece tener definido el oficialismo es una continuidad: mantendrá en esta elección la estrategia de llevar el apellido “Andreotti” al frente de la lista. Será la hermana mayor del intendente y actual secretaria Eva Andreotti, quien encabece la lista local del FdT. La rápida y efectiva identificación del apellido con la gestión, el peso de la marca “Andreotti” superarían, en la balanza del oficialismo, a cualquier crítica por la continuidad familiar en los primeros puestos electivos desde 2011. 

Al apellido “Andreotti” se le suma como valor diferencial en este arranque de campaña el buen posicionamiento que tiene San Fernando en las últimas semanas en los rankings de distritos con mayor porcentaje de población vacunada con al menos una dosis.

Sobre estos ejes el Andreotismo comienza a delinear su campaña electoral, donde por primera vez deberá deberá dar cuenta, no solo de errores y aciertos propios, sino también de las gestiones de Alberto Fernandez y Axel Kicillof, con menos margen para la estrategia de la tijera, tan conocida para el oficialismo local.

##### LAS BANCAS DEL FRENTE DE TODOS

De las 7 bancas que pone en juego el FdT, que responden a la elección del 2017, 5 son del FR y 2 del por entonces Frente para la Victoria (FPV). Los cálculos del oficialismo es que ganará la elección consiguiendo 6 de las 10 bancas en juego. Pero eso abre un escenario de negociación de listas donde el FdT perderá espacios.

Por parte del Andreottismo, las negociaciones las lleva adelante el intendente, con acuerdo del líder del espacio, el ex intendente Luis Andreotti para la definición de la nómina. El que se sienta a la mesa por parte del núcleo kirchnerista es el diputado provincial Matías Molle, de La Cámpora. La mesa del FdT que a nivel nacional tuvo 3 patas, en el 2019 en San Fernando tuvo dos. No hubo representantes del Albertismo. Quien podría sumarse, de buscar replicar el modelo nacional sería Gustavo Aguilera. subsecretario en el Ministerio de Desarrollo nacional.

La experiencia del último cierre de listas muestra al FR negociando poco: en los primeros 5 lugares fueron concejales andreottistas y no abrió espacios del ejecutivo a ninguno de los nuevos aliados. Fuentes cercanas a los negociadores esperan pocas novedades al respecto en esta nueva definición, siendo los 5 primeros lugares para dirigentes de confianza del ex intendente, y la posible pérdida de lugares quede para los espacios del kirchnerismo.

#### LAS BANCAS DE JUNTOS

Después de la dura derrota electoral del 2019 (quedó a mas de 30 puntos del FdT), la alianza opositora se encuentra en un escenario electoral favorable para achicarle margen al oficialismo y sumar 1 o 2 bancas a las 3 que pone en juego.

Fundamentalmente, JxC deberá reelaborar una narrativa opositora después de haber estado 4 años tentando al Andreottismo para sumarse a Cambiemos. Y desde 2019, sin dos referentes locales de peso en el territorio, ya que Alex Campbell y Sebastián Salvador emigraron a la legislatura provincial y nacional respectivamente, enflaqueciendo el volumen político opositor en el distrito. 

Aún sin definirse la interna a nivel nacional, por el momento a nivel local habría tres listas de JxC, una por cada candidatura nacional.

Fuentes de la lista de la UCR oficialista (que lleva a Facundo Manes encabezando la lista nacional) evalúan por estas horas la posibilidad que Sebastián Salvador vuelva a disputar el distrito y posicionarse como candidato a intendente en 2023. Salvador tiene aún 2 años más de mandato como diputado y lo más probable es que los complete, siendo finalmente un militante de muchos años en el espacio quien encabece esa lista. 

Por el PRO (con Diego Santilli como candidato nacional) irá por su segundo mandato en el HCD la última candidata a intendenta, Agustina Ciarletta. Y en la lista que encabezaría el intendente de San Isidro, Gustavo Posse a nivel nacional, le han ofrecido la candidatura al “Chino” Luna, ídolo de los hinchas de Tigre, pero parece difícil que el futbolista acepte. En esa lista buscaría su reelección el radical Federico Fernández Storani.