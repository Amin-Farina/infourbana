+++
author = ""
date = 2021-11-08T03:00:00Z
description = ""
image = "/images/cristina-jorgelarrosapagina12-1.jpg"
image_webp = "/images/cristina-jorgelarrosapagina12.jpg"
title = "RECHAZARON UN PLANTEO DE LA OPOSICIÓN PARA REVISAR LA DOBLE JUBILACIÓN DE CRISTINA KIRCHNER"

+++

##### **_La Cámara Federal de la Seguridad Social rechazó hoy un pedido de la oposición para que se revise la doble jubilación que cobra la vicepresidenta de la Nación, Cristina Kirchner. El tribunal no aceptó un planteo que hizo la semana pasada Graciela Ocaña, diputada nacional y candidata para renovar su banca por “Juntos” en la provincia de Buenos Aires, para que se declare nula la decisión de la ANSES de no apelar el fallo que benefició a la ex presidenta._**

El juez de la Sala III de la Cámara Néstor Fasciolo no aceptó el planteo de Ocaña porque es el mismo que un grupo de legisladores de la oposición ya había hecho anteriormente y porque la resolución judicial que le permite a Cristina Kirchner cobrar las dos jubilaciones ya está firme, informaron las fuentes judiciales.

En diciembre del año pasado, el juez de primera instancia de la Seguridad Social Ezequiel Pérez Nami hizo lugar a una causa judicial que inició en 2017 Cristina Kirchner y la autorizó a cobrar una doble pensión: la suya como ex presidenta de la Nación y la su fallecido marido, el ex presidente Néstor Kirchner, y a cobrarlo de manera retroactiva por el tiempo que no las percibió y sin la aplicación del impuesto a las ganancias. El monto de las dos pensiones que cobra la vicepresidente asciende a 2.5 millones de pesos y el retroactivo a 120 millones. Desde que la ex mandataria cobra los dos ingresos desistió de percibir su sueldo como vicepresidenta.

El reclamo se inició porque el gobierno de Mauricio Macri, a través del Ministerio de Desarrollo Social, había dictado una resolución por la cual dispuso que Cristina Kirchner debía optar por una de las dos pensiones.

El fallo del juez Pérez Nami fue apelado por la ANSES para que sea revisado por la Cámara Federal de la Seguridad Social. Pero en marzo el procurador del Tesoro de la Nación, Carlos Zannini, dictó una resolución por la cual revocó esa decisión. “No existe incompatibilidad en la percepción de los beneficios reconocidos por medio de la Disposición CNPA número 5135/10 y la Resolución MDS número 3193/15″, sostuvo la Procuración.

Con ese dictamen, la ANSES actual retiró la apelación que había presentado para que el fallo de primera instancia sea revocado. “Prestando conformidad con el desistimiento formulado por la actora respecto de su planteo de quedar eximida del impuesto a las ganancias y solicitando, por las razones que más abajo se exponen, que se declare abstracta la cuestión debatida en autos y, consecuentemente, se tenga por desistida la apelación interpuesta por mi representada (ANSES)”, sostuvo la titular del organismo, Fernanda Raverta, en la presentación judicial.

Con eso quedó firme el fallo que habilitó a Cristina Kirchner a cobrar las dos pensiones en simultáneo. Pero Ocaña se presentó la semana pasada en la justicia y pidió que se revoque la decisión de la ANSES de no apelar el fallo y continuar en la causa, como hace la ANSES en los expedientes de jubilaciones que llegan hasta la Corte Suprema de Justicia de la Nación.

“El desistimiento del organismo produce un enorme perjuicio al erario público, que es el dinero recaudado con el esfuerzo de todos los argentinos, que en nuestro carácter de representantes del Pueblo nos vemos obligados a defender, ante la escandalosa actitud de los funcionarios que deberían ocuparse de ello”, sostuvo Ocaña en su presentación de 29 páginas. Agregó que “nadie podría considerar válido que un funcionario en ejercicio de dicha facultad, autorice el desistimiento de una apelación con el objeto de favorecer a su líder política o a su hermano o a un amigo. Es decir, la facultad no es absoluta ni se encuentra exenta de control. Debe ejercitarse dentro del marco de la ley”.

El juez de la Cámara Fasciolo rechazó el planteo. Explicó que “el nuevo pedido de intervención de tercero formulado por la Sra. Diputada Nacional María Graciela Ocaña en tal carácter y a título individual, es reiteratorio del presentado anteriormente por la nombrada en conjunto con otros legisladores”. Y que sobre ese primer pedido “recayó sentencia interlocutoria de fecha 30 de septiembre del corriente por la que este tribunal desestimó de modo expreso y categórico ese planteo por no darse la condiciones de admisibilidad previstas por el art. 90 y concordantes del C.P.C.C.N”. Es decir, que no es parte de la causa.

“El mentado pronunciamiento, debidamente notificado, quedó firme y consentido; y que las motivaciones alegadas no alteran lo decidido, estese a lo allí resuelto”, concluyó el juez.