+++
author = ""
date = 2021-08-04T03:00:00Z
description = ""
image = "/images/clipping_znp4kt_77bf.jpg"
image_webp = "/images/clipping_znp4kt_77bf.jpg"
title = "¿QUÉ VACUNAS SE PODRÁN COMBINAR PARA COMPLETAR EL ESQUEMA?"

+++

**_El Gobierno ya tiene definido cuáles serán las combinaciones de vacunas que se podrán efectuar a partir de ahora para completar los esquemas de vacunación en Argentina. La resolución es consecuencia del análisis de estudios y experiencias internacionales para poder suplantar la falta de segundas dosis de Sputnik V que sufre el país por la demora de Rusia en el envío de los componentes._**

Frente a ese escenario de incertidumbre, que empujó a la Casa Rosada, a través de la asesora presidencial Cecilia Nicolini, a presionar al Fondo de Inversión Ruso por el incumplimiento del contrario, el Ministerio de Salud decidió acelerar el estudio de las combinaciones para contar con una herramienta más para completar el ciclo de vacunación de todos los que tienen la primera dosis de Sputnik V.

La Comisión Nacional de Inmunizaciones (Conain), en una reunión ampliada junto al Comité de Expertos, resolvió, por consenso mayoritario, que la evidencia de los ensayos clínicos que se llevaron adelante durante las últimas semanas son suficientes para, con los primeros datos preliminares de seguridad inmunogenicidad, poder avanzar en las combinaciones.

Las personas que empezaron el esquema de vacunación con la primera dosis de Sputnik V, podrán completarlo con dosis de AstraZeneca o de Moderna. En tanto, aquellos que lo arrancaron con las vacunas de AstraZeneca lo van a poder completar con las de Moderna, un proceso que ya se está realizando en el Reino Unido y Alemania.

Si una persona que tiene colocada la primera dosis de Sputnik V y decide esperar la segunda dosis de la misma vacuna, será citada a los vacunatorios cuando haya disponibilidad de esas dosis.

La decisión de completar el esquema con una vacuna distinta depende de cada persona. Es optativo. Tiene la opción de hacerlo o de esperar a cerrar el ciclo con la segunda dosis de la misma vacuna. Pero podrá elegir una vez que se cumpla la ventana de tiempo estimada entre las dos dosis. Ese tiempo es de 8 semanas tanto para la Sputnik V como para la de AstraZeneca. Una vez que se cumpla el período, podrán optar.

En orden de prioridades, la primer opción va a ser completar el esquema con las dosis del mismo laboratorio. La segunda será combinar las dosis de distintas vacunas.

Todas las autoridades sanitarias de las provincias están trabajando para identificar a las personas que hayan pasado ese límite de tiempo desde su primera dosis, con el objetivo de estratificar prioridades e iniciar la vacunación con las segundas dosis. En especial buscan a los mayores de cincuenta años y personas con condiciones de riesgo.

Los estudios que se llevan a cabo cuentan con la cooperación del Fondo Ruso de Inversión Directa y aún se están llevando a cabo en Buenos Aires, Ciudad Autónoma de Buenos Aires (CABA), Córdoba, La Rioja y San Luis. Estudian los esquemas heterólogos con las diferentes vacunas contra COVID-19 disponibles en el país con el objetivo de evaluar su inmunogenicidad y seguridad.

En cuanto a la seguridad, hasta el momento no se registraron efectos adversos graves en ninguno de los grupos. La mayoría de los eventos informados son los ya conocidos y asociados a la vacunación tales como dolor en el sitio de aplicación, cefalea y astenia (cansancio).

La resolución sobre las combinaciones llega en el inicio de un mes clave para el Gobierno en lo que respecta a la gestión sanitaria. La variante Delta ya llegó a la Argentina y en pocos días, según asumen en la cartera de Salud, comenzará a circular comunitariamente.

Con esa proyección, el objetivo oficial es completar la mayor cantidad de ciclos de vacunación para que haya más ciudadanos protegidos frente al impacto de la nueva cepa. Las dos dosis son determinantes para reducir las hospitalizaciones y la tasa de mortalidad. Por eso en el Gobierno definen a agosto como “el mes de las segundas dosis”. Es lo único importante en estas próximas semanas.

En la cartera que conduce la ministra Carla Vizzotti esperan, de mínima, poder completar durante el próximo mes el 60% de la cobertura con dos dosis de los mayores de 60 años. Incluso creen que podrían elevar ese porcentaje a un 90%. Todo dependerá del ritmo de vacunación que puedan imprimir los distritos en lo que resta del mes.

Argentina recibió de Estados Unidos una donación de 3.500.000 de dosis de las vacunas de Moderna. De ese total, 1.800.000 fueron reservadas para aplicarle a los adolescentes, de entre 12 y 17 años, que tengan comorbilidades. A ese remanente se le sumarán las 20.000.000 de dosis que contiene el contrato que el gobierno de Alberto Fernández firmó con Moderna a principios de julio y que recién llegarían en el primer trimestre de 2022.

Hasta el momento en Argentina hay 25.454.176 personas vacunadas con una dosis y 7.469.534 con las dos. Esa diferencia es la que buscan achicar a toda velocidad para combatir la variante Delta, que es más contagiosa y que podría generar un nuevo pico de casos.

De todas formas, en el Ministerio de Salud consideran que el país vive otra etapa de la pandemia y que si bien puede haber una gran cantidad de casos, el porcentaje de vacunados que hay, permitirá que bajen las internaciones, por lo que el sistema sanitario no tendrá riesgo de colapso.