+++
author = ""
date = 2022-01-27T03:00:00Z
description = ""
image = "/images/614e5c66c8578-1.jpg"
image_webp = "/images/614e5c66c8578.jpg"
title = "KULFAS CUESTIONÓ A LOS DIRIGENTES KIRCHNERISTAS QUE PROMUEVEN EL DEFAULT: “ES UNA IRRESPONSABILIDAD”"

+++

##### **_El ministro de Desarrollo Productivo salió al cruce del kirchnerismo duro y defendió la negociación del Gobierno con el FMI para reestructurar la deuda. “No hacer ningún acuerdo es una hipótesis que no se valida con la realidad”, señaló_**

“Poner en riesgo la reactivación por una hipótesis, que el acuerdo con el FMI sería mucho peor, me parece un acto de voluntarismo y, en algunos casos, de irresponsabilidad”. Sin dar nombres, el ministro de Desarrollo Productivo, Matías Kulfas, criticó hoy a dirigentes del kirchnerismo duro, como el diputado Leopoldo Moreau o la economista Fernanda Vallejos, que esta semana plantearon el default como alternativa a un acuerdo con el organismo de crédito multilateral para reestructurar la deuda argentina.

En este sentido, y en diálogo con Radio Con Vos, el funcionario nacional señaló: “Estoy convencido que es mucho mejor para la Argentina hacer un acuerdo que nos permita seguir creciendo a, como escucho por ahí, no hacer ningún acuerdo porque eso de por sí va a ser malo; esa hipótesis no se valida con la realidad”.

La negociación entre el Gobierno y el FMI se encuentra en momentos de definición, sobre todo porque entre el viernes y el martes próximo la Argentina deberá pagar vencimientos por USD 1.100 millones. En este marco, ayer, Moreau planteó una mirada crítica sobre las condiciones que impone el Fondo: “Default es una palabra que aterroriza, pero tampoco tiene que aterrorizar tanto como para creer que es el peor de los remedios”, sostuvo.

El dirigente oficialista, en este sentido, agregó en una entrevista radial: “Nos empujan al default, no somos el partido del default, pero tampoco somos un sector político que va a entregar de pies y manos a la sociedad al peor de los ajustes, a más miseria a la que estamos atravesando, de la que recién ahora estamos saliendo, no nos vamos a jugar en una timba la democracia”.

“¿Es peor el default? Néstor Kirchner gobernó la Argentina prácticamente dos o tres años en default, hasta que logró los acuerdos, con los bonistas y el FMI, y mal no le fue, esas divisas que no utilizó en pagar la deuda la usó para reactivar la economía”, completó.

En la misma línea se manifestó Vallejos, que publicó un duro mensaje contra el organismo de crédito y el gobierno de Joseph Biden en medio de las negociaciones. “No existe, en la historia moderna, mayor violador sistemático de los DDHH de los pueblos que el FMI, instrumento de EEUU para someter a los países deudores a sus intereses geopolíticos y al del capital transnacional que hace negocios en nuestras economías”, comenzó la economista en sus redes sociales.

Y continuó: “¿Por qué no se ocupa EEUU de los DDHH a la salud, a la educación, del pueblo norteamericano? ¿Por qué no habla “la comunidad internacional” de los DDHH de los negros y latinos en EEUU? ¿O de cómo financió vía FMI la implantación del neoliberalismo con dictaduras en Chile y Argentina?”.

Consultado por estas declaraciones, Kulfas replicó: “Escucho que se dice que hemos estado en default y la economía creció, durante el gobierno de Néstor Kirchner. En ese momento estuvimos en default pero con acreedores privados, no con el FMI, con el Fondo hubo una demora de 72 horas en el pago, eso no califica como un evento de default”.

“Tenemos que actuar con responsabilidad, la negociación se ha planteado en estos términos, de que efectivamente no haya un ajuste sino que Argentina pueda seguir creciendo”, enfatizó el ministro nacional, e insistió: “Es una fantasía esta idea de que no le pago al Fondo y esa plata la uso para otra cosa, es no tomar en cuenta la multiplicidad da elementos que también van a afectar a la economía argentina”.

Por otro lado, y aunque evitó dar una definición sobre si el Gobierno cancelará o no el vencimiento de USD 731 millones, sostuvo que “pagar los compromiso no es un gesto de buena voluntad o no, es pagar los compromisos, te pueden gustar o no gustar; de ninguna manera fue algo bueno acordar con el FMI en 2018, fue un error garrafal, ahora los compromisos están, corresponde pagarlos.

Más allá de esta postura, y más relacionado con las declaraciones de Moreau y Vallejos, la vicepresidenta Cristina Kirchner criticó a los organismos de crédito y apuntó a “las políticas de ajuste”. Fue en Honduras, a donde fue para participar de la asunción presidencial de Xiomara Castro: “Es curioso que los que impulsaron los programas de ajuste después dicen que hay que combatir al narco”, sostuvo.