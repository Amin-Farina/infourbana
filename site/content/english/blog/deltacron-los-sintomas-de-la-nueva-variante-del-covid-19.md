+++
author = ""
date = 2022-03-14T03:00:00Z
description = ""
image = "/images/deltacron-1.jpg"
image_webp = "/images/deltacron.jpg"
title = "DELTACRON: LOS SÍNTOMAS DE LA NUEVA VARIANTE DEL COVID-19"

+++
#### Las autoridades globales temen que esta nueva mutación, denominada Deltacron, pueda volver a cambiar el curso de la pandemia.

A principios de enero, la noticia de una “supervariante” de COVID-19, que es una mezcla entre Delta y Ómicron, volvió a encender las alarmas mundiales. Fue el virólogo Leondios Kostrikis, profesor de Ciencias Biológicas, jefe del Laboratorio de Biotecnología y Virología Molecular y responsable del grupo de la Universidad en Nicosia, quien identificó a esta mutación y anunció públicamente el 7 de enero la existencia de la Deltacron en Chipre, específicamente en 25 casos de personas contagiadas.

Por entonces, el experto aseveró que habían caracterizado varios genomas del virus del covid que mostraban elementos de las variantes Delta y Ómicron. No obstante, científicos de todo el mundo acusaron que esa mutación sería el resultado de una contaminación de laboratorio y no una nueva variante.

No obstante, Kostrikis defendió su postura y descartó que se trate de una contaminación de laboratorio, ya que ambas variantes presentan particularidades a la hora de secuenciarlas. Incluso, afirmó: “Este argumento fue encabezado por las redes sociales sin considerar nuestros datos completos y sin proporcionar ninguna evidencia real y sólida de que no es real”. “Veremos en el futuro si esta variante es más patológica o contagiosa que las anteriores o si se impondrá a Delta y Ómicron”, indicó.

Pese a las opiniones encontradas sobre la Deltacron, la Organización Mundial de la Salud (OMS) confirmó la existencia de esta nueva variante.

“Tenemos conocimiento sobre esta recombinación. Es una combinación de Delta AY.4 y Ómicron BA.1. Se ha detectado en Francia, Países Bajos y Dinamarca, pero a niveles muy bajos. Esta recombinación era de esperar”, señaló Maria Van Kerkhoveen, epidemióloga líder de la OMS.

De acuerdo con la OMS, los síntomas de esta variante son los mismos que los experimentados por los infectados en la primera mutación del SARS-CoV-2, como:

* Secreción nasal.
* Tos.
* Fatiga.
* Dolor de garganta.
* Debilidad corporal.
* Fiebre.
* Posible pérdida del gusto y olfato.
* Estornudos.

Desde la OMS pidieron extremar los recaudos y medidas preventivas en caso de experimentar alguno de los síntomas anteriormente mencionados.

Ahora, uno de los asuntos que más preocupa a la OMS es la guerra de Rusia con Ucrania. Se debe a la baja tasa de vacunación en el país invadido y a la cantidad de gente que se refugia en países o regiones con bajos niveles de inmunidad frente al SARS-CoV-2. “Desafortunadamente, este virus aprovechará las oportunidades para continuar propagándose”, advirtió al respecto Van Kerkhove.

Mike Ryan, director ejecutivo del programa de Emergencias Sanitarias de la OMS, dijo que “sin dudas habrá un aumento de covid en Ucrania” y argumentó que eso será inevitable frente a la interrupción de las vacunas por la guerra y “una población estresada y cansada de la guerra con tasas de inmunidad muy bajas”.