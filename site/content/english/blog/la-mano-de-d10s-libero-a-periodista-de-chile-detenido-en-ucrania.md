+++
author = ""
date = 2022-03-08T03:00:00Z
description = ""
image = "/images/periodista-chileno-tatuaje-maradonajpg-1.jpg"
image_webp = "/images/periodista-chileno-tatuaje-maradonajpg.jpg"
title = "LA \"MANO DE D10S\" LIBERÓ A PERIODISTA DE CHILE DETENIDO EN UCRANIA"

+++

##### **_Daniel Matamala viajó desde Chile a Ucrania para cubrir el conflicto bélico. Fue detenido en un control policial y cuando estaba en la comisaría le vieron el tatuaje que identifica a Diego Maradona._**

El reconocido periodista chileno Daniel Matamala, enviado especial para cubrir la guerra en Ucrania, vivió un tenso momento cuando fue detenido en un control policial y escoltado a la comisaría, pero se salvó por un tatuaje de Diego Maradona.

> "Hoy, en uno de los controles de la ruta, la policía nos requisó documentos, cámaras, teléfonos, y nos escolaron a la comisaría. Los primeros interrogatorios fueron tensos: es un país en guerra y se sospecha de espías o saboteadores", indicó el corresponsal de Chilevisión a través de historias de Instagram.

La tensión fue aumentando debido a que, entre el nerviosismo por el momento y el idioma, los integrantes del equipo periodístico y los policías ucranianos no lograban entenderse.

> "Hasta que uno de los policías vio los pasaportes de mis colegas argentinos y entre un montón de palabras en ucraniano, dijo dos que entendimos ‘Messi’ y ‘Maradona‘. Ahí todo cambió. Nuestro gran camarógrafo mostró que tiene un tatuaje de Diego Maradona en la pantorrilla", relató el periodista chileno.

La imagen del astro argentino fue motivo suficiente para que el equipo periodístico pueda continuar su camino. "Así que nos dejaron libres y nos devolvieron los equipos. Nos rescató la mano de D1OS", destacó.