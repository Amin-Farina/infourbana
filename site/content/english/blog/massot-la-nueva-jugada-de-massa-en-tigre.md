+++
author = ""
date = 2021-07-06T03:00:00Z
description = ""
draft = true
image = "/images/massa-filmus-y-massot.jpeg"
image_webp = "/images/massa-filmus-y-massot.jpeg"
title = "MANES Y UN ACUERDO QUE FAVORECE LOS PLANES DE MASSA EN TIGRE"

+++

##### **_Sergio Massa avanza junto a Nicolás Massot en la interna de Juntos por el Cambio y busca debilitar a la oposición en Tigre. El ex diputado cordobés logró un lugar en la lista de Manes y fortaleció la estrategia de Sergio Massa_**

El ex Intendente de Tigre y actual presidente de la Cámara de Diputados, Sergio Massa, juega partidos simultáneos en varias canchas pero nunca deja de prestar máxima atención a Tigre donde ahora se mete en la interna de Juntos por el Cambio con la candidatura de Nicolás Massot.

> “A partir del 11 de diciembre tenemos que trabajar con Nicolás Massot y Emilio Monzó”. [_La frase citada por La Voz del Interior de Córdoba la pronunció Sergio Massa apenas cuarenta y ocho horas después de las Paso y luego de almorzar en Nueva York con el ex diputado cordobés Nicolás Massot con quienes son amigos._](https://www.lavoz.com.ar/politica/massa-pidio-por-massot-pero-fernandez-parece-ponerle-un-freno/ "Massa pidió por Massot, pero Fernández parece ponerle un freno") _(_[**_link_**](https://www.google.com/url?q=https://www.lavoz.com.ar/politica/massa-pidio-por-massot-pero-fernandez-parece-ponerle-un-freno/&sa=D&source=editors&ust=1625596506750000&usg=AOvVaw2O313C_MZ4eODah3J67e57)_)_

De acuerdo a lo consignado por el medio [_www.hora60.com_](http://www.hora60.com/massot-el-verdadero-plan-de-massa-en-tigre/ "MASSOT: EL VERDADERO PLAN DE MASSA EN TIGRE") _(_[**_link_**](http://www.hora60.com/massot-el-verdadero-plan-de-massa-en-tigre/)_)_ Nicolás Massot es el nombre de Sergio Massa para recuperar el territorio de Tigre. El Frente Renovador ya no cuenta entre los suyos al intendente Julio Zamora y con esta acción suman una jugada a dos puntas para debilitar a la oposición y colocar en ese rol a un Nicolás Massot muy cercano al actual y poderoso presidente de la Cámara de Diputados Sergio Massa.

También Infobae da cuenta del inicio de la operación en 2019 después de la derrota de Juntos por el Cambio. De acuerdo a ese medio el ex intendente de Tigre lo quiso convencer a Massot de que volviera a Buenos Aires y trabaje con el peronismo en la nueva etapa que se viene en la Argentina. [_Según www.hora60.com esa foto en Nueva York, con detalles del encuentro que publicó Infobae, omitió considerar el impacto en el distrito: fue la primera vez que ambos actores hablaron del pago chico_](http://www.hora60.com "Massot y Massa en Nueva York") _(_[**_LINK_**](http://www.hora60.com/massot-el-verdadero-plan-de-massa-en-tigre/ "MASSOT: EL VERDADERO PLAN DE MASSA EN TIGRE")_)_.

Pero el archivo rescata más señales. Ante la pregunta de Clarín si aceptaría un cargo en el gobierno de Alberto Fernández, Nicolás Massot oriundo de Bahía Blanca, afirmó: [_"En este momento no. Hay veces que hay que deponer ideas críticas y unirse. En algún determinado momento no lo descartaría…”](https://www.clarin.com/politica/nicolas-massot-elogio-maximo-kirchner-cualidades-hacen-bien-politica-_0_h1TB2-XF.html "Nicolás Massot elogió a Máximo Kirchner") ([**_Link_**](https://www.clarin.com/politica/nicolas-massot-elogio-maximo-kirchner-cualidades-hacen-bien-politica-_0_h1TB2-XF.html)) y opinó sobre Máximo Kirchner: “No estoy hablando del contenido; pero sí de las formas y de muchos prejuicios que se han construido sobre él. No es lo que mucha gente cree que es. Tiene cualidades que le hacen muy bien a la política".

Massot y Monzó cargan con una pesada factura en la interna de Juntos por el Cambio que además incomoda a la UCR. No solo le achacan al ex presidente de Bloque del Pro sus [_dichos despectivos sobre el radicalismo mientras creía que tenía el micrófono apagado en el programa de Antonio Laje en A24_](https://www.youtube.com/watch?v=UXSMl2Ro3vs) _(_[**_Link_**](https://youtu.be/UXSMl2Ro3vs)_)_ si no que [_lo responsabilizan junto a Gustavo Posse por la fractura del bloque en la legislatura bonaerense para darle a Axel Kicillof el manejo de la Cámara de Diputados Bonaerense como señaló La PolíticaOnLine.com_](https://www.lapoliticaonline.com.ar/nota/123425-monzo-parte-el-bloque-en-la-provincia-y-le-permite-a-kicillof-nombrar-a-otermin-y-cuto-moreno/) ([**_Link_**](https://www.lapoliticaonline.com.ar/nota/123425-monzo-parte-el-bloque-en-la-provincia-y-le-permite-a-kicillof-nombrar-a-otermin-y-cuto-moreno/))_._

En las últimas horas y con estos antecedentes a cuestas Nicolás Massot intentará lograr competir en Tigre de la mano de un acuerdo con Gustavo Posse y sin muchos más referentes de peso en el distrito que ven en su figura pocas posibilidades de éxito.