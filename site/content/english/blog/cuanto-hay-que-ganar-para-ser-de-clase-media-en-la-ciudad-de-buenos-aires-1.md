+++
author = ""
date = 2021-12-16T03:00:00Z
description = ""
image = "/images/20210402130524_familia_pobre_argentina-1.jpg"
image_webp = "/images/20210402130524_familia_pobre_argentina.jpg"
title = "¿CUÁNTO HAY QUE GANAR PARA SER DE CLASE MEDIA EN LA CIUDAD DE BUENOS AIRES?"

+++

#### **_Una familia porteña tipo necesitó ingresos de al menos $115.112,20 en noviembre para ser considerada de clase media según los criterios de estratificación social que utiliza la Ciudad Autónoma de Buenos Aires. Ese umbral, de unos $3.837,07 por día, subió 1,92% respecto al mes de octubre, por debajo de la inflación del mes. En términos interanuales, los ingresos necesarios para alcanzar ese nivel socioeconómico aumentaron casi 47,78% en comparación con mismo mes del año pasado._**

La Dirección General de Estadísticas y Censos de la Ciudad Autónoma de Buenos Aires publicó los datos de Costo de vida, Líneas de pobreza y Canastas de Consumo para la Ciudad de Buenos Aires del mes de septiembre de 2021. El reporte, entre otros datos, fija los criterios actualizados que toma el distrito para delimitar los niveles socioeconómicos en base al nivel ingreso.

Así, una familia de cuatro integrantes necesitó disponer de entre $115.112,20 y $368.359,03 durante el mes pasado para entrar en “sector medio”.

El reporte mensual que elabora establece una estratificación social en base a cinco niveles socioeconómicos.

\- En situación de indigencia: Hogares cuyo ingreso total mensual no alcanza para cubrir la Canasta Básica Alimentaria (CBA - Línea de indigencia). A noviembre, se consideraba indigentes a las familias que no alcanzaron los $38.944,86 mensuales.

\- En situación de pobreza no indigente: Hogares cuyo ingreso total mensual no alcanza para cubrir la Canasta Básica Total (CBT – Línea de pobreza) pero permite al menos adquirir la CBA. El rango de ingresos para este estrato se fijó entre $38.944,87 y $74.036,95 por mes.

\- No pobres vulnerables: Hogares cuyo ingreso total mensual es de al menos la CBT y no alcanza la Canasta Total (CT) del Sistema de Canastas de Consumo. Ganan entre $74.036,96 y $92.089,75 al mes.

\- Sector medio frágil: Hogares cuyo ingreso total mensual es de al menos la CT y no alcanza 1,25 veces la CT del Sistema de Canastas de Consumo. Tienen ingresos mensuales entre $92.089,76 y $115.112,19 pesos.

\- Sector medio “clase media”: Hogares cuyo ingreso total mensual es de al menos 1,25 veces la CT y no alcanza 4 veces la CT del Sistema de Canastas de Consumo. Ganan desde $115.112,20 y hasta $368.359,03 por mes.

\- Sector acomodado: Hogares cuyo ingreso mensual es de 4 veces o más la CT del Sistema de Canastas de Consumo. Son los ingresos de $368.359,04 al mes o más.

“Esta estratificación brinda, por un lado, la posibilidad de identificar a los sectores más desprotegidos de la sociedad en términos de situaciones de indigencia y de pobreza y, por el otro, da cuenta de la heterogeneidad de los sectores no pobres de manera de facilitar un análisis más integral de la situación social de la Ciudad de Buenos Aires, la de cada uno de los estratos definidos y su evolución en el tiempo”, explica el informe.

En base a esos estratos, los ingresos teóricos necesarios de una pareja de clase media compuesta por una mujer y un varón de 35 años de edad, ambos económicamente activos y propietarios de la vivienda en la que viven, con dos hijos varones de 9 y 6 años, se dispararon en el último año aunque no alcanzaron el ritmo de avance del nivel general de inflación que marcha en el orden del 52% anual, según el dato de Indec a septiembre.

Visto en términos de necesidades diarias, esa misma familia tipo necesitó al menos $3.837,07 por día para alcanzar un nivel de consumo consecuente con el estrato medio.

Para no caer en la pobreza, mientras tanto, se necesitaron $74.036,96 al mes -un 46,8% más que hace un año-, mientras que quienes superan los $92.089,76 al mes pero no alcanzan los $115.112,19 mensuales son considerados “sector medio frágil”.