+++
author = ""
date = 2021-08-18T03:00:00Z
description = ""
image = "/images/517015-1.jpg"
image_webp = "/images/517015.jpg"
title = "LA MESA DE ENLACE SUMA APOYOS PARA EXIGIR LA APERTURA DEL CEPO A LA CARNE"

+++

**_Se reunirá con exportadores y consignatarios de hacienda y otros eslabones de la cadena productiva perjudicados por las restricciones a las exportaciones._**

Los presidentes de las 4 entidades que conforman la mesa de enlace se reunirán este miércoles al mediodía con exportadores y consignatarios de hacienda en el predio de la Sociedad Rural Argentina (SRA), en el barrio porteño de Palermo, y si bien descartaron definir medidas de fuerza, emitirían un documento con una posición unificada para exigirle al Gobierno que abra las exportaciones de carne vacuna.

Actualmente las ventas externas están restringidas a un 50% del volumen registrado el año pasado. Y a fines de este mes vence el plazo sobre esos cupos exportables. El Ejecutivo deberá definir si prorroga, o no, la medida hasta fin de año.

En ese marco, Nicolás Pino (SRA), Elbio Laucirica (Coninagro), Carlos Achetoni (Federación Agraria) y Jorge Chemes (CRA) compartirán un almuerzo con distintos actores del sector directamente afectados por el cepo exportador.

En diálogo con TN.com.ar, Pino adelantó: “Nos reuniremos con distintos eslabones de la cadena para analizar los perjuicios ocasionaos en la intervención al mercado ganadero, que el Ejecutivo llevó adelante en los últimos meses”.

A su turno, Laucirica manifestó a TN.com.ar sus “expectativas por la necesidad de un abordaje unido con toda la cadena, en la búsqueda de soluciones para nuestras producciones”.

Entre los presentes, estarán el presidente del Consorcio de Exportadores de Carnes Argentinas (ABC), Mario Ravettino, el titular de la Federación de Industrias Frigoríficas Regionales Argentinas (FIFRA), Daniel Urcía, y representantes de la Unión de la Industria Cárnica Argentina (UNICA).

También fueron invitados el titular del Centro de Consignatarios, Carlos Colombo, y Andrés Mendizábal, quien preside el Mercado Agroganadero de Cañuelas (MAG), que desde octubre reemplazará al de Liniers. Ambos, acaban de protagonizar un cruce con el oficialismo, luego de que Máximo Kirchner protagonizó el sábado un acto partidario en ese predio, sin el consentimiento de los empresarios ganaderos.

Entrevistado por TN.com.ar, Colombo, quien también forma parte del MAG, comentó que los 45 consignatarios que operan en Liniers se vieron “muy afectados” en el último paro ganadero que anunció de manera “intempestiva” la mesa de enlace, ya que no se comercializó hacienda por el circuito formal. “Aunque circuló de manera directa de productores a frigoríficos, por el predio de Liniers no pasó una vaca”, agregó.

Sobre el encuentro de este miércoles, Colombo aseveró: “Entiendo que la idea es redactar una carta hacia el presidente (Alberto Fernández) sobre cómo evaluamos el resultado de las decisiones en los últimos meses, y así transmitir un mensaje unificado en desacuerdo con el cierre de exportaciones, ya que no le da previsibilidad a la producción”,

Vale recordar que el Gobierno anunció, el fin de semana pasado, una cuota de 3500 toneladas para la exportación de carne kosher a Israel, tras las críticas de ese país y los diferentes referentes de la actividad bovina. Sin embargo, desde la mesa de enlace lo consideraron insuficiente al anuncio.

Como tampoco conformó, a muchos dirigentes, el Plan Ganadero que los funcionarios nacionales le presentaron al Consejo Agroindustrial Argentino (CAA), con beneficios impositivos y crediticios para incentivar a la producción de carne.

De hecho, Horacio Salaverri, presidente de la Confederación de Asociaciones Rurales de Buenos Aires y La Pampa (Carbap), que forma parte de CRA, lo calificó públicamente como una copia de proyectos que ya había elaborado el CAA, y que además le faltaba una “mirada de largo plazo”.

Justamente, Carbap organizará, el próximo sábado 21, en Olavarría, un encuentro de Consejo Directivo abierto para abordar las consecuencias de la resolución oficial sobre la actividad.