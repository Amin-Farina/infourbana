+++
author = ""
date = 2021-06-28T03:00:00Z
description = ""
image = "/images/1622903894_805738_1622903955_noticia_normal_recorte1-1.jpg"
image_webp = "/images/1622903894_805738_1622903955_noticia_normal_recorte1.jpg"
title = "EL GOBIERNO DEFINIÓ UN ALZA DEL 70% A DEVOLVER PARA TITULARES DEL EL AUH"

+++

**_En medio de la escalada de la inflación, el Gobierno actualizó los montos del reintegro de 15% para consumos con tarjeta de débito que realizan los jubilados, pensionados que cobran el haber mínimo, así como los titulares de la Asignación Universal por Hijo (AUH) y por embarazo (AUE)._**

La decisión implica que desde este jueves 1° de julio se eleva 70% en los montos máximos a reintegrar de manera automática a más de 1,3 millón de personas que perciben bajos ingresos. Así, se estableció que la devolución será de $1200 por mes por beneficiario, y subirá hasta $2400 para aquellos titulares de AUH que tengan 2 o más hijos. Hasta el momento, el beneficio tenía un tope individual de $700 y $1400, respectivamente.

Además, se definió extender hasta el 31 de diciembre próximo el mecanismo, que vencía este miércoles. Se sumaron también gastos en farmacias a los rubros en los que se aplica el reintegro, se informó de manera oficial.

La Casa Rosada ya había decidido que siguiera el beneficio hace unas semanas, como anticipó TN.com.ar. Los plazos y actualización de los montos se terminaron de definir en la última reunión del gabinete económico. “La recuperación de los ingresos reales de las familias es un objetivo central de la política económica. En esa lógica se inscribe esta ampliación del reintegro”, dijo el jefe de Gabinete, Santiago Cafiero.

#### Quiénes pueden acceder al reintegro de 15% para las compras con tarjeta de débito

* Jubilados y pensionados que cobren el haber mínimo (que en junio es de $23.064,70) o menos.
* Titulares de AUH y AUE, cuyo beneficio es hoy $4504.

#### Cuáles son lo requisitos para acceder al reintegro del 15%

* Están excluidos quienes cobran una jubilación mínima pero tributan Bienes Personales (sin contar la vivienda única) o que tienen ingresos adicionales por un empleo registrado.
* Se aplica por haber y no por persona. Así, quien cobre una jubilación mínima y una pensión tiene hasta $2400 por mes de devolución.
* Los hogares con más de dos AUH reciben, como máximo, $2400 de reintegro.
* Es para las compras con la tarjeta de débito asociada al haber o a la prestación social que se recibe.
* Los bancos devuelven el 15% de cada compra en las 24 horas posteriores a la transacción, con el tope mensual correspondiente.

Se puede comprar en comercios inscriptos en actividades económicas vinculadas a la venta de alimentos y bebidas, tanto al por mayor como minoristas. En el caso de los supermercados, hipermercados y autoservicios, con un surtido de productos más amplio no hay discriminación por productos. Desde julio, se suman gastos en farmacias.