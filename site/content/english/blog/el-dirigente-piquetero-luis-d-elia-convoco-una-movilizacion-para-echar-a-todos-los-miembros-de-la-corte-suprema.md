+++
author = ""
date = 2022-01-11T03:00:00Z
description = ""
image = "/images/gordo-hijo-de-puta.jpg"
image_webp = "/images/gordo-hijo-de-puta.jpg"
title = "EL DIRIGENTE PIQUETERO LUIS D’ELIA CONVOCÓ UNA MOVILIZACIÓN PARA “ECHAR” A TODOS LOS MIEMBROS DE LA CORTE SUPREMA"

+++

##### **_El viceministro de Justicia Martín Mena respaldó la convocatoria del dirigente piquetero Luis D’Elia a una movilización para “echar” a todos los miembros de la Corte Suprema. El funcionario dijo que le parece “sano y necesario que la gente se pronuncie”._**

Mena dijo que le parece “bien” el llamado a la marcha contra los integrantes del máximo tribunal, prevista para el 1° de febrero: “Siempre avalo toda expresión popular directa de la gente”.

El viceministro de Justicia dijo que siempre está a favor “de la manifestación popular de la gente, cuando sin intermediarios le dicen a los poderes hegemónicos su opinión y limites”.

Para Mena se está “llegando a límites que el gobierno de Mauricio Macri los superó, barriendo con todo en materia de manipulación política, judicial y protección del poder mediático”, en declaraciones a El Destape radio.

###### ¿Qué dijo Martín Mena sobre los proyectos de reforma del Poder Judicial?

El viceministro de Justicia recordó que enviaron al Congreso proyectos “para adecuar el funcionamiento del órgano de administración del Poder Judicial, que es el Consejo de la Magistratura, de fortalecimiento de la Justicia federal y otras que se irán atendiendo para las demás áreas”.

###### Convocatoria del kirchnerismo duro a una marcha para “echar” a los miembros de la Corte

A principios de año D’Elia convocó a una marcha para “echar” a todos los integrantes de la Corte, a los que calificó de “miserables”, en una movilización prevista para el 1° de febrero a las 18 hacia el Palacio de Tribunales.El dirigente piquetero planteó: “El final del lawfare en la Argentina se tiene que terminar con el pueblo en la calle y que echemos a patadas a esta Corte miserable”. Luego aclaró que retiraba “lo de patadas porque nadie quiere violencia”.

###### Duras críticas de Martín Soria a la Corte después de una reunión con el máximo tribunal

A principios de diciembre el ministro de Justicia Martín Soria se reunió por primera vez con los integrantes de la Corte Suprema. Tras el encuentro acusó al máximo tribunal de “tener una crisis de funcionamiento” y le pidió resoluciones más “ágiles”.

Soria planteó a través de un comunicado, tras la reunión con la Corte: “Vinimos a expresar nuestra preocupación por la gravedad institucional a la que llegó la Justicia argentina en los últimos 5 años”.

El ministro de Justicia reclamó “una justicia ágil, que trabaje de cara a la gente, que se ocupe de los problemas que les preocupan a todos los argentinos”.