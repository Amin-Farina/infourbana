+++
author = ""
date = 2021-08-25T03:00:00Z
description = ""
image = "/images/n7o5njyn2vbb3efshdiqkqbv24.jpg"
image_webp = "/images/n7o5njyn2vbb3efshdiqkqbv24.jpg"
title = "LUIS D´ELIA CORTÓ SU TOBILLERA ELECTRÓNICA Y ENCABEZÓ UNA MARCHA TRAS RECUPERAR LA LIBERTAD"

+++

**_Tras cumplir mil días preso, el dirigente del partido Miles y de la Federación Tierra, Vivienda y Hábitat (FTV), Luis D’Elía, recuperó hoy la libertad. Lo celebró con un acto político junto a agrupaciones y organizaciones piqueteras._**

Los militantes se convocaron en la puerta de la casa del dirigente social kirchnerista, en la localidad bonaerense de Isidro Casanova, partido de La Matanza. Allí, antes de movilizarse, D’Elía se asomó por la terraza de la vivienda para llevar a cabo “un acto simbólico”: tomó un cuchillo y cortó su tobillera electrónica. Luego de ello, encabezó una marcha hacia la sede de la FTV, en la calle Tres Cruces 4151, donde comenzó poco después de las 15 el acto central.

D’Elía definió la jornada como “un día de reivindicación, de lucha y de fiesta”. Durante la convocatoria se inauguró una fábrica de alimentos congelados y hubo una comunicación telefónica con la líder de la agrupación Tupac Amaru, Milagro Sala. En el lugar estuvieron presentes el intendente de Ensenada, Mario Secco, el juez federal Juan Ramos Padilla, el secretario general de ATE y adjunto de la CTA Autónoma, Hugo “Cachorro” Godoy, el referente del Frente Barrial CTA Nacional, Juan Vita.

Al momento de tomar la palabra, D’Elía dijo que fueron “muy duros” los tres años que pasó bajo arresto. “No se lo deseo a nadie. Es muy triste estar en una Navidad, pasar un cumpleaños en la cárcel. En la Argentina hay 50 presos políticos y es una vergüenza. Queremos independencia del Poder Judicial, jueces probos y libertad a todos los presos políticos. Pongo las manos en el fuego por todos ellos. Creo en todos y cada uno de mis compañeros”, agregó.

El dirigente de Miles fue condenado en noviembre de 2017 a tres años y nueve meses de prisión por haber dirigido la protesta y la toma de la Comisaría 24 en el barrio porteño de La Boca, hace 16 años. Por entonces era funcionario del gobierno de Néstor Kirchner, a cargo de la Subsecretaría de Tierras para el Hábitat Social, e ingresó con otras personas a la seccional para reclamar por el crimen del dirigente comunitario Martín “Oso” Cisneros, asesinado por un vendedor de drogas de la zona.

El caso se demoró durante años. Recién en noviembre de 2017 terminó el juicio oral y D’Elía fue condenado por el Tribunal Oral Federal 6 por los delitos de atentado a la autoridad, lesiones leves a la Policía Federal, instigación a cometer delitos, privación ilegal de la libertad y usurpación. Luego Casación confirmó la condena y redujo la pena a tres años y nueve meses de prisión.

Al comienzo de la pandemia, se le concedió el arresto domiciliario por su delicado estado de salud: es paciente coronario, diabético, con isquemia peri necrosis. Desde entonces permaneció en la casa de Isidro Casanova, salvo un breve período en julio del año pasado, cuando contrajo coronavirus y estuvo ocho días internado en el Sanatorio Otamendi por tratarse de un paciente de riesgo.

El referente kirchnerista fue detenido recién el 25 de febrero de 2019 cuando Casación rechazó el último planteo que lo mantenía en libertad, con lo que la pena impuesta vencerá recién el 24 de noviembre de 2022.

La semana pasada, la jueza Sabrina Namer concedió la libertad condicional. Para dicha resolución, la magistrada tuvo en cuenta “la conducta procesal que el interno registró durante su encierro domiciliario, y la mantenida a lo largo del proceso, destacándose que se presentó a estar a derecho una vez que la sentencia recaída a su respecto se tornó ejecutable, lo que también se vio reflejado en los informes de control de la prisión domiciliaria agregados al legajo y en la no comisión de nuevos delitos verificada en autos”.

El fallo le impuso a D’Elía la obligación de cumplir una serie de reglas de conducta: deberá residir en el domicilio de Isidro Casanova, abstenerse de abusar de bebidas alcohólicas o consumir sustancias estupefacientes; no cometer nuevos delitos; someterse al cuidado de la Dirección Nacional de Control y Asistencia de la Ejecución Penal, y deberá informar si no está en su domicilio.