+++
author = ""
date = 2021-07-15T03:00:00Z
description = ""
image = "/images/5f2c1bd929ccf_800x450-1.jpg"
image_webp = "/images/5f2c1bd929ccf_800x450.jpg"
title = "FAMILIARES DE LAS VÍCTIMAS DE AMIA ANUNCIARON QUE NO VAN A IR A LA AUDIENCIA ORGANIZADA POR CRISTINA KIRCHNER"

+++

**_Luis Czyzewski y Mario Averbuch, querellantes en la causa conocida como Memorándum con Irán, presentaron este jueves un escrito informando al Tribunal Oral Federal 8 que no iban a asistir a la audiencia solicitada por las defensas de la vicepresidenta Cristina Kirchner y otros acusados para reclamar la nulidad de todo el proceso._** 

Explicaron que mañana se hará el acto por el 27 aniversario de la tragedia a la AMIA. En una clara alusión al procurador del Tesoro Carlos Zannini, sostuvieron: “Los familiares de las víctimas del atentado terrorista a las sedes de AMIA-DAIA no le tenemos miedo a la verdad”.

En el escrito al que accedió Infobae, los familiares dijeron al TOF: “Vuestras Excelencias decidieron realizar una audiencia oral no legislada, que fue pedida por los imputados para tratar la nulidad de fallos dictados por la Cámara de Casación Penal, el día en que se celebrará el acto en conmemoración de un nuevo aniversario del atentado terrorista a las sedes de AMIA-DAIA. Informamos al Tribunal que hemos decidido no participar de la audiencia”.

“En primer lugar porque, como lo hacemos hace 27 años, respetaremos la memoria de 85 argentinos que perdieron la vida en ese infausto atentado terrorista, entre quienes se encontraban nuestras hijas; en segundo lugar porque consideramos que la audiencia ordenada por el Tribunal es nula, y no la convalidaremos con nuestra presencia. Los familiares de las víctimas del atentado terrorista a las sedes de AMIA-DAIA no le tenemos miedo a la verdad. Por el contrario, desde que el Fiscal Nisman formuló su denuncia solo deseamos que se realice el Juicio Oral y Público, oportunidad en la que los imputados podrán ejercer plenamente sus defensas”, afirmaron.

El TOF citó para mañana a las 11:30 a la audiencia solicitada por las defensas para reclamar la nulidad de la causa que investiga la denuncia del fiscal Alberto Nisman, y que plantea que se buscó hacer caer las alertas rojas de los iraníes prófugos por el atentado contra la mutual judía.

La DAIA solicitó la postergación de la audiencia. Tras recordar que la DAIA es “co-organizadora” del acto, la querella sostuvo que la entidad, “representación política de la comunidad judía argentina y víctima directa del atentado terrorista, considera una afrenta a la memoria de las víctimas la eventual realización de dicha audiencia el mismo día en el que se realizará el acto”.

“Recordar, año tras año, aquel trágico hecho es para la sociedad argentina una obligación moral. Creemos que no puede haber nada más importante ni trascendente que recordar a las 85 personas asesinadas por el terrorismo. Por ello, y entendiendo que todas las partes del proceso estarán conformes con lo que se peticiona, es que se solicita la urgente postergación de la audiencia fijada para el día 16 de julio de 2021”, se indicó.

El fiscal Marcelo Colombo también reclamó la postergación de la audiencia. En su caso, porque aún no habían llegado los elementos de prueba que solicitó de cara analizar si existen investigaciones en torno a las reuniones que habrían mantenido los jueces de Casación Gustavo Hornos y Mariano Borinsky con el entonces presidente Mauricio Macri, ejes del pedido de nulidad de CFK.

El planteo será evaluado por los jueces Gabriela López Iñiguez, Daniel Obligado y José Michilini. Los tres pertenecen a distintos tribunales y tienen previstos distintos juicios; por eso, según indicaron fuentes judiciales, hubo que coordinar agendas para pautar la audiencia de mañana, último día antes de que comience la feria judicial de invierno (receso que durará dos semanas).

Los familiares de víctimas que están presentados como querellantes, Jorge Averbuch y Luis Czyzewski, habían planteado a través de sus abogados -Juan José Avila y Tomás Farini Duggan- la recusación de los tres jueces del TOF y del fiscal Colombo por haber autorizado esta audiencia previa al juicio. Aseguran que se trata de un escenario inventado para hacer caer el juicio oral que tiene entre sus acusados a la vicepresidenta, como ocurrió con dólar futuro.

Zannini había cuestionado la oposición de los acusadores privados a la audiencia pública y el pedido de recusación a los jueces diciendo que “el temor de las querellas no es a la parcialidad, como trasunta en sus planteos, sino a que se convalide la verdad”. Eso, dijo, “aterra a las acusaciones privadas, que son completamente conscientes de que esta causa nunca debió existir y que ello puede ser percibido por cualquier persona; el hecho de que salga a la luz significaría revelar la arbitraria, enceguecida e incalificable persecución de un sector ideológicamente contaminado de la jurisdicción y de las querellas a personas totalmente inocentes que, hace mucho tiempo, están sometidas a un injusto proceso”.

Los jueces del TOF rechazaron los argumentos de la querella que entendía que estaba comprometida su imparcialidad. Luego, citaron a una audiencia -también pública- en donde escucharon el pedido de apartamiento para el fiscal Colombo y la respuesta del Ministerio Público, que resaltó la necesidad de ser transparente en esta causa que tiene gravedad institucional. El lunes último, el TOF rechazó la recusación del fiscal Colombo al sostener que no se vio comprometido su deber de objetividad.

La audiencia que promovieron las defensas busca que se dicte la nulidad de la causa por el Memorándum que nació en enero de 2015 con la acusación del fiscal Alberto Nisman. Allí se asegura que ese Pacto con Irán, firmado en 2013, fue un ardid para hacer caer las alertas rojas que pesan sobre un grupo de iraníes que están imputados como ideólogos del ataque a la AMIA. Esos iraníes nunca aceptaron comparecer ante la Justicia argentina y por eso la causa se vio virtualmente paralizada.