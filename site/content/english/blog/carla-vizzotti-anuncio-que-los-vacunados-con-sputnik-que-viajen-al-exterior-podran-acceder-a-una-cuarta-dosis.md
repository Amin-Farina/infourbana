+++
author = ""
date = 2022-03-17T03:00:00Z
description = ""
image = "/images/carla-vizzotti.webp"
image_webp = "/images/carla-vizzotti.webp"
title = "CARLA VIZZOTTI ANUNCIÓ QUE LOS VACUNADOS CON SPUTNIK QUE VIAJEN AL EXTERIOR PODRÁN ACCEDER A UNA CUARTA DOSIS"

+++
#### La ministra de Salud de la Nación aclaró que el Estado no lo recomienda. Por lo que los interesados deberán firmar un consentimiento informado.

La ministra de Salud de la Nación, Carla Vizzotti, anunció este jueves que el Gobierno nacional habilitará la aplicación de una cuarta dosis para "todas las personas que necesiten viajar" y estén inmunizadas con la vacuna rusa Sputnik V contra el coronavirus.

"Lo que estamos haciendo es avalando la aplicación de la cuarta dosis en personas que necesiten viajar, presentando el pasaje, ya sea por razones personales, familiares, humanitarias, de trabajo, de estudio, religiosas o recreativas", puntualizó Vizzotti.

En declaraciones a _Radio Con Vos_, la funcionaria nacional detalló: "Lo que se está habilitando ahora es que la persona que necesita viajar a algún lugar que requiere una cuarta dosis y decide que quiere hacerlo \[aplicarse la cuarta dosis\], porque no tiene indicación sanitaria y nosotros no lo recomendamos, se tiene que acercar para presentar la evidencia de que tiene que viajar y firmar un consentimiento informado".

"Sabemos que el sobreestímulo puede generar una hiporrespuesta, entonces tenemos que evaluar toda la situación. Tenemos que evaluar el riesgo, la situación epidemiológica, la disponibilidad de información de las vacunas, y siempre priorizar la salud. Justamente por eso nunca fue una recomendación recibir vacunas ni por las dudas, ni demás", advirtió Vizzotti.

Según detalló la ministra de Salud, la persona que requiera la aplicación de la cuarta dosis deberá "cumplir los intervalos mínimos y estar dentro de la edad en la que se puede vacunar", además de "acreditar que tiene que viajar".

Vizzotti explicó que la decisión tiene que ver con que la Organización Mundial de la Salud (OMS) debió postergar su visita de inspección a la planta del Instituto Gamaleya, donde se fabrica la vacuna Sputnik V, como consecuencia de la guerra en Ucrania.

"Estamos en contacto con la OMS y el avance que se logró fue enorme. El Instituto Gamaleya presentó los más de 800 documentos que se le habían solicitado. Estaba programada la visita de la inspección y justo sucede la situación en relación a la guerra y se posterga. Lo que se posterga es que la OMS pueda hacer la evaluación para incorporar en la lista de emergencia la vacuna Sputnik, que es lo que genera la complicación del tema de los viajes", puntualizó la funcionaria nacional.

En ese marco, Vizzotti afirmó que se trata de "un inconveniente que no es sanitario sino que es un inconveniente de decisión de países de no aceptar una vacuna que tiene probada eficacia, efectividad y seguridad, pero la gente no tiene la culpa".

"Desde Argentina, como tenemos un stock de vacunas para asegurar que puedan recibir la vacuna todos los que tienen que iniciar o completar su esquema, estamos habilitando la dosis requerida por la OMS para que si alguien tiene que viajar pueda hacerlo", agregó.