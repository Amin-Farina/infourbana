+++
author = ""
date = 2021-10-29T03:00:00Z
description = ""
image = "/images/f608x342-104881_134604_0-1.jpeg"
image_webp = "/images/f608x342-104881_134604_0.jpeg"
title = "CONFIRMAN EL PROCESAMIENTO DEL EX INTENDENTE DE PILAR"

+++
##### **_La Cámara Federal de San Martín confirmó hoy el procesamiento del ex intendente de Pilar del PRO Nicolás Ducoté por el delito de administración infiel en perjuicio de la administración pública por presuntas irregularidades en tres planes de gobiernos por los que el municipio recibió fondos nacionales pero nunca se hicieron las obras._**

Fuentes judiciales informaron que junto con Ducoté la Cámara ratificó los procesamientos que en primera instancia había dispuesto el juez federal de Campana, Adrián González Charvay, de otras nueve personas.

La causa es por la firma de tres convenios en 2016: el acceso de vecinos a microcréditos para mejorar de viviendas y acceso a agua potable, cloacas, gas y electricidad; la urbanización del barrio Monterrey; y la instalación de una planta de tratamiento cloacal.

Los jueces de la Cámara Federal de San Martín Alberto Lugones, Marcos Moran y Néstor Barral señalaron que Ducoté y el resto de los acusados “efectuaron sus aportes para lograr la desviación de recursos girados desde el ex Ministerio del Interior, Obras Públicas y Vivienda de la Nación a la Municipalidad de Pilar en favor de terceros y en perjuicio de la Administración Pública Nacional”.

Puntualmente remarcaron que el Ministro de Interior, Obras Públicas y Vivienda, entonces a cargo de Rogelio Frigerio, envió fondos públicos a municipios tras la firma de los convenios pero las obras no se hicieron o completaron. De hecho la Cámara marcó irregularidades en las empresas a las que se le adjudicaron la realización de las obras.

“Esas maniobras se llevaron a cabo mediante cuantiosas aplicaciones de recursos y la concreción de los acuerdos necesarios para justificar con obras inexistentes o comisiones indebidas la aplicación de sumas millonarias de dinero por las cuales ni el Estado ni la sociedad se vieron beneficiados”, sostuvieron los magistrados y agregaron que “lejos de efectuar una correcta distribución de los recursos que les fueron girados desde las arcas nacionales, los funcionarios municipales, discrecionalmente, generaron gastos y costos que conllevaron la distracción de importantes sumas de dinero destinadas a los ciudadanos del Municipio de Pilar, en manos de terceros”.

“En concreto, la intervención de Nicolás José Ducoté, en su carácter de intendente de la Municipalidad de Pilar, aflora del trámite atinente al pedido y la posterior recepción de los fondos, del proceso licitatorio, de las deficiencias relativas a la ejecución de las obras y de la posterior rendición de cuentas. De modo que, para comenzar, no sólo incumplió con el plazo de 30 días -contados a partir de la fecha de recepción del primer desembolso para iniciar las obras-, sino que además, no devolvió de manera inmediata los fondos, conforme se estipulaba en la cláusula sexta del convenio específico para esa misma circunstancia”, especificaron los camaristas sobre la actuación del ex intendente en una de las obras.

La Cámara también ratificó el embargo de 100.000.000 de pesos sobre los bienes de Ducoté. Junto con el ex intendente de Pilar la Cámara también confirmó los procesamientos de Juan Pablo Martignone, Federico Iván Leonhardt, Guillermo Horacio Iglesias, Sandra Edith Sosa, Osvaldo Nicolás Caccaviello, Norberto Fabián Giulianelli, Sergio Miguel Russo, Gabriel Adrián Lucero y Fernando María Bonafede, quienes participaron de la maniobra.

En la causa fueron imputados e indagados Iván Kerr, ex secretario de Vivienda y Mariana Klemensiewicz, ex secretaria de Infraestructura Urbana del ministerio del Interior, Obras Públicas y Vivienda. La Cámara confirmó las faltas de mérito que recibieron y seguirán siendo investigados.

Los acusados habían apelado sus procesamientos por considerarlos prematuras. “Se estima que la resolución recurrida cumple con la manda de motivación, pues contiene una explicación de la conclusión a la que arriba el magistrado de grado, que aparece como el resultado de un análisis racional de los elementos obrantes en el legajo y su aplicación al caso concreto, de modo que la pretensión de las defensas en tal sentido, no habrá de prosperar”, ratificó la Cámara la intervención del juez González Charvay.

Con la confirmación de los procesamientos la causa queda en condiciones de ser elevada a juicio oral.