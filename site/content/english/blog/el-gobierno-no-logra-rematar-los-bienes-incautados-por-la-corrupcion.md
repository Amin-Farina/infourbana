+++
author = ""
date = 2021-10-18T03:00:00Z
description = ""
image = "/images/fh4sjo7zvnbhtd2lj3j5fdtxay.jpg"
image_webp = "/images/fh4sjo7zvnbhtd2lj3j5fdtxay.jpg"
title = "EL GOBIERNO NO LOGRA REMATAR LOS BIENES INCAUTADOS POR LA CORRUPCIÓN"

+++

### **_Un departamento en Puerto Madero y dos propiedades en Villa Urquiza y Devoto se pusieron a la venta, pero no hubo oferentes. El particular motivo que frenaría las operaciones._**

Un lujoso departamento en Puerto Madero, un complejo de 630 m2 en Villa Urquiza y una coqueta casa en Villa Devoto. Tres propiedades que cualquier inmobiliaria quisiera ofrecer a sus clientes, pero que el Estado nacional no logra subastar. Se trata, en rigor, de bienes decomisados en la causa Cuadernos de las coimas que esperan por conocer su futuro y a sus nuevos propietarios.

La Agencia de Administración de Bienes del Estado los colocó a la venta en los últimos meses, pero por diversos motivos no hubo oferentes o en algunos casos el proceso se debió dar de baja para volver a tasar el inmueble. Incluso el propio gobierno de Mauricio Macri ofreció uno de estos departamentos, pero ante la falta de interesados se dio por terminado el proceso.

#### Bienes de la corrupción: de un departamento en Puerto Madero al “búnker” de Villa Urquiza

El departamento ubicado en el exclusivo barrio de Puerto Madero se ofreció a un precio base de $68.700.000 (unos U$S660.000 a cotización del Banco Nación del 15/10), pero debido al paso del tiempo se resolvió que se vuelva a tasar y se dio de baja la publicación.

Se trata de un bien que pertenecía a Sergio Todisco, acusado de ser testaferro de Daniel Muñoz, exsecretario privado de Néstor Kirchner. El inmueble posee 99m2, cocina integrada, dormitorio con terraza y baño principal. Además cuenta con cochera, baulera y seguridad las 24 horas.

En el caso del complejo de Pedro Ignacio Rivera, en Villa Urquiza, que pertenecía a Muñoz y Víctor Manzanares, histórico contador de la familia Kirchner, fue ofrecido a un precio base de $117.000.000 (U$S 1.125.000) pero la subasta resultó desierta debido a que ni siquiera hubo un solo interesado en la propiedad.

Se trata de un edificio de 630 m2 de dos pisos y altillo y tres entradas. Fuentes judiciales consultadas por TN aseguran que el sitio se utilizaba como una especie de base de operaciones en las épocas en las que se llevaba adelante el entramado de pago de coimas que aún investiga la Justicia.

El tercero de los bienes que se intentó subastar sin éxito es una casa ubicada en Avenida Salvador María del Carril, en Villa Devoto, que se colocó a un precio base de $133.600.000 (U$S 1.284.615).

Al igual que en el caso de la propiedad de Villa Urquiza, no hubo oferentes ni interesados. El inmueble se conforma de cuatro plantas y posee un quincho con cocina, parilla, baño y conexión directa al patio. Además hay un gimnasio, un sauna y cocheras, entre otros detalles de categoría. Esta propiedad también pertenecía a Muñoz.

#### Los bienes de la corrupción se ofrecieron a organismos del Estado, pero nadie los quiso

Una fuente al tanto de las subastas dijo que estas propiedades no logran venderse pese a su bajo precio ya que los interesados deberían blanquear el dinero y revelar el origen de los fondos.

Desde la Agencia de Administración de Bienes del Estado (AABE) explican que ellos no colocan el precio de los bienes sino que lo hace el Tribunal de Tasaciones de la Nación. Fuentes de este organismo incluso aseguran haber ofrecido estas propiedades a otras entidades del Estado como la Cancillería, pero nadie se mostró interesado en utilizarlas.

En el caso Cuadernos, la vicepresidenta Cristina Kirchner está acusada de ser la jefa de una asociación ilícita que recaudaba coimas de empresarios que tenían contratos con el Estado. Las subastas forman parte del programa oficial de recupero de bienes de la corrupción que impulsó la gestión de Mauricio Macri a través del decreto 598/2019. Los procesos solo pueden llevarse adelante con bienes que tengan decomiso firme.

Desde la AABE aclararon que pese a que los procesos quedaron truncos, abrirán nuevas ofertas para intentar desprenderse en un futuro de estos bienes que -en algunos casos- generan gastos para el Estado mientras permanecen en su guarda. Un ejemplo de esto es la cochera de la calle Amenabar (Palermo - CABA), que antes de concesionarse insumía unos $24.552, solo de ABL.

#### El destino de los fondos

El dinero recaudado toda subasta de esta clase irá a una cuenta especial del Tesoro Nacional y se destinará a financiar el funcionamiento de la Unidad de Información Financiera (UIF), a la lucha contra el narcotráfico y la rehabilitación de los afectados por el consumo (art.39 - ley 23.737) y a programas de salud y capacitación laboral.