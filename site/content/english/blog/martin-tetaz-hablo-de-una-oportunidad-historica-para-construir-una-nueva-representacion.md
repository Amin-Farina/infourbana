+++
author = ""
date = 2021-07-13T03:00:00Z
description = ""
image = "/images/martin_tetaz.png"
image_webp = "/images/martin_tetaz.png"
title = "MARTÍN TETAZ HABLÓ DE UNA \"OPORTUNIDAD HISTÓRICA PARA CONSTRUIR UNA NUEVA REPRESENTACIÓN\""

+++

El precandidato a diputado nacional de Juntos por el Cambio (JxC) por la Ciudad de Buenos Aires, Martín Tetaz, aseguró que el senador nacional Martín Lousteau lo invitó en varias ocasiones a participar en política, y estimó que "hay una oportunidad histórica" para construir un espacio capaz de generar "una nueva representación" en la ciudadanía.

"Martín Lousteau me viene llamando hace muchas elecciones para invitarme a participar y por mi carrera profesional lo venía postergando. Soy orgullosamente radical, me afilié a los 18 años y milité en Franja Morada. La política me apasiona desde siempre, diría que lo llevo en la sangre. Me sumé porque creo que en la actualidad hay una oportunidad histórica para construir un espacio distinto que contenga a mucha gente que piensa parecido pero no encuentra representación", consideró este martes Tetaz en diálogo con Radio Urbana.

El columnista radial lanzó este lunes su precandidatura a diputado nacional por la Ciudad de Buenos Aires, en un acto realizado en Plaza Houssay, en el que estuvieron presentes el senador nacional Martín Lousteau y la presidenta de la UCR porteña, Mariela Coletta.

Tetaz afirmó que "el contexto es muy favorable para la emergencia del radicalismo" y celebró el "tsunami" que generó la decisión de Facundo Manes de postularse en la provincia de Buenos Aires.

En ese sentido, se pronunció en favor de construir "una nueva alianza entre trabajadores, productores y estudiantes para derrotar al kirchnerismmo.

El economista afirmó, en otro orden, que "la forma de terminar la pobreza" en Argentina es mediante la "transformación" de los planes sociales en empleo.

"La gente tiene que trabajar en Argentina. El triunfo o fracaso de un gobierno se tiene que juzgar por la cantidad de planes sociales que ese gobierno logra transformar en empleo. Esa es la única forma de terminar con la pobreza", sostuvo.

En ese sentido, se pronunció en favor de construir "una nueva alianza entre trabajadores, productores y estudiantes para derrotar al kirchnerismmo.

El economista afirmó, en otro orden, que "la forma de terminar la pobreza" en Argentina es mediante la "transformación" de los planes sociales en empleo.

"La gente tiene que trabajar en Argentina. El triunfo o fracaso de un gobierno se tiene que juzgar por la cantidad de planes sociales que ese gobierno logra transformar en empleo. Esa es la única forma de terminar con la pobreza", sostuvo.