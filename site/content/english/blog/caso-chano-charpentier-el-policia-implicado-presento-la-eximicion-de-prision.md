+++
author = ""
date = 2021-08-01T03:00:00Z
description = ""
image = "/images/610166bb7c140_1004x565-1.jpg"
image_webp = "/images/610166bb7c140_1004x565.jpg"
title = "CASO CHANO CHARPENTIER: EL POLICÍA IMPLICADO PRESENTÓ LA EXIMICIÓN DE PRISIÓN"

+++

**_El oficial que le disparó al cantante presentó el pedido para evitar que haya una orden de detención preventiva. Declaran tres testigos._**

Esta semana será clave en la causa por las “lesiones gravísimas” sufridas por Santiago Moreno Charpentier, “Chano”. El fiscal Martín Zocca podrá considerar las pruebas centrales para analizar la actuación del único imputado, el oficial subayudante Facundo Nahuel Amendolara. El peritaje balístico que harán este martes los expertos de la Policía Federal y el estudio sobre las ropas que llevaba Chano al momento del hecho servirían para establecer la distancia desde la que se hizo el disparo que hirió en el abdomen al cantante.

A eso se sumará la ronda de testimonios de las personas que presenciaron lo ocurrido en la madrugada del lunes 26 de julio en la vivienda del barrio privado Parque La Verdad, de la localidad bonaerense de Exaltación de la Cruz. La única duda es si estará en condiciones de concurrir Marina Carpentier, la madre de Chano.

El fiscal Zocca tendrá la posibilidad de ubicar visualmente cada declaración, con el conocimiento que tiene por la inspección ocular que realizó la semana pasada en la casa de Chano. La distancia desde la que disparó Amendolara va a ser un dato central, a la hora de determinar el “riesgo de vida” que supuestamente corría el imputado.

Eso es lo que dirá Amendolara cuando llegue la indagatoria y es lo que aseguró la oficial Vanesa Jeanatte Flores, que participó en el operativo. Ella dijo que su compañero “hizo lo correcto” porque su vida corría peligro, mientras que a partir de lo dicho por el abogado defensor del acusado, Fernando Soto, trascendió que Amendolara va a decir que el cuchillo que tenía Chano en la mano lo puso ante una disyuntiva: “Era él o yo”.

Uno de los temas importantes fue el análisis de las prendas que vestía Chano esa noche. Por lo que se supo hasta ahora, el proyectil que produjo lesiones gravísimas en un riñón, el bazo y el páncreas del músico, fue hallado entre sus ropas, cuando lo subieron a la ambulancia que lo llevó al Sanatorio Otamendi. Sobre esa prueba, el defensor Soto puso dudas sobre si las prendas habían sido debidamente conservadas para evitar “contaminaciones”.

Las ropas fueron enviadas para su peritaje al equipo de Criminalística del Ministerio Público de Zárate-Campana. Se trata de la remera y el buzo que vestía Chano cuando recibió el disparo. El análisis sirve para determinar el tamaño de los orificios y los rastros de pólvora que ayudarán a establecer la distancia aproximada desde la que se accionó la 9 milímetros del policía. Es una prueba objetiva considerada fundamental por los investigadores.

Eso se complementará con el peritaje balístico que se realizará mañana martes en la sede que la Policía Federal tiene en el barrio porteño de San Telmo. Se analizará el estado del proyectil, la vaina y se supone que se harán pruebas con el arma, para reunir datos que puedan aportar indicios ciertos sobre la distancia desde la que se efectuó el disparo.

Fuentes judiciales señalaron que la vaina de la bala calibre 9 milímetros fue hallada en la escena del hecho, presuntamente en el jardín de la casa. La vaina y el plomo fueron entregados a la Unidad Búsqueda de Evidencia de la Federal. Todos los elementos reunidos, servirán a la hora de recibir las declaraciones de los testigos presenciales, que ya declararon en sede policial, pero que volverán a hacerlo ante el fiscal Zocca.

Los testigos presenciales son muchos y variados: Esteban Charpentier y Oscar José Ottonello, tío y padrastro de Chano; el médico psiquiatra Gonzalo Caligiuri; Héctor Pain, chofer de la ambulancia de la obra social Osde; Juan Marcelo Giménez y Emily Torrico Céspedes, enfermero y médica clínica de la empresa Mas Vida; Juan Manuel Zanandrea, médico a cargo de la ambulancia, y Javier Hernán Blanco, vecino del lugar quien dijo haber visto a Chano, antes de los hechos, como si estuviera “perdido en tiempo y espacio”.

También se tomarán en cuenta las manifestaciones de los oficiales Mariano Andrés Giacco y Vanesa Jeanette Flores, compañeros del imputado, quienes ya declararon ante los efectivos de la Federal que colaboran en la instrucción. Los compañeros de Amendolara serán ahora citados por el fiscal Zocca. De toda la información reunida por el titular de la Fiscalía de Delitos Complejos, surgirán los elementos por confrontar cuando llegue la declaración indagatoria del imputado.

Amendolara está imputado por el delito de “lesiones gravísimas agravadas por el uso de armas de fuego y por ser funcionario policial”. Podría caberle una pena de entre 3 y 15 años de prisión. La imputación se agrava por las lesiones sufridas por Chano en tres órganos, en concordancia con lo que establece el artículo 91 del Código Penal.

El defensor del policía, Fernando Soto, consideró “excesiva” la imputación, anticipó que alegará que su representado actuó en “legítima defensa” por el “riesgo que corría su vida” y dio a entender que cuando mucho, lo que correspondía era una imputación por “exceso en la legítima defensa”. Soto afirmó que “claramente no hubo una actitud dolosa por parte de Amendolara”,