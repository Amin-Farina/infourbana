+++
author = ""
date = 2022-03-03T03:00:00Z
description = ""
image = "/images/argentina-votacion-condena-invasion-rusia.jpg"
image_webp = "/images/argentina-votacion-condena-invasion-rusia.jpg"
title = "GUERRA RUSIA-UCRANIA: ARGENTINA ACOMPAÑÓ EL RECHAZO A MOSCÚ EN LA ASAMBLEA DE EMERGENCIA DE LA ONU"

+++

##### **_La embajadora ante el organismo internacional, María del Carmen Squeff, pidió que el gobierno de Vladimir Putin “cese inmediatamente en el uso ilegítimo de la fuerza”_**

La Argentina ratificó este miércoles su postura condenatoria a Rusia por la invasión a Ucrania y lo hizo ante la Asamblea de emergencia de Naciones Unidas, dando un mensaje a nivel internacional.

Este miércoles, la ONU aprobó una resolución que “deplora” la invasión de Rusia, con el apoyo de 141 países, cinco rechazos y 34 abstenciones, entre ellas, la de China.

La Representante Permanente de la Argentina ante la Organización de las Naciones Unidas, María del Carmen Squeff, destacó la necesidad de “cooperar para desactivar ese conflicto” y promover “el regreso a la mesa de negociaciones”.

La embajadora volvió a reclamar a la Federación Rusa que “cese inmediatamente en el uso ilegítimo de la fuerza, así como las operaciones militares en territorio” extranjero.

Squeff se expresó así en el marco del undécimo período extraordinario de sesiones de emergencia de la Asamblea General del organismo internacional, que tiene lugar en Nueva York.

La resolución recibió el voto a favor de 141 naciones y tuvo el rechazo de Rusia, Siria, Bielorrusia, Corea del Norte y Eritrea.

##### El Gobierno porteño sí recibió al representante de Ucrania

El jefe de Gobierno porteño, Horacio Rodríguez Larreta, recibió en la sede del Gobierno porteño, en Parque Patricios, al encargado de Negocios de la embajada de Ucrania en la Argentina, Sergiy Nebrat, y a integrantes de la comunidad ucraniana.

“Quiero expresar mi solidaridad con el pueblo ucraniano y con la enorme colectividad ucraniana en Argentina y en Buenos Aires. Fiel a nuestra tradición histórica, la Ciudad siempre dará la bienvenida a quienes necesiten refugio”, expresó Rodríguez Larreta.

Ciudades, como Londres, Madrid y Hamburgo, entre muchas otras, hicieron ofrecimientos similares en los últimos días.

De esta forma, el Gobierno porteño se diferenció de su par nacional, que más allá del pedido de Nebrat no fue recibido por el canciller Santiago Cafiero.

Se estima que la colectividad ucraniana en la Argentina está integrada por 300.000 personas, entre migrantes y descendientes.

La mitad de esa población se encuentra dispersada en el Área Metropolitana de Buenos Aires (AMBA).