+++
author = ""
date = 2021-06-28T03:00:00Z
description = ""
image = "/images/candela-correa-concejal-1.jpg"
image_webp = "/images/candela-correa-concejal-1.jpg"
title = "UNA CONCEJAL DE SALTA CONVOCÓ A JÓVENES A PRESENTAR PROYECTOS Y OFRECIÓ PREMIOS DE $10.000"

+++

**_Candela Correa, concejal de la ciudad de Salta, presentó una iniciativa para que jóvenes presenten proyectos relacionados a políticas públicas._**

El autor de la idea más innovadora se llevará un premio de 10 mil pesos, según la propuesta que promocionó en sus redes sociales, después que no avanzara en el debate parlamentario municipal por la falta de acuerdos políticos en la capital provincial.

En dicho municipio existe la iniciativa de “Concejal por un día” que permite que los ciudadanos se acerquen al Concejo Deliberante a presentar sus ideas. Sin embargo, por la pandemia, quedó trunco y no hubo consenso para encarar alternativas que les permitiera a los jóvenes acercar sus ideas. Ahora la edil decidió encabezar personalmente la convocatoria para que sean ellos los que tengan la posibilidad de exponer sus proyectos.

“Este proyecto ya existe, pero desde año pasado no se viene haciendo por el tema de la pandemia, por eso es que propuse hacerlo de manera virtual, pero todavía ese proyecto no avanzó”, explicó Candela Correa. En declaraciones a medios locales, Correa manifestó: “Quiero abarcar a este sector, realmente creo que es importante poder escuchar a los jóvenes, sus ideas, poder acompañarlos, motivar a que se puedan involucrar en la política y acuerden políticas públicas para poder beneficiar al pueblo salteño, a nuestra querida ciudad”.

Según indicó Correa ya son varios los inscriptos: “Ellos van a defender su proyecto, van a hacer un discurso donde van a contar por qué quieren ser concejales por un día, qué es lo que harían ellos siendo concejales, qué temas le gustaría que se aborden”.

En la convocatoria que publicó en su perfil de redes sociales -donde es muy activa tanto en temas políticos como también personales y laborales- la concejal convocó: “Animate a ser parte de este cambio, compartí tus proyectos, tus ideas serán escuchadas para poder hacer realidad cada propuesta! Juntos podemos aportar y mejorar nuestra querida ciudad”.

Precisó que “cada persona que participe como “concejal por un día” estará en nuestras oficinas, con0cerá a fondo el trabajo, lo acompañaremos, y se le entregará un certificado de reconocimiento por la participación. El proyecto más innovador tendrá un premio”. Correa, que difundió dos números de WhatsApp para que los interesados se anoten, destacó que la iniciativa apunta a jóvenes a partir de los 16 años.