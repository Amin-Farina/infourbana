+++
author = ""
date = 2022-03-21T03:00:00Z
description = ""
image = "/images/avion-chino.jpg"
image_webp = "/images/avion-chino.jpg"
title = "SE ESTRELLA UN AVIÓN CHINO CON 132 PASAJEROS A BORDO"

+++
#### Se estrelló en una zona montañosa al sur del país y creen que no hay sobrevivientes

Un avión con 132 personas a bordo se estrelló en el suroeste de China. El Boeing 737 de la compañía China Eastern Airlines se accidentó en una zona rural cerca de la ciudad de Wuzhou, en la región de Guangxi, y su caída “provocó un incendio” en la montaña, dijo la emisora CCTV, según reprodujo la agencia de noticias AFP.

Los medios de comunicación locales informaron que, según el personal del aeropuerto, el vuelo MU5735 no llegó a su destino previsto en Guangzhou tras haber despegado de la ciudad de Kunming poco después de las 13 hora local.

El vuelo tenía una duración prevista de una hora y cuarenta minutos, en la que el aparato, de casi siete años de antigüedad, debía recorrer los 1.357 kilómetros que separan Kunming de Cantón.

Según datos del portal de seguimiento de vuelos FlightRadar24, la aeronave volaba a las 14.19 hora local a una altitud de 29.100 pies (8.870 metros) cuando, a unos 55 kilómetros al oeste de la localidad de Wuzhou, comenzó a descender.

El último punto de contacto del vuelo fue a unos 25 kilómetros al suroeste de Wuzhou, a una altitud de 3.225 pies (989 metros), a las 14.22 hora local, lo que supondría que en apenas tres minutos el aparato descendió casi 8.000 metros.

El reporte agregó que los equipos de rescate fueron enviados al lugar. China Eastern es una de las tres grandes aerolíneas de China.

En los últimos años, China mantuvo buenos estándares de seguridad aérea, en un país repleto de aeropuertos recién construidos y cubierto por nuevas aerolíneas establecidas para atender el crecimiento vertiginoso del país en las últimas décadas.

El último gran accidente aéreo en China fue en agosto de 2010, con un saldo de 42 víctimas. Fue el último accidente de un vuelo comercial chino de pasajeros que causó víctimas civiles.

El siniestro de vuelo comercial chino más mortífero fue un accidente de China Northwest Airlines en 1994 en el que murieron las 160 personas a bordo.

###### Qué avión fue el que protagonizó el accidente en China

El Boeing 737 bimotor de un pasillo es uno de los aviones más populares del mundo para trayectos de corta y media distancia. En un primer momento no estaba claro qué versión del modelo se había visto implicada en el accidente.

China Eastern opera varias versiones de la popular aeronave, incluidas las 737-800 y el 737 Max. La versión 737 Max estuvo dos años en tierra tras dos accidentes letales.

El regulador chino de aviación dio luz verde al modelo para volver a operar a finales del año pasado, el último gran mercado del mundo que reanudaba sus servicios.