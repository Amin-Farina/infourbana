+++
author = ""
date = 2021-11-12T03:00:00Z
description = ""
image = "/images/045842-1.jpg"
image_webp = "/images/045842.jpg"
title = "LA JUSTICIA DESBARATA EN TIGRE UNA MANIOBRA ELECTORAL DE ESPERT Y EL FRENTE DE TODOS"

+++

##### **_La Cámara de Apelaciones de San Martín desestimó un planteo de José Luis Espert para beneficiar al kirchnerismo proscribiendo a un candidato de Juntos._**

La justicia anuló una maniobra orquestada por el Frente de Todos para beneficiar a Espert y perjudicar a Juntos. La Cámara en lo Contencioso Administrativo de San Martín anuló un amparo presentado por el candidato a primer concejal de la lista de Espert, Avanza Libertad, Juan José Cervetto, luego de que la Junta Electoral de la provincia de Buenos Aires desestimara una presentación similar antes de las Primarias Abiertas Simultáneas y Obligatorias (PASO).

La Cámara, a través de un fallo firmado por los jueces Ana María Bezzi y Jorge Augusto Saulquin, declaró “la nulidad parcial de la decisión recurrida” y “dejar sin efecto lo resuelto en relación a la idoneidad de la vía intentada, así como también anular la medida precautoria dispuesta por el Tribunal de Trabajo N° 1 de San Isidro”.

“Asumir la competencia positiva para resolver los capítulos del decisorio que fueran

declarados nulos conforme surge del resultado de la votación de esta Alzada al expedirse acerca de la primera cuestión”, continuaron.

Finalmente, se dictaminó “denegar la medida cautelar incoada por el Sr. Cervetto” y “comunicar de modo urgente tanto a la Junta Electoral de la Provincia de Buenos Aires como al Juzgado Federal con competencia electoral de La Plata todo lo aquí resuelto, dejándose expresamente aclarado que no subsiste la medida que fuera dispuesta por el Tribunal de Trabajo N° 1 de San Isidro”