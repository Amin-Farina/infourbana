+++
author = ""
date = 2021-11-07T03:00:00Z
description = ""
image = "/images/1201d702f223104c41d19e57ea263102_xl.jpg"
image_webp = "/images/1201d702f223104c41d19e57ea263102_xl.jpg"
title = "BAYER ANUNCIÓ INVERSIONES EN ARGENTINA POR USD 150 MILLONES PARA LOS PRÓXIMOS TRES AÑOS"

+++

#### **_El plan se focalizará en la mejora de procesos productivos en sus plantas operativas en el país que impactarán en mayor innovación, digitalización y sustentabilidad._**

Bayer anunció inversiones en Argentina por 150 millones de dólares para los próximos tres años. con foco en innovación, digitalización y sustentabilidad, alineado con la estrategia global de la empresa, guiada por su misión “Ciencia para una vida mejor”.

“La compañía trabaja para liderar en Innovación, Sustentabilidad y Transformación Digital por lo que esta serie de inversiones se destinarán a nuevos desarrollos y mejoras en procesos productivos en todas sus operaciones enfocados en el abastecimiento del mercado interno, la sustitución de importaciones y el aumento de las exportaciones”, detalló la multinacional de la salud, nutrición y agricultura.

El anuncio se realizó en un acto llevado a cabo en la planta industrial de la empresa en el partido de Pilar, provincia de Buenos Aires. Con la presencia del Jefe de Gabinete de la Nación, Juan Manzur; el Ministro de Desarrollo Productivo de la Nación, Matías Kulfas , el Intendente de Pilar, Federico Achával y el Embajador de Alemania en Argentina, Ullrich Sante, quienes estuvieron acompañados por altos funcionarios nacionales, provinciales y municipales.

“Nos nutrimos de más de 150 años de investigación y desarrollo a nivel global y regional, para ayudar a que las personas y nuestro planeta prosperen, atendiendo las necesidades en el campo de la salud y la alimentación. Vemos los grandes desafíos de hoy y estamos convencidos que juntos podemos hacer más por un futuro mejor. Es nuestra visión, ‘Salud para todos y hambre para nadie’, la que nos mueve y nos impulsa día a día a innovar y por eso reforzamos nuestro compromiso de largo plazo en Argentina, compartiendo con la comunidad nuestro plan de inversiones para los próximos años”, destacó el actual líder de la división agro y futuro CEO de Bayer para Argentina y Cono Sur a partir de noviembre, Juan Farinati.

El plan de inversiones también potenciará la capacidad productiva y la gestión operativa de la planta de productos farmacéuticos de Pilar (la fábrica de comprimidos más grande de Sudamérica y una de las tres más importantes del mundo en su tipo); la planta María Eugenia, en Rojas, la más grande en el mundo en el procesamiento de semillas de maíz; y la de Zárate, especializada en productos para protección de cultivos.

“Parte de esta inversión estará focalizada en desarrollar procesos innovadores que buscarán alcanzar el objetivo de la compañía de convertirse en una organización sin emisiones de carbono para 2030 y reducir el impacto de gases de efecto invernadero en la agricultura en un 30% hacia el mismo año”, destacó la empresa que tiene presencia local desde hace 110 años, cuenta con 12 bases de actividades operativas y tiene 2.200 colaboradores locales.