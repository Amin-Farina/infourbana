+++
author = ""
date = 2021-11-10T03:00:00Z
description = ""
image = "/images/herrera.jpg"
image_webp = "/images/herrera.jpg"
title = "UNA CANDIDATA DE 20 AÑOS Y UN PUEBLO CON UNA SOLA MESA DE VOTACIÓN EN SANTIAGO DEL ESTERO"

+++

#### **Rita Verónica Ibarra será la candidata a comisionada municipal más joven en Herrera, localidad de 150 habitantes, y el paraje Argentina, con una sola mesa de votación,  serán los rasgos particulares de la elección del domingo en la provincia**

Una candidata a comisionada municipal de tan solo 20 años, en la localidad de Herrera, un pequeño paraje cercano a Santa Fe, de 150 habitantes y una sola mesa de votación, serán los rasgos particulares de las elecciones nacionales del próximo domingo en Santiago del Estero, distrito que además elegirá gobernador y legisladores provinciales.

Rita Verónica Ibarra, con 20 años recién cumplidos, se convirtió en la candidata más joven de los comicios que se desarrollarán en 137 comisiones municipales del interior provincial.

Estudiante del Profesorado en Psicología, Ibarra se inscribió con 19 años como candidata para la localidad de Herrera, departamento Avellaneda, por el Frente Renovador, convencida en la necesidad de que los jóvenes se "comprometan" con política, porque "es una herramienta de cambio".

"Orgullosa" de sus raíces, como ella misma se define, Rita -nacida y criada en Herrera- señaló a Télam que su participación política se dio de manera natural: "Siempre viví alrededor de la política", porque "mi padre viene militando hace 30 años aproximadamente, y cuando salía a reuniones siempre lo acompañaba y me gustaba escuchar", relató.

> "Hay muchas cosas por hacer en mi pueblo, por eso decidí tomar esta responsabilidad y el apoyo de la gente me motivó a ser candidata", señalo la joven, que hace apenas unos días cumplió sus 20 años.

Ibarra siente que el hecho de haber asumido una candidatura con tan solo 20 años, la convirtió en un "ejemplo para los jóvenes" santiagueños y un estímulo para "que se animen a participar" en la política.  

> "En estos días me llegaron muchísimos mensajes de chicos agradeciéndome y contándome que ahora ellos ya se animarán para las próximas elecciones a postularse", indicó.

Ibarra vive con sus padres y un hermano de 16, y tiene pensado además de la carrera política, "terminar el profesorado, hacer la licenciatura y seguir estudiando".

La educación y un mejor acceso de los jóvenes de Herrera a carreras terciarias y universitarias son una de las mayores inquietudes que tiene y a ello -dijo- concentrará sus esfuerzos en caso de ganar la elección a comisionada municipal.

"Quiero que nosotros los jóvenes tengamos más oportunidades de capacitaciones y carreras de estudios aquí en nuestro pueblo", sostuvo Rita, quien también focalizaría su gestión en el cuidado del medio ambiente y en la creación de un centro de profesionales en el aspecto sanitario.  

Rita, quién compite con otros cuatro candidatos, dijo que espera que los jóvenes se animen más a participar en política, porque "es una herramienta de cambio".

Son muchos los candidatos a comisionados municipales en los diferentes departamentos de la provincia, y cada uno de los pueblos tiene su impronta y sus características.

#### Los "más" y los "menos" de la elección provincial

Ante esta elección tan amplia y compleja por tratarse de 137 lugares en donde sus pobladores acudirán a votar, existen algunas particularidades.

Una de las comisiones municipales con mayor cantidad de postulantes es la del Zanjón, departamento Capital, en donde existen 12 listas con sus respectivas propuestas, con una población de 3.308 habitantes aproximadamente.

Al otro extremo se encuentra la localidad de Argentina, departamento de Aguirre, a 38 km del límite con la provincia de Santa Fe y a unos 300 kilómetros de la ciudad Capital, en donde sólo hay dos listas y una sola mesa para votar.

Argentina es una localidad ubicada sobre la Ruta Nacional 34, en la cual habitan alrededor de 150 habitantes y por eso sólo tendrá una mesa para votar este domingo para reelegir a su actual comisionado Luis Alberto Antón o bien optar por Gerardo Bertolín.

El Frente Cívico por Santiago lleva candidatos a comisionados en las 137 localidades, en donde tiene listas diferentes, ya que por un lado van los peronistas y por otro los del FC; a su vez Juntos por el Cambio sólo postula a ocho candidatos a comisionados en las localidades de Casares, Sacháyoj, Icaño, Guardia Escolta, Zanjón, Colonia Alpina, Villa Robles y Nueva Francia.

La tercera fuerza de la provincia, Frente Renovador, postula a 38 candidatos para comisionados municipales en las localidades de Zanjón, El Deán, Icaño, Lugones, Herrera, Abra Grande, Cañada Escobar, Estación Simbolar, La Aurora, Los Quirogas y Negra Muerta.

También en San Ramón-La Dársena, Tapso, Villa Rivadavia, La Cañada, Guampacha, Lavalle, El Bobadal, Arenales, El Cuadrado, Matará, KM49, Colonia Alpina, Villa Robles, Villa Hipólita, Los Romanos, Colonia Simbolar, Pozuelos, Los Ovejero, Sotelo, Amicha, El Sauzal, Árraga, Manogasta, La Nena-Simona, Tacañitas, Tomas Young y Averías.