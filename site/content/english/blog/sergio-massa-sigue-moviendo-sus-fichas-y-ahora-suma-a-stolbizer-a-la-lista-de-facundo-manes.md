+++
author = ""
date = ""
description = ""
draft = true
image = ""
image_webp = ""
title = "SERGIO MASSA SIGUE MOVIENDO SUS FICHAS Y AHORA SUMA A STOLBIZER A LA LISTA DE FACUNDO MANES"

+++
SERGIO MASSA SIGUE MOVIENDO SUS FICHAS Y AHORA SUMA A STOLBIZER A LA LISTA DE FACUNDO MANES

OTRO MASSISTA MÁS ACOMPAÑARA LA LISTA DE FACUNDO MANES EN LA PROVINCIA DE BUENOS AIRES

Meses atrás habíamos adelantado el estratégico movimiento de Sergio Massa para desarmar la lista de Juntos por el Cambio en Tigre a través de su candidato Nicolás Massot. Esta vez, el ex intendente de Tigre planea su siguiente paso con suma cautela, al sumar a su socia Margarita Stolbizer a la lista del Neurocientífico Facundo Manes en la provincia de Buenos Aires.

El pasado jueves. la líder del partido GEN, Margarita Stolbizer, había confirmado que apoyaría la candidatura de Manes en las elecciones primarias (PASO) de septiembre.

“_Nos presentaríamos en la boleta de Manes_”, ratificó Stolbizer en diálogo con CNN Radio. “_Empezamos a discutir dentro del partido después de muchos años de intentar un camino de no grieta y nos hemos dado cuenta que, lejos de aflojar la polarización, es cada vez más fuerte_”, señaló durante el programa conducido María Laura Santillán.

Por su parte, Manes viene buscando seducir a los aliados del PRO para competir en la interna con Santilli. Por eso, le llegó a ofrecer el tercer lugar en su lista de diputados nacionales a Emilio Monzó, aliado estratégico de Sergio Massa y padrino político de Nicolás Massot.