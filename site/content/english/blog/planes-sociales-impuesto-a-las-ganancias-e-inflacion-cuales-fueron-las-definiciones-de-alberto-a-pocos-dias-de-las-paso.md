+++
author = ""
date = 2021-09-06T03:00:00Z
description = ""
image = "/images/alberto-fernandez-en-tecnopolis-20210831-1223995-1.jpg"
image_webp = "/images/alberto-fernandez-en-tecnopolis-20210831-1223995.jpg"
title = "PLANES SOCIALES, IMPUESTO A LAS GANANCIAS E INFLACIÓN: ¿CUÁLES FUERON LAS DEFINICIONES DE ALBERTO A POCOS DÍAS DE LAS PASO?"

+++

##### _El jefe del Estado enfatizó que apunta a crear puestos de trabajo formales que vayan reemplazando a las asistencias estatales “de emergencia”. Reforzó que la industria está reactivando, que menos empleados pagan el impuesto a los ingresos y que la carne vacuna bajó gracias al cepo exportador._

En la previa a las elecciones primarias del domingo 12, el presidente Alberto Fernández repasó este lunes una serie de indicadores que, a su criterio dan cuenta de una mejora en la situación económica del país, reforzó que la vacunación contra el coronavirus posibilita la reapertura y la reactivación y planteó que uno de los mayores desafíos que enfrenta su gestión es generar empleo y que se deje de lado la asistencia estatal como principal vía de ingreso para gran parte de los sectores más vulnerados.

“Necesito que cada vez haya menos planes y que cada vez más gente trabaje, Eso no significa que quiero sacar los planes, pero sí que son una salida de emergencia y que deben convertirse en trabajo genuino”, sostuvo el mandatario en una extensa entrevista con El destape radio, en la que volvió a cargar contra Mauricio Macri.

El mandatario planteó que el camino para lograr esa transformación comenzó con el decreto por el cual se estableció que el trabajo rural puede ser compatible con el cobro de un plan Potenciar Trabajo o ser beneficiario de la AUH o la Tarjeta Alimentar, entre otros. Este lunes, los ministerios de Trabajo y de Desarrollo Social mediante la resolución conjunta 8 por la que se implementa “la promoción del trabajo registrado y la protección social de 250.000 personas en labores rurales temporarias de la economías regionales.

La normativa, publicada este lunes en el Boletín Oficial, establece que aquellas personas que se registren cobrarán el salario y los planes sociales hasta 6 meses de trabajo continuo. A partir del séptimo mes, “las prestaciones dinerarias de los planes y programas sociales y de empleo (Programa de Inclusión socio-productiva y desarrollo local) podrán ser consideradas a cuenta del salario de los trabajadores y trabajadoras a partir del sexto mes de iniciada la relación laboral con prestación efectiva de tareas”. Es decir que el plan se transforma en un subsidio al empleador si mantiene en su nómina al trabajador.

El esquema de planes compatibles con empleo rural es una “prueba piloto” que el Gobierno buscará replicar en al menos otros cinco sectores antes de fin de año. La idea, confirmada por Fernández este lunes es que buena parte de esos planes sean reemplazados -en el mediano plazo- por el empleo genuino que, sostienen, ya se está demandando en distintos rubros y lugares del país. Esos puestos laborales podrían darse a través de cooperativas, aunque la apuesta oficial es que sea el sector privado el que los sume a sus dotaciones.

En una especie de “puente” hacia esa reconversión, el Gobierno puso el foco en la industria textil, la construcción y algunas actividades relacionadas con la producción de alimentos como los sectores formales en los que se podría iniciar el camino en los próximos meses. En cada sector se evaluará las necesidades específicas y a la vez se establecerá cuánto tiempo se mantendrá el programa mixto, es decir que los planes sean compatibles con un trabajo formal.

##### EL PRECIO DE LA CARNE Y LA INFLACIÓN A FINES DEL 2022

Fernández, además, hizo mención a la escalada en los precios de los alimentos y a la decisión de limitar las exportaciones de carne vacuna para garantizar precios accesibles al consumidor. “Los frigoríficos y el sector deberían entender que no es justo que la carne se venda en la Argentina a los mismos precios que en Beijing o París porque el costo no es el mismo en esas distintas ciudades”, manifestó.

“Los precios de la carne bajaron desde el momento en que restringimos las exportaciones”, sostuvo y mencionó un informe del Centro de Economía Política Argentina (CEPA), en el que se afirma que los valores al mostrador en los últimos dos meses cayeron 0,1% en julio y 1,4% en agosto.

Según el reporte mensual del Instituto de Promoción de la Carne Vacuna Argentina (IPVCA), los cortes cuyo valor más se redujo en ese último mes fueron la picada común (-4,8%), el vacío (-3,5%) y el asado de tira (-2,8%). Sin embargo, los precios de agosto último están un 77% por encima de un año atrás.

Fernández reafirmó la intención de su Gobierno en apuntar a una baja de la inflación, “algo que se complicó este año, porque en todo el mundo subieron los alimentos”. Indicó que por eso tomaron medidas como Precios Cuidados y las investigaciones y multas contra varias empresas por “posición dominante” y que “se requiere del compromiso de todos” para lograrlo.

Consultado sobre si ve posible una inflación de 1% o menos para 2022, el mandatario evitó hacer pronósticos, pero sostuvo que el objetivo de su administración es “ir reduciendo” las alzas de los precios, en especial de los alimentos, “que es lo que más nos preocupa”.

En otro tramo de la entrevista, además, adelantó que se está analizando “cómo se puede ayudar” a Molino Cañuelas, que se presentó el jueves pasado en concurso de acreedores, a fin de garantizar la continuidad de los 3000 puestos de trabajo que el grupo tiene en todo el país.

“Veremos en qué se la puede ayudar para que continúe en términos razonables. Entre los acreedores está el Banco Nación y la AFIP, hay que analizar la situación y garantizar que sigan los empleos”, indicó.

##### SALARIOS E IMPUESTO A LAS GANANCIAS

El jefe del Estado remarcó que su gestión impulsó durante este año medidas impositivas “que van en contra del sentido regresivo que caracteriza al sistema tributario nacional” y que implicaron “un mejoramiento del salario que es muy importante”.

“Hoy en día, el Impuesto a las Ganancias, claramente, muestra que las PYMES no pagan lo mismo que las grandes empresas. Y, en segundo lugar, los que trabajan, más de un millón de trabajadores y jubilados, dejaron de pagar Ganancias” con la eximición de los sueldos brutos de hasta $150.000 del cálculo del gravamen. Nada dijo, no obstante, sobre el nuevo piso del impuesto, que subirá en torno al 20% por la inflación y la reapertura de paritarias y que el Gobierno anunciará posiblemente tras las PASO del domingo.