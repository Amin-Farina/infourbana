+++
author = ""
date = 2022-03-16T03:00:00Z
description = ""
image = "/images/rusia-y-ucrania-2.jpg"
image_webp = "/images/rusia-y-ucrania-2.jpg"
title = "UCRANIA Y RUSIA ESTÁN DEBATIENDO UN ACUERDO DE 15 PUNTOS PARA PONERLE FIN A LA GUERRA"

+++
#### El acuerdo establece que Ucrania no podrá formar parte de la OTAN y sus fuerzas armadas sufrirán limitaciones

Mientras Rusia continúa asediando las principales ciudades ucranianas con bombardeos que incluyen edificios civiles, los gobiernos de ambos países debaten un posible acuerdo para ponerle fin a la guerra. Y de allí surgió en los medios un borrador que tiene 15 puntos que incluyen la renuncia de Kiev a integrarse a la OTAN y también limitaciones a sus Fuerzas Armadas.

El texto, adelantado por el Financial Times, daría cuenta de un "compromiso" en algunas cuestiones clave, en sintonía con lo que había adelantado en la mañana de este miércoles el Canciller ruso Serguei Lavrov.

“Hay una serie de formulaciones de los acuerdos con Ucrania sobre el estatuto de neutralidad y las garantías de seguridad que han estado a punto de lograrse”, destacó Lavrov después de una nueva ronda de negociaciones. Pero desde el lado ucraniano dicen que sólo se divulgaron los puntos que exige Putin.

El borrador del supuesto acuerdo que difundió el FT incluye que Ucrania no albergaría bases militares o armas extranjeras a cambio de protección de aliados como Estados Unidos, Reino Unido y Turquía.

El posible acuerdo también menciona disposiciones sobre la consagración de los derechos del idioma ruso en Ucrania, donde el ucraniano es el único idioma oficial.

Por su parte, Rusia pide que Ucrania reconozca su anexión de Crimea en 2014 y la independencia de dos estados separatistas en la región fronteriza oriental de Donbas.

Pero desde Ucrania aseguran que "el Financial Times publicó un borrador que muestra la solicitud de la parte rusa y no más".

"La parte ucraniana tiene sus posiciones. Lo único que confirmamos en esta etapa es el alto el fuego, retirada de las tropas rusas y garantías de seguridad de varios países", dijo Mykhailo Podolyak, uno de los negociadores y a la vez consejero del presidente ucraniano.

Volodimir Zelenski, por su parte, reconoció en las últimas horas que las negociaciones con la delegación rusa parecían "más realistas", aunque este miércoles insistió con el pedido de ayuda a Estados Unidos y los países occidentales para "para poner fin a este terror".

Por estas horas, el conflicto avanza a dos bandas. Los responsables de la diplomacia intentan llegar a un acuerdo para ponerle fin a la guerra, pero por el otro Rusia doblega sus ataques y asedia ciudades ucranianas.

Es que la rueda de las tratativas, que ya entra en su sexta ronda, no enfría el ataque ruso. Por el contrario. Rusia aumenta sus bombardeos sobre Kiev y otras ciudades como la portuaria de Mariupol, en una cruenta ofensiva.

Las fuerzas rusas atacaban este miércoles la región de Kiev, la capital de Ucrania, y otras ciudades grandes, en un intento de aplastar una defensa ucraniana que ha frustrado sus avances casi tres semanas después de invadir el país.

El avance ruso por tierra sobre Kiev seguía paralizado pese al bombardeo continuo.

Los vecinos de la capital se refugiaban en viviendas y refugios durante un toque de queda declarado en la ciudad hasta el jueves por la mañana, mientras Rusia arrojaba proyectiles sobre la urbe y sus alrededores. Un edificio de departamentos de 12 plantas en el centro de la ciudad estalló en llamas tras ser alcanzado por metralla.

Zelenski explicó que las fuerzas rusas no habían podido adentrarse más en territorio ucraniano, aunque habían continuado su intenso bombardeo sobre ciudades.

Para Ucrania, las fuerzas de Putin están tratando de cortar las arterias de transporte de Kiev y de destruir las capacidades logísticas mientras planean un ataque a gran escala para tomar la capital.

Mientras la invasión iniciaba su tercera semana, el presidente ucraniano sugirió que aún había algunos motivos para ser optimista sobre que las negociaciones pudieran producir un acuerdo con el gobierno ruso.

Después de la reunión por videoconferencia de las dos delegaciones, Zelenski dijo que las demandas rusas se estaban haciendo "más realistas''.

#### 