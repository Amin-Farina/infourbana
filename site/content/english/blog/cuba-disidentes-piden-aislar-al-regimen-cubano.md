+++
author = ""
date = 2021-07-27T03:00:00Z
description = ""
image = ""
image_webp = ""
title = "CUBA: DISIDENTES PIDEN AISLAR AL RÉGIMEN CUBANO"

+++

**_El opositor Movimiento Cristiano Liberación (MCL) de Cuba pidió este martes a la comunidad internacional que adopte “medidas concretas de aislamiento” del régimen cubano hasta que libere a todos los detenidos por participar en las protestas pacíficas de este mes y a los presos políticos y de conciencia._**

“Consideramos que la comunidad internacional debe responder enérgicamente ante la represión desatada por el régimen cubano. Por eso es necesario una fuerte condena pública de la dictadura”, dijo la organización fundada por el ya desaparecido Osvaldo Payá en un comunicado colgado en Facebook.

El MCL añadió que aunque las declaraciones de condena son necesarias, “no son suficientes para hacerle saber a la junta militar en el poder en Cuba que la comunidad internacional no va a tolerar la impunidad de la dictadura”.

Por ello pidió 11 “medidas concretas de aislamiento internacional, como las implantadas al (extinto) régimen del ‘apartheid’ (segregación racial) de Sudáfrica”, según el comunicado firmado por Eduardo Cardet, coordinador nacional de movimiento, Antonio Díaz Sánchez, secretario general, y Regis Iglesias, portavoz.

Además de excluir al régimen cubano de todos los foros, cumbres y eventos internacionales, MLC pide que se suspendan todos los acuerdos de cooperación económica y militar con la isla y que el Consejo de Derechos Humanos de la ONU investigue a Cuba por violaciones de derechos humanos.

También reclaman que no se otorguen líneas de crédito al régimen cubano, que se desestimule la inversión extranjera y el turismo internacional hacia Cuba y se realice un boicot a los productos exportados desde la isla, directa o indirectamente.

Además, el MLC pide un embargo internacional de armas y equipamiento represivo a Cuba y que se suprima la participación de representantes cubanos de todos los eventos deportivos, culturales y académicos internacionales.

“Que no se otorguen o se revoquen visas a funcionarios de la junta político-militar y sus familiares, así como a los miembros del Partido Comunista de Cuba y a todos los miembros de las organizaciones e instituciones que apoyen o participen en los actos represivos contra el pueblo” es otra de las medidas.

Como parte de esta campaña de aislamiento al régimen y en solidaridad con el pueblo cubano se deberían abrir canales de envío de ayuda humanitaria y se debe crear una comisión internacional de apoyo a la democracia en Cuba, que “promueva la ejecución de estas y otras medidas y vele por el cumplimiento de las mismas”, concluye el comunicado.