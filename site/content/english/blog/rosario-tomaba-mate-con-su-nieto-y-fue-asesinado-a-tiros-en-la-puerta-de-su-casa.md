+++
author = ""
date = 2022-01-25T03:00:00Z
description = ""
image = "/images/canajpg-1.jpg"
image_webp = "/images/canajpg.jpg"
title = "ROSARIO: TOMABA MATE CON SU NIETO Y FUE ASESINADO A TIROS EN LA PUERTA DE SU CASA"

+++

##### **_Un hombre de 63 años fue acribillado de al menos once disparos en la esquina de Virasoro y Constitución. Recibió dos tiros en la cabeza justo cuando intentaba proteger a su nieto con su cuerpo. Otra semana de violencia en la ciudad santafecina_**

Los vecinos de la esquina de Virasoro y Constitución, en la ciudad de Rosario, quedaron consternados en las últimas horas por el asesinato de “Cachilo”, un hombre de 63 años que fue acribillado por un sicario a bordo de una moto cuando tomaba mates en la puerta de su casa junto a su nieto.

“Cachilo” también era conocido como “Cabezón”. Se llamaba Oscar Alberto Sosa y había vivido toda su vida en el barrio. Según informó el diario La Capital, el hombre murió de dos disparos en la cabeza tras haber sido atacado por dos hombres a bordo de una moto.

El hecho ocurrió el lunes cerca de las 17.30, cuando Sosa se encontraba tomando mates junto a su nieto en la puerta de su domicilio, una práctica habitual a lo largo de los últimos años.

De acuerdo al relato de testigos, en un momento apareció una moto negra de 110 cc. con dos personas arriba. El acompañante se bajó del rodado y efectuó sin mediar palabra una ráfaga de al menos 11 disparos contra la entrada de la casa de “Cachilo”.

Sosa se lanzó de inmediato hacia su nieto con la intención de cubrirlo con su cuerpo y fue alcanzado por dos proyectiles en su cabeza, lo que lo dejó tirado en el piso inherte. Moriría a los pocos minutos.

Un vecino lo llevó en su auto particular al Hospital de Emergencias de Rosario (Heca) pero los médicos no pudieron hacer nada. Por su lado, los peritos de la Agencia de Investigación Criminal (AIC) que trabajaron en el lugar recogieron 11 vainas servidas de proyectiles calibre 11 mm.

De acuerdo a los medios locales, Sosa se había jubilado por invalidez y cursaba un cuadro de demencia senil desde hacía al menos un año. Pasaba sus días vendiendo repasadores en el barrio y toda la zona oeste. Sus vecinos aseguraron que no tenía problemas con nadie.

Sosa también padecía un cáncer de riñón y era muy conocido en el Club Provincial, donde jugaba desde hace tiempo en el equipo de veteranos.

Actuó en la causa el fiscal Gastón Ávila, quien ordenó el relevamiento de las cámaras de seguridad de la zona con el fin de estudiar el punto de origen y el de escape de los homicidas.

En tanto, muchos vecinos aseguraron no haber visto nada en la cuadra. Se sospecha que el miedo de realizar declaraciones les hizo a muchos no brindar ningún tipo de ayuda a la causa.

Rosario volvió así a ser noticia por un crimen en una semana donde el clima se puso aún más tenso debido a las detenciones y posteriores amenazas en el núcleo de la familia Cantero.

El último sábado, Lorena Verdún -viuda del ex líder de la narcobanda “Los Monos”, Claudio “Pájaro” Cantero, asesinado en 2013- fue imputada junto a su hijo de 18 años, Uriel Luciano Cantero, por la tenencia ilegal de varias armas de fuego. Uriel Luciano fue acusado de regentear una organización armada dedicada a diversos aprietes y extorsiones para obtener dinero, con un fusil FAL en su arsenal. En la audiencia en la que fue formalmente acusada, Verdún pidió que dejen de “agarrársela” con su familia e hizo una dura advertencia hacia el fiscal Matías Edery, uno de los históricos investigadores que le siguen el rastro a Los Monos y que encarceló a varios de sus miembros jerárquicos como “El Viejo” y “Guille” Cantero.

“Esto no va a quedar así”, le dijo, para continuar detenida. Poco después, la violencia recrudeció. El cartel que ilustra esta nota, que advierte que “si siguen verduguiando (sic) a la familia de los alto perfil, para la próxima vamos contra las familia de los empleados de servicio”, fue hallado en la Oficina de Asuntos Penitenciarios en la esquina de Alsina y Pellegrini, barrio Echesortu, el macrocentro de Rosario, de cara a la audiencia del clan Cantero. El domingo por la mañana, según confirmaron fuentes del caso, la fachada de la Oficina recibió un disparo.