+++
author = ""
date = 2021-10-22T03:00:00Z
description = ""
image = "/images/alberto-fernandez-1249926-2.jpg"
image_webp = "/images/alberto-fernandez-1249926-1.jpg"
title = "ALBERTO FERNÁNDEZ: \"NO ES FUNCIÓN DEL GOBIERNO BRINDAR MÁS SEGURIDAD EN LA REGIÓN\""

+++

##### **_El presidente Alberto Fernández le envió hoy una carta a la gobernadora de Río Negro, Arabela Carreras, para informarle que enviará gendarmes por la escalada de violencia generada por grupos autodenominados mapuches. No obstante, planteó que no es responsabilidad del Ejecutivo nacional reforzar la seguridad de la región._**

En el texto de la carta, Jefe de Estado aclaró: “He decidido asistir a la provincia en su cargo con efectivos de Gendarmería que patrullarán la zona en cuestión. No obstante, es imperioso aclarar que no es una función del gobierno nacional reforzar el control en las rutas nacional o brindar mayor seguridad en la región”.

En la misiva, además, el Presidente aconsejó que, para prevenir futuros incidentes, la provincia patagónica forme “un cuerpo específico que se ocupe de los refuerzos del control y mayor seguridad en el futuro”.

Más temprano, el ministro de Seguridad, Aníbal Fernández, afirmó que la decisión de reforzar la seguridad en Río Negro con el redespliegue de fuerzas federales tras el ataque al Club andino Piltriquitrón, en El Bolsón, fue adoptada por “solidaridad” con la gobernadora Arabela Carreras y para “colaborar” con la provincia.

“La posición que asumimos en el día de ayer, porque ya ha habido varios desmanes, es la de colaborar. Y lo estamos haciendo en este momento. Pero la escuché a la gobernadora de Río Negro diciendo que ella no pedía favores y que era nuestra obligación hacerlo. No señora, no es nuestra obligación”, contestó hoy Fernández a la mandataria provincial, en declaraciones a radio del Plata.

“Es cuestión de leer la Ley de Seguridad Interior y se dará cuenta de que la participación de las fuerzas federales en las provincias solo está habilitada cuando hay complicaciones respecto de restituir el valor de la institucionalidad, de volver las cosas a la normalidad”, agregó el funcionario nacional.

“No es nuestra obligación, ni la responsabilidad del Estado Nacional. Ella tiene una policía con la que podría crear un cuerpo específicamente dedicado a los mapuches o a esta situación que tiene. Hay como 167 comunidades en la zona que ya han resuelto por la vía pacífica estas cosas”, explicó el Ministro de Seguridad.

Durante la madrugada de ayer, un incendio destruyó el Club andino Piltriquitrón, donde se encontraron panfletos contra la gobernadora de Río Negro; el intendente de El Bolsón, Bruno Pogliano; y empresarios extranjeros que son propietarios de tierras de la zona.

En la misma línea se manifestó la nueva portavoz presidencial, Gabriela Cerruti, que encabezó hoy su primera conferencia de prensa. “Es un conflicto que tiene que resolver la gobernadora”, indicó. Y completó con un pedido a la oposición para que este tipo de incidentes “no sean utilizados de manera electoral”.

En medio de las declaraciones cruzadas, Carreras solicitó hoy que el Ejecutivo “cumpla” con ese “envío o redistribución de las fuerzas federales” para prevenir nuevos ataques y cuestionó hoy la actuación del Instituto Nacional de Asuntos Indígenas (INAI), un organismo que depende de autoridades nacionales.

“Su titular, Magdalena Odarda, y su segundo, ambos de Río Negro, tienen una militancia exacerbada en torno a ciertos temas y una larga data de expresiones y actuaciones desafortunadas. Este sector promovió que Parques Nacionales se retirara de la querella y esto no contribuye al esclarecimiento” de los incidentes, expresó Carreras en radio Con Vos.

Por decisión del directorio de la Administración de Parque Nacionales, organismo dependiente del Ministerio de Medio Ambiente y Desarrollo Sostenible que comanda Juan Cabandié, el Estado argentino desistió de avanzar en la causa por usurpación de tierras privadas en Villa Mascardi, ubicado a unos 35 kilómetros de la ciudad de San Carlos de Bariloche.

Según una resolución firmada el pasado domingo 27 de junio, Parque Nacionales retiró la querella por usurpación contra la agrupación mapuche Lafken Winkul Mapu, que desde 2017 mantiene tomado un predio de unas 30 hectáreas.