+++
author = ""
date = 2021-06-29T03:00:00Z
description = ""
image = "/images/congreso_nacional_buenos_aires.jpg"
image_webp = "/images/congreso_nacional_buenos_aires.jpg"
title = "EL GOBIERNO BUSCA AVALAR EL DNU SOBRE RESTRICCIONES ANTE LA SEGUNDA OLA"

+++

**_El oficialismo buscará este miércoles en la comisión Bicameral Permanente de Trámite Legislativo respaldar los Decretos del Poder Ejecutivo que extienden hasta fin de año la emergencia sanitaria y prorrogan hasta el 9 de julio las restricciones a la circulación._**

El oficialismo buscará este miércoles en la comisión Bicameral Permanente de Trámite Legislativo respaldar los Decretos de Necesidad y Urgencia (DNU) del Poder Ejecutivo Nacional que extienden hasta fin de año la emergencia sanitaria y prorrogan hasta el 9 de julio las restricciones a la circulación, ante la segunda ola de contagios de coronavirus que afecta al país.

La comisión que preside el diputado nacional Marcos Cleri (Frente de Todos), fue citada para este miércoles a las 9 para evaluar los dos DNU dictados por el presidente Alberto Fernández para contener la propagación del coronavirus.

Fuentes parlamentarias del oficialismo señalaron a Télam que así como lo hicieron en las últimas reuniones, se intentará emitir dictamen en comisión, y que luego las normativas se aprueben en el recinto de sesiones del Senado, ya que de esta forma, un rechazo sólo pude darse si se produce un voto negativo en las dos cámaras.

Los legisladores debatirán el DNU 167, emitido el 11 de marzo de 2021, que extiende la Emergencia Sanitaria hasta el 31 de diciembre y el 411, del 25 de junio de 2021, que prorroga medidas de contención para mitigar la propagación del virus hasta el próximo 9 de julio.

Uno de los temas claves que estará dentro de la discusión son las restricciones de los vuelos que llegan desde el exterior, y en lo relacionado al apartado que estableció que podrán arribar al aeropuerto de Ezeiza hasta 600 pasajeros, una medida que es rechazada desde Juntos por el Cambio.

En los considerandos del decreto 411 el Gobierno nacional se refiere a la preocupación por las nuevas variantes de virus, en especial la Delta, que tiene mayor contagiosidad y transmisibilidad que las detectadas con anterioridad.

#### Situación epidemiológica

En cuanto a la situación epidemiológica actual del país, en los considerandos de la norma se indica que "entre las semanas 15 y 19 se observó un pequeño descenso de casos posiblemente relacionado con las medidas implementadas a partir del 15 de mayo" y que "se registra un descenso de casos en la gran mayoría de las jurisdicciones".

El descenso observado en las últimas semanas a nivel nacional se refleja también en los grandes centros urbanos, indica el DNU, y señala que "en las últimas semanas se comenzó a registrar un descenso en el número de personas internadas".

Se precisa además que 21 de los 24 distritos del país "presentan alta incidencia en promedio -250 casos cada 100 mil habitantes en los últimos 14 días-, y 7 de ellos superan los 500 casos cada 100 mil habitantes en los últimos 14 días,12 menos que las 2 semanas previas".

Asimismo precisa que las jurisdicciones con mayor ocupación promedio de camas de terapia intensiva son las provincias de Neuquén, Santa Fe, Corrientes, Río Negro, Salta, San Juan y Córdoba.