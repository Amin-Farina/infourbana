+++
author = ""
date = 2021-07-23T03:00:00Z
description = ""
image = "/images/maduro-guaido-1.jpg"
image_webp = "/images/maduro-guaido.jpg"
title = "¿NICOLÁS MADURO QUIERE INICIAR NEGOCIACIONES CON LA OPOSICIÓN?"

+++

**_El presidente de Venezuela, Nicolás Maduro, se declaró este jueves listo para sentarse a negociar con la oposición venezolana en México._**

“Estamos listos para ir a México”, dijo el mandatario, a pesar de que en los contactos con dirigentes opositores y el gobierno de Noruega, que media entre las partes, nunca se mencionó a ese país como eventual sede de una cumbre.

Hasta el momento, no existe una fecha para que las dos delegaciones se sienten en una mesa para comenzar las negociaciones. Sin embargo, Maduro ha insistido en varias ocasiones sobre la posibilidad de que las reuniones se lleven a cabo en ese país gobernado por el presidente Andrés Manuel López Obrador, de izquierda.

Maduro dijo que el presidente del Parlamento, Jorge Rodríguez y el gobernador del céntrico estado Miranda, Héctor Rodríguez, “ya le han comunicado a todas las delegaciones de la oposición y del Gobierno de Noruega que estamos listos para ir a México”.

Durante un acto oficial, el mandatario dijo que ambas partes deben sentarse con “una agenda realista, objetiva, verdaderamente venezolana, para tratar todos los asuntos que haya que tratar, para llegar a acuerdos parciales por la paz y la soberanía y de Venezuela”, así como “para que se levanten” las sanciones internacionales que pesan sobre el país.

Y añadió: ”Estamos listos. Además, ahora que han dado buenos gestos todos los líderes de la oposición y están anunciando sus candidaturas, me dan una gran alegría”.

Venezuela tendrá elecciones regionales el 21 de noviembre en los que se deben renovar todos los cargos ejecutivos y legislativos de los 23 estados y 335 municipios del país.

#### Qué dice la oposición de Venezuela

En mayo pasado, el líder opositor Juan Guaidó anunció su disposición a negociar con el Gobierno de Maduro para alcanzar un “acuerdo de salvación nacional” con el fin de superar la crisis que vive Venezuela. El dirigente aspira a conseguir elecciones generales “libres” y “justas”.

Maduro le respondió que está listo para reunirse con “toda la oposición”, pero exigió que se levanten las sanciones internacionales que pesan sobre el país antes de sentarse a dialogar.

Sin embargo, Guaidó planteó el levantamiento de estas sanciones a condición de que el Gobierno ceda en sus posiciones con el fin de llegar a unas elecciones “democráticas y transparentes”.

Mientras tanto, el titular del Parlamento, Jorge Rodríguez, encabeza desde enero, cuando asumió su cargo, una comisión parlamentaria que busca incluir a numerosos sectores políticos, empresariales, religiosos y sociales de Venezuela.

Maduro anunció que Jorge Rodríguez y Héctor Rodríguez “son los voceros de la revolución para las negociaciones con todas las oposiciones”. Ambos ya encabezaron la delegación oficialista en las fallidas negociaciones que en 2019 trataron de abrir un proceso de diálogo bajo el liderazgo unitario de Guaidó.