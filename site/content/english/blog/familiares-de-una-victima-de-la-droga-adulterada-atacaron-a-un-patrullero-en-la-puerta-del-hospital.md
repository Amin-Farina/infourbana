+++
author = ""
date = 2022-02-03T03:00:00Z
description = ""
image = "/images/sm-1.jpg"
image_webp = "/images/sm.jpg"
title = "FAMILIARES DE UNA VÍCTIMA DE LA DROGA ADULTERADA ATACARON A UN PATRULLERO EN LA PUERTA DEL HOSPITAL"

+++

##### **_Fue en protesta contra la Policía de Hurlingham por la cocaína envenenada que ya mató a 17 personas en el conurbano bonaerense_**

Una serie de incidentes se desataron este miércoles por la tarde frente al hospital de Hurlingham donde está internado una de los intoxicados con la droga adulterada que ya mató a 17 personas.

Algunos familiares de esta persona atacaron a un patrullero de la Policía local que se encontraba estacionado frente al centro de salud.

De los 17 fallecidos que hay hasta el momento siete de ellos fallecieron en hospitales de Hurlingham, otros siete en San Martín y los tres restantes en Tres de Febrero.

Además de los muertos, otras 56 personas debieron ser internadas tras consumir cocaína envenenada o adulterada en las mencionadas localidades del Conurbano, según informaron desde el Ministerio de Seguridad de la Provincia de Buenos Aires.

La fiscalía que investiga el caso advirtió que existe circulación de estupefaciente con “altísima toxicidad”.

> El ministro de Seguridad, Sergio Berni, advirtió que “quienes compraron cocaína en las últimas 24 horas tienen que descartarla”.

A su vez, el ministro de Salud bonaerense Nicolás Kreplak dijo que “nos preocupa que haya más fallecidos por haber consumido esta droga que estén en sus casas”.

Droga envenenada: el comunicado de la fiscalía tras las muertes por “alta intoxicación”

Fue la Fiscalía General de San Martín la que confirmó tales internaciones y fallecimientos por las secuelas del consumo de la droga. Si bien no abundan las precisiones alrededor del hecho, fuentes judiciales informaron a TN que la Justicia centró su investigación en descubrir las causales de los decesos y de las intoxicaciones.

En su mensaje, la Fiscalía destacó: “Se comunica dicha información a la población en general con el fin de que adopten comportamiento positivos con el fin de protegerse a si mismos y cuidar de su salud”.

Descartan que la cocaína tenga veneno para ratas y temen que aumenten las víctimas

El Ministerio de Seguridad bonaerense descartó esta tarde que la cocaína haya sido adulterada con veneno para ratas y afirmó que investigan el químico que tenía la droga.

En los allanamientos, encontraron 200 envoltorios de marihuana y 400 de cocaína.