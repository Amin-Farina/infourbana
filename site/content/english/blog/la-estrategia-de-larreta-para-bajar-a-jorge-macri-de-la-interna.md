+++
author = ""
date = 2021-07-05T03:00:00Z
description = ""
image = "/images/horacio-rodriguez-larreta-y-jorge-macri-buscan-una-salida-la-interna-del-pro-provincia-buenos-aires-foto-archivo-1.jpg"
image_webp = "/images/horacio-rodriguez-larreta-y-jorge-macri-buscan-una-salida-la-interna-del-pro-provincia-buenos-aires-foto-archivo.jpg"
title = "LA ESTRATEGIA DE LARRETA PARA BAJAR A JORGE MACRI DE LA INTERNA"

+++

**_Llevan tres reuniones en siete días y no logran un acuerdo. En la Ciudad creen que abrir el gobierno porteño es un gesto que podría entusiasmar al intendente._**

Sin Patricia Bullrich en el camino inmediato, el jefe de Gobierno porteño Horacio Rodríguez Larreta está decidido a ordenar las candidaturas del PRO en la provincia de Buenos Aires. El primer lugar en la lista que pergeña es para su vicealcalde Diego Santilli, pero en el horizonte bonaerense todavía queda una amenaza: el intendente de Vicente López, Jorge Macri, que se niega a bajar su candidatura. Luego de tres intentos fallidos, el alcalde estudia la posibilidad de ofrecerle un ministerio de su gabinete para contener el objetivo del primo del expresidente de proyectar su imagen por fuera del municipio que conduce desde 2015.

La oferta concreta será que ocupe desde el año que viene el ministerio de Gobierno porteño, que actualmente conduce Bruno Screnci Silva, del riñón de Santilli. Tal como contó este portal, Rodríguez Larreta también estudia abrir su administración a representantes de la exgobernadora María Eugenia Vidal para acompañar su candidatura porteña.  Una opción pasa por dividir en dos el ministerio de Justicia y Seguridad porteño y entregarle la nueva cartera judicial al exministro bonaerense Gustavo Ferrari. Otra giró en torno a la posibilidad de poner al jefe del bloque de diputados del PRO, Cristian Ritondo, al frente de un futuro ministerio porteño de Seguridad, con el secretario Marcelo D'Alessandro en segundo lugar.

A Ritondo lo tentaba más el ministerio de Gobierno que el de Seguridad, pero ahora la cartera que conduce Screnci será ofrendada como parte de las alternativas que Rodríguez Larreta diseña para allanarle el camino a Santilli. 

 Este lunes, Larreta y Jorge Macri volvieron a reunirse. Fue el primer round de esta semana y el tercero de los últimos siete días. Cerca de Macri explicaron que no hubo cambios en la posición del intendente. Se trató de una reunión a solas, de una hora y media, donde buscaron "alternativas y soluciones para evitar la ruptura del espacio en la provincia", detallaron desde Vicente López. La explicación refleja el nivel de tensión que mantiene Macri sobre la negociación bonaerense. Si no se baja, encabezará una lista propia de precandidatos para medirse con Facundo Manes, de la UCR y también con Santilli, que trabaja con empeño para posicionarse como el único aspirante del partido amarillo en la provincia.

En la sede capitalina negaron que el alcalde "le haya ofrecido" el cargo al primo del expresidente, pero según pudo reconstruir este portal la posibilidad es parte de una oferta que entrará en juego en medio de un tironeo empantanado. "Están dialogando y trabajando en encontrar puntos de encuentro para lograr un entendimiento y finalizar en una lista de unidad del PRO, pero en ello no está tal ofrecimiento", insistieron.

La posibilidad de que Macri deje Vicente López no es nueva. Forma parte del imaginario de poder de su entorno desde el año pasado, cuando el partido amarillo empezó a transitar el despoder a nivel nacional y en la provincia. "Lo único que haríamos sería sacar el apellido de allá y traerlo a la capital, con todos los beneficios que eso significa", argumentó un funcionario porteño para defender la oferta y sostener que el cambio de domicilio pondría a Macri en otro lugar expectante para mostrarse como un competidor por la sucesión de Rodríguez Larreta en 2023.

En rigor, la cartera de Gobierno le permitiría contar con una proyección nacional desde el epicentro capitalino, pero la arena porteña también lo reencontraría con Vidal, que desde este fin de semana es la virtual cabeza de lista porteña, ante la salida de Bullrich, que ahora reclama espacio para los suyos.

Un puesto en el Gabinete larretista es una nueva variante que maneja el PRO porteño, luego de analizar la opción de proponerle que encabece la lista de senadores bonaerenses, una oferta que el intendente considera demasiado exigua.