+++
author = ""
date = 2022-01-14T03:00:00Z
description = ""
image = "/images/gondola-1.jpg"
image_webp = "/images/gondola.jpg"
title = "TOMATE, QUESO, PESCADO Y CARNE, LOS ALIMENTOS QUE MÁS AUMENTARON Y EMPUJARON LA INFLACIÓN DE 2021"

+++

#### **_La inflación de 2021, con un 50,9%, fue la segunda más alta de los últimos 30 años, apenas superada por el 53,8% de 2019. Con la suba de los alimentos jugando un papel clave en esa cifra de casi un 51%, el tomate, el queso, el pescado y la carne encabezaron el listado de los que más aumentaron en el año en este rubro._** 

En tanto, entre las bebidas, el vino común quedó en primer lugar; y de los artículos de limpieza y tocador, el número uno fue el desodorante.

En sentido contrario, bajaron de precio el zapallo anco, la batata, la papa y la naranja, según informó este jueves el Instituto Nacional de Estadística y Censos (Indec).

##### Ránking de productos que más aumentaron de precio en 2021

De acuerdo con los datos del Indec, el único alimento que superó el 100% de aumento de forma interanual fue el tomate redondo, que subió 111% en diciembre de 2021 respecto del mismo mes de 2020. En el caso de las bebidas, el vino común se encareció un 108%.

Por encima de la media del 50,9% se ubicaron en total 22 productos. Además del tomate redondo y el vino común, también superaron ese valor:

* Queso sardo, que subió 76%
* Café molido, 72%
* Filet de merluza fresco, 71%;
* Leche en polvo entera, 70%
* Queso pategrás y la carne picada común, 69%
* Yogur firme, 68%

Además, entre ese grupo de productos se encuentran:

* El cuadril y el asado, que aumentaron en 2021 un 67%
* Queso cremoso, la nalga y el dulce de leche, 66%
* Paleta, 65%
* Manteca y aceite de girasol, 63%
* Banana, 60%
* Leche fresca entera en sachet, 58%
* Jamón cocido, 55%
* Yerba mate, 52%
* Salchichón, 51%