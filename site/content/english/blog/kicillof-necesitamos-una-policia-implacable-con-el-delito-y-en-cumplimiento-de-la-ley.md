+++
author = ""
date = 2021-12-13T03:00:00Z
description = ""
image = "/images/moviles2-678x381-1.jpg"
image_webp = "/images/moviles2-678x381.jpg"
title = "KICILLOF: \"NECESITAMOS UNA POLICÍA IMPLACABLE CON EL DELITO Y EN CUMPLIMIENTO DE LA LEY\""

+++

#### **_El gobernador bonaerense destacó que su Gobierno decidió "generar una transformación" de la Policía debido al "desprestigio que acosaba a la fuerza", y prometió ser "implacable" ante un hecho "de corrupción o una violación de los derechos humanos"._**

El gobernador bonaerense, Axel Kicillof, afirmó este lunes que se trabaja para contar con una Policía "que sea implacable con el delito y en cumplimiento de la ley" y prometió ser "implacable" ante un hecho "de corrupción o una violación de los derechos humanos".

El mandatario formuló esas declaraciones al conmemorar el bicentenario de la creación de la Policía de la provincia, acto en el que también se tomó juramento de fidelidad a 3.000 agentes egresados de la escuela "Juan Vucetich", que se formarán parte del Operativo Sol durante le verano.

"Este día es muy importante porque tras dos años de pandemia, la formación en nuestra policía se dio en condiciones casi imposibles", planteó Kicillof y remarcó que "se cumplen los 200 años de la Policía de la provincia".

Destacó que su Gobierno decidió "generar una transformación" de la Policía debido al "desprestigio que acosaba a la fuerza" y analizó que "es fundamental reconciliarnos con el pueblo, ganar su confianza y afecto".

"Cuando asumimos, vimos que los salarios de la policía habían perdido un 25% en cuatro años; su equipamiento estaba ausente o destruido: faltaban armas, chalecos, patrulleros, tecnología y había problemas graves en la formación", rememoró.

Luego, contó que "con una inversión enorme" su administración logró "la equiparación salarial de la policía provincial con la de las fuerzas federales" y recordó que se dispuso un plan de inversión "sin precedentes en la historia: 38 mil millones de pesos para equipar la fuerza y comprar móviles".

"Nos proponemos cambiar la forma en que se prepara nuestra policía porque hace falta darle jerarquía universitaria a la formación policial. El Instituto Universitario Vucetich cambió sus programas y llevó adelante un proceso de formación con teoría y práctica, abrimos cursos de manejo, generamos cursos de entrenamiento en uso de armas. Esto lo hacemos porque necesitamos una policía más profesional y más preparada", continuó.

Y remarcó: "Esta inversión millonaria tiene un objetivo. Necesitamos una Policía que sea implacable con el delito y que haga cumplir la ley dentro de la ley. No vamos a soportar y seremos implacables con todo hecho de corrupción o violación a los derechos humanos".

Sostuvo que "el desafío es contar con una nueva Policía para la provincia y para eso vamos a duplicar la cantidad de agentes en los próximos años" y añadió que se necesita "una Policía comprometida y solidaria, como lo ha mostrado en la pandemia" de coronavirus.

"Necesitamos una Policía que sea más humana y que forme parte de nuestro pueblo", concluyó.

Participaron del acto el ministro de Seguridad provincial, Sergio Berni; la ministra de Gobierno, María Cristina Álvarez Rodríguez; el jefe de Asesores, Carlos Bianco; y el vicepresidente primero del Senado bonaerense, Luis Vivona.

"Nuestra institución cumple 200 años", dijo Berni a su turno y agregó: "Esta Policía nació con la patria misma; acompañó a nuestros próceres en el cumplimiento de ese sueño de libertad".

"La Policía vio crecer a la patria, consolidarse, gestarse como una potencia mundial. También la vio caerse, pero nunca la vio rendirse porque no está en el espíritu de ningún argentino bajar los brazos", prosiguió.

En ese contexto, el ministro planteó que "un policía nunca debe apartarse de la historia de la nación, no debe olvidarse de dónde viene y jamás deberá apartarse de los principios por los cuales están aquí hoy".

Asimismo, puso de relieve que actualmente se lleva adelante "el mayor desafío institucional que haya tenido la policía en sus 200 años: duplicar su capacidad operativa en dos años".

Berni pidió a los flamantes efectivos "ser leales no solamente a la patria y a su profesión, sino a su vocación", pidió que ejerzan "esa autoridad que les delega la Constitución con la firmeza y la humildad que caracteriza a los profesionales que saben lo que hacen, pero nunca con superioridad, abusos o arbitrariedad".

"Mucho menos con improvisación porque la sociedad valora a aquellos hombres y mujeres que están dispuestos a dar la vida para cuidarlos", acotó.

El ministro razonó que "el orden es un pilar en la construcción de la Argentina", evaluó que "sin policías honestos, valerosos y eficientes, junto con jueces y fiscales que ayuden en el ejercicio de la profesión, se condena a nuestra provincia a la desconfianza y el caos".

"Créanme que nada duele más que cuando la justicia y la sociedad condenan y deprecian a quienes se apartan del ejercicio correcto de sus funciones, pero nada enorgullece más que el respeto, la confianza y el valor que deposita cada habitante en ustedes", señaló.

"Estoy seguro que no los van a defraudar. Les reclamo por eso orden, subordinación y valor", cerró Berni.