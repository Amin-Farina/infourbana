+++
author = ""
date = 2022-01-12T03:00:00Z
description = ""
image = "/images/z2f3ghkwk5hjrejnwajrks2e64.jpg"
image_webp = "/images/z2f3ghkwk5hjrejnwajrks2e64.jpg"
title = "ESTE ES EL RANKING DE LOS PASAPORTES MÁS FUERTES EN 2022"

+++

##### **_Japón sigue teniendo el pasaporte “más fuerte” del mundo, según el más reciente Índice de Pasaportes de Henley & Partners, que evalúa la cantidad de países a los que los ciudadanos pueden viajar sin necesidad de tramitar una visa._**

En los últimos años se ha notado una creciente dominación del ranking por parte de los países asiáticos desarrollados, con cada vez más influencia, seguidos de las naciones europeas. El número de países que admiten la entrada sin trámites previos, basado en datos de la Asociación Internacional de Transporte Aéreo (IATA), es visto como una señal de confianza.

Japón ya había liderado el ranking en solitario, pero ahora comparte la cima con Singapur, como también ocurrió en 2020. En la actualidad, ambas naciones asiáticas tienen permitido viajar a 192 destinos.

El segundo lugar es compartido por Alemania y Corea del Sur, con acceso libre a 190 naciones. Y en tercer lugar aparecen Finlandia, Italia, Luxemburgo y España, con 189 accesos.

El primer país latinoamericano en el ranking es Chile, en el puesto 16, con acceso a 174 países, los mismos que Mónaco y Rumania. Y luego están Argentina, en el puesto 19 con 170 destinos, y Brasil en el puesto 18, con 169 naciones para viajar.

Christian H. Kaelin, presidente de Henley & Partners y creador del índice, afirma que la apertura de los canales de migración será crucial para la recuperación tras la pandemia. “Los pasaportes y visados son uno de los instrumentos más importantes que influyen en la desigualdad social en todo el mundo, ya que determinan las oportunidades de movilidad global”, declaró, citado por la cadena CNN.

> “Las fronteras dentro de las cuales nacemos, y los documentos que tenemos derecho a poseer, no son menos arbitrarios que el color de nuestra piel. Los estados más ricos deben fomentar la inmigración positiva en un esfuerzo por ayudar a redistribuir y reequilibrar los recursos humanos y materiales en todo el mundo”, aseveró Christian H. Kaelin.

##### Los mejores pasaportes para tener en 2022 son:

1\. Japón, Singapur (192 destinos)

2\. Alemania, Corea del Sur (190)

3\. Finlandia, Italia, Luxemburgo, España (189)

4\. Austria, Dinamarca, Francia, Países Bajos, Suecia (188)

5\. Irlanda, Portugal (187)

6\. Bélgica, Nueva Zelanda, Noruega, Suiza, Reino Unido, Estados Unidos (186)

7\. Australia, Canadá, República Checa, Grecia, Malta (185)

8\. Polonia, Hungría (183)

9\. Lituania, Eslovaquia (182)

10\. Estonia, Letonia, Eslovenia (181)

##### Los peores pasaportes para tener en 2022:

104\. Corea del Norte (39 destinos)

105\. Nepal y territorios palestinos (37)

106\. Somalia (34)

107\. Yemen (33)

108\. Pakistán (31)

109\. Siria (29)

110\. Irak (28)

111\. Afganistán (26)

##### Los países latinoamericanos en el ranking:

16\. Chile (174 accesos)

19\. Argentina (170)

20\. Brasil (169)

24\. México (159)

27\. Uruguay (153)

30\. Costa Rica (150)

34\. Panamá (142)

35\. Paraguay (141)

36\. Perú (135)

37\. El Salvador (134)

38\. Guatemala y Honduras (133)

39\. Colombia (131)

40\. Venezuela (129)

41\. Nicaragua (127)

56\. Ecuador (91)

65\. Bolivia (79)

79\. Cuba (64)