+++
author = ""
date = 2021-10-05T03:00:00Z
description = ""
image = "/images/larreta-jorge-macri-g20210715-1204874-1.jpg"
image_webp = "/images/larreta-jorge-macri-g20210715-1204874.jpg"
title = "JORGE MACRI DESCARTA PASARSE A CABA Y REAFIRMA SU DESEO DE DISPUTAR LA PROVINCIA"

+++

##### **_El intendente ratificó su deseo de competir contra Kicillof y dio de baja a las especulaciones sobre su supuesto pase a Ciudad de Buenos Aires._**

Tras varios días signados por especulaciones sobre su porvenir político, Jorge Macri reafirmó sus intenciones de mantener el foco puesto en Provincia de Buenos Aires. Ante los trascendidos de su posible “mudanza” a CABA para aspirar a la jefatura de Gobierno, el intendente de Vicente López ratificó su intención de dar la pelea por el territorio bonaerense.

“Es un sueño ser gobernador de la provincia de Buenos Aires”, fue la definición del jefe Comunal de Vicente López, en el marco de una entrevista publicada por el portal Perfil, donde uno de los ejes fueron las proyecciones a futuro dentro de la coalición opositora.

Una de las versiones que circulaban en la plana política eran los rumores de una presunta invitación por parte de Horacio Rodríguez Larreta para sumarse al armado de Ciudad Autónoma de Buenos Aires, de cara a una eventual candidatura como jefe de Gabinete. Esa versión coincidiría con el esquema de “trueques” entre figuras de GBA y CABA, tal como quedó sellado entre Diego Santilli y María Eugenia Vidal.

Al ser consultado por este punto, Macri despejó las dudas y descartó este escenario. “No hay una propuesta de incorporarse al gabinete porteño. Sí hemos hablado mucho con Horacio, en el último tiempo”, dijo el intendente bonaerense y agregó: “Lo que estamos hablando es que deberíamos empezar a trabajar mucho más en equipo sobre la problemática de la Ciudad y del Conurbano. No hay más que eso. No ha habido una propuesta de incorporarme y la verdad es que hablamos de profundizar la gestión”.

En tanto, Macri volvió a ratificar su deseo de aspirar a la gobernación de la provincia de Buenos Aires, intención que fue motivo de disputas con las figuras del PRO que pusieron su mirada en el territorio bonaerense cuando comenzaron a discutirse las listas para las PASO. Durante un tiempo prudencial, el intendente de Vicente López intentó marcar terreno y dejar en claro que él debería ser el representante “natural” del PRO en tierra gobernada por Axel Kicillof.

“Siempre es un deseo que yo expreso. Tengo demasiados años de dar vueltas por la provincia de Buenos Aires, son 17 años, desde que fundamos el PRO el 4 de septiembre de 2004. Así que conozco esta provincia y creo, a diferencia de muchos, que se puede mejorar, que es administrable si uno la entiende y si no se queda solamente en la descripción de los problemas”, definió el mandatario comunal.