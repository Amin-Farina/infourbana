+++
author = ""
date = 2022-04-11T03:00:00Z
description = ""
image = "/images/putin-8-1160x773.jpeg"
image_webp = "/images/putin-8-1160x773.jpeg"
title = "LA INVASIÓN DE PUTIN A UCRANIA INICIÓ UNA CRISIS ALIMENTARIA MUNDIAL"

+++
#### Además de las millones de víctimas ucranianas, la ofensiva rusa desatará una guerra de hambre en todo el mundo

Fosas comunes, cadáveres en sótanos, ciudades enteras destruidas… La invasión rusa a Ucrania conmociona al mundo. Según la agencia para los refugiados de la ONU, son más de 4,4 millones los refugiados ucranianos. La cifra se duplica cuando se habla de desplazados. Sin embargo, como Ucrania y Rusia son grandes exportadores de alimentos, las víctimas de esta invasión rusa serán indefectiblemente muchas más y se encuentran lejos de las fronteras ucranianas.

Mientras las granjas ucranianas se han convertido en campos de batalla, la incertidumbre en torno a las exportaciones agrícolas del país, así como las de Rusia, ha creado una emergencia alimentaria mundial al hacer subir los precios del trigo, el maíz, la soja, los fertilizantes y el aceite de girasol.

En un informe especial para _The New York Times_, Sara Menker -fundadora de Gro Intelligence, una empresa de inteligencia artificial que pronostica los mercados agrícolas mundiales y los efectos del cambio climático- y Rajiv Shah - presidente de la Fundación Rockefeller y antiguo administrador de la Agencia de los Estados Unidos para el Desarrollo Internacional- explican las consecuencias catastróficas de la ofensiva iniciada por Putin el 24 de febrero pasado.

Detallan que si bien los precios de los productos básicos como el trigo y el maíz son globales, sus impactos son desiguales. Los países y las personas más ricas pueden absorber las fuertes subidas de precios pero a los habitantes de los países más pobres, como Sudán y Afganistán, les resulta mucho más caro comer. “En Sudán, el aumento de los precios del trigo ha hecho que el precio del pan se duplique aproximadamente. Como Ucrania y Rusia exportaban piensos y fertilizantes antes de la guerra, el coste y la dificultad de producir alimentos aumentarán en los próximos meses y años”, vaticinan.

Además de Sudán y Afganistán, a Egipto le espera un año difícil. El país es el mayor importador mundial de trigo, que es un 33% más caro que a finales del año pasado.

“Por desgracia -advierten- muchos de estos países se enfrentan a otras crisis. Las redes de seguridad social se han desgastado por el Covid-19. Los precios del petróleo siguen siendo altos. Y más de la mitad de los países de bajos ingresos se encuentran en dificultades de endeudamiento o corren un alto riesgo de padecerlas a medida que aumentan los tipos de interés, lo que limita su capacidad de pedir préstamos para pagar los alimentos”.

Explican que antes de la invasión rusa, unos 811 millones de personas en todo el mundo no tenían suficiente para comer. “Esa cifra podría aumentar enormemente en esta temporada de hambre, el tiempo entre la siembra de primavera y la cosecha de otoño, cuando los alimentos suelen agotarse”, se lamentan.

Además, advierten que el cambio climático agravará estos riesgos ya que en las regiones agrícolas clave, las condiciones de sequía son las peores en décadas.

“Las múltiples implicaciones de la guerra son angustiosas. Las crisis alimentarias suelen provocar disturbios sociales, conflictos, gobiernos fallidos y migraciones masivas. Por ejemplo, algunos investigadores señalan el aumento de los precios de los alimentos como motor de las revueltas de la Primavera Árabe en 2011″, citan como ejemplo claro de la pesadilla que Putin desató para el mundo.

Menker y Shah afirman, sin embargo, que el mundo puede organizar una respuesta integral al hambre. Aseguran que las crisis de los precios de los alimentos de 2008 puede ser el antecedente clave para tener un plan de contingencia responsable.

“En primer lugar, los países y las instituciones deben actuar rápidamente para salvar vidas. Eso empieza por financiar plenamente el Programa Mundial de Alimentos y aprovechar las reservas de alimentos existentes para ayudar a los países en apuros. Las Naciones Unidas, la Organización Mundial del Comercio y otros organismos también deben trabajar con los países para evitar las prohibiciones de exportación de alimentos, que ya están socavando el suministro mundial de alimentos”, proponen.

En segundo lugar, agregan, el Grupo de los 7 y China deben liderar una nueva ronda de alivio de emergencia de la deuda oficial para que los países vulnerables puedan responder al hambre: “El alivio de la deuda fue una bendición para el desarrollo a principios de la década de 2000 y podría liberar recursos en la actualidad”. Y afirman que las instituciones financieras multilaterales también deben tomar medidas agresivas, utilizando instrumentos de emergencia como la reasignación de los derechos especiales de giro del Fondo Monetario Internacional, que pueden aumentar las reservas oficiales de divisas de los países.

“En tercer lugar, a largo plazo, el mundo debe ayudar a que las economías vulnerables tengan más seguridad alimentaria. La iniciativa Feed the Future del gobierno de Estados Unidos, creada en 2010 con apoyo bipartidista, ha ayudado a transformar la agricultura en África y en otros lugares”, explican.

Además, aseguran que el mundo necesita nuevas inversiones en la transformación de sistemas alimentarios especialmente en la agricultura regenerativa, para que las naciones sean más resistentes a las crisis energéticas, climáticas, sanitarias y geopolíticas.