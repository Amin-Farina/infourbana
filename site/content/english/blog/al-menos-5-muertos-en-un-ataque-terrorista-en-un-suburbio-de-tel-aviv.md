+++
author = ""
date = 2022-03-29T03:00:00Z
description = ""
image = "/images/ataque-tel-aviv.webp"
image_webp = "/images/ataque-tel-aviv.webp"
title = "AL MENOS 5 MUERTOS EN UN ATAQUE TERRORISTA EN UN SUBURBIO DE TEL AVIV"

+++
#### El tercer atentado en menos de una semana en Israel. El primer ministro israelí instruyó ampliar la cantidad de soldados y reservistas que portan armas, aumentar el seguimiento de discursos en redes sociales para identificar potenciales atacantes, entre otras medidas

Cinco personas murieron en ataques armados el martes cerca de la ciudad costera israelí de Tel Aviv, dijeron los servicios de emergencia.

“Lamentablemente, tenemos que señalar que cinco personas han muerto”, dijo Eli Bin, jefe de los servicios de emergencia de Magen David Adom, después de haber calculado en dos el número de muertos por tiroteos en dos lugares de la ciudad ultraortodoxa de Bnei Brak.

El atacante fue abatido por la policía, mientras que un sospechoso fue arrestado. Las autoridades aún desconocen los móviles del atentado.

Los tiroteos ocurrieron en dos lugares de Bnei Brak, una ciudad ultraortodoxa al este de Tel Aviv. La policía dijo en un comunicado que una investigación preliminar encontró que el hombre armado estaba armado con un rifle de asalto y abrió fuego contra los transeúntes antes de que los oficiales le dispararan en la escena.

Si bien las circunstancias no están claras, el tiroteo es el último de una serie de ataques de terroristas árabes antes del mes sagrado musulmán del Ramadán. Los medios israelíes dijeron que el atacante era un palestino de Cisjordania.

Con este es el tercer ataque terrorista en menos de una semana en Israel. El domingo, dos personas murieron y seis, entre ellos dos agentes de la Policía de Israel, resultaron heridos en un ataque a tiros en Hadera por la noche, confirmó el alcalde Zvi Gendelman.

Los tiradores fueron abatidos por las fuerzas de seguridad que se encontraban en el lugar del ataque. Al parecer, los atacantes esperaron a que pasara un autobús para disparar contra él.

La semana pasada, un simpatizante del Estado Islámico convicto mató a cuatro israelíes en una ola de apuñalamientos y embestidas de automóviles en la ciudad sureña de Beersheba.

Los dos ataques anteriores, llevados a cabo por ciudadanos árabes de Israel inspirados por el grupo terrorista Estado Islámico, han generado preocupación por una mayor violencia

El primer ministro israelí, Naftali Bennett, anunció el lunes una serie de nuevas medidas de seguridad tras el ataque en la ciudad de Hadera.

Benet instruyó ampliar la cantidad de soldados y reservistas que portan armas, aumentar el seguimiento de discursos en redes sociales para identificar potenciales atacantes y utilizar recursos judiciales, económicos, digitales y de inteligencia para evitar incidentes adicionales y para detener a quienes los instigan o ayudan a perpetrarlos, según un comunicado de su oficina.

Entre las nuevas medidas, se destaca la orden de implementar el régimen de detenciones administrativas. Además, el primer ministro instruyó reforzar las distintas fuerzas de seguridad, sobre todo en los puntos más calientes y al menos hasta la celebración del Día de la Independencia a comienzos del mes de mayo.

“El primer ministro Bennett enfatizó que esta era una nueva situación que requiere que el estamento de seguridad se prepare y se adapte a las circunstancias en las que los segmentos extremistas del sector árabe, guiados por una ideología islamista extrema, se dedican al terrorismo y dañan vidas humanas”, señaló un portavoz del mandatario.

Estas medidas fueron anunciadas tras una reunión entre Benet y líderes del Ejército, la Policía, los servicios de inteligencia y los ministerios de Defensa y Seguridad Interior, que se realizó de manera virtual dado que el primer ministro fue confirmado esta mañana como positivo de covid-19.

Se espera que el Ramadán comience el sábado. El año pasado, los enfrentamientos entre la policía israelí y los manifestantes musulmanes durante el mes sagrado se convirtieron en una guerra de 11 días entre Israel y Hamas, el grupo terrorista islámico que gobierna Gaza. Hamas elogió el tiroteo como una “operación heroica”.

Israel ha estado tomando medidas para calmar las tensiones con los palestinos este año para evitar que se repita la violencia. Los ataques mortales de ISIS dentro de Israel y los ataques de ciudadanos árabes de Israel son poco comunes.

El grupo opera principalmente en Irak y Siria, donde recientemente ha intensificado los ataques contra las fuerzas de seguridad. Ya no controla ningún territorio sino que opera a través de células durmientes. IS ha reclamado ataques contra tropas israelíes en el pasado y tiene sucursales en Afganistán y otros países.