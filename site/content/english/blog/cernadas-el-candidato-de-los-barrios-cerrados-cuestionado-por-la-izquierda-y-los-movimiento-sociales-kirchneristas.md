+++
author = ""
date = 2021-08-09T03:00:00Z
description = ""
image = "/images/img_3330_1.jpg"
image_webp = "/images/img_3330_1.jpg"
title = "CERNADAS \"EL CANDIDATO DE LOS BARRIOS CERRADOS\" CUESTIONADO POR LA IZQUIERDA Y LOS MOVIMIENTO SOCIALES KIRCHNERISTAS"

+++

**_Estas últimas semanas, el candidato de Juntos en Tigre, Segundo Cernadas, estuvo recibiendo críticas por parte de los votantes del oficialismo, debido a sus reiteradas "visitas a los barrios cerrados del distrito en lugar de recorrer las otras localidades de Tigre."_**

En sus publicaciones de redes sociales, el dirigente opositor recibe tantos mensajes de apoyo como críticas por su disociación entre su escaso apoyo a los sectores más vulnerables del distrito y sus fuertes vínculos con las clases más acomodadas del municipio. 

"_El actor, representante de quienes viven en los countries con “necesidades” opuestas a las del pueblo trabajador._" destacaron fuentes cercanas a movimientos populares y de los trabajadores en Tigre.

Parecería ser que el actor no logra comprender qué es lo que sucede del otro lado del paredón: "grandes barrios populares con amplitud de carencias, sectores de trabajadores que se organizan y salen a pelear y jóvenes que son reprimidos por la policía, pero que no logran visibilidad en los grandes medios de comunicación". afirmaron desde la izquierda diario.