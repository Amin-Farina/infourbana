+++
author = ""
date = 2021-10-25T03:00:00Z
description = ""
image = "/images/el-ministro-de-seguridad-anibal___ertmkvknx_340x340__1.jpg"
image_webp = "/images/el-ministro-de-seguridad-anibal___ertmkvknx_340x340__1.jpg"
title = "SIGUEN LAS TENSIONES ENTRE ANÍBAL FERNÁNDEZ Y SERGIO BERNI"

+++

#### **_El ministro de Seguridad dijo que si tiene necesidad “hablará” con el ministro de Seguridad bonaerense, con el que está enfrentado por su postura sobre el conflicto mapuche en la Patagonia._**

El ministro de Seguridad Aníbal Fernández redobló las críticas a su par de la provincia de Buenos Aires Sergio Berni, del que consideró que “no se comprende bien ni de dónde viene ni adónde quiere ir”. Aclaró que si tiene necesidad “hablará” con él “y si no, no”. Los funcionarios están enfrentados por sus posturas en relación al conflicto mapuche en el Sur.

Después de las cuestionamientos iniciales Aníbal Fernández trató de bajarle el tono a la polémica con el funcionario bonaerense, que le respondió con ironía a sus declaraciones de que ni el presidente Alberto Fernández ni él necesitaban ser “aprobados” por Berni. El ministro de Seguridad bonaerense lo cruzó en Twitter: “Como de costumbre siempre tiene razón querido compañero, ni usted ni el Presidente necesitan de mi aprobación, no es mi intención contradecir tan brillante acto de soberbia”.

> Berni además chicaneó al ministro de Seguridad con la derrota del Gobierno en las elecciones primarias (PASO): “Lamento informarle lo obvio, sería necesario contar con la aprobación y el consenso de la sociedad en su conjunto. Si mis matemáticas no me fallan el 12 de Septiembre hubo 16.323.291 argentinos que no aprobaron nuestra gestión. Fui claro? O le hago un dibujito?”, con mención a la amenaza del funcionario al dibujante Nik.

Aníbal Fernández: “Yo tengo responsabilidad y los demás miran desde la tribuna”

> El ministro de Seguridad dijo en respuesta los cruces de Berni que tiene “responsabilidad” y que “los demás miran desde la tribuna”: “Desde ahí todo es fácil, le dicen a Lionel Messi cómo tiene que patear el tiro libre”. Dijo que va a seguir “hablando de las cosas que suceden” como de los ataques de mapuches en Río Negro: “Tengo jurisdicción sobre el área que estoy hablando y responsabilidades al respecto”.

Aníbal Fernández explicó que en los puntos del conflicto mapuche intervienen el escuadrón 34 y 35 de Gendarmería, además de los destacamentos de la Policía Federal, Prefectura y Policía de Seguridad Aeroportuaria (PSA). Dijo que les importa el conflicto en el Sur y que están “colaborando en la investigación”, en declaraciones a El Destape radio y a la prensa en la sede del Ministerio de Seguridad.

Para Aníbal Fernández los ataques mapuches en el Sur “no son terrorismo”

El ministro de Seguridad ratificó su posición de que para él los ataques mapuches en la Patagonia “no son terrorismo”, a diferencia de lo que plantean Berni y la gobernadora de Río Negro Arabela Carreras que hizo una denuncia por este delito. El funcionario pidió “hablar en serio”, “no banalizar” términos como terrorismo y habló de los ataques en Río Negro como “delitos comunes que tienen que tratarse con la policía de la provincia y la justicia de la provincia”.