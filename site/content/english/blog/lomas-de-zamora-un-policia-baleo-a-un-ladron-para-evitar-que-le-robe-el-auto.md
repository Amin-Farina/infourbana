+++
author = ""
date = 2021-06-28T03:00:00Z
description = ""
image = "/images/baleadojpg-1.jpg"
image_webp = "/images/baleadojpg.jpg"
title = "LOMAS DE ZAMORA: UN POLICÍA BALEÓ A UN LADRÓN PARA EVITAR QUE LE ROBE EL AUTO"

+++

**_Un efectivo de la Policía Federal Argentina (PFA) baleó a un delincuente armado que intentó asaltarlo cuando bajaba de su camioneta, mientras que un cómplice se dio a la fuga, en la localidad bonaerense de Ingeniero Budge, Lomas de Zamora._**

El hecho ocurrió ayer poco después de las 18 cuando la víctima del robo, un agente de 32 años que estaba de franco de servicio y vestido de civil, llegaba a su casa. “Estaba bajando las cosas del auto y lo abordaron”, contó a TN Daniel, un vecino. Toda la secuencia fue registrada por la cámara de seguridad de una vivienda vecina, donde se puede observar a uno de los asaltantes descender del vehículo y amenazar al policía a punta de pistola.

“Me imagino que no sabían que era policía”, arriesgó sobre la audacia de los delincuentes que, incluso después de que el efectivo se identificara como tal, forcejeó con él para concretar el robo. Pero entonces la víctima desenfundó una Bersa Thunder y disparó al menos tres veces contra el ladrón.

“El delincuente también disparó contra el policía y en la huida volvió a disparar”, relató el vecino. Pero en el fuego cruzado, uno de los balazos que había realizado el efectivo lo alcanzó en la zona abdominal y se desplomó a los pocos metros sobre el asfalto. Más tarde, cerca de su cuerpo, los peritos encontraron un revólver calibre 32 que fue incautado para analizar.

El delincuente herido fue identificado después como Matías López, de 32 años, quien fue trasladado al Hospital Gandulfo de Lomas de Zamora para recibir las primeras curaciones. En tanto, su cómplice se dio a la fuga en un vehículo que tenía un pedido de captura por robo.

El caso es investigado por la Unidad Funcional de Instrucción (UFI) 5 del Departamento Judicial de Lomas de Zamora.

Balean a un expolicía durante un intento de robo en La Matanza

Un policía retirado de la Ciudad de 49 años resultó herido de dos balazos al ser asaltado por cuatro delincuentes que intentaron robarle en la localidad de San Justo, partido de La Matanza.

El hecho ocurrió ayer por la tarde, en el cruce de las calles Lynch y Balbastro de la mencionada localidad del sudoeste del conurbano bonaerense, donde la víctima fue interceptada por cuatro delincuentes, a bordo de una camioneta marca Peugeot Partner de color azul, con intenciones de sustraerle el vehículo.

Ante esa situación, el expolicía, identificado como Víctor Hugo Gómez, se resistió al asalto y se produjo un enfrentamiento armado en el que sufrió dos heridas de bala, mientras que los delincuentes se dieron a la fuga.