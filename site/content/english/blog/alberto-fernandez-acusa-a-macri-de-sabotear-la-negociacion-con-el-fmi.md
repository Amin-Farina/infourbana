+++
author = ""
date = ""
description = ""
image = "/images/mauricio-macri-alberto-fernandez-y___ve6sphifn_1200x630__1.jpg"
image_webp = "/images/mauricio-macri-alberto-fernandez-y___ve6sphifn_1200x630__1.jpg"
title = "ALBERTO FERNÁNDEZ ACUSA A MACRI DE SABOTEAR LA NEGOCIACIÓN CON EL FMI"

+++

##### **_El Presidente acusa a Mauricio Macri de sabotear la negociación que su gobierno encaró con ese organismo internacional por la deuda de US$44.904 millones._**

Alberto Fernández sostiene que Mauricio Macri está haciendo todo lo posible para que la Argentina no puede llegar a un acuerdo con el FMI y complicar, de esa manera, los últimos dos años de la gestión del Frente de Todos.

“Es lo mismo que hizo (Domingo) Cavallo con (Raúl) Alfonsín cuando pidió que no le presten plata al Gobierno radical”, dice en la intimidad Alberto Fernández, en referencia a los años en los que el exministro de Economía de Carlos Menem le pidió al mercado internacional que corte lazos con el gobierno radical con la promesa de que se relanzaría un nuevo vínculo con un eventual gobierno justicialista.

La reunión de Alberto Fernández con Kristalina Georgieva

El 30 de octubre pasado, Alberto Fernández se reunió con Kristalina Georgieva para intentar acercar posiciones en la renegociación de la deuda que el país mantiene con ese organismo internacional. El encuentro se postergó por más de una hora y media y se llevó a cabo en la Embajada de Roma.

La Casa Rosada busca avanzar en la definición de un acuerdo que permita extender los plazos para evitar pagar 19 mil millones de dólares en 2022, otros 18 mil millones en 2023 y cerca de 5.000 millones en 2024.

El vocero del FMI Gerry Rice afirmó que el Gobierno argentino sigue trabajando con el organismo en un acuerdo de facilidades extendidas, cuyo plazo máximo son 10 años aunque no dio precisiones sobre la fecha del acuerdo. De acuerdo con los datos publicados en la web del FMI, en diciembre la Argentina tendrá que abonar una cuota de capital por 1874 millones de dólares.

Por su parte el expresidente Mauricio Macri contraataca y asegura que el mundo no le cree a Alberto Fernández y que el jefe de Estado lanza esta fuerte acusación para victimizarse ante la falta de acuerdo con el FMI.

Por el momento no hay una fecha concreta para llegar a un acuerdo con el FMI y el propio ministro de Economía Martín Guzmán asegura que “se trabaja en los distintos aspectos”.