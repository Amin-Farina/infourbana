+++
author = ""
date = 2021-06-30T03:00:00Z
description = ""
image = "/images/el-papa-francisco-diserto-un-foro-empresarios-argentinos-1.jpeg"
image_webp = "/images/el-papa-francisco-diserto-un-foro-empresarios-argentinos.jpeg"
title = "FRANCISCO LLAMÓ A LOS EMPRESARIOS ARGENTINOS A \"NO ESCONDER LA PLATA EN PARAÍSOS FISCALES\""

+++

**_El papa Francisco pidió a los empresarios argentinos "invertir" y "no esconder la plata en los paraísos fiscales", al tiempo que los convocó a elegir "el camino de la economía social"._**

"Invertir en el bien común, no esconder la plata en los paraísos fiscales. Invertir", pidió el pontífice en un videomensaje que fue difundido este miércoles a la mañana durante la apertura del XXIV Encuentro Anual de la Asociación Cristiana de Dirigentes de Empresa (Acde).

El encuentro se desarrolla entre el miércoles y el jueves en forma virtual, bajo el lema "Hacia un capitalismo más humano", con la participación de referentes del mundo empresarial y la política argentina, y en el que se conmemora además el centenario del nacimiento del empresario argentino en proceso de beatificación, Enrique Shaw.

"La inversión es dar vida, es crear, es creativa. Saber invertir, no esconder. Uno esconde cuando no tiene la conciencia limpia o cuando está rabioso", expuso Jorge Bergoglio en su mensaje de apertura del encuentro.

"Todos sabemos lo que se dice en el campo cuando la vaca no da la leche. ¿Qué le habrá pasado a la vaca, porque está enojada, qué 'esconde' la leche?. Cuando escondemos es porque algo está funcionando mal", planteó luego el Papa en su discurso ante los empresarios argentinos.

Así, Bergoglio animó a los participantes a actuar con "claridad, transparencia y producción" e insistió: "Invertir. E ir creando la confianza social. Es muy difícil construir sin confianza social".

"A veces esos grandes acuerdos de grandes empresas o grandes inversores o grande gente, están todos alrededor de la mesa, después de horas hacen el acuerdo, lo firman, y en el momento que están brindando por el acuerdo éste que está del lado de la mesa con aquél que está allá hacen uno por debajo de la mesa. No, con confianza y nunca traicionar la confianza", convocó el Papa.

Durante su mensaje, aseveró además que "la mirada cristiana de la economía y de la sociedad que es distinta de la mirada pagana o de la mirada ideológica, es cristiana y nace del mensaje de Jesús, de las bienaventuranzas, de Mateo 25, de ahí nace la mirada".

"Y la construcción de una comunidad justa, económicamente y socialmente para todos, la tienen que hacer todos: sindicalistas y empresarios, trabajadores y dirigentes. Tenemos que ir por el camino de la economía social", animó a los participantes.

"Seamos realistas, la economía últimamente, en los últimos decenios, engendró las finanzas y las finanzas tienen el riesgo de terminar como la cadena de San Antonio, ¿no?, que creemos que hay mucho y al final no hay nada", les dijo.

En ese marco, reclamó "volver a la economía de lo concreto, no perder lo concreto. Y lo concreto es la producción, el trabajo de todos, que no haya falta de trabajo, las familias, la patria, la sociedad. Lo concreto".

"En una sociedad donde haya un margen de pobreza muy grande, uno se tiene que preguntar cómo va la economía, si es justa, si es social o simplemente busca intereses personales. La economía es social", sostuvo el pontífice.

De cara a los empresarios, Francisco resaltó además que, "para generar empleo, es importante el poder de las pymes porque de abajo viene la creatividad siempre".

"Por lo tanto, ir hacia el bien común, con el gesto de crear empleo. Es un desafío, el encuentro de ustedes es un desafío a la creatividad. Crear empleo, donde hay un momento, la pandemia nos llevó a esto donde falta", finalizó.

En cuanto a los paraísos fiscales, a los que hizo referencia el Papa en su mensaje de este miércoles, ya en junio de 2020, por medio de un decreto, el Vaticano dispuso una nueva legislación de compras con eje en "transparencia, centralización y competencia", por la que dejará de comprar "bienes, servicios, obras y trabajos" a empresas y personas que tengan su sede en paraísos fiscales o que tengan accionistas residentes en esos Estados.

Tras su mensaje, expuso también en el foro de ACDE el Secretario Adjunto del Dicasterio para el Desarrollo Humano Integral del Vaticano, Augusto Zampini, quien sostuvo en su mensaje que "la política no puede estar dominada por la economía, pero tiene que haber una buena relación".

"Hace falta más diálogo, para que la economía se coloque al servicio de la vida humana. La buena economía está hecha por buenos empresarios, que son los que invierten en una economía distinta", sostuvo.

"Vivimos un momento difícil producto de un modelo de desarrollo que genera inequidad, que se ha exacerbado con el Covid. Pero estamos ante una oportunidad de soñar en grande y construir una nueva realidad más próspera", agregó en su disertación.

Durante el fin de semana, en una entrevista con Télam, el presidente de la Conferencia Episcopal Argentina (CEA), monseñor Oscar Ojea, cuestionó las "serias descalificaciones" hacia la figura del Papa que se registraron en los últimos días entre ciertos sectores de la política y los medios de comunicación, criticó la manera "banal" en que se lo juzga y recomendó a la sociedad argentina leer sus mensajes en forma completa y "no sacarlo de contexto".

De esta manera, el jefe de la Iglesia católica argentina salió en defensa de Jorge Bergoglio, tras las declaraciones del Papa argentino sobre la función social de la propiedad privada ante la Organización Internacional del Trabajo (OIT), en sintonía con la Doctrina Social de la Iglesia, pero que en Argentina despertó disparatados cuestionamientos de ciertos sectores políticos y de los medios de comunicación.

"Escuchamos serias descalificaciones a lo que dice el Papa y, a través de algunos comunicadores, de un modo un poco banal. El Papa es muy provocador, provoca la reflexión sobre el rumbo del planeta y la humanidad. Sería absurdo no pensar en eso, en medio de una pandemia que es consecuencia de algo que se ha desmadrado en el trato con la naturaleza y en nuestro trato social", subrayó Ojea.

Aquellas declaraciones en cuestión fueron pronunciadas por el Papa el pasado 17 de junio, en un videomensaje en español de 27 minutos a los participantes de la 109 conferencia de la OIT, que se desarrolló en forma virtual por la pandemia.

En ese discurso, Francisco pidió "garantizar que la asistencia social llegue a la economía informal" golpeada por los efectos del coronavirus, al tiempo que lamentó el aumento de la pobreza y el desempleo por falta de medidas a nivel mundial durante la pandemia.