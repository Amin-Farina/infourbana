+++
author = ""
date = 2021-11-11T03:00:00Z
description = ""
image = "/images/familia-tipo.jpg"
image_webp = "/images/familia-tipo.jpg"
title = "¿CUÁNTO HAY QUE GANAR PARA SER DE CLASE MEDIA EN LA CIUDAD DE BUENOS AIRES?"

+++

##### **_Una familia porteña tipo necesitó ingresos de al menos $112.935 en octubre para ser considerada de clase media según los criterios de estratificación social que utiliza la Ciudad Autónoma de Buenos Aires. Ese umbral, de unos $3.643 por día, subió 3,92% respecto al mes de septiembre, en línea con el movimiento de la inflación en ese mes. En términos interanuales, los ingresos necesarios para alcanzar ese nivel socioeconómico aumentaron casi 48,08% en comparación con mismo mes del año pasado._**

La Dirección General de Estadísticas y Censos de la Ciudad Autónoma de Buenos Aires publicó los datos de Costo de vida, Líneas de pobreza y Canastas de Consumo para la Ciudad de Buenos Aires del mes de septiembre de 2021. El reporte, entre otros datos, fija los criterios actualizados que toma el distrito para delimitar los niveles socioeconómicos en base al nivel ingreso.

Así, una familia de cuatro integrantes necesitó disponer de entre $108.674,24 y $347.757,55 durante el mes pasado para entrar en “sector medio”.

##### **¿Cuál es el ingreso mensual necesario para pertenecer a cada estrato?**

###### _Datos de octubre_

_Intervalo de ingreso total familiar teórico por estrato para un hogar compuesto por una pareja mujer y varón de 35 años de edad, ambos económicamente activos y propietarios de la vivienda, con dos hijos varones de 9 y 6 años_

El reporte mensual que elabora establece una estratificación social en base a cinco niveles socioeconómicos.

* En situación de indigencia: Hogares cuyo ingreso total mensual no alcanza para cubrir la Canasta Básica Alimentaria (CBA - Línea de indigencia). A octubre, se consideraba indigentes a las familias que no alcanzaron los $38.306,13 mensuales.
* En situación de pobreza no indigente: Hogares cuyo ingreso total mensual no alcanza para cubrir la Canasta Básica Total (CBT – Línea de pobreza) pero permite al menos adquirir la CBA. El rango de ingresos para este estrato se fijó entre $38.306,14 y $72.600,68 por mes.
* No pobres vulnerables: Hogares cuyo ingreso total mensual es de al menos la CBT y no alcanza la Canasta Total (CT) del Sistema de Canastas de Consumo. Ganan entre $72.600,69 y $90.348,18 al mes.
* Sector medio frágil: Hogares cuyo ingreso total mensual es de al menos la CT y no alcanza 1,25 veces la CT del Sistema de Canastas de Consumo. Tienen ingresos mensuales entre $90.348,19 y $112.935,23 pesos.
* Sector medio “clase media”: Hogares cuyo ingreso total mensual es de al menos 1,25 veces la CT y no alcanza 4 veces la CT del Sistema de Canastas de Consumo. Ganan desde $112.935,24 y $361.392,75 por mes.
* Sector acomodado: Hogares cuyo ingreso mensual es de 4 veces o más la CT del Sistema de Canastas de Consumo. Son los ingresos de $361.392,76 al mes o más.

###### **Ingresos diarios necesarios según estrato económico**

**_Datos a octubre_**

Intervalo de ingreso total familiar teórico por estrato para un hogar compuesto por una pareja mujer y varón de 35 años de edad, ambos económicamente activos y propietarios de la vivienda, con dos hijos varones de 9 y 6 años.

“Esta estratificación brinda, por un lado, la posibilidad de identificar a los sectores más desprotegidos de la sociedad en términos de situaciones de indigencia y de pobreza y, por el otro, da cuenta de la heterogeneidad de los sectores no pobres de manera de facilitar un análisis más integral de la situación social de la Ciudad de Buenos Aires, la de cada uno de los estratos definidos y su evolución en el tiempo”, explica el informe.

En base a esos estratos, los ingresos teóricos necesarios de una pareja de clase media compuesta por una mujer y un varón de 35 años de edad, ambos económicamente activos y propietarios de la vivienda en la que viven, con dos hijos varones de 9 y 6 años, se dispararon en el último año aunque no alcanzaron el ritmo de avance del nivel general de inflación que marcha en el orden del 52% anual, según el dato de Indec a septiembre.

Visto en términos de necesidades diarias, esa misma familia tipo necesitó al menos $3.643,07 por día para alcanzar un nivel de consumo consecuente con el estrato medio.

Para no caer en la pobreza, mientras tanto, se necesitaron $72.600,68 al mes -un 47,49% más que hace un año-, mientras que quienes superan los $90.348,19 al mes pero no alcanzan los $112.935,23 el mes son considerados “sector medio frágil”.