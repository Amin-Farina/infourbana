+++
author = ""
date = 2021-12-29T03:00:00Z
description = ""
image = "/images/camara-diputadosjpeg-1.jpeg"
image_webp = "/images/camara-diputadosjpeg.jpeg"
title = "LA LEGISLATURA BONAERENSE APROBÓ LA HABILITACIÓN A LOS INTENDENTES A PRESENTARSE A LA REELECCIÓN EN 2023"

+++

#### **_Tras la aprobación en el Senado, la Cámara baja provincial convirtió en ley que el desbloqueo al tercer mandato consecutivo de los jefes comunales; una pelea que atravesó al Frente de Todos y Juntos por el Cambio_**

Con una rápida aprobación que sorteó las dos cámaras en una sola jornada, la Legislatura de la provincia de Buenos Aires flexibilizó ayer la ley que limitaba a dos los mandatos consecutivos permitidos a intendentes, legisladores, concejales y consejeros escolares bonaerenses. 

La modificación permite que el primero de los dos períodos a contabilizar sea el que comenzó en 2019 y no el iniciado en 2015, como indicaba la norma aprobada en 2016. El segundo mandato se contará como total más allá de ser cumplido parcialmente. La votación dividió a los bloques de Juntos y del Frente de Todos. Los legisladores que responden a María Eugenia Vidal y a Elisa Carrió, en la oposición, y a Sergio Massa, en el oficialismo, votaron en contra. Pero la mayoría de ambas bancadas apoyó las modificaciones.

En solo cinco horas, la Legislatura de la provincia de Buenos Aires aprobó ayer una modificación de la ley que limita los mandatos consecutivos permitidos a intendentes, legisladores, concejales y consejeros escolares bonaerenses. El cambio permite que el primero de los dos períodos a contabilizar sea el que comenzó en 2019 y no el iniciado en 2015, como indicaba la norma aprobada en 2016. También se modificó la reglamentación y se eliminó la posibilidad de aspirar a una nueva reelección si el segundo mandato se abandonaba antes de cumplir la mitad.

El Senado bonaerense dio el primer paso, al votar a favor de los cambios con 36 votos positivos, 8 negativos, una abstención y una ausencia. La votación dividió a los bloques de Juntos y del Frente de Todos. Entre los votos negativos, hubo dos de senadores vidalistas, cuatro massistas y dos de la Coalición Cívica. Pero la mayoría de ambas bancadas apoyó las modificaciones.

En el Senado comenzó el debate con la propuesta de los senadores de Juntos por el Cambio Juan Pablo Allan y Joaquín de la Torre, que propusieron cambiar el decreto reglamentario que permitió el atajo de las licencias a los intendentes, firmado por María Eugenia Vidal, Federico Salvai y el propio De la Torre en 2019. El segundo mandato se contará más allá de ser cumplido total o parcialmente.

“La ley ha demostrado tener en la práctica algunas deficiencias”, afirmó Allan en el debate en el recinto. “La reelección indefinida sigue vigente. Los dos mandatos ya no son ocho años, sino seis. La ley merece un cerrojo, para que la voluntad del legislador no encuentre un resquicio reglamentario”, agregó, y remarcó que los intendentes de Juntos que no habían dejado sus cargos por licencia estaban en desventaja con respecto a los del Frente de Todos.

En contra se expresaron dirigentes del mismo espacio político que Allan, como Andrés de Leo (Coalición Cívica) o los vidalistas Walter Lanaro u Owen Fernández. “Que veinte tipos hayan encontrado la forma de gambetear la ley no puede habilitar a otros 70 a la reelección”, afirmó Lanaro.

La senadora massista Sofía Vanelli marcó la postura contraria a los cambios del Frente Renovador. “No se está explicando por qué, en el artículo 4, se plantea la continuidad de dos mandatos, el de 2019 y el de 2021″, advirtió la legisladora.

Teresa García, jefa del bloque oficialista, justificó los cambios en la ley y desgranó críticas a Vidal. “De ningún modo voy a admitir que haya una línea divisoria entre quienes defienden a la gente y otros que nos meteríamos en un balde de mugre. Tampoco voy a admitir que se exalte la figura de la exgobernadora que cuando el pueblo de la provincia no quiso acompañarla alzó las velas hacia otro horizonte”, sostuvo.

Durante toda la jornada, la mirada estuvo puesta en la Cámara de Diputados, porque era la que tenía los números más ajustados para obtener los dos tercios necesarios para habilitar el tratamiento sobre tablas del proyecto, en sesiones extraordinarias.

En el Senado, el número era holgado, pero igualmente el inicio del debate se demoró mientras se negociaba en Diputados para reunir las voluntades. Se preveía para horas del mediodía, pero arrancó después de las 17.

En Diputados, alrededor de las 20 ya se daba comienzo a la sesión con el proyecto que modificaba la ley de reelecciones con la media sanción en el Senado consumada. Los dos tercios se reunieron con facilidad. Sobre un cuerpo de 92 diputados, hubo 83 presentes para dar inicio a la sesión y para habilitar el tratamiento se superó largamente los dos tercios. Se reunieron 68 votos, entre los que hubo algunos significativos que luego se abstuvieron en la votación en general, como el de Daniel Lipovetzky. También acompañaron diputados referenciados con Vidal como Fabián Perechodnik o Martiniano Molina al momento de habilitar el tratamiento.

El massismo se opuso, con Rubén Eslaiman como vocero, y también hizo lo propio el bloque de Avanza Libertad. La izquierda tampoco respaldó la iniciativa; sí lo hizo desde su monobloque el diputado Fabio Britos, hermano del intendente de Chivilcoy.

El radical Maximiliano Abad, jefe del bloque de Juntos por el Cambio y titular de la UCR bonaerense, propuso el ingreso del tema en la sesión y después justificó su voto favorable, como el de la mayoría de su bloque.

“Entendemos la importancia para la calidad democrática de la alternancia. Pero en la política uno debe hacer lo que corresponde, dejar de hablar para la tribuna y para que titulen los medios”, señaló Abad.

Abad reprochó “el atajo de pedir licencia” y marcó que muchos jefes comunales se fueron a cargos nacionales o provinciales para volver a presentarse, “pero en otros casos, como el del intendente de Merlo \[Gustavo Menéndez\], se toman licencia sin cargo; la trampa garantiza reelección indefinida”.

El último discurso fue el del jefe del bloque del Frente de Todos, César Valicenti. “No nos demos latigazos en la espalda”, afirmó. La Cámara llamó a cuarto intermedio y debía seguir la sesión al cierre de esta edición. Esperaba el presupuesto.