+++
author = ""
date = 2021-08-11T03:00:00Z
description = ""
image = "/images/diego-santilli-patricia-bullrich-y___djqfyrru-_1256x620__1.jpg"
image_webp = "/images/diego-santilli-patricia-bullrich-y___djqfyrru-_1256x620__1.jpg"
title = "LARRETA ENDURECIÓ SU DISCURSO Y SANTILLI SE MOSTRÓ CON BULLRICH"

+++

**_La campaña electoral está prácticamente en sus comienzos, pero ya hay candidatos recalculando su discurso y su estrategia para sumar más votos: la culpa es de las encuestas, esas reinas con aires de tirano que obsesionan a los políticos, cambian sus decisiones y condicionan sus pasos._**

En Juntos por el Cambio, por ejemplo, los últimos sondeos prendieron luces amarillas en el tablero de Horacio Rodríguez Larreta: en los focus group más recientes, la gente demandó más firmeza de los dirigentes opositores y precisamente a esas opiniones se atribuye en las filas opositoras el endurecimiento del discurso del jefe de Gobierno desde hace 48 horas, lapso en el que se mostró mucho más crítico del Gobierno que antes.

Incluso la invitación pública de Rodríguez Larreta y María Eugenia Vidal a Mauricio Macri para que se sume a la campaña porteña tiene origen, según admitieron en JxC, en el inesperado crecimiento en las encuestas de Ricardo López Murphy y Javier Milei, que están sacándole a la candidata de Juntos Podemos Más una parte de los votos del electorado más duro y antikirchnerista que prefería la candidatura de Patricia Bullrich.

La presencia del ex presidente al lado de Vidal, cuya figura se sigue asociando con la moderación, podría evitar que el núcleo más intransigente de los votantes de Juntos por el Cambio porteños elija otras opciones en las PASO de la Ciudad de Buenos Aires. Al menos, esa es la apuesta del comando larretista, que estaba resignado a que la ex gobernadora obtuviera en las primarias un 40% de los votos, López Murphy un 10% y Milei, un 5%, con la certeza de que en las elecciones generales iba a superar el 50% por la suma de los sufragios que obtendrá el candidato de Republicanos Unidos.

Pero en las encuestas que circulan en la calle Uspallata, la sede del gobierno porteño, se estaría registrando menos respaldo para Vidal y un crecimiento superior a lo previsto para López Murphy y Milei. Por más que en los comicios del 14 de noviembre los porcentajes finales mejoren, puede ser demoledor el efecto psicológico (y político) de que la principal candidata elegida por Rodríguez Larreta no pase claramente la barrera del 40% de los votos en las PASO. Esa preocupación explicaría la necesidad de que Macri, el líder del electorado duro, se muestre con Vidal.

El ex mandatario todavía no definió como participará de la campaña. Ya habría desistido de acompañar a Diego Santilli en sus recorridas por la provincia de Buenos Aires, donde las encuestas marcan que tiene una fuerte imagen negativa, pero ya habló con Rodríguez Larreta de qué forma podría estar al lado de Vidal para intentar transferirle el apoyo del voto duro porteño y analiza viajar a Córdoba, un enclave antikirchnerista donde conserva un alto nivel de popularidad, para respaldar al precandidato a diputado Gustavo Santos, ministro de Turismo de Cambiemos.

Pese a la opinión de Rodríguez Larreta y de Vidal, hay referentes de Juntos por el Cambio que no comparten la idea de sumar a Macri a la actividad proselitista, como Alfredo Cornejo, el titular de la UCR: “Macri no debe estar en la campaña porque por su presencia puede ser funcional al discurso del kirchnerismo”, dijo anoche en una entrevista con Luis Novaresio. La polémica sobre el tema, por lo visto, apenas comienza.

Para colmo, Alberto Fernández se subió a este escenario sin ninguna ingenuidad: sus críticas a los liberales en el acto de este lunes en Tecnópolis, ante cientos de jóvenes, fueron una forma de ayudar a que Milei se instale como candidato opositor y así le reste votos a Juntos por el Cambio. En las filas de la coalición opositora miraron con preocupación la enorme concurrencia juvenil al primer acto de campaña del economista liberal, que tuvo lugar en la Plaza Holanda, en Palermo. “Es un fenómeno que explotó en las redes, pero no nos podemos descuidar”, admitieron en JxC.

Si el dilema de Vidal es cómo asegurar que los votos duros no se le escapen, otro problema imposible de resolver es la composición de la lista que encabeza. O, mejor dicho, la convivencia notoriamente forzada de una dirigente prudente como ella con un fundamentalista como Fernando Iglesias, el candidato que pidió incorporar Macri en la nómina de diputados de Capital, de quien tanto la ex gobernadora como el jefe de Gobierno decidieron tomar distancia por sus polémicos tuits sobre las mujeres que visitaron la Quinta de Olivos durante la cuarentena obligatoria.

En Provincia de Buenos Aires, Santilli se mantuvo hasta ahora enfocado en un discurso de campaña cuyo eje son “las preocupaciones de la gente” más que el ataque directo al kirchnerismo. Este miércoles por la mañana, sin embargo, Patricia Bullrich debutará al lado del primer candidato de Juntos para una recorrida por La Plata, junto con el intendente Julio Garro, y ambos coincidirán en hablar sobre seguridad, uno de los temas que mejor conocen por sus reconocidas gestiones en la Nación y en la Ciudad. Se prevén fuertes críticas contra los gobiernos nacional y bonaerense.

Lo que también estaría previsto es la presencia de Bullrich en una actividad junto con López Murphy en Capital. Si bien la jefa del PRO ya estuvo en una recorrida con Vidal, la posible foto con el candidato que tanto inquieta al larretismo, en medio de la volatilidad del voto duro en la Ciudad, seguramente causará nuevos cimbronazos en la campaña opositora. Quizá tantos como los que están provocando las encuestas en estas horas.