+++
author = ""
date = 2022-01-17T03:00:00Z
description = ""
image = "/images/derrumbe-sanatorio-ninos-rosario-1.jpg"
image_webp = "/images/derrumbe-sanatorio-ninos-rosario.jpg"
title = "ROSARIO: COLAPSÓ EL TECHO DE LA GUARDIA DEL SANATORIO DE NIÑOS"

+++

#### **_Un bloque de durlock se desplomó al no soportar la gran cantidad de agua de lluvia, en un aparente desperfecto en los desagües._**

La fuerte tormenta que azotó este domingo por la noche a Rosario, provocó el desplome de parte del techo del Sanatorio de Niños. Por fortuna, no se registraron heridos.

Parte del techo de la guardia del conocido sanatorio infantil colapsó porque no soportó la gran cantidad de agua de lluvia que se había acumulado, en un aparente desperfecto en los desagües, que ya era investigado por las autoridades del centro de salud.

Como se puede ver en el video, había algunos padres con sus chicos esperando para ser atendidos, pero afortunadamente no hubo heridos.