+++
author = ""
date = 2021-06-25T03:00:00Z
description = ""
image = "/images/me2dgzbzg43dmyzvmnsggnrwga.jpg"
image_webp = "/images/me2dgzbzg43dmyzvmnsggnrwga.jpg"
title = "DEBIN, LA NUEVA MODALIDAD DE ESTAFA VIRTUAL"

+++

**_En las últimas jornadas las entidades bancarias salieron a alertar a sus clientes por una nueva modalidad de estafa virtual. Se trata del Débito Inmediato (DEBIN), un mecanismo concebido para agilizar transacciones pero que ha sido utilizado para realizar engaños._**

La pandemia de COVID-19 provocó un auge de las ventas online y de transacción de dinero virtual. Con ello, un escenario proclive a las estafas.

El Débito Inmediato (DEBIN) es un medio de pago online que genera automáticamente un débito en la cuenta de la personas que lo recibe, que puede aceptarlo o rechazarlo. Si lo acepta, el dinero no ingresará a su cuenta sino que será debitado para que lo reciba quien envió el DEBIN.

El mecanismo funciona entre cuentas en pesos y en dólares de todos los bancos a través de la banca por internet o la banca móvil. El DEBIN puede ser generado y cobrado por todos los clientes con cuentas, tanto en pesos como dólares, personas físicas o jurídicas. A diferencia de otro tipo de operaciones, la otra parte no debe ser incorporada previamente y alcanza con solicitarle el nombre de la cuenta (ALIAS-CBU), sin agregar otros datos como el DNI o el CUIT. Además se puede concretar todos los días, las 24 horas.

El sistema está vigente desde septiembre de 2017 en todos los bancos con sistema de banca por internet y/o banca móvil. Fue ideado durante la gestión de Federico Sturzenegger, con el objetivo de avanzar en la eliminación del dinero en efectivo y agilizar los pagos. Sin embargo, en la última semana comenzaron a alertar sobre un engaño vinculado a esta modalidad.

La estafa consiste en que una persona avise a un tercero que le hará un pago a través de este método y en vez de enviarle dinero, se lo saca. Por ejemplo, el estafador se hace pasar por un comprador y le manda un mail o un mensaje al comerciante con la leyenda: “Te envié el pago por DEBIN, acéptalo para recibir el dinero”. El texto viene acompañado de un link, que la persona al aceptar, en vez de recibir dinero se le debita de su cuenta.

Los bancos enviaron mails a sus clientes en los últimos días, alertando por esta modalidad y recomendaron que para evitar ser víctima del engaño, hay que asegurarse que la persona que solicita el DEBIN “es con quien acordaste pagarle”, “conociendo el motivo de la transacción e informándote sobre este medio de pago”.

Días atrás, la señal de noticias TN citó el caso de Georgina, la dueña de un local de venta de muebles y sillones de la localidad de Martínez. Un supuesto cliente se contactó con el negocio pidiendo precios y opciones para comprar un sofá. Tras intercambiar mensajes y fotos de la mercadería, el cliente eligió acceder a uno de los modelos más costosos y pidió abonar por transferencia bancaria.

Bajo el argumento del precio del producto, el cliente dijo que necesitaba permisos especiales para hacer la operación. Pidió hablar con la administración del local. El estafador estaba próximo a lograr su cometido: tras ponerse en contacto con una empleada administrativa, le indicó que necesitaba realizar un paso más para hacer la transferencia de dinero, ya que se trataba de un depósito que necesitaba de su autorización.

Le indicó que ingrese a la banca por internet y que acepte la transferencia que le está llegando. Pero no resultó ser una simple transferencia, fue un DEBIN, un pedido para que el comercio le transfiera la plata a él. La empleada dio el visto bueno sin darse cuenta que había aceptado transferirle el monto del valor del sillón al estafador.

Pese a que Georgina avanzó con la investigación y pudo dar con la titular de la cuenta desde donde se originó el DEBIN, la dueña dijo que hacía años que no la utilizaba. El estafador retiró los fondos de inmediato y no se supo más nada.

Por su parte, la semana pasada la Defensoría del Pueblo de la provincia de Santa Fe le envió una nota al Banco Central de la República Argentina solicitando información sobre esta modalidad de estafa y solicitando nuevos parámetros de seguridad para implementar. “El hecho sobre el que tomamos conocimiento consistió en una aparente estafa mediante DEBIN. El mecanismo consistiría en obtener los datos de cuentas de las víctimas utilizando como pretexto una supuesta operación comercial y generar un DEBIN. Luego se comunican y avisan de una transferencia que requiere de autorización del destinatario, cuando la persona ingresa efectivamente observa una operación pendiente de ejecución y, al aceptar creyendo autorizar una transferencia a su favor, se le debita automáticamente el monto del DEBIN puesto por los estafadores”, relató el organismo.

Y le consultó al BCRA “si no se deberían considerar nuevos parámetros de seguridad en este tipo de operatorias, tales como exigir factores de seguridad (PIN, token, coordenadas, SMS)” o implementarse “un sistema de alerta donde el destinatario pueda verificar efectivamente que la operatoria es de débito inmediato de fondos, instruyéndose a todos los bancos que procedan de esa manera”.

La Defensoría del Pueblo de Santa Fe consideró “pertinente sugerir que las entidades financieras efectúen campaña de difusión alertando a los usuarios respecto a esta actividad delictiva”.