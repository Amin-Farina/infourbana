+++
author = ""
date = 2022-02-08T03:00:00Z
description = ""
image = "/images/61fff291ad570_940_529.jpg"
image_webp = "/images/61fff291ad570_940_529.jpg"
title = "LA RUTA DE LA SEDA CHINA 2.0: LAS CLAVES DEL \"PROYECTO DEL SIGLO\""

+++
##### **_Se trata de una plataforma para promocionar comercio e inversiones, integración de los mercados y cooperación económica regional, abierta e inclusiva. En 2021 involucró inversiones y créditos por 59.5 mil millones de dólares._**

Según un estudio de enero de 2022 de la Universidad de Fudan, una de las más prestigiosas de China, sita en Shanghai, la Iniciativa la Franja y la Ruta (IFyR, o BRI en inglés) involucró durante 2021 inversiones y créditos por 59,5 mil millones de dólares. En 2020 había sido apenas un poco más, 60,5 mil millones. Pero puede decirse entonces que ese es el rango del capital anual que se pone en juego, hoy, en la mayor apuesta de política exterior de la República Popular China.

El año pasado, los 145 países que adhirieron a la IFyR recibieron 13,9 mil millones en inversión y 45,6 mil millones en créditos donde China juega un rol clave.

El sector que más está congregando financiación es el de **energía** (todo un dato del cambio de matriz global: el año pasado no se fondeó ningún proyecto de carbón, y avanzan los de energías limpias). Y si en un principio la región euroasiática era prioritaria, en 2021 se sumó con fuerza África y Medio Oriente (en especial Irak, destruido tras la invasión de Estados Unidos en 2003). América Latina, de donde son socios 19 países, espera su turno.

##### **¿De qué se trata este proyecto anunciado por el presidente Xi Jinping en 2013?**

Una descripción general podría definirlo como una plataforma para promocionar **comercio e inversiones**, **integración de los mercados** y **cooperación económica regional**, abierta e inclusiva. Más allá de que persiga objetivos nacionales estratégicos, China ha puesto ese mecanismo a disposición del mundo. Vista, igual que Rusia, como rival si no enemigo estratégico de EEUU, el presidente de este último, Joe Biden, lanzó el año pasado otra idea llamada "Build Back Better for the World" (Reconstruir mejor para el mundo), un nombre tan pomposo como incierto hasta ahora, que ya recibió no pocas críticas.

La IFyR habla de cooperación y conectividad vía terrestre (la "Franja Económica de la Ruta de la Seda") y transoceánica (la "Ruta Marítima de la Seda"), con diversos corredores. Y ha crecido hasta abarcar la salud, lo ambiental, lo digital y otros cuantos capítulos.

Los países que adhieren a ella firman un Memorando de Entendimiento (o MOU, sigla en inglés), un documento con firma, sello y foto de rigor que el protocolo chino adora. Esos MOU, explica el analista Sebastián Schultz, tienen siete partes que describen la decisión del país firmante y de China en cooperar y buscar prosperidad compartida, los objetivos y principios rectores de la cooperación, las áreas involucradas, las formas cooperativas, los tribunales que revisarán las normas o conflictos (Argentina ya tiene estudiosos de esos temas jurídicos, como María Francesca Staiano e Ignacio Portela, ambos de la Universidad Nacional de La Plata) y finalmente explicita la entrada en vigencia del mismo.

##### **Pero hay más que comercio e inversiones**

La IFyR tiene varias aristas, aunque a la Argentina y a Latinoamérica en general le interesan -sobre todo- las obras de infraestructura que tanto necesitan y que podría fondear el Banco Asiático de Inversiones en Infraestructura, con sede en Beijing y al que Argentina ya adhirió.

Es que la conectividad que busca China también sirve a otros objetivos geopolíticos y de desarrollo.

En su propio territorio, China, que hace un año dejó atrás la indigencia en un hito histórico para la humanidad y que acelera su dominio en la vanguardia de varias tecnologías de punta, viene mejorando en forma acelerada la base material de su área occidental, la más atrasada y la que es fronteriza con regiones de Asia Central muy complicadas por el fanatismo religioso y los afanes separatistas, que entre otras fuentes se alimenta de las operaciones de Occidente para complicarle el ascenso a China, y a Rusia.

A largo plazo, un mejor entorno socioeconómico puede ayudar a aliviar esas tensiones. Y a nivel de su comercio exterior (China no es autosuficiente en energía, minerales ni alimentos), esas tensiones con Occidente, esto es con EEUU, pueden agravarse y bloquear las aguas del Pacífico y del Mar del Sur de la China, por donde hoy pasan gran parte de los buques cargueros con ese suministro y donde hay aliados estadounidenses como Japón, Corea del Sur o el gobierno de la isla de Taiwán, amen de la mayor concentración de marines del globo.

De ahí que, hasta ahora, la obra de mayor envergadura de la IFyR haya sido el corredor vial y el puerto de Gwadar en Pakistán (uno de los principales aliados de China), cuya salida al océano Índico operaría como opción al Pacífico si allí las cosas se agravan y estrangulan los suministros mencionados para China. Cabe recordar que, desde hace tres años, la OTAN ya menciona a China entre sus objetivos.

Todo esto puede parecer lejano para Argentina. Pero no debería serlo para su clase dirigente si quiere salir un poco de la hojarasca y prefigurar cómo vendrá el siglo XXI. Nuestro país necesita inversiones y ve a través de ese prisma la posibilidad de la IFyR, como ya hace 17 años avizoró el primer gobierno kirchnerista y continuaron los siguientes, sin despreciar (las pocas veces que asoman) ofertas de cualquier otra parte del mundo, en una mirada multipolar sin prejuicios ideológicos. Pero la geopolítica vuelve a estar de moda.

En ese sentido, el activísimo embajador en Beijing Sabino Vaca Narvaja, uno de los mayores impulsores (desde antes de ejercer ese cargo) de profundizar las relaciones con China, es un gran lector de Juan Carlos Puig, un teórico de la relación entre la dependencia y la autonomía de los países periféricos. Puig fue canciller en el breve gobierno de Héctor J. Cámpora en 1973, pero en febrero de 1972, cuando Argentina y la República Popular formalizaron sus relaciones diplomáticas, que este mes cumplen así medio siglo, era miembro del Consejo Nacional de Seguridad (Conase).

Los equilibrios para un país como la Argentina actual, con **economía frágil** por la deuda externa, **pocos consensos internos** y **ávido de inversiones** y de **obras de infraestructura** (con muy pocos oferentes mundiales) tienen acaso en esas ideas de autonomías relativas pero defensa al fin de la soberanía un puntal para entender el mundo actual, donde la IFyR constituye quizá el mayor portador de desarrollo global.