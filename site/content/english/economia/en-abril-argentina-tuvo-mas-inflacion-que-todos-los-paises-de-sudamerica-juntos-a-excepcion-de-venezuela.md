+++
author = ""
date = 2021-05-11T03:00:00Z
description = ""
image = "/images/desabastecimiento_1_crop1613565402732-png_543804098.png"
image_webp = "/images/desabastecimiento_1_crop1613565402732-png_543804098.png"
title = "EN ABRIL, ARGENTINA TUVO MÁS INFLACIÓN QUE TODOS LOS PAISES DE SUDAMERICA JUNTOS (A EXCEPCIÓN DE VENEZUELA)"

+++

Una vez más, la Argentina registró la segunda inflación más alta en América latina. En abril, el dato de la inflación local -que oficialmente difundirá este jueves el Instituto Nacional de Estadística y Censos (Indec)- rondó el 4%, según las primeras estimaciones que se manejan en el equipo económico

De este modo, la inflación en la Argentina solo quedó debajo de la de Venezuela, cuyo último registro es el 16,1% de marzo y que vive bajo un régimen de hiperinflación desde hace cuatro años. De hecho, el Fondo Monetario Internacional (FMI) estimó que este año la suba de los precios en el país que gobierna Nicolás Maduro llegará al 5500 por ciento.

Dejando al margen el excepcional caso venezolano, las cifras del costo de vida de marzo indican que el 4,3% registrado por la Argentina es superior a la suma de los incrementos del resto de los países sudamericanos, entre los cuales Paraguay tuvo deflación con un índice del -0,1 por ciento.

La Argentina, con el 4,3% estimado por el estudio Eco Go, acumuló una suba del 43,5% en el último año y los consultores privados proyectan que terminará entre el 45 y el 50 por ciento en diciembre próximo.

Uruguay registró un 0,5% en abril, 6,2% en los últimos 12 meses y se prevé que termine en el 7,2% a fin de año. Brasil habría registrado un 0,6% el mes pasado y el 6,2% en 12 meses según las proyecciones del Itaú Unibanco; se estima que terminará 2021 con una suba del 4,5 por ciento.

En tanto, Paraguay tuvo un resultado casi neutro, del 0,0,5% el mes pasado y acumula el 2,5% en 12 meses; se proyecta que termine en el 3,3% este año. Chile sumó 0,4% en abril y el 3,3% en 12 meses; se espera que termine en el 3,2%, según la proyección de los analistas del Latinfocus Forecast Economics, una estimación similar a la de México, que registró 0,3% el mes pasado y 6% en el último año.

Colombia aportó una suba del 0,35% el mes pasado y del 1,9% en 12 meses; se estima que terminará con una inflación del 2,8%, al igual que Bolivia, cuyo IPC terminó en el 0% en abril y en el 0,6% en los últimos 12 meses. Por su parte, Perú registró un alza del 0,05% en abril y del 2,6% en el último año; se proyecta un 2,2% para diciembre, frente al 0,9% para Ecuador, que tuvo una suba del 0,35% el mes pasado y una deflación del 1,4% el último año.

¿Qué pasó en la Argentina?

Según el relevamiento de precios minoristas de Eco Go, “los precios aumentaron en abril un 4,3%; la inflación Core RPM se aceleró en el mes y se ubicó en 4,3%, en línea con el nivel general, impulsada por los servicios (+4,5%) mientras los bienes presentaron una suba de 4,3% explicada principalmente por alimentos y artículos de esparcimiento como libros y juguetes”.

“Los precios regulados mostraron una fuerte desaceleración respecto de la suba de marzo para ubicarse en 3,6% en abril. Esto se vio principalmente influenciado por los servicios educativos (+11%), combustibles (+4,6% promedio) y transporte público, incluyendo taxis, (+5,4%)”.

En el caso de los precios estacionales “se destacaron aumentos en indumentaria (+13,1%) y verduras (+8,2%), ya que las frutas retrocedieron -1,4%”, indicó el estudio de Marina Dal Poggetto.

Y aunque el Gobierno menciona la suba internacional en el precio de los alimentos como uno de los factores que presiona a la inflación local, la economista María Castiglioni explicó a Infobae que “si uno observa la aceleración de la inflación desde fines del año pasado, luego del freno por la pandemia, se dio en casi todos los rubros y sólo se ve frenada por algunos precios congelados o semi-congelados, como las tarifas, el programa de Precios Máximos y otras regulaciones”.

Además, precisó que “al realizar una comparación con otros países, el efecto de la suba de las materias primas prácticamente no afectó a Perú y Colombia en el precio de los alimentos, aunque sí lo hizo en México y Chile, mientras que en los otros dos países donde más sube la inflación luego de la Argentina, que son Brasil y Uruguay, esto se debe a las políticas de expansión fiscal más que a la inflación importada por las commodities”.

Al respecto, Matías Carugati, de la consultora Seido, indicó que “la suba del precio de los alimentos lógicamente impulsa los IPC hacia arriba. El asunto es que no lo hace de igual forma en todos los países, porque el peso que puede tener el precio internacional sobre el precio al consumidor depende mucho de la estructura económica de cada país”.

Además, “este impacto ocurre en contextos completamente distintos al nuestro. En la mayoría de los países, la inflación es baja o moderada, las expectativas de inflación están ancladas y los bancos centrales efectivamente actúan contra la inflación”.

“En Argentina, la explicación oficial se basa en la ‘inflación importada’, porque los precios de las materias primas subieron a niveles realmente elevados, pero nada dice de la montaña de pesos emitida durante 2020, ni del desanclaje de expectativas. Es, a mi entender, una mirada parcial y equivocada, que elije tomar un factor y esconder los otros”.

En promedio, los analistas del mercado que releva el Banco Central proyectaron que la inflación minorista para abril en la Argentina fue del 3,8% e indicaron que la suba de precios para todo el año se ubicará en 47,3% interanual, con un aumento de 1,3 puntos porcentuales respecto de los pronósticos informados a fines del mes anterior (46% interanual).

Entre las consultoras y bancos, el JP Morgan estima que el IPC llegará al 55%, UBS al 54,9% y Moody’s al 52,8%, mientras que, del otro lado, Gabriel Rubinstein & Asociados estimó el 38 por ciento.

Cabe recordar que el Ministerio de Economía planteó una pauta anual del 29% para este año, aunque los funcionarios se manejan con otros datos que todavía no se dieron a conocer y se acercan más a los pronósticos de los analistas privados que, tanto el ministro Martín Guzmán como el Banco Central, criticaron públicamente, lo que llevó a que varias consultoras optaran por no aportar más sus datos al relevamiento que realiza la entidad que preside Miguel Pesce.

Para mayo, los primeros registros parecen algo más alentadores que en abril. Eco Go marcó una suba del 1,2% en alimentos y proyecta un incremento mensual del 4% en este rubro y y del 3,5% en su relevamiento general de precios. En tanto, Seido registró en la primera semana del mes una suba del 0,8% y del 3,8% como promedio en cuatro semanas, mientras que LCG también arrancó con el 0,8%. “Desacelera, pero sigue en niveles elevados, por encima del 3,5% mensual. Y el mes terminará entre el 3 y el 3,5 por ciento”, concluyó su director, Guido Lorenzo.