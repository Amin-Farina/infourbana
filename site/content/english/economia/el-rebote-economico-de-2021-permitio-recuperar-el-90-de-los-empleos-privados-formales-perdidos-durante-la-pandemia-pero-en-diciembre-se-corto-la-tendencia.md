+++
author = ""
date = 2022-02-07T03:00:00Z
description = ""
image = "/images/2020-11-18_09-11-34___3358-1.jpg"
image_webp = "/images/2020-11-18_09-11-34___3358.jpg"
title = "EL REBOTE ECONÓMICO DE 2021 PERMITIÓ RECUPERAR EL 90% DE LOS EMPLEOS PRIVADOS FORMALES PERDIDOS DURANTE LA PANDEMIA, PERO EN DICIEMBRE SE CORTÓ LA TENDENCIA"

+++

##### **_Desde noviembre de 2020 hasta noviembre pasado la economía sumó 164.000 puestos de trabajo en blanco en el sector privado, según un informe oficial. Qué dicen los datos acerca de las suspensiones_**

La recuperación económica que tuvo lugar a lo largo de 2021 permitió recobrar la mayor parte de los trabajos registrados en el sector privado perdidos durante la etapa inicial de la pandemia, la de mayor impacto económico, a causa de las medidas de confinamiento. Según un informe oficial, con datos hasta noviembre pasado, el mercado laboral había regenerado el 90% de los puestos de ese tipo.

Un informe reciente del Ministerio de Trabajo mostró que en noviembre, de acuerdo a la información que surge de los registros del Sistema Integrado Previsional Argentino (SIPA), el número de trabajadores con empleo asalariado registrado del sector privado creció un 0,3% (sin estacionalidad). Esa proporción representaría la incorporación de 19.300 personas al mundo del empleo asalariado registrado del sector privado.

De acuerdo a las estimaciones oficiales, los datos de noviembre mostrarían así la continuidad de un período de 13 meses de crecimiento del empleo de forma ininterrumpida. “Así, desde noviembre de 2020 hasta noviembre de 2021, 164 mil personas lograron incorporarse al empleo asalariado registrado. Como resultado de este proceso expansivo, hasta el mes de noviembre de 2021, se reintegró al trabajo asalariado registrado en empresas privadas el 90% del total del empleo perdido durante la pandemia”, publicó la cartera laboral.

##### Suspensiones

Otra medición habitual que lleva adelante Trabajo para medir la situación del mercado laboral y que sufrió un impacto considerable por la pandemia fue el de las suspensiones. Tras las primeras restricciones estrictas a la actividad, a mediados de 2020, ese índice había mostrado que prácticamente una de cada cinco empresas había aplicado cesantías temporales entre su plantilla de empleados, un número récord. Cerca de un 9% de los trabajadores había sido suspendido.

“La sostenida reactivación económica y la flexibilización de las restricciones impuestas a la circulación permitieron no solo recuperar buena parte del empleo perdido en el peor momento de la pandemia (entre abril y julio de 2020), sino también reintegrar a la actividad productiva al personal suspendido”, consideró la Encuesta de Indicadores Laborales (EIL), uno de los dos informes mensuales de empleo que publica el Ministerio de Trabajo.

“En efecto, las suspensiones con percepción de una asignación no contributiva, que funcionaron como un instrumento fundamental para sostener el nivel de empleo asalariado registrado durante la pandemia por COVID-19, presentan en diciembre de 2021 una incidencia en el total del empleo asalariado similar al nivel observado en la pre pandemia”, consideraron.

Según la Encuesta de Indicadores Laborales, el porcentaje de empleos suspendidos pasó de un máximo de 8,8% en mayo de 2020 al 0,5% en diciembre de 2021. Mientras que las empresas que aplicaron suspensiones pasaron de su máximo en julio de 2020 del 19,4% al 5,4% en diciembre de 2021. Ambos indicadores se ubican actualmente en valores bajos, incluso algo inferiores a los de diciembre de 2019, explicaron

El EIL tiene un alcance menor que el de la Situación y evolución del Trabajo Registrado ya que solo alcanza a compañías que tienen más de 10 personas en su dotación de personal, aunque tiene un mes menos de rezago. Por eso, este índice ya completó su serie mensual de todo 2021 y arrojó algunas conclusiones.

En diciembre de 2021, el nivel de empleo privado registrado en empresas de más de 10 personas ocupadas del total de los aglomerados relevados por el Ministerio de Trabajo cayó un 0,2% en relación al mes anterior. De esta forma, se interrumpió la tendencia creciente que tenía lugar desde julio. Tanto en el Gran Buenos Aires (GBA) como en el conjunto de aglomerados del interior del país se observaron valores negativos (-0,2% y -0,1% respectivamente).

Una de las razones que explicaron este declive en el último mes del año fue un efecto que suele ser habitual en cada diciembre. “Este resultado está impulsado principalmente por el comportamiento del empleo en el sector de la construcción que cayó 4% luego de 6 meses consecutivos de crecimiento”, explicaron desde Trabajo.

“Sin embargo, en el mes de diciembre este sector suele presentar un comportamiento particular, que suele traducirse en bajas de personal por finalización de obra y renuncias de las y los trabajadores relacionadas con el acceso al Fondo de Desempleo. Habitualmente, esas desvinculaciones son luego informadas como altas de personal en los meses de enero y subsiguientes”.

Como conclusión, el número final de recuperación laboral medido por el EIL entre las empresas con más de 10 empleados fue de 1,2% a lo largo de 2021. Analizado por sectores, el rubro que más puestos de trabajo recuperó fue la construcción (5,3%), seguido por la industria manufacturera (1,5%), servicios financieros y a las empresas (1,4%), Comercio, restaurantes y hoteles (1%), Servicios comunales, sociales y personales (0,3 por ciento). Entre los incluidos en el informe, solo Transporte, almacenaje y comunicaciones perdió parte de su dotación de personal (-0,3 por ciento).

##### Salarios y precios: lo que viene

El Gobierno ya explicitó cuál será la pauta de aumentos salariales que habilitará para la discusión de paritarias de este año, en medio de la negociación con el FMI, un programa que contemplará, al menos según lo anunció el ministro de Economía Martín Guzmán, acuerdos de precios y salarios entre empresarios y sindicatos para alinear expectativas de inflación.

“Lo que vamos a hacer es repetir la mecánica de este año. Hay una pauta que está alrededor del 40%. Con esa pauta iniciamos las negociaciones y en todos los casos vamos a prever reaperturas. En caso de que las reaperturas no sean suficientes”, dijo en los últimos días el ministro de Trabajo Claudio Moroni.

Si bien no tiene una fecha prevista por el momento, el Gobierno planea retomar la dinámica de reuniones tripartitas con representantes del sector empresario y de los gremios para buscar alguna forma de coordinación en el ritmo de incremento de los precios y las pretensiones de recomposición salarial.

Según Guzmán, este tipo de consensos -aunque el año pasado el Gobierno también intentó llevarlos a cabo pero no lo logró- estarán incluidos en un plan de cuatro puntas incluido como esqueleto del acuerdo con el FMI para combatir a la inflación, además del aumento de exportaciones, la reducción de la asistencia monetaria del Banco Central al Tesoro y tasas de interés reales positivas para las inversiones en pesos. Algunos de los detalles que compondrán esos cuatro aspectos todavía están en negociación entre Washington y Buenos Aires.