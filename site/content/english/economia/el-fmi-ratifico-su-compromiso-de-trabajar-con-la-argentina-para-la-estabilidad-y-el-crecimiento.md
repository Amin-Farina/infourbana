+++
author = ""
date = 2021-01-21T03:00:00Z
description = ""
image = "/images/fernandez-fmi.jpg"
image_webp = "/images/fernandez-fmi.jpg"
title = "El FMI ratificó su compromiso de trabajar con la Argentina para la estabilidad y el crecimiento"

+++

**_El presidente Alberto Fernández y la titular del organismo mantuvieron una videoconferencia y coincidieron en seguir trabajando en un nuevo acuerdo de financiamiento para el país._**

La titular del Fondo Monetario Internacional (FMI), Kristalina Georgieva, ratificó el compromiso del organismo multilateral de trabajar junto al Gobierno "para mejorar la estabilidad y apoyar un crecimiento fuerte e inclusivo". durante una conversación que mantuvo con el presidente Alberto Fernández en la que coincidieron en la necesidad de que el nuevo programa de financiamiento tenga una "visión realista de cómo funciona" la economía argentina.

El diálogo se produjo a través de una videoconferencia, en la que ambos coincidieron en continuar trabajando en la elaboración de un programa apoyado por el organismo multilateral y diseñado y conducido por la Argentina.

Luego del contacto, Alberto Fernández y Kristalina Georgieva dieron detalles del encuentro virtual a través de mensajes en sus respectivas cuentas en la red social Twitter.

"Muy buena llamada con el presidente de Argentina @alferdez", sostuvo la titular del FMI, para luego señalar que "discutimos las perspectivas de crecimiento global y las acciones para combatir la crisis económica y de salud".

"También destaqué nuestra gran colaboración con (el ministro de Economía) @Martin_M_Guzman & equipo para mejorar la estabilidad y apoyar un crecimiento fuerte e inclusivo", agregó la directora Gerente del organismo multilateral.

El Presidente, por su parte, calificó como una "muy buena conversación" al contacto que mantuvo con la directora gerente del Fondo Monetario Internacional.

También a través de un serie de mensajes en su cuenta en la red social Twitter, el jefe del Estado ratificó la decisión de continuar trabajando en un programa diseñado y conducido por la Argentina, que cuente con el apoyo del organismo, y remarcó "la importancia de la recuperación económica y de un orden fiscal consistente con ese crecimiento como condiciones necesarias para la estabilización".

"También coincidimos en que el programa debe tener una visión realista de cómo funciona nuestra economía", agregó el Presidente.

"Además, compartimos nuestra intención de trabajar, desde lo que se espera sea un renovado multilateralismo, por una economía mundial más justa e inclusiva", refirió Fernández, quien ratificó que "esa será la posición de la Argentina como miembro del G20 (Grupo de los 20) y del FMI".

Los mensajes del Presidente por Twitter se publicaron unos minutos después de que la Casa Rosada informara -a través de un comunicado- detalles del contacto que mantuvieron Fernández y Georgieva.

"Durante la conversación se remarcó la importancia de la recuperación económica como condición necesaria para la estabilización, así como la necesidad de poner las cuentas fiscales en orden a una velocidad que sea consistente con el crecimiento para garantizar la estabilidad de mediano plazo", informó el Gobierno.

También enfatizaron que existió una coincidencia en que "el programa debe estar basado en supuestos realistas sobre cómo funciona la economía argentina".

La semana pasada, el vocero del organismo multilateral, Gerry Rice, dijo que la Argentina y el FMI "continúan con la conversaciones" sobre la negociación de un nuevo programa de mediano plazo que le permita al país reprogramar los vencimientos del programa vigente por 44.000 millones de dólares y, eventualmente, solicitar nuevos fondos para la recuperación económica.

"Nuestras conversaciones con las autoridades argentinas continúan. Esas discusiones entre el Gobierno argentino y el equipo del FMI continuarán en este período" que acaba de comenzar, dijo Rice durante la primera conferencia de prensa del año, el jueves pasado en Washington.

El vocero precisó que "hubo un breve descanso en el diálogo durante el período de vacaciones (por las Fiestas), pero esperamos que el ritmo de las reuniones, todas virtuales, aumente en las próximas semanas".

Según el funcionario del FMI, "las autoridades del país continúan trabajando en el diseño de su plan económico de mediano plazo y discutiendo las medidas específicas para su implementación y trabajando para apuntalarlo con un amplio apoyo político y social".

En este sentido, Rice dijo que "compartimos la opinión de las autoridades de que abordar los desafíos de Argentina requerirá un conjunto de políticas cuidadosamente equilibradas que fomenten la estabilidad, restablezcan la confianza, protejan a los más vulnerables de Argentina y establezcan las bases para un crecimiento sostenible e inclusivo".

"Argentina, como casi todos los demás países, se enfrenta al desafío de la pandemia y los problemas de salud y humanitarios que esto conlleva", agregó el directivo.

La Argentina solicitó en agosto de 2020 un nuevo programa con el FMI para reemplazar al stand by vigente, a través del cual el país debe 44.000 millones de dólares, con vencimientos concentrados en los próximos dos años.