+++
author = ""
date = 2022-03-07T03:00:00Z
description = ""
image = "/images/riesgo-pais_1000_1100-1.jpg"
image_webp = "/images/riesgo-pais_1000_1100.jpg"
title = "MERCADOS: LOS BONOS ARGENTINOS TOCAN NUEVOS MÍNIMOS Y EL RIESGO PAÍS ROZA LOS 2.000 PUNTOS"

+++

##### **_En un día de fuertes caídas en las bolsas globales, los activos argentinos no son la excepción y se pliegan a las bajas. Más allá de estar descontado, el avance parlamentario del acuerdo con el Fondo Monetario generó amplia repercusión, a la par del movimiento financiero que provocó en el exterior la agresión militar de Rusia a Ucrania._**

Pero la incertidumbre es la que se impone este lunes para los títulos soberanos argentinos, con una pérdida promedio de 3% para los Globales en dólares, emitidos con el canje de deuda. Emisiones como los Globales 2035 y 2046 (GD35 y GD46), con ley extranjera, ya cotizan debajo de los 28 dólares. Y las tasas de retorno del Global 30 (GD30) y del Bonar 30 (AL30, con ley argentina) llegan al 28% anual en dólares.

El riesgo país de Argentina escala 64 unidades, a 1.991 puntos básicos. El indicador de JP Morgan, que mide el diferencial de tasas de los bonos del Tesoro de EEUU con sus pares emergentes, anota un nuevo récord desde la reestructuración de septiembre de 2020.

Desde que el presidente Alberto Fernández anunció el pasado 28 de enero el acuerdo con el organismo de crédito, los dólares alternativos al control de cambio reaccionaron a la baja, inclinación que se profundizó en la última semana. Los bonos repuntaron fuerte en febrero, pero en marzo no solo descontaron lo que habían recuperado, sino que profundizaron las pérdidas, para anotar nuevos mínimos, por debajo del piso de finales de enero.

El ministro de Economía, Martín Guzmán, dio una serie de definiciones respecto a lo que será la implementación del nuevo acuerdo con el Fondo, en caso que resulte aprobado por el Congreso Nacional y el directorio del organismo. Explicó que el movimiento del dólar oficial será “más o menos parecido” al de la inflación para mantener la competitividad de la economía.

Guzmán además confirmó que si se aprueba el acuerdo habrá un primer desembolso de USD 9.800 millones y, por último, advirtió que habrá meses de “alta tensión” en precios. Hay que recordar que ni bien asumió la Presidencia, Alberto Fernández desistió de recibir los desembolsos restantes del stand-by de 2018 y 2019, por unos 11.000 millones de dólares.

Los bonos en dólares atravesaron una última semana “knock out”, con una pérdida promedio de 5% en los Globales y un riesgo país que volvió a escalar sobre los 1.900 puntos. Ahora, cerca de los 2.000 puntos, alcanza un nuevo máximo desde los 2.147 enteros del 9 de septiembre de 2020, un año y medio atrás.

Junto con el descenso del tipo de cambio implícito, las acciones argentinas sostuvieron sus precios, pues éstas ya tenían incorporado el acuerdo con el FMI -por ejemplo, con la ganancia de las empresas de servicios públicos y energía beneficiadas por un ajuste tarifario-, aunque debieron sortear el asedio bajista de Wall Street, donde calaron hondo el anuncio de suba de tasas de la Reserva Federal en marzo y la incertidumbre ocasionada por la guerra en Ucrania.

“Ello se debe a que no sólo el programa económico con el FMI podría ser considerado como moderado -en vista a que actuaría como ‘puente’- sino también por las dudas aún así de lograr cumplirse con las metas fiscales, monetarias y cambiarias. Adicionalmente, y más allá de que se descuenta una aprobación en el Congreso, el tratamiento en las próximas semanas se espera acentúe los tironeos políticos, en medio de especulaciones sobre el escenario hacia 2023″, consideró el economista Gustavo Ber.

Según The Economist Intelligence Unit (EIU), el impacto global de las sanciones al régimen de Vladimir Putin “será limitado”, mientras que espera que “el efecto más grave del conflicto entre Rusia y Ucrania para la economía mundial llegue en forma de precios más altos de las materias primas”.

“Los precios de las materias primas podrían dispararse debido a tres factores: preocupaciones sobre los suministros, la destrucción de la infraestructura física y las sanciones. Nuestra suposición central es que ni la Unión Europea ni los EEUU impondrán una prohibición a las exportaciones de hidrocarburos de Rusia. Incluso en ausencia de un embargo, los precios del petróleo, el gas, los metales básicos y los granos se dispararán”, apuntó el informa de EIU.