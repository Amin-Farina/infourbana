+++
author = ""
date = 2022-01-28T03:00:00Z
description = ""
image = "/images/33ssyspnbzf5rl4mnl3zywg6p4.jpg"
image_webp = "/images/33ssyspnbzf5rl4mnl3zywg6p4.jpg"
title = "EL FMI CONFIRMÓ EL ACUERDO Y ANTICIPÓ QUE SE REDUCIRÁN LOS SUBSIDIOS A LA ENERGÍA"

+++

#### **_El staff emitió un comunicado con los ejes del entendimiento y aclaró que la aprobación final está sujeta al directorio del organismo multilateral_**

El Fondo Monetario Internacional (FMI) confirmó el principio de acuerdo con la Argentina y afirmó que con menos subsidios y un aumento de tasas bajará la inflación.

En un comunicado, el organismo expresó: “Un equipo del Fondo Monetario Internacional (FMI), encabezado por Julie Kozack, Subdirectora del Departamento del Hemisferio Occidental, y Luis Cubeddu, jefe de la misión para Argentina” expresó que “el personal técnico del FMI y las autoridades argentinas han llegado a un entendimiento sobre políticas clave como parte de las discusiones en curso sobre un programa respaldado por el FMI”.

> “Las principales áreas de acuerdo son las siguientes: El personal del FMI y las autoridades argentinas han acordado la senda de consolidación fiscal que constituirá un ancla política clave del programa. La senda fiscal acordada mejoraría de forma gradual y sostenible las finanzas públicas y reduciría la financiación monetaria”.

“Es importante destacar que también permitiría aumentar el gasto en infraestructura y ciencia y tecnología, y protegería programas sociales específicos. Acordamos que una estrategia para reducir los subsidios a la energía de forma progresiva será esencial para mejorar la composición del gasto público”.

“Hemos llegado a un acuerdo sobre un marco para la aplicación de la política monetaria como parte de un enfoque múltiple para hacer frente a la persistente alta inflación. Este marco pretende ofrecer tipos de interés reales positivos para apoyar la financiación interna y reforzar la estabilidad”.

“También hemos acordado que el apoyo financiero adicional de los socios internacionales de Argentina ayudaría a reforzar la resistencia externa del país, y sus esfuerzos para asegurar un crecimiento más inclusivo y sostenible”, continuó el comunicado.

> “El personal del FMI y las autoridades argentinas continuarán trabajando en las próximas semanas para alcanzar un acuerdo a nivel del personal. Como siempre es el caso, el acuerdo final sobre un acuerdo de programa estaría sujeto a la aprobación del Directorio Ejecutivo del FMI”, concluyeron.

La directora gerente del organismo Kristalina Georgieva se mostró conforme “por el progreso de hoy entre el personal técnico del FMI y las autoridades de Argentina para llegar a un entendimiento sobre políticas clave para un programa respaldado por el FMI para abordar los desafíos actuales como la inflación y asegurar un crecimiento más inclusivo y sostenible para el pueblo argentino”, mencionó.

Por su parte, la directora gerenta adjunta Gita Gopinath dijo que “el personal técnico del FMI y las autoridades argentinas llegaron a un entendimiento sobre políticas clave, incluida una vía fiscal para mejorar de manera gradual y sostenible las finanzas públicas, reducir el financiamiento monetario y reducir el déficit primario a 0 para 2025. Se continuará trabajando para llegar a un acuerdo a nivel de personal”, consideró.

El entendimiento implica un nuevo programa de dos años y medio durante el cual el FMI hará revisiones trimestrales de las metas acordadas y desembolsos de dinero que la Argentina utilizará para cancelar el stand by contraído por el gobierno de Mauricio Macri y fortalecer las reservas. Luego habrá un período de 10 años para cancelar la refinanciación, que comenzaría en 2026.

El acuerdo alcanzado es el primer paso en la resolución “completa” de la reestructuración de la deuda. Este primer entendimiento alcanza las políticas que deberá llevar adelante el Gobierno en los próximos dos años y medio y que incluirán diez evaluaciones por parte del Fondo. La segunda parte es la que aún está en negociación y que, adelantó Guzmán, tomará unas semanas más: qué plazo habrá luego de esos dos años y medio para que la Argentina repague los desembolsos.

El entendimiento técnico incluirá metas fiscales, de financiamiento monetario desde el Banco Central, tasas de interés en pesos positivas, no incluirá un salto cambiario según mencionó el ministro de Economía, acumulación de reservas y no contemplará reformas estructurales tradicionales como suelen requerir este tipo de programas financieros con el FMI.