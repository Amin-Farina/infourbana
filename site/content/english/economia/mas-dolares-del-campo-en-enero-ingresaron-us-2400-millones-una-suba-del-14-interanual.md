+++
author = ""
date = 2022-02-02T03:00:00Z
description = ""
image = "/images/cereales-1.jpg"
image_webp = "/images/cereales.jpg"
title = "MÁS DÓLARES DEL CAMPO: EN ENERO INGRESARON US$ 2400 MILLONES, UNA SUBA DEL 14% INTERANUAL"

+++

##### **_El sector agroindustrial liquidó más de US$ 2.441 millones durante enero, según informó la Cámara de la Industria Aceitera de la República Argentina y el Centro de Exportadores de Cereales (CIARA-CEC)._**

Esta cifra representa una suba del 14,07% respecto al mismo mes del 2021, cuando se liquidaron más de US$ 2.140 millones, Ambas entidades, que representan el 40% de las exportaciones argentinas, puntualizaron que en la generación de divisas “sigue influyendo el crecimiento internacional de precios de los commodities, aceites y sus derivados industrializados por la pandemia”.

No obstante, señalaron que la menor exportación de subproductos con valor agregado y derivados de la soja, que suelen tener precios más elevados que la materia prima, es lo que imposibilita un mayor ingreso de divisas. Al respecto, también mencionaron “las dificultades operativas por la bajante persistente del río Paraná y las demoras en las operaciones por la aplicación de los estrictos protocolos sanitarios en el proceso de exportación”.

En ese contexto, se registró una merma intermensual del 8,85% respecto a diciembre, cuando se alcanzaron unos US$ 2.678 millones. Con todo, enero suele ser un mes con menor liquidación de divisas respecto de diciembre. La entrada de dólares está “fundamentalmente relacionada” con la compra de granos que luego serán exportados, ya sea en su mismo estado o como productos procesados, luego de una transformación industrial”.

Desde CIARA-CEC además remarcaron que “la mayor parte del ingreso de divisas en este sector se produce con bastante antelación a la exportación, que ronda los 30 días en el caso de la exportación de granos y alcanza hasta los 90 en el caso de la exportación de aceites y harinas proteicas. Esa anticipación depende también del momento de la campaña y del grano de que se trate, por lo que no existen retrasos en la liquidación de divisas”

Asimismo, aclararon que, en este sector, las comparaciones estadísticas entre distintos períodos “son generalmente imprecisas o inexactas, ya que la liquidación de divisas está fuertemente influida por el ciclo comercial de los granos, que depende de diversos y cambiantes factores exógenos”.

Recordaron que el complejo oleaginoso-cerealero aportó, el año pasado, el 40,78 % del total de las exportaciones de la Argentina, según datos del INDEC.

“El principal producto de exportación del país es la harina de soja (13,5 % del total), que es un subproducto industrializado generado por este complejo agroindustrial, que tiene actualmente una elevada capacidad ociosa cercana al 50%”, concluyeron desde la Cámara de la Industria Aceitera y el Centro de Exportadores de Cereales.