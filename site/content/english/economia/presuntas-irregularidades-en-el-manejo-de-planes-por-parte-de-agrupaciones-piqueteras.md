+++
author = ""
date = 2021-08-19T03:00:00Z
description = ""
image = "/images/piqueteros-1.jpg"
image_webp = "/images/piqueteros.jpg"
title = "PRESUNTAS IRREGULARIDADES EN EL MANEJO DE PLANES POR PARTE DE AGRUPACIONES PIQUETERAS"

+++

FUENTE: TN

**_El diputado nacional Héctor “Toty” Flores y la dirigente de la Coalición Cívica ARI Fernanda Reyes presentaron un pedido de informes al ministro de Desarrollo Social Juan Zabaleta. El requerimiento es para que el funcionario brinde precisiones sobre posibles irregularidades por parte de agrupaciones piqueteras en el manejo de planes sociales que distribuye el organismo a su cargo._**

La presentación se hizo tras las imágenes que emitió TN en la que agrupaciones le tomaban asistencia a las personas que fueron a la masiva marcha del miércoles en reclamo de planes sociales. En una recorrida TN registró el testimonio de personas en la protesta a Desarrollo Social que aseguraron que tienen que ir a las movilizaciones para poder cobrar la plata que reciben de las organizaciones.

En el documento los dirigentes mencionaron las imágenes de TN: “Informe si a partir de la información periodística presentada en el día de ayer en el canal de televisión Todo Noticias-TN, sobre posibles irregularidades en la asignación de programas sociales, se iniciaron las actuaciones administrativas correspondientes, a los efectos de investigar y sancionar los hechos denunciados.

Requirieron también que se explique y se detalle cuáles son los canales institucionales existentes para canalizar las denuncias sobre irregularidades en los programas sociales. Pidieron además que se responda si las organizaciones denunciadas fueron o son beneficiarias de créditos y/o subsidios por parte del Estado Nacional y cuáles serán las acciones que llevarán adelante ante estos hechos.

Solicitaron que Desarrollo Social comunique si recibió denuncias sobre presuntas irregularidades en el desarrollo o ejecución del Programa Potenciar Trabajo. En caso afirmativo se requiere que acompañe un informe estadístico en el que detalle cantidad y objeto de las presentaciones, cooperativa y/o organización civil sin fines de lucro denunciada, jurisdicciones donde se desarrollaría el acto denunciado y cantidad de denuncias que motivaron el inicio de una acción judicial por parte del Ministerio.

Además del pedido de informes los dirigentes expresaron su repudio a este tipo de prácticas. El diputado planteó: “Esto demuestra lo que vengo insistiendo hace tiempo: tienen de rehenes a los vecinos con los planes. Tenemos que cambiar esta realidad. No me digan que no se puede porque nosotros lo hicimos con La Juanita; donde miles de vecinos empezaron a trabajar. Es 100% decisión política”.

Reyes criticó: “No puede ser que los que den los planes sean los que los reciban y sean los que se auditen. Eso es el Estado ausente en su máxima expresión y las únicas perjudicadas son las personas, rehenes de un sistema que los condena a la pobreza y a la resignación”.