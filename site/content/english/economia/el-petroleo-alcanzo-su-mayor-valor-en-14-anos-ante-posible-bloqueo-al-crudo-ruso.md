+++
author = ""
date = 2022-03-08T03:00:00Z
description = ""
image = "/images/barril-de-petroleo.jpg"
image_webp = "/images/barril-de-petroleo.jpg"
title = "EL PETRÓLEO ALCANZÓ SU MAYOR VALOR EN 14 AÑOS ANTE POSIBLE BLOQUEO AL CRUDO RUSO"

+++

##### **_El barril de petróleo alcanzó este lunes en Nueva York su mayor precio en 14 años, luego de que los Estados Unidos consideraran la posibilidad de un bloqueo total a las compras de crudo y gas provenientes de Rusia, al tiempo que el oro cerró en 2.000 dólares, en el marco del agudo conflicto que se vive en Ucrania._**

El barril de la variedad WTI ganó 3,6% y cerró en 120 dólares; mientras que el tipo Brent subió 4,5% y se pactó en 123,40 dólares, según cifras consignadas en el New York Mercantil Exchange (NYMEX).

El petróleo saltó en la apertura del mercado tras las noticias de que la administración Joseph Biden estaba analizando la prohibición de importar crudo ruso, según reporta la agencia Bloomberg. Los precios se redujeron después de que Alemania dijo no tiene planes de detener las importaciones de energía rusas, lo que reforzó la volatilidad en el mercado.

Los analistas consideran que una prohibición total de las importaciones de crudo ruso, podría eliminar hasta 4 millones de barriles diarios del mercado.El aumento de los precios del petróleo y los temores sobre la oferta también están elevando el precio de los combustibles que impacta de manera directa sobre la inflación.

###### El Fondo Monetario Internacional advirtió sobre graves consecuencias para la economía mundial.

El secretario de Estado de los Estados Unidos, Antony Blinken había dicho durante el fin de semana que la Casa Blanca está en "discusiones muy activas" con Europa sobre una prohibición para endurecer la presión económica sobre Putin, pero la mayoría de los compradores se niegan a aceptarla.

JPMorgan Chase & Co. pronosticó que el barril del Brent podría terminar el año en 185 dólares por barril si los envíos rusos fueran interrumpidos.

Mientras tanto, el acuerdo nuclear con Irán está lejos de concretarse ya que la Agencia Internacional para la Energía Nuclear (AIEA) y Teherán han postergado las decisiones para mediados de año, lo que aleja la posibilidad de un aporte de unos 2 millones de barriles diarios por parte del régimen persa.

Al mismo tiempo, Libia informó este lunes que su producción cayó por debajo de un millón de barriles diarios, debido a una crisis política interna, lo cual la lleva a no poder cumplir con su pacto con la Organización de Países Exportadores de Petróleo y sus aliados (OPEP+).

En tanto, el precio el gas natural se negoció en 4,80 dólares con una baja de 4,3% por cada millón de BTU.

Finalmente, el oro subió 1,7% y alcanzó este lunes los 2.000 dólares por onza, tras el recrudecimiento del conflicto en el este europeo y las consecuencias sobre los precios de las materias primas y su impacto sobre la inflación.