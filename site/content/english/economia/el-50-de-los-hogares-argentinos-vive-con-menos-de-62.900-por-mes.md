+++
author = ""
date = 2021-09-28T03:00:00Z
description = ""
image = "/images/clase-media.jpg"
image_webp = "/images/clase-media.jpg"
title = "EL 50% DE LOS HOGARES ARGENTINOS VIVE CON MENOS DE $62.900 POR MES"

+++

#### **_La cifra se desprende de la Encuesta Permanente de Hogares (EPH) del Indec, correspondiente al segundo trimestre de este año. En tanto unas 2,9 millones de personas, que integran el decil más pobre, subsistieron con menos de 6667pesos. El ingreso per cápita promedio alcanzó los $26.021._**

La mitad de los hogares de la Argentina vive con menos de $62.900 por mes, según el informe de Evolución de la Distribución del Ingreso (EPH) publicado este martes por el Indec, que concluyó que el decil más rico de la sociedad tiene ingresos 16 veces más altos que la porción más pobre.

Los datos difundidos por el organismo, que corresponden al segundo trimestre del 2021, señalan que el 10% más pobre de la población total explicaba el 1,6% del total de ingresos, mientras que el 10% más rico concentró el 32%.

En lo que respecta al nivel de equidad de los ingresos, medidos a través del Coeficiente de Gini, una relación matemática que tiene como referencia al “0″ para el nivel de mayor igualdad y al “1″ para el de mayor desequilibrio, al cierre del primer trimestre fue de 0,434 puntos, frente al 0,451 de igual período de 2020.