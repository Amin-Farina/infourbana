+++
author = ""
date = 2022-02-11T03:00:00Z
description = ""
image = "/images/wnqcgxbkz5e3fpvkfes5wopf4q-1.jpg"
image_webp = "/images/wnqcgxbkz5e3fpvkfes5wopf4q.webp"
title = "EL GOBIERNO ACELERA LA LICITACIÓN DEL GASODUCTO NÉSTOR KIRCHNER PARA AMPLIAR EL TRANSPORTE DE GAS"

+++

##### **_A través de un Decreto de Necesidad y Urgencia (DNU) que firmará en los próximos días el presidente Alberto Fernández, el Gobierno habilitará a la empresa estatal Ieasa (la ex Enarsa) para convocar a las licitaciones para la construcción del primer tramo del Gasoducto Néstor Kirchner, que en una primera etapa conectará Vaca Muerta, la zona de producción de hidrocarburos no convencionales de Neuquén, con las provincias de Río Negro, La Pampa, Buenos Aires y Santa Fe._**

La producción nacional de gas actual no permite cubrir la demanda interna por la falta de capacidad de transporte desde los centros de producción hasta los centros de consumo. Por eso, la Secretaría de Energía avanzó en el trámite para la construcción del Gasoducto mediante la resolución 67/2022, publicada el miércoles pasado en el Boletín Oficial, en la que se declara a la realización de la obra de interés público nacional. Así, advirtió que su construcción no admite demoras ya que la capacidad de transporte del área se encuentra prácticamente saturada.

“Este conjunto de obras decidido por el presidente Alberto Fernández y la vicepresidenta Cristina Fernández de Kirchner, significa el más ambicioso plan de ampliación del sistema nacional de transporte de gas”, destacó el secretario de Energía Darío Martínez.

“Tienen por objeto ampliar la capacidad de transporte en 44 millones de m3 diarios y así utilizar producción nacional y trabajo argentino para sustituir miles de millones de dólares de importaciones actuales de GNC y gasoil”, agregó. El secretario anticipó que se la primera etapa se va a licitar y construir a través de Iesa. 

“Sus equipos ya están trabajando con el objetivo de acelerar al máximo los tiempos, con la colaboración de los técnicos de nuestra secretaria”, detalló.

Martínez adelantó que es “inminente” la firma del DNI presidencial que otorga la concesión del transporte del nuevo gasoducto a Ieasa y la autoriza a constituir un fideicomiso como herramienta de administración y financiera del programa.

> “El financiamiento para la primera etapa está completamente garantizado con fondos del tesoro, y los que la ley 27.605 de Aporte Solidario le otorgó a Ieasa. Se trata de una inversión de USD 1.566 millones, que tiene como obra central el tramo Tratayen a Saliqueló del Gasoducto Néstor Kirchner, más otras obras”, señaló.

Entre las obras complementarias se encuentran la ampliación de capacidad del Centro Oeste, el Gasoducto Mercedes a Cardales, la ampliación del Neuba II en Ordoqui, los tramos finales Norte y Sur de Amba, y las dos primeras etapas de la Reversión del Gasoducto Norte.

El funcionario informó que en septiembre firmó órdenes de pago por $60.190 millones para que Tesorería transfiera a Iesa los fondos de la Ley 27.605 de Aporte Solidario (impuesto a las grandes fortunas); y en diciembre hizo lo mismo con una orden de pago por $59.000 millones para obras de Transporte de Gas del Presupuesto 2021.

Martínez explicó que la primera etapa permitirá ampliar en 24 millones de m3 diarios la capacidad de transporte. “Se van a generar puestos de trabajo, producción y ahorros significativos para el país, porque más capacidad de transporte significa la posibilidad y el desafío de producir gas argentino para ser transportado. No dudo que los trabajadores, técnicos, pymes y las empresas productoras, lo harán realidad, transformando las reservas del subsuelo, en energía para el país.

Desde la Secretaría de Energía destacaron que las obras permitirán sustituir importaciones de GNL que hoy se requieren para cubrir la demanda interna; reemplazar el combustible líquido utilizado en las centrales termoeléctricas por gas natural de producción nacional; y enfrentar la caída de la producción de gas en la cuenca NOA y de Bolivia, tradicional proveedor de la Argentina.

##### Las importaciones de gas previstas para el invierno

El Gobierno anticipó los niveles de gas que deberá importar para cubrir la demanda durante el invierno, en los meses de más consumo. Según datos oficiales, durante 2022 se prevé que la producción nacional abastecerá el 84% de la oferta total de gas natural. Mientras que el gas importado de Bolivia aportará el 7% y el GNL importado el 9% de la oferta total.

En los meses de verano, los de menor demanda, el gas de origen local cubrirá en promedio el 94% de la demanda total y el 6% restante será aportado por el gas de origen boliviano. En el invierno, en tanto, el gas Nacional representará el 73% del total de la demanda y el resto se cubrirá con gas de Bolivia (9%) y GNL importado (18%).

Mientras que se espera que el precio del gas local ronde los USD 4,84 por millón de BTU en 2022, el costo del gas importado de Bolivia se estima en USD 7,46 por millón de BTU. Mientras que el precio del GNL importado al ser un commodity se determina por el mercado internacional: según la cotización del pasado 12 de enero asciende a un promedio de USD 23,72 por millón de BTU para el periodo de mayo a septiembre de 2022 (a este componente debe sumarse el costo de la regasificación).