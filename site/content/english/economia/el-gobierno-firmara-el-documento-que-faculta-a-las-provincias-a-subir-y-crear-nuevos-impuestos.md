+++
author = ""
date = 2021-12-27T03:00:00Z
description = ""
image = "/images/2jvhh65j4zhhnaglbfiw5w72yy-1.jpg"
image_webp = "/images/2jvhh65j4zhhnaglbfiw5w72yy-1.jpg"
title = "EL GOBIERNO FIRMARÁ EL DOCUMENTO QUE FACULTA A LAS PROVINCIAS A SUBIR Y CREAR NUEVOS IMPUESTOS"

+++

#### **_Alberto Fernández y casi todos los gobernadores firmarán este lunes el Consenso Fiscal que faculta a las provincias a subir y crear nuevos impuestos._**

Con el respaldo de oficialistas y opositores, el Presidente apunta a fortalecerse dentro del Frente de Todos y utilizará el pacto para mostrar iniciativa y consenso político frente a las negociaciones con el Fondo Monetario Internacional (FMI).

Hoy a las 17 el mandatario junto al jefe de Gabinete, Juan Manzur, y el ministro del Interior, Eduardo “Wado” de Pedro, recibirá a gobernadores y vicegobernadores en Casa Rosada. Hasta este domingo restaba definir si el acto se llevará a cabo en el Museo del Bicentenario o en el Salón Eva Perón. Alberto Fernández buscará ponerse en el centro de la escena política nacional luego del rechazo legislativo propinado por la oposición al Presupuesto 2022.

“Trabajamos construyendo acuerdos con las provincias, atendiendo la diversidad y las necesidades de nuestro extenso país con una mirada federal. Por tercer año consecutivo vamos a firmar el Consenso Fiscal con todos los distritos excepto CABA”, manifestó este domingo De Pedro remarcando el rechazo de Horacio Rodríguez Larreta.

El Consenso Fiscal 2021 —que se aplicará a partir del año próximo— le permitiría a las provincias crear nuevos impuestos, como por ejemplo, un “impuesto a la herencia” y también incrementar otros, como las alícuotas de ingresos brutos de algunas actividades. Sin embargo, en Casa de Gobierno aseguran que se trata de una “armonización tributaria”.

“El nuevo acuerdo devuelve autonomía a las provincias y también equilibra el sistema fiscal a lo largo del territorio. Es momento de impulsar políticas pensando en una Argentina con más industria, empleo y crecimiento”, argumentó el Ministro del Interior a través de sus redes sociales.

Por su parte, Silvina Batakis, secretaria de Provincias, y encargada de la parte técnica y política del pacto, definió que se trata de “una corrección específica que les devuelve autonomía a las provincias y también les da estabilidad y seguridad jurídica a los distritos y a los contribuyentes respecto a los impuestos provinciales”. En diálogo con Radio 10, agregó que el Gobierno Nacional impulsa “una redacción nueva” de este acuerdo firmado en 2017 porque “estipulaba muchas cláusulas con las que los gobernadores no estaban de acuerdo”.

El Consenso Fiscal fue impulsado durante la gestión de Mauricio Macri con la mira puesta en ordenar las estructuras tributarias de las jurisdicciones. En 2017, todas las provincias -excepto San Luis y La Pampa- se habían comprometido a bajar de modo gradual la presión impositiva hasta el 2022. Pero con la disminución en la recaudación, tras el estallido de la crisis económica y la devaluación de 2018, quedaron con los números en rojo.

Este es uno de los argumentos que utilizaron los gobernadores de la oposición para confirmar que asistirán a la firma del pacto. Son los radicales Gerardo Morales, Gustavo Valdés y Rodolfo Suárez. “No firmarlo para nosotros es volver a la aplicación del Consenso Fiscal 2017 que establece una baja de impuestos no gradual, sino inmediata”, explicaron. En el caso de Jujuy impactaría en una reducción de recaudación en torno a los 6 mil millones de pesos. La situación de Corrientes y Mendoza es similar, no pueden bajar su nivel de ingresos y su único camino para no perder recaudación es acompañar al Presidente. Desde las tres provincias aseguraron que no aumentarán impuestos.

El que se despegó fue Rodríguez Larreta quien, a diferencia del resto de las jurisdicciones, tiene una caja que la garantiza cierta autonomía.”En la Ciudad no vamos a aumentar impuestos, por eso no lo vamos a firmar”, aclaró el jefe de Gobierno, días atrás.

En 2020 el Gobierno porteño también había rechazado la firma del pacto fiscal ya que había un párrafo que ratificaba la quita de la coparticipación a la Ciudad que encabezó Alberto Fernández y que fue el puntapié inicial de la ruptura del diálogo con el intendente de la Capital Federal. “Si Rodríguez Larreta firmara, estaría avalando que la coparticipación no es el vehículo para la transferencia de servicios y sus competencias”, explicaron fuentes de Casa Rosada a Infobae.

Desde las oficinas de Uspallata explicaron: “El Consenso establece la suspensión por el plazo de un año de los juicios contra el Estado Nacional. Aceptar suspender dichos plazos implica posponer la resolución del caso, lo que obliga a sostener en el tiempo las medidas de emergencia tributaria que tuvimos que tomar, para lo cual tenemos el compromiso de dar de baja en el caso de que Corte nos dé la razón”.

En Nación consideran que Larreta quedará “aislado, obligado por su estrategia en la Corte”, mientras que Alberto Fernández saldrá “fortalecido” luego de la foto que obtenga esta tarde junto a gobernadores propios y opositores.

El Presidente necesita mostrar consenso y respaldo político en el marco de las negociaciones con el Fondo Monetario Internacional. El evento de esta tarde buscará ser utilizado como la antesala del Plan Plurianual y un eventual nuevo Presupuesto que serán enviados al Congreso de la Nación en los primeros meses de 2022.

Los principales puntos del Consenso Fiscal 2021:

* Las provincias y la Ciudad de Buenos Aires durante 2022 podrán legislar un impuesto a un aumento de riqueza obtenido por herencias, legados, donaciones y anticipos de herencia.
* Aplicar alícuotas máximas para el impuesto sobre los Ingresos Brutos: para agricultura, ganadería, pesca, minería de 0,75%; industria manufacturera, 1,50%; construcción, 2,5%; comercio, 5%; hoteles y restaurantes, 4,5%; comunicaciones 5,5%; telefonía celular 6,5%; intermediación y servicios financieros 9% (créditos hipotecarios exento); actividades Inmobiliarias, empresariales y de alquiler 5%; servicios sociales y de salud 4,75%.

Estos nuevos porcentajes implican subas en varios rubros como comunicaciones y servicios financieros.

* Las actividades dentro de agricultura, ganadería , pesca y minería que vendan a consumidores finales quedarán sujetas, en relación a esas operaciones, a la alícuota máxima establecida para comercio.
* Mantener desgravados los ingresos provenientes de las actividades de exportación de bienes (excepto los vinculados con actividades mineras o hidrocarburíferas y sus servicios complementarios) y los ingresos provenientes de prestaciones de servicios cuya utilización o explotación efectiva se lleve a cabo en el exterior del país.
* Se gravará el comercio electrónico de servicios digitales prestados por sujetos radicados, residentes o constituidos en el exterior a consumidores o empresas domiciliados, radicados o constituidos en jurisdicción provincial y la CABA. Esto incluye el servicio de suscripción online para acceso a entretenimiento (música, videos, transmisiones audiovisuales y juegos) y la intermediación en la prestación de servicios a través de plataformas digitales (hoteleros, turísticos y financieros) y las actividades de juego que se desarrollen o exploten a través de cualquier medio digital.
* Establecer una alícuota máxima del 3,5% a la transferencia de inmuebles, del 3% a la transferencia de automotores y del 2% para el resto de contratos y operaciones alcanzadas por este tributo.
* Las provincias y la CABA solo podrán incrementar el stock de deuda denominada en moneda extranjera solo en los casos de líneas de financiamiento con organismos bilaterales o multilaterales de crédito, desembolsos pendientes, incrementos de stock generados por administración de pasivos, canjes o reestructuraciones y amortizaciones de capital.