+++
author = ""
date = 2022-05-10T03:00:00Z
description = ""
image = "/images/foto-martin-guzman.webp"
image_webp = "/images/foto-martin-guzman.webp"
title = "EL EMBAJADOR DE EEUU EL A MARTÍN GUZMÁN: \"EL TRABAJO QUE HIZO ANTE EL FMI FUE IMPRESIONANTE  "

+++
#### En medio de la interna oficial que tiene al ministro en la mira del kirchnerismo, Marc Stanley respaldó al funcionario y destacó su rol en la renegociación de la deuda

En plena ofensiva del kirchnerismo y sectores del Frente de Todos que buscan desplazarlo, el embajador de Estados Unidos en Buenos Aires, Marc Stanley envió un fuerte mensaje político de respaldo al ministro de Economía Martín Guzmán: “El trabajo que hizo ante el Fondo Monetario Internacional (FMI) fue impresionante”.

Durante su exposición en la tercera edición del “AmCham Summit”, un encuentro de la Cámara de Comercio de Estados Unidos en la Argentina (AmCham) al que también asisten autoridades parlamentarias y dirigentes de la oposición, el representante norteamericano se dirigió directamente a Guzmán -que estaba presente en el recinto-, y lo elogió por su tarea de renegociación de la deuda contraída por el gobierno de Mauricio Macri.

_“No se si tuvo el suficiente crédito. El trabajo que hizo el ministro Guzman en el FMI fue realmente impresionante. Su comportamiento, su conocimiento y su habilidad para trabajar cada tema. Lo que yo atestigüé fue muy increíble”,_ subrayó Stanley en su intervención.

El titular de Economía recibió de esta manera un estratégico respaldo, en momentos en que resiste en el cargo ante las críticas de dirigentes de La Cámpora y otros actores del oficialismo que lo cuestionan por su política económica.

El comentario se produjo mientras el evento se desarrollaba en el Alvear Icon Hotel de Puerto Madero y el ministro irrumpió en la escena de la Amcham, antes de dar su discurso ante las empresas e inversores norteamericanos. En ese marco, Stanley notó la presencia del funcionario y llamó la atención sobre su llegada. “Aquí viene un buen hombre”, dijo sobre Guzmán, para luego continuar con sus elogios hacia su trabajo en el FMI.

Guzmán no fue el único funcionario del Gobierno que fue reinvindicado por el embajador de Estados Unidos. Otro de los hombres que recibió un fuerte respaldo en su gestión fue su par argentino en Estados Unidos, Jorge Argüello.

_“Es el mejor compañero que puedo imaginar. Totalmente enfocado y siempre mejorando”_, afirmó Stanley sobre su colega argentino. “La relación bilateral es muy respetada, en Washington tiene las puertas abiertas. Desearía tener tantas puertas abiertas yo aquí en Argentina. Lo estoy intentando”, completó.

A lo largo de toda su alocución, el embajador calificó el vínculo bilateral con Argentina y sus gobiernos como de “amistad”, a pesar de las diferencias y matices que existen en la relación diplomática. _“Probablemente no estemos de acuerdo en todo y no tengamos los mismos amigos, pero tenemos intereses y objetivos en común, y disfrutamos estar juntos”_, sostuvo Stanley con tono diplomático, en una referencia a los lazos cercanos de la gestión de Alberto Fernández con países de América latina que confrontan con Washington, como Cuba, Venezuela, Nicaragua.

Además, Stanley pidió continuar con el “trabajo conjunto” contra las consecuencias de la “invasión rusa a Ucrania que incluye ataques a civiles y violaciones a los derechos humanos”, y remarcó los valores compartidos con Argentina en materia de “derechos humanos”, entre otros. Y advirtió que el país “puede ser la respuesta a diversos problemas” que ocurren en el mundo.

_“Estoy aprendiendo mucho de este Gobierno. Creo en el país y me gusta ver cómo son resilientes con otras naciones. Amo a la Argentina, creo que tiene mucho por ofrecer”, afirmó Stanley, que puso como pilares del vínculo con el país con los ejes de “amistad, cooperación, comercio y turismo”. Y remarcó que Argentina “tiene una oportunidad frente al escenario actual tan complejo en el mundo: ofrecer soja, energía y capital humano”._

_“Quiero ser el embajador anti-grieta. Aunque se rían, es precisamente lo que quiero ser”, comentó Stanley, recientemente llegado hace pocos meses. “Argentina elige a sus líderes, nosotros queremos trabajar con todos”, afirmó el diplomático, quien poco antes había escuchado al jefe de gobierno Horacio Rodríguez Larreta, pedir un “gobierno de coalición” para 2023, “sin el kirchnerismo ni la izquierda, porque con ellos no me voy a poner de acuerdo”._

El embajador argentino Jorge Argüello devolvió los elogios a Stanley y defendió el “diálogo” del Presidente con todos los países de la región. “Casi ninguno de los presidentes latinoamericanos habla con todos los presidentes, eso es algo que es detectado en la Casa Blanca”, dijo, al tocarle el turno de intervenir en la Amcham.

En su discurso, Argüello destacó la frecuencia de los contactos asiduos entre ambas embajadas y sostuvo que la relación con Marc Stanley y los Estados Unidos se puede definir como “excelente y mejorando”. “Nuestra relación está centrada en la confianza y en el afecto, pero que es capaz de coordinar acciones y cooperar en las taras que nos proponemos”, resaltó.

Desde el punto de vista de su rol, el embajador indicó que “cada cual defiende el interés que representa”, en el marco de la relación bilateral, “pero contribuyen algunas circunstancias” para el entendimiento.

“Los embajadores son los representantes personales de los jefes de Estado que los designan. Se da la característica de la cercanía de Marc con Joe Biden, con el hecho concreto de mi antiquísima relación personal que tengo con el presidente Alberto Fernández. Esto ayuda mucho”, comparó. Y dijo que ambos diplomáticos tienen “idénticas instrucciones” de sus respectivos mandatarios: “Elevar el nivel de intercambios de nuestros paises, potenciar la agenda bilateral y lograr mejores y mayores niveles de entendimiento”.

_“¿Hay ruidos y diferencias? Sí, claro que las hay, como en cualquier relación. Lo que importa es la capacidad de poder tomar los ruidos, aislar las diferencias y resolver las cuestiones. Construir un piso más alto para los próximos ruidos y diferencias para poder ser capaces de resolverlos”,_ expuso Argüello.

_“Estoy convencido que la historia va a colocar al presidente Alberto dentro de la tradición peronista, que es la de responder a los desafíos con decisión y pragmatismo. Y siento que esto es valorado por mis interlocutores en Washington, como es valorada la estabilidad institucional que tenemos (a diferencia de otros países de la región) y la capacidad de diálogo del Presidente que puede hablar con todos”,_ completó.