+++
author = ""
date = 2022-02-17T03:00:00Z
description = ""
image = "/images/5ff75d8d81869_671_377.jpg"
image_webp = "/images/5ff75d8d81869_671_377.jpg"
title = "IMPORTACIONES: DESDE EL GOBIERNO ASEGURAN QUE NO FALTAN DÓLARES Y CRITICAN A LAS EMPRESAS QUE LOS PIDEN PARA APROVECHAR LA BRECHA"

+++

##### **_Desde el ministerio de Economía y el BCRA salieron a frenar las fuertes críticas que llegaron desde distintas cámaras empresarias, incluyendo la Unión Industrial Argentina y la Cámara Argentina de Comercio, por las nuevas trabas a las importaciones._** 

Según aseguraron altas fuentes del equipo económico se trata de una percepción falsa: “En los primeros cinco días hábiles de febrero el volumen de importaciones fue de USD 270 millones diarios, o sea 13% más que el promedio de enero y 54% más que en el mismo período de 2021″.

Las quejas de muchas empresas pasaron por la decisión de la AFIP de modificar el cálculo de Capacidad Económica Financiera (CEF) de las empresas, que en muchos casos se redujo un 90%. Éste es el último filtro antes de poder importar y muchas se quedaron afuera por supuestamente no reunir los requisitos necesario. 

> “Las empresas que no pasan este filtro pueden pedir revisión. Del total de las bochadas, apenas dos docenas lo solicitaron, lo que es la mejor demostración de que estaban intentando importar fuera de su lógica comercial”, aseguran desde Economía.

Las razones de este comportamiento podrían ser dos, de acuerdo a la interpretación oficial: o estaban adelantando importaciones para aprovechar el “dólar barato” o nunca habían importado pero se tiraban el piletazo a ver si lo conseguían.

“Las que piden revisión son las que tienen una operación habitual y pueden demostrarlo. En esos casos, se revisa y en caso de estar todo en regla seguramente se las termina autorizando”, agregan.

El nuevo filtro de AFIP fue interpretado por las empresas como un freno para las importaciones, en momentos en que el Banco Central sufre una fuerte escasez de reservas líquidas para intervenir en el mercado. Por eso, el Gobierno habría decidido ganar un poco de tiempo hasta que ingresen a fin de marzo los dólares del FMI tras el acuerdo (a través de Derechos Especiales de Giro) y sobre a partir de mediados de abril las divisas de la cosecha gruesa.

Sin embargo, en el equipo económico relativizan totalmente esta “teoría” y aseguran que no habrá faltantes de dólares para la producción. Uno de los peligros de frenar procesos productivos por dificultades para acceder a divisas es que reduce la oferta de bienes y esto termina impulsando mayor inflación.

Por otra parte, la brecha cambiaria genera una recurrente vocación de las empresas por importar todo lo que pueden y lo antes posible para aprovechar un tipo de cambio mucho más atractivo. El dólar oficial cotiza aún abajo de los $110 cuando el contado con liquidación supera los $210. Esa diferencia al mismo tiempo retrae a los exportadores, que no quieren vender sus divisas tan baratas.

Las nuevas disposiciones de AFIP, sin embargo, pusieron otra piedra en el camino de las empresas y como mínimo demoraron algunas semanas los pedidos de importación. Al mismo tiempo, también desecharon aquellos que en apariencia buscaban más una ganancia financiera que comprar un insumo para la producción.

El peligro de cerrar todavía más el cepo a los importadores es sufrir una caída de la oferta de bienes. En ese escenario, las empresas podrían acelerar las remarcaciones ante la falta de mercadería, algo que el Gobierno busca evitar para que no se acelere aún más la inflación.

En las últimas jornadas el BCRA consiguió al menos comprar dólares marginalmente en el mercado para empezar a recomponerse. Sin embargo, aún está en un momento crítico, que el stock de reservas netas continuaría siendo negativo en alrededor de USD 300 millones.