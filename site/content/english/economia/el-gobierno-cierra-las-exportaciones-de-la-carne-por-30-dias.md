+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/carne-1.jpg"
image_webp = "/images/carne.jpg"
title = "EL GOBIERNO CIERRA LAS EXPORTACIONES DE LA CARNE POR 30 DÍAS"

+++
**_Tras la fuerte suba de precios de las últimas semanas, el Gobierno cerró las exportaciones de carne por un plazo de 30 días._**

Indicaron que la medida ya fue comunicada al Consorcio de Exportadores ABC, que hasta ahora ha sido el principal interlocutor del Gobierno para contener los precios, especialmente con la oferta de 10 cortes a precios populares en determinados días de la semana.

La carne vacuna es uno de los alimentos que más subió en los últimos meses y acumuló hasta abril un alza de 65% promedio en las carnicerías de la Ciudad y el conurbano bonaerense. El domingo por la noche, el presidente Alberto Fernández se había quejado de la escalada de valores y había anticipado que iba a ocuparse del tema con medidas que no quiso anticipar en ese momento.

#### Kulfas se hizo cargo de la medida

El ministerio de Desarrollo Productivo, a cargo de Matías Kulfas, difundió un comunicado por el cual su área se hizo prácticamente cargo de la medida, con el argumento de que “como consecuencia del aumento sostenido del precio de la carne vacuna en el mercado interno, el gobierno nacional decidió la instrumentación de un conjunto de medidas de emergencia tendientes a ordenar el funcionamiento del sector, restringir prácticas especulativas, mejorar la trazabilidad de las exportaciones y evitar la evasión fiscal en el comercio exterior”.

Indicaron que “la decisión fue comunicada este lunes por el Presidente a representantes del sector exportador de carnes nucleados en el consorcio ABC”. En el encuentro, según consignaron, participaron, además del Presidente y Kulfas; la secretaria de Comercio Interior, Paula Español; y los representantes del sector exportador de carnes Mario Ravettino, Carlos Alberto Rusech, Gustavo Kahal y Martín Costantini.

El consorcio de exportadores ABC informó días atrás que, de acuerdo con datos provisorios y parciales proporcionados por el INDEC, en los tres primeros meses de este año, se exportaron 207.000 toneladas de carne vacuna, 17% por encima del primer trimestre de 2020, mientras que el total facturado de US$614 millones, fue similar al verificado en igual lapso de 2020.

En ese marco, a principios de mayo, la Aduana había fijado nuevos valores de referencia para la exportación de carne vacuna, en un intento por aumentar los controles. Y hubo rumores de intervención en el mercado de Liniers, cuya desmentida no despejó la sensación de los operadores sobre el afán regulatorio del Gobierno.

Este lunes la medida llega en un momento en el cual en el Gobierno creen que deben tomar medidas más drásticas contra la inflación y consideran que la suba de precios en el mercado interno se debe a la presión de los valores internacionales. En ese sentido, aunque también se ha discutido mucho en los últimos días sobre el precio del pan y de la leche, entre otros productos de la canasta básica, la carne vacuna fue siempre el producto que estuvo en el eje de esos debates.

“El Estado tiene que volver a regular la carne y los alimentos, aunque no le guste a los gorilas, como en la época de Perón, pero adaptado a estos tiempos”, dijo este viernes el intendente de Pehuajó, Pablo Zurro, elegido por el Frente de Todos y de especial alineamiento con Cristina Kirchner.

Las declaraciones de Zurro se sumaron el contexto de haberse conocido la inflación de abril, que tuvo un índice de 4,1%, según el Indec informó este jueves. “No podemos dejar liberado el precio para que un argentino pague los precios de exportación, porque en vez de 800 pesos el kilo de asado va a valer 2.000 pesos”, opinó. De esta manera, se convirtió en otro funcionario K que puso en el “banquillo de los acusados” a los productores agroindustriales, en relación a la suba de precios de los alimentos, como ya se han expresado la Secretaria de Comercio Interior, Paula Español, entre otros.

La suba del precio del maíz, un insumo alimenticio clave para el ganado bovino, se ve como un gran condicionante especialmente desde los sectores más afines al kirchnerismo. Exponen que el cereal duplicó su valor internacional en el último año. Aunque los productores argentinos cobran el 40% de los US$300 dólares que se pagan por el maíz en Chicago, debido a los descuentos por retenciones y brecha cambiaria, dos factores que implican de hecho un desacople respecto del mundo.

Otro factor de peso en el enfoque del oficialismo es que la demanda internacional compite por la carne que consumen los argentinos. En los últimos años ha crecido mucho la exportación, liderada por la demanda china, y ese buen dato para la balanza comercial y el ingreso de divisas se observa como negativo desde el punto de vista de la mesa de los argentinos.

#### Cómo se compone el precio

Según un estudio económico de la Fundación Agropecuaria para el Desarrollo de la Argentina (FADA) los impuestos tienen más peso en el valor de los productos de la canasta básica que los granos. En ese marco, indicaron que la carne es el alimento más afectado por los impuestos ya que de $531 de precio promedio por kilo, $149 representan la carga tributaria. La cría representa $158 (30%), el feedlot $141 (27%), el frigorífico $35 (6%), la carnicería $47 (9%) y los impuestos $149 (28%). El maíz significa el 15% del precio del novillo y 11% del kilo de carne al mostrador.

“Cuando se analiza la participación de cada eslabón, es importante destacar que, si bien la cría del ternero representa el 30% del precio, hay que tener en cuenta que es la etapa de la cadena más costosa por el tiempo que se requiere”, detalló la economista Natalia Ariño.

#### El antecedente de 2006

En marzo de 2006, el gobierno de Néstor Kirchner cerró las exportaciones de carne, lo cual representó la irrupción en escena del ex secretario de Comercio Guillermo Moreno, que luego lideró el control discrecional de las ventas externas a través de los Registros de Operaciones de Exportación (ROE). Desde el sábado rige un mecanismo similar, ahora denominado Declaraciones Juradas de Exportaciones Cárnicas (DJEC), que desde la agroindustria ven como una amenaza a la transparencia comercial para el desarrollo de esta actividad.

Especialistas de la cadena de ganados y carnes coinciden que entre 2006 y 2015, por las restricciones a la exportación se perdieron 17.000 puestos de trabajo de la industria, 100 frigoríficos y 10 millones de cabezas de ganado, sumado a que el precio de la carne en la mesa de los argentinos subió varias veces más que la inflación.

Desde el Gobierno se destacó que esta vez “el período de 30 días podrá reducirse en el caso de que la implementación de las medidas antes señaladas -algunas ya adoptadas y otras a implementar en los próximos días- generen resultados positivos”.

Aclararon que “se habilitarán mecanismos de excepción para operaciones en curso”. Quizás por eso en el comunicado de Desarrollo Productivo se utilizaron las palabras restringir y limitar.

Menos probable parece ser que esa “flexibilidad” haya sido para evitar el papelón internacional de 2006, cuando Alemania había encargado carne argentina para vender durante la realización del Mundial de Fútbol en ese país.

El cierre de las exportaciones de aquel año hizo perder una gran oportunidad de promoción internacional al producto más emblemático de las exportaciones argentinas. Y quedó una mancha que en cuestiones de comercio exterior cuesta mucho remontar.