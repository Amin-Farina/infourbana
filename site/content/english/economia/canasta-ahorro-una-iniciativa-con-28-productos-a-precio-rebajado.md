+++
author = ""
date = 2021-05-06T03:00:00Z
description = ""
image = "/images/feria3.jpg"
image_webp = "/images/feria3.jpg"
title = "\"CANASTA AHORRO\": UNA INICIATIVA CON 28 PRODUCTOS A PRECIO REBAJADO"

+++

**_El Ministerio de Desarrollo Social lanzará el programa "Canasta Ahorro", una iniciativa que ofrecerá una canasta básica de 28 alimentos a precios rebajados en ferias populares de todo el país._**

La iniciativa, según se informó oficialmente, será presentada por el titular del ministerio, Daniel Arroyo, durante un acto que se hará a partir de las 10.30 en la Plaza Central de Florencio Varela, acompañado por el intendente local, Andrés Watson, y el director nacional de Políticas Integradoras, Rafael Klejzer.

#### Los productos

En el marco del Plan Federal de Ferias, se ofrecerán productos fundamentales como **_leche en polvo y en sachet, yogurt, queso crema, aceite mezcla, arroz largo fino, harina de trigo, leche en polvo, azúcar, yerba mate, puré de tomate, cacao, toallitas femeninas y lavandina_**.

Asimismo, en puestos de carnicería se ofrecerán cortes como **_asado, vacío, paleta, carne picada y bola de lomo para milanesas_**. En el rubro pescadería, se encontrarán **_filet de merluza y medallones de pescado_**.

También estarán a la venta **_lentejas, lentejón, avena, harina de maíz, harina de mandioca, harina de garbanzo, poroto colorado y miel_**.

"Son 28 productos que incluyen avena, leche, queso, carne y pescado, entre otros productos. Apuntamos a que el productor de pequeña escala le venda directamente al consumidor, sin intermediarios", dijo el miércoles Arroyo al explicar la iniciativa en una entrevista que concedió a El Destape Radio.

Como ejemplo, el funcionario indicó que esta canasta de alimentos incluirá "la harina a 38 pesos, el azúcar a 48, la leche a 55 pesos, y el yogur a 65 pesos".

"Empezamos por las ferias pero la idea es que la gente pueda solicitarlo y los propios productores les vayan enviando", indicó el ministro.