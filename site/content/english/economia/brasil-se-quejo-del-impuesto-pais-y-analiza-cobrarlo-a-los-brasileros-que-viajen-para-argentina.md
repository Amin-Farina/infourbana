+++
author = ""
date = 2021-09-03T03:00:00Z
description = ""
image = "/images/6m7pvnevrqppqwqf3pxkwwoag4-730x409.jpg"
image_webp = "/images/6m7pvnevrqppqwqf3pxkwwoag4-730x409.jpg"
title = "BRASIL SE QUEJÓ DEL IMPUESTO PAIS Y ANALIZA COBRARLO A LOS BRASILEROS QUE VIAJEN PARA ARGENTINA"

+++

**_Se trata del recargo de 30% que deben pagar los argentinos por hacer consumos en el exterior. El ministro de Turismo del país vecino afirmó que la imposición no es democrática y criticó su vigencia para los viajes a países del Mercosur._**

Las diferencias entre los gobiernos de Argentina y Brasil no son nuevas y, ante la reapertura de fronteras, surgen nuevas asperezas. El último capítulo de las rivalidades entre los funcionarios de Alberto Fernández y los de Jair Bolsonaro tiene al turismo en el centro de la escena.

“Yo les diría a los argentinos que quieren venir a Brasil que le pidan al Presidente de la Argentina que elimine el 30% que le cobra al que quiere viajar al exterior. No es democrático imponer 30% a los ciudadanos que quieran ir a otro país, principalmente a los del Mercosur”, dijo el ministro de Turismo brasileño, Gilson Machado Neto, en una entrevista con La Derecha Diario.

El funcionario advirtió que es posible que Brasil responda con una imposición similar. “Nosotros estamos evaluando también cobrarle ese 30% a los brasileños que viajen a la Argentina. ¿Quién se va a beneficiar? Chile, Paraguay, Uruguay... Entonces, que revisen esa posición de cobrarle el 30% a los gastos con tarjeta de crédito en pasajes para venir a Brasil”, insistió.

Según datos oficiales, alrededor del 40% de los turistas que llegaban a Brasil cada año -antes de la pandemia- eran argentinos. A la vez, entre los ciudadanos residentes en la Argentina que salían del país cada mes a través de los aeropuertos internacionales, cerca de un 30% se dirigían a Brasil.

El impuesto PAIS fue aprobado por el Congreso de la Nación en diciembre de 2019 e impone un recargo de 30% a las compras en moneda extranjera que se hagan con tarjetas de crédito y a los dólares que se adquieran para atesoramiento. Nació en ese momento el “dólar solidario”, que incluye el valor del minorista junto con el recargo.

En septiembre del año pasado, en tanto, se sumó otro 35% adicional a los gastos con tarjetas en el exterior. Esta vez en concepto de “adelanto de Impuesto a las Ganancias”. Ese monto luego se puede imputar en la liquidación del tributo o pedir la devolución a la AFIP, pero encarece el viaje al momento de realizarlo. Así, el dólar para los turistas argentinos es un 65% más caro que el que se ve en las pantallas de cotización oficiales.

No es la primera vez que el funcionario de Bolsonaro se queja del recargo que pesa sobre los viajeros argentinos. Ya había expresado su preocupación en mayo, en ocasión de la Feria Internacional de Turismo. Allí Machado Neto le pidió a su par argentino, Matías Lammens, que eliminaran el recargo.

También se reunió en agosto con el embajador argentino en Brasil, Daniel Scioli. Según informó el excandidato a presidente en sus redes sociales, él y Machado Neto coincidieron en que el turismo de proximidad entre Argentina y Brasil, será la base para retomar los viajes internacionales con un potencial de 11 millones de turistas brasileños listos para viajar.

Sin embargo, por ahora el Gobierno parece más interesado en promover el turismo interno, que no gasta dólares en el exterior. Para ello se relanzó “Previaje 2″ y la cartera de Lammens estudia hacer promociones agresivas para atraer viajeros internacionales, que podrían incluir la bonificación de pasajes en Aerolíneas Argentinas. Los brasileños, que antes de la pandemia representaban un 20% de los turistas que llegaban al país, podrían aprovechar estos beneficios.