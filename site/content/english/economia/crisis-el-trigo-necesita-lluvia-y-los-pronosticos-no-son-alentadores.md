+++
author = ""
date = 2021-08-02T03:00:00Z
description = ""
image = "/images/trigo770.jpg"
image_webp = "/images/trigo770.jpg"
title = "CRISIS: EL TRIGO NECESITA LLUVIA Y LOS PRONÓSTICOS NO SON ALENTADORES"

+++

**_La siembra de trigo alcanzó el 99% del área nacional proyectada, y se espera una producción récord superior a los 20 millones de toneladas, según el último informe de la Bolsa de Cereales de Buenos Aires (BCBA), pero las bajas temperaturas y las limitaciones hídricas podrían afectar las condiciones del cultivo. Y los pronósticos a largo plazo no son alentadores._**

Según adelantó el meteorólogo Leonardo De Benedictis, habrá que esperar hasta septiembre para ver caer buenos acumulados que ayuden al cereal.

Durante esta primera semana de agosto habrá tiempo estable en casi todo el territorio nacional, a excepción de la región sur, principalmente en el sector cordillerano, donde podrían generarse nevadas aisladas.

“Durante la primera quincena del mes de agosto no se esperan lluvias y habrá tiempo estable en todo el territorio nacional tanto en la franja central como en el norte argentino”, afirmó el profesional.

En cuanto a la tendencia climática de 8 a 14 días, De Benedictis explicó que existirá un “pequeño cambio” pero que “no será demasiado significativo”. “Se observará algún tipo de actividad sobre la zona del litoral y parte del NEA, que podrían registrar algunos chaparrones o tormentas, pero serán fenómenos muy escasos”, detalló.

En tanto, en el sur del país podrían caer entre 3 y hasta 10 milímetros (como máximo), mientras que en la franja central (la región cuyana, La Pampa, Córdoba y casi toda la provincia de Buenos Aires) no se esperan precipitaciones. Esta situación de déficit hídrico recién se empezaría a revertir a principios del mes de septiembre.

##### LA SIEMBRA ESTÁ LISTA

Luego de concluir la siembra del cereal a nivel nacional, los productores de todo el país esperan lluvias que ayuden al buen crecimiento del cultivo de invierno.

Según destacó Martín López, especialista de la BCBA se necesitan, al menos, registros de 50 milímetros para obtener un correcto desarrollo del cereal que además ayudarían a la evolución de la precampaña de soja y maíz.

“Tanto el trigo como la cebada requieren lluvias durante agosto porque es un mes clave para el inicio de la etapa reproductiva. Para recomponer los perfiles se necesitan al menos entre 50 y 60 milímetros en el centro y sur del país. Venimos de un otoño e invierno relativamente seco y esperemos que a principios de primavera haya más precipitaciones para no generar mermas en el rendimiento en provincias como Buenos Aires y Córdoba”, destacó López en diálogo con Canal Rural.

Vale recordar que según la entidad bursátil, durante la última semana, el avance de un frente frío sobre el área agrícola nacional “provocó daños estructurales en los cuadros más avanzados del norte del área agrícola y en los lotes tardíos”.

Según el informe, la condición del cultivo se vio más afectada en aquellos sectores con mayores limitantes hídricas y, ante la ausencia de pronósticos de lluvias, podría comprometerse su recuperación.