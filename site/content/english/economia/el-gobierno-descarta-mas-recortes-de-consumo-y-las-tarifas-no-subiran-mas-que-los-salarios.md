+++
author = ""
date = 2022-01-17T03:00:00Z
description = ""
image = "/images/facturas-gas-luz-aguajpg-1.jpg"
image_webp = "/images/facturas-gas-luz-aguajpg.jpg"
title = "EL GOBIERNO DESCARTA MÁS RECORTES DE CONSUMO Y LAS TARIFAS NO SUBIRÁN MÁS QUE LOS SALARIOS"

+++

#### **_"Hubo récord de uso de energía. El Presidente tomó la decisión del asueto porque necesitábamos bajar el consumo de GBA. Logramos mantener el sistema", dijo el secretario de Energía, Darío Martínez._**

El secretario de Energía, Darío Martínez, descartó que esta semana sea necesario aplicar nuevos recortes de consumo de energía a grandes usuarios u organismos públicos, y señaló que la idea del Gobierno es que las tarifas de electricidad no evolucionen por encima de los salarios.

"No estaremos ante la situación de reducir el consumo de energía en los próximos días", dijo Martínez a Radio 10, en referencia a la reducción del consumo que solicitó el Gobierno a los grandes usuarios y el asueto en dependencias públicas el jueves y viernes pasados, ante el pico de demanda que hubo en el país por una ola de calor con temperaturas récord.

"Lo que sucedió en esos días fue una combinación de cuatro factores que no se habían dado nunca: primero la reactivación, que viene demandando energía a un nivel importantísimo; eso, combinado con una ola de calor histórica, nuestra región fue la más calurosa del planeta, tercero la baja hidroactividad por la sequía y, cuarto, la desinversión de la gestión anterior del macrismo. La combinación de esos cuatro factores desencadenó en la tensión del sistema", sostuvo el funcionario nacional.

> "Firmamos el plan AMBA 1, que es esencial para resolver la falta de inversión de los últimos años. Aún con pandemia sumamos muchísimas líneas de energía"

"Hubo récord de uso de energía. El Presidente tomó la decisión del asueto porque necesitábamos bajar el consumo de GBA. Logramos mantener el sistema".

"Fueron decisiones que se tomaron pero que dieron resultados para preservar a los hogares y que el sistema no colapse", indicó.

> En cuanto a inversiones del sector, Martínez señaló que "el otro día firmamos el plan AMBA 1, que es esencial para resolver la falta de inversión de los últimos años. Aún con pandemia sumamos muchísimas líneas de energía", acotó.

##### Las tarifas y los sueldos

"Venimos de una experiencia donde se dieron tarifazos y había cortes. Con eso no se resuelve. Las tarifas no van a aumentar por encima de los sueldos", agregó Martínez a Radio 10.

"La idea de nuestro proyecto junto con el presidente es que las tarifas no evolucionen por encima de los salarios. No explica solo el esquema de tarifas el problema que tenemos. Hay una falta de inversión de la época de macrismo", añadió.

##### Rol de las distribuidoras

Respecto al rol de las distribuidoras, el secretario de Energía observó una recuperación del servicio "mucho más lenta en Edesur que en Edenor, lo cual tiene que ver con la cantidad de cuadrillas que se ponen a resolver los problemas y con que el sistema es más endeble en la zona sur".

> "El ENRE está haciendo su trabajo y los vecinos se merecen tener un sistema que ande correctamente. Hay que sumar mayores controles y las sanciones que se tienen que dar", completó.