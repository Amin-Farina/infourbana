+++
author = ""
date = 2021-09-02T03:00:00Z
description = ""
image = "/images/comercio-1.jpeg"
image_webp = "/images/comercio.jpeg"
title = "LAS EXPORTACIONES ARGENTINAS HACIA BRASIL ALCANZARON EL MÁXIMO NIVEL EN TRES AÑOS"

+++

**_Las exportaciones argentinas hacia Brasil alcanzaron el registro más elevado de los últimos tres años, resultado que responde al reflejo de la recuperación de ambas economías, destacaron diferentes informes._**

“Las exportaciones a Brasil totalizaron USD 1.003 millones, mostrando un alza de 59,9% interanual, siendo agosto el octavo mes consecutivo de crecimiento, al tiempo que nuestras compras desde este origen saltaron 54,8% interanual a USD 1.119 millones, sexto mes consecutivo en alza”, indicó la consultora Abeceb.

Respecto a agosto 2019 (pre-pandemia), destacó que crecieron 29% las importaciones de Brasil, mientras que las exportaciones argentinas mostraron una suba algo menor (+21%).

“En los primeros ocho meses del año, las exportaciones a Brasil acumulan un alza de 45,8% promedio, totalizando USD 7.153 millones, mientras que nuestras importaciones muestran un crecimiento del 53% a USD 7.830 millones”, añadió.

Así, el intercambio de bienes con Brasil consolida su recuperación tras más de dos años de fuerte retroceso. “El flujo comercial (exportaciones más importaciones) con nuestro principal socio se elevó a USD 2.122 millones en agosto (+57,2% i.a.), creciendo por octavo mes consecutivo y ubicándose en su mayor valor desde agosto 2018”, analizó la consultora.

Y agregó que de esta forma, “se recuperó completamente del impacto de la cuarentena de 2020 al acumular una suba del 49,5% i.a. en 2021 y del 7,1% frente al mismo lapso de 2019”.

En tanto, la Cámara Argentina de Comercio y Servicios (CAC) destacó que el saldo comercial para Argentina arrojó un déficit de USD 116 millones, marcando un retroceso de 29,9% mensual.

Asimismo, aclaró que “no obstante, se observa un avance del 20,9% en la comparación interanual, ya que en igual mes de 2020 se verificó un déficit de USD 96 millones, de acuerdo con los datos publicados por el Ministerio de Economía de Brasil”.

En ese sentido, y en línea con Abeceb, indicó que el comercio bilateral entre Argentina y Brasil fue de USD 2.122 millones en el octavo mes del año, un 57,2% superior al valor obtenido en 2020 y el mayor valor en los últimos tres años.

“La suba de las exportaciones de Argentina hacia Brasil (60%) correspondió a Vehículos de motor para transporte de mercancías, Trigo y centeno sin moler y Vehículos de turismo, mientras que el incremento en las importaciones argentinas (54,8%) se explicó principalmente por Minerales de hierro y sus concentrados, Piezas y Accesorios de vehículos automotores y Vehículos de turismo”, detalló la CAC.

Con relación a los destinos, la Argentina se posicionó en tercer lugar entre los mayores proveedores de Brasil, detrás de China, Hong Kong y Macao (USD 4.052 millones) y Estados Unidos (USD 4.005 millones). A su vez, entre los principales compradores de Brasil, Argentina también se ubicó tercera, detrás de China, Hong Kong y Macao (USD 9.464 millones) y Estados Unidos (USD 2.770 millones).

En tanto, la CAC indicó que en lo que va del año, el flujo de comercio entre ambas economías se incrementó un 49,5% interanual al alcanzar USD 14.982 millones, mientras que el déficit comercial de Argentina mostró un crecimiento de 221%, al situarse en 677 millones de dólares.

En otro orden, señaló que en el segundo trimestre del año, el PBI de Brasil se contrajo un 0,1% respecto al trimestre anterior, aunque mejoró un 12,4% en la comparación interanual.

A su turno, la consultora Ecolatina también analizó los resultados del intercambio y sostuvo que “el aumento en el comercio se dio tanto por el lado de las importaciones como de las exportaciones, que crecieron ambas a ritmos similares”.

“Considerando que el comercio global se vio golpeado por la pandemia en 2020, es más adecuado comparar el intercambio bilateral con el de 2019, año que no se vio afectado por ella”, puntualizó.

En ese sentido, manifestó que tanto las compras como las ventas ya superan sus valores de entonces. Al comparar con agosto de ese año, las exportaciones fueron 21% superiores y las importaciones 29% mayores.

“El crecimiento del intercambio comercial es el reflejo de la recuperación de ambas economías. Sin embargo, también implica para nuestro país un mayor uso neto de divisas, dado que la balanza comercial con Brasil es tradicionalmente deficitaria, más aún considerando que el crecimiento proyectado es mayor para Argentina (+7,0% anual) que para Brasil (+5,2%)”, resaltó.

##### PERSPECTIVAS

Para Abeceb, se espera para 2021 una profundización del déficit comercial con Brasil, producto de un mayor dinamismo en las importaciones que en las exportaciones. “Para las ventas externas, vemos un fuerte crecimiento en las exportaciones del sector automotriz y de las manufacturas de origen industrial (MOI) en general, en línea con recuperación de la producción industrial de Brasil (de acuerdo con el consenso de mercado, este segmento crecería 6,4%, por encima del 5,3% promedio de la economía). De esta forma, las exportaciones totales superarán los niveles pre-pandemia”, remarcó.

En tanto, destacó que por el lado de las compras, vehículos y autopartes serían los rubros más dinámicos, mientras que las importaciones totales se ubicarían bien por encima de los niveles pre-pandemia.

Y concluyó al afirmar que “una brecha cambiaria más elevada que la vigente en la primera parte del año junto a mayores expectativas de devaluación post-elecciones podría alentar un adelantamiento de las compras al exterior”.