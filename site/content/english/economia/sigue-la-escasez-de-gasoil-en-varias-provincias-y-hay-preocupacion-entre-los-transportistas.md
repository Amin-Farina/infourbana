+++
author = ""
date = 2022-03-30T03:00:00Z
description = ""
image = "/images/foto-gasoil.webp"
image_webp = "/images/foto-gasoil.webp"
title = "SIGUE LA ESCASEZ DE GASOIL EN VARIAS PROVINCIAS Y HAY PREOCUPACIÓN ENTRE LOS TRANSPORTISTAS"

+++
#### Desde el sector dijeron que el faltante de combustible se siente de modo particular en las zonas fronterizas por el hecho de que consumidores de otros países pagan más que la tarifa oficial y en dólares. Los estacioneros advirtieron por un mayor desabastecimiento en abril y reclamaron medidas al Gobierno

La Argentina continúa sufriendo el desabastecimiento parcial de gasoil por el desfase entre el precio de venta y el de importación, lo que lleva a las petroleras a racionar las entregas a las estaciones de servicio, que aplican cupos de venta de hasta 15 litros por vehículo. Se trata de una situación que además pone en riesgo la cosecha de granos como el trigo y la soja.

Desde el sector transportista dijeron que el faltante de combustible se siente de modo particular en las provincias fronterizas, dado que las estaciones de servicio dan prioridad de carga a camiones de Bolivia -en Jujuy y Salta-, de Paraguay -en Chaco, Corrientes y Misiones-, de Uruguay –en Entre Ríos-, de Brasil en todo el corredor de la Ruta 14, y de Chile, en Mendoza, “por el hecho de que pagan más que la tarifa oficial y en dólares”.

En ese sentido, detallaron que históricamente el precio del litro de gasoil en América latina osciló entre USD 1,10 y USD 1,20, mientras en la Argentina el litro de gasoil, cuando se consigue, ronda los $120 promedio, en Brasil cuesta $270; en Uruguay $220; y en Paraguay $160.

Por su parte, YPF informó a través de un comunicado que “está garantizado el abastecimiento de gasoil para los consumidores del canal mayorista”.

“En especial, a través de sus más de 100 distribuidores YPF AGRO con presencia en todo el país, la compañía garantiza el acceso al gasoil a los productores agropecuarios”, añadió.

La compañía estatal remarcó que durante el primer trimestre de este año, según cifras oficiales, el consumo de gasoil grado 2 tuvo un crecimiento del 12% aproximadamente. En tanto, agregó que el canal mayorista comenzó a registrar una caída en sus ventas del 2,5% promedio, “lo que indica una leve migración de consumidores mayoristas al canal minorista y complejiza los requerimientos logísticos”.

YPF es el principal proveedor de gasoil del país con el 56% del mercado. En un contexto internacional de escasez de gasoil, la compañía indicó que se encuentra haciendo “un importante esfuerzo logístico para llegar con gasoil a todos sus puntos de venta, y garantizar el abastecimiento del sector productivo”.

No obstante, desde el sector transportista indicaron que los principales problemas de abastecimiento que hasta ahora se manifiestan de modo intermitente se dan a lo largo de los grandes corredores viales como en las rutas 34 y 9 y en la zona de Gran Rosario. “No hay una zona que esté continuamente sin combustible. Por ejemplo en Cañuelas cuando se quedan sin combustibles a las seis u ocho horas vuelven a cargar. En muchas estaciones hay horarios de carga”, indicaron las fuentes.

Mañana la Federación Argentina de Entidades Empresarias del Autotransporte de Cargas (Fadeeac) realizará una reunión con más de 40 entidades de la actividad para tratar específicamente este problema y establecer los pasos a seguir.

Sin embargo, señalaron que el faltante de gasoil no está oficializado por parte de las estaciones. “Cuando los camiones llegan no les dicen que no hay combustible. Les dicen que hay problemas con el posnet o que no hay servicio o que se cayó el sistema. Hay distintos artilugios para no oficializar el faltante”, destacaron.

En tanto, una empresa del sector petrolero dijo a este medio que el primer problema es que los productores de biodiesel no entregan, lo venden como aceite de soja, situación que golpea al sector. “Hoy, la producción local de combustibles es a pérdida, nunca se recuperó la brecha que se armó desde 2019. El año pasado el aumento fue la mitad de inflación y no cubrió siquiera la suba del crudo. Y ahora tenemos la guerra, que complica la importación”, describieron.

Según la empresa, hoy importar combustible hace que se pierdan 350 dólares por metro cúbico. Y añadieron que por cada litro de combustible importado que despacha una estación de servicio pierde entre 38 y 39 pesos.

“Es un tema de demanda asociada al precio, nadie quiere vender a pérdida y así las empresas limitan su venta a las que tienen contrato, sus estaciones y sus distribuidores. El resto quedan afuera. Esto es un tema de precios, se mire por donde se mire”, destacaron.

En ese marco, la Confederación de Entidades del Comercio de Hidrocarburos y Afines de Argentina (Cecha), que reúne a 4.500 propietarios de estaciones de servicios de todo el país, advirtió en un comunicado sobre el desabastecimiento y “la falta de respuesta” tanto del secretario de Energía Darío Martínez como del de Comercio Interior, Roberto Feletti. Asimismo, alertó que “el atraso en los precios jaquean la provisión de combustibles y ponen en peligro toda la cadena productiva”.

Según la entidad, el atraso del 32% por ciento en los precios es lo que llevó a que hoy existan las primeras señales de desabastecimiento en combustibles claves como gasoil y el diesel. “Es urgente que el Gobierno convoque a todos los actores del sector y busquemos soluciones para garantizar su normal funcionamiento”, remarcó en el comunicado.

De acuerdo a Cecha, pese al “leve repunte” de las ventas de combustibles durante febrero y la incipiente suba de precios, la Argentina está “a las puertas de una crisis” que podría afectar a toda la cadena productiva hasta incidir en los usuarios.

“La venta de 15 litros por vehículo se da en algunas localidades. El problema es que si viene la cosecha se necesitan vender más litros. Cuando arranca la economía, se vende más”, dijo a Infobae el titular de CECHA, Gabriel Bornoroni. En ese sentido, si bien afirmó que no hay colas para cargar en las estaciones de servicio, no descartó que esa situación se de en abril. “Los estacioneros decimos que si todo sigue como está a fines de abril vamos a estar con las colas”, advirtió.

Asimismo, Bornoroni cuestionó al Gobierno por no dar una solución al problema. “Pedimos reunión con Darío Martínez y Roberto Feletti, pero ninguno de los dos nos atendió”, se quejó el dirigente en relación al pedido a los secretarios de Energía y Comercio Interior.

Productores agropecuarios y transportistas dijeron que las estaciones de servicio empezaron a poner cupos a la cantidad de gasoil que venden por vehículo en localidades como Henderson, Daireaux, Carlos Casares, General Pico, Catriló y Pellegrini. La provincia de Mendoza, mientras tanto, se vio particularmente afectada. San Luis, General Roca en Río Negro y Garín en la provincia de Buenos Aires fueron otros puntos en los que se reportaron cupos de unos 50 litros por camión.

De acuerdo a los datos del “informe de expendios de combustibles” de febrero de 2022, el volumen total de combustibles líquidos vendido por los minoristas se incrementó en un 2,1% con subas en 16 de las 24 jurisdicciones provinciales, aunque se mantiene un 4,7% por debajo del nivel de abril de 2018, situación que se registró en 18 jurisdicciones.

Pero según Cecha, la crisis no se explica únicamente por la recesión y la pandemia; además de estos factores, desde el gremio sostienen que entre las causas se encuentran la fuerte inflación, el aumento de costos, las discusiones paritarias con un techo alto y el atraso en los precios en los surtidores. Todo esto, generó un “combo explosivo que cada día drena las posibilidades de supervivencia de las 5 mil estaciones que operan en el país”, según definieron en el comunicado.

##### Vaca Muerta también se ve afectada

Los problemas de abastecimiento del gasoil que existen en casi todo el país están impactando en la región clave de Vaca Muerta. Según el diario Río Negro, “un relevamiento de la Cámara de Expendedores de Combustibles de Río Negro y Neuquén detectó puntos desabastecidos desde hace tres días y envíos muy por debajo de los cupos autorizados. Desde la entidad aseguraron que las empresas de servicios petroleros de Vaca Muerta son las más perjudicadas”.

El matutino informó que el titular de la Cámara, Carlos Pinto, “explicó que en el caso de una cadena de estaciones de servicio de la ciudad de Neuquén, este mediodía había una total falta de gasoil, tanto en el tipo Premium como en el común”.

Por otro lado, “en las demás estaciones de servicio de la capital neuquina la constante es que las refinerías envíen un menor cupo de gasoil de tipo común que el previamente fijado por las refinerías”, se indicó.

Mientras tanto, y de cara a la mayor demanda por la cosecha de granos, se espera que el Gobierno tome alguna medida para destrabar el conflicto y que no falte combustible.