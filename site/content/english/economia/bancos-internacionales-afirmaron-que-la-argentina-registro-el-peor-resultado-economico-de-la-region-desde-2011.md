+++
author = ""
date = 2022-02-09T03:00:00Z
description = ""
image = "/images/national_bank_of_argentina_-92205.jpg"
image_webp = "/images/national_bank_of_argentina_-92205.jpg"
title = "BANCOS INTERNACIONALES AFIRMARON QUE LA ARGENTINA REGISTRÓ EL PEOR RESULTADO ECONÓMICO DE LA REGIÓN DESDE 2011"

+++

#### **_Un informe del Instituto de Finanzas Internacionales analiza las cifras del PBI de la última década, aunque sin tomar en cuenta a Venezuela, que tuvo una recesión más profunda; cuáles son los temores ante el acuerdo con el FMI_**

La Argentina presentó el peor resultado económico de la región en la última década y no tiene un panorama halagüeño por delante, según el think tank que agrupa a los principales bancos del mundo.

El economista jefe del Instituto de Finanzas Internacionales (IIF, según su sigla en inglés), Robin Brooks, señaló cómo evolucionó el producto bruto interno en un grupo de países de América latina, aunque sin mencionar el caso venezolano, que, al igual que la Argentina hasta 2016, tiene cifras consideradas poco confiables. El IIF espera para este año una mayor presión inflacionaria y un crecimiento económico débil.

En particular, explicó que Perú acumuló un crecimiento del 39%, Chile del 33%, Colombia también del 33%, Uruguay del 15%, México del 14%, Ecuador del 13% y Brasil del 3 por ciento. En cambio, Argentina cayó un -3% en la última década, según los datos elaborados por Brooks con los economistas Sergi Lanau y Martín Castellano.

Los datos de Venezuela, según mediciones de organismos multilaterales y de consultoras privadas, exhibirían un resultado peor que el de la Argentina, ya que desde 2011 el país creció ese año 4,2%, en 2012 5,6% y en 2013 1,3%, pero luego cayó -3,9% en 2014; -6,2% en 2015; -17% en 2016; -15,7% en 2017; -19,6% en 2018; -30,7% en 2019; y -27,4 en 2020.

El IIF tiene una visión crítica sobre la situación argentina en los últimos años. Días atrás, Lanau advirtió en Twitter que “Argentina continuó perdiendo reservas en enero y será casi imposible reconstruir las reservas de forma significativa durante mucho tiempo”.

Los expertos creen que, más allá de la firma del programa con el FMI, la clave será el compromiso que exhiba el Gobierno para cumplirlo, un concepto en el que coinciden con el staff del organismo.

En este sentido, creen que la suba de tarifas de los servicios públicos y una mayor devaluación que en 2021 colocarán mayor presión sobre la inflación, que podría mantenerse muy alta pero sin descarrillarse si se aplican las metas acordadas con el Fondo. En cuanto al PBI, entienden que el crecimiento de este año será muy leve por las restricciones al movimiento de capitales, que se verán compensados por un contexto externo favorable.

En este sentido, Lanau consideró que “el programa de ‘control de daños’ del FMI seguirá adelante y bastará para elevar un poco las valoraciones de los bonos con respecto a los niveles actuales”.

A fines del 2021, Lanau expresó en diálogo con Bloomberg que “es muy improbable que el FMI exija medidas de shock y un tipo de cambio flotante”, en sintonía con el enfoque “pragmático” con el que el organismo ha denominado el acuerdo que busca firmar con la Argentina, según las palabras de la vicedirectora gerente, Gita Gopinath.

En tanto, el economista Martín Castellano del IIF señaló en Twitter que “Argentina podría recuperar más de 4.000 millones de dólares de los pagos de amortización efectuados recientemente al FMI. Este impulso podría ayudar a cubrir parcialmente los déficits de financiación externa y a hacer frente a un desajuste del tipo de cambio que estimamos en más del 20%. Sin embargo, la clave es un compromiso sostenido y generalizado con el nuevo programa”.

“Una consolidación fiscal moderada y un ambicioso retroceso de la financiación monetaria en 2022-23 supondrían una fuerte emisión de deuda en un mercado financiero local poco profundo, lo que añadiría presión a las tasas de interés. La deuda global no es una opción, y será difícil conseguir más financiación oficial”, advirtió Castellano.

##### Ajuste ineludible

Previamente, el IIF había advertido que la Argentina solo podrá estabilizar su economía con un programa de ajuste fiscal y monetario apoyado con dólares “frescos”, más allá de los fuertes pagos que debe hacerle al Fondo en 2022 si no llegara a un acuerdo de refinanciación.

“Al igual que en el ejemplo de Argentina, la asignación de los derechos especiales de giro (DEGs) no altera fundamentalmente la necesidad de apoyo del FMI en forma de un marco plurianual, pero ha llevado a una menor sensación de urgencia. Es probable que se produzca una dinámica similar en Sri Lanka, donde los ajustes inevitables ahora pueden llegar tarde”, expresó el IIF.

Otro estudio de la entidad advirtió que la Argentina está entre los países más afectados por el pase a precios de una devaluación, luego de Vietnam, República Dominicana, Ucrania y Honduras. En este sentido, por cada punto de aumento del dólar, la inflación salta 0,5 puntos porcentuales.

Para este año, el Gobierno proyectó un crecimiento del 4% y una inflación del 33% -aunque algunos ministros ya reconocen que el aumento de precios rondará el 40%-, mientras que el FMI y los analistas privados creen que el PBI rebotará un 3 por ciento, luego del 10% del 2021 y una caída similar en 2020. En cuanto a la inflación, los analistas prevén en promedio un aumento del 55%, aunque algunos se estiran más allá del 60% por el aumento de la devaluación y de las tarifas de los servicios públicos previsto para este año.

El IIF, presidido por Axel Weber, representa la visión del sector privado financiero global y ha marcado en numerosas oportunidades sus diferencias con las políticas de la Argentina, sobre todo en lo que se refiere a la cuestión del déficit fiscal, el dólar y la emisión monetaria.