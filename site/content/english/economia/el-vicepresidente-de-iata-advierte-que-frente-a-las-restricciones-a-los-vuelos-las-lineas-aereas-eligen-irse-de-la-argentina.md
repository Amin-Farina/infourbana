+++
author = ""
date = 2021-07-01T03:00:00Z
description = ""
image = "/images/peter-cerda-iata-17-e1523362343646-1.jpg"
image_webp = "/images/peter-cerda-iata-17-e1523362343646.jpg"
title = "EL VICEPRESIDENTE DE IATA ADVIERTE QUE \"FRENTE A LAS RESTRICCIONES A LOS VUELOS, LAS LÍNEAS AÉREAS ELIGEN IRSE DE LA ARGENTINA\""

+++

**_El vicepresidente Regional para América de la Asociación de Transporte Aéreo Internacional (IATA), Peter Cerdá, dijo que las compañías aéreas eligen irse del mercado argentino por las complicaciones a las que las somete las medidas sanitarias sorpresivas._** 

“Poner un tope no va a ayudar a la situación y pone en desventaja a los pasajeros que necesitan volver al país”, dijo.

La Decisión Administrativa 643/2021 de la semana pasada redujo de 2.000 a 600 la cantidad de pasajeros que pueden ingresar por día desde el exterior. La decisión, fundamentada en el objetivo de demorar el ingreso al país de la variante delta del coronavirus Covid-19, generó cancelaciones masivas de vuelos, dejó a miles de argentinos varados en el exterior y causó malestar entre el sector aerocomercial.

El funcionario que representa a las empresas aéreas de la región comentó en una entrevista a radio Continental que esperaban un encuentro con el Gobierno para intentar operar en mejores condiciones, pero que la reunión tuvo que ser postergada.

“Íbamos a tener ayer una reunión con el gobierno y pospusimos la reunión hasta que pueda estar presente el jefe de gabinete, Santiago Cafiero”, dijo Cerdá.

“Tenemos que ajustar los horarios de los vuelos. 1.400 pasajeros desde el comienzo de esta semana no pueden volver a la Argentina y las líneas aéreas no tienen posibilidad de ayudar”, se lamentó el ejecutivo.

El representante de IATA, además, argumentó que las medidas que se tomaron en la Argentina y que implican un cupo de ingreso de 600 personas al día por vía aérea no se parecen a las que se decidieron en otras partes del mundo.

“Los países a nivel mundial están intentando controlar la pandemia, que es algo lógico. Las líneas aéreas están operando sin ningún tipo de control, lo que están controlando son a los pasajeros”, comentó.

“Es parecido a lo que hace la Argentina. El problema es que no se les hace un seguimiento riguroso a los pasajeros. Poner un tope no va a ayudar a la situación y pone en desventaja a los pasajeros que necesitan volver al país. Este cupo a nivel logística es muy difícil. La línea aérea se tiene que preparar para mover pasajeros. Ese cambio sin ningún tipo de coordinación nos afecta a todos”, agregó.

“Ante estas complicaciones, las líneas aéreas eligen irse del mercado argentino. Tenemos que establecer un plan para repatriar a los pasajeros. Tenemos que incrementar la capacidad de vuelos, pasamos de tener 9 a 2 vuelos por día. La prioridad es traer a los pasajeros y después poco a poco abrir el mercado para que más pasajeros puedan volver a su país. No podemos trabajar de esta manera. La Argentina está implementando una medida que es única en el mundo”, concluyó Cerdá.