+++
author = ""
date = 2022-04-13T03:00:00Z
description = ""
image = "/images/inflacion-1.jpeg"
image_webp = "/images/inflacion.jpeg"
title = "SIN MANERA DE CONTROLARLA, LA INFLACIÓN SEGUIRÁ MUY ALTA EN LOS PRÓXIMOS MESES"

+++
#### Abril apunta a cerrar arriba del 5% y empieza a quedar corto el pronóstico de 60% para el 2022. Se acelera la indexación de la economía y las paritarias no tienen techo

El dato de inflación de marzo que divulgará hoy el INDEC marcará el pico máximo más alto del gobierno de Alberto Fernández. Según adelantó el ministro Martín Guzmán se ubicará cómodamente por encima del 6% y con pronóstico reservado hacia adelante, en una economía dominada por la indexación y más factores que seguirán presionando sobre los precios.

El economista y ex titular del INDEC denominó lo sucedido en marzo como “la tormenta perfecta”, porque se alinearon los planetas para acelerar dramáticamente la inflación. La invasión de Rusia en Ucrania provocó un salto mundial en los precios de los alimentos y la energía que también repercutió en la Argentina.

Pero además confluyeron distintos aspectos que aceleraron la tendencia alcista que ya se había observado en el primer bimestre del 2022. Suba de prepagas, recambio de temporada de indumentaria, incremento de 11% promedio en combustibles y cuotas de colegios privados, entre los ítems más destacados empujaron el fuerte salto inflacionario del mes pasado.

Hubo otros aspectos que también colaboraron para que se haya dado semejante salto de precios. El Central aceleró la suba del dólar arriba del 3% y además se registraron problemas para importar insumos, lo que también provoca mayores remarcaciones en el sector industrial.

Pero lo más impactante será una vez más la fuerte suba del rubro alimentos y bebidas, que en febrero había subido 7,5% y ahora quedaría incluso por encima de este ajuste. El impacto sobre el nivel de pobreza será inevitable cuando se conozcan las cifras del primer semestre. La inflación está licuando los salarios, pero también jubilaciones y planes sociales.

Guzmán aseguró que marzo será el mes de mayor inflación del año, algo que efectivamente sucedió en 2021. En esta ocasión se podría repetir el vaticinio, sin embargo los valores seguirán altísimos en abril y los meses venideros. No será fácil que el índice se ubique por debajo del 60% este año, lo que representa un sustancial salto respecto al 50,9% registrado el año pasado.

¿Cuáles son los factores que seguirán impulsando los niveles de inflación en los próximos meses? Los más relevantes serían los siguientes:

**- Se viene un fuerte aumento de tarifas:** antes de fin de mes debe efectuarse la audiencia pública que determinará los futuros aumentos. Según se definió en el acuerdo con el FMI, los ajustes deben estar en línea con la inflación, con lo cual saldrán del virtual congelamiento de los últimos dos años. Esto tendrá un impacto directo en los índices de inflación, pero además también repercutirá en los costos de las empresas, que tendrán más argumentos para remarcar.

**- El dólar oficial seguirá acelerando**: ya en la última semana el Banco Central lo ajustó a una tasa del 4% mensual, lo que arroja un 60% interanual. Ese incremento ya impacta en el costo de las importaciones, tanto en lo que respecta a los insumos que forman parte de la producción como en los bienes de consumo finales que ingresan (cada vez menos).

**- La guerra va para largo**: contra los pronósticos que indicaban un conflicto corto, todo indica que se prolongará más de lo esperado. Por lo tanto, el shock sobre los precios de los alimentos y la energía podría también prolongarse más de lo previsto, lo que claramente tendrá un efecto sobre la inflación.

**- Las paritarias ya dejaron atrás el 40% y no tienen un ancla nominal**: el Gobierno arrancó el año buscando imponer ese ajuste, pero quedó rápidamente superado por la inflación de los primeros meses del 2022. Difícilmente los gremios más fuertes acepten cerrar por menos del 55%. Algunos como camioneros presionan para una reapertura de paritarias con el objetivo de no perder respecto a lo que habían acordado en 2021.

**- Si se cumple, el acuerdo con el FMI tardará en hacer efecto en la inflación:** el objetivo principal del programa es conseguir una desaceleración de los precios. Para eso se apunta a reducir el déficit fiscal de 3% a 2,5% y al mismo tiempo bajar fuertemente la emisión monetaria para financiar al Tesoro. Pero aún en caso de que se cumpla el impacto sobre los precios tardará en suceder. Y además el cambio de las condiciones internacionales vuelve mucho más difícil que se pueda cumplir con la rebaja del rojo fiscal.