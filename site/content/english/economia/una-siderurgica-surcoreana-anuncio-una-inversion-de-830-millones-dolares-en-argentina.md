+++
author = ""
date = 2022-01-06T03:00:00Z
description = ""
image = "/images/5cf7e0ae1b991_900-1.jpg"
image_webp = "/images/5cf7e0ae1b991_900.jpg"
title = "UNA SIDERÚRGICA SURCOREANA ANUNCIÓ UNA INVERSIÓN DE 830 MILLONES DÓLARES EN ARGENTINA"

+++

##### **_El jefe de Gabinete, Juan Manzur, recibió este miércoles al presidente de la siderúrgica surcoreana Posco Argentina, KwangBok Kim, y al director de la compañía, SungKook Chung, quienes anunciaron una inversión en su planta de Argentina por 830 millones dólares._**

"Nosotros tomamos la decisión de ejecutar nuestro proyecto comenzando la construcción de nuestra planta comercial en marzo de este año con una inversión directa cerca de 830 millones de dólares", señaló el titular de Posco Argentina, KwangBok Kim en declaraciones a los medios acreditados en Casa Rosada.

Manzur, por su parte, en su cuenta de Twitter señaló: "Hoy recibí al presidente de Posco Argentina, KwangBok Kim y a su director, SungKook Chung. Esta empresa lleva adelante un proyecto integral de extracción de litio en el Salar del Hombre Muerto -entre Salta y Catamarca- que se encuentra en una etapa de exploración avanzada".

El jefe de Gabinete apuntó que "analizamos diferentes vías de respaldo estatal para que este emprendimiento pueda seguir creciendo y generando nuevos puestos de trabajo, bajo los estándares ambientales de nuestro país. Seguimos afianzando la recuperación económica".

En tanto, el titular de la empresa siderúrgica coreana sostuvo que Manzur "se comprometió a dar todo el apoyo necesario para que nuestro proyecto tenga un éxito en Argentina".

"A partir de marzo iniciaremos la primera etapa de construcción de la planta comercial que va a generar en forma significativa mano de obra y recursos para Argentina", destacó el empresario coreano.

Por su parte, en declaraciones a Télam en la Casa Rosada, el gobernador de Catamarca, Raúl Jalil, consideró que "hay muchas inversiones mineras en Catamarca, Salta y Jujuy, sobre todo en litio, que van a cambiar la matriz energética de la electromovilidad y todo lo que se necesita para los paneles solares y la energía eólica".

"Estoy muy conforme con el plan de inversiones que están teniendo muchas empresas de distintas nacionalidades. Posco es una empresa muy grande que visitaremos en abril", señaló el gobernador.

Jalill analizó que la "economía argentina está creciendo bien" y el sistema turístico gubernamental PreViaje "está funcionando bien y hay que tomar todas las medidas necesarias para que la economía no pare" en medio de la pandemia por coronavirus.

El "proyecto integral de litio" de Posco se denomina Sal de Oro y está ubicado en el Salar del Hombre Muerto, límite entre las provincias de Salta y Catamarca.