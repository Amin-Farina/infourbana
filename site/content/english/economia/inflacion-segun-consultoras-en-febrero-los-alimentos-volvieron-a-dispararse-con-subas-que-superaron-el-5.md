+++
author = ""
date = 2022-03-03T03:00:00Z
description = ""
image = "/images/internos-maqueta-foto-portada-notas-mercadovillaa.jpg"
image_webp = "/images/internos-maqueta-foto-portada-notas-mercadovillaa.jpg"
title = "INFLACIÓN: SEGÚN CONSULTORAS, EN FEBRERO LOS ALIMENTOS VOLVIERON A DISPARARSE CON SUBAS QUE SUPERARON EL 5%"

+++

##### **_De acuerdo a diversos consultores que relevan precios, la suba del segundo mes del año estuvo por arriba de lo registrado por el Indec en enero, cuando alcanzó un 4,9% mensual. Los mayores aumentos se dan en frutas, verduras y en carnes_**

Luego de que en febrero la inflación alcanzara el 3,9%% y acumulara una variación de 50,7% en los últimos doce meses de acuerdo al Instituto Nacional de Estadísticas y Censos (Indec), los especialistas calcularon que los alimentos volvieron a impulsar el alza de precios en febrero con incrementos que llegan a superar el 5 por ciento.

De acuerdo a la consultora LCG, el índice de alimentos y bebidas presentó una inflación mensual de 4,8% promedio en las últimas 4 semanas de y 4,6% punta a punta en el mismo periodo. En ese sentido, indicó que **_las verduras lideraron la inflación de febrero con un alza de 14,8%_**. Detrás aparecen **_bebidas e infusiones para consumir en el hogar, con un incremento de 5,7% y le siguen frutas con una suba del 5 por ciento_**.

Más lejos figuran lácteos y huevos, con un alza de 4,8% seguido por aceites (+4,5%); carnes (+3,8%); comidas para llevar (+2,9%) y productos de panificación, cereales y pastas (2,4%).

En tanto, desde la ONG Consumidores Libres informaron que, según el relevamiento efectuado por la entidad en supermercados y negocios barriales de la ciudad de Buenos Aires, el precio de los 21 productos de la llamada “canasta básica de alimentos” tuvo un aumento del 4,29% durante febrero, mientras que desde el primero de enero la suma acumulada es del 9,54 por ciento.

En ese marco, la entidad informó que “Almacén” registró un alza de 7,66%. La docena de huevos fue lo que más aumentó con una suba de 20%, seguida por el kilo de pan fresco que registró un alza de 16,67% intermensual.

Asimismo, “Frutas y Verduras” registró un alza de 29,32%, de acuerdo a Consumidores Libres. Lo que más aumentó en este caso fue el kilo de zanahorias, con un incremento del 91%; seguido por el paquete de acelga, con un incremento de 66,6 por ciento. A su vez, “Carnes” registró un incremento de 4,46% encabezado por el kilo de paleta que aumentó 6,5% y seguido por la carne picada común, con un 4,5% de aumento.

En tanto, el relevamiento de precios minoristas de C&T para el Gran Buenos Aires arrojó una suba mensual de 3,9% en febrero, superior al 3,6% de febrero de 2021 pero menor al 4,5% de enero. Así, la variación interanual aumentó a 51,9%, la mayor desde octubre.

Según la consultora que dirige el economista Camilo Tiscornia, “Alimentos y bebidas” fue el rubro de mayor incremento con un alza de más de 6% y con gran incidencia de verduras, pero con aumentos importantes en la mayoría de los componentes.

##### En los almacenes pequeños

La canasta de productos alimenticios que el Gobierno anunció para los almacenes y comercios de cercanía todavía sigue en lista de espera. La propuesta había surgido hace cerca de un mes por iniciativa de los propios minoristas, pero hasta el momento la Secretaría de Comercio Interior no le encuentra la vuelta a su puesta en funcionamiento.

El eje central del conflicto está puesto en que los mayoristas, proveedores de los comerciantes, les venden los productos incluidos en el programa Precios Cuidados –hoy disponible en grandes superficies- al precio final que figura en las listas oficiales, lo que les impide obtener un margen de ganancia.

A raíz de esto, los minoristas reclamaron un mecanismo que les permitiera lograr ese beneficio, algo que por el momento no tuvo éxito. “Lo que pedimos no es ninguna locura. Solo queremos formar parte de un programa que nos permitirá acercar una mayor cantidad de propuestas a nuestros clientes, pero que al mismo tiempo colaborará con la necesidad del Gobierno de combatir la inflación”, sostuvo Fernando Savore, presidente de Federación de Almaceneros de la provincia de Buenos Aires (FABA).

“Si vamos a tener una lista que diga a cuánto vamos a tener que vender, también deberemos tener una lista que paute a cuánto deberemos comprar”, continuó.

La expectativa que los minoristas tienen hoy es la misma que tenían el año pasado, cuando también habían manifestado su intención de participar de Precios Cuidados, aunque fue la misma problemática de hoy la que no hizo esto posible. La solución a la que se intenta acercar el Gobierno tiene que ver con convencer a las alimenticias y los mayoristas de “partir diferencias”, tal como dijo un funcionario a NA.

“Si los fabricantes reducen sus márgenes y le venden más barato a los mayoristas, y lo mismo hacen estos con los minoristas, tendríamos el problema solucionado. Pero por ahora seguimos negociando”, dijo la fuente consultada.

En este sentido, se espera que en los próximos días continúen las negociaciones, ya que la idea original de Comercio Interior, con Roberto Feletti a la cabeza, era que el programa arrancara junto con el mes de marzo, algo que ya no podrá ser.