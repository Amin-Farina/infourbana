+++
author = ""
date = 2022-02-17T03:00:00Z
description = ""
image = "/images/78589_imagen_1088x650xrecortarxagrandar-1.jpg"
image_webp = "/images/78589_imagen_1088x650xrecortarxagrandar.jpg"
title = "DE ARGENTINA A MEDIO ORIENTE: CRECE EL INTERÉS POR LA YERBA MATE NACIONAL"

+++
##### 

##### **_Distintos establecimientos yerbateros de Misiones que participan en la feria alimentaria Gulfood Dubái -considerada como la más importante de los Emiratos Árabes Unidos y puerta de entrada a Medio Oriente- señalan asombrados el gran interés y el potencial de crecimiento. Siria concentra el 80% de los embarques y constituye el principal destino de exportación de la yerba mate argentina._**

La yerba mate argentina se encuentra participando de la feria Gulfood Dubái que se desarrolla entre el 13 y el 17 de febrero en esa ciudad de los Emiratos Árabes Unidos y en la cual se destaca el interés de los visitantes por conocer el origen del producto.

> "En Medio Oriente están muy interesados por el origen de la yerba mate argentina, por todo el proceso productivo que empieza con la cosecha", comentó Rubén Álvez, representante de los trabajadores rurales en el Directorio del Instituto Nacional de la Yerba Mate (INYM).

Los establecimientos yerbateros de Misiones que participan subrayan el potencial de crecimiento que tiene el producto en Medio Oriente y resumen como "muy positivo" el balance que registra la presencia de la yerba mate argentina en la feria Gulfood Dubái.

Además de Álvez, María Marta Oria, subgerente de Promoción y Desarrollo del INYM, acompañan a los establecimientos yerbateros que solicitaron apoyo para estar presentes en esta feria alimentaria; considerada como la más importante de los Emiratos Árabes Unidos y puerta de entrada a Medio Oriente.

> "Es muy importante la labor que desarrollan los importadores para posicionar la yerba mate argentina, y ellos también están acá, junto a las firmas que participan en la feria", resaltó Oria.

En tanto Gustavo Redondo, gerente Corporativo Comercial del Establecimiento Santa Ana señaló que, "para empezar quedamos gratamente sorprendidos por la cantidad de visitantes que están asistiendo, desde nuestro punto de vista, superó en cantidad a los que la visitaban en épocas de pre-pandemia",

En esa misma línea se manifestó Gerardo Koropeski, de la firma Hreñuk SA, quien también se mostró sorprendido por la convocatoria de la feria y por el interés en la yerba mate.

"Hemos tenido muchas empresas que visitaron nuestro stand para ver las posibilidades de ingresar con nuestra marca a distintos países del Golfo, lo cual para nosotros es una muy buena noticia", relató entusiasmado.

La importancia de esta feria radica en su cercanía con Siria; país que concentra el 80% de los embarques y constituye el principal destino de exportación de la yerba mate argentina.

"Por lo que pude observar -apuntó Álvez- los consumidores de esta región están muy interesados en el origen y tuve la oportunidad de explicar cómo se cosecha y llega con excelente calidad hasta estos mercados", indicó.