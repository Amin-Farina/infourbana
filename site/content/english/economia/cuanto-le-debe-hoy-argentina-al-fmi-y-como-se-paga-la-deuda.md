+++
author = ""
date = 2021-11-02T03:00:00Z
description = ""
image = "/images/alberto-fernandez-reunido-este-jueves___atnvs0baz_1256x620__1.jpg"
image_webp = "/images/alberto-fernandez-reunido-este-jueves___atnvs0baz_1256x620__1.jpg"
title = "CUÁNTO LE DEBE HOY ARGENTINA AL FMI Y CÓMO SE PAGA LA DEUDA"

+++

##### **_Aunque el Gobierno considera que la carga de la deuda es insostenible y negocia un nuevo acuerdo, sigue pagando en tiempo y forma los vencimientos. Este año ya hizo varios giros y en diciembre tendrá que hacer otro desembolso._**

El Gobierno continúa negociando con el FMI para llegar a un nuevo acuerdo que le permita reestructurar el préstamo por alrededor de US$45.000 millones que la Argentina pidió en 2018.

A diferencia de lo que sucede con un canje de deuda con acreedores privados, que se refinancia mediante un acuerdo de partes y con la posterior emisión de nuevos bonos, el organismo multilateral no admite ese tipo de operación. Por el contrario, en el caso de no poder cumplir con sus compromisos, el país en cuestión debe firmar un nuevo acuerdo.

Mientras el Gobierno sigue dialogando con los técnicos del FMI, el Tesoro gira en tiempo y forma los pagos, según el cronograma acordado. En lo que va de 2021, La Argentina hizo cinco desembolsos para cancelar intereses y uno para disminuir el capital adeudado.

##### Cómo se pagan las deudas con el FMI

Tanto los desembolsos que hace el FMI a los países como los pagos que los deudores le giran al organismo se realizan en “Derechos especiales de giro (DEG)”, que funciona como la unidad de cuenta de la entidad. “El DEG es un activo de reserva internacional creado en 1969 por el FMI para complementar las reservas oficiales de los países miembros”, define el organismo en su web.

Cuando el FMI hace una asignación de DEG se contabilizan en las reservas del Banco Central, que también sufren el impacto al momento de hacer los pagos.

El valor de los DEG está determinado en función de una canasta de monedas que deben cumplir con dos requisitos:

Debe ser emitida por un país miembro del FMI o de una unión monetaria que incluya integrantes del FMI. Además, el emisor de esa moneda debe ser uno de los cinco principales exportadores del mundo.

La moneda debe ser de libre uso, lo que significa que se utilice ampliamente para saldar transacciones internacionales y que se negocie ampliamente en los principales mercados de cambio. Además, esta clase de monedas pueden utilizarse en las operaciones financieras del FMI.

La composición de la canasta de monedas se revisa cada cinco años y actualmente está integrada por el dólar estadounidense, el euro, el yuan, el yen y la libra esterlina. El valor del DEG se determina diariamente a partir de los tipos de cambio de esas monedas en el mercado.

A partir de esas cotizaciones, se pueden convertir a dólares los desembolsos desde y hacia el FMI. Por ejemplo, al cierre de esta nota, un DEG equivalía a US$1,412290. Ese tasa de conversión explica las variaciones que se pueden encontrar entre montos adeudados al FMI expresados en moneda estadounidense, pero estimados en diferentes momentos.

##### Cuánto le debe la Argentina al FMI

Además de los US$1300 millones que giró el año pasado, en lo que va de 2021, el Gobierno ya hizo cinco pagos de intereses al FMI. Por intereses, el Tesoro abonó según el siguiente cronograma:

* US$314 millones en febrero;
* US$316 millones en mayo;
* US$349 millones en agosto;
* US$137 millones en septiembre
* US$388 millones el 1° de noviembre.

Además, en septiembre canceló una cuota de capital por US$1884 millones. De acuerdo con los datos publicados en la web del FMI, en diciembre la Argentina tendrá que abonar otra cuota de capital por US$1874 millones.

Pese a los pagos que ya se hicieron, a la Argentina le pasa con el FMI lo mismo que a cualquier deudor: al principio canceló intereses en lugar de capital, todavía restan abonar unos US$44.904 millones al organismo, de los cuales US$43.198 millones corresponden a vencimiento de capital. Así, la Argentina tendrá que pagar aproximadamente una cifra similar a todo el préstamo que recibió en 2018.

Las amortizaciones más fuertes de capital están planificadas para 2022 y 2023, que suman vencimientos por más de US$38.000 millones, similares a todas las reservas que hoy tiene el Banco Central.

Si no consigue un nuevo programa con el FMI, el año que viene la Argentina tendría que pagar US$18.956 millones. Hacia 2023, en tanto, los desembolsos previstos sumarían otros US$19.205 millones. Luego, en el período que va desde 2024 hasta 2036, el Gobierno argentino debería pagar unos US$2,2 millones por año.

Al interior del Gobierno buscan cerrar el nuevo programa, en el que no descartan que el FMI le devuelva a la Argentina los pagos que hizo durante este tiempo. Otra alternativa es conseguir un trato similar al que se hizo con el Club de París: un puente de tiempo que evite hacer los desembolsos, pero, a la vez, permita al Gobierno esquivar el default.