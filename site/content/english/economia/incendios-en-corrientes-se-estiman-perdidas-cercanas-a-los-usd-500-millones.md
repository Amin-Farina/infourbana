+++
author = ""
date = 2022-02-15T03:00:00Z
description = ""
image = "/images/incendios-de-campos-vacas-1.jpg"
image_webp = "/images/incendios-de-campos-vacas.jpg"
title = "INCENDIOS EN CORRIENTES: SE ESTIMAN PÉRDIDAS CERCANAS A LOS USD 500 MILLONES"

+++

##### **_Jorge Vara, exministro de Producción de la provincia de Corrientes, aseguró que las pérdidas de las cadenas productivas por los incendios en la provincia podrían llegar a los 500 millones de dólares, donde la ganadería será una de las más perjudicadas y que representa para Corrientes el 40% del PBI Rural._**

“El daño final será mayor. A todo esto hay que agregar que aquellos campos que no sufrieron las consecuencias de los incendios, están atravesados desde hace tiempo por una dura sequía, con forestaciones donde las plantas se están muriendo por la ausencia de lluvias”, dijo Vara, quien agregó que si bien hay pronósticos de precipitaciones para marzo o abril, los problemas seguirán existiendo porque la está afectada la floración. “Todavía vamos a estar reflejando pérdidas el año próximo de esta situación dramática”, señaló.

En Corrientes todos coinciden que el momento climático adverso está más vigente que nunca y que tendrá un largo tiempo de duración, donde recién para marzo o abril están pronosticando el regreso de las lluvias importantes. Y todo esto deriva en la preocupación de los productores sobre cómo enfrentar la dramática crisis. Muchos de ellos ya están pensando, lamentablemente en dejar la actividad, porque perdieron absolutamente todo por los incendios, y no tienen respaldo económico y financiero para volver a empezar en la producción, y además consideran insuficiente la ayuda que envía el Estado nacional y provincia. “Es imposible que el Gobierno nacional y provincial puedan afrontar todos los daños. La ayuda les va a servir para seguir produciendo, pero no para salvar las pérdidas”, manifestó Vara.

##### Herramientas

En la mirada de cómo prevenir este tipo de fenómenos climáticos, que lamentablemente castigan muy seguido al sector agropecuario, el diputado y ex funcionario planteó la necesidad de trabajar en mejorar el sistema de manejo del fuego. Al respecto, comentó: “Cuando dicha área estuvo bajo la órbita del ministerio de Seguridad funcionó mejor que cuando depende del ministerio de Ambiente. Hay que mejorarlo al mismo, ya que no responde la autoridad de aplicación. No se están usando las nuevas tecnologías, falta profesionalismo y conocimiento. Hay que trabajar para prevenir este tipo de fenómenos y si ocurren actual de manera rápida”.

Por último, al referirse a un histórico reclamo del sector agropecuario de implementar una Ley a nivel nacional de seguro multirriesgo, Vara sostuvo que habrá que analizar con detenimiento cada una de las propuestas ya que los mismos tienen un alto costo de administración. Además, desde hace tiempo los productores vienen reclamando una ampliación del monto de la Ley de Emergencia Agropecuaria, que actualmente se encuentra en 500 millones de pesos, pero el ministro de Agricultura, Julián Domínguez, pido al Jefe de Gabinete, Juan Manzur, elevar ese monto a 12.500 millones de pesos.