+++
author = ""
date = 2022-02-23T03:00:00Z
description = ""
image = "/images/soja-ucrania.jpg"
image_webp = "/images/soja-ucrania.jpg"
title = "SOJA EN ALZA: SUPERA LOS USD 600 EN MEDIO DE PROBLEMAS CLIMÁTICOS EN SUDAMÉRICA Y LA TENSIÓN EN UCRANIA"

+++

#### **_El precio internacional de la soja escaló a USD 604 por tonelada para el contrato a marzo, un máximo de casi un año en medio de la tensión por el ingreso de tropas rusas a Ucrania y los problemas climáticos en Sudamérica._** 

El cultivo más importante para la Argentina no es el más afectado por el conflicto en el Este de Europa, que incluye a dos de los principales productores de trigo y maíz en el mundo, pero el impacto sobre materias primas en general se monta sobre las dudas por la cosecha de soja en medio de la sequía que azota a Argentina y Brasil.

La tonelada de soja para marzo ganaba USD 3,67 en las operaciones nocturnas del mercado de Chicago, de referencia para el comercio de materias primas agropecuarias a nivel global. El contrato para mayo, mientras tanto, se pactaba con una suba de UD 2,66 para llegar a USD 603,41.

> “Ayer se rompió un techo que se había tocado en mayo del año pasado, te tenés que ir varios años atrás para encontrar de nuevo ese techo”, dijo Dante Romano de FyO.

Según los especialistas de la Bolsa de Comercio de Rosario (BCR) la que se encamina a ser la quinta rueda consecutiva de avance para la soja ocurre como consecuencia que persisten las preocupaciones sobre el clima seco en las zonas de producción de Brasil y Argentina.

Por el lado del maíz, los incrementos de precios se generaron ayer se ecplican por temores a una posible interrupción del comercio en la región del Mar Negro, a medida que se profundiza la crisis entre Rusia y Ucrania. Y los futuros de trigo, también por las crecientes tensiones entre Rusia y Occidente sobre Ucrania, que generan preocupaciones en torno al suministro del cereal en esta región, lo que apuntala a los precios.

Sin embargo, después del sacudón de ayer martes cuando el mundo amaneció con el reconocimiento por parte de Rusia de la independencia de dos regiones de Ucrania controladas por separatistas pro rusos y el ingreso de tropas a ese territorio ucraniano, los precios del trigo y el maíz se toman un descanso este miércoles.

El contrato de trigo a marzo cae a USD 308.92, una baja de USD 1,29 en lo que va del día. Es apenas un recorte en comparación con los USD 17 que supo escalar cuando los tanques de guerra cruzaron la frontera de Ucrania.

En el caso del maíz, mientras tanto, el contrato a marzo perdía USD 1,48 esta madrugada luego de haber cerrado el martes con una baja de unos pocos centavos.