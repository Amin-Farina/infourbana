+++
author = ""
date = 2021-12-06T03:00:00Z
description = ""
image = "/images/67gwkkxv5rggvgmfsosigtie5i.jpg"
image_webp = "/images/67gwkkxv5rggvgmfsosigtie5i.jpg"
title = "RENOVABLES: UN FRIGORÍFICO COMENZÓ A PRODUCIR ENERGÍA CON ESTIERCOL BOVINO PARA LA RED ELÉCTRICA NACIONAL"

+++

#### **_ArreBeef Energía inauguró una planta que producirá 7.200 MW anuales. Es la primera en el mundo en generar energía sostenible a partir de residuos orgánicos del ganado vacuno._**

Un frigorífico local se convirtió en uno de los primeros en el mundo en producir energía a partir de los residuos orgánicos del ganado vacuno. Se trata de la empresa ArreBeef Energía, una compañía del frigorífico ArreBeef, que anunció que producirá 7.200 MW anuales, que se intregrarán a la red eléctrica nacional, con una inversión de USD 6,5 millones.

ArreBeef Energía inauguró esta semana oficialmente su planta ubicada en Pérez Millán, partido de Ramallo, en la provincia de Buenos Aires. El proyecto se inició en 2017 como una nueva división del Frigorífico ArreBeef: sus propietarios buscaban integrar a su proceso productivo la generación de energías renovables a partir de un tratamiento más eficiente de sus residuos.

En el marco del programa RenovAR 2.0, la compañía asumió el compromiso de abastecer a la red eléctrica nacional con 7.200 MW anuales. Desde julio pasado, cuando la planta obtuvo su habilitación comercial, hasta fines de noviembre, ya aportaron a la red nacional unos 2.643 MW eléctricos de origen renovable.

“De esta manera, se evitan la emisión a la atmosfera de unas 1020 toneladas de dióxido de carbono. Esta cifra es el equivalente a 157.000 automóviles menos circulando durante un día, y se hubiesen necesitado unos 14.000 árboles adultos para mitigar el impacto ambiental de estas emisiones”, explicó la empresa en un comunicado.

“ArreBeef Energía es un proyecto innovador, que nos trae una nueva mirada sobre la producción y se sostiene en la sólida trayectoria de nuestra empresa —señaló Hugo Borrell, vicepresidente de ArreBeef—. Sabemos que este es el punto de partida para otros proyectos que nos permitirán crecer y potenciar el trabajo en la región. Además, seguiremos afianzando nuestros pilares motivados por el desarrollo de acciones sostenibles que busquen contagiar proveedores, clientes y colaboradores, contribuyendo a la protección del medioambiente.”

Para lograr producir energía, la empresa construyó un biodigestor que transforma todos los residuos orgánicos del frigorífico en el biogás necesario para el funcionamiento de un motor de cogeneración. Este motor convierte el metano presente en el biogás en energía eléctrica, entregando 1.5 MW de potencia, equivalente al consumo energético de una población de 7.000 habitantes, según detallaron.

La compañía frigorífica tuvo sus inicios en 1921, cuando Jaime Borrell comenzó a trabajar como carnicero en la localidad de Arrecifes. Las siguientes generaciones de la familia aportaron nuevas formas de hacer negocios y un cambio en la formas de producción.

Actualmente, la empresa cuenta con más de 1.000 empleados y una producción anual aproximada de 80.000 toneladas de carne destinadas a exportación y mercado local. En los últimos años, comenzaron a orientarse a la sostenibilidad de las operaciones.

“La inauguración de esta planta es un logro que hemos conseguido junto a nuestros colaboradores, que nos acompañan en este camino y se suman a este nuevo paradigma de producción sostenible. Estamos orgullosos de ser pioneros en la generación de este tipo de energía en nuestra industria y ser protagonistas del cambio consolidando a Arrebeef como una empresa sostenible”, comentó Borrell.

En septiembre pasado, las energías renovables cubrieron el 14,2% de la demanda eléctrica nacional y alcanzaron un pico histórico de casi el 29% el día 26 de septiembre. Según detallaron desde la secretaría de Energía, en el tercer trimestre de 2021 se incorporaron 103,22 MW de potencia instalada por fuentes renovables a través de cinco proyectos en tres provincias.

En el tercer trimestre de 2021 se habilitaron cinco nuevos proyectos de energías renovables en tres provincias que suman 103,22 MW de potencia instalada al Sistema Argentino de Interconexión (SADI). Además de la Central Térmica de biogás Arre Beef Energía, los proyectos habilitados fueron: el Parque Solar Guañizuil II A, en la provincia de San Juan, con 100 MW; la Central Térmica de Biogás Resener I en la provincia de Buenos Aires y dos aprovechamientos hidroeléctricos en la provincia de Córdoba, PAH Cruz del Eje II y PAH Boca del Río.