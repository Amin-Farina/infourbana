+++
author = ""
date = 2021-12-17T03:00:00Z
description = ""
image = "/images/ocdefao_1-1.jpg"
image_webp = "/images/ocdefao_1.jpg"
title = "FUERTE EXPANSIÓN DE LAS EXPORTACIONES DE LA ARGENTINA HACIA BRASIL, POR ENCIMA DE OTROS SOCIOS COMERCIALES"

+++

#### **_Las ventas externas hacia el vecino país crecieron un 71,3% entre noviembre del año pasado y el mismo mes de 2021, según un informe de la Fundación Getulio Vargas._**

La Argentina mostró en noviembre un destacado desempeño en las exportaciones a Brasil, transformándose en el socio comercial que registró un mayor aumento de sus ventas externas a ese país. Durante el mes pasado, las exportaciones argentinas a Brasil crecieron un 71,3% en relación al mismo mes que el año pasado, medidas en volúmenes.

En el mismo período, las ventas externas de Asia al mercado brasileño crecieron un 10,1%, las de la Unión Europea un 9,8% y las de China un 6,3%, mientras que las exportaciones del resto de América del Sur cayeron un 9,6%, al igual que las de Estados Unidos (-13,8%) y México (-23,3%). La información corresponde al “Indicador de Comercio Exterior” que publica cada mes la Fundación Getulio Vargas.

“Las importaciones de maquinarias y equipos, así como de bienes intermedios, pueden entenderse como un indicador de expectativas favorables para el crecimiento de un sector. Se observa que, en la comparación interanual mensual, las variaciones en los volúmenes importados de agricultura superaron a las de la industria manufacturera, tanto en la compra de máquinas como en la de bienes intermedios”, apuntó el informe del think tank brasileño.

Daniel Scioli destacó el crecimiento exponencial de las exportaciones como “el resultado de una serie de acciones que tienen que ver con salir a vender, no esperar que nos vengan a comprar”. En ese sentido el embajador argentino en Brasil mencionó “un relevamiento que se hizo de todo el mercado brasileño, innumerable cantidad de misiones virtuales a pesar de la pandemia” y visitas en persona a todos los estados del país vecino donde mantuvo encuentros con gobernadores, empresarios y supermercadistas, siempre persiguiendo el objetivo de “alcanzar nuevos mercados dentro de Brasil”.

El informe destaca que para el comercio exterior brasileño “la devaluación cambiaria pesa más sobre la industria manufacturera que sobre la agricultura, dada la participación diferencial de los bienes intermedios en los sectores”. En ese escenario, dados los altos precios internacionales de las materias primas, el sector agrícola se vio menos afectado en términos relativos que la industria del país.

Otro trabajo, de la consultora argentina Abeceb, destacó que las exportaciones argentinas a Brasil alcanzaron el mesa pasado los USD 1.355 millones, “la cifra más alta de los últimos 8 años”. En la composición de esas ventas externas, se destacaron las subas de trigo y centeno sin moler, que crecieron un 630% interanual con un total exportado de USD 104 millones; los vehículos para transporte de pasajeros, que subieron un 260% hasta los USD 176,9 millones y los vehículos para transporte de mercancías (+48%; USD 226,7 millones).

También se registró una suba fuerte de las exportaciones de maíz, que alcanzaron los USD 82 millones frente a “niveles insignificantes del año pasado”. En Abeceb puntualizaron que “la Argentina aumentó su participación en el total de importaciones de Brasil pasando de un 5,2% promedio en los últimos 12 meses a representar un 6,9% en el último mes”.

Scioli enmarcó este logro dentro de “una política muy intensa de traer inversiones para la Argentina” y remarcó el trabajo cotidiano en conjunto con la Cancillería a cargo de Santiago Cafiero.

“En la práctica la Embajada es una maquinaria de generar puestos de trabajo y divisas para la Argentina, que es lo que mas necesita el país”, agregó el ex gobernador bonaerense.

Más allá de los productos tradicionales que históricamente importó Brasil desde Argentina, en esta oportunidad se destacó que “cada vez se exportan más manufacturas de origen industrial con valor agregado” y se ha podido incrementar la venta de energía. “El gran futuro por delante es exportar cada vez más gas”, planteó Scioli.

Pese las diferencias ideológicas entre los gobiernos de Alberto Fernández y de Jair Bolsonaro, el embajador mencionó el valor de “la relación estratégica para la Argentina” y que en este marco generó “una relación de diálogo directo” con el mandatario brasileño.

“Tengo la mejor expectativa para 2022″, se mostró entusiasmado e informó que se están diseñando las bases para el año próximo ir a la búsqueda de “incentivar a pymes” e incrementar la comercialización de autopartes, “programas de financiamiento con el BICE, el Banco Nación y de la Provincia para mejorar costos de logísticas y servicios aduaneros y otorgarles mayor competitividad”. En este marco, Scioli celebró “la fuerte sinergia entre la Embajada y todo el sector privado de Brasil y Argentina”.