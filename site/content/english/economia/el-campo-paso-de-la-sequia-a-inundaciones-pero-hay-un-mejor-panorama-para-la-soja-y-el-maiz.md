+++
author = ""
date = 2022-01-25T03:00:00Z
description = ""
image = "/images/campo-inundado.jpg"
image_webp = "/images/campo-inundado.jpg"
title = "EL CAMPO PASÓ DE LA SEQUÍA A INUNDACIONES, PERO HAY UN MEJOR PANORAMA PARA LA SOJA Y EL MAÍZ"

+++

##### **_El epicentro de las últimas precipitaciones fue el centro y oeste de Buenos Aires, sur de Córdoba y norte de La Pampa. Desde la Bolsa de Cereales de Buenos Aires señalaron que en el sur del área agrícola el contexto cambió rotundamente_**

El año comenzó con una fuerte sequía y una extrema ola de calor en casi todo el territorio nacional que puso en jaque la campaña gruesa de granos, en especial a los cultivos de maíz y soja. Sin embargo, las lluvias de estos últimos días, sobre todo las que acontecieron entre el jueves y el domingo parecen haber cambiado el escenario en una vasta zona del país, incluso con anegamientos, y prendió una luz de esperanza en el campo argentino. Una situación que una vez más debe en evidencia los graves problemas de infraestructura que deben hacer frente los productores, con caminos rurales en mal estado, entre otros, en tiempos de una presión tributaria asfixiante.

Así lo señaló el jefe de Estimaciones Agrícolas de la Bolsa de Cereales de Buenos Aires (BCBA), Esteban Copati. Según destacó el especialista a este medio, las lluvias que se dieron entre el jueves y el sábado permitieron recomponer la humedad en los lotes ubicados sobre el sur de Córdoba, gran parte de Buenos Aires y La Pampa revirtiendo la falta de agua que aquejaba a la zona, mientras que la franja centro y norte deberán consolidar su recuperación con nuevas precipitaciones que se podrían dar en el cierre de enero.

“En términos generales pasó de ser una escenario drástico de recortes de producción con muchos riesgos a haber consolidado una buena recuperación en zonas claves del área agrícola y eso levanta el piso del potencial de rendimiento”, indicó Copati, aunque remarcó que todavía “hay riesgo”, sobre todo respecto a lo que pueda pasar en febrero, que si bien marca un interrogante, “por lo menos a vamos a ingresar a ese mes en plena etapa reproductiva para el maíz tardío y soja de primera con un escenario más alentador”.

Según destacó Copati, en el sur del área agrícola el panorama “cambió rotundamente”, con lluvias acumuladas de 240 milímetros en el partido bonaerense de 9 de Julio, de 200 milímetros en Pehuajó, de 315 milímetros en algunas zonas de Carlos Tejedor, de 203 milímetros en Tres Algarrobos, 109 milímetros, en Las Flores y 100 en Laboulaye, entre otros puntos de Buenos Aires, Córdoba y La Pampa. Caso emblemático fue el de la localidad cordobesa de Jovita, donde en pocas horas cayeron 250 milímetros y no solo se inundaron los campos, sino el pueblo entero.

Además, en el sur de Patagones, en la provincia de Buenos Aires, recibió en dos días más de 200 milímetros, que es más de la mitad de promedio anual histórico de lluvias de la zona. Lo insólito es que el viernes pasado el Gobierno de Axel Kicillof declaró el estado de emergencia y desastre agropecuario por sequía en el partido de Patagones del 1º de enero y al 31 de diciembre del 2021. Por otro lado, el propio intendente del mencionado distrito, José Luis Zara, rescató a una pareja que había quedado atrapados en una chacra vecina a la suya, y mostró desde su cuenta de Twitter que en apenas horas la zona pasó de la sequía a la inundaciones.

“El escenario se ha acomodado bastante y estamos en una campaña con un buen potencial de rendimiento pese a que ya hubo algunas pérdidas”, destacó Copati y, más allá de que se produjeron algunos anegamientos en ciertas zonas donde las lluvias fueron más intensas y cayeron muchos milímetros en pocas horas, el especialista consideró que “siempre es preferible estar en un año húmedo que en uno seco. Si bien la inundación trae sus problemas, siempre deja buenos rindes”. Cabe mencionar que la entidad bursátil estima que la producción de maíz alcanzará las 57 millones de toneladas, la soja 44 millones, el sorgo y el girasol 3,5 millones cada uno.

##### Centro y norte

Donde las lluvias fueron menos homogéneas y se debe consolidar la etapa de recuperación es en el centro y el norte del área agrícola, siendo el área más importante la zona núcleo. “Habrá que ver si en estas regiones se puede consolidar una recuperación como vimos en los últimos días en el sur de Córdoba, Buenos Aires y La Pampa. Sería importante emparejar esa situación”, señaló Copati.

El especialista puntualizó que todavía hay lugares de Santa Fe que no han acumulado milímetros necesarios para consolidar una recuperación, sino que las precipitaciones recibidas funcionaron como un suerte de parche, que si bien mejoró la situación, no lo hará durante mucho tiempo. “En la zona núcleo el cambio todavía está un poco dispar pero con buenos pronósticos para los próximos días. El pronóstico hasta fin de enero va mejorando dando agua”, explicó.

Respecto al norte, el especialista de la Bolsa de Cereales de Buenos Aires destacó la importancia de seguir sumando humedad al sostener que “todo ese sector va a depender en gran medida para homogeneizar un escenario favorable del pronóstico para fines de enero, el cual es bueno”. No obstante, subrayó el problema de las altas temperaturas, en especial en la región del NEA, donde se espera una nueva ola de calor sin lluvias importantes en los próximos días.