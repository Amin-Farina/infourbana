+++
author = ""
date = 2021-12-15T03:00:00Z
description = ""
image = "/images/fgz2wlo3xl2r6wkgi32fmt3e2y.jpg"
image_webp = "/images/fgz2wlo3xl2r6wkgi32fmt3e2y.jpg"
title = "LOS ECONOMISTAS ANTICIPAN UNA ACELERACIÓN DE LA INFLACIÓN EN DICIEMBRE: EL AÑO CERRARÁ CERCA DEL 50%"

+++

#### **_El programa Precios Cuidados, según los analistas, sirvió para garantizar el acceso a ciertos productos, pero tiene un impacto muy limitado en el índice inflacionario._**

Luego de conocerse el índice de inflación de noviembre, que con un 2,5% fue más bajo de lo esperado, las consultoras económicas ya anticipan que la inflación de diciembre se acelerará y rondará el 3%. Por lo tanto, el año cerrará con una suba en el índice de precios cercana al 50% interanual.

Desde Analytica, proyectan que el indice de precios de diciembre va a aumentar 3,2% respecto a noviembre principalmente por una aceleración del ritmo devaluatorio, que puede llevar la inflación núcleo al 3,5%. “En diciembre la actualización de los salarios junto con la inflación importada que no cesa son dos de los principales factores que le ponen un piso alto. Como consecuencia esperamos que la inflación sea en diciembre de 3% terminando el año con un aumento de precios del 49,7%”, aseguró Claudio Caprarulo, director ejecutivo de la consultora.

La inflación de noviembre y los niveles que se proyectan para diciembre mostraron el escaso impacto del programa de Precios Congelados que el Gobierno aplicó para una canasta de 1.400 productos desde octubre pasado y que vence el próximo 7 de enero. Para la próxima etapa, a partir del 8 de enero, desde la secretaría de Comercio ya anticiparon que el programa tendrá una lista de entre 1.200 y 1.300 productos pero con una actualización mensual en los precios, que se revisará cada tres meses.

“Precios congelados es una política de ingresos que busca garantizar el acceso a ciertos bienes, pero desde el día uno señalamos que su impacto en la inflación iba a ser muy bajo. La inflación es un problema macroeconómico, no sectorial”, aseguró Caprarulo.

Con todo, el porcentaje cercano al 50% estimado para de 2021 va a ser un piso para la inflación de 2022. “Mientras el Gobierno no ponga sobre la mesa un plan anti-inflacionario hasta el momento ninguna variable nos permite proyectar una inflación menor al 50% para el año próximo”, alertó el economista.

La consultora EcoGo también prevé que la inflación se acelere y se ubique en 3% para diciembre y el año 2021 también termine con un índice levemente por debajo del 50% interanual. “La suba obedece principalmente al arrastre de los precios de los alimentos de las últimas semanas de noviembre, principalmente en las carnes. De hecho, en las últimas semanas los avances fueron bajos con 0,7% y 0,2% semanal”, detalló Sebastián Menescaldi, director de EcoGo.

“Hubo fuertes subas en noviembre, antes de las elecciones, por incertidumbre y precaución. Luego también se ve el salto de la última semana por los precios de la carne vacuna. Con ese colchón que hicieron hoy regulan. Respecto a los precios congelados, las subas estuvieron mayormente concentradas en los alimentos frescos, que están fuera de la canasta”, explicó el economista.

Este jueves, el secretario de Comercio Interior, Roberto Feletti, habló del impacto de los aumentos de la carne en el índice de noviembre. “Estamos hablando entre 11% y 12% de aumento, de acuerdo a nuestro relevamiento. Habría que descarnar el índice, quitar el efecto carne, y ahí vas a tener la medida justa de si la canasta fue un ancla antiinflacionaria o no”, dijo en declaraciones a El Destape Radio.

Guido Lorenzo, economista de la consultora LCG, señaló que esperan que en diciembre la inflación ronde el 3,5% y un cierre para el año del 50%. “Hacemos una medición sobre alimentos y bebidas en forma semanal y está subiendo en forma generalizada, sobre eso se suman aumentos puntuales, como colegios, que tienen impacto en el gasto de los hogares”, apuntó.

Sobre el congelamiento de precios, el especialista destacó que entre el efecto que provocó la previa —donde hubo fuertes aumentos— y la merma posterior, el programa no tuvo efecto. Los pronósticos de la consultora Abeceb, en tanto, anticipan una inflación de 3,3% para diciembre y de 50,7% para fin de año.