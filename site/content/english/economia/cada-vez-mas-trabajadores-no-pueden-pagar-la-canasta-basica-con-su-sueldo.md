+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/obreros-1.jpg"
image_webp = "/images/obreros.jpg"
title = "CADA VEZ MÁS TRABAJADORES NO PUEDEN PAGAR LA CANASTA BÁSICA CON SU SUELDO"

+++

**_El empleo no es una garantía de evitar la pobreza en la Argentina y así lo refleja el hecho de que uno de cada cinco trabajadores formales no tiene los medios suficientes para vivir en forma digna._**

Este concepto resulta cada vez más certero, de acuerdo con los datos oficiales y a la opinión de los expertos en estadísticas sociales. El promedio histórico de los trabajadores pobres se ubicaba en el 20%, indicó el experto Javier Lindenboim, pero la disminución del número de ocupados y el aumento de la canasta básica se conjugaron para llevar este porcentaje por encima del 30 por ciento. Por lo tanto, cerca de un tercio de los trabajadores totales no alcanza a ganar el mínimo de ingresos para ser pobre, indicaron fuentes oficiales.

Según los datos del Instituto Nacional de Estadística y Censos (Indec) del año pasado, hubo un fuerte aumento de la pobreza entre quienes tienen un trabajo, aunque se trate de un fenómeno de larga data, aclaró Lindenboim.

Los microdatos del Indec del IV trimestre de 2020 conocidos la semana pasada reflejan que un tercio de las personas ocupadas entra en la categoría de pobreza para las estadísticas oficiales; el último dato de la pobreza semestral marca que la tasa fue del 42%, pero llega a 45,1% si se tiene en cuenta los nuevos datos del último cuatrimestre del 2020. De cara a lo que va de 2021, la cifra podría subir aún más por efecto de la inflación.

A partir de la crisis que comenzó en 2018, esta correlación se aceleró, al pasar del 17% al 24% de la población ocupada durante 2018 y luego al 27% cuando Mauricio Macri dejó la presidencia. Un año después, con la pandemia y la cuarentena mediante, el dato ascendió al 33 por ciento.

Al respecto, un estudio de los trabajadores de ATE-Indec indicó que “al conocerse la variación de precios del mes de abril 2021, se hace visible la grave situación socioeconómica del país, que desgraciadamente golpea a los sectores vulnerables de la sociedad, los jubilados y los trabajadores”.

En este sentido, cuestionó los aumentos salariales proyectados y precisó que “el aumento escalonado del Salario mínimo, vital y móvil que recién en febrero del próximo año llegaría a los $ 29.160, apenas por arriba de la Canasta básica alimentaria para un grupo familiar de 4 integrantes que se valoriza actualmente en $25.685”.

Las cifras también muestra un fuerte deterioro en las condiciones de vida de los argentinos, producto de la crisis económica y el impacto de la pandemia. “Sólo 52,3% de los hogares, que abarca al 46,4% de las personas, accede a los tres servicios básicos de agua corriente, cloacas y gas natural”, mencionó el informe del Indec.

En este sentido, ATE cuestionó los aumentos salariales proyectados y precisó que “el aumento escalonado del Salario mínimo, vital y móvil que recién en febrero del próximo año llegaría a los $ 29.160, apenas por arriba de la Canasta básica alimentaria para un grupo familiar de 4 integrantes que se valoriza actualmente en $25.685”.

De esta manera, la inflación acumulada del periodo noviembre del 2015 hasta abril 2021 es del 557%, con una inflación acumulada interanual del 46,3%; mientras tanto, “la pérdida del poder adquisitivo respecto a noviembre del 2015 es del 42,4 por ciento”, precisaron.

El primer lugar del ranking de pobreza lo ocupan los asalariados no registrados –especialmente afectados por el extenso cierre de actividades en 2020 y parte de este año- y luego se ubicaron los cuentapropistas, también con un aumento de casi cuatro puntos desde el cambio de gobierno. En tercer lugar, los empleados registrados, con un aumento mayor de la pobreza en términos relativos, lo que se refleja en el hecho de que aún con un buen trabajo una de cada cinco personas es pobre.

María Belén Rubio de Abeceb dijo a Infobae que “esta situación se da en un contexto de caída del crecimiento, que afecta a todos las formas de inserción laboral, tanto a los trabajadores formales como informales. Luego de un año tan especial como el 2020, este será nuevamente otro de caída del poder adquisitivo del sector formal, con un proceso de reducción del número de trabajadores”.

“Salvo por algunos sectores, como lo puede ser la recuperación de la construcción, en el resto de los actores continúan siendo muy desafiados en términos de empleo”, indicó la economista.

“A esto se suma un proceso de un proceso de empobrecimiento vinculado a la caída del crecimiento y al proceso de aumento de precios que muestra dificultades para bajar, a pesar de la apreciación que estamos viendo en el tipo de cambio sobre todo en los últimos meses”, concluyó.

La tasa de desempleo de Argentina subió a 11% en el cuarto trimestre de 2020 frente al 8,9% registrado en el mismo período del 2019; los analistas privados prevén que terminará en 11,5% a fin de este año. En tanto, la inflación fue del 36% en 2020 luego del 53,8% del 2019 -aunque con la mayoría de los precios congelados- y apunta a superar el 40% este año, según el pronóstico de la mayoría de los analistas privados.