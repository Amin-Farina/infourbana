+++
author = ""
date = 2021-11-02T03:00:00Z
description = ""
image = "/images/roberto-felettijpg-1.jpg"
image_webp = "/images/roberto-felettijpg.jpg"
title = "FELETTI BUSCA \"REGULAR\" EL PRECIO DE LOS MEDICAMENTOS Y LOS FARMACÉUTICOS SALIERON AL CRUCE"

+++

##### **_La Secretaría de Comercio Interior apunta frenar la tendencia alcista del rubro. Piden se cumpla la Ley de Genéricos y que se rompa con los laboratorios monopolizados._**

El secretario de Comercio Interior, Roberto Feletti, manifestó este martes que está analizando el sector farmacéutico y que evalúa controlar el precio de los medicamentos. La posibilidad de un “congelamiento” en el sector despertó el malestar en los afectados. El Sindicato Argentino de Farmacéuticos y Bioquímicos pide que se cumpla con la Ley de Genéricos, y la Cámara Argentina de Farmacias (CAF) afirma que se debe rever el reparto de rentabilidad porque están casi a pérdida.

"No puede haber consumos esenciales como alimentos o medicamentos que no tengan algún grado de regulación del Estado. No puede quedar librado a la asignación de recursos que hace el mercado. Nadie dice que las empresas prestadoras, productoras o laboratorios no tienen que ganar plata, pero esto es así", aseguró el funcionario.

Por tal motivo, Feletti dijo en declaraciones a Radio 10 que "no hay política de ingresos que se sostenga si en los consumos esenciales hay apropiaciones indebidas. Nadie dice que las empresas no tienen que ganar. Pero todos sabemos que a los monopolios, si no se los regula, ajustan por precio y no por cantidad".

#### Panorama del sector farmacéutico

Uno de los puntos que azota a la economía argentina en todos los ámbitos es la inflación. De hecho, la misión con la que asumió el secretario de Comercio Interior fue frenar el alza de precios, y está vez es el turno del sector farmacéutico.

El Sindicato Argentino de Farmacéuticos y Bioquímicos (SAFYB) manifestó en diálogo con PERFIL que el aumento interanual de los medicamentos es del 75%. Es decir, que su incremento supera a la inflación interanual que es del 53%.

De acuerdo con relevamientos efectuados por el Colegio de Farmacéuticos, los precios de los medicamentos promediaron una suba del 45% y los de mayor prescripción médica se incrementaron un 86% en lo que va del año.

El segundo punto es el tema de la monopolización del rubro. Actualmente, en Argentina hay 200 productores, 1000 droguerías y 11 mil farmacias. El secretario general de SAFYB afirmó que el 10% de los laboratorios del país facturan el 80% de los medicamentos.

En línea con el sindicalista, Roberto Feletti, sostiene que "El consumo no puede quedar encorsetado a los planes de negocios de 30 ó 40 empresas. Esto también hay que ponerlo en discusión".

El tercer punto es la rentabilidad que le queda a las farmacias. Alejandro Bobbio, vicepresidente de la CAF, apuntó al gobierno nacional y expresó que “cuando dicen que PAMI da medicamentos al 100%, son las farmacias las que pagan ese beneficio”. Y graficó que cuando venden por la obra social de los jubilados y pensionados tienen tres escenarios: venden a pérdida, venden al costo o venden a una rentabilidad menor al 4%.

Desde la cámara indicaron que las farmacias no son las formadoras de precios sino los laboratorios que se quedan con grandes márgenes de rentabilidad. Sin embargo, la solución no está en el congelamiento sino en “una distribución de ganancias más equitativas” y en que “no sean ellos quienes financien los descuentos que dan IOMA, PAMI o las obras sociales”.

##### ¿Se viene una seguidilla de reuniones?

El secretario de Comercio Interior se reunió con la titular de PAMI, Luana Volnovich;  y este miércoles se reunirá con la ministra de Salud, Carla Vizzotti, para abordar la cuestión.

Altas fuentes ligadas a Roberto Feletti expresaron a PERFIL que “por ahora solo se está analizando el sector y qué es lo que pasa con los precios, razón por la cual, habría varias reuniones en la agenda del funcionario”. Por su parte, desde este medio se contactó a referentes de la obra social pero no respondieron.