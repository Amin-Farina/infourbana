+++
author = ""
date = 2022-01-06T03:00:00Z
description = ""
image = "/images/la-expectativas-estara-puesta-este___s1zvacbgg_1200x630__1.jpg"
image_webp = "/images/la-expectativas-estara-puesta-este___s1zvacbgg_1200x630__1.jpg"
title = "CAEN LOS BONOS ARGENTINOS LUEGO DE QUE GUZMÁN ADMITIERA DIFICULTADES PARA CERRAR CON EL FMI"

+++

#### **_Las bolsas caían con fuerza y el rendimiento de algunos de los principales títulos del Tesoro de los EEUU tocaban su nivel más alto en años el jueves, después de que la Reserva Federal señaló la posibilidad de una subida de tasas de interés en Estados Unidos más rápida de lo previsto y la retirada de los estímulos._**

En ese marco, los bonos Globales argentinos, emitidos con el canje de deuda de septiembre de 2020, caen este jueves un 1% en promedio y el riesgo país supera los 1.760 puntos.

Estos títulos públicos en dólares con ley extranjera son la referencia de la deuda argentina en el exterior y en el breve transcurso de 2022 caen 4% en promedio. Hay que recordar que ya el año pasado habían anotado una baja de cotización del 9% anual en promedio.

El ministro de Economía, Martín Guzmán, aseguró que “un acuerdo no va a resolver todos los problemas de endeudamiento externo de la Argentina”, y afirmó que “la diferencia que separa al país de un acuerdo con el FMI es el sendero fiscal que propone la Argentina para reducir el déficit de forma gradual, de manera virtuosa sobre un mayor crecimiento de la actividad económica”.

El mensaje trajo más escepticismo a los fondos de inversión, que ahora tienen más dudas acerca de la posibilidad de un entendimiento con el organismo de crédito antes de que termine el primer trimestre del año.

“El mercado estima que además se necesitarán medidas relacionadas a un aumento de los ingresos o a una baja del gasto público, y hasta inclusive una combinación de ambas”, confiaron los analistas de Research for Traders.

##### Tendencia bajista desde el exterior

Tanto las bolsas de Asia como las de Europa se desplomaron después de que el Nasdaq perdió más de un 3% en la víspera en Wall Street, y de que el retorno de los bonos del Tesoro estadounidense a dos y cinco años -importantes motores de los costos del endeudamiento a nivel mundial- se dispararon hasta máximos desde que se desató la pandemia en marzo de 2020.

Las minutas de la reunión de diciembre de la Fed mostraron que un mercado de trabajo ajustado y una inflación persistente podrían obligar al banco central estadounidense a subir las tasas antes de lo previsto y a empezar a reducir sus tenencias de activos en general.

El índice paneuropeo STOXX 600 declinaba un 1,3%, borrando todas las ganancias del año, que lo habían llevado hasta máximos históricos. En Asia también hubo fuertes desplomes. Las acciones australianas cayeron un 2,7%, en su mayor descenso porcentual diario desde principios de septiembre de 2020, y el Nikkei japonés perdió un 2,9%, su declive diario más pronunciado desde junio.

“Algunas personas están bastante asustadas por las minutas de la Fed y un posible ajuste más rápido”, dijo a Reuters Carlos de Sousa, de Vontobel Asset Management. “No obstante, quizá el mercado esté exagerando un poco. El hecho de que estén discutiendo esto no significa que vayan a hacerlo”.

La caída del 3,3% del Nasdaq el miércoles fue su mayor descenso porcentual en un día desde febrero del año pasado y el S&P 500 sufrió su mayor descenso desde el 26 de noviembre, cuando la noticia de la variante ómicron llegó por primera vez a los mercados mundiales.

El rendimiento de los bonos del Tesoro seguía subiendo a lo largo de toda la curva. El retorno de las notas referenciales a 10 años tocaba su nivel más alto desde abril de 2021, por encima del 1,73% anual.

Asimismo, el rendimiento de la deuda a dos años, sensible a la política monetaria, alcanzó un nuevo máximo de 22 meses al 0,863%, mientras que el de los papeles a cinco años tocaba su cota más elevada desde febrero de 2020, al 1,459 por ciento.

El alza en el retorno de los bonos seguía respaldando la firmeza del dólar, aunque cedía algo de terreno frente al yen después de haber tocado máximos de cinco años a principios de esta semana, cayendo un 0,21%, a 115,86 unidades.