+++
author = ""
date = 2022-03-23T03:00:00Z
description = ""
image = "/images/foto-noticia.webp"
image_webp = "/images/foto-noticia.webp"
title = "\"GUERRA\" CONTRA LA INFLACIÓN: EL GOBIERNO ANUNCRIARÁ UN PLAN DE PRECIOS CUIDADOS PARA COMERCIOS DE BARRIOS"

+++
#### Lo informará esta tarde Roberto Feletti y será una medida adicional a la anulación de aumentos de las últimas semanas para 580 productos. Según estimaciones privadas, hay productos que debería bajar hasta 26 por ciento

El Gobierno anunciará este miércoles un nueva medida en el marco de la “guerra” contra la inflación: buscará implementar un “mini Precios Cuidados” en comercios de cercanía, donde los programas de precios tienen un nivel de cumplimiento mucho menor que en las grandes cadenas. Es, de todas formas, una medida que ya había ensayado el Poder Ejecutivo el año pasado con el plan Súper Cerca que, afirman desde el sector privado y reconocen fuentes oficiales, nunca terminó de arrancar.

El secretario de Comercio Interior Roberto Feletti presentará esta tarde en público el nuevo programa, que tendría un nombre nuevo y que incluirá, en un espíritu similar al programa intentado el año pasado, una canasta muy reducida de productos básicos alimenticios y de limpieza con precios de referencia. La intención es que ese grupo de bienes tengan penetración en el sector más ramificado del consumo masivo. Según estimaciones oficiales, el 65% de las compras del público se hacen en este circuito, y no en las grandes superficies comerciales.

La canasta nueva tendría unos 70 productos, entre los que hay algunos de los rubros más consumidos. Como referencia, el Súper Cerca incluyó agua mineral, arroz, azúcar, fideos, galletitas, harina, mermelada, pan rallado, tapas de empanadas, yerba, jabón en polvo, leche, queso, yogur, lavandina, limpiador de piso, pan de mesa, pañales, papel higiénico y shampoo, entre otros. Muchos de estos productos también estarán incluidos en el nuevo muestreo para supermercados de barrio y almacenes.

Feletti, por otra parte, detallará el funcionamiento del fideicomiso financiado por la suba de dos puntos porcentuales en los derechos de exportación de la harina y el aceite de soja. Con ese fondo público se subsidiará el precio de la harina de trigo a niveles anteriores al inicio de la guerra en Ucrania. De esta manera, espera el Gobierno, ese subsidio “derramará” en los valores de fideos, el pan y la harina al consumidor. También incluirá un anuncio sobre la implementación de una línea de créditos por $8.000 millones a tasa subsidiada para la industria molinera.

Desde este miércoles, además, el Gobierno da por hecho que los supermercados comenzarán con la retracción de precios al nivel del 10 de marzo pasado, tras el anuncio realizado el martes por Comercio Interior, en el que instó a las grandes cadenas a que volvieran atrás con los valores de sus listas que tenían ese día y en un comunicado en el que apuntó directamente contra algunas empresas líderes de la industria alimenticia, a las que acusó de “operar” contra los programas como Precios Cuidados.

“Se observó que algunas empresas como La Serenísima y Molinos Río de la Plata están priorizando el abastecimiento a los comercios de proximidad por sobre los supermercados, atentando así contra el programa +Precios Cuidados, que funciona en las grandes cadenas de todo el país”, dijo esa dependencia. Las empresas en cuestión operan contra el establecimiento de una canasta regulada, que obra como ancla antiinflacionaria, garantizando el acceso de las y los argentinos a una amplia y diversa cantidad de bienes de consumo masivo”, disparó Feletti.

Entre las empresas nombradas no realizaron comentarios de manera oficial tras haber sido apuntadas directamente por el secretario de Comercio Interior. En algunas compañías se tomó con sorpresa la referencia puntual a dos grandes jugadores del mercado ya que, afirmaron, se trata de dos firmas que están dentro del esquema de Precios Cuidados y que mantienen un diálogo frecuente con las autoridades.

Tal como mencionó Infobae, de todas formas, entre las propias compañías se miran de reojo por los aumentos de las últimas semanas. “Nosotros remarcamos 9% pero hay otras que aprovecharon y aumentaron 15%”, bufaban desde una firma de consumo masivo.

La consultora Focus Market midió cuánto deberían bajar de precio un puñado de productos que tuvieron, según sus registros, fuertes incrementos en las últimas semanas. En un breve muestreo, esa consultora midió que algunos productos como la leche larga vida descremada debería retrotraer aumentos de un 26% entre el 10 y el 23 de marzo.

De esa manera, pasaría de $116 el litro como se vendía hasta ayer a $91,96. Otros tendrán retrocesos de 24% como en el caso de la harina leudante (de $79,21 a $63,83) y de 20% para los grisines con salvado (de $171,50 a $137,20) y las magdalenas (de $92,25 a $73,80).

“La aceleración de precios en las últimas semanas es preocupante pero no solo en Alimentos y Bebidas sino también en Indumentaria y calzado, Transporte, Vivienda y Educación. Es decir, la inflación es la suba generalizada de todos los bienes y servicios de la economía y la variación que se está dando es de una suba de su nivel anterior. Contener este proceso sin un plan de estabilización concreto en el mediano plazo no logrará los resultados buscados” afirmó Damián Di Pace, director de Focus Market.

“Las grandes cadenas de supermercados serían quienes deberán fiscalizar esta acción de retrotraer precios al 10 de marzo cuando le hagan el pedido a su proveedor. Como las empresas tienen argumentos genuinos de suba de precios de insumos, materias primas y servicios el programa tendrá muchas complejidades para hacerse efectivo. Como ya ha sucedido en otras oportunidades de sostenerse en el tiempo ajustará nivel de precios por cantidad de producto entregado en punto de venta”, indicó.

Este miércoles el presidente del Mercado Central Nahuel Levaggi volvió a hablar sobre la idea que manifestaron dirigentes del oficialismo sobre la creación de una empresa nacional de alimentos. En ese sentido, consideró que “la emrpesa ya está en el mercado, no hay que armarla, ya hay una herramienta” para que el Estado intervenga, afirmó en declaraciones radiales.

“Es una propuesta de nosotros. En la cuestión de frutas y verduras hay mucha variabilidad por estacionalidad y cuestiones climáticas. A las 2 de la mañana en el Mercado Central tiene un precio, a las 5 tiene otro y a las 8 tiene otro. La propuesta es poder aumentar la producción y salir con mayor oferta con un valor predeterminado que le sirva al productor y al consumidor”, dijo Levaggi. “El esquema incluye papa, tomate y cebolla, con compras a futuro a los productos para establecer un precio y salir con una oferta masiva”, afirmó.

En ese sentido, cuestionó el alcance de medidas como los acuerdos de precios y dijo que el Gobierno necesita intervenir más en las empresas del sector. “Hay medidas paliativas como los acuerdos de precios que hoy están y mañana no. Tenemos una matriz agroalimentaria dependiente de grandes monopolios. Por un lado el Gobierno no puede controlar la inflación, pero los que aumentan los precios son los privados, no el Gobierno. Nos cuesta poner el ojo y decir ‘¿qué onda las empresas? ¿Qué responsabilidad tienen?’ Los privados se la están guardando en el bolsillo”, dijo en declaraciones a Radio 10.

Este lunes el ministro de Desarrollo Productivo Matías Kulfas se había referido a la posibilidad de una empresa nacional de alimentos y también lo había limitado solo a la intervención vía compras anticipadas de ese grupo de hortalizas. No está determinado aún cuál sería el fondeo que tendría ese fideicomiso público para la compra futura de papa, cebolla y tomate.