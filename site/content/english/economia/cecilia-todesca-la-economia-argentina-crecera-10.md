+++
author = ""
date = 2021-12-02T03:00:00Z
description = ""
image = "/images/5ff73d26d355d-1.jpg"
image_webp = "/images/5ff73d26d355d.jpg"
title = "CECILIA TODESCA: \"LA ECONOMÍA ARGENTINA CRECERÁ 10%\" "

+++

#### **_La secretaria de Relaciones Económicas Internacionales destacó también el crecimiento de las importaciones y consideró que cuando aumentan las compras de insumos "es un reflejo de un crecimiento general de la economía"._**

La secretaria de Relaciones Económicas Internacionales, Cecilia Todesca Bocco, dijo este martes que el crecimiento "se ubicará este año en torno del 10% luego de tres años de recesión, lo que es una buena noticia, y ya estamos 2,8% arriba del nivel de diciembre de 2018".

En paralelo, la funcionaria de la Cancillería afirmó que el Gobierno "trabaja para la recuperación de las reservas", y en ese sentido remarcó que para este año "ya tenemos un superávit comercial en torno de los US$ 13.900 millones, y es un número importante".

##### El acuerdo con el Fondo

Además, en una conferencia de prensa en la Casa Rosada, Todesca Bocco dijo que el Gobierno prevé hacer frente al pago de US$ 1.892 millones al Fondo Monetario Internacional, el 18 de diciembre próximo.

"Por supuesto que Argentina tiene las tensiones en el sector externo que ya se conocen. Sí, la idea es hacer el pago a que usted hace mención", señaló en respuesta a la pregunta de un periodista.

La secretaria, en una conferencia de prensa tras participar de una reunión del Gabinete Económico encabezada por el presidente Alberto Fernández en la Casa Rosada, sostuvo además que "se está trabajando en el plan plurianual y con el Fondo Monetario", como había adelantado el primer mandatario.

"Quisiéramos llegar al mejor acuerdo posible en el menor tiempo posible. Ese es nuestro objetivo. Es una negociación de envergadura y cuando esté todo listo lo enviaremos al Congreso", puso de relieve.

En cuanto a una versión periodística de una financiación ruso saudí de la deuda, Todesca Bocco la rechazó y en cambio dijo que se conversa con ambos países sobre "exportaciones e inversiones productivas", en el marco de una "agenda muy importante".

Sobre la "modificación en la financiación para la compra paquetes y pasajes al extranjero", manifestó: "lo que hacemos es cuidar las reservas para tener dólares suficientes para seguir creciendo. Ese es nuestro objetivo".

"Al mismo tiempo que dijimos que no podemos subsidiar viajes al exterior en cuotas fijas en pesos, sí hicimos algunas modificaciones para que los empresarios puedan hacer pagos adelantados de maquinaria que necesitan para producir", destacó.

Todesca Bocco insistió en que el Gobierno "no quiere castigar a nadie, al contrario, lo que no puede es financiar en pesos a tasa fija porque estaríamos dando un subsidio a un sector de la población que no lo requiere".

En el encuentro, desarrollado en el Salón Eva Perón, también participaron el jefe de Gabinete, Juan Manzur, el canciller Santiago Cafiero; los ministros de Economía, Martín Guzmán; de Desarrollo Productivo, Matías Kulfas; y de Trabajo, Claudio Moroni; el secretario de Asuntos Estratégicos, Gustavo Beliz; la titular de la AFIP, Mercedes Marcó del Pont y -por videoconferencia- el presidente del Banco Central, Miguel Pesce.

"Básicamente se revisó la marcha de la economía y de un conjunto de variables que nos parecen muy relevantes", señaló la funcionaria en el inicio de conferencia de prensa, y subrayó que "se ve una recuperación fuerte de la actividad económica".

##### El crecimiento

Al respecto, señaló que el ministro Guzmán contó que "estamos en torno de un 10% de crecimiento PIB de este año, y éste es un dato muy relevante porque venimos de tres años consecutivos de caída, de manera que crecer 10 puntos este año es una muy buena noticia".

Todesca Bocco explicó que "el crecimiento está traccionado por el consumo, la inversión y por el sector externo, y esto también es muy bueno".

"La inversión va a estar creciendo en torno al 30%, hay que esperar los datos, pero eso indica que se logra más inversión cuando hay demanda", indicó.

Asimismo, apuntó que la industria "se ha recuperado mucho y eso es muy importante porque es traccionadora de otros sectores y además paga buenos salarios en general y ya estamos 8,1 arriba de 2019".

También dijo que "se ve una recuperación muy importante de la producción nacional" y, en ese aspecto, sostuvo que el ministro Kulfas dio "números muy importantes como el patentamiento de maquinaria agrícola", sector en el que "el 73% es de origen nacional".

En tanto, indicó que "en los patentamientos de autos el 54% es de origen nacional", y agregó: "eso nos importa porque esas industrias traccionan otras industrias y eso es lo que finalmente genera empleo".

Acerca de la recuperación del empleo", explicó que "también es buena y tenemos 10 meses consecutivos de aumento del empleo asalariado privado".

En cuanto a una de las tareas del Gobierno en pandemia, Todesca Bocco expresó que "el primer trabajo fue dar vuelta las suspensiones ya que teníamos por efecto de la pandemia una cantidad muy importante, en torno a 600 mil trabajadores con suspensiones, y se dieron vuelta".

"Esos trabajadores -precisó- no perdieron su empleo y ahora cobran el total de su salario, eso es lo primero a la salida de la pandemia, y sobre eso hemos recuperado desde el punto más bajo unos 140 mil empleos y estoy hablando del sector sector privado registrado".

En paralelo, detalló que en el empleo industrial "tenemos 42 mil empleos más que en el 2019 ,y tenemos industrias como la de software que está generando unos1000/ 1.100 empleo por mes".

En cuanto a la construcción, consignó que los números "dan muy bien también con un 3,2 arriba del 2019, y tenemos el nivel de permisos para construir que es el mayor de los últimos cinco años y esto es importante en términos de empleo".

La funcionaria de la Cancillería, sobre el sector externo, remarcó que "tenemos un año con una recuperación muy importante de las exportaciones que crecieron un 39%, en torno al 40%, al mismo tiempo que las importaciones que han crecido en torno al 50%".

"Estamos -aseguró- en un nivel de exportaciones cercano a los 6 mil millones mensuales cuando en 2019 estábamos en torno a los 4 mil. Y esto es lógico porque cuando la economía está creciendo, cuando sucede esto necesitamos importar un montón de insumos".