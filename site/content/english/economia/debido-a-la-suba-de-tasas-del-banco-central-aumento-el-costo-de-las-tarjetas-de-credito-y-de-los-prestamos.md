+++
author = ""
date = 2022-01-14T03:00:00Z
description = ""
image = "/images/nfdloft3svg6fnvcdwonejbbje.jpg"
image_webp = "/images/nfdloft3svg6fnvcdwonejbbje.jpg"
title = "DEBIDO A LA SUBA DE TASAS DEL BANCO CENTRAL, AUMENTÓ EL COSTO DE LAS TARJETAS DE CRÉDITO Y DE LOS PRÉSTAMOS"

+++

#### **_Es el interés que cobran los bancos por refinanciar el saldo pendiente de los plásticos, que está regulado desde 2020. Además, se incrementó el costo financiero para las empresas que pidan créditos de las líneas de inversión productivas_**

La semana pasada el Banco Central (BCRA) hizo un primer guiño al FMI y decidió aumentar su tasa de referencia, que pasó de 38% a 40%. La medida repercutió positivamente en los rendimientos de los plazos fijos, pero también aumentó los costos del crédito.

En particular, afecta a dos tasas de créditos que estaban reguladas por el BCRA:

* El costo de refinanciación de los saldos en tarjeta de crédito.
* La tasa de algunas de las líneas de créditos subsidiados para mipymes.

Varios bancos consultados por TN reconocieron que están estudiando cómo implementarán la suba de tasas que habilitó el BCRA. Algunos ya están informando los aumentos a los clientes.

##### ¿Cuánto costará refinanciar la tarjeta de crédito?

Desde 2020 el BCRA viene limitando la tasa que los bancos pueden cobrar a los clientes para refinanciar los saldos adeudados en tarjetas de crédito.

Esa operación, que en la jerga financiera se conoce como revolving, es usada por aproximadamente el 40% de los titulares de plásticos. Son aquellos que no llegan a pagar el total del resumen y hacen un depósito parcial, ya sea el mínimo o algo más, y refinancian el resto.

Desde hacía un año, el BCRA había establecido que la tasa para refinanciar saldos de hasta $200.000 tenía un tope de 43% nominal anual.

A partir de la comunicación que elevó la tasa de referencia, en tanto, ese costo máximo subió seis puntos hasta 49% para financiar esos mismos montos.

El aumento puede empezar a regir desde la facturación de enero, pero los bancos tendrán que comunicar a sus clientes sobre el cambio en los costos.

Cuando el saldo a refinanciar supere los $200.000, la tasa de interés compensatorio tampoco podrá superar en más del 25% a aquella que resulte del promedio del costo que el banco haya aplicado a los préstamos personales que otorgó en el mes previo.

##### Sube el costo de los préstamos subsidiados

En plena pandemia, el BCRA relanzó las líneas de inversión productivas, con tasas subsidiadas para las mipymes y empresas de salud, entre otros rubros.

Hasta ahora, el costo máximo que podían aplicar los bancos a esos créditos era de 35% nominal anual, ahora esa tasa máxima subió a 41% en los siguientes casos:

* Cuando se trate de la financiación de capital de trabajo y al descuento de cheques de pago diferido y facturas de crédito electrónicas mipyme.
* Si el crédito se otorga a empresas de cualquier tamaño cuya actividad principal sea prestar servicios relativos a la salud o bien destinen los fondos a adquirir maquinarias y equipos producidos por mipymes argentinas.