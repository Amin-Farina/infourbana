+++
author = ""
date = 2022-01-24T03:00:00Z
description = ""
image = "/images/espacio-entre-las-plantas-de-soja-en-una-plantacion-de-una-granja-88905818.jpg"
image_webp = "/images/espacio-entre-las-plantas-de-soja-en-una-plantacion-de-una-granja-88905818.jpg"
title = "TRAS LA OLA DE CALOR, ESTIMAN PÉRDIDAS DE HASTA EL 50% EN LA SOJA"

+++

##### **_La Bolsa de Rosario reportó las mermas de rinde que se esperan en algunos lotes de la zona núcleo a pesar de las últimas lluvias que permitieron aliviar al cultivo. No obstante, en la oleaginosa de segunda se espera una recuperación._**

A pesar de las últimas lluvias, se esperan mermas del 10% al 50% en los rindes de soja en la zona núcleo, según el panorama semanal de la Bolsa de Comercio de Rosario (BCR).

“Vemos ramilletes de chauchas muy pequeños y envejecidos, altura de planta muy baja. El cultivo puede reaccionar rebotando y haciendo las últimas tandas de flores y tener una última oportunidad para un rinde aceptable. Pero estamos a solo 60 días de la cosecha. El peso de granos me parece que va ser pobre y desparejo. Hay un 20% a 30 % de reducción de rinde sin posibilidades de que cambie”, dijeron en Cañada Rosquín sobre la soja de primera. “Por eso, incluso con precipitaciones, ya se estiman pérdidas del 10% al 50%”, señaló la Bolsa.

No obstante, la BCR aclaró: “Las últimas lluvias permitieron un mejoramiento en la condición de la oleaginosa, los lotes regulares a malos disminuyeron 5 puntos, ahora totalizan el 30%, y aumentaron los que estaban en buenas a muy buenas condiciones, pasando a ser 70%”.

Pero el informe puntualizó que la sequía y la ola de calor del este mes dejaron su marca. “En Cañada de Gómez, se calcula una pérdida del rinde potencial en el 80% de los lotes. En Bigand solo con una nueva lluvia de 60 a 70 mm se verá una mejoría en el cultivo. En San Pedro, noreste de Buenos Aires, las plantas están muy desmejoradas, quedaron muy chicas. En General Villegas, al NO bonaerense, se observa aborto de vainas”, ejemplificaron.

En tanto, más de la mitad de los cuadros de soja de primera de la región están produciendo chauchas (entre R3 y R4). Sin embargo, hay un 45% que está en la última etapa de floración (R2) y aún “con cierta oportunidad si logran generar la última tanda de flores”.

##### La soja de segunda todavía tiene potencial

Con respecto a la soja de segunda, el panorama semanal indicó que la mitad de los lotes aún está en estado vegetativo. “Gracias a las últimas lluvias, se espera una recuperación del cultivo. Esto es que vuelva a producir hojas y retomar el crecimiento. Pero las precipitaciones tienen que acompañar en el resto del ciclo del cultivo”, agregaron.

En Carlos Pellegrini, Rojas y Laborde son optimistas: estiman que no habrá bajas importantes de los rendimientos. “En El Trébol, la mayoría de los lotes aún conserva el potencial de rinde intacto. Sin embargo, existen algunos cuadros que costara más recuperar. En Bigand, aquellos lotes donde el trigo tuvo buen rendimiento, debe llover muy bien para salvar a la soja de segunda. En suelos complejos, hay áreas donde el cultivo desapareció. Allí, se estima que entre un 15% y 20 % de la superficie de soja de segunda está perdida. En General Villegas la merma se espera por el atraso de la fecha de siembra”, concluyó el reporte.