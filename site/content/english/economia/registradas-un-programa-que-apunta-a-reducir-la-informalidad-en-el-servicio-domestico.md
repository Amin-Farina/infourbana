+++
author = ""
date = 2021-09-27T03:00:00Z
description = ""
image = "/images/60f17a733cdf0_1420_.jpg"
image_webp = "/images/60f17a733cdf0_1420_.jpg"
title = "\"REGISTRADAS\": UN PROGRAMA QUE APUNTA A REDUCIR LA INFORMALIDAD EN EL SERVICIO DOMÉSTICO"

+++

#### **_El Estado nacional pagará una parte del sueldo de la trabajadora durante seis meses, mientras la parte empleadora deberá registrarla y pagar sus aportes, contribuciones, ART y el porcentaje del sueldo restante._**

El gobierno nacional presentó este lunes"Registradas", un programa queposibilitará "la reducción de la informalidad en el sector de las trabajadoras de casas particulares", así como también la reactivación de esa franja de la actividad y su bancarización y garantizará su acceso un empleo registrado.

Así lo informó la ministra de las Mujeres, Géneros y Diversidad, Elizabeth Gómez Alcorta,en una conferencia de prensa que ofreció en Casa Rosada junto al ministro de Trabajo, Claudio Moroni; el presidente del Banco Nación, Eduardo Hecker, y la titular de AFIP, Mercedes Marcó del Pont.

Gómez Alcorta dijo que "Registradas" apunta aformalizar y a "subsidiar entre un 30 y 50 por ciento del salario de las trabajadoras", en un esquema en el que se pagará durante seis meses hasta la mitad del sueldo de las trabajadoras de casas particulares que sean blanqueadas.

"Se trata de un sector en el que una de cada cinco mujeres del país trabaja en casas particulares, y donde cayó la contratación porque tuvo un impacto muy fuerte durante la pandemia" de coronavirus, sostuvo la funcionaria, luego de un encuentro con el presidente Alberto Fernández y otros ministros para ultimar los detalles del programa.

A través del plan, que entrará en vigencia a partir del primero de octubre venidero, el Estado nacional pagará una parte del sueldo de la trabajadora durante 6 meses, mientras la parte empleadora deberá registrarla y pagar sus aportes, contribuciones, ART y el porcentaje del sueldo restante, comprometerse a mantener la ocupación durante cuatro meses más luego de concluir el plazo, explicó.

"El apoyo económico se brindará a las familias empleadoras que cobren por debajo de 175.000 pesos y va a ser equivalente a un monto entre el 30 y el 50% del valor del salario que esté conveniado, con un tope de 15.000 pesos", detalló la ministra.

Señaló que hasta ahora los empleadores "solo tenían como incentivo para la registración la deducción de los costos del impuesto a las ganancias, por eso es una medida progresiva que apunta a la clase media, donde muchos, por el impacto de la pandemia, han tenido que restringir esas contrataciones en un sector en el cual el 98 % de trabajadores son mujeres y el 2% restante está ocupado por jardineros y choferes".

"Por eso nosotros queremos apoyar y dar un alivio a esas familias de clase media que en su organización al reactivarse la vida y de las actividades laborales y escolares, necesitan esos apoyo de la economía del cuidado", subrayó Gómez Alcorta.

Cómo acceder al programa

Para poder acceder a la política, la parte empleadora deberá percibir ingresos mensuales inferiores al mínimo no imponible del Impuesto a las Ganancias, y será la responsable de afrontar el porcentaje restante del sueldo, los aportes, las contribuciones y de la cuota de la ART de la trabajadora inscripta o del trabajador inscripto, según se informó oficialmente.

En el caso de la trabajadora, deberá trabajar como mínimo 12 horas semanales y podrá estar inscripta únicamente bajo la nómina de un empleador o empleadora.

Además, al acceder al Programa podrá continuar siendo beneficiaria de la Asignación Universal por Hijo, la Asignación Universal por Embarazo, la Tarjeta Alimentar, el Plan Progresar, y de Potenciar Trabajo, entre otras políticas.

La medida se aplicará a aquellas relaciones laborales nuevas con personal de casas particulares, y el empleador o empleadora deberá mantener el puesto de trabajo los cuatro meses posteriores a la finalización del beneficio.

Cómo se implementa

Cuando el empleador o empleadora dé el alta a la nueva trabajadora en la AFIP, el Estado creará una cuenta sueldo en el Banco de la Nación Argentina para ella y transferirá allí, de manera mensual, un porcentaje del salario neto declarado (30 o 50 por ciento según corresponda), por un monto de hasta 15.000 mensuales.

El porcentaje del sueldo que paga el Estado dependerá del ingreso mensual de las y los empleadores.

Será un 50 por ciento de la remuneración neta mensual mínima cuando la parte empleadora tengan un ingreso bruto mensual inferior al 70 por ciento del mínimo no imponible del Impuesto a las Ganancias (que actualmente es de 117.374)

En cambio, será de un 30 por ciento, cuando la parte empleadora cuente con un ingreso bruto mensual que esté comprendido entre el 70 y el 100 por ciento del mínimo no imponible.

Además, para poder solicitarlo, durante los 12 meses anteriores a la entrada en vigencia del decreto, el empleador o empleadora debe haber percibido en promedio mensual ingresos brutos de cualquier naturaleza iguales o inferiores al mínimo no imponible del Impuesto a las Ganancias, es decir 175 mil pesos.