+++
author = ""
date = 2021-06-24T03:00:00Z
description = ""
image = "/images/dolar-2.jpg"
image_webp = "/images/dolar-1.jpg"
title = "CONTENER LA SUBA DEL DÓLAR LE CUESTA U$D 15 MILLONES POR DÍA AL BANCO CENTRAL"

+++

**_El Banco Central tuvo que intervenir con USD 26 millones en dos días para evitar la suba de los dólares financieros._**

El monto continúa creciendo, debido a que el martes tuvo que vender bonos en dólares contra pesos por USD 30 millones nominales que equivalen a USD 11 millones billete y ayer el monto de bonos AL30C fue de USD 43,1 millones nominales (USD 15,3 millones). A esto hay que sumarle la intervención con el AL30 que, contra pesos, operó USD 65 millones que equivalen a $ 3.850 millones.

Ese dinero sale de las reservas porque con una parte de las compras diarias de billetes que hace la entidad, adquiere bonos AL30, que los incorpora a las reservas, para intervenir en la plaza. Con una mano vende y con la otra compra. Mientras el dólar estuvo tranquilo las operaciones en este bono no sobrepasaban USD 300.000 en billetes.

El movimiento comenzó el miércoles de la semana pasada. Ese día tuvo que intervenir con USD 4 millones que los distribuyó con cautela a lo largo de la rueda. Al día siguiente, vendió USD 10 millones y el viernes, USD 6 millones. En una semana, el dólar bajó 46 millones el volumen de las reservas y es una cantidad creciente que comienza a preocupar más que por el monto, por la intensidad con que sale la mesa de dinero del Central a intervenir. 

> “Están muy desprolijos porque se ve que están urgidos desde arriba. Un operador experimentado no actúa tan apresurado y vende como sea con tal de bajar al dólar. Por supuesto, todos se aprovechan de esa desesperación del Central para hacerse de dólares baratos a través de los bonos y cada día le piden más. Las aperturas de los dólares financieros marcan alzas de hasta 1,5%. Un comienzo así los asusta y comienzan a intervenir desde temprano”, aseguró un experimentado operador. “Volvieron a los días de verano en cuanto a desprolijidades”, agregó.

Lo cierto que el dólar mostró dos caras. El contado con liquidación en la plaza oficial, donde se opera contra el bono AL30 cerró a $ 163,97, el mismo valor del día anterior, pero en las mesas de dinero donde las transacciones se hacen con el GD30 cotizó a $ 167,77, o sea que hay una brecha de casi $ 4 que resulta exagerada para este mercado. Cabe aclarar que en la plaza que maneja el Central el precio de este dólar quedó sin cambios, pero en las mesas subió $ 1,40. El dólar MEP con negocios por USD 42,6 millones aumentó 29 centavos a $ 160,92 y en la plaza libre, avanzó 75 centavos a $ 162,25.

El “blue”, en el mercado libre, y casi sin negocios, aumentó $ 1 a $ 166, pero no es el dólar que preocupa al Banco Central porque es una plaza que opera escasas divisas.

Los bonos de la deuda, que comenzaron con alzas de 25 centavos en el exterior, terminaron casi neutros por eso el riesgo país quedó casi sin cambios y bajó apenas 1 unidad a 1.534 puntos básicos. Los bonos indexados más cortos quedaron neutros y los más largos subieron hasta 0,43% como fue el Discount que tiene su propia legión de seguidores y nació con el canje de la deuda de Néstor Kirchner. Los montos que se operan en este título son crecientes.

El dólar mayorista subió 4 centavos a $ 95,54 con operaciones por USD 265 millones. El Banco Central compró y vendió y el final de la rueda lo sorprendió sin saldo a favor. Las reservas, en tanto, aumentaron USD 26 millones a USD 42.400 millones.

En tanto, los depósitos UVA redujeron su crecimiento de algo más de $ 1.000 millones diarios a la mitad al conocerse la variación de precios de mayo y por la proximidad a su vencimiento (el plazo mínimo es 90 días) de las elecciones.

Las acciones tuvieron un día positivo que hubiera si mejor de no ser por la mala performance de los bancos. De todas maneras, el monto de negocios se mantuvo en niveles prudentes de $ 1.225 millones. El S&P Merval, el índice de las acciones líderes, subió 0,28% después de haber estado 1,5% arriba. Lo mejor estuvo en Ternium (+4,27%), seguido de Telecom (+4,03%) y la cementera Holcim (+3,30%). Las caídas correspondieron a BBVA (-3,08%), Banco Supervielle (-3,06%) y Pampa Energía (-2,97%).

Los ADR’s -certificados de tenencias de acciones que cotizan en las Bolsas de Nueva York- perforaron el techo de los $ 3 mil millones. Se negociaron $ 3.117 millones por el entusiasmo que hubo en Wall Street tras las declaraciones de los miembros de la Reserva Federal de cómo iban a manejar la política monetaria ante una inflación que calificaron de “transitoria”.

Entre los certificados argentinos lo mejor pasó por Bioceres (+7,14%), Corporación América (+4,02%) y Telecom (+3,89%).

Para hoy se espera otra ardua intervención del Banco Central en la plaza cambiaria. La orden no es del ministro de Economía, Martín Guzmán, sino de la parte más alta del poder porque el precio de la moneda dejó de ser un tema económico, ahora es electoral.