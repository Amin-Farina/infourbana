+++
author = ""
date = 2022-02-28T03:00:00Z
description = ""
image = "/images/yamana-gold-1-1.jpg"
image_webp = "/images/yamana-gold-1.jpg"
title = "YAMANA GOLD ASUMIÓ EL COMPROMISO DE TRANSICIÓN ENERGÉTICA"

+++

##### **_La transición energética comienza a ganar espacio en la agenda pública y la industria minera a asumir compromisos formales con objetivos concretos, como el caso de la canadiense Yamana Gold, que en Catamarca opera el Proyecto Integrado MARA._**

De acuerdo a un comunicado oficial, la compañía asumió el compromiso global de reducción de gases invernadero de cara al 2030, planteado en la conferencia sobre cambio climático y el Acuerdo de Paris hace algunos años, y recientemente refrendado en la Conferencia de la Naciones Unidas sobre el Cambio Climático celebrado en Glasgow en octubre de 2021, mediante el Pacto Climático de Glasgow (Glasgow Climate Pact), un documento que contiene las guías de acción política acordadas entre todos los países, y que se basa en limitar el calentamiento futuro del planeta a 1,5 grados C desde la era preindustrial, llevando adelante las medidas que sean necesarias para evitar superar el límite prestablecido.

“Desde comienzos de 2021, hemos trabajado y alineado nuestra estrategia para alcanzar el objetivo de 1,5ºC en comparación con los niveles preindustriales. Se estima que la tasa anual de reducción de emisiones necesaria para alcanzar el objetivo en 2030 oscila entre el 4% y el 5% y se espera que solo requiera una cantidad incremental de inversión. Desde Yamana Gold continuaremos evaluando nuevas oportunidades para mejorar nuestros objetivos, incluida la adopción de tecnologías avanzadas en nuestras operaciones. Esto nos permitirá continuar en el camino de la producción de aproximadamente el 85% de onzas de oro con energía renovable para fines de 2022″, aseguraron desde la minera.

A su vez explicaron que “todo el trabajo realizado se hizo conforme con las mejores prácticas internacionales en evolución, incluido el Protocolo de GEI, las directrices de la Iniciativa de objetivos basados en la ciencia y el Protocolo de cambio climático hacia una minería sostenible de la Asociación Minera de Canadá”.

La capacidad de las mineras para cumplir con el escenario del límite de temperatura de 1,5ºC debe darse en parte por el resultado de un enfoque de trabajo en minas eficientes, contar con controles exhaustivos y eficaces por parte de los organismos de control, y además operar en jurisdicciones con gran proporción de electricidad verde disponible, como por ejemplo: electricidad renovable, utilización de vehículos eléctricos a batería, automatización y otras tecnologías emergentes.

En Argentina

La minería en Argentina corre la misma suerte, y las compañías que operan en el país alinean sus objetivos locales con los de sus casas matrices para impulsar su producción de manera eficiente y lo más “verde” posible.

En el caso de la Minera Alumbrera, que forma parte del Proyecto Integrado MARA, se informó que aseguró la provisión de energía renovable de aproximadamente 50% de su demanda energética total por 10 años. Para ello utiliza el parque eólico “Los Olivos”, próximo a la localidad de Las Achiras, en la provincia de Córdoba. Su consumo energético, luego de la paralización de las actividades a cielo abierto del yacimiento, es de aproximadamente 7,5MW de potencia dado por las actividades de cuidado y mantenimiento de las instalaciones, actividades de rehabilitación progresiva de los depósitos de estéril y todas las tareas ambientales de cierre de mina.

##### Sin minería no hay transición

Se sabe que todos los objetivos planteados para reducir el calentamiento global giran en torno, sobre todo, a la reducción de la huella de carbono y los subsidios fósiles, mediante la transición energética. Todas las fuentes de energía renovables, y la electromovilidad necesitan, como insumo básico, minerales industriales como el cobre y el litio, producidos únicamente por la industria minera.

Es así que se plantea que, sin minería, no sería posible la transición energética. Por eso, se explica en los foros internacionales, es vital la iniciativa de las empresas mineras, como la de Yamana, de asumir compromisos formales y alcanzables en cuanto al objetivo global de limitar el calentamiento futuro del planeta a 1,5 grados Celsius (2,7 grados Fahrenheit) desde la era preindustrial.