+++
author = ""
date = 2022-04-21T03:00:00Z
description = ""
image = "/images/27sgfa3sh7ncl5autweg2gyxta.jpg"
image_webp = "/images/27sgfa3sh7ncl5autweg2gyxta.jpg"
title = "LA BANCARIA LANZÓ UN PARO NACIONAL EN RECLAMO DE AUMENTO SALARIAL"

+++
#### El gremio anunció la huelga para el 28 de abril en los bancos públicos y privados. Es en reclamo por la paritaria. Cerrarán sus puertas y se reprogramarán los pagos de jubilaciones y asignaciones.

El jueves 28 de abril habrá un paro nacional de bancos. La medida de fuerza, convocada para reclamar por una mejora salarial y mejores condiciones laborales, afectará a entidades públicas y privadas, que cerrarán sus puertas. Además, se reprogramarán los pagos de jubilaciones y asignaciones de la ANSES

Tras rechazar en reunión paritaria la propuesta presentada por las entidades financieras, de un aumento salarial del 50% con una revisión en septiembre, el sindicato La Bancaria anunció que irá a la huelga en todo el país.

##### Cuándo es el paro nacional bancario

El paro nacional bancario será el jueves 28 de abril. Afectará a las sucursales de entidades públicas y privadas de todo el país.

La medida se tomó en medio de la discusión con las cámaras empresariales ante el Ministerio de Trabajo por la paritaria 2022.

##### Por qué se realiza el paro nacional bancario

Tal como informó La Bancaria en el comunicado que difundió este miércoles, el gremio reclama un recomposición salarial para los empleados y mejoras laborales en el sector. Ante la alta inflación, el sindicato reclama una suba por encima del 60% Además, pretenden:

* Regulación de la ley de teletrabajo;
* Guardería universal;
* Terciarizaciones;
* Desarticulación del trabajo bancario;
* Reconversión de los puestos laborales respecto del avance tecnológico;
* Frenar el cierre de sucursales;
* Derogar las circulares del BCRA que permiten brindar servicio financiero por fuera del sistema bancario.

##### Cómo afectará el paro bancario

A raíz de la medida lanzada por La Bancaria, los bancos públicos y privados permanecerán cerrados el 28 de abril. Por eso, las entidades deberán reprogramar los pagos de las asignaciones y jubilaciones para quienes busquen cobrar por caja.

“Damos aviso con suficiente antelación para prever y organizar todo lo referido al pago de jubilaciones, como así también las distintas asignaciones sociales que deban pagarse ese día en las entidades bancarias, y hacemos responsables a las cámaras patronales por cualquier tipo de inconveniente en ese sentido”, sostuvo La Bancaria en un comunicado.

##### Qué dijo La Bancaria sobre el paro del 28 de abril

A través del comunicado, La Bancaria aseguró que las cámaras empresariales ofrecieron ante el Ministerio de Trabajo “una propuesta insuficiente” y las acusó de dilatar el acuerdo salarial.

Y se quejaron porque las propias entidades “sostienen ante el BCRA, en REM, un pronóstico inflacionario para el presente año, pero por otro lado pretenden dar migajas a sus trabajadores”.