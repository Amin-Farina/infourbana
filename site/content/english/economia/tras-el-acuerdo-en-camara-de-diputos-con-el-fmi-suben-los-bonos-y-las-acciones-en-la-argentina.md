+++
author = ""
date = 2022-03-11T03:00:00Z
description = ""
image = "/images/camara-diputados-acuerdo-fmi.jpg"
image_webp = "/images/camara-diputados-acuerdo-fmi.jpg"
title = "TRAS EL ACUERDO EN CÁMARA DE DIPUTOS CON EL FMI, SUBEN LOS BONOS Y LAS ACCIONES EN LA ARGENTINA"

+++
#### Los títulos públicos en dólares avanzan 1%, mientras que el riesgo país cede debajo de los 1.800 puntos. Toma de ganancias para acciones

Los mercados mostraron una primera reacción positiva a la media sanción en Diputados del acuerdo con el Fondo Monetario Internacional (FMI) que logró una parte del oficialismo junto al principal bloque opositor.

La Bolsa de Comercio porteña abrió el viernes con un alza de 1,3% en su principal índice, el S&P Merval, que se ubicaba en 91.400 puntos y ya acumula una mejora en pesos del 9,5% en lo que va del año, un número superior a la inflación.

Mientras en Wall Street los índices marcan alzas de 1% , los ADR y las acciones argentinas negociados en dólares exhibían alzas en torno al 1,5% en títulos como YPF y Grupo Galicia.

Por otra parte, el riesgo país de JP Morgan, que mide la brecha de tasas de los bonos del Tesoro de los Estados Unidos con países emergentes, cae 38 unidades para la Argentina hasta los 1.787 puntos básicos, después de haber anotado un máximo de 1.991 puntos.

A nivel mundial, los mercados observan indicios de paz en Ucrania aunque persisten los temores sobre el impacto de la guerra sobre la inflación y la economía.

El S&P 500 subió 0,6% en la apertura, después que en la víspera registró su quinta caída en seis días. El Dow Jones Industrial avanzó 339 puntos, 1%, a 33.513, en tanto el Nasdaq aumentó 0,5%.