+++
author = ""
date = 2022-06-01T03:00:00Z
description = ""
image = "/images/soja.jpg"
image_webp = "/images/soja.jpg"
title = "LA LIQUIDACIÓN DE LA SOJA SE ENCUENTRA EN EL RITMO MÁS LENTO DE LA ÚLTIMA DÉCADA"

+++
#### Un informe de la agencia Bloomberg señaló que, hasta la tercera semana de mayo, se había comercializado apenas un 18% de la cosecha argentina a pesar de la escalada de las cotizaciones internacionales. Las exportaciones de soja son uno de los principales rubros generadores de divisas para el país.

Las ventas del poroto de soja se encuentran en su ritmo más lento de los últimos 10 años en la Argentina y se había comercializado apenas un 18% de la cosecha hasta la tercera semana de mayo, limitando el abastecimiento de los mercados mundiales de harina y aceite de este cultivo, según un informe de la agencia económica _Bloomberg_, en base a datos de la Bolsa de Cereales de Buenos Aires (BCBA) y el Ministerio de Agricultura de la Nación.

Esta demora en la comercialización de los granos complicaría al Banco Central, que depende de las exportaciones de la oleaginosa para acumular más reservas en la divisa norteamericana. De hecho, se espera que el complejo sojero logre ingresos récord por más de US$24.000 millones durante este año.

Si bien la soja se encuentra cerca de los récords históricos en el mercado Chicago, con una tonelada que ronda los US$630, lo cierto es que los productores reciben un 40% de ese valor en dólares, por la brecha cambiaria y las retenciones del 33%, según indicó la consultora _Az Group_ en un informe que TN publicó días atrás.

Esto se debe a que los productores reciben el pago de su cosecha en pesos al dólar oficial, descontadas aparte las alícuotas. Del monto que les queda en manos, además, luego deberán afrontar el pago de otros impuestos, como Ganancias, sumado a los costos de producción.

##### El campo exportó un récord de US$ 4200 millones en mayo

Pese al menor ritmo de venta de la soja, durante mayo el sector agroindustrial liquidó exportaciones por US$4231,7 millones, lo que significa un 33% por encima de abril y el mejor monto para ese mes desde que la Cámara de la Industria Aceitera de la República Argentina (CIARA) y el Centro de Exportadores de Cereales (CEC) llevan registros al respecto.

Ambas entidades precisaron, además, que en los primeros 5 meses del 2022, las cerealeras ingresaron al país US$15.329,6 millones, según el reporte de ambas entidades, que en conjunto representan un 48% de las exportaciones argentinas.

“El total de las divisas ingresadas mensualmente quedan en poder del Banco Central, que entrega pesos, a tipo de cambio de oficial, a los exportadores para poder realizar las operaciones de compra y venta de granos en el mercado nacional”, aclararon.

Según Ciara/CEC, “la exportación de granos se vio afectada por los movimientos bruscos en el mercado internacional, por la existencia de volúmenes de exportación en maíz y trigo regulados por el Ministerio de Agricultura, así como por una enorme capacidad ociosa de la industria de molienda de soja que sigue trabajando con márgenes negativos aun en plena campaña gruesa”.

A su vez, explicaron que la mayor parte del ingreso de divisas del sector se produce “con bastante antelación a la exportación, que ronda los 30 días en el caso de la exportación de granos y alcanza hasta los 90 días en el caso de aceites y harinas proteicas”.

Esa anticipación depende también del momento de la campaña y del grano de que se trate, por lo que “no existen retrasos en la liquidación de divisas”, consideraron.