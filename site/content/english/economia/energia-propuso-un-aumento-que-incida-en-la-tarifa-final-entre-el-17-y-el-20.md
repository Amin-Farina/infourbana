+++
author = ""
date = 2022-02-17T03:00:00Z
description = ""
image = "/images/60096b7d4c5ff-1.png"
image_webp = "/images/60096b7d4c5ff.png"
title = "ENERGÍA PROPUSO UN AUMENTO QUE INCIDA EN LA TARIFA FINAL \"ENTRE EL 17% Y EL 20%\""

+++

##### **_Así lo planteó el director nacional de Regulación del Mercado Eléctrico Mayorista, Marcelo Positino, al exponer en el inicio de la audiencia pública para analizar las propuestas de un nuevo cuadro tarifario provisorio_**

La Secretaría de Energía planteó hoy ajustes en las distintas etapas de transporte y distribución de electricidad para que se reflejen en un incremento de "entre el 17% y el 20%" en la tarifa establecida en la factura de los usuarios.

Así lo planteó el director nacional de Regulación del Mercado Eléctrico Mayorista, Marcelo Positino, al exponer en el inicio de la audiencia pública para analizar las propuestas de un nuevo cuadro tarifario provisorio para las distribuidoras Edenor y Edesur, que operan en el Área Metropolitana de Buenos Aires (AMBA) bajo la órbita del Ente Nacional Regulador de la Electricidad (ENRE)

> "Se plantea la fijación de precios que se defina de forma tal cuyo impacto final en las facturas de los usuarios oscile entre el 17% y el 20%", expresó Positino.

La audiencia pública es presidida por la interventora del ENRE, María Soledad Manín, y comenzó a las 8.08 con una breve intervención del secretario de Energía, Darío Martínez, quien planteó como premisa el "compromiso" del Gobierno para que "los aumentos tarifarios siempre sean inferiores a la evolución de los salarios".

Positino señaló en su exposición que en 2021, en un contexto de crecimiento de la demanda y la generación, hubo una emergencia hídrica caracterizada por la bajante más aguda del caudal del río Paraná en 77 años.

En consecuencia, la generación de energía hidroeléctrica cayó 17,1% y debió recurrirse a una mayor generación térmica (con un mayor consumo de combustibles, que en el caso del gasoil creció un 137%) y, en menor medida, por energías renovables.

> "Por estas circunstancias se prevé que los costos aumenten en 2022 respecto de 2021", advirtió el funcionario.

Asimismo, puntualizó que si los subsidios a la energía eléctrica aumentaron en 2021 fue "principalmente por los costos de generación, debido a la devaluación y el precio de los combustibles", que representaron un valor adicional de $ 394.326 millones.

En ese sentido, indicó que para afrontar esos mayores costos "las 17 distribuidoras más grandes del país aumentaron en 2021 sus tarifas en un promedio del 23%".

Por otra parte, Positino aseguró que si en febrero de 2021 se hubiera otorgado un incremento del 40% en la tarifa final de los usuarios "los subsidios habrían disminuido un 16%".