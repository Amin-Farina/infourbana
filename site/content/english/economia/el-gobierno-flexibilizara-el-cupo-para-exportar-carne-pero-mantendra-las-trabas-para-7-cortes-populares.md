+++
author = ""
date = 2021-12-06T03:00:00Z
description = ""
image = "/images/asadojpeg-1.jpeg"
image_webp = "/images/asadojpeg.jpeg"
title = "EL GOBIERNO FLEXIBILIZARÁ EL CUPO PARA EXPORTAR CARNE PERO MANTENDRÁ LAS TRABAS PARA 7 CORTES POPULARES"

+++

#### **_Todo se definirá este jueves durante una reunión entre el ministro de Agricultura, Julián Domínguez, y los presidentes de las entidades del campo que integran la Mesa de Enlace._**

Son días decisivos para la definición del nuevo esquema de exportación de carne vacuna para el 2022, con el objetivo que no genere tensión en los precios al consumidor. El mismo se anunciará este jueves por la tarde luego de la reunión que mantendrán el ministro de Agricultura, Julián Domínguez, y los presidentes de las cuatro entidades de productores que conforman la Mesa de Enlace, desde las 18hs en la sede de la cartera agropecuaria.

El próximo 31 de diciembre no solamente vencen el cupo general del 50%, teniendo en cuenta el promedio de lo que exportó Argentina en el segundo semestre del año pasado, sino también la cuota especial para Israel y la de vaca conserva o manufactura a China. También concluye la prohibición para exportar los siete cortes populares como el asado, el vacío, el matambre, la falda, entre otros.

Al respecto, tras el encuentro de ayer del titular de la cartera agropecuaria con técnicos de la Mesa de Enlace, fuentes de las entidades señalaron que continuarán las cuotas arancelarias solamente para aquellos cortes que tienen incidencia en el mercado interno, se mantendrá la prohibición para exportar siete cortes populares y seguirán sin restricciones las cuotas arancelarias. En relación a las liberaciones, las mismas contemplarían a la categoría D y E de la vaca con destino a China, y otros cortes de la vaca como el garrón y el brazuelo. También se liberaría la exportación de carne kosher a Israel, que actualmente se encuentra cuotificada. Mañana se definirá el caso de la vaca gorda, que contiene cortes parrilleros, y el novillo Hilton.

Además, en marzo habría una revisión del esquema cuando estén los números finales de stock vacuno. Si bien en la reunión de ayer de técnicos de Agricultura y de la Mesa de Enlace “convalidaron la metodología establecida mediante Resolución 105 con fecha octubre del 2019 para el registro pecuario, que reafirma que al día de la fecha el stock de ganado bovino es de 53,5 millones cabezas”, hay diferencias desde el sector privado con estas estadísticas, que se ubicarían en 53,9 millones de cabezas.

La medida que anunciará mañana el Gobierno no es lo que buscaba la dirigencia del campo, que venía pidiendo la liberación total de la comercialización de carne al mundo, por lo que algunas entidades ya están expresando internamente su malestar con el ministro de Agricultura, ya que según comentan, se habría comprometido a dejar sin efecto las restricciones a fin de año. Hace instantes los integrantes de la Mesa de Enlace de Córdoba, reclamaron mediante un comunicado la liberación total de las exportaciones de carne, y que cese en forma definitiva la intervención en los mercados de trigo y maíz.

“Leemos con preocupación los lineamientos que, sin consulta previa, se pretenden establecer para un supuesto plan ganadero. Sin embargo, todo plan ganadero necesita que las ventas externas estén abiertas y sin cuotas de ningún tipo. Asimismo, no será posible alcanzar mayor producción de carne cuando se limita la demanda cerrando o cupificando las exportaciones. Además, los mercados de trigo y de maíz deben funcionar sin trabas, en el que los compradores compitan libremente por los granos, sin acuerdos espúreos ni órdenes por debajo de la mesa, sin cartelizaciones”, señalaron los dirigentes de Córdoba.

Los anuncios que realice mañana el ministro Julián Domínguez significarían para el campo aceptar que se mantienen las restricciones a la exportación de carne vacuna, pero limitado solamente a esos siete cortes bovinos que compiten directamente con el consumo de los argentinos. También sería dejar atrás un mecanismo de cupos, que fueron diseñados a mediados del presente año por el ministro de Desarrollo Productivo, Matías Kulfas, que significaron un freno a las exportaciones de aquellos cortes, como las categorías D y E de la vaca conserva o manufactura con destino a China, el principal comprador de carne de nuestro país, y por consiguiente un mayor ingreso de divisas.

#### Potencial

Los dirigentes que representan a los productores vienen planteando desde hace tiempo el potencial que tiene la cadena de ganados y carnes y el aporte que la misma podría realizar para resolver los graves problemas del país. Al respecto, el director de la consultora Conocimiento Ganadero, Fernando Canosa, señaló que sin restricciones y aprovechando la buena dinámica y la alta demanda global de carne vacuna, Argentina podría aumentar su producción de 3,2 a 5 millones de toneladas en el mediano plazo, pudiendo superar las 2 millones de toneladas en exportación, creando 200.000 nuevos puestos de trabajo y generando USD 6.000 millones adicionales para el país.

“Hemos participado de una reunión técnica de trabajo donde hicimos un aporte estadístico de la cadena de ganados y carnes, que permite reabrir las exportaciones a partir del año 2022, abasteciendo sin problemas al mercado interno y la exportación, generando las divisas necesarias para una macroeconomía que debe estabilizarse”, dijo ayer el presidente de la Sociedad Rural Argentina, Nicolás Pino.

Y concluyó: “Esperamos que el Ministro Domínguez libere la cuotificación establecida. Normalizar las exportaciones sería una señal positiva que generaría una luz verde para la inversión retrasada por los eslabones de la cadena. Le pedimos al gobierno un voto de confianza, que reabra las exportaciones para el bien de todos los argentinos. Es la única manera de solucionar este problema”.