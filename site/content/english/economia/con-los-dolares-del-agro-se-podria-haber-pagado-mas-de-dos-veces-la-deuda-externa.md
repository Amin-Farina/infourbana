+++
author = ""
date = 2021-08-17T03:00:00Z
description = ""
image = "/images/dolar-trigo.jpg"
image_webp = "/images/dolar-trigo.jpg"
title = "CON LOS DÓLARES DEL AGRO SE PODRÍA HABER PAGADO MÁS DE DOS VECES LA DEUDA EXTERNA"

+++

**_El campo generó, en los últimos 20 años, la cantidad de dólares necesarios para pagar 2,45 veces el valor nominal de la deuda externa, según puntualizó un informe que realizó Coninagro en base a datos del INDEC._**

La entidad agropecuaria consignó que, desde el 2001 a la fecha, el sector agroexportador de Argentina ha generado ingresos por US$ 632.181 millones. Mientras que los compromisos externos se sitúan en US$ 258.378 millones, el 77% de la deuda pública total, que es de US$ 335.556 millones, de acuerdo a los datos publicados por el Gobierno en el portal argentina.gob.ar

“La deuda pública en dólares es ampliamente cubierta con las exportaciones”, aseveró el presidente interino de Coninagro, Elbio Laucirica. De hecho, si se toma en cuenta solamente la última década, del 2011 al 2020 inclusive, los envíos al exterior del agro representaron una vez y media el total de esas obligaciones, al acumular US$384.763 millones. En tanto, durante los últimos 8 años, se juntó un monto similar al de los compromisos en el extranjero.

Según los números del INDEC, el año de mayores ingresos fue el 2011 con US$ 19.833 millones en concepto de comercialización de productos primarios (PP), y US$ 27.765 recibido por las ventas de manufacturas de origen industrial (MOA). Mientras que los de menores ingresos fueron el 2001 (US$ 6.052 PP y US$ 7460 MOA) y el 2002 US$ 5.272 (PP) y 8138 (MOA).

En tanto, de punta a punta de la serie (2001-2020), el crecimiento de las exportaciones fue mayor al 120%, lo que hace una tasa anual de incremento superior al 5%.

“Mucho se habla de la restricción que a la economía argentina le generan los vencimientos de deuda, que deben ser cubiertos con divisas. Es un número significativo, pero el sector agroexportador ha generado mucho más que eso. Los ingresos por exportaciones, pese a inconsistencias e idas y vueltas, han crecido año tras año”, concluyó el análisis.

##### DATOS QUE DIMENSIONAN EL PESO DEL SECTOR

En ese sentido, la Bolsa de Comercio de Rosario (BCR) estimó en julio que las exportaciones del campo dejarían unos US$ 35.192 millones en 2021, lo que refleja un ingreso extra de US$ 11.212 millones respecto al año pasado, principalmente gracias al buen recorrido que tuvieron las valuaciones de los commodities. De hecho, la recaudación por retenciones a la agroindustria creció 390% en el primer semestre.

Además, un estudio del consultor Marcelo Elizondo, que días atrás publicó TN.com.ar, reflejó que esta cadena generó el 70,5% del total de las exportaciones argentinas en la primera parte del año, lo que significa US$ 24.933 millones.

Asimismo, estudios de la Fundación Instituto de Investigación de la CEEA Regional Córdoba y de la Fundación Agropecuaria para el Desarrollo de Argentina (FADA) mostraron, recientemente, que con lo que se recauda en un año de impuestos a las exportaciones agroindustriales de cuatro departamentos de Córdoba, unos US$ 1.161 millones, se podría haberle dado empleo a 15.700 familias y comprar dos dosis de la vacuna Sputnik V para toda la población argentina.

Las localidades analizadas en ese entonces fueron Río Cuarto, Marcos Juárez, Tercero Arriba y San Martín, que explican un tercio de las retenciones que aporta esa provincia. Así, se trató de dimensionar “lo que se va de esas regiones y no vuelve”.

En definitiva, todos estos ejemplos difundidos por distintos actores de la actividad buscan también poner en contexto la importancia de políticas que promuevan al intercambio comercial y se eviten trabas para el desarrollo del campo.