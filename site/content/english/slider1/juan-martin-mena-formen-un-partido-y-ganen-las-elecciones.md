+++
author = ""
date = 2021-05-05T03:00:00Z
description = ""
image = "/images/juan-martin-mena.jpg"
image_webp = "/images/juan-martin-mena.jpg"
title = "JUAN MARTÍN MENA: \"FORMEN UN PARTIDO Y GANEN LAS ELECCIONES\""

+++

**_El viceministro de Justicia, Juan Martín Mena, cuestionó la resolución del máximo órgano judicial, que ayer avaló la autonomía de la ciudad de Buenos Aires para mantener las clases presenciales a pesar de un DNU firmado por Alberto Fernández._**

Apenas conocido el fallo de la Corte Suprema de Justicia, ayer, que avaló la autonomía de la ciudad de Buenos Aires para mantener las clases presenciales a pesar del DNU que firmó Alberto Fernández con restricciones por la pandemia del coronavirus, la vicepresidenta Cristina Kirchner fue la primera voz oficial del Gobierno que manifestó su rechazo. Habló de “golpe” a las instituciones. Después la siguió el Presidente (habló de “decrepitud”), los ministros Nicolás Trotta (Educación) y Martín Soria (Justicia), entre otros funcionarios.

Hoy, el viceministro de Justicia, Juan Martín Mena, fue más allá y apuntó contra los jueces Carlos Rosenkrantz, Ricardo Lorenzetti, Carlos Maqueda y Horacio Rosatti (los cuatro que firmaron la resolución, Elena Highton de Nolasco se abstuvo). “Formen un partido y ganen las elecciones. Así se gobierna, pero no desde un despacho judicial, y menos cuando están en riesgo la vida y la salud de los argentinos”, señaló.

“Estos muchachos están muy cebados; alguien, que tiene nombre y apellido, el gobierno de Mauricio Macri, en los últimos años los invitó a jugar un juego de poder del cual parece que todavía no pudieron volver a la normalidad de sus funciones”, cuestionó el funcionario nacional en radio El Destape.

Para Mena, el fallo de la Corte “una vez invade esferas de otros poderes del Estado, como lo han hecho en los últimos años”, y consideró que es “bastante inexplicable, y confuso, en cuestión que es de política partidaria”. “La Corte pareciera que se ve obligada a definir cuestiones políticas por vía de sentencias judiciales, y eso me parece alarmante”, agregó.

Ayer, luego de las primeras críticas sincronizadas entre varios miembros del gabinete nacional, desde la Casa Rosada se difundió un comunicado oficial con 20 puntos en el que se cuestionó el contenido de la decisión de los jueces del máximo tribunal y se advirtió que el nuevo DNU con la suspensión de las clases presenciales “sigue vigente”.

Según el Gobierno, la Corte “se mete con un objetivo netamente político a embarrar la cancha, pero con vidas en juego”. “Vamos a seguir ejerciendo todas las potestades para administrar el país, no nos vamos a detener porque la cabeza del Poder Judicial esté queriendo meterse en una disputa política”, insistió Mena.

Ayer, durante un acto junto a Trotta y el gobernador bonaerense, Axel Kicillof, Alberto Fernández aseguró que seguirá cuidando “la salud de los argentinos y de las argentinas por más que escriban muchas hojas de sentencias”. “Yo soy un hombre de derecho, respeto las sentencias judiciales, pero no saben lo que me apena la decrepitud del derecho convertida en sentencia; es el tiempo que me ha tocado y también tenemos que luchar contra eso”, agregó.

“Nada de lo que hacemos lo hacemos para complicarle la vida a nadie, lo hacemos para preservar la salud de la gente; contra eso, dicten las sentencias que quieran, vamos a hacer lo que debemos”, completó el Presidente.

En tanto, en el comunicado oficial firmado por el secretario de Comunicación y Prensa, Juan Pablo Biondi, se lamentó el “inusitado rigor formal” del máximo tribunal -”como si no estuviese en riesgo la salud pública”- y se rechazó haber decretado las nuevas restricciones en base a “consideraciones meramente conjeturales”.

“Omitir la adopción de medidas oportunas, similares a las que se adoptaron en otros lugares del mundo que registraban un crecimiento de casos mucho menor que el que exhibía el AMBA, hubiese significado aceptar un riesgo de consecuencias irreversibles para la salud pública y para la vida de las personas”, se afirmó en el texto.

Además, se cuestionó al gobierno porteño por no haber adoptado medidas para disminuir la cantidad de contagios, lo cual generó “una situación de alarma epidemiológica y sanitaria que podía llevar, en poco tiempo, a la saturación del sistema de salud”.