+++
author = ""
date = 2021-08-03T03:00:00Z
description = ""
image = "/images/maquinista-naval-1.jpg"
image_webp = "/images/maquinista-naval.jpg"
title = "MAQUINISTA NAVAL: UNA DE LAS PROFESIONES MEJOR REMUNERADA Y CON MUY POCOS INTERESADOS EN ESTUDIARLA"

+++

**_La falta de profesionales en la carrera de Maquinista Naval pone en peligro al transporte marítimo nacional y hay preocupación en la Armada Argentina y el Ministerio de Transporte. Los sueldos van desde 200 mil pesos a 550 mil pesos._**

 Hoy se cumplen 222 años de la creación de la Escuela Nacional de Náutica “Manuel Belgrano. La institución sigue siendo la única especializada en la formación de personal superior habilitado para operar buques de gran porte dedicados al transporte de todo tipo de mercancías por los mares del mundo.

Dependiente de la Universidad de la Defensa, en ella se cursan las dos carreras fundamentales para la conducción de un buque mercante, científico o de cualquier otra índole. Los egresados lo hacen con el título de Piloto de Ultramar o Maquinista Naval, según hayan cursado la especialidad cubierta o máquinas.

 Además de contar con su acreditación como marinos, los que se reciben lo hacen con un título universitario en grado de licenciatura que, en el caso de los maquinistas, los deja a muy pocas materias de alcanzar la condición de ingenieros mecánicos, industriales o electrónicos.

 Pero que sea redituable y prestigiosa parece no ser suficiente para concitar el interés de quienes buscan un horizonte profesional. Es que de la carrera de Maquinista Naval es una de las profesiones mejor remunerada del país, con sueldos que van desde 200 mil pesos a 550 mil pesos, y tiene plena salida laboral, pero no hay interesados en estudiarla.

 Desde el Ministerio de Transporte, en tanto, reconocen que no han sido felices hasta el momento las acciones para difundir los beneficios de esta profesión. “Muchos chicos piensan que los estamos invitando a seguir la carrera militar porque ven a los cadetes luciendo su uniforme de marinos. Vale destacar que al egreso del instituto de formación la carrera se ejerce en el ámbito civil y en el sector privado. Es verdad que todo oficial de la Marina Mercante egresa también como Oficial de Reserva de la Armada Argentina, pero su eventual convocatoria solo registra antecedentes durante la guerra de Malvinas”, explicaron.

 Siendo que la formación está a cargo del Estado Nacional la misma es gratuita. Es cierto también que el cadete por el tipo de régimen y los horarios de actividades no puede tener actividad laboral mientras cursa la carrera, pero para cubrir las eventuales necesidades que un aspirante -sobre todo del interior- pueda tener, el propio Centro de Maquinistas Navales beca a la totalidad de los cadetes y les brindará a partir de 2022 alojamiento en caso que lo necesiten”, aseguraron.

#### Quienes pueden inscribirse en la carrera de Maquinista Naval

Las carreras de Maquinista Naval y Piloto de Ultramar están abiertas para jóvenes de ambos sexos de entre 18 y 24 años (con una tolerancia en algunos casos hasta 26), argentinos nativos o por opción. Los requisitos de ingreso incluyen el tener estudios secundarios completos, rendir un examen de ingreso que incluye matemáticas e inglés (entre otras materias), acreditar aptitud psicofísica conforme a la profesión y sortear un periodo inicial de un mes denominado “adaptación al medio”.

 Ambas especialidades tienen una duración de 4 años, tres en aula y uno a bordo de distintas unidades de la Marina Mercante durante el cual el cadete pone en práctica los conocimientos adquiridos y es supervisado y calificado por los oficiales del buque que tripule. El título de egreso tiene – en virtud de la normativa vigente- reconocimiento internacional sin necesidad de ninguna reválida y los sucesivos exámenes de ascenso se rinden en la misma escuela previa realización de los cursos respectivos. El título de grado corresponde a la categoría “Licenciatura”.