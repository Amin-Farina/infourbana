+++
author = ""
date = 2021-06-24T03:00:00Z
description = ""
image = "/images/jxc-2021.jpg"
image_webp = "/images/jxc-2021.jpg"
title = "JUNTOS POR EL CAMBIO MUESTRA UNIDAD EN MEDIO DE LAS DISPUTAS INTERNAS"

+++

**_Los máximos dirigentes del espacio se reunieron durante más de una hora y acordaron reglas de juego de cara a las elecciones. A pesar de las diferencias, buscarán consolidar la coalición y no descartan internas._**

Durante más de una hora, los principales referentes de Juntos por el Cambio dialogaron en un bar en el barrio porteño de Palermo. Allí ratificaron el nombre de la coalición de cara a las próximas elecciones legislativas y discutieron las reglas de juego generales para determinar las listas. Buscarán que existan listas de unidades en cada distrito, pero de ser necesario recurrirán a las PASO.

Por el PRO participaron Mauricio Macri, Horacio Rodríguez Larreta, Humberto Schiavoni, Cristian Ritondo y Jorge Macri. Estuvieron ausentes Patricia Bullrich, por un tema familiar, y María Eugenia Vidal, quien cumple aislamiento luego de su viaje a Estados Unidos.

Los representantes de la UCR fueron Alfredo Cornejo, Luis Naidenoff, Gerardo Morales, Maximiliano Abad, Mario Negri y Martín Lusteau. Por la Coalición Cívica estuvieron Maximiliano Ferraro, Juan Manuel López y Maricel Etchecoin. También estuvo Miguel Ángel Pichetto.

A la salida del encuentro, Ritondo explicó que analizaron especialmente la provincia de Buenos Aires, y que por eso participaron Abad y Jorge Macri, presidentes de UCR y PRO en el distrito. Además, explicó que están "trabajando por la unidad y poniendo las reglas por si tenemos PASO".

Cornejo, por su parte, confirmó que utilizarán nuevamente el nombre de Juntos por el Cambio, luego de haber debatido una modificación. En esa línea, ratificó que "la provincia de Buenos Aires es muy relevante por la cantidad de diputados que se eligen. Esta elección es clave para el equilibrio de poder, se juega que el oficialismo tenga mayoría en ambas cámaras".

De cara a las primarias, Cornejo sostuvo que "si hay lista de unidad, bienvenido. Pero si hay competencia y es entre buenos candidatos tampoco lo vemos mal". De hecho, no esquivó los nombres propios que se barajan y evaluó que dos listas encabezadas por "Facundo Manes y Diego Santilli sería muy provechoso para nuestra coalición, como también lo sería hacer una lista de unidad".

Ferraro, presidente de la CC, se expresó en el mismo sentido en cuanto a ratificar "la voluntad para sostener la unidad de Juntos por el Cambio". Además, consideró que esta elección de medio término "es un primer paso para la alternativa en 2023. Hoy estamos enfocados en la unidad de Juntos por el Cambio y en llevar a los mejores candidatos en las provincias".

Rodríguez Larreta, por su parte, volvió a mostrar una vez más su voluntad de esquivar el conflicto. Luego de que Macri marcara ayer la cancha, el jefe de gobierno porteño utilizó su cuenta de Twitter para expresar su coincidencia con el ex presidente sobre "profundizar la unidad de Juntos por el Cambio".