+++
author = ""
date = 2021-12-10T03:00:00Z
description = ""
image = "/images/img_7145.jpg"
image_webp = "/images/img_7145.jpg"
title = "¿CÓMO QUEDÓ CONFORMADO EL NUEVO CONCEJO DELIBERANTE DE TIGRE?"

+++

#### **_El reglamento legislativo establece para las sesiones preparatorias donde existe paridad de fuerzas, que debe prevalecer la propuesta para la presidencia del partido ganador en las últimas elecciones. En este caso, se trata de Juntos, quien tras imponerse ante el Frente de Todos el pasado 14 de noviembre a nivel local, pudo definir quién encabeza el cuerpo._**

Segundo Cernadas, referente de Juntos, fue elegido como presidente del Concejo Deliberante, en una sesión marcada por el nuevo reparto de fuerzas de 12 votos para el oficialismo y 12 para la oposición.

"No queremos que sea un 12 'sí' y 12 'no'. Hubo un acuerdo para que todos tuviéramos participación. Quiero ser un presidente para todos los concejales, y que cada uno trabaje lo mejor posible", manifestó Cernadas y agregó: "Vamos a tratar de buscar un trabajo en conjunto, con el oficialismo. Tenemos la posibilidad de generar ideas y propuestas y poder llevarlas al oficialismo teniendo una fuerza importante, diciendo qué es lo que queremos hacer".

Además, se refirió al trabajo del presidente saliente, el massista Fernando Mantelli, y expresó que "me encantó el laburo que ha hecho, porque más allá de su trabajo se huele un buen tipo, y eso va más allá del color político, quiero imitar su trabajo".

Que el espacio opositor se haya quedado con la presidencia rompe una tradición no escrita en las leyes, que tiene que ver con que los legislativos son conducidos por los oficialismos más allá de las mayorías circunstanciales.

La incertidumbre se mantuvo hasta último momento, ya que se desconocía la posición que podía tomar Nicolás Massot, referente de Juntos que disputó la interna en las PASO contra Cernadas: si es que iba a acompañar la posición de su bloque o iba a respetar esta tradición legislativa y abstenerse en la votación, favoreciendo de esta manera una mayoría del Frente de Todos.

Por otra parte, dentro del oficialismo, las tensiones entre el zamorismo y el massismo volvieron a resurgir en esta definición y generaron disputas sobre cuál iba a ser la propuesta del bloque del Frente de Todos para ocupar la presidencia. Si Fernando Mantelli debía continuar en orden al acuerdo de unidad que se conformó previo a las PASO, o ese lugar debía ser ocupado por alguien que respondiera al intendente Julio Zamora, en este caso, la concejal Florencia Mosqueda.

La sesión que debía comenzar a las 11 de demoró cinco horas por las negociaciones de los cargos en las comisiones, dónde el zamorismo buscaba mantener el control de las áreas sensibles para la gestión.

En el encuentro, también se definieron las vicepresidencias del HCD: la vicepresidencia del Concejo Deliberante quedó para Fernando Mantelli (Frente de Todos - massismo), la vicepresidencia 2º la ejercerá Sofía Bravo (Juntos) y la vicepresidencia 3º, Florencia Mosqueda (Frente de Todos - zamorismo).