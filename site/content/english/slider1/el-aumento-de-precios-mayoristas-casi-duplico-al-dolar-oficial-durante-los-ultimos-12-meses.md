+++
author = ""
date = 2021-10-04T03:00:00Z
description = ""
image = "/images/z-mayoristas-1w-1.jpg"
image_webp = "/images/z-mayoristas-1w.jpg"
title = "EL AUMENTO DE PRECIOS MAYORISTAS CASI DUPLICÓ AL DÓLAR OFICIAL DURANTE LOS ÚLTIMOS 12 MESES"

+++

La diferencia fue de 60,5% a 32,1 por ciento. Los productos nacionales subieron más que los importados.

Desde principios de 2021, el Gobierno viene usando el “ancla cambiaria”, esto es la política de que el precio del dólar oficial crezca a un ritmo muy inferior al de la inflación, en la creencia de que de ese modo, retrasando el valor real del dólar y “revaluando” el peso, iría quitándole fuerza al aumento de los precios.

Por cierto, cuando el dólar aumenta mucho o muy bruscamente, hay un efecto casi inmediato y reflejo sobre los precios, pero en el funcionamiento real de la economía, la polea de transmisión en el sentido inverso –esto es, para que la inflación desacelere- opera a través de los precios mayoristas, mucho más atados al comercio exterior (en particular, a la importación) que se hace al tipo de cambio oficial, y a los costos de producción.

La inflación minorista de agosto fue del 2,5% y la de septiembre se conocerá recién el jueves 14 de octubre, lo que demuestra que el efecto “ancla” fue muy pobre: en los 12 meses hasta agosto pasado los precios al consumidor aumentaron 51,4%. La lectura es más desalentadora aún si se mira la evolución de los precios mayoristas. Un reciente informe del Ieral de la Fundación Mediterránea precisa al respecto que en los últimos 12 meses los precios mayoristas crecieron casi al doble del dólar oficia: 60,5 vs 32,1 por ciento.

En el análisis por categorías, los precios que más aumentaron fueron los de productos manufacturados y minerales, con subas interanuales de 62,4 % y de 59,7 %, respectivamente, seguido por los productos agropecuarios y los precios de energía eléctrica, con aumentos de 56,2 % y de 48,7 % en cada caso. Los precios mayoristas que menos aumentaron fueron los de productos pesqueros, 25,3 por ciento, destaca un análisis de Marcos O’Connor.

Además, nota el economista, en los últimos 12 meses el costo de la construcción anotó una suba interanual de 66,2 por ciento.

Los resultados ponen en cuestión la estrategia oficial de intentar “anclar” los precios. “A diferencia del Índice de Precios al Consumidor, en la canasta que mide el Índice de Precios Mayoristas prevalecen los bienes e insumos transables, que son aquellos que se comercian con el resto del mundo y, por ende, son más sensibles a la evolución del tipo de cambio, de los precios internacionales y de las condiciones de abastecimiento del mercado local”, dice el informe.

Sin embargo, la brecha entre precios mayoristas y dólar oficial se ensanchó constantemente hasta registrar, en los 12 meses a agosto, 28,4 puntos porcentuales.

Pero la cosa puede llegar a ser peor. En el caso de los precios mayoristas, observó O’Connor, “hay que tener también en cuenta la incertidumbre existente sobre el valor de reposición de los inventarios, no sólo por eventuales expectativas de devaluación, sino también por las restricciones introducidas al comercio exterior, la existencia de cupos y la dificultad para planificar cobros y pagos al exterior”.

Ancla de telgopor

No deja de ser un efecto perverso que el “ancla cambiaria” no haya servido para “anclar” ni siquiera los precios mayoristas, que no solo aumentaron más que los minoristas sino que ahora podrían ser afectados por “expectativas” de devaluación generadas por el retraso cambiario oficial, cuyo objetivo había sido, precisamente, desacelerar el ritmo de los precios mayoristas y, en definitiva, los precios al consumidor.

El informe incluso corta más tela al destacar que en el análisis por categorías de los precios mayoristas, los productos “nacionales” crecieron 60,9%, casi cinco puntos más que los “importados”, que aumentaron 56,1 por ciento.

A su vez, en el desagregado de los productos nacionales, los precios que más aumentaron fueron los de bienes industriales y minerales, con subas interanuales de 62,4 % y 59,7 %, respectivamente.

Así y todo, los precios mayoristas fueron a la zaga de la Construcción, asociados a lo que cuesta construir viviendas en el Área Metropolitana de Buenos Aires (AMBA), que crecieron 66,6%, parecido al “número del demonio”.

En definitiva, no está claro qué fue lo que “ancló” el dólar oficial, si es que ancló algo.