+++
author = ""
date = 2021-09-10T03:00:00Z
description = ""
image = "/images/045584-1.jpg"
image_webp = "/images/045584.jpg"
title = "VICENTE LÓPEZ: REFERENTES DEL FDT ACOMPAÑARON A BOYANOVSKY Y PEREYRA EN EL CIERRE DE LA CAMPAÑA"

+++

**_En la última semana antes de las PASO entre reuniones, caminatas, charlas y plazas feministas, ministros y referentes provinciales y nacionales del Frente de Todos acompañaron en Vicente López a los precandidatos a concejales de la lista 4, Lucas Boyanovsky y Erica Pereyra._**

El ministro de Salud bonaerense, Nicolás Kreplak, se reunió con trabajadorxs y profesionales de la salud en Olivos.

La senadora nacional Juliana Di Tulio junto a Estela Díaz, ministra provincial de las Mujeres, Políticas de Género y Diversidad Sexual, participaron de un encuentro feminista en la plaza Alem de Munro.

Daniel Filmus brindó una charla sobre educación en Florida.

El ministro de Desarrollo de la Comunidad bonaerense, Andrés Larroque, visitó a vecinos e instituciones comunitarias del barrio Las Flores en Florida Oeste y participó del plenario de la militancia del Frente de Todos local. 

“Este domingo se ponen en juego cuestiones que definen el porvenir de nuestra comunidad. De futuro, de organización, de militancia y de la enorme tarea que tenemos por delante. La salida a la vida que queremos es construyendo un Vicente López con más trabajo, producción y menos especulación inmobiliaria, con un desarrollo sostenible donde se acompañe a las pymes, comercios y emprendedores locales; un sistema de salud integrado y una ciudad más inclusiva”, expresó Boyanovsky.