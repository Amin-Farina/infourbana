+++
author = ""
date = 2021-08-30T03:00:00Z
description = ""
image = "/images/victoria-tolosa-paz-1207647-1.jpg"
image_webp = "/images/victoria-tolosa-paz-1207647.jpg"
title = "VICTORIA TOLOSA PAZ: \"EN EL PERONISMO SIEMPRE SE GARCHÓ\""

+++

**_En plena campaña antes de las PASO, la precandidata a diputada por el Frente de Todos Victoria Tolosa Paz brindó una entrevista descontracturada en la que sorprendió al hablar sobre la importancia del sexo y aseguró: “En el peronismo siempre se garchó”._**

“Nosotros vinimos para hacer posible la felicidad de un pueblo y la grandeza de una patria, y no hay felicidad de un pueblo sin garchar. Perdón, nosotros somos así. Lo que digo, es parte importante de la vida, el baile, el disfrute, el goce, no lo vamos a ocultar. Somos seres humanos, nos gusta gozar, nos gusta divertirnos”, señaló la precandidata del kirchnerismo por Buenos Aires en diálogo con Saliendo Que es Eléctrica, un ciclo emitido por YouTube que conducen Pedro Rosenblat -”El Cadete”- y Martín Rechimuzzi.

Tolosa Paz remarcó que “los pibes”, tras más de un año de pandemia, “están desesperados, hartos de los aforos y quieren bailar”; y aseguró que para la primavera esos problemas serán solucionados porque en la provincia de Buenos Aires se marcó un “hito” por el “altísimo nivel de inmunidad”.

Tolosa Paz habló además de la campaña del oficialismo para las elecciones legislativas y las presidenciales de 2023: “Tenemos que ganar la Copa del Mundo, hay que entrenarse, vamos a las eliminatorias con un gran equipo, armados para hacer muchos goles. Vamos a tener un gran hito en la provincia de Buenos Aires y en la Argentina, que es tener un nivel de inmunidad todos y todas”, indicó.

La afirmación de Tolosa Paz sobre el peronismo y el sexo trajo inmediata repercusión en el ámbito político: de hecho, una de las primeras críticas a los dichos llegó desde las filas del oficialismo: el ministro de Seguridad bonaerense, Sergio Berni, aseguró que “si no hay justicia social, por más garche que haya el pueblo es infeliz”.

En tanto, la precandidata a diputada por la Ciudad de Juntos por el Cambio María Eugenia Vidal, afirmó que “el gobierno subestima a los jóvenes”. “Dejemos que que ellos se ocupen de su sexualidad, nosotros tenemos que encargarnos de que tengan trabajo y educación”, opinó la exgobernadora de Buenos Aires.

En un raid por los medios, Tolosa Paz también pasó por Crónica TV y criticó a los referentes de Juntos por sus apariciones en los medios: “(Mauricio) Macri y (María Eugenia) Vidal tienen la cara de piedra”, disparó.

“Podrían ganarse un Oscar por su actuación porque estuvieron guardados durante toda la pandemia y ahora reaparecen”, criticó.