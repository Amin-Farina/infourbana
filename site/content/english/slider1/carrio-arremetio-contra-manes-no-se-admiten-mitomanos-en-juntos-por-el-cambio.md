+++
author = ""
date = 2021-07-30T03:00:00Z
description = ""
image = "/images/elisa_carrio_vs_facundo_manes_critica_jxc-1.jpg"
image_webp = "/images/elisa_carrio_vs_facundo_manes_critica_jxc.jpg"
title = "CARRIÓ ARREMETIÓ CONTRA MANES: \"NO SE ADMITEN MITÓMANOS EN JUNTOS POR EL CAMBIO\""

+++

**_Carrió pidió "derecho a réplica sobre los dichos de Manes", quien en un reportaje reciente aseguró: "La doctora Carrió me vino a ofrecer ser candidato a vicepresidente de ella en 2015"_**

La referente de la Coalición Cívica (CC), Elisa Carrió, criticó este martes al precandidato bonaerense de esa fuerza, el neurocientífico Facundo Manes, y le advirtió que "no se admiten mitómanos, por lo menos en Juntos por el Cambio" (JxC).

Carrió -socia fundadora de la alianza opositora- apuntó a Manes, luego de que el precandidato radical asegurara en una entrevista que la propia Carrió lo visitó en persona para ofrecerle ser su candidato a vicepresidente en 2015, en la interna contra el expresidente Mauricio Macri.

En un audio subido a su página de la red social Facebook, Carrió dijo que pedía "derecho a réplica sobre los dichos de Manes", quien en un reportaje publicado en el diario La Nación aseguró: "La doctora Carrió me vino a ofrecer ser candidato a vicepresidente de ella en 2015" y dijo desconocer por qué ella lo criticaba en esta campaña.

"Yo no cambié nada desde 2015 hasta acá", planteó Manes en relación a las constantes críticas que le viene dedicando la líder de la Coalición Cívica.

Esa mención enfureció a Carrió, que en un posteo con video y texto dijo que fue "en una sola oportunidad al departamento de Manes, invitada por Toty Flores", quien ya era su candidato a vice, y rechazó haberle hecho ese ofrecimiento.

"Tuve una fuerte discusión académica porque el señor desconocía los tipos de conocimiento, el práctico, el teórico, el místico, y solo reconocía el conocimiento neurocientífico", recordó Carrió sobre ese día.

"Entonces, el tipo de conocimiento que surge de la poesía, de la filosofía, de las humanidades es algo que no estaba en su registro, con lo cual fue un diálogo fuerte de discusión académica", insistió Carrió en sus redes sociales.

Siguió explicando más alternativas de sus diferencias con el neurocirujano y concluyó: "Las reglas de juego deben ser claras y la prohibición de la mentira entran dentro de las reglas de juego, no se admiten mitómanos, por lo menos en Juntos por el Cambio".

Por si no bastaba, Carrió también pidió derecho a réplica a La Nación (donde se publicó la entrevista en cuestión) y allí declaró que Manes "miente descaradamente" y que ella "nunca" fue a ofrecerle una vicepresidencia a su casa.

#### LAS FIGURITAS POLÉMICAS QUE ACOMPAÑAN A FACUNDO MANES EN SU LISTA

Margarita Stolbizer (4ta en la lista del Neurocientífico): En las elecciones de 2003 fue candidata a gobernadora de la provincia de Buenos Aires por la UCR. En las elecciones de 2011, se presentó como candidata a gobernadora de la provincia de Buenos Aires por el Frente Amplio Progresista, acompañando la candidatura de Hermes Binner. Para las elecciones legislativas de 2013 forma parte del Frente Progresista, Cívico y Social, encabezando la lista de diputados nacionales, secundada por Ricardo Alfonsín. Para las elecciones presidenciales de 2015 se presentó como candidata a presidenta por la alianza Progresistas. En las elecciones legislativas de 2017 fue segunda candidata a senadora nacional por la provincia de Buenos Aires por la alianza 1País,44​ surgida ese mismo año principalmente de la unión con el Frente Renovador liderado por Sergio Massa. En las elecciones presidenciales de 2019 Stolbizer apoyó la coalición Consenso Federal que llevó a Roberto Lavagna. Un cuadro que sin lugar a dudas genera todo menos fidelidad y firmeza.

La otra figurita polémica que acompaña a Manes es nada más y nada menos que el ex Intendente de Malvinas Argentinas, Jesús Cariglino. Este personaje está sospechado de haber cometido 5 delitos penales: defraudación agravada, asociación ilícita, malversación de los caudales públicos, negociación incompatible con el ejercicio de funciones públicas y enriquecimiento ilícito.