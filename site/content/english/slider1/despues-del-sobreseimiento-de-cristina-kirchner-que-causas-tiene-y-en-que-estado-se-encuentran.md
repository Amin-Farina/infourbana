+++
author = ""
date = 2021-04-14T03:00:00Z
description = ""
image = "/images/cristina1202.jpg"
image_webp = "/images/cristina1202.jpg"
title = "¿DESPUÉS DEL SOBRESEIMIENTO DE CRISTINA KIRCHNER, QUÉ CAUSAS TIENE Y EN QUÉ ESTADO SE ENCUENTRAN? "

+++

La vicepresidenta de la Nación, Cristina Kirchner, fue sobreseída ayer en la causa de dólar futuro y se trata del primer expediente que tiene en etapa de juicio oral que se cierra, aunque la Fiscalía de la Cámara Federal de Casación Penal puede apelar para que la decisión sea revocada por la Corte Suprema. Pero tiene otros cuatro expedientes en juicio oral: cuáles son y en qué etapa se encuentran

#### OBRA PÚBLICA

Cristina Kirchner está siendo juzgada desde mayo de 2019 por las presuntas irregularidades en la concesión de 51 obras públicas que el empresario Lázaro Báez recibió para la provincia de Santa Cruz durante los 12 años de los gobiernos kirchneristas. El Tribunal Oral Federal 2 juzga también a Báez, al ex ministro de Planificación Federal Julio De Vido, al ex secretario de Obras Públicas José López, al ex titular de Vialidad Nacional Nelson Periotti y a otros ex funcionarios nacionales y de la provincia de Santa Cruz. El proceso se encuentra en la etapa de declaración de testigos.

“Este tribunal, el del lawfare, seguramente tiene la condena escrita. No me interesa, a mi me absolvió la historia, me va a absolver la historia. Y a ustedes, seguramente, los va a condenar la historia”, dijo Cristina Kirchner el 2 de diciembre de 2019 cuando declaró en el juicio.

El caso tiene varios planteos en la Corte Suprema de Justicia de la Nación para que sea declarado nulo. La defensa de la vicepresidenta pidió el cierre porque entiende que debe investigarse en Santa Cruz, porque el caso ya lo investigó la justicia federal de esa provincia y lo desestimó, porque el peritaje fue parcial sobre cinco obras y no sobre las 51 que se cuestionan, porque no se hizo lugar a todas las pruebas que pidió y por la actuación de un perito. El máximo tribunal todavía no resolvió.

#### MEMORÁNDUN CON IRÁN

Para muchos se trata de un caso similar al de dólar futuro: fue una decisión política que no puede ser judicializa. Fue la firma con Irán de un acuerdo para avanzar en la investigación del atentado a la AMIA. Puntualmente que las autoridades judiciales de la causa, el fallecido fiscal Alberto Nisman y el entonces juez del caso, Rodolfo Canicoba Corral, puedan viajar a ese país para tomarle declaración indagatoria a los acusados. También contemplaba la creación de una comisión de la verdad para que analicen el caso juristas de los dos países. Pero el memorándum nunca entró en vigencia porque fue declarado inconstitucional.

La denuncia la presentó Nisman -cuatro días después fue encontrado muerto- y se cerró por inexistencia de delito. Luego fue reabierta por la Cámara de Casación. Fue con los votos de los jueces Gustavo Hornos y Mariano Borisnky, que en las últimas semanas fueron blanco de críticas del oficialismo porque mantuvieron reuniones en la Casa Rosada y la quinta de Olivos con el entonces presidente Mauricio Macri. Con eso, la defensa de Cristina Kirchner pedirá la nulidad de la causa. Otras defensas ya hicieron presentaciones.

El caso lo tiene el Tribunal Oral Federal 8 a la espera de que ponga fecha de inicio de juicio y de resolver cómo será la declaración del ex titular de Interpol Ronald Noble, quién públicamente dijo que nunca estuvieron en riesgo las ordenes de detención internacional de los acusados del atentado, lo que en la acusación del caso fue parte del acuerdo ilegal del memorándum.

#### HOTESUR-LOS SAUCES

La vicepresidenta está acusada junto a sus hijos, Máximo y Florencia Kirchner, por presunto lavado de dinero en el alquiler de sus hoteles y viviendas a empresarios que recibían obra pública durante su gobierno. En esta causa los Kirchner recibieron una buena nota la semana pasada: el Tribunal Oral Federal 5 les permitió recuperar la administración de sus propiedades que estaban bajo custodia judicial.

El caso está en etapa de prueba. Resta el resultado de un peritaje para que el tribunal oral quede en condiciones de poner fecha de juicio. Este expediente también tiene planteos de la defensa de Cristina Kirchner en la Corte Suprema. Allí objetó la figura del delito de lavado de dinero, los pedidos de elevación a juicio y planteó la litispendencia, que es que ya fue investigada. En este caso sostiene que con el lavado de dinero se está analizando su patrimonio por lo cual ya fue sobreseída.

#### CUADERNOS DE LA CORRUPCIÓN K

Es el último caso que llegó a juicio oral y está a cargo del Tribunal Oral Federal 7. El expediente tiene más de 100 imputados y se investiga el pago de coimas de empresarios a funcionarios nacionales, lo que quedó registrado en anotaciones que hizo Oscar Centeno, chofer del ex funcionario de Planificación Federal Roberto Baratta.

La causa tuvo más de 30 arrepentidos que contaron cómo era el circuito de sobornos. Esas declaraciones fueron objetadas pero hasta ahora fueron convalidadas por la justicia. La nulidad de la declaración de los arrepentidos se encuentra en la Corte Suprema.