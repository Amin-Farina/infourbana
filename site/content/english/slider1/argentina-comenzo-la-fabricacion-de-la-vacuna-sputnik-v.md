+++
author = ""
date = 2021-04-20T03:00:00Z
description = ""
image = "/images/sputnik-v2jpg.jpg"
image_webp = "/images/sputnik-v2jpg.jpg"
title = "ARGENTINA COMENZÓ LA FABRICACIÓN DE LA VACUNA SPUTNIK V"

+++

**_La empresa farmacéutica Laboratorios Richmond Sacif produjo el primer lote de la vacuna rusa contra el coronavirus, que deberá ser controlado por el Centro Gamaleya._**

El Fondo Ruso de Inversión Directa (RDIF) anunció que la empresa farmacéutica Laboratorios Richmond Sacif produjo el primer lote de la vacuna rusa contra el coronavirus Sputnik V en la Argentina, que será enviado al Centro Gamaleya para que realice el control de calidad, en tanto que la producción a gran escala en el país está prevista para junio.

"La Argentina se ha convertido en el primer país de América Latina en comenzar la producción de la vacuna Sputnik V. RDIF y sus socios han realizado una transferencia de tecnología a los Laboratorios Richmond", señaló el comunicado difundido por el organismo ruso.

Y continuó: "El primer lote producido será entregado al Centro Gamaleya para realizar el control de calidad correspondiente. Está previsto que la producción a gran escala de Sputnik V en Argentina comience en junio".

El comunicado añadió que "la vacuna producida en la Argentina podrá luego se exportada a otros países de América Central y América Latina".

Argentina fue el primer país latinoamericano en registrar oficialmente la vacuna Sputnik V a través de una autorización de uso de emergencia y comenzó a aplicarla en la población el 29 de diciembre de 2020.

Actualmente, ya está registrada en 60 países. La vacuna se basa en una plataforma de vectores adenovirales humanos, virus inactivados que se utilizan para transportar material genético del virus que se quiere inocular.

En este caso, la Sputnik V utiliza dos vectores diferentes (uno diferente en cada dosis), lo que proporciona una inmunidad más duradera que las vacunas que utilizan el mismo mecanismo de administración para ambas inoculaciones.

En el momento en que se anunció el memorando de entendimiento entre el RDIF y Laboratorios Richmond -en febrero pasado-, el presidente Alberto Fernández destacó el acuerdo.

"Estamos muy entusiasmados con la posibilidad de producir la Sputnik V en la Argentina, vacuna con la cual ya estamos protegiendo a gran parte de nuestra población con excelentes resultados. Será una gran oportunidad para avanzar en la lucha contra la pandemia no solo en la Argentina, sino también en América Latina", había dicho el presidente.

Por su parte, Kirill Dmitriev, director ejecutivo (CEO) del Fondo de Inversión Directa de Rusia sostuvo en el comunicado difundido hoy que "Argentina fue el primer país en América Latina en aprobar el uso de la vacuna Sputnik V y comenzar a vacunar a la población. Hoy nos complace anunciar que Argentina también se ha convertido en el primer estado de la región en iniciar la producción de Sputnik V gracias a la asociación entre RDIF y Laboratorios Richmond".

Finalmente, Marcelo Figueiras, presidente de Laboratorios Richmond, señaló que "se enorgullece" del apoyo de RDIF y añadió que "nuestra empresa hará todo lo posible para garantizar que la vacuna esté disponible lo antes posible para Argentina y para toda la región de América Latina”.

Según los últimos datos difundidos por el RDIF, la efectividad de la vacuna es del 97,6%, basados en el resultado del análisis de datos sobre la incidencia del coronavirus entre los rusos vacunados con ambos componentes en el período comprendido entre el 5 de diciembre de 2020 y el 31 de marzo de 2021.