+++
author = ""
date = 2022-02-28T03:00:00Z
description = ""
image = "/images/rublo-y-pesos-argentinos.jpg"
image_webp = "/images/rublo-y-pesos-argentinos.jpg"
title = "LA CAÍDA DEL RUBLO: LA MONEDA RUSA TOCÓ MÍNIMOS HISTÓRICOS Y YA VALE MENOS QUE 1 PESO ARGENTINO"

+++

#### **_Luego de devaluarse más de 30% el lunes, la relación es de 0,96 pesos argentinos por rublo. Sucede en medio de la invasión a Ucrania y tras la contraofensiva de occidente a través de la desconexión de la red interbancaria Swift_**

En medio de la invasión de Rusia a Ucrania y las consecuentes medidas financieras llevadas a cabo por Occidente a través de la desconexión de la red interbancaria Swift, el rublo ruso se desplomó más de 30% el lunes y si bien se recupera en torno a un 5% este martes, sigue en mínimos históricos y ya vale menos que el peso argentino, unas de las monedas más devaluadas en el último año junto a la lira turca.

El rublo registraba hoy una suba de alrededor del 2% en las primeras operaciones de la jornada, tras la estrepitosa caída del lunes, en el marco de la crisis financiera que atraviesa Rusia, sin embargo, la relación es de 0,96 pesos argentinos por rublo.

El país presidido por Vladimir Putin recibió numerosas sanciones económicas que lo aislaron del mundo luego de invadir Ucrania. Este martes, la moneda rusa experimentaba una suba de más de 2% y cotizaba a USD 96,80, tras el histórico desplome del lunes, cuando la divisa llegó a bajar hasta un 30 por ciento.

Por otro lado, la suerte del peso argentino, lejos de toda crisis militar, está signada por la necesidad del Gobierno de frenar el atraso del tipo de cambio oficial respecto de la inflación. El dólar mayorista intentó funcionar como ancla inflacionaria durante 2021. Avanzó 22% en el año mientras la inflación superó el 50 por ciento.

Cabe recordar que el rublo se mantuvo desde 2003 y por más de una década debajo del peso argentino en medio de la transición de una economía socialista a una capitalista de mercado. A partir de 2014, la economía rusa empezó a sufrir una serie de altibajos, como consecuencia de la caída de los precios del petróleo y las sanciones impuestas por Estados Unidos y la Unión Europea y una fuerte fuga de capitales. Recién a partir de mediados de 2020 el rublo logró superar en valor al peso argentino hasta que el derrumbe que registró esta semana en medio de la escala bélica con Ucrania lo volvió a dejar por debajo de nuestra moneda.

Respecto a la caída del rublo, las potencias occidentales decidieron durante el último fin de semana prohibir las operaciones del Banco Central ruso y expulsar a algunos bancos de ese país del sistema internacional Swift. Esas medidas provocaron una masiva fuga de capitales, situación que afecta la estructura financiera rusa.

El lunes, el rublo se derrumbó pero luego recortó esa caída en torno al 17%, por la decisión de las autoridades rusas de que las empresas exportadoras de energía, como Gazprom o Rosneft, dispongan la conversión a rublos de sus ingresos en divisas ante la incapacidad del Banco Central de intervenir.

Por su parte, la Bolsa de Comercio de Moscú permanecía sin operaciones este martes, al igual que en la jornada anterior: hasta el momento, no se informó oficialmente cuándo retomará su actividad.

En este marco, las petroleras Shell, BP y la noruega Equinor anunciaron que abandonarán sus posiciones en Rusia, mientras que bancos multinacionales, aerolíneas y fabricantes de automóviles también recortaron sus envíos al país.

El banco central ruso afirmó que está capacitado para mantener la estabilidad financiera del país a pesar del congelamiento de sus activos internacionales anunciada en la víspera por Estados Unidos, la Unión Europea, Canadá y Reino Unido, en una de las sanciones más duras recibidas por Rusia desde que comenzó su invasión de Ucrania.

Estas sanciones -que también comprenden la exclusión de algunos bancos rusos del mecanismo financiero Swift- paralizan buena parte de los 600.000 millones de euros en oro y divisas de la autoridad monetaria.

El objetivo de las sanciones es hacerle pagar a Putin un alto costo económico e interno y convertir a Rusia un “paria financiero internacional”, desactivando la operatividad de los bancos rusos y “desarmando” al Banco Central, conducido por Elvira Nabiulina, una economista y asesora de la máxima confianza del líder ruso.

La idea es que al no poder comprar rublos de otros bancos centrales, la moneda rusa colapsará, aumentará la inflación y se complicará la situación interna de Putin.

Sin embargo, tras estas sanciones, el Banco de Rusia anunció una batería de medidas económicas: suba de tasas de interés al 20%, cierre de la operativa bursátil y controles de capitales para inversores extranjeros.

No hay que perder de vista que el conjunto de medidas contra el país que preside Vladimir Putin crece y hace que cada día la guerra cueste más.

Las acciones petroleras que podían actuar en el mercado financiero cayeron hasta 76% el lunes, los principales fondos de inversión del mundo se retiraron del mercado ruso, las grandes petroleras del mundo deshicieron sus convenios de asociación e inversión, Morgan Stanley los eliminó de su índice MCSI, donde la Argentina está en una de las calificaciones más bajas (standalone) y J. P. Morgan anunció que están a punto de entrar en recesión porque la economía se contraerá 20% en el segundo trimestre del año y terminará con una caída anual de 3,5 por ciento.