+++
author = ""
date = 2021-10-14T03:00:00Z
description = ""
image = "/images/dbkewd7gpvdzni6zyvqdgzgysy.jpg"
image_webp = "/images/dbkewd7gpvdzni6zyvqdgzgysy.jpg"
title = "MANZUR LLEVÓ A SU ESPOSA A NUEVA YORK EN EL AVIÓN DE LA PROVINCIA DE TUCUMÁN Y LE COSTÓ U$S70.000 A LOS ARGENTINOS "

+++

#### **_El jefe de Gabinete viajó en el avión de la provincia de Tucumán, aunque las dos naves de la flota presidencial estaban operativas y disponibles. Además de su esposa, lo acompañaron su secretaria privada y su vocera._**

A las 21:17 (hora local) de anoche, el avión de la provincia de Tucumán aterrizó en las afueras de Nueva York con el jefe de Gabinete, Juan Manzur. En ese vuelo también viajó su esposa, Sandra Mattar Sabio, quien no tiene agenda oficial ni cumple un cargo dentro del Gobierno, y otras dos personas del entorno del funcionario.

Manzur decidió volar en el avión de Tucumán aunque ya no es gobernador. El costo de ese vuelo supera los 70 mil dólares, teniendo en cuenta los gastos operativos, aunque si fuera un cliente privado debería desembolsar unos 100 mil dólares (al valor oficial), según confirmaron varias fuentes aeronáuticas consultadas por este medio.

“Tuvo una parada en el aeropuerto de Viru Viru (Bolivia) que cuesta 3000 dólares, otra en Barranquilla (Colombia), y los gastos en Nueva York. Solo de gastos de aterrizaje son 20 mil dólares”, apuntó un experto. A eso hay que sumarle unos 17 mil litros de combustible.

Si Manzur y sus tres acompañantes hubieran optado por viajar en primera clase de una aerolínea comercial habrían pagado unos 7 mil dólares por pasaje. Ayer mismo había vuelos disponibles. Los números no cierran.

La decisión de utilizar el avión de Tucumán, un LearJet 60, tampoco se justifica desde el punto de vista operativo porque se trata de una aeronave que no tiene autonomía para llegar hasta Nueva York de manera directa. De hecho, tuvo que hacer dos escalas. A las 15:22 (hora local) despegó desde Barranquilla.

Tampoco fue una decisión precipitada. El domingo pasado ya se sabía dentro del Gobierno que se iba a utilizar el avión de Tucumán con cuatro pasajeros. Entre lunes y martes, el jefe de Gabinete y su comitiva tuvieron que cumplir ciertos trámites burocráticos para poder viajar a Estados Unidos.

Otro dato más: los dos aviones de la flota presidencial están disponibles. Se trata del Tango 04, un avión adquirido durante la presidencia de Cristina Kirchner que tiene capacidad para trasladar hasta 60 pasajeros, y el Tango 11, un Lear Jet adquirido por la Fuerza Aérea que tiene capacidad para ocho pasajeros. “Los dos aviones están operativos”, dijo una fuente oficial ante una consulta de este medio.

En la agenda de esos aviones solo hay un vuelo previsto para el próximo sábado, con destino a la provincia de Salta, para trasladar al ministro del Interior, Eduardo “Wado” de Pedro, pero se podría haber modificado en el caso que Manzur hubiera elegido ese avión.

Con toda la flota disponible, Manzur decidió utilizar el avión de Tucumán, tal como adelantó ayer Radio Rivadavia. La aeronave despegó ayer a las 7:56 AM desde Aeroparque y recién aterrizó en el aeropuerto Westchester County, en las afueras de Nueva York, a las 22:17 (hora de la Argentina). En el vuelo también viajaron su esposa, Sandra Mattar Sabio, su secretaria privada, Vanesa Demarziani, y la secretaria de Comunicación Pública de la Nación, Valeria Zapesochny.

El avión fue adquirido en 2010 por el entonces gobernador José Alperovich, que ya había comprado otras dos aeronaves. La Legislatura provincial lo había autorizado a gastar $30 millones, pero Alperovich terminó llevando esa cifra por decreto a 36,9 millones.

Ante la polémica por la utilización del avión de Tucumán, cerca de Manzur argumentaron que no es una aeronave sanitaria, una vieja polémica en esa provincia. “Es un avión ejecutivo que se ha dispuesto con capacidad para atender una urgencia sanitaria, el avión sanitario está en Tucumán en este momento”, dijo una fuente oficial consultada por este medio.

Manzur viajó a Nueva York para acompañar al ministro de Economía, Martín Guzmán, en una agenda de reuniones con empresarios e inversores. Uno de los ejes es la negociación por el pago de la deuda con el Fondo Monetario Internacional (FMI). “Es una indicación del Presidente de la Nación. Acompañar al ministro de Economía en el marco de una reunión que el día viernes tendrá con una serie de empresarios y inversores a nivel global. Vamos a llevar la visión del Gobierno nacional”, explicó el funcionario cuando anunció su viaje.