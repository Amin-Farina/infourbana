+++
author = ""
date = 2021-05-21T03:00:00Z
description = ""
image = "/images/afa.jpg"
image_webp = "/images/afa.jpg"
title = "LA AFA ANUNCIÓ LA SUSPENSIÓN DEL FÚTBOL FRENTE AL NUEVO CONFINAMIENTO"

+++

**_La dirigencia indicó en un comunicado que “adhiere a la política de resguardo de la salud pública” del Gobierno Nacional. Así, Boca-Racing y Colón-Independiente se reprogramarían para después del 30 de mayo._**

El Gobierno y la AFA acordaron este viernes la suspensión de las semifinales de la Copa de la Liga que este fin de semana se iban a disputar con los partidos Boca-Racing y Colón-Independiente, en el marco del nuevo confinamiento nacional anunciado por el presidente Alberto Fernández.

“La Liga Profesional de Fútbol de la AFA adhiere a la política de resguardo de la salud pública determinada por las autoridades nacionales, y decidió suspender y reprogramar los partidos de semifinales y final de la Copa de la Liga”, se indicó en una breve nota difundida a través de las redes sociales.

Los encuentros habían sido programados originalmente para su disputa en el estadio San Juan Del Bicentenario, que ya fue la sede de la definición de la Copa Diego Maradona. La primera semifinal, entre Colón e Independiente, estaba programada para el sábado 22, a partir de las 19, mientras que la segunda, entre Racing y Boca, tendría lugar el domingo 23, desde las 15.30.

Minutos antes de la nota oficial emitida por las autoridades de la Copa de la Liga, el jefe de Ministros, Santiago Cafiero, dijo que “el fútbol siempre tuvo una actitud de mucha responsabilidad”, y anticipó así la decisión consensuada entre el Ejecutivo Nacional y la dirigencia del fútbol argentino.

La AFA, en tanto, dio a conocer los argumentos de la decisión mediante un comunicado en el que indica que acompañará a las autoridades nacionales “en este momento donde nuestro querido país debe hacer un esfuerzo extra para controlar esta angustiante pandemia que nos aqueja”.

El comunicado

La Asociación del Fútbol Argentino comunica a la opinión pública que a fin de acompañar las medidas sanitarias anunciadas por el Presidente Alberto Fernández en el día de ayer, tendientes a combatir la segunda ola de la pandemia Covid-19 que estamos viviendo, ha decidido suspender la programación de partidos en todas las competencias locales que organiza desde las 20 horas del día de hoy hasta el domingo 30 de mayo de 2021 inclusive.

Esta Asociación viene trabajando en conjunto con las autoridades nacionales, provinciales y municipales desde el 20 de marzo del 2020 en la elaboración de protocolos sanitarios destinados al cuidado de la familia del fútbol y la sociedad en general, respetando todas y cada una de las decisiones tomadas por nuestros mandatarios y por los expertos epidemiológicos.

Es por ello, que en este momento donde nuestro querido país debe hacer un esfuerzo extra para controlar esta angustiante pandemia que nos aqueja, la AFA y todos los que componemos el fútbol acompañaremos una vez más a las autoridades nacionales suspendiendo la programación de partidos correspondientes a todas las competencias locales por el lapso de 9 días.

En ese sentido, trabajaremos en conjunto con los gobiernos de las provincias de San Juan, Santiago del Estero y las autoridades de la Liga Profesional de Fútbol a fin de reprogramar las semifinales y la gran final de la Copa de la Liga en los escenarios originalmente previstos.

Cabe mencionar en este contexto que la provincia de San Juan, gobernada por Sergio Uñac y que iba a recibir las semifinales dela Copa de la Liga, adhirió al DNU del Ejecutivo Nacional y le había transmitido al Gobierno su preferencia por la reprogramación de las semifinales ante el recrudecimiento de la pandemia.