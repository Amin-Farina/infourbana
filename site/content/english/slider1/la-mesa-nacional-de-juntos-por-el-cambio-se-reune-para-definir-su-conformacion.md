+++
author = ""
date = 2021-12-27T03:00:00Z
description = ""
image = "/images/1640612002688-1.jpg"
image_webp = "/images/1640612002688.jpg"
title = "LA MESA NACIONAL DE JUNTOS POR EL CAMBIO SE REÚNE PARA DEFINIR SU CONFORMACIÓN"

+++
#### **_Con el objetivo de reducir su cantidad de miembros y en sumido en una dispersión de sus bloques legislativos, el espacio de conducción de la coalición opositora define su conformación._**

La Mesa Nacional de Juntos por el Cambio (JxC) se reunirá este mediodía en la sede del PRO del barrio porteño de San Telmo, en un encuentro marcado por los cambios en el espacio de conducción de la coalición opositora tras los comicios legislativos de noviembre pasado.

Según conversaron la semana pasada los presidentes de los partidos que integran el espacio, el objetivo del encuentro es intentar reducir la cantidad de miembros de la Mesa, para que sea más ejecutiva en la toma de decisiones.

La idea consensuada hasta el momento es que en la Mesa participen los presidentes de los partidos de JxC, los gobernadores del espacio y los titulares de los bloques legislativos. Asimismo, cada presidente de los partidos que forman la coalición podría sumar un integrante y en este sentido Patricia Bullrich, titular del PRO, agregaría al expresidente Mauricio Macri y Gerardo Morales, presidente del radicalismo, a Martín Lousteau.

Uno de los temas que dominará el encuentro es el llamado Consenso Fiscal que firmará esta tarde el presidente Alberto Fernández con los gobernadores, y que fue motivo de fricciones internas dentro de Cambiemos.

Mientras los gobernadores Gerardo Morales (Jujuy), Rodolfo Suárez (Mendoza) y Gustavo Valdés (Corrientes) están dispuestos a firmar el pacto fiscal con la Casa Rosada, el jefe de Gobierno porteño, Horacio Rodríguez Larreta, ya avisó que no rubricará el acuerdo.

Durante la reunión también se tratará de mejorar la coordinación legislativa de JxC: el espacio tenía en la Cámara de Diputados tres bloques y luego de las legislativas se atomizó a diez bloques.

En este sentido, siguen las repercusiones dentro de Juntos por el Cambio por la sesión legislativa en la cual el Frente de Todos logró aprobar cambios en la norma sobre Bienes Personales por solo un voto, y con la ausencia de tres legisladores opositores.