+++
author = ""
date = 2022-01-31T03:00:00Z
description = ""
image = "/images/respaldo-carlos-zannini-juan-manuel-abal-medina-y-oscar-parrilli-los-de-mayor-confianza-0828-g1-1.jpg"
image_webp = "/images/respaldo-carlos-zannini-juan-manuel-abal-medina-y-oscar-parrilli-los-de-mayor-confianza-0828-g1.jpg"
title = "ZANNINI Y ABAL MEDINA DECLARARÁN MAÑANA EN UN JUICIO CONTRA CRISTINA KIRCHNER"

+++

#### **_Fueron citados como testigos por la propia vicepresidenta. La lista también la integra el presidente Alberto Fernández, que buscará demorar su exposición ante el TOF por su visita a Rusia, China y Barbados_**

El Tribunal Oral Federal 2, que lleva adelante el juicio oral por la obra pública concedida a Lázaro Báez en Santa Cruz entre 2003 y 2015, retomará este martes las audiencias con una ronda de declaraciones pedidas por la principal acusada, la vicepresidenta Cristina Kirchner. Así comenzará el desfile de los ex jefes de Gabinete de su Gobierno y el de su esposo, Néstor Kirchner, entre los cuales está el presidente Alberto Fernández.

Para este martes 1° de febrero, día del regreso a la actividad judicial y de la marcha que el kirchnerismo motorizó contra la Corte Suprema, los jueces Jorge Gorini, Rodrigo Giménez Uriburu y Andrés Basso esperan el testimonio del ex jefe de Gabinete Juan Manuel Abal Medina y del ex secretario Legal y Técnico y hoy Procurador del Tesoro, Carlos Zannini. La cita está prevista para las 14.30 y 15.30.

Según el cronograma que había fijado el TOF a fines de diciembre, para el 7 de febrero fueron citados el presidente de la Cámara de Diputados, Sergio Massa, y el gobernador de Chaco, Jorge Capitanich. Y para el 8 de febrero el presidente de la Nación, Alberto Fernández, y el ministro de Seguridad de la Nación, Aníbal Fernández.

Por los cargos que ocupan los cuatro actualmente, esos testigos tienen la posibilidad de declarar por escrito. Sin embargo, el presidente Alberto Fernández tiene intenciones de presentarse ante el TOF y hablar ante los jueces. Por ese motivo, solicitará una postergación de la citación porque viaja a Rusia, China y Barbados en los primeros días de febrero y no estará para el día de su convocatoria en tribunales, confirmaron a este medio fuentes oficiales. Lo mismo sucedería con Massa, que tiene un viaje previsto a Panamá.

Las partes del juicio hasta ahora no fueron notificadas de ningún cambio en el cronograma, aunque puede informarse a partir del próximo martes cuando se reactive la actividad en los tribunales.

Con la declaración de los ex jefes de Gabinetes K, comenzará el capítulo más político del juicio oral que se sustancia desde el 21 de mayo de 2019 en Comodoro Py 2002, con la entonces ex presidenta como principal acusada de haber encabezado una asociación ilícita que buscó desviar fondos de la obra pública de Santa Cruz, privilegiando al empresario Lázaro Báez.

Apenas tres días antes de sentarse en el banquillo de los tribunales de Retiro, Cristina Kirchner anunció públicamente que Alberto Fernández sería el candidato a presidente del Frente de Todos, secundado por ella como vicepresidenta.

Fue la propia CFK, una semana antes de asumir como vicepresidenta, la que se encargó de disparar ante el TOF por su acusación y destacar la importancia de la citación de sus ex jefes de Gabinete. “Van a tener que citar al Presidente de la República que fue jefe de gabinete de 2003 a 2008. Será interesante escuchar lo que tiene para decirles”, dijo Cristina Kirchner cuando declaró en indagatoria en el juicio, el 2 de diciembre de 2019, pocos días antes de asumir.

Es que los ex jefes de Gabinete eran los encargados de las resignaciones de los fondos del presupuesto nacional. Esa metodología se uso para enviarle partidas de dinero a Báez. Zannini, en tanto, como secretario de Legal y Técnica era quien controlaba la formalidad de las resignaciones.

En el juicio hay trece acusados, entre los que está el ex ministro de Planificación Federal Julio De Vido el ex secretario de Obras Públicas José López -condenado por enriquecimiento ilícito y devenido en arrepentido en la causa de los cuadernos- y el ex titular de la Dirección de Vialidad Nacional Nelson Periotti, entre otros. También está sentado en el banquillo Lázaro Báez, ya condenado por lavado de dinero en la causa conocida como “ruta del dinero”.

Allí se intenta determinar si hubo irregularidades en las 51 obras públicas que recibió el empresario para la provincia de Santa Cruz durante los gobiernos de Néstor y Cristina Kirchner. La acusación habla de sobreprecios, obras sin terminar y asignaciones cuando las compañías no tenían antecedentes para hacerlas. Las defensas rechazan esa hipótesis y cuestionan el peritaje que derivó en esa conclusiones.

Para la hoy vicepresidenta, el juicio en su contra fue “un plan ordenado por el gobierno” de Mauricio Macri, con la complicidad de jueces y medios de comunicación. “Soy jefa de cuatro asociaciones ilícitas, la verdad que no sé cómo tuve tiempo para gobernar porque me la pasaba haciendo asociaciones ilícitas”, había dicho el día de su exposición que terminó vaticinando el resultado del juicio como una advertencia a los jueces: “Este tribunal, el del lawfare, seguramente tiene la condena escrita. No me interesa, a mí me absolvió la historia, me va a absolver la historia. Y a ustedes, seguramente, los va a condenar la historia”