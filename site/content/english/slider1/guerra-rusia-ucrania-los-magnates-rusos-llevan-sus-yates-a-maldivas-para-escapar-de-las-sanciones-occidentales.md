+++
author = ""
date = 2022-03-03T03:00:00Z
description = ""
image = "/images/maldivas_norma_st-regis.jpg"
image_webp = "/images/maldivas_norma_st-regis.jpg"
title = "GUERRA RUSIA-UCRANIA: LOS MAGNATES RUSOS LLEVAN SUS YATES A MALDIVAS PARA ESCAPAR DE LAS SANCIONES OCCIDENTALES"

+++

##### **_Multimillonarios rusos decidieron trasladar sus superyates a Maldivas, una nación insular del Océano Índico que no tiene tratados de extradición con Estados Unidos, tras la imposición de severas sanciones occidentales a Rusia en represalia por su invasión de Ucrania el 24 de febrero._** 

En tanto, las sanciones no impidieron que los oligarcas rusos continúen entrando y saliendo de Moscú con sus jets privados.

El superyate Clio, propiedad de Oleg Deripaska, el fundador del gigante de aluminio Rusal, fondeó frente a la capital Male el miércoles, según la base de datos marítima MarineTraffic.

Deripaska, quien alguna vez fue uno de los hombres más ricos de Rusia, fue sancionado por Estados Unidos en 2018 . El departamento del Tesoro lo acusó de lavar dinero, ordenar el asesinato de un empresario y de tener vínculos con grupos del crimen organizado ruso.

Washington impuso sanciones a Deripaska y a otros rusos influyentes en 2018 por sus vínculos con el presidente Vladimir Putin tras la supuesta injerencia rusa en las elecciones estadounidenses de 2016, algo que Moscú niega.

El “Titán”, propiedad de Alexander Abramov, cofundador del productor de acero Evraz, llegó el 28 de febrero a los puertos de Maldivas.

Mientras tanto, otros tres yates de multimillonarios fueron vistos en las aguas de la isla este miércoles. Entre ellos se encuentra “Nirvana”, de 88 metros, propiedad del hombre más rico de Rusia, Vladimir Potanin.

La mayoría de los buques fueron vistos por última vez anclados en puertos de Oriente Medio a principios de año.

##### Jets en el aire

En tanto, los jets privados de la elite rusa siguen recorriendo el mundo, detectados por el sitio Flightradar.

Bloomberg informó el viernes que los aviones de los multimillonarios rusos aterrizan en Moscú con normalidad, a pesar de las sanciones occidentales.

Los aviones de Dmitry Mazepin, el presidente de la empresa química Uralchem, y del barón del acero Alexey Mordashov, aterrizaron en la capital rusa el jueves. La aeronave de Mazepin llegó desde Nueva York, mientras el de Mordashov desde las islas Seychelles.

Estados Unidos dijo que tomará medidas estrictas para confiscar los bienes de los rusos sancionados.

“Esta próxima semana, lanzaremos un grupo de trabajo multilateral transatlántico para identificar, cazar y congelar los activos de las empresas y oligarcas rusos sancionados: sus yates, sus mansiones y cualquier otra ganancia mal habida que podamos encontrar y congelar según la ley”, dijo la Casa Blanca en un tuit el domingo.