+++
author = ""
date = 2021-11-29T03:00:00Z
description = ""
image = "/images/1638299038712-2.jpg"
image_webp = "/images/1638299038712-1.jpg"
title = "EN NOVIEMBRE EL BANCO CENTRAL PERDIÓ LA TERCERA PARTE DE LOS DÓLARES QUE DEJARÁ LA COSECHA"

+++

#### **_El retraso cambiario no ayudó a controlar la inflación. Las cotizaciones del mercado de dólar futuro ven una aceleración de la devaluación para los próximos meses_**

El Banco Central sigue vendiendo dólares, aunque no intervenga en los mercados financieros. Los inversores, al ver que las reservas están en un nivel crítico, las atacan tratando de hacerse de dólares al precio oficial de cualquier manera. Hay importadores que van a la Justicia y logran recursos de amparos y otros, que no los necesitan porque sus operaciones están aprobadas, tratan de comprar en el exterior todo lo que pueden porque no saben cuánto tiempo de vida le queda al anclaje. Por supuesto, también hay trampas clásicas como sobrefacturar importaciones o crear empresas importadoras para girar dólares al exterior sin traer la mercadería. Por el lado de la exportación, se retienen las ventas al exterior porque pueden financiarse a tasas de mercado convenientes. No faltan, y la Prefectura, Gendarmería y Aduana, dan fe, las ventas al exterior no declaradas triangulando con algún país limítrofe.

En este marco, el Banco Central insistió en no mover el tipo de cambio y mantuvo el dólar a $100,94. La demanda de los importadores hizo que tuviera que venderles USD 135 millones que le hicieron perder USD 489 millones en reservas que perforaron el piso de los USD 42 mil millones y quedaron en USD 41.549 millones. En noviembre, el BCRA perdió USD 1.267 millones, una cifra que es apenas UDS 600 millones inferior a lo que hay que pagarle al FMI a fin de mes y la tercera parte de lo que esperan que los productores ingresen por la cosecha fina este mes.

¿Vale el retraso cambiario este costo? Por caso, en los mercados de futuros no creen que puedan soportar mucho tiempo más este atraso que no logró frenar siquiera la inflación. De hecho, el Banco Central, intervino en ese mercado. Noviembre cerró a $100,92 y gano $4.500 millones porque estuvo en posiciones vendedoras y el dólar cerró por debajo del valor al que apostaron los compradores. El FMI le autoriza al Central a comprar y vender por el equivalente a USD 6.500 millones. Como ese cupo estaba agotado, no pudo intervenir en las últimas semanas y los futuros más cortos se le dispararon. Por eso aprovechó lo que le liberó noviembre para vender en las posiciones fin de diciembre, fin de enero y fin de febrero que tuvieron bajas de hasta 0,67%.

Fin de año cerró a $105,05 lo que equivale a una devaluación de 4,03% que prácticamente acompaña a la inflación. La duda es si el Central insistirá con el anclaje y devaluará 1,24% como ocurrió en noviembre. Para fin de enero, los compradores de dólar futuro apuestan a que cotizará a $111,20 y fin de febrero a $115,40. El ritmo de devaluación de enero, según el precio futuro, es de 5,85% que equivale a una tasa anual de 70,25%.

Cuando se observan estas cifras, se nota la pulseada entre el sector público y el privado. La diferencia es que los privados saben que no hay suficientes reservas para sostener al dólar y que el ingreso de divisas depende pura y exclusivamente de sus exportaciones, más allá de que arregle o no con el FMI. Los que especulan conocen una ley primordial de la economía: el producto más escaso no puede ser el más barato.

En la plaza de los dólares financieros, todo es confusión. La circular del Banco Central que obliga a las entidades a que sus posiciones en dólares contado no excedan la Responsabilidad Patrimonial, los obligaría a incumplir con los encajes de los depósitos en dólares y garantías que tienen por operaciones en el exterior de sus clientes con tarjetas de crédito y con bancos con los que intercambian operaciones. Los bancos le reclamaron a Miguel Pesce, el titular del Central, por este parche que se llevó por delante otras regulaciones.

Esta situación hizo que menguara la venta de dólares de las entidades para cumplir con la circular, algo que no ocurrió el lunes. De esta manera, el dólar MEP recuperó el valor que había perdido el día anterior y subió $3,78 a $200,72 en la plaza oficial donde se opera con el bono AL30D. Contra el GD30, subió $3,20 y cerró a $202.

El contado con liquidación tuvo un comportamiento dispar, aunque hay que reconocer que en el mercado del AL30C, su volumen es inexistente. Allí cotizó a $214,49 (-$1,49) y en las negociaciones libres, en el SENEBI, donde operan con el operador sin que aparezcan los precios en pantalla, cerró $4,30 arriba a $215,50.

El “blue” también reaccionó y aumentó 50 centavos a $201,50. Cabe aclarar que, por el fin de mes, el mercado de divisas tuvo escaso movimiento.

Los bonos de la deuda siguen sin encontrar piso y los que entraron al canje están más que arrepentidos por lo desolador de las cotizaciones. Los títulos con ley extranjera cayeron hasta 1,20% e hicieron subir el riesgo país 21 unidades a 1.898 puntos básicos. Durante la rueda perforó el techo de los 1.900 puntos. Sus precios están próximos a los de un bono defaulteado.

EVOLUCIÓN DEL RIESGO PAÍS

Pero cabe aclarar que la baja de los bonos no es patrimonio de la Argentina. Los bonos ecuatorianos perdieron 2% y los indicadores de riesgo país de todos los emergentes aumentaron porque preocupa la inflación de Estados Unidos y Europa y que la Reserva Federal comience a restringir la ayuda a los mercados. Por eso subieron los bonos del Tesoro de norteamericano y su renta bajó a 1,44%. Esto significa que hubo inversores que sacaron de sus carteras a los bonos emergentes para cambiarlos por bonos menos rentables, pero de menor riesgo.

La Argentina se ve afectada más que nadie porque tiene uno de los riesgos más altos del mundo. Brasil, por caso, tiene un riesgo de 366 puntos básicos; Perú de 164 y Uruguay de 149.