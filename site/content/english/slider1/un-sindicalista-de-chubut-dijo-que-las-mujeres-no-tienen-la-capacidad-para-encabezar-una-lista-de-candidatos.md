+++
author = ""
date = 2021-07-22T03:00:00Z
description = ""
image = "/images/541014_95-1.png"
image_webp = "/images/541014_95.png"
title = "UN SINDICALISTA DE CHUBUT DIJO QUE \"LAS MUJERES NO TIENEN LA CAPACIDAD PARA ENCABEZAR UNA LISTA DE CANDIDATOS\""

+++

**_El secretario general del Sindicato de Petroleros Privados, Jorge Ávila, entró en un nuevo escándalo luego de una polémica declaración sobre las listas de las candidaturas de cara a las elecciones legislativas, donde descartó la participación de mujeres por “falta de capacidad técnica”._**

El referente gremial petrolero afirmó que el sindicato no pudo presentar un precandidato porque “es una elección muy cerrada” y los “obligan a buscar candidatos a nivel local para acompañarlos” y que en esa figura apareció el ex intendente Carlos Linares.

“Estamos acompañando y creemos que vamos a acompañar porque creemos que es el único que nos puede llegar a abrir alguna puerta si se abre un conflicto nacional a nivel petróleo o a nivel general en la República Argentina”, aseguró.

Asimismo, en declaraciones a FM Generis 101.5, Avila consideró “lógico que el candidato que nos represente sea de Comodoro Rivadavia y no que encabece la lista una mujer”.

Pero no quedó ahí. Para justificar su postura, explicó que no puede ser una mujer porque no cuentan con la “capacidad técnica” para elaborar un proyecto que los pueda representar. “Nosotros creamos la Secretaría de la Mujer, tenemos mujeres preparadas para encarar pero no están hoy en condiciones para ir a hacer un análisis de ese tipo. Hoy no hay mujeres con esa condición”, afirmó.

“Necesitamos tanto de los hombres y las mujeres para ir a poner el hombro para ayudarnos. No es que estemos en contra de la mujer ni que estemos diciendo que estamos favoreciendo a un sector. Sino simplemente hoy en condiciones políticas dentro de la provincia, la capacidad de los hombres está un puntito más arriba que la de las mujeres”, concluyó.

Las declaraciones de Ávila generaron repercusiones de inmediato. “Nos preocupa profundamente que un representante del Movimiento Obrero Organizado y figura pública incurra en esas expresiones”, sostiene el documento que hicieron público desde el Frente de Todos Chubut, y consideraron que “algunos dirigentes no están a la altura de las circunstancias actuales”.

“Las Mujeres y Diversidades del Frente de Todxs de Chubut expresamos nuestro máximo repudio a los dichos del Secretario General del Sindicato del Petróleo y Gas del Chubut, Jorge “Loma” Ávila, con respecto al armado de listas electorales para Legisladores y Legisladoras de cara a las PASO 2021, refiriéndose a las mujeres como ‘no tener la capacidad técnica para elaborar un proyecto y de llevar algo a Nación que nos pueda representar’, y apuntando al género no de estar en condiciones ‘para integrar una lista de candidatos’”, sostiene el texto que hicieron público.

El comunicado agrega: “Escuchar declaraciones machistas, misóginas, y con elevado tono discriminatorio, después de tantos logros referidos a la equidad, la igualdad, los derechos adquiridos en relación al género y diversidades, no hace más que corroborar que algunos dirigentes no están a la altura de las circunstancias actuales”.

“Exigimos al sr. Loma Ávila la inmediata retractación de sus dichos, y la reparación de la situación encarando acciones positivas como la implementación de la formación en género -Ley Micaela- en su persona y en el sindicato que representa”, concluye el comunicado, que lleva la firma de dirigentes políticos, legisladores y distintas organizaciones.