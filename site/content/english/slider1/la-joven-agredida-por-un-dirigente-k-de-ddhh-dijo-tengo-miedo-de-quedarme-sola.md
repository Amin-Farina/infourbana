+++
author = ""
date = 2022-02-14T03:00:00Z
description = ""
image = "/images/xubxbrj3ozg23ishmmmbeakymq.jpg"
image_webp = "/images/xubxbrj3ozg23ishmmmbeakymq.jpg"
title = "LA JOVEN AGREDIDA POR UN DIRIGENTE K DE DDHH DIJO: “TENGO MIEDO DE QUEDARME SOLA”"

+++

#### **_Candela Valdez dijo que José Schulman solo le pidió disculpas obligado por la policía y después de enterarse de que todo había quedado grabado por las cámaras de seguridad_**

Candela Valdez, la empleada de la terminal de ómnibus de Santa Clara del Mar agredida por el dirigente kirchnerista y presidente de la Liga Argentina por los Derechos Humanos, José Schulman, dijo este domingo que desde el ataque tiene miedo de quedarse sola en el trabajo. Además contó que el agresor solo le pidió disculpas después de que se enteró que todo había quedado registrado por las cámaras de seguridad.

“Ahora tienen que quedarse conmigo los chicos que llevan las valijas o mi familia haciéndome compañía porque tengo miedo de quedarme sola”, dijo Váldez.

José Schulman quedó involucrado en el escándalo por insultar y golpear con una fuerte bofetada a Valdez, de 21 años, empleada en la terminal de micros de el balneario bonaerense de Santa Clara del Mar. La violenta secuencia quedó grabada por una cámaras de seguridad.

> “Por momentos me agarra angustia e impotencia, pero trato de estar bien”, indicó.

###### **¿Cómo fue la agresión de José Schulman contra una empleada de la terminal de ómnibus de Santa Clara del Mar?**

El jueves pasado, Schulman insultó y golpeó a Valdez, una empleada en la terminal de ómnibus de Santa Clara del Mar, tras quejarse en forma reiterada por las demoras en el servicio.

En las imágenes captadas por las cámaras de seguridad se observa cómo Schulman le sacó fotos a la empleada de la terminal, a la que amenazó con “meterla en cana” e insultarla. Además, le recriminó que se rió y la golpeó.

Valdez contó que Schulman la agredió porque el micro tuvo un desperfecto y ella fue la encargada de comunicarle a los pasajeros la razón de la demora. Tras dos horas de espera, el dirigente kirchnerista se acercó y comenzó a agredirla.

En ese momento, Schulman la trató de “mentirosa” y le dijo que era “una pendeja de mierda”, tras lo cual la amenazó -junto a una mujer que estaba en el lugar para comprar un pasaje- con meterla presa. Y ese instante le dio una bofetada.

“Una no puede hacer nada, tenes que quedarte callada porque te mata si no. No me dio tiempo a nada”, comentó.

Valdéz contó que tras la agresión llegó un chofer y Schulman se subió al micro y no se quería bajar. Entonces llamaron a la policía y el agresor no quería descender del vehículo y negó el episodio.

> “En el único momento en que vino a pedirme disculpas fue cuando el oficial (de policía) le dijo que iban a mirar las cámaras” de seguridad. “Ahí abrió grande los ojos. Yo no las sentí sinceras, fueron más por obligación. Cuando vio que había cámaras no le quedó otra que pedir perdón porque se vio atrapado”, señaló.

###### **Valdéz hizo la denuncia por lesiones leves ante la comisaría**

Más tarde, Schulman pidió disculpas en su cuenta de Facebook por haber insultado y golpeado a la empleada. El dirigente kirchnerista se dirigió a sus “compañeres” y se disculpó por “haber llevado adelante una conducta reprochable con una trabajadora”. Incluso calificó de “inaceptable” el episodio

Schulman dijo que “como muches saben” es discapacitado motriz. Al intentar explicar la situación, dijo que que pasó muchas horas de espera de un micro para volver de su viaje a la costa y eso le produjo, según afirmó, “un enorme dolor” y que lo “desencajó”.