+++
author = ""
date = 2022-02-21T03:00:00Z
description = ""
image = "/images/plaini-impuesto-bienes-exterior.jpg"
image_webp = "/images/plaini-impuesto-bienes-exterior.jpg"
title = "EL KIRCHNERISMO QUIERE OTRO IMPUESTO PARA LOS ARGENTINOS QUE TIENEN BIENES Y ACTIVOS EN EL EXTERIOR"

+++

##### **_“El ajuste no debe pagarlo el trabajador”, planteó el dirigente sindical y senador provincial Omar Plaini. La iniciativa se suma a la propuesta de hacer permanente el gravamen a las grandes fortunas_**

“Hay una manera de hacer que paguen los que más tienen”. Con esta frase, el sindicalista y senador provincial Omar Plaini introdujo su propuesta para incrementar la recaudación mientras dure el convenio entre el Gobierno y el FMI, la misma que expresó en la reunión del Consejo del PJ bonaerense y aplaudió el flamante presidente del partido, Máximo Kirchner.

De manera resumida, **el dirigente kirchnerista propuso no sólo hacer permanente el impuesto a las grandes fortunas,** que nació por impulso del diputado Kirchner y Carlos Heller como un “aporte solidario y extraordinario para ayudar a morigerar los efectos de la pandemia”, sino que **también busca que el gravamen sea extensible a los argentinos que tienen bienes y activos en el exterior**.

“Este ultimo endeudamiento de argentina, que fue una estafa, una irresponsabilidad del gobierno anterior y del FMI, fue un acción geopolítica que hizo el propio (Donald) Trump. Esta estafa no se puede aceptar una vez más que la pague el pueblo argentino”, señaló Plaini a radio El Destape.

> Y agregó: “Esta vez no lo debe pagar el hombre y la mujer de pie, con el esfuerzo de su trabajo, así que vengo sugiriendo que aquel gravamen que se cobró el año pasado a las grandes fortunas quede de forma permanente hasta tanto esté el condicionamiento que hace el fondo de cumplir con los 44 mil millones de dólares”.

Ese tributo “por única vez” recayó sobre personas físicas con bienes declarados por $200 millones o más. En este caso, la propuesta de Plaini busca abarcar a personas con patrimonios menores a aquel umbral, sumando una nueva condición.

> “Y también que haya otra ley para que todos los argentinos que tengan bienes y activos en el exterior hagan un aporte. Sería la mejor manera de resolver esta estafa. El ajuste no debe pagarlo el trabajador”, completó.

La discusión sobre este gravamen a las grandes fortunas cobró intensidad en las últimas semanas, sobre todo en el marco de la restricción económica que implica el eventual acuerdo con el FMI, y también por las declaraciones del ex futbolista Sergio Kun Agüero, que en sus redes sociales se preguntó por qué si como jugador pagó altos impuestos sobre sus ingresos, debería ahora pagar sobre lo que acumuló con sus ahorros.

“Es una locura (....) Si vos toda tu vida generaste plata y pagaste los ingresos, pagaste los impuestos, ¿por qué tenés que seguir pagando más?”, afirmó.

La opinión del astro del fútbol, que se retiró por una afección cardíaca tras brillar en Argentina, España e Inglaterra, generó fuerte repercusión en las redes sociales y reacciones del mundo económico y, sobre todo, político. “Lo que dice Sergio Agüero es impecable y es exactamente lo que Argentina debe cambiar: el Estado debe dejar de saquear a los contribuyentes con impuestos”, fue la opinión del diputado nacional Ricardo López Murphy.

En diálogo con sus seguidores y, en términos precisos, el Kun contó lo que pasa con los sistemas impositivos de distintos países y argumentó que los Estados que cobran ese tipo de impuestos caen en una doble imposición, porque son bienes y activos que ya pagaron impuestos a los ingresos. 

En el caso argentino, con el aporte extraordinario, se trata de una tercera imposición. “Ustedes saben lo que es el patrimonio ¿no? Patrimonio es todo lo que tenés en el mundo. Hay países que por tener plata en tu cuenta o en cualquier parte del mundo a vos te cobran. Anual. Te cobran, ¿ok?”, empezó el delantero, quien aclaró que no tributa ante el fisco argentino.

“¿Por qué te cobran? Si vos ya los pagás los impuestos. Entonces, tenés varios países que no te cobran. Yo a lo que voy es, escuchen bien. si vos generás ingresos -y a la gente le gusta pagar menos-, a mi no me molesta pagar por ingresos. Si vos tenés ingresos, claramente es porque estás trabajando y te está yendo bien. Entonces no pasa nada si pagás por ingresos. Pagás el 30%, el 35%, el 50%. En Inglaterra pagás el 50%, por ejemplo. ¿Entendés? Pero vos estás generando plata, entonces si estás generando plata está bien, pagás”, explicó Agüero.

“Pero lo que no me convence es, digamos, que vos pagues un porcentaje anual del patrimonio que vos tengas. Me parece que es una locura en cualquier lugar del mundo. Que hay lugares, un montón de países, que parece que si vos ya estás pagando los ingresos, ¿por qué te siguen sacando más plata? ¿Entendés? Si vos toda tu vida generaste plata y pagaste los ingresos, pagaste los impuestos, ¿por qué tenés que seguir pagando más?”, aseguró el Kun, en una crítica desde el sentido común a los países que cobran el tipo de impuestos que ideó, impulsó y logró que se apruebe Máximo Kirchner.

En su primera y supuestamente única versión, el impuesto a la riqueza recaudó unos $ 240.000 millones, lejos de los $ 400.000 millones que se preveían.