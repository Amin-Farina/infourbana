+++
author = ""
date = 2021-07-28T03:00:00Z
description = ""
image = "/images/bsqueda-trabajo-conurbanojpg-337066-1.jpg"
image_webp = "/images/bsqueda-trabajo-conurbanojpg-337066.jpg"
title = "NUEVO PROGRAMA DE INCERSIÓN LABORAL PODRÍA BENEFICIAR A 50.000 JÓVENES"

+++

**_El programa permitirá que las pequeñas y medianas empresa que contraten jóvenes se vean beneficiadas con una reducción en aportes patronales, y además reciban un apoyo económico del Estado para el pago del salario durante los primeros 12 meses, a partir del alta laboral._**

El ministro de Desarrollo Productivo, Matías Kulfas, adelantó este miércoles que el nuevo programa “Te Sumo” tendrá como objetivo fomentar la inserción laboral de 50.000 jóvenes de 18 a 24 años en una primera etapa.

El programa permitirá que las pequeñas y medianas empresa (pymes) que contraten jóvenes se vean beneficiadas con una reducción en aportes patronales, y además reciban un apoyo económico del Estado para el pago del salario durante los primeros 12 meses, a partir del alta laboral.

“El plan apunta a 50.000 jóvenes durante la primera etapa” señaló Kulfas esta mañana en diálogo con Radio 10, y agregó que no se tratará simplemente de un incentivo sino de un plan general que buscará “la formación y la sociabilidad de los chicos y chicas” en el mercado laboral; y que estos consigan su "primer empleo formal y de calidad".

El nuevo programa será anunciado formalmente en un acto a las 11:30 en el Parque Industrial del partido bonaerense de Almirante Brown, que contará con la presencia, además de Kulfas, del jefe de Gabinete, Santiago Cafiero; y del ministro de Trabajo, Claudio Moroni.

“Se busca que sea la primera experiencia de tener un empleo formal para jóvenes de todo el país”, expresó el ministro en referencia a la nueva iniciativa.

Según Kulfas, las pymes que inscriban a jóvenes en sus plantillas inmediatamente podrán acceder a esta serie de beneficios, que incluyen el pago de parte del salario además de una reducción en las contribuciones patronales.

“El programa va a dar un incentivo económico muy fuerte que además va a ser mayor en el caso de las mujeres jóvenes”, destacó el titular de la cartera de Desarrollo Productivo.

De acuerdo con el Indec, actualmente el grupo más afectado por el desempleo son las mujeres de 14 a 29 años con una tasa de 24,9% en el primer trimestre de 2021, más del doble de la tasa general de 10,2%, además de representar un aumento de 1% respecto del mismo periodo de 2020.

En tanto, en el caso de los varones jóvenes, la tasa de desocupados es de 17% en comparación con el 18,5% de igual periodo del año anterior.

“Con la pandemia se han agudizado las brechas de género”, dijo Kulfas en referencia a la problemática, y lo adjudicó al hecho de que los sectores con mayor recuperación “son los más masculinizados como la industria y la construcción”.

Respecto de la marcha de la economía, Kulfas recordó que el crecimiento se encuentra “a dos velocidades”, con “sectores muy expansivos como la construcción, la industria, la economía del conocimiento y parte del agro”, y otras áreas “muy golpeadas como la gastronomía y el turismo”.

El ministro reiteró su expectativa de que a medida de que avance la campaña de vacunación, “todos los sectores se incorporen a la recuperación económica”.

##### **Para Apyme, el programa es “sumamente auspicioso”**

El presidente de la delegación Buenos Aires de la Asamblea de Pequeños y Medianos Empresarios, Daniel Cámpora, manifestó que el nuevo programa de empleo juvenil Te Sumo "resulta sumamente auspicioso”, al recoger ideas y posicionamientos planteados por la entidad.

“Responde a parte de nuestras propuestas en cuanto a la generación de empleo de calidad y oportunidad del primer empleo para los jóvenes, que es la franja donde se da el mayor índice de desocupación”, puntualizó Cámpora a Télam Radio, en referencia al nuevo programa que busca fomentar la inserción laboral de los jóvenes.

Cámpora enfatizó la necesidad de que sumen a las pymes “jóvenes que puedan aportar desde una nueva mirada a la generación de trabajo de calidad”, en el marco de un “plan de recuperación de la productividad y de la economía en la Argentina"; y sugirió que se realice un seguimiento “del resultado de la aplicación del programa en las empresas”.