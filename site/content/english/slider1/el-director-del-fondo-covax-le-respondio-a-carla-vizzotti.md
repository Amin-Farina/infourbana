+++
author = ""
date = 2021-06-02T03:00:00Z
description = ""
image = "/images/6093e93be0cb4_1004x565.jpg"
image_webp = "/images/6093e93be0cb4_1004x565-1.jpg"
title = "EL DIRECTOR DEL FONDO COVAX LE RESPONDIÓ A CARLA VIZZOTTI"

+++

**_La ministra de Salud, Carla Vizzotti, reprodujo esta mañana en una conferencia de prensa un mail de Santiago Cornejo, el director del fondo COVAX que ayer reconoció en público que la Argentina había rechazado vacunas del laboratorio Pfizer._**

En el escrito, que luego fue difundido por el equipo de comunicación de Presidencia de la Nación, está fechado hoy, 2 de junio, y lleva el asunto: “Aclaración sobre mis palabras”.

“En mi respuesta utilicé la traducción de los términos en nuestro acuerdo legal con los países (que llamamos “Opt-in/Opt-out Windows”) y la traducción de este término se está interpretando tan solo como una cuestión de interés de parte del gobierno con la vacuna cuando no es así. Estamos subiendo un comunicado en nuestra página web aclarando que la Argentina tenía interés de recibir la vacuna de Pfizer a través del mecanismo COVAX, pero como no acordó con los términos de indemnización y responsabilidad del fabricante, no pudo continuar con la ventana de COVAX”, explicitó Cornejo en el mail.

Y agregó: “Como el propósito de la reunión no era hablar sobre Argentina no entré en el detalle de esta transacción, que bien sabés que son muy complejas. En mi presentación tampoco resalté el compromiso de la Argentina con COVAX y que la Argentina no solo ha cumplido con todos nuestros requisitos sino también que tu equipo trabaja conjuntamente con nosotros para ayudarnos a mejorar nuestra respuesta multilateral”.

A partir de estos dichos de Cornejo, Vizzotti -visiblemente conmocionada- aseguró que es “mentira” que el gobierno argentino rechazó dosis de Pfizer y le pidió a la dirigencia política que “baje la obsesión” con el tema.

El Fondo COVAX es el mecanismo creado por la OMS para distribuir de manera equitativa vacunas contra el coronavirus en todo el mundo. Su director para América Latina es Cornejo, quien ayer en una sesión académica organizada por el Grupo Joven, el Comité de Salud Global y Seguridad Humana y el Grupo de Trabajo para la Cooperación al Desarrollo del Consejo Argentino de Relaciones Internacionales (CARI) fue consultado sobre la diputada nacional por Mendoza Claudia Najul sobre el vínculo con el mecanismo COVAX.

“Los países que se autofinancian tienen dos modelos, uno de compra comprometida y otra de compra opcional. Argentina es compra opcional. Entonces nosotros antes de firmar un acuerdo con cada compañía farmacéutica le damos la posibilidad al país de decidir si quiere que sumemos a la demanda en ese acuerdo. Si el país dice que no, obviamente eso no significa que van a recibir menos dosis de nosotros, sino que no van a recibir de ese candidato”, introdujo.

Luego, insistió en que en el caso de “Argentina es opcional” y concluyó: “Entonces antes de hacer cada acuerdo le preguntamos a Argentina y a los países opcionales si quieren acceder a esa vacuna. Y en el caso de Pfizer nos han dicho que no”.

Esta mañana, Cornejo -con Vizzotti como vocera- dijo que fue mal interpretado y se sorprendió por la repercusión de sus dichos. Lo llamativo del caso es que Cornejo es argentino y como tal conoce las diferencias surgidas en torno a la negociación entre el Gobierno y Pfizer por la adquisición de vacunas.

##### A continuación, la transcripción completa del mail de Cornejo:

_Estimada Carla,_

_Me comunico para aclarar mis comentarios que ahora están difundiendo los medios. El propósito de la reunión de ayer era describir el mecanismo COVAX y nuestra propuesta multilateral; por lo tanto cuando respondí a una pregunta sobre la Argentina lo hice rápidamente y no entre en detalle porque no era el propósito de la reunión. En mi respuesta utilicé la traducción de los términos en nuestro acuerdo legal con los países (que llamamos “Opt-in/Opt-out Windows”) y la traducción de este término se está interpretando tan solo como una cuestión de interés de parte del gobierno con la vacuna cuando no es así. Estamos subiendo un comunicado en nuestra página web aclarando que la Argentina tenía interés de recibir la vacuna de Pfizer a través del mecanismo COVAX pero como no acordó con los términos de indemnización y responsabilidad del fabricante no pudo continuar con la ventana de COVAX._

_Como el propósito de la reunión no era hablar sobre Argentina, no entré en el detalle de esta transacción, que bien sabés que son muy complejas. En mi presentación tampoco resalté el compromiso de la Argentina con COVAX y que la Argentina no solo ha cumplido con todos nuestros requisitos sino también que tu equipo trabaja conjuntamente con nosotros para ayudarnos a mejorar nuestra respuesta multilateral._

_En estos momento de crisis necesitamos trabajar todos juntos y tenemos que estar mas unidos que nunca. Justamente el mensaje de mi presentación y COVAX fue la necesidad de trabajar todos juntos porque es la única manera en la que vamos a derrotar esta pandemia. Fue una gran sorpresa la repercusión de mis dichos y no pude responder antes porque ocurrió en mi madrugada. Lamento el foco que se está haciendo de mis comentarios en un encuentro privado sobre el mecanismo COVAX y continuamos trabajando conjuntamente para que la Argentina y el mundo entero reciba vacunas de COVID._

_De mi mayor consideración,_

_Santiago Cornejo_