+++
author = ""
date = 2021-10-29T03:00:00Z
description = ""
image = "/images/g20-2.jpg"
image_webp = "/images/g20-1.jpg"
title = "EL G20 SE REÚNE EN ROMA CON TENSIONES Y EL FOCO PUESTO EN EL CLIMA, LA ECONOMÍA Y LA POSPANDEMIA "

+++

##### **_Con algunas ausencias como las de China y Rusia, los jefes de Estado y de Gobierno de los países miembros del G20 se reúnen este fin de semana en una Roma blindada para una cumbre que estará marcada por las tensiones entre las potencias, la lucha contra el cambio climático, la recuperación de la economía global, la pospandemia y el debut del presidente estadounidense Joe Biden en el foro._**

La reunión que se hará este sábado y el domingo en el centro de convenciones "La Nuvola" del barrio romano de Eur, y de la que participará el presidente Alberto Fernández, será la cumbre número 16 del foro que reúne a los países más industrializados del mundo y un grupo de naciones emergentes, y marcará el regreso de las cumbres presenciales de líderes tras el encuentro virtual de 2020 en Arabia Saudita.

El G20, está compuesto por la Unión Europea (UE) y 19 países: Alemania, Arabia Saudita, Argentina, Australia, Brasil, Canadá, China, Corea del Sur, Estados Unidos, Francia, India, Indonesia, Italia, Japón, México, Reino Unido, Rusia, Sudáfrica y Turquía. Algunos países que no son miembros, como España, participan de todos modos como invitados de forma regular.

Entre los 20 miembros, concentran el 60% de la población mundial, el 75% del comercio global y más del 80% del producto bruto global.

El encuentro que comienza este sábado se dará además en la víspera de la cumbre climática de la COP26 que iniciará el lunes en la ciudad escocesa de Glasgow, de la que también participará Alberto Fernández, y a la que irá la mayoría de los jefes de Estado y de Gobierno participantes de la reunión del G20.

La reunión de mandatarios, que iniciará cerca del mediodía italiano del sábado y terminará el domingo a las 16 (12 hora argentina) con una conferencia de prensa del anfitrión, tendrá de todos modos algunas ausencias de peso como las de los presidentes de la Federación Rusa, Vladiimr Putin, y de China, Xi Jinping.

Putin, que se quedará en su país a causa de la nueva ola del coronavirus, se conectará por videoconferencia, según informó el Kremlin a mediados de mes. Xi, en tanto, no sale de China desde que en enero de 2020 viajó a Myanmar, aunque -según confirmaron a Télam fuentes oficiales- la Presidencia italiana buscará acomodar los horarios de las discusiones para que "no sean excluidos ni un segundo de la posibilidad de que den su palabra".