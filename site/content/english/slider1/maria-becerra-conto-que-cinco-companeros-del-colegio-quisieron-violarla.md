+++
author = ""
date = 2021-06-26T03:00:00Z
description = ""
image = "/images/la-exitosa-cantante-trap-maria-becerra-los-mammones-1.jpg"
image_webp = "/images/la-exitosa-cantante-trap-maria-becerra-los-mammones.jpg"
title = "MARÍA BECERRA CONTÓ QUE CINCO COMPAÑEROS DEL COLEGIO QUISIERON VIOLARLA"

+++

**_La noche del sábado, María Becerra fue una de las invitadas a PH podemos hablar (Telefe). Si bien durante el programa recordó divertidas experiencias de sus comienzos en el mundo de la música, sorprendió al contar que vivió un intento de abuso sexual en su adolescencia y más precisamente en la escuela._**

Una de las preguntas planteadas por el conductor para sus invitados fue que den un paso al frente y cuenten sus experiencias quienes la pasaron mal durante la infancia y la adolescencia. Cuando llegó el turno de la cantante, manifestó: “Fue complicada mi pre-adolescencia. La pasé muy mal en el colegio, me hicieron mucho bullying y viví situaciones muy feas. El 2012 fue el peor año de mi vida”.

Al entrar en detalles indicó que como le gustaba la carpintería y su hermano había estudiado en un colegio técnico, ella también quería asistir a uno: “Les dije a mis viejos que quería ir y ellos no querían, pero les hinche tanto que me anotaron. Y ¡maldito el día en que me anotaron!”.

Becerra, de 21 años, señaló: “Éramos dos mujeres entre 400 varones. Antes era típico que vayas caminando y que los compañeros te tocaran el cul.., que te manosearan en el boliche o en el colegio. Eran así los varones, muy pajer... y yo estaba todos los días en la oficina quejándome y me mandaban a mí con la psicopedagoga. Las preceptoras me decían: ‘¿No pensaste en no usar jean?’. Me decían que trajera ropa holgada y ahí nació mi obsesión por usar ropa holgada. Hoy en día no me pongo un vestido ni de caño, por lo mal que la pasé. No me pongo short ni polleras”.

Andy Kusnetzoff se quedó atónito al escuchar las respuestas que le dieron a María las autoridades del colegio. Entonces ella se refirió a que además del acoso que sufrió de sus compañeros, también hubo violencia física: “Me pegaban feo, no había nadie controlando. Me tenían de punto. Me decían ‘aprovechamos y te pegamos pero te manoseamos’”.

La joven pasó un año en el establecimiento educativo y, según explicó, no le contaba nada a sus padres sobre lo que vivía porque los agresores le hacían creer que ella era la culpable de lo que le pasaba: “He llegado (a mi casa) moretoneada, con el labio cortajeado. Me pegaban feo y nunca le dije a mi mamá cuando me preguntaba, le metía excusas.. hasta que llegó un punto que bueno...”.

Al llegar a ese momento del relato, la cantante habló de una situación puntual que vivió con cinco compañeros que eran los mismos que la golpeaban reiteradamente: “Me llevaron al baño y me tenían uno de cada mano. Me tenían las piernas y me empezaron a querer sacar la ropa. Quisieron abusar de mí. Pasó justo una preceptora y yo estaba gritando y fue la nada misma. Me salvó que tocara el timbre, sino no sé lo que pasaba. Ese colegio era lo peor”.

Ese mismo día, cuando la joven volvió a su casa decidió contarle a su mamá lo ocurrido: “Ella se puso a llorar, se puso pálida y al otro día fue al colegio y armó un quilom... No podía creer cuando le conté todo lo que había pasado. Fue horrible, las caras de esos compañeros las tengo grabadas, no los quiero ver ni cruzar”.

Por último, Becerra dijo que cuando saltó a la fama y comenzó a hacerse conocida, uno de los jóvenes que la agredió la contactó por redes sociales: “Se llama Ariel y era el peor de todos. Me habló al Facebook como si nada hubiese pasado, me dijo ‘hola, yo era tu compañero, mirate ahora’. Yo le dije ‘¿vos no te acordás todo lo que hiciste en el colegio? ¿Cómo me hablás?’. Leí ese mensaje y me puse muy mal y lo mandé a la mier.., no sé ni lo que me contestó porque no quise leer. El colegio ahora está clausurado creo”.