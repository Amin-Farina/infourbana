+++
author = ""
date = 2021-09-13T03:00:00Z
description = ""
image = "/images/nrlbolwm2rguzknleoj6qd23sa.jpg"
image_webp = "/images/nrlbolwm2rguzknleoj6qd23sa.jpg"
title = "CÓMO QUEDARÁN LAS BOLETAS DE JUNTOS EN CABA Y EN PBA PARA LAS GENERALES"

+++

#### **_Diego Santilli se impuso en territorio bonaerense y María Eugenia Vidal en la ciudad de Buenos Aires. Tras las PASO, se empezó a definir en qué lugar irá cada candidato en los comicios de noviembre_**

Uno de los grandes ganadores de las PASO no fue candidato. El jefe de Gobierno porteño, Horacio Rodríguez Larreta, validó su liderazgo dentro de Juntos por el Cambio tras el desempeño electoral de Diego Santilli en la provincia de Buenos Aires y de María Eugenia Vidal en CABA, un resultado exitoso que lo tuvo como principal impulsor y defensor, tanto en el frente interno como en el externo.

Ahora, superada la etapa de PASO, en la coalición opositora se comenzó a diseñar cómo quedará integrada -hasta el momento- la oferta en ambos distritos, integrada por los partidos que participaron de la interna: PRO, UCR, Coalición Cívica, GEN, Confianza Pública y Peronismo Republicano.

En territorio bonaerense, Juntos por el Cambio resolvió -en su reglamento interno- no aplicar el sistema D’Hont para repartir las candidaturas y que la combinación con la ley de paridad de género no genere cortocircuitos. Así, y según el 60,18% de los votos que logró la lista “Es Juntos” contra el 39,81 de “Dar el Paso”, la boleta para diputados nacionales en las elecciones generales de diciembre estará encabezada por Santilli, seguido de Graciela Ocaña y el neurocientífico radical Facundo Manes, que obtuvo 1.254.220 de votos en las PASO, en el tercer lugar.

La previsión en la coalición opositora, si mantiene el caudal de votos sin fugas para otros espacios políticos, es lograr 15 bancas.

En el cuarto y en el quinto lugar, dos candidatos que responden a Elisa Carrió: Marcela Campagnoli y Juan Manuel López, que en el cierre de la campaña había dado un discurso muy fuerte contra Alberto Fernández y Axel Kicillof. En el sexto puesto vuelve otro radical: Danya Tavella. En el séptimo, un dirigente de Patricia Bullrich, Gerardo Millman, ex funcionario en el Ministerio de Seguridad. En el octavo puesto irá María Sotolano.

Recién el lugar nueve aparece el peronista Emilio Monzó, ex presidente de la Cámara de Diputados durante la gestión de Cambiemos y uno de los hombres fuertes de la campaña de Manes. Del 10 al 15, en la boleta irán Gabriela Besana (PRO), el ex titular de Medio Públicos, Hernán Lombardi, Margarita Stolbizer, el ex ministro de Educación Alejandro Finnochiaro, Victoria Borrego (Coalición Cívica) y Fabio Quetglas (UCR).

Esta integración corresponde al 96,91 % de las mesas escrutadas.

“La clave de la elección estuvo en que ganamos la primera y quedamos a siete puntos en la tercera sección electoral con relación al Frente de Todos”, señaló a Infobae uno de los dirigentes que participa de la campaña de Santilli. “El batacazo es el AMBA. Donde el peronismo unido en el 2019 hizo la diferencia. Ahí es donde Diego generó la mayor diferencia con Facundo. Y es la razón central de porque era el mejor candidato. Puede romper el techo histórico de la provincia en el conurbano”, agregó.

“Nuestra estrategia de cara a la primaria era recorrer y visitar 47 municipios estratégicos. De esos 45 ganamos en 45. Y los dos que perdió el Colo, fueron por un punto. De esos 45 que ganamos, en la mayoría también también estuvo Horacio”, amplió el mismo dirigente.

Ayer por la noche, al momento de los discursos, tanto Santilli como Manes (que aprovechó para mostrar que ganó la interna en la mayoría de los distritos del interior de la provincia) ratificaron que trabajarán juntos en la campaña rumbo a las elecciones generales. “Los que estamos aquí compartimos los mismos valores: el trabajo, la seguridad, la educación y el conocimiento. Nos comprometimos a estar juntos y estamos acá, cumpliendo nuestro compromiso”, señaló el ex vicejefe de gobierno porteño.

En la misma línea, el neurocientífico insistió en que los votantes de Juntos por el Cambio se tienen que “quedar tranquilos” ya que una coalición “tiene que tener diferencias”. Sin embargo, aclaró que “los trazos gruesos de la república” son los mismos para ambos sectores. “Todos juntos le vamos a ganar al kirchnerismo en noviembre y en 2023. Pero no basta con ganar, hay que ganar y transformar la Argentina, por eso hay que ampliar”, planteó.

#### LA BOLETA EN CABA

La situación en la ciudad de Buenos Aires tiene su particularidad. Vidal compitió y se impuso a López Murphy y a la lista impulsada por un sector del radicalismo encabezada por el ex secretario de Salud Adolfo Rubinstein. A último minuto, los partidos que integran la alianza opositora llegaron a un acuerdo para evitar que la ley de Paridad de Género desplace al ex ministro de Economía hacia un lugar no preponderante en la lista definitiva, algo que podría facilitar una fuga de votos hacia otros espacios liberales, como el que representa Javier Milei, que hizo una gran elección y fue el tercer candidato más votado.

En función de ese acuerdo, la boleta final quedaría integrada de la siguiente manera: Vidal, Martín Tetaz, Paula Olivetto Lago (Coalición Cívica), López Murphy, Carla Carrizo (UCR), Fernando Iglesias, Sabrina Ajmechet (cercana a Patricia Bullrich), Fernando Sánchez (Coalición Cívica) y Sandra Pita, de la lista Republicanos.

Vidal, López Murphy y Rubinstein, que se quedó fuera de la lista final, también tuvieron ayer su foto de unidad ayer, en el bunker de Costa Salguero. “Esta noche los votos dijeron ‘falta menos’, ustedes los saben, yo lo sé y el kirchnerismo lo sabe. Falta menos para tener un bloque opositor en el Congreso que defienda la República, para que el cinismo y la mentira se termine, para recuperar la educación”, lanzó la candidata del PRO, quien obtuvo el 33% de los votos contra el 24% de Leandro Santoro. En esa línea, explicó que el “falta menos” significa “poder llegar al Congreso para hacer realidad nuestras 23 propuestas, a las que ahora se sumarán las de Ricardo y Adolfo”.