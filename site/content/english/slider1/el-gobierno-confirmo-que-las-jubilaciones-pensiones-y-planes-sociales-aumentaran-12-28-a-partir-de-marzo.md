+++
author = ""
date = 2022-02-10T03:00:00Z
description = ""
image = "/images/anses-planesjpg-1.jpg"
image_webp = "/images/anses-planesjpg.jpg"
title = "EL GOBIERNO CONFIRMÓ QUE LAS JUBILACIONES, PENSIONES Y PLANES SOCIALES AUMENTARÁN 12,28% A PARTIR DE MARZO"

+++

#### **_El haber mínimo pasará a ser de $32.630. Los ingresos de los jubilados se actualizan en forma trimestral de acuerdo a la evolución de los salarios formales y de la recaudación de la Anses. El aumento impactará también en la Asignación Universal por Hijo (AUH)_**

El Gobierno anunció que las jubilaciones, pensiones y planes sociales, como la Asignación Universal por Hijo (AUH), subirán en marzo un 12,28% por lo que el haber mínimo pasará a ser de $32.630 y el monto mensual de esa ayuda social quedará establecida en $6.375, de acuerdo al primer incremento trimestral previsto para este año.

La fórmula de movilidad está compuesta en un 50% por la evolución de los salarios. El indicador que se toma es el que sea más alto al comparar la Remuneración Promedio de los Trabajadores Estables (Ripte), que mide el Ministerio de Trabajo, y el Indice de Salarios que releva el Indec.

Entre todas las sumas que se abonarán se alcanza a más de 16 millones de beneficiarios: más de 7,1 millones de jubilados y/o pensionados y a casi 9 millones de niños y adolescentes (4,4 millones de AUH y 4,3 millones de Asignaciones Familiares). Asimismo, también impactará en otras asignaciones familiares como la Asignación por Embarazo y prenatal, nacimiento, adopción, matrimonio y asignaciones por cónyuge.

El incremento también abarcará a la franja de jubilación más alta que pasará de $103.064,23 de hace dos años a $219.571,69. Las asignaciones por Hijo y por Embarazo pasarán de $2.746 a $6.375.

Desde el cambio de la Ley de Movilidad, aprobado a comienzos del 2021, los haberes se actualizan en forma trimestral de acuerdo a la evolución de los salarios formales y de la recaudación de la Anses. A lo largo del año pasado las subas fueron de 8,07% en marzo; 12,12% en junio, 12,39% en septiembre y 12,11% para completar los cuatro incrementos del 2021. Durante el año pasado, el acumulado de los cuatro aumentos establecidos alcanzó el 52,7%. Desde el Gobierno señalaron que el incremento hubiera sido de 48,6% si se hubiese mantenido la fórmula anterior, que estuvo vigente durante el Gobierno de Mauricio Macri.

Además, en 2021 se otorgaron bonificaciones para los jubilados que cobran los haberes más bajos. Los bonos fueron pagados a lo largo del año pasado en los meses de abril y mayo, por $1.500 cada uno; en agosto, por $5.000; y en diciembre, por $8.000.

El Gobierno argumentó que los aumentos dados por la nueva fórmula fueron superiores a los que habrían resultado con el esquema de actualización anterior que se puso en marcha durante el mandato de Mauricio Macri. Sin contar los bonos extraordinarios, la jubilación mínima terminó el 2021 con $4.359 más que si hubiera estado vigente la fórmula anterior.

De este modo, el incremento que se otorgará es superior en 2,4 puntos porcentuales al que habría correspondido si se hubiese utilizado para el cálculo la fórmula implementada por la gestión anterior. Desde que se aplica la nueva fórmula, hace 15 meses, los haberes jubilatorios y las asignaciones recibieron incrementos que, acumulados, se encuentran 8,1 puntos porcentuales por encima de los que hubiese otorgado la fórmula anterior”, afirmó el Poder Ejecutivo en un comunicado.

“Esta recomposición progresiva de los haberes se suma a dos políticas de inclusión previsional muy importantes que implementamos desde el año pasado, como fueron el Reconocimiento de Aportes por Tareas de Cuidado y la Jubilación Anticipada. Seguimos recuperando los ingresos de millones de argentinas y argentinos después de un 2021 en el que la fórmula de movilidad que sancionamos también le ganó a la inflación”, mencionó el presidente Alberto Fernández en su cuenta de Twitter al anunciar el aumento de marzo.