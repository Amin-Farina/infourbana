+++
author = ""
date = 2021-07-14T03:00:00Z
description = ""
image = "/images/axel-kicillof-2-1200x736.jpg"
image_webp = "/images/axel-kicillof-2-1200x736.jpg"
title = "AXEL KICILLOF AVANZÓ CON NOMBRAMIENTOS EN LA JUSTICIA"

+++

**_El gobernador bonaerense, Axel Kicillof, avanzó en la firma de 31 decretos para nombrar a trece Agentes Fiscales, ocho jueces/zas de Tribunal Oral en lo Criminal, cinco Defensores/as, una Asesora de Incapaces, una Defensora Civil Comercial y Familia, un juez de Cámara Civil y Comercial, un juez de Familia y un juez de Garantías._** 

Las designaciones venían suspendidas porque formaban parte de un ida y vuelta con la oposición, ya que los nombres que por estas horas habilitó el mandatario integraban paquete de designaciones (cuarenta y tres) que había girado al Senado la ex gobernadora María Eugenia Vidal meses antes de terminar su mandato y que la Cámara alta bonaerense aprobó solo con los votos de Juntos por el Cambio y hasta la fecha estaban en un limbo a la espera de una definición.

Los nombramientos se publicarán en las próximas horas en el Boletín Oficial. Serán las primeras designaciones firmes que Kicillof realiza en la órbita judicial desde que ocupa el cargo de gobernador. Semanas atrás, el mandatario mantuvo un acercamiento con la Suprema Corte de Justicia al inaugurar el nuevo edificio del Tribunal de Casación Penal. Ese evento, el 2 de julio, contó con la presencia del cuerpo, Luis Genoud y la vicepresidenta del máximo tribunal bonaerense, Hilda Kogan .

Hay dos personas que ofician de enlace entre el mandatario y el Poder Judicial: el ministro de Justicia, Julio Alak, y el Secretario General de Gobierno, Federico Thea. En la revisión de los nombramientos que selló el mandatario hubo un trabajo en conjunto con dichos funcionarios.

Finalmente, fueron 31 los decretos que firmó Kicillof de un total de 42 designaciones que el Senado había aprobado solo con los votos de Juntos por el Cambio. Fue tras omitir el pedido del Gobernador de retirar esos pliegos (enviados por Vidal) para su estudio previo.

Como relató Infobae, el 7 de septiembre del año pasado Kicillof solicitó a través del decreto 773/20 el retiro de 42 pliegos que estaban en la Cámara alta provincial y habían sido girados por la ex gobernadora en septiembre del 2019, una vez que ya había perdido las elecciones primarias por una holgada diferencia.

Pero los pliegos no se retiraron y se votaron. Con mayoría en el Senado, Juntos por el Cambio terminó aprobando, el 11 de septiembre del 2020, 41 de esos 42 pliegos enviados por Vidal a fines del 2019.

El proceso de selección de postulantes corre por cuenta del Consejo de la Magistratura bonaerense que termina definiendo las ternas. Luego es el Poder Ejecutivo quien elige a una de las tres personas, arma el pliego y lo envía al Senado.

Una vez que el Senado aprueba el pliego, el expediente regresa al Ejecutivo, que los revisa, decide si corresponde el nombramiento y luego la Suprema Corte toma juramento. Recién ahí entra en funciones la persona designada. Once designaciones, que tenían la aprobación del Senado, no pasaron el filtro de Kicillof.

Uno de los más resonantes es el de Francisco Pont Vergés, actual Secretario de Política Criminal, Coordinación Fiscal e Instrucción Penal de la Procuración General y aspirante a uno de los cargos del Tribunal de Casación Penal. Como mano derecha del Procurador Julio Conte Grand, el 25 de junio de 2019 Pont Vergés denunció, dentro del Ministerio Público y también en el ámbito judicial, al entonces Defensor de Casación Mario Coriolano por haber ingresado con una periodista a una unidad del sistema penitenciario y grabar una entrevista con una persona trans detenida, que denunció haber sufrido torturas. La causa contra Coriolano fue archivada. Sin embargo, al poco tiempo que la entonces gobernadora Vidal envío su pliego al Senado, el Comité Nacional para la Prevención de la Tortura, el Centro de Estudios Legales y Sociales, y la Comisión Provincial por la Memoria, enviaron distintas notas rechazando su nombramiento como juez del Tribunal de Casación.

Los cargos que dejarán de estar vacantes corresponden a departamentos judiciales de Avellaneda-Lanús, La Matanza, San Isidro, Bahía Blanca, Lomas de Zamora, La Plata, Quilmes, Moreno- General Rodríguez, Zárate-Campana, Azul y Mercedes

Además de las firmas que estampó el mandatario también tuvo señales positivas en la Cámara alta bonaerense. Sucede que la Comisión de Asuntos Constitucionales y Acuerdos del Senado emitió dictamen favorable para aprobar otros 31 pliegos enviados por el gobernador el pasado 2 de junio.