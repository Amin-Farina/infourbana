+++
author = ""
date = 2021-05-18T03:00:00Z
description = ""
image = "/images/labpax.jpg"
image_webp = "/images/labpax.jpg"
title = "LOS DETALLES MÁS OSCUROS DEL CONTRATO DEL LABORATORIO QUE HACÍA LOS HISOPADOS EN EZEIZA Y QUE TODAVÍA OPERA EN AEROPARQUE"

+++
**_Pese a las promesas de AA2000 de rescindir el contrato, el cuestionado laboratorio LabPax, que también operaba en Ezeiza, continúa como encargado de realizar los testeos exprés en el Aeroparque Jorge Newbery, a cambio de $2500 cada uno. La firma, que se constituyó en noviembre de 2020, está liderada por dos monotributistas de las categorías más bajas que, según denuncias de la diputada nacional Mariana Zuvic, serían testaferros de dirigentes cercanos a La Cámpora y habrían dado la dirección de una plaza pública como su sede de negocios._**

La firma continúa facturando con el visto bueno de la empresa concesionaria Aeropuertos Argentina 2000 y el aval de los funcionarios nacionales de los ministerios de Transporte y de Salud de la Nación, en esa terminal aérea porteña, en medio de la pandemia de coronavirus y con la cepa andina, originaria de Chile y Perú, que ya dejó en la Argentina miles de infectados y centenares de muertos a medida que se propagó el virus durante los últimos días.

Al menos así se desprende de lo informado hasta el 16 de mayo cuando aterrizó un vuelo procedente desde Lima, Perú, con pasajeros que debieron hisoparse antes de pasar por la oficina de migraciones para realizar el ingreso al país.

Si bien desde el Organismo Regulador del Sistema Nacional de Aeropuertos (ORSNA), que dirige el santiagueño Carlos Pedro Mario Aníbal Lugones Aignasse, vienen desmarcándose de las responsabilidades por las irregularidades detectadas y denunciadas ante la Justicia por el accionar del laboratorio LabPax, fue este organismo que regula la actividad aérea el que autorizó los vuelos regionales desde y hacia la estación aeroportuaria ubicada en la Ciudad Autónoma de Buenos Aires.

Según informaron periodistas de TN Sergio Farella y Rodrigo Alegre, LabPax es una sociedad conformada por dos monotributistas sin ningún tipo de antecedentes en análisis clínicos. Una de sus dueñas, Paola Perillo Orellana, inscripta en la categoría A del régimen de la AFIP, declaró facturar no más de 18.000 pesos mensuales. En tanto, su socia Laura Cáceres está un escalón más arriba y factura por hasta 34.700 pesos por mes.

Por la investigación judicial que lleva adelante el juez federal Luis Armella por el escándalo de los hisopados truchos, AA 2000 ya sufrió el allanamiento de sus oficinas en busca de pruebas que echen luz acerca del entramado que permita descubrir quiénes están detrás de las dos monotributistas que facturaron hasta el 15 de mayo cerca de cinco millones de pesos diarios.

Por eso, es difícil comprender las razones por las que en el aeroparque metropolitano continúa operando LabPax y no fue reemplazado este laboratorio, flojo de papeles, como sucedió en el aeropuerto de Ezeiza luego de que el escándalo tomó dimensión pública y la preocupación por la filtración de turistas con cepas nuevas de coronavirus se convirtió en una hipótesis plausible.

Ahora, la Justicia busca desentrañar la trama contractual por la que LabPax obtuvo una lucrativa tercerización de servicios de parte del Estado Nacional. Para que esa firma y Sanity Care obtuvieran el negocio debieron recurrir a una fundación, San Lázaro, cuya sede se ubica en Virrey del Pino 3511 en el barrio porteño de Belgrano donde, además, funciona el colegio Mekhitarista de Buenos Aires.

El titular de la fundación, organización sin fines de lucro, Carlos Potikian, fue el que estampó la firma con una de las monotributistas, Laura Nadia Viviana Cáceres. Sin embargo, para que se pudiera concretar el sueño del laboratorio propio con clientela cautiva se debía conseguir el permiso de la empresa concesionaria de las terminales aeroportuarias, Aeropuertos Argentina 2000.

Para eso, se utilizó el antecedente de un convenio firmado en enero pasado por el cual AA 2000 le entregó un espacio a la Fundación San Lázaro para que montara un área que sirviera para realizar los hisopados de pasajeros que llegaban a Buenos Aires. De lo recaudado en los test se derivaba un 15% al hospital de la localidad de Ezeiza.

Este convenio, que lleva las firmas de Patricia Eurnekian y de Carlos Potikian, devino en la contratación de LabPax con el compromiso de entregar el 29,49% del total de la facturación a la fundación educativa que a su vez derivaría el 15% de ese monto al hospital de Ezeiza.

El magistrado que actúa en la causa debió, primero y de manera urgente, intervenir las operaciones de testeo en la terminal aeroportuaria con auditores de la Universidad Nacional de Avellaneda para asegurar que no exista ningún tipo de filtraciones de pasajeros infectados con el virus hasta que Stamboulian Servicios de Salud se hizo cargo de las tareas el pasado 15 de mayo.

En una segunda etapa avanzará con la investigación de lo actuado en los allanamientos producidos en los primeros días del mes de mayo en donde ya se encontró con sorpresas. Cuando las autoridades fueron a allanar LabPax, se encontraron con chicos jugando en una soleada mañana otoñal: la dirección que figuraba en el expediente de la causa era la de una plaza pública.

Según indicó la agencia NA, finalmente se dio con la sede de la firma donde se recabaron documentos que permitirían desagregar el entramado contractual y económico del escándalo de los hisopados.