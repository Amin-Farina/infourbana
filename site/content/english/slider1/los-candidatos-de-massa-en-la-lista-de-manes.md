+++
author = ""
date = 2021-09-06T03:00:00Z
description = ""
draft = true
image = "/images/frega.png"
image_webp = "/images/frega.png"
title = "LOS CANDIDATOS DE MASSA EN LA LISTA DE MANES"

+++
###### FUENTE: [LETRAP](https://www.letrap.com.ar/nota/2021-9-4-11-29-0-vidalismo-residual-exmassistas-y-mas-el-adn-multitarget-de-la-lista-de-manes)

### _Dirigentes vinculados a Sergio Massa y Julio Zamora conviven en la misma boleta con Facundo Manes. En Tigre se da el caso de quien fue Concejera Escolar por el Massismo y hoy aspira a ser Diputada Nacional._

Más allá de la visibilidad de quienes ocupan los primeros lugares, hay varias historias para contar detrás de los 35 nombres que pueblan la lista de aspirantes a la Cámara de Diputados liderada por Facundo Manes.

El cometido de asociar esta propuesta al conocimiento y la investigación fue una de las razones por las que en el décimo casillero figura María José Navajas. Tucumana asentada en Vicente López, es investigadora en Historia del Conicet, con maestría en México. Abocada al estudio de campañas electorales, también abordó proyectos como  “La política y las emociones. El miedo en la historia política de Argentina y México”. Precisamente el miedo es una de las emociones en las que enfoca Manes. Su postulación fue celebrada por Sandra Pitta, otra investigadora del Conicet que juega esta elección en Juntos, en su caso, bajo la propuesta porteña de Ricardo López Murphy.

Con Pitta y otras "ciudadanas y ciudadanos independientes", en 2019 firmaron el documento "Por qué votamos Juntos por el Cambio", en el que apoyaron los cuatro años de macrismo, periodo del que, en estos tiempos de campaña, Manes se desliga y se lo endosa a su oponente, Diego Santilli. Más acá en el tiempo, Navajas, también miembro de la asociación “Padres Organizados”, promovió marchas en reclamo por el retorno a clases presenciales.

Siete casilleros más abajo, en la ubicación 17, aparece Marcelo Di Mario. Radical de Malvinas Argentinas, fue director de Consejos Escolares durante la gestión Vidal y lejos estuvo de pasar desapercibido. Como máximo responsable provincial, Di Mario fue el encargado de anunciar, en octubre de 2017, la intervención de la Provincia al Consejo Escolar de Moreno, distrito donde el 2 de agosto de 2018 una explosión en la escuela 49 se llevó la vida de la vicedirectora, Sandra Calamano, y el auxiliar Rubén Rodríguez, tragedia que puso en el ojo de la tormenta a la intervención vidalista con Di Mario a la cabeza.

En la ubicación 15 de la lista liderada por Manes aparece Martín Salaverry, abogado bahiense que en 2011 fue candidato radical a la intendencia y que en 2013 accedió al Concejo Deliberante local bajo el ala de Unidos por la Libertad y el Trabajo, de Francisco de Narváez. Tras esa experiencia y el retiro del Colorado, Salaverry se alineó a Sergio Massa y en 2017 fue postulante de 1País. Ahora, volvió al redil radical.

Otra figura que supo articular con el massismo es Adriana Frega, inspectora en Tigre que fue candidata a consejera escolar de Julio Zamora en 2015. Más tarde, entre 2017 y 2019, fue directora de Inspección General de Educación la Provincia. Hoy, se muestra en el distrito con el monzoista y aliado de Sergio Massa, el "colorado" Nicolás Massot.

En el sexto lugar, se encuentra Elsa Llenderrozas, que, más allá de ser directora de la carrera de Ciencia Política de la UBA, en el partido centenario varias voces consignan con malestar que no posee una militancia partidaria activa, como sí la tiene su esposo, Horacio Barreiro, subsecretario de Trabajo bonaerense en tiempos de Vidal y de vínculo directo con Salvador.

Inmediatamente después de Cariglino, en los casilleros ocho y nueve, aparecen dos figuras también cercanas al exvice de Vidal, como la diputada nacional oriunda de La Matanza Josefina Mendoza y Pablo Juliano, expresidente de la Juventud Radical bonaerense en la primera parte de la gestión Cambiemos  y exmiembro del directorio del IOMA.

Además, el trazo de la lapicera de Salvador se advierte en la precandidatura del actual secretario general del partido, Mariano Mugnolo, quien, como hombre de confianza del exvicegobernador, se desempeñó como secretario legislativo del Senado provincial en tiempos de Cambiemos, además de haber sido previamente titular de la Convención bonaerense, desde donde confrontó fuerte con el alfonsinismo ricardista.

Exintegrantes de la cúpula de la JR salvadorista aparecen también en la lista de Manes, como Martín Borrazas y Matías Garabani. Como ejemplos más directos, se avizoran las precandidaturas de Gloria Gutiérrez (lugar 22) y Gerardo Buj (en el 33), ambos vocales del comité de la UCR San Fernando, distrito base de la familia Salvador.

El radicalismo platense, en tanto, anota varios nombres: Marcelo Ponti, Lorena Prieto, Valería Venturín y Maitén Agüero. De Evolución, además de la vicerrectora de la UNNOBA, Danya Tavela, que secunda al neurólogo, en la lista aparecen otras dos figuras del sector referenciado en Martín Lousteau: Augusto Spinosa (Lobos) y Cristina Coria (concejala de Mar del Plata).

Del Partido del Diálogo, además de su principal referente, Emilio Monzó, se anotan el exsenador provincial oriundo de Pergamino Marcelo Pacífico y Carla Cazzaniga. En el caso del GEN, más allá de Margarita Stolbizer, aparece la pinamarense María Cristina Malegni en el décimo segundo lugar. Solo un coterráneo de Manes figura en la lista y en el último casillero de titulares, el consejero escolar de Salto Leopoldo Muza.

Link: [https://www.letrap.com.ar/nota/2021-9-4-11-29-0-vidalismo-residual-exmassistas-y-mas-el-adn-multitarget-de-la-lista-de-manes](https://www.letrap.com.ar/nota/2021-9-4-11-29-0-vidalismo-residual-exmassistas-y-mas-el-adn-multitarget-de-la-lista-de-manes "https://www.letrap.com.ar/nota/2021-9-4-11-29-0-vidalismo-residual-exmassistas-y-mas-el-adn-multitarget-de-la-lista-de-manes")