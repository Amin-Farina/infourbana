+++
author = ""
date = 2021-05-11T03:00:00Z
description = ""
image = "/images/zwkhu5nlinkjjncgk5nhkmhjiu.jpg"
image_webp = "/images/zwkhu5nlinkjjncgk5nhkmhjiu.jpg"
title = "LA CREATIVA RESPUESTA DE UN JOCKEY CUYO CABALLO DIO POSITIVO EN EL ANTIDOPPING"

+++

Después de llevarse el Derby de Kentucky, la primera carrera de la llamada Triple Corona, la organización anunció el positivo del caballo Medina Spirit. Dio positivo en un control antidopaje al encontrarse restos de betametasona (21 picogramos), una cantidad aproximada al doble del umbral legal en este tipo de competiciones.

Aunque el entrenador Bob Baffert negó cualquier tipo de irregularidad, apeló la decisión solicitando que se realizara un nuevo análisis a parte de la muestra original y prometió total transparencia con los oficiales de la carrera, Churchill Downs le prohibió a Baffert inscribir caballos en la pista. De mantenerse la decisión, Medina Spirit podría ser descalificado y el subcampeón Mandaloun sería declarado como ganador.

“Es una injusticia para el caballo. No me siento avergonzado, siento que me robaron”, soltó Baffert, en diálogo con Dan Patrick Show, donde dio una extravagante explicación sobre lo acontecido: esbozó que un mozo de cuadras pudo haber tomado un medicamento para la tos y luego orinar en el heno que comió el caballo. Y luego, añadió: “Estos caballos no viven en una burbuja. Están en una granja abierta. La gente los toca. Pasó antes del Derby y después del Derby. Hay muchas formas en que estos caballos pueden contaminarse y cuando se están probando a niveles realmente ridículamente bajos. Lo he estado diciendo durante más de un año, esto va a meter en problemas a personas inocentes y esto es lo que sucedió ahora”.

Pese a que cinco de sus caballos no han pasado la prueba de drogas en poco más de un año, Bob Baffert, en una entrevista con Fox News, recalcó que “No sucedió. Ese caballo nunca ha sido tratado con el esteroide. En realidad, es una medicina terapéutica legal y la cantidad que contenía no tendría ningún efecto en el caballo de todos modos. Pero, nosotros no… Ese caballo nunca fue tratado con eso y esa es la parte inquietante”.

“Nunca pensé que tendría que luchar por mi reputación y la reputación del pobre caballo. Debido a las nuevas regulaciones que han impuesto los regulares, están probando estos caballos a niveles contaminados y ha sido una experiencia horrible”, sostuvo el entrenador, quien ahora quedó en el centro de la escena tras obtener su séptima victoria en el Derby.

Según informó AP, oficiales del Maryland Jockey Club y Pimlico indicaron que tomarán una decisión sobre el estatus de Medina Spirit para la segunda joya de la Triple Corona después de que revisen todos los hechos. Todo esto se desarrollará mientras Baffert está en California y no en la carrera en la que buscará un récord de ocho triunfos.

Vale destacar que al caballo Medina Spirit se le detectaron 21 picogramos de betametasona, un esteroide que en ocasiones se utiliza para el tratamiento de dolor e inflamación en los caballos. Esta droga es la misma que se le encontró a Gamine (también entrenada por Baffert), una potranca que terminó tercera en el Kentucky Oaks de 2020 pero que luego fue descalificada. Por este caso el entrenador fue multado con 1.500 dólares.