+++
author = ""
date = 2021-10-05T03:00:00Z
description = ""
image = "/images/xk7esnbkfjgjvnxxidzc5bt2sq.jpeg"
image_webp = "/images/xk7esnbkfjgjvnxxidzc5bt2sq.jpeg"
title = "PRESENTÓ SU RENUNCIA LA JUEZA DE LA CORTE SUPREMA, ELENA HIGHTON DE NOLASCO"

+++

#### **_La jueza Elena Highton de Nolasco presentó su renuncia a la Corte Suprema en una carta enviada al presidente Alberto Fernández. La renuncia será efectiva a partir del 1 de noviembre próximo, de acuerdo con el texto que llegó a la Casa Rosada._**

"Tengo el agrado de dirigirme al Señor Presidente con el objeto de presentar mi renuncia al cargo de la Corte Suprema de Justicia de la Nación", sostiene la misiva, que agrega que la dimisión es "con efectos al 1 de noviembre del corriente año".

Antes de enviar la carga, Highton se comunicó telefónicamente con Alberto Fernández para adelantarle la decisión, según indicaron fuentes oficiales a la agencia Télam.  El mandatario manifestó en reiteradas ocasiones su preocupación por el funcionamiento de la Corte Suprema.

##### EL DETONANTE DE LA SALIDA

La carta tiene fecha del 30 de septiembre, con lo que la dimisión llegó días después de la elección de autoridades en el máximo tribunal, ocasión en la que Highton de Nolasco evitó expresarse a favor de la elección de Horacio Rosatti como nuevo presidente de la Corte.

La jueza, aliada de Ricardo Lorenzetti, no participó de la elección del recientemente designado Rosatti al frente del máximo tribunal, quien ingresó bajo el mandato de Mauricio Macri. Junto con el presidente de la Corte Suprema, cuestionó la elección en la que sólo participaron tres de los cinco miembros.

##### DESIGNACIÓN DE JUECES

De acuerdo con sus allegados, la magistrada siente que "cumplió un ciclo". Con su salida se abre una vacante para que el oficialismo pueda designar un juez en el supremo tribunal. Sin embargo, para ratificarlo necesita que sea en acuerdo con el Congreso, para lo que necesita el apoyo de dos tercios del Senado Nacional.

La jueza había llegado a la Corte en 2004 propuesta por el entonces presidente Néstor Kirchner, luego de la destitución de Eduardo Moliné O'Connor.

El flamante ministro de Seguridad, Aníbal Fernández, planteó la semana pasada que "debería ampliarse la Corte Suprema", aunque aseguró que lo hacía a título personal. También criticó la designación de Rosatti. "Miro con preocupación la crisis de la Corte; no goza de poder político porque los otros poderes no la respetan", afirmó en declaraciones a AM750.

##### ¿QUIÉN ES ELENA HIGHTON DE NOLASCO?

La jueza Highton de Nolasco tiene 78 años y es la primera mujer que accedió al máximo tribunal bajo la democracia. Se desempeña en ese cargo desde 2004. 

La abogada, nacida en Lomas de Zamora, cobró notoriedad también cuando resistió la renuncia al llegar al límite de edad de 75 años planteado para los jueces supremos y se mantuvo en el cargo.

Los jueces de la Corte suman casi $ 100 millones de patrimonio

Highton de Nolasco presentó un amparo en 2017 para pedir que "se declare la nulidad de la reforma del 1994 en relación al artículo 99, inciso 4º, párrafo tercero, de la Constitución Nacional", referido al límite de edad de los miembros de la Corte.

La jueza había aseverado en su presentación judicial que en 1994 "no existía autorización alguna para modificar el régimen de estabilidad vitalicia de los magistrados, ni fijar un límite de 75 años como finalmente lo hizo la Convención Constituyente.