+++
author = ""
date = 2021-06-17T03:00:00Z
description = ""
image = "/images/5fe78927c16a6_800x450-1.jpg"
image_webp = "/images/5fe78927c16a6_800x450.jpg"
title = "UN NUEVO ESPACIO POLÍTICO DENTRO DEL FRENTE DE TODOS"

+++

**_Distintas agrupaciones políticas y sociales lanzarán el "Frente dentro del Frente de Todos", con el objetivo de "consolidar la unidad" del espacio oficialista y "proponer la profundización del programa de transformación del Gobierno que presiden Alberto Fernández y Cristina Fernández de Kirchner"._**

Con la presentación, que se realizará hoy a partir de las 19:00 vía Zoom, el espacio que reúne a Compromiso Federal, Proyecto Sur y el Partido Intransigente, entre otras fuerzas políticas y organizaciones sociales, busca dar "el primer paso" para "conformar un frente patriótico, popular y democrático", indica la convocatoria.

Bajo el objetivo de "consolidar la unidad del Frente de Todos" y "la profundización del programa de transformación del gobierno que presiden Alberto Fernández y Cristina Fernández de Kirchner", el espacio plantea la "necesaria institucionalización" de la coalición gobernante.

"Es necesaria la institucionalización del Frente de Todos en todos los niveles que permita el debate más amplio en unidad y diversidad y a la vez estar organizados para enfrentar las embestidas de la derecha y ultraderecha", sostiene el documento.

La nueva agrupación que integran además el Movimiento La Dignidad-Confluencia, personalidades de la cultura, referentes de derechos humanos y dirigentes sindicales, expresó además la necesidad de "organizar las PASO y garantizar la participación democrática de todos los partidos del Frente de Todos", de cara a las elecciones legislativas.

"Unas PASO en las que no solo se discutan candidaturas, sino que sea la oportunidad para un gran debate de propuestas de cada vertiente del Frente con la participación de la ciudadanía", señala el comunicado.

De acuerdo con estas organizaciones, la solución a los "problemas profundos" del país "necesitan de la creatividad, el debate y la participación de todo el movimiento nacional y popular, de sus organizaciones sociales y políticas".

"Hay que reconstruir el país. Se necesita un cauce político adecuado de transformación para lograr convertir en realidad el Nunca Más a las políticas de destrucción y lograr la felicidad de nuestro pueblo", sostiene el "Frente dentro del Frente de Todos".

Firman el documento los dirigentes Gabriel Mariotto, Jorge Selser, Mariano Pinedo, Luis Torres, Hugo Oroná, Juan Ferreyra, Rafael Klejzer, Patricia Perelló, Néstor Morelli, Fabiola Arnelli, Laura Bitto, Alejandro Mosquera, María Inés Basile, José "Pepe" Gazpio, Florencia Prego, Nora Saltalamacchia, María A Camiña, Marina Joski, Fátima Nader, Alejandro Marmoni y Lucila González, entre otros.