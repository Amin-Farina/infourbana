+++
author = ""
date = 2021-12-23T03:00:00Z
description = ""
image = "/images/sk5yn547d5etbk5ofxb3i37eqa-1.jpg"
image_webp = "/images/sk5yn547d5etbk5ofxb3i37eqa-1.jpg"
title = "ANALISTAS ADVIERTEN QUE LAS RESERVAS LÍQUIDAS DEL BCRA NO LLEGAN A USD 2.600 MILLONES Y PREVÉN UN FRENO DE LA ECONOMÍA"

+++

#### **_Sucede luego de que el país afrontara el pago de unos USD 1.892 millones al FMI y las reservas brutas del Banco Central quedaron debajo de los USD 40.000 millones. Los especialistas esperan que se acelere la devaluación del tipo de cambio o un endurecimiento del cepo._**

Las reservas brutas del Banco Central cayeron el miércoles USD 1.956 millones en relación al cierre del día anterior y quedaron en USD 39.153 millones, el valor más bajo en nueve meses. Esto sucedió luego de que el Gobierno afrontara el último gran vencimiento de deuda del año, por unos USD 1.892 millones al FMI, mientras todavía se desarrollan las negociaciones con el organismo para establecer un nuevo acuerdo. 

Grupos de economistas afirmaron que las reservas líquidas de la entidad monetaria quedaron por debajo de los USD 2.600 millones -lo que equivale “al pago de 15 días de importaciones”-, al tiempo que advirtieron que el Gobierno puede acelerar la devaluación del tipo de cambio o implementar un endurecimiento del cepo a través, por ejemplo, de mayores restricciones a las importaciones.

En ese sentido, indicaron que ante la escasez de divisas por parte del BCRA es posible que la economía se frene de cara al 2022.

Asimismo, consideraron que el escaso stock de reservas internacionales implica que el Banco Central tenga menos margen para intervenir en el mercado de cambios hasta que ingresen divisas por el grueso de la liquidación del agro, que no tendría lugar hasta marzo.

Respecto a la posibilidad de subir la tasa de interés, alertaron que si bien es necesario corregirla al alza para reducir la inflación porque la tasa real es casi un 9% negativa, por otro lado el Central tiene un stock de pasivos remunerados, es decir Leliqs y pases, de más de 4 billones de pesos sobre los que paga intereses. Por ello, una suba de la tasa terminaría incrementando esa deuda y, por lo tanto, la inflación futura.

Juan Ignacio Paolicchi, de Empiria, dijo que de acuerdo a los cálculos de la consultora, el Banco Central tiene USD 2.600 millones de reservas netas, lo que equivale a 15 días de los dólares que consumen las importaciones.

“Esto pone dudas sobre la sostenibilidad del tipo de cambio actual. Todos se apuran a pagar importaciones, el resto demora las exportaciones y el resultado es que el BCRA no puede comprar dólares pese a que el agro está liquidando divisas. El riesgo es que si el Central no hace un giro en su estrategia cambiaria, en algún momento el tipo de cambio se tenga que corregir por las malas. Esto genera más inflación y más pobreza”, remarcó.

En ese sentido, Lorenzo Sigaut Gravina, de Equilibra dijo: “En nuestro cálculo de reservas netas excluimos los DEG, por lo que para nosotros el pago de deuda al FMI con DEG no afecta reservas netas, pero si afecta las brutas que caen en un monto similar al pago al Fondo, es decir casi USD 1.900 millones. De todas formas, según nuestros cálculos el stock de reservas netas está en torno a 4.000 millones de dólares, lo que no alcanza siquiera a cubrir un mes de importaciones. Además, si a las reservas netas no se contabiliza el oro, para tratar de dar cuenta de la inmediata liquidez, son prácticamente nulas”, analizó.

Asimismo, afirmó que el verano en materia cambiaria viene levantando temperatura dado que hay pocas reservas netas, la demanda estacional de pesos de fin de año se revierte en enero y sobre todo en febrero, la liquidación de agro-divisas es estacionalmente baja hasta el otoño y en el primer trimestre de 2022 hay nuevos vencimientos con el FMI, el Club de Paris y pagos en moneda de deuda reestructurada.

“El Gobierno puede acelerar la devaluación pero si lo hace tiene que subir las tasas lo cual tiene costos. O puede seguir poniendo restricciones a las importaciones para administrar la escasez de divisas”, consideró. Y añadió: “Lo más probable es que la economía se frene por esta escasez de divisas”.

A su turno, Yanel Llohis, de la consultora Orlando Ferreres, dijo que el nivel de las reservas líquidas -descontando DEGs y swap chino- “es crítico”.

“En noviembre finalizaron apenas por encima de los USD 5.000 millones, mientras que en lo que va de diciembre descendieron a USD 2.728 millones”, aseguró de acuerdo a los cálculos de la consultora.

En ese sentido, dijo que por un lado, el escaso stock de reservas internacionales implica que el Banco Central tendrá menos margen para intervenir en el mercado de cambios, “lo cual quedó evidenciado luego de la liberación del dólar contado con liquidación tras las elecciones de noviembre”.

“Además, el nivel de brecha cambiaria actual no contribuye. La necesidad de corregir este desequilibrio explica en parte la aceleración de la depreciación en las últimas semanas. De cara a un acuerdo con el FMI, cabe esperar que la tasa de depreciación del dólar oficial continúa acelerándose de a poco”, afirmó en línea con sus colegas.

Hay que subrayar que el proyecto de ley de Presupuesto 2022, elaborado en septiembre y rechazado en diciembre en su tratamiento parlamentario, no incluye pagos al FMI; es decir, que descuenta un acuerdo para refinanciar dicha deuda por USD 45.000 millones antes de fin de este año.

El actual cronograma de compromisos en moneda extranjera muestra que en enero habrá otra posta exigente, unos USD 1.400 millones entre distintos acreedores, que podría afrontarse aún sin un entendimiento firme con el organismo.

Aldo Abram, director de la Fundación Libertad y Progreso dijo a este medio que los niveles de reservas actuales marcan que “hay muy poco espacio para mantener el atraso cambiario que hay” y aclaró que si bien se aceleró un poco, “no es suficiente”.

“Es insostenible mantener al Banco Central perdiendo reservas cuando debería estar comprando un montón para hacer frente a pagos de deuda. Va a tener que acelerar sí o sí, aunque no les guste, el ritmo de suba del tipo de cambio mayorista. No un salto, pero sí una suba que lleve en el semestre que viene a una suba del 30%”, opinó.

En ese marco, Sebastián Menescaldi, de Eco Go dijo que según los propios cálculos de la consultora, las reservas quedaron en torno a 2.400 millones de dólares.

“Eso implica volver a foja cero. Esto hace que la situación sea débil y ante cualquier cambio como un shock externo, una sequía o menores ingresos por alguna cuestión de las exportaciones, te lleven a que las reservas netas estén en valores cercanos a cero y eso es complicado para poder absorber shocks externos”, destacó.

En tanto, Amilcar Collante, economista Jefe del Centro de Estudios Económicos del Sur (CeSur) dijo que el punto clave es mirar las reservas netas, que según sus estimaciones están en 2.250 millones de dólares. “Es medio mes de importaciones. Es muy poco el poder de fuego del BCRA”, afirmó en el mismo sentido que el resto de los economistas consultados.

Y concluyó: “Esto no es una buena señal. El Banco Central está lleno de pesos y leliqs y del lado de los dólares está muy desequilibrado porque hay muy pocos. Si no hay movimiento de tasas de interés y no se cierra un acuerdo con el Fondo, es difícil anclar expectativas”.