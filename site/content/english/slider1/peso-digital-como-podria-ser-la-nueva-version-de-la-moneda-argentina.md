+++
author = ""
date = 2021-08-17T03:00:00Z
description = ""
image = "/images/moneda-digital-argentina-1.jpg"
image_webp = "/images/moneda-digital-argentina.jpg"
title = "¿PESO DIGITAL? ¿CÓMO PODRÍA SER LA NUEVA VERSIÓN DE LA MONEDA ARGENTINA?"

+++

**_¿Se puede dejar de crear dinero sin encender “la maquinita”? La respuesta corta es sí. El debate por el dinero digital se está dando en todo el mundo y la Argentina no es la excepción._**

El presidente de la autoridad monetaria, Miguel Pesce, dijo recientemente que los bancos centrales “tienen” que emitir este tipo de activos y, aunque descartó planes inmediatos, sugirió que en la Argentina se podría hacer en conjunto con el sector privado.

Las declaraciones de Pesce sobre finanzas digitales despertaron todo tipo de comentarios y preguntas. Según pudo saber TN.com.ar, más allá de la intención del funcionario, por el momento no hay proyectos concretos en danza ni mayores precisiones sobre si podría o no existir un “peso digital” y, en caso de que sí, cómo sería. Una aclaración: parece una idea descabellada, pero en China ya se está probando un yuan digital que reemplazará parte de los billetes y monedas.

Primero, una definición básica. ¿Qué es una moneda digital? Es una que no tiene correlato físico. ¿Y en qué se diferencia de un peso que está en una cuenta bancaria o en un plazo fijo? En que ese peso puede o no tener su correlato físico (o sea, que $100 sean literalmente un billete con la imagen de una taruca), pero de ser necesario -porque quiere retirarlo de la cuenta a través del cajero-, lo tendrá.

Podría ser, también, un híbrido entre el mundo criptográfico (porque la moneda digital se nutre de la misma tecnología que Bitcoin, por caso) y el de las finanzas tradicionales, ya que, a diferencia de Bitcoin, que es una moneda descentralizada, aquí hay un banco central que la emite y lo declara dinero por decreto, es decir, lo convierte en lo que se conoce como “dinero fiat”.

“Las monedas digitales son simples representaciones de otro activo monetario pero en formato digital. Un peso en un banco o un peso en una plataforma de pagos, aunque instrumental y técnicamente son diferentes, en esencia son pesos digitales: tienen un respaldo legal de un activo subyacente y, además, un banco se compromete a que puedan ser cambiados por pesos físicos”, explica Rodolfo Andragnes, de la ONG Bitcoin Argentina.

En cambio, este “peso digital” respaldado en criptomonedas -sin correlato físico posible- es un concepto híbrido, dice, ya que según la ley que lo ampare podría o no representar activos subyacentes depositados en alguna cuenta bancaria o, si los emite el Estado, podría considerarlos válidos a la par del peso real, detalla.

Para entender qué implicancias podría tener el desarrollo de un peso digital en al Argentina, primero haría falta definirlo, explica Ignacio Carballo, director del Ecosistema Fintech & Digital Banking de la Universidad Católica Argentina (UCA).

“Se tiene que definir cuál va a ser el protocolo, la cotización, si va a tener una emisión controlada, quiénes van a poder acceder y para qué actividades servirá”, explica, a modo de ejemplo, el especialista. Hay al menos 50 proyectos de monedas digitales en el mundo y no todos son iguales, añade. Uruguay, Brasil y Chile son países donde la idea se está debatiendo.

“De mínima esperamos que esté almacenado en alguna configuración de blockchain (la tecnología detrás de las criptomonedas), que pueda ser trazable, cuantificable e inadulterable; algo que no necesariamente funciona así en el sistema financiero actual”, detalla Carballo. Por ejemplo, dice, lo que se sabe del yuan digital es que es una moneda emitida por el gobierno chino, pero no hay datos sobre la configuración de la emisión. “Lo único que sabemos es que está almacenado en blockchain y que se puede usar para el comercio minorista, como para cobrar premios de la lotería”, señala.

En ese sentido, Andragnes suma que los países que desarrollan sus monedas digitales lo hacen por cuestiones de eficiencia (por mayor trazabilidad y programabilidad) y de certeza (porque, explica, la emisión no es generalmente “auditable por todos”) y por la creencia de que es “más confiable”, aunque eso es debatible, señala. Con el yuan digital, apunta, China pretende agilizar el flujo de dinero digital, aumentar el acceso a cuentas monetarias digitales entre sus ciudadanos, facilitar el uso en el resto del mundo y evitar la falsificación. Además, como está integrada en contratos inteligentes, podría darse una situación de este tipo: que un comerciante envíe mercadería a China y que, cuando se abra la caja en el destino, automáticamente se ejecute el pago en otra parte del mundo.

##### ¿PUEDE EL PESO DIGITAL AYUDAR A BAJAR LA INFLACIÓN?

En la Argentina, el debate más allá de la eficiencia y la trazabilidad pasa por la inflación. ¿Puede ayudar a contenerla? “La idea de un ‘peso digital’ es interesante en la medida que logre reducir los costos y las barreras de entrada al sistema financiero, pero requiere que el organismo que la emita tenga credibilidad respecto a cómo va a evolucionar la oferta monetaria futura. El BCRA no es actualmente una institución que genere esa credibilidad”, dice Federico Moll, director de la consultora Ecolatina.

“El desequilibrio monetario argentino no está relacionado con la herramienta sino con su uso. El peso es tan buena moneda como la autoridad monetaria que lo controla, ¿por qué el ‘peso digital’ tendría mejor suerte que el peso?”, se cuestiona el economista.

“Suponiendo que un criptopeso fuera desarrollado sobre una blockchain no manipulable por el Estado y con una emisión escasa y predefinida, y esta moneda fuese la única de curso legal, entonces el Estado no podrá emitir más de las que estén programadas, y eso indefectiblemente llevará a una baja de la inflación, aunque hay muchos otros factores que deberían también ser ajustados”, explica Andragnes.

Para Carballo, no existiría razón para que un peso digital baje la inflación a menos que se asegure que con eso no va a haber más emisión. “Sin embargo, sí puede mejorar otros problemas macroeconómicos, como la inclusión financiera. Si se mejora la eficiencia de los sistemas financieros, se logra que la política económica tenga impacto en mayor cantidad de personas que están siendo partícipes de ese sistema”, apunta.

Para terminar, Moll deja abierto un debate: ¿Qué pasaría con la economía informal en un esquema en el que el único medio de pago sea una moneda digital controlada por el gobierno? “La respuesta no me parece obvia, pero no me imagino un país funcional en ese escenario”, concluye.