+++
author = ""
date = 2021-07-19T03:00:00Z
description = ""
image = "/images/d4skrj7pzbayrafbbg2rwscxvu.jpg"
image_webp = "/images/d4skrj7pzbayrafbbg2rwscxvu.jpg"
title = "SEGÚN FELIPE SOLÁ, \"LAS ACTITUDES HOSTILES DE BRASIL MATARON EL DEBATE EN EL MERCOSUR\""

+++

**_Las tensiones en el Mercosur entre Argentina y Brasil no paran de subir. Luego de que Alberto Fernández y Jair Bolsonaro sacaran a la luz sus diferencias sobre el rumbo del organismo, el canciller argentino, Felipe Solá, viajó al país vecino para reunirse con su par brasilero y en una entrevista con el diario O Globo apuntó duramente contra la administración de Bolsonaro, haciendo principal hincapié en su ministro de Economía, Paulo Guedes._**

> “Con Brasil no hay debate, conversaciones entre ministros, debates académicos, con empresarios, sindicatos, debates francos, todo eso es impensable. Las actitudes hostiles mataron el debate”, sentenció el funcionario.

Y, para dejar en claro la postura del Gobierno, agregó: “Argentina está actuando como alguien que quiere preservar un matrimonio, pese a lo que los otros hacen”.

La disputa entre Argentina y Brasil, que tiene el apoyo de Uruguay y generó un contrapunto entre Alberto Fernández y Luis Lacalle Pou en la última reunión del 8 de julio, es, primero, por la posibilidad flexibilizar acuerdos -que cada país pueda negociar con terceros países sin la autorización o el consenso de sus socios del bloque- y, segundo, para que el Arancel Externo Común (AEC) se reduzca drásticamente para ganar competitividad.

En promedio este arancel en el bloque es del 13% (el impuesto que paga cada mercancía al ingresar al Mercosur por cualquiera de sus Estados Partes) y Bolsonaro junto a su ministro de Economía, Paulo Guedes, y secundado por Uruguay, quiere rebajarlo al 6 o 7%. A nivel mundial, en promedio, el arancel ronda el 5,5%. Argentina propuso hace un par de meses que esa reducción del AEC se haga de manera progresiva: una baja del 10% ahora y otra para 2022. Pero no fue aceptada.

Solá sostuvo que llevar esto a cabo perjudicaría a los países del bloque y no redundaría en una baja de los precios: “Brasil defiende reducir la TEC (arancel de importaciones) sin tener mecanismos de control del impacto de esas medidas sobre sectores productivos de nuestros países, no tiene en consideración la opinión de su industria y de la industria de los países”.

Asimismo, se mostró sorprendido por la tensión entre ambos países pero dejó en claro que la Argentina está dispuesta a retomar las conversaciones: “Nunca pensamos que eso podría suceder. Después nos fuimos acostumbrando y finalmente la única cosa que queríamos era no ser insultados. Nos acostumbramos a negociar a pesar de las circunstancias y continuaremos intentando negociar. Si el ministro Guedes quiere negociar con nosotros estamos abiertos. Si se quiere imponer sin negociación, eso es otra cosa”.

El viaje de Solá fue para reunirse con su su par de Brasil, Carlos França, con quien analizó la situación del Mercosur, repasó diversos aspectos de la agenda bilateral y evaluó temas de cooperación. El encuentro tuvo lugar luego de la participación de ambos ministros de Relaciones Exteriores en el acto de conmemoración por el 30º Aniversario de la creación de la Agencia Brasileño-Argentina de Contabilidad y Control de Materiales Nucleares (ABACC), realizado en Río de Janeiro.

Sobre el comercio bilateral, Solá le planteó a França que existe un “gran potencial” para que siga creciendo y resaltó que durante este primer semestre de 2021 repuntó el intercambio entre ambos países.

Otro de los planteos del Canciller fue la necesidad de continuar con las reuniones bilaterales para monitorear la evolución de la bajante extraordinaria que afecta a la Cuenca del Plata y tomar las medidas necesarias a fin de garantizar el suministro de agua potable a las poblaciones de las provincias ribereñas de la Argentina.

De la reunión de cancilleres participó el director general del Organismo Internacional de Energía Atómica (OIEA), Rafael Gross y, por el lado argentino, estuvieron el ministro de Ciencia y Tecnología, Roberto Salvarezza; el jefe de Gabinete de la Cancillería, Guillermo Justo Chaves y el embajador Daniel Scioli.