+++
author = ""
date = 2022-01-12T03:00:00Z
description = ""
image = "/images/kfvbc2hfbfctnkhudg2oc7di6u.jpg"
image_webp = "/images/kfvbc2hfbfctnkhudg2oc7di6u.jpg"
title = "LUANA VOLNOVICH, LA TITULAR DEL PAMI DECIDIÓ VERANEAR EN EL CARIBE MEXICANO"

+++

#### **_Luana Volnovich, a pesar de que el presidente pidió a sus funcionarios viajar por el país, se fue a esas playas junto a Martín Rodríguez, su pareja, que es el N° 2 del organismo de los jubilados_**

Antes del inicio de la temporada, cuando el gobierno nacional decidió prohibir la venta de pasajes al exterior en cuotas, en un intento de no agravar la escasez de dólares, Alberto Fernández le pidió a sus funcionarios que disfruten de sus vacaciones dentro del país.

Este pedido informal buscaba que los referentes del oficialismo “den el ejemplo” -sobre todo en medio de la explosión de contagios de coronavirus- y eviten generar indignación entre quienes no pudieron viajar.

Sin embargo, lo que temían en la Casa Rosada terminó sucediendo. Las imágenes de las vacaciones de la titular del PAMI, Luana Volnovich, en la paradisíaca isla de Holbox, en el Caribe de México, comenzaron a viralizarse en las redes sociales y generaron una ola de repudio.

De hecho, el propio presidente Alberto Fernández, hacia fin de año, en una reunión informal que se desarrolló en la Quinta de Olivos confirmó que les había transmitido a sus funcionarios que no tenía problemas en que viajaran a descansar después de un año exigente e intenso por el COVID, pero que “no viajaran al exterior, nadie”.

La polémica se desató con un video en el que se puede observar cómo Volnovich -referente de la agrupación La Cámpora- disfruta un trago y conversa en un bar con su pareja, Martín Rodríguez, quien es Subdirector Ejecutivo de la obra social de los jubilados.

Rodríguez también es militante de La Cámpora y antes de trabajar en PAMI fue presidente del Concejo Deliberante de Hurlingham (2015 - 2019).

Holbox es una isla con playa de arena blanca ubicada al norte de Cancún, en la península de Yucatán. Una zona muy visitada por argentinos. Muy lejos de los destinos nacionales que promueven desde el gobierno nacional.

La situación generó una rápida respuesta desde la Casa Rosada. Aclararon que Alberto Fernández “nunca sugirió no tomarse vacaciones”. “Es una funcionaria que trabaja mucho y merece tomarse 15 días de descanso”, fue la explicación oficial. Similar a la que esgrimieron desde el PAMI.

Entre las críticas que recibió la funcionaria en las redes sociales, varios usuarios remarcaron que ayer, cuando ya se encontraba en el Caribe, desde su cuenta oficial Volnovich elogió a dos jubiladas que disfrutaban de sus vacaciones en el país.

“Qué lindo ver a Yolanda y sus amigas encontrarse y disfrutar en las Termas de Colón y Mardel con #PreViajePAMI”, decía el mensaje publicado el 10 de enero.

“Es una vergüenza mientras los jubilados no tienen prestaciones , y cobran jubilaciones de 29 mil pesos , Luana Volnovich y su segundo está de vacaciones en el Caribe”, planteó Graciela Ocaña, diputada nacional y ex titular del PAMI.