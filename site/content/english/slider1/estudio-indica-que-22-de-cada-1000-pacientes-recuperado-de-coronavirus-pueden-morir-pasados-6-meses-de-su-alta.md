+++
author = ""
date = 2021-04-29T03:00:00Z
description = ""
image = "/images/amp-pacientes-con-covid-19-atendidos-en-uci-pueden-tener-complicaciones-a-largo-plazo.jpg"
image_webp = "/images/amp-pacientes-con-covid-19-atendidos-en-uci-pueden-tener-complicaciones-a-largo-plazo.jpg"
title = "ESTUDIO INDICA QUE 22 DE CADA 1000 PACIENTES RECUPERADO DE CORONAVIRUS PUEDEN MORIR PASADOS 6 MESES DE SU ALTA"

+++

**_Se trata de un estudio publicado en la revista Nature y realizado en los Estados Unidos. Los investigadores dicen que los médicos deben prestarles atención a los pacientes durante los meses siguientes a la recuperación._**

Un estudio en los Estados Unidos descubrió que 22 personas de cada 1.000 que padecieron COVID-19 podrían morir a los 6 meses de recibir el alta en comparación con 14 de cada 1.000 que no tuvieron la infección. Si bien es un riesgo de aumento de la mortalidad bajo, los expertos que hicieron el estudio y lo publicaron en la revista científica Nature advierten que el sistema de salud pública debería tenerlo en cuenta para prestar mejor atención a los pacientes recuperados.

“Los resultados proporcionan una hoja de ruta para informar la planificación del sistema de salud y el desarrollo de estrategias de atención multidisciplinaria para reducir la pérdida de salud crónica entre los recuperados de COVID-19”, escribieron en el trabajo los investigadores Ziyad Al-Aly, de la Facultad de Medicina de la Universidad de Washington, Yan Xie y Benjamin Bowe.

Para hacer la investigación, usaron la base de datos de la Administración de Veteranos de los Estados Unidos. Analizaron los datos de seguimiento de 73.435 pacientes que se contagiaron y que no fueron internados e incluyeron a 4.990.835 personas como controles. Además, consideraron 13.654 pacientes que sí fueron hospitalizados por COVID-19 y los compararon con 13.997 pacientes que tuvieron gripe y que fueron internados.

Los recuperados del COVID-19 tienen un riesgo de muerte casi un 60% mayor hasta seis meses después de la infección en comparación con las personas no infectadas, según el trabajo. Esto equivale a unas ocho muertes adicionales por cada 1.000 pacientes en seis meses. Según el investigador principal, Al-Aly, “Cuando contabilizamos las muertes por COVID-19, el total real de muertes es mucho mayor”.

La mayoría de las muertes causadas por las complicaciones a largo plazo de la COVID-19 no se registran como muertes por la COVID-19, señaló Al-Aly al . Por eso, “lo que estamos viendo ahora es sólo la punta del iceberg”, añadió. “Nuestro estudio demuestra que hasta seis meses después del diagnóstico, el riesgo de muerte tras un caso, incluso leve, de COVID-19 no es trivial y aumenta con la gravedad de la enfermedad”, remarcó el investigador. “No es exagerado decir que la COVID-19 larga -las consecuencias sanitarias a largo plazo de la COVID-19- es la próxima gran crisis sanitaria de Estados Unidos”, sostuvo.

Al tener en cuenta que más de 30 millones de estadounidenses se han infectado con este virus, y dado que la carga de COVID-19 largo es sustancial, los efectos persistentes de esta enfermedad impactarían durante muchos años e incluso décadas, alertó el investigador e hizo un llamado de atención para el personal de la salud: “Deben estar atentos a la hora de evaluar a las personas que han tenido COVID-19. Estos pacientes necesitarán una atención integrada y multidisciplinar”.

Aclararon que el nuevo estudio difiere de otros que han analizado el impacto del COVID-19 a largo plazo porque, en lugar de centrarse sólo en las complicaciones neurológicas o cardiovasculares, se usó la base de datos de la Administración de Salud de los Veteranos (VHA) para catalogar exhaustivamente todas las enfermedades que pueden atribuirse al COVID-19″, dijo Al-Aly, quien es también director del Centro de Epidemiología Clínica y jefe del Servicio de Investigación y Educación del Sistema de Atención Médica de Asuntos de los Veteranos de San Luis.

“Todavia estamos aprendiendo y conociendo una nueva faceta de esta enfermedad”, dijo a Infobae el doctor Mario Boskis, médico cardiólogo y director del Grupo Cardiológico Boskis, que lleva adelante un protocolo de seguimiento de pacientes recuperados de COVID-19 en Argentina. “De acuerdo a varios estudios epidemiologicos efectuados tanto en Europa como en los Estados unidos hay cerca de un 30% de pacientes que permanecen con sintomas mas alla de las tres semanas del alta por Covid. Lo llamamos COVID sub agudo o COVID Largo”, explicó.

Los sintomas van desde manifestaciones fisicas como palpitaciones, dolor de pecho, fatiga, hasta psicologicas como depresion o dificiltad para concentrarse. “Hoy le recomendamos a todos los que han padecido el COVID-19, que más allá de la gravedad de los sintomas, se hagan un control médico entre las dos a tres semanas del alta, para evaluar si hay alguna lesión residual en el organismo, especialmente si se quiere volver a efectuar actividad fisica vigorosa”, expresó el doctor Boskis.

Hasta el momento, se han reportado trabajos que informan diferentes síntomas y trastornos después de la infección por el coronavirus. En el sistema respiratorio, puede producirse tos persistente, dificultad para respirar y bajos niveles de oxígeno en la sangre. En el sistema nervioso, ataque cerebral, dolores de cabeza, problemas de memoria y problemas con los sentidos del gusto y del olfato.

También el COVID-19 se ha asociado con secuelas en la salud mental, con ansiedad, depresión, problemas de sueño y abuso de sustancias, y en el sistema cardiovascular, con enfermedad coronaria aguda, insuficiencia cardíaca, palpitaciones y ritmos cardíacos irregulares. En el sistema gastrointestinal, algunos pacientes recuperados padecen estreñimiento, diarrea y reflujo ácido. Malestar, fatiga y anemia también pueden ser secuelas.

A nivel mundial, se sabe que uno de cada diez pacientes que sufre COVID puede tener alguna forma de inflamación cardiaca. De acuerdo con el médico cardiólogo Juan Pablo Costabel, del Instituto Cardiovascular, “se sabe que aquellos que tienen más chances de padecerla son quienes tienen formas graves de COVID, en los que el proceso inflamatorio es mayor”.