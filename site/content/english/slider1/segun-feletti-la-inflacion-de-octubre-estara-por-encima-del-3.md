+++
author = ""
date = 2021-11-05T03:00:00Z
description = ""
image = "/images/roberto-feletti-20211011-1243072-1.jpg"
image_webp = "/images/roberto-feletti-20211011-1243072.jpg"
title = "SEGÚN FELETTI, LA INFLACIÓN DE OCTUBRE \"ESTARÁ POR ENCIMA DEL 3%\""

+++

#### **_El secretario de Comercio Interior, Roberto Feletti, dijo hoy que “no cree” que la inflación de octubre sea menor al 3% pese al congelamiento de precios que decretó el Gobierno._**

> “La inflación no la vamos a resolver sólo desde el control de precios, que es solo un dato. Pero sabemos que hay una oferta monopólica y en ese contexto hay que regular los consumos esenciales”, insistió Feletti, en diálogo con Radio 10. “Esa es la primera tarea y la más inmediata de la Secretaria, que me planteó y me pidió el Presidente”, agregó.

Feletti anticipó que el efecto del congelamiento de precios de 1432 productos recién se sentirá en el índice de precios al consumidor correspondiente a noviembre: “El índice general de octubre no creo que esté debajo del 3%, esperamos que el índice de alimentos esté por debajo del 2,9%”.

Feletti indicó que el primer objetivo de la Secretaría al comenzar su gestión fue “parar y estabilizar precios”, luego de observar “una aceleración muy fuerte e inexplicable de precios en alimentos y en medicamentos en las primeras dos a tres semanas de octubre”.

Por su parte, adelantó que el lunes se reunirá con las cadenas regionales y los pequeños comerciantes con el objeto de “evaluar cuestiones de márgenes y abastecimiento”, en los cuales “hay diferencias con las grandes cadenas”.

##### ¿Cuándo se ordenarían los precios de los medicamentos?

El secretario de Comercio Interior también se refirió al principio de acuerdo con los laboratorios para retrotraer los precios de los medicamentos al 1 de noviembre y mantenerlos sin cambios hasta el 7 de enero.

> “Todos sabemos, y eso por supuesto lo tienen que decir los sanitaristas, que el consumo de medicamentos normalmente gira en torno de 500 productos, pero van a ir todos los medicamentos”, explicó Feletti.

Tras lo cual, aseveró que hubo “afortunadamente compresión de las cámaras farmacéuticas”, y adelantó que desde el lunes “tendrían que empezar a ordenarse” los precios.

##### Cómo se realizará el monitoreo de los precios de medicamentos

Feletti precisó que la modalidad de monitoreo será a través de la carga de precios por parte de los laboratorios a través del Vademecum Nacional de Medicamentos que publica la Administración Nacional de Medicamentos, Alimentos y Tecnología Médica (Anmat), la base de datos de los medicamentos que se comercializan actualmente en el país.

“Los laboratorios acordaron que hay que integrar lo que no estaba integrado en el Vademecum”, precisó Feletti, a la vez que recordó que el mecanismo fue “abandonado por el macrismo” y “ahora hay consensos con la Ministra (de Salud, Carla) Vizzotti de reconstruirlo”.

A partir del lunes los laboratorios van a incorporar los productos “y, sobre eso, vamos a empezar a trabajar la estabilización”, manifestó el funcionario.

“En principio sería sobre todos los productos hasta el 7 de enero, y luego en la mesa de trabajo, todos sabemos que en estas cosas suelen haber discusiones por lo que se irá rectificando y corrigiendo”, agregó. En ese sentido, Feletti subrayó la necesidad de “abrir un conjunto de mesas de trabajo, como en el caso de los alimentos”, con el objetivo de “ir consolidando estas dos canastas reguladas de bienes esenciales”.

“En el caso de los medicamentos, hay afortunadamente una industria nacional importante y fuerte; y también tiene que haber un escenario en el cual haya una regulación sobre la base de definir el acceso a los ingresos populares”, expresó el secretario, y agregó: “la gente no elige comer o no comer; o atenderse o no atenderse cuando está enfermo; es un mercado de demanda rígida”.