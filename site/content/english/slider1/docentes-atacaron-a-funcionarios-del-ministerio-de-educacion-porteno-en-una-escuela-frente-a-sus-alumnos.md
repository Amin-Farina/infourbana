+++
author = ""
date = 2021-09-22T03:00:00Z
description = ""
image = "/images/screenshot_1-jpg_758443921.jpg"
image_webp = "/images/screenshot_1-jpg_758443921.jpg"
title = "DOCENTES ATACARON A FUNCIONARIOS DEL MINISTERIO DE EDUCACIÓN PORTEÑO EN UNA ESCUELA FRENTE A SUS ALUMNOS"

+++

El viernes pasado, durante una visita por parte del personal del ministerio de Educación porteño a la Escuela N°19 “José Martí” de Villa Soldati, tres docentes increparon a los funcionarios frente a los alumnos y no los dejaron ingresar.

El video lo difundió la propia ministra de Educación de la Ciudad de Buenos Aires, Soledad Acuña, en su cuenta de Twitter. “Lo que van a ver en el video es todo lo que no queremos que pase dentro de la escuela. Sucedió durante una visita realizada por personal del Ministerio el viernes pasado en una Escuela de la Ciudad”, comentó.

Del mismo modo, Acuña informó que se les inició un sumario a los tres docentes involucrados, por “increpar con lenguaje inapropiado a los trabajadores del Ministerio” frente a los alumnos y “sin justificación”. La escena ocurrió mientras el personal hacía una de las recorridas habituales por los colegios de la Ciudad.

En el video se ve a tres docentes impidiendo el ingreso de los trabajadores del ministerio. Uno de ellos es Jorge Adaro, titular de Ademys, el gremio porteño más combativo que rechazó una y otra vez el regreso de los chicos a las aulas. “35 compañeros muertos. Filmá lo que quieras. Filmá lo que quieras como buen rati que sos. 35 compañeros muertos tenemos con esta presencialidad criminal”, dijo cuando advirtió que estaba siendo grabado.

El otro docente también los increpó: “Dejó sin educación a muchísimos chicos por decisión política. Tenemos compañeros enterrador por decisión suya. Nunca le importó la educación pública. Es un mentiroso”. La escena transcurrió durante la salida de los chicos del colegio. Por eso, varios alumnos de primaria presenciaron la discusión que se dio en el patio.

La ministra Acuña lamentó el incidente. “Sobran las palabras ante estas imágenes. Creemos firmemente en un sistema Educativo basado en los principios de libertad y la pluralidad para el desarrollo de las personas. Tenemos la convicción que el rol trascendental de los docentes es la formación integral del estudiante y que para ello deben tener una conducta acorde a la función educativa”.

En la misma línea, agregó: “No voy a permitir este tipo de comportamientos. Las familias confían la educación y la formación de sus hijos a los docentes, quienes además son sus referentes y modelos de ejemplaridad y excelencia. No normalicemos los malos ejemplos”.

Si bien a los tres docentes se les abrió un sumario, seguirán dando clase en la escuela hasta que se resuelva el proceso administrativo. Dentro de las posibles sanciones, figura la suspensión provisoria y separación de sus cargos.