+++
author = ""
date = 2021-10-12T03:00:00Z
description = ""
image = "/images/6k2bvoz4yrg5te7af6edyox5zy.jpg"
image_webp = "/images/6k2bvoz4yrg5te7af6edyox5zy.jpg"
title = "NIK CONTÓ QUE LO LLAMÓ EL VOCERO DE ANÍBAL FERNÁNDEZ"

+++

##### **_Horas después del mensaje amenazante que le escribió Aníbal Fernández en redes sociales, Nik reveló que recibió un llamado del vocero del ministro de Seguridad de la Nación. "Me dijo, riéndose, que a Aníbal se le fue un poquito la mano", relató el humorista gráfico, al borde del llanto. Y culminó: "No son conscientes de lo que hacen"._**

La polémica comenzó este domingo, cuando Fernández respondió a un mensaje de Twitter en el que Nik cuestionaba el clientelismo político y pronosticaba una nueva derrota del Gobierno en las elecciones de noviembre.

"Muchas escuelas y colegios de la CABA reciben subsidios del estado y está bien. Por ejemplo la escuela/colegio ORT. ¿La conoces? Sí que la conoces… O querés que te haga un dibujito? Excelente escuela lo garantizo. Repito… ¿Lo conoces?", le preguntó, insistente, el funcionario.

A la ORT acuden las hijas del dibujante. Por eso, Nik calificó el mensaje de Aníbal como "persecutorio", dijo que era antisemita y aseguró tener miedo.

Este lunes por la tarde, el creador de Gaturro se refirió al episodio y reveló un llamado del vocero del ministro.

"No sé si debo contar esto. Me llamó el jefe de prensa de Aníbal... Que Aníbal está en Tucumán. Que en realidad, con una sonrisa, se le fue un poquito la mano. Se reía. Que sí, que me quería llamar", fue relatando Nik lo que el vocero le habría dicho.

Notoriamente nervioso, continuó: "Yo estoy más para abrazar a mis hijas y ponerme a llorar que para hablar con este señor, que no tiene conciencia de lo que hace".

Al borde del llanto y con dificultades para hablar, agregó: "Me dio una indignación. Se te cagan de risa en la cara. Me dan ganas de llorar. ¿Cómo te va a llamar riéndose?".

Al humorista gráfico se le quiebra la voz en el diálogo con Radio Mitre. Calla por unos instantes. Minutos antes Nik había contado que iba a llevar a sus hijas al cine, para despejarse: una de ellas se había largado a llorar esta tarde.