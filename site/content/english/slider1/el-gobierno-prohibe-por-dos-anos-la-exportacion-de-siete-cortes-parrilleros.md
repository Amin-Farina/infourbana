+++
author = ""
date = 2022-01-03T03:00:00Z
description = ""
image = "/images/la_aduana_evitx_exportacixn_de_cortes_de_carne_no_permitidosx_x1x_1_crop1630438165593-jpg_1325023081.jpg"
image_webp = "/images/la_aduana_evitx_exportacixn_de_cortes_de_carne_no_permitidosx_x1x_1_crop1630438165593-jpg_1325023081.jpg"
title = "EL  GOBIERNO PROHIBE POR DOS AÑOS LA EXPORTACIÓN DE SIETE CORTES PARRILLEROS"

+++

#### **_El Gobierno oficializó hoy el nuevo esquema de exportación de carne vacuna para el presente año. Fue mediante el Decreto 911/2021 que lleva las firmas del presidente Alberto Fernández, el Jefe de Gabinete de ministros, Juan Manzur, y del ministro de Agricultura, Julián Domínguez, quien además publicó dos resoluciones al respecto._** 

Todo esto sucede luego que en el último día del 2021 finalizara la vigencia de las restricciones a la exportación que desde mayo del mencionado año instrumentó el Gobierno, con el objetivo de bajar el precio de la carne al consumidor.

En el artículo 1º del Decreto, se suspende hasta el 31 de diciembre del 2023 la exportación definitiva y/o suspensiva, con destino al exterior del país, de los siguientes cortes bovinos frescos, enfriados o congelados: **_Reses Enteras, Medias Reses, Cuarto Delantero Con Hueso, Cuarto Trasero Con Hueso, Medias reses incompletas con Hueso y Cuartos delanteros incompletos con Hueso_**.

Por otro lado, se prohíbe la exportación hasta fin del 2023 de los siete cortes parrilleros que tienen una alta demanda en el mercado interno, que son frescos, enfriados o congelados. Ellos son: Asado con o sin hueso, Falda, Matambre, Tapa de asado, Nalga, Paleta y Vacío. 

En los considerandos de la medida, se señaló que “es necesario construir una política pública para la cadena de la carne con el objeto de aumentar la producción, la existencia ganadera y el peso promedio de faena, generando previsibilidad al productor, e incrementar asimismo los volúmenes exportables. Además, que las medidas que se disponen contribuyen a generar un equilibro entre el mercado argentino y la exportación de productos cárnicos”.

En mayo del año pasado el Gobierno comenzó a restringir las exportaciones de carne vacuna con el objetivo de hacer bajar el precio de la carne al consumidor, algo que hasta el momento no se consiguió. En noviembre pasado, el precio en los mostradores de las carnicerías de Capital Federal y Gran Buenos Aires, aumentó más de un 10%, según los datos del Instituto de Promoción de la Carne Vacuna Argentina (IPCVA).

Otra decisión que contempla el Decreto, es la facultad que se otorga al ministerio de Agricultura a dictar las normas complementarias que resulten necesarias para la instrumentación de la medida. Asimismo, el referido Ministerio queda facultado para establecer excepciones debidamente fundadas a las pautas establecidas en el artículo referido a la prohibición de exportar los cortes parrilleros.

Además, la medida contempla que se encuentran excluidas de las limitaciones establecidas en el artículo 2° de la presente medida, las exportaciones con destino al Área Aduanera Especial, creada por la Ley N° 19.640 y/o las realizadas desde esa Área Aduanera Especial con destino al Territorio Aduanero General.

##### Resoluciones

A su vez, el ministerio de Agricultura publicó en el Boletín Oficial las Resoluciones 301/21 y 302/21 que instrumentan el Decreto N° 911/21 del presidente Alberto Fernández y la Resolución conjunta con la cartera de Desarrollo Productivo N° 10.

Mediante la Resolución 301/2021 se libera en su totalidad las exportaciones de la carne vacuna proveniente de la faena de las vacas conserva o manufactura categorías D y E (es una carne de baja calidad que se comercializa a China), y toros, así como de huesos con carne resultantes del desposte. Dichas categorías quedan exceptuadas de las limitaciones a los cortes parrilleros. También mantiene el listado de 63 frigoríficos a los que se le asignó un cupo de exportación el año pasado y se les solicita presentar una Declaración Jurada de Exportación de Carnes (DJEC) cada vez que quieran exportar carne.

En dicha Resolución se destaca, que “las DJECs de los productos de los artículos 2°, 3° y 4° de la presente medida deberán estar conformadas, en su totalidad, por cortes de carne correspondientes a estas categorías únicamente. Para poder declarar las toneladas, las cajas embaladas con los cortes cárnicos deberán estar en depósito al momento de la presentación de la DJEC”. Eso significa, que esa carne que usualmente tiene como destino el mercado de China estará liberada, pero condicionada a la aprobación de las DJEC por parte de los funcionarios.

A todo esto, mediante las facultades otorgadas en el Decreto presidencial, la cartera agropecuaria publicó la Resolución 302/21 por la cual se abre la inscripción para la exportación de carne vacuna, para aquellas plantas faenadoras o procesadoras habilitadas que no fueron incluidas en las normativas que se implementaron en junio de 2021, y también a los grupos de productores que pretendan exportar carne vacuna para el año 2022.

A su vez, la Resolución en conjunto con el Ministerio de Desarrollo Productivo Nª 10/21, determina que la carne vacuna con destino al comercio minorista deben salir de los frigoríficos en unidades de hasta 32 kilos. Según comentaron desde la Subsecretaría de Ganadería del ministerio de Agricultura, con dicha medida se está dando respuesta a un pedido de las entidades gremiales que representan a los productores. Para cumplir con estos requisitos se otorgan plazos de hasta 6 meses.

Mediante un comunicado, desde la subsecretaría de Ganadería señalaron que las medidas anunciadas por el Gobierno en el Boletín Oficial fueron consensuadas con las entidades de productores y la industria frigorífica, con el objeto de dar previsibilidad y confianza a la ganadería argentina, garantizando la producción, la exportación y el consumo de los argentinos, en base a los análisis técnicos del sector”.

##### Creación

En las disposición de la cartera agropecuaria para la exportación de carne vacuna para el presente año, se instrumentó la creación en el ámbito del Ministerio de Agricultura del “Observatorio de la Producción de la Carne Vacuna”, que será un organismo de carácter consultivo, con la finalidad de brindar asesoramiento para aumentar la productividad, otorgar previsibilidad y confianza al productor y generar un adecuado ambiente de negocios.

El mismo estará presidido por la máxima autoridad de la Unidad Gabinete de Asesores del Ministerio de Agricultura, Ganadería y Pesca, que dictará su reglamento de funcionamiento, y deberá constituirse dentro de los 90 días de la entrada en vigencia de la presente medida. Se invitará a participar del Observatorio a los representantes de las provincias argentinas, de las entidades del sector agropecuario, de las cámaras de la industria frigorífica, de engorde a corral, de matarifes y abastecedores, y de productores exportadores, del Instituto de Promoción de la Carne Vacuna Argentina, y a la coordinación de la Mesa de las Carnes.

###### Según consta en la Resolución, las funciones del Observatorio serán:

* Analizar la evolución y composición de las existencias de ganado bovino.
* Analizar la evolución y la composición de la faena bovina y de las exportaciones de carne.
* Proponer políticas públicas para el desarrollo de la ganadería argentina y su cadena de valor.
* Evaluar la previsibilidad y sustentabilidad de la promoción y el desarrollo de la ganadería argentina y de todas las políticas públicas referentes a las exportaciones de carne.
* Sugerir la composición y proyecciones de exportaciones de carne vacuna de conformidad con los informes del Observatorio.