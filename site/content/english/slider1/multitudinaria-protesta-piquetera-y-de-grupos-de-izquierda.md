+++
author = ""
date = 2021-06-18T03:00:00Z
description = ""
image = "/images/2o3y3zzeofcejjovu53fpuulcq.jpg"
image_webp = "/images/2o3y3zzeofcejjovu53fpuulcq.jpg"
title = "MULTITUDINARIA PROTESTA PIQUETERA Y DE GRUPOS DE IZQUIERDA"

+++

**_Viernes complicado en el centro porteño. Una de las mayores movilizaciones de protesta de los diferentes movimientos sociales críticos del oficialismo y principalmente de grupos de izquierda, se concentra frente al edificio del Ministerio de Desarrollo Social, en plena avenida 9 de Julio, y hasta amenaza con montar carpas en la zona si sus referentes no son recibidos por los funcionarios nacionales._**

Ante estas manifestaciones, el Gobierno a través del Ministerio de Seguridad de la Nación monitorea los ingresos a la Ciudad y sostiene que movilizarse “es un derecho”. También fuentes de esa cartera afirman que es la Ciudad, a través de su propia Policía, la que deberá controlar y disuadir a quienes protestan.

Fuentes de ese Ministerio dijeron que están atentos a las movilizaciones pero que no se trata de un delito federal. “Al descomprimirse las restricciones empezamos a ver estos movimientos. Hay reclamos sociales y también gremiales. Detrás obvio que existe una animosidad política, claramente”, expresan por lo bajo. “Faltan tres meses para las elecciones y es la forma de algunos de tener visibilidad”, agregan.

Desde el Ministerio de Seguridad de la Nación que conduce Sabina Frederic dispusieron hoy desde temprano un operativo de seguridad para impedir el corte del Puente Pueyrredón que conecta Avellaneda con el barrio porteño de Barracas, pero no tuvieron demasiado éxito, ya que el acceso desde Avenida Mitre quedó cortado. Hubo un gran dispositivo de la Prefectura y de la Policía Federal para garantizar la circulación vehicular ante la columna de unos 50 trabajadores de la salud autoconvocados, en su mayoría enfermeros que reclaman ser profesionales ante el gobierno de Horacio Rodríguez Larreta.

Además, se sumó al operativo personal de la Policía de la provincia de Buenos Aires, de la Ciudad de Buenos Aires y agentes de tránsito de la municipalidad de Avellaneda. Esto provocó momentos de tensión, con empujones, donde volaron algunas pedradas y el tránsito quedó interrumpido. Varios de los manifestantes se quedaran sentados o acostados sobre el asfalto a la espera de alguna solución y se mantienen en asamblea debatiendo los pasos a seguir.

Otras columnas de grupos de izquierda, en tanto, ingresaron a la Ciudad desde la zona Sur por el Viejo Puente Pueyrredón de Avellaneda para ir hacia Desarrollo Social. Una nutrida columna que nucleó al Polo Obrero, Barrios de Pie, el Movimiento Teresa Rodríguez, Libres del Sur, el Frente de Organizaciones en Lucha (FOL) y el Movimiento Territorial de Liberación pasó por allí sin interrumpir la circulación. Otras columnas se sumarán después del mediodía desde Once y desde el norte por la autopista Illia, provocando un caos de tránsito en el Centro porteño.

Funcionarios del Gobierno ven “intenciones políticas” detrás de las marchas y apuntan a grupos “chicos”, a la izquierda y a Libres del Sur por la alianza con el espacio peronista opositor de Florencio Randazzo. El viernes de la semana pasada, una manifestación impulsada por el Plenario del Sindicalismo Combativo, agrupamiento gremial afín al Frente de Izquierda y de los Trabajadores (FIT), provocó un conflicto en el Puente Pueyrredón que derivó en incidentes. Mientras que otros grupos de izquierda hicieron colapsar el centro porteño cuando llegaron de a miles con bombos y altoparlantes hasta la Plaza de Mayo.

Una semana después, se esperan nuevas movilizaciones que, fueron gestadas en el bloque piquetero. “Casi 20 meses de gobierno y sigue creciendo la pobreza. En el sexto mes del año, segundo año del mandato de Alberto Fernández, han quedado más claro que nunca sus prioridades”, reza el comunicado del Polo Obrero con la convocatoria a cortes y movilizaciones.

Eduardo Belliboni, dirigente del Polo Obrero, “el Gobierno está haciendo un ajuste para pagarle al FMI y el único recurso que nos queda es la movilización popular para frenarlo”. Piden que se reinstaure el IFE, la ayuda social que se había dispuesto durante los primeros meses de la pandemia y que alcanzó a unos 8 millones de personas. “Se rompieron los diques de contención, por eso salimos a ver si aflojan con temas elementales como una ayuda elemental para quienes perdieron comida, trabajo y abrigo”, agregó Belliboni.

Desde el Ministerio de Desarrollo Social, que conduce Daniel Arroyo, anunciaron ayer un bono de 6.000 pesos para los beneficiarios del Plan Potenciar Trabajo; hace un mes aumentaron los montos de la Tarjeta Alimentar, y aseguran que la cantidad de personas que asisten a comedores comunitarios se mantiene estable, en 10 millones de personas. “Tenemos diálogo permanente con todos, y consideramos que algunos reclamos tienen sentido, pero hay que cuidarnos y ser cuidadosos en este contexto de pandemia”, dijo una alta fuente de Desarrollo Social, en referencia a los contagios en las marchas.

En la Nación insisten hace meses en que esos reclamos no son viables y están firmes en que no se volverá al IFE. Aseguran que hubo respuestas a las necesidades sociales a través de los aumentos en la Asignación Universal por Hijo y el incremento y la ampliación del Plan Potenciar Trabajo, además de la ampliación del Repro2, entre otros programas.