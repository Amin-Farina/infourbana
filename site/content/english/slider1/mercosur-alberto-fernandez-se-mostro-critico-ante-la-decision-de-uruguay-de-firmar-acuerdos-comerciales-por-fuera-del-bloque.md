+++
author = ""
date = 2021-07-08T03:00:00Z
description = ""
image = "/images/605df202f07e7_1004x565-1.jpg"
image_webp = "/images/605df202f07e7_1004x565.jpg"
title = "MERCOSUR: ALBERTO FERNÁNDEZ SE MOSTRÓ CRÍTICO ANTE LA DECISIÓN DE URUGUAY DE FIRMAR ACUERDOS COMERCIALES POR FUERA DEL BLOQUE "

+++

**_Los presidentes de los países miembros del Mercosur participan este jueves de una nueva cumbre que finalizará con el traspaso de la conducción desde el presidente Alberto Fernández hacia su par de Brasil, Jair Bolsonaro._**

Sin embargo, más allá de lo estrictamente protocolar, el encuentro será clave porque se dará apenas horas después de la decisión de Uruguay de firmar acuerdos comerciales por fuera del bloque.

Uruguay le comunicó este miércoles a sus socios que comenzará a buscar acuerdos comerciales con terceros países, algo que hasta ahora requería consenso de los miembros, en una decisión que sacudió al grupo antes de la cumbre presidencial.

El mandatario argentino fue el encargado de abrir el encuentro y el primero en brindar un discurso, de manera remota, desde Casa Rosada. El Gobierno argentino decidió solo transmitir el discurso del jefe de Estado y cortar la emisión de los mensajes de los otros presidentes.

“Es a través de más integración regional y no de menos integración regional, que estaremos en mejores condiciones de producir, comerciar, negociar y competir”, planteó Alberto Fernández que, aunque no hizo mención puntual al anuncio del gobierno de Luis Lacalle Pou, amplió: “La Argentina reafirma una vez más que ‘nadie se salva solo’ y que un Mercosur de corazón solidario es la nave insignia de su estrategia de integración”.

“Tenemos la responsabilidad histórica de fortalecer el Mercosur frente a la crisis del COVID-19, que aceleró de modo exponencial las transformaciones mundiales en marcha. Se reorganizan las cadenas globales de valor, con un comercio mundial más influenciado por consideraciones geopolíticas y de seguridad nacional. Y se hacen intentos, no siempre exitosos, de revivir el multilateralismo. El resultado de estos procesos no será necesariamente el de una reversión de la globalización, sino el de una economía mundial más regionalizada”, agregó.

En relación al pedido de “flexibilizar” el bloque regional, señaló que “las negociaciones deben iniciarse y concluirse de manera conjunta”, y que “cualquier propuesta debe estar basada en la regla del consenso”.

“Las divisiones de América Latina solo pueden fomentar debilidad, fragmentación, polarización, disgregación de energías comunitarias y fisuras de proyectos grandes y generosos. Solos, alimentaremos el espejismo de una vana prosperidad”, afirmó, al tiempo que destacó la necesidad de una revisión del arancel externo común que contemple “a los sectores más sensibles”, y buscar acuerdos para impulsar a los sectores productivos del Mercosur “ante un mundo más proteccionista y fundamentalmente más agresivo”.

La reacción argentina, en la previa, generó expectativas en particular tras el tenso cruce que tuvieron por este tema los presidentes Luis Lacalle Pou y Alberto Fernández en la cumbre del 26 de marzo.

En ese momento, el mandatario uruguayo sostuvo que el Mercosur no podía “ser un lastre” que impidiera el avance comercial de su país, a lo que Fernández contestó que si Argentina era considerado un lastre, “que tomen otro barco”.

Tres días después, Lacalle insistió con que se le “afloje la piola (cuerda)” a su país para poder negociar con terceros. El mandatario recordó que en Uruguay se habla de la flexibilización del Mercosur “por lo menos desde 2006”, cuando gobernaba Tabaré Vázquez, ex mandatario de izquierda que falleció el año pasado.

Creado hace 30 años, el Mercosur agrupa a más de 300 millones de habitantes y es la quinta economía del mundo, según el Fondo Monetario Internacional (FMI), con un territorio de más de 14 millones de kilómetros cuadrados.