+++
author = ""
date = 2021-04-23T03:00:00Z
description = ""
image = "/images/paula-espanol-2-e1613667007796-1.jpg"
image_webp = "/images/paula-espanol-2-e1613667007796.jpg"
title = "EL GOBIERNO EVALÚA APLICAR UN AUMENTO DE LAS RETENCIONES A LAS EXPORTACIONES"

+++

**_La secretaria de Comercio Interior, Paula Español, reconoció que lo están analizando. “No me va a temblar el pulso para tomar las medidas que hagan falta para cuidar los precios”, dijo_**

La secretaria de Comercio Interior, Paula Español, aseguró que “el aumento de retenciones está en análisis” en un intento por divorciar a los precios internos de los alimentos de los valores internacionales, a cuyo aumento atribuyó las subas de precios de la canasta básica en el país. “Para desacoplar los precios de exportación del precio doméstico hay que tomar medidas como los cupos, las retenciones o las declaraciones juradas”, dijo la funcionaria.

En declaraciones a El Destape Radio, Español se refirió a la marcha de la inflación y el aumento de la canasta básica. “Nos preocupa el dato de que una familia necesite 60.784 para no ser pobre”, dijo y atribuyó los aumentos de precios de alimentos a un fenómeno internacional.

“Hay que entender que no es sólo de la Argentina. En Brasil alimentos subió 3 veces lo que subió el índice común”, comentó.

“Nos preocupa la inflación. Y las presiones en el precio de los alimentos las tiene todo el mundo. (...) Argentina produce alimentos y los exporta. El problema es desacoplar los precios de exportación del precio interno”, consideró.

“Para desacoplar los precios de exportación del precio doméstico hay que tomar medidas como los cupos, las retenciones o las declaraciones juradas (...) El aumento de retenciones está en análisis. Hay 2 elementos que cuando se hizo el presupuesto no tenían esta magnitud: la 2da ola y la suba de los precios internacionales”, agregó.

“Las retenciones son precisamente eso, desacoplar los precios de los niveles internacionales. O un encaje, que hace que se pueda exportar, entren dólares, pero que una parte quede en el país a un precio más razonable”, detalló.

Con todo, la funcionaria dijo que un aumento de las retenciones a las exportaciones es una de un abanico de medidas en consideración.

A pesar del dato de inflación de marzo, del 4,8%, que fuerza a una desaceleración rápida de la inflación para alcanzar el 29% de avance del índice de precios al consumidor previsto en el presupuesto, la titular de Comercio dijo que la pauta oficial se mantiene.

En ese sentido, Español agregó: “La meta de inflación sigue siendo la del presupuesto”.

“El girasol en un año subió 135% en dólares. Lo mismo pasa con la soja y el maíz. Todas esas presiones son mayores, comentó.

Además, la Secretaria se refirió a la implementación del Sistema de Información para la Implementación de Políticas de Reactivación Económica (SIPRE), que fuerza a las empresas a brindar información al Gobierno respecto de su producción, ventas, precios y otras variables, con las que se pretende alcanzar un mayor control de la dinámica de precios.

“Es un sistema nuevo, el SIPRE apuntaba a unas 500 empresas y ya hay arriba de 100 empresas que completaron toda la información. Después está habiendo algunas cuestiones con el sistema informático, pero la información va a estar, pero es el primer mes. Pero es una información que es útil para ver la evolución y sobre todo a lo largo de la cadena”, comentó.

Por último, Español se refirió al aumento de precios de la carne y aseguró estar tomando medidas para intentar frenarlas.

“Estamos trabajando para que frene el aumento de la carne, con el sistema de declaraciones juradas para exportar y otras medidas (...) No me va a temblar el pulso para tomar las medidas que haga falta para cuidar los precios”, agregó.