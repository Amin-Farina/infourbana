+++
author = ""
date = 2021-12-07T03:00:00Z
description = ""
image = "/images/e7272ddf-8cb7-4fc3-a1be-332eb79401b1_16-9-aspect-ratio_default_0-1.jpg"
image_webp = "/images/e7272ddf-8cb7-4fc3-a1be-332eb79401b1_16-9-aspect-ratio_default_0.jpg"
title = "LOUSTEAU PERDERÁ UN CARGO, LO REEMPLAZARÁ CAROLINA LOSADA Y CORNEJO PRESIDIRÁ EL INTERBLOQUE DE JXC"

+++

#### **_Tras la ruptura en Diputados, el líder de Evolución le dejará a la senadora por Santa Fe la vicepresidencia tercera de la Cámara alta. El jefe de la UCR logró el consenso interno para ese cargo y Luis Naidenoff seguirá al frente de la bancada._**

Luego de la tormenta radical en la Cámara de Diputados, que ocasionó la traumática ruptura del bloque, los senadores de la UCR están tratando de evitar las decisiones traumáticas y ya se pusieron de acuerdo en que Luis Naidenoff siga como jefe de la bancada y Alfredo Cornejo, titular del partido hasta el 17 de diciembre, se convierta en el presidente del interbloque de Juntos por el Cambio.

El que perderá un lugar en la estructura del Senado será Martín Lousteau: la vicepresidencia tercera de la Cámara que desempeñaba será para la santafesina Carolina Losada, a quien le reconocerán el triunfo electoral en una provincia clave, aunque algunos lo interpretan como otro gesto de hostilidad del gobernador jujeño Gerardo Morales contra el ex ministro de Economía. Otros aclaran que él cedió el cargo en disconformidad con que Naidenoff continúe al frente del bloque de senadores de la UCR.

Las decisiones se concretarán este jueves en sendos encuentros del bloque radical y del interbloque de JxC, que tendrán lugar tras la sesión en la que jurarán los senadores, que comenzará a las 12.

Para Cornejo, la presidencia del interbloque de JxC se convertirá en un desafío no sólo porque debutará como senador nacional y está a punto de dejar la jefatura del Comité Nacional de la UCR: desde aquel cargo también aspirará a mantener su lugar en la Mesa Nacional de Juntos por el Cambio, que se reunirá la semana próxima para debatir cómo recomponer sus autoridades en la nueva etapa política que se abrió luego del triunfo opositor en las últimas elecciones.

El ex gobernador mendocino mantiene una postura equidistante entre las fracciones radicales en disputa: los cuatro diputados de su provincia (Julio Cobos, Pamela Verasay, Lisandro Nieri y Jimena Latorre) se quedaron en el bloque que lidera Mario Negri luego de la decisión del sector de Lousteau de formar una bancada propia de 12 legisladores conducida por el cordobés Rodrigo De Loredo.

Naidenoff quería retener la jefatura del bloque de la UCR y del interbloque de JxC, aunque aseguran que cedió ante el interés de Cornejo en este último cargo y ante el consenso interno para concretar el cambio. El esquema de tener un mismo titular para ambos puestos se mantenía tanto en Diputados como el Senado para mantener unificadas las jefaturas y no sumar frentes de disidencias internas. Ahora, el desafío de Naidenoff y Cornejo desde mañana será trabajar en sintonía. Ambos mantienen una buena relación y descuentan que encontrarán la forma de coordinar sus responsabilidades.

Entre los senadores del PRO no hubo fisuras internas y fue ratificado el misionero Humberto Schiavoni como jefe del bloque en la Cámara alta. Para la vicepresidencia segunda del Senado, que ejercía la cordobesa Laura Rodríguez Machado (y que asumió como diputada nacional), hay acuerdo para sea designada la porteña Guadalupe Tagliaferri, enrolada en el larretismo.

En la Cámara baja, la fuerte pelea entre Negri y Emiliano Yacobitti, el diputado del sector Evolución que está alineado con Lousteau, hizo que decidiera postergar hasta febrero la elección del jefe del interbloque. Hasta ahora, el puesto también estaba en manos del legislador cordobés, pero quien pretende reemplazarlo es Cristian Ritondo, el ratificado titular del bloque de diputados del PRO.

Con la ruptura radical, el interbloque de Juntos por el Cambio pasará de tener tres bancadas (PRO, UCR y Coalición Cívica) a siete: se suman Evolución, que encabeza De Loredo; el de Emilio Monzó y Margarita Stolbizer y los monobloques de Ricardo López Murphy y de Carlos Zapata, de Salta.

La representación legislativa de JxC en Diputados será un rompecabezas. La UCR tendrá 45 diputados, divididos entre los 33 que responden a Negri y 12 alineados con Losteau, mientras que el bloque del PRO se llamará Frente PRO para incluir legisladores de fuerzas aliadas como Claudio Poggi, de San Luis; Florencia Klipauka Lewtak, de Misiones, y Marcelo Orrego y Susana Laciar, de San Juan, lo que le permitirá a Ritondo conducir una bancada que saltará de 50 a 54 miembros.