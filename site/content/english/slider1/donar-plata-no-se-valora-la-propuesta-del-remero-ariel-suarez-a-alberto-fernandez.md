+++
author = ""
date = 2021-08-25T03:00:00Z
description = ""
image = "/images/mlizmvfqibdjpnmnenxwp35scm.jpg"
image_webp = "/images/mlizmvfqibdjpnmnenxwp35scm.jpg"
title = "\"DONAR PLATA NO SE VALORA\": LA PROPUESTA DEL REMERO ARIEL SUAREZ A ALBERTO FERNÁNDEZ"

+++

**_El remero Ariel Suárez volvió a cargar contra el presidente Alberto Fernández por la fiesta de cumpleaños de Fabiola Yañez en Olivos. El deportista fue noticia hace un año cuando decidió romper el aislamiento obligatorio para ir a entrenar. “¿Por qué ellos y yo no?”, se preguntaba en agosto de 2020 y optó por el camino que le valió críticas y una notificación de Prefectura: agarró su bote y fue a entrenar de todas formas._**

En las últimas horas, Suárez escribió: “Te propongo una cosa, Alberto Fernández, ya que no vas a llegar a ningún juicio político. Hagamos trabajo social con nuestras manos y limpiemos la Pista Nacional de Remo. Vos, yo y todos los que tenemos un acta”. Por último, cerró: “Donar plata no se valora, trabajar sí”.

El mensaje del deportista tiene relación con la decisión de Fernández de donar dinero para compensar por el daño ocasionado por la aparición de la foto. Según el abogado Gregorio Dalbón, Alberto Fernández tiene pensando presentarse ante la Justicia y proponerle al fiscal Ramiro González realizar una donación a modo de reparación a una entidad de bien público. Aún no hay fecha para que esto ocurra.

Suárez había ironizado hace dos semanas sobre el cumpleaños VIP de Fabiola Yañez, que contó con 12 invitados en plena pandemia y cuando todavía regía la prohibición de circulación. La foto que se filtro, en la que aparece Alberto Fernández, generó una fuerte grieta en la escena de la política argentina, con amplias críticas desde la oposición y poca autocrítica desde el oficialismo.

El remero había tuiteado: “Señor @alferdez quiero contarle que no fue culpa mía el salir a remar, fue mi bote el culpable que quería refrescarse y mojarse en nuestro hermoso Delta. Lamento hacerle caso a mi bote”. Es que el presidente responsabilizó a su pareja, Yañez, ya que ella “fue la que convocó a sus amigos para un brindis”. En las últimas horas volvió a utilizar su cuenta de Twitter para hacerle llegar a Alberto Fernández su malestar.

Suárez, finalista olímpico en Londres 2012 y mundial en Hamburgo 2011, además de campeón panamericano en Lima 2019, cumplió con su aviso y el 11 de agosto de 2020 volvió a entrenarse en el Río Lujan, en Tigre, pese a no tener la autorización para hacerlo, y por eso se le labró un acta contravencional. Sin embargo, avisó que hoy volverá a hacerlo porque “es insólito lo que está pasando”.

“Me cansé de esperar. El remo ya tiene el protocolo aprobado y no hay ningún riesgo de contagio porque entrenamos solos. Yo estoy con mi bote y nada más. No entiendo por qué los futbolistas pueden entrenarse y yo no. Me siento discriminado y por eso vuelvo a entrenarme”, dijo el deportista en declaraciones a TyC Sports. En aquel entonces el remero de 40 años fue una de las tendencias en redes sociales.

Un día después, se oficializó hoy mediante una Decisión Administrativa la extensión de la excepción al cumplimiento del “aislamiento social, preventivo y obligatorio” y de la prohibición de circular a los atletas argentinos miembros de la Selección Argentina de Rugby Los Pumas como ya lo había hecho con los atletas clasificados a Tokio 2021 o que buscan su plaza y con los planteles de Primera División del fútbol argentino.