+++
author = ""
date = 2022-02-02T03:00:00Z
description = ""
image = "/images/ddc131021-009f01-1.jpg"
image_webp = "/images/ddc131021-009f01.jpg"
title = "ANÍBAL FERNÁNDEZ CRITICÓ A MÁXIMO KIRCHNER: “SI NO TE GUSTA, ESPEREMOS A QUE VOS SEAS PRESIDENTE”"

+++

##### **_El ministro de Seguridad Aníbal Fernández criticó duramente a Máximo Kirchner por su decisión de renunciar a la presidencia del bloque del Frente de Todos en la Cámara de Diputados, en rechazo por el acuerdo con el FMI_**

“Si no te gusta, esperemos a que vos seas presidente, qué querés que te diga. Lo único que falta es que yo le diga al Presidente que no lo acompaño porque no hace lo que yo quiero”, lanzó.

##### Sorpresa en la Casa Rosada por la renuncia de Máximo Kirchner

En declaraciones televisivas, el funcionario nacional manifestó su “sorpresa” por la renuncia del hijo de la vicepresidenta a la presidencia del bloque del Frente de Todos.

“Es un dirigente muy importante en la línea de nuestro movimiento político. Si es lo que él ha entendido que tiene que llevar a la práctica... Lo dijo el Presidente ayer, tiene que resolverlo como lo resolvió en 24 horas para seguir con una estrategia común que comprenda a todos, también a Máximo”, expresó al canal C5N. alberto Fernández finalmente nombró a Germán Martínez para reemplazar a Máximo.

Aníbal Fernández le restó valor a la carta de Máximo Kirchner, en la que el diputado criticaba al Presidente y al ministro de Economía Martín Guzmán por el acuerdo con el FMI. “La importancia de la discusión que se está dando en este momento, la que tiene a la Argentina en vilo, se tiene que dar en los más altos niveles”, afirmó.

“Para los que venimos del peronismo, primero está la Patria y si suponemos que la Patria está en riesgo, lo primero que se va a hacer es sumarse a una estrategia que está llevando adelante el Presidente. No puede haber cuarenta cabezas acá, hay una sola”, criticó a Kirchner.

Luego, el ministro de Seguridad se dirigió directamente a Máximo: “Si no te gusta, esperemos a que vos seas presidente, qué queres que te diga. Lo único que falta es que yo le diga al Presidente que no lo acompaño porque no hace lo que yo quiero”.

Aníbal también recalcó que el Presidente es quien debe tener la última palabra en el debate. “Hay un presidente de la Nación, elegido por la voluntad popular que es el que tiene que hacerse cargo de dar vuelta esa situación en la que nos encontramos que es impagable. Esa situación a discutir y poner sobre la marcha la tiene que dar el Presidente. Si se ponen de acuerdo, no se ponen de acuerdo, todo tiene un límite, cuando no se pueden poner de acuerdo, la decisión la toma el Presidente”, argumentó.

##### ¿Qué había dicho Máximo Kirchner sobre el acuerdo con el FMI?

Máximo Kirchner planteó al anunciar su renuncia a la jefatura de bloque del Frente del Todos en Diputados que no compartía “la estrategia utilizada y mucho menos los resultados obtenidos en la negociación con el FMI”.

El hijo de la vicepresidenta Cristina Kirchner dijo en una carta que las gestiones del Gobierno con el organismo multilateral fueron “llevadas adelante exclusivamente por el gabinete económico y el grupo negociador que responde y cuenta con la absoluta confianza del Presidente de la Nación”.

Máximo Kirchner dijo que a Alberto Fernández nunca dejó de expresarle su “visión para no llegar a este resultado”, en relación al entendimiento con el FMI.

Miguel Ángel Pesce advirtió que la renuncia de Máximo Kirchner “tendrá consecuencias” en la economía

Aníbal Fernández no fue el único en hablar sobre la renuncia de Máximo Kirchner. El titular del Banco Central, Miguel Ángel Pesce, advirtió que “tendrá consecuencias” en la economía.

Pesce calificó como “un buen acuerdo” al entendimiento del Gobierno con el Fondo Monetario Internacional (FMI) por la deuda de US$45 mil millones con el organismo multilateral. Consideró que el acuerdo “va a ser entendido de esa manera y va a ser aprobado por el Congreso”.

El titular dle Banco Central dijo que el acuerdo con el FMI “no prevé un ajuste muy duro de la economía” y que “no está previsto ningún salto devaluatorio”: “La idea es sostener el tipo de cambio real en los términos que está ahora, que permite las exportaciones y es competitivo”.