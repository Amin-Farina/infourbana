+++
author = ""
date = 2021-10-19T03:00:00Z
description = ""
image = "/images/bolson-conflicto-mapuche-cuesta-del-ternero-1.jpg"
image_webp = "/images/bolson-conflicto-mapuche-cuesta-del-ternero.jpg"
title = "EL GOBIERNO ENVIARÁ FUERZAS FEDERALES A RÍO NEGRO PARA SOLUCIONAR EL CONFLICTO CON LOS MAPUCHES EN EL BOLSÓN"

+++

### **_Son efectivos de Gendarmería, PFA y PSA. Era un pedido de la gobernadora Arabela Carreras tras el ataque incendiario contra el club Andino Piltriquitrón._**

El Ministerio de Seguridad de la Nación decidió asignar fuerzas federales -efectivos de Gendarmería, Policía Federal (PFA) y Policía de Seguridad Aeroportuaria (PSA)- a la provincia de Río Negro, donde este miércoles hubo un nuevo ataque de mapuches contra el club Andino Piltriquitrón.

Según informaron fuentes de la cartera que conduce Aníbal Fernández, se redesplegarán fuerzas federales en la zona sur de la provincia “con el objetivo de prevenir posibles conflictos” y “actos de vandalismo”. De acuerdo al gobierno, no se trata de un envío de tropas “a demanda”, sino que “se está cumpliendo con la Ley de Seguridad Interior”.

El envío de fuerzas federales era un pedido de la gobernadora local, Arabela Carreras, quien dijo estar “preocupada” por el tercer ataque mapuche en el último mes.

“Aquí no hay una actitud de solicitud de mi parte por una necesidad de acompañamiento. Acá estamos exigiendo que cada jurisdicción actúe en el ámbito de su competencia. Es lo que corresponde. No estoy pidiendo un favor. Estoy exigiendo que las competencias se ejerzan como corresponde, nada más”, dijo Carreras durante una entrevista con CNN Radio.

La ministra de Seguridad provincial, Bettiana Minor, ya había hecho el reclamo a su par nacional semanas atrás. Sin embargo, Aníbal Fernández se negó inicialmente a colaborar y pidió que haya diálogo entre las partes.

Carreras se encontraba esta mañana “redactando el mismo pedido” para enviar al presidente Alberto Fernández “sobre todo para actúe en los ámbitos de su competencia, por ejemplo, las rutas nacionales”. En concreto, Carreras pedía refuerzos de la Gendarmería Nacional.

##### El ataque incendiario contra el Club Andino Piltriquitrón

El edificio del Club Andino Piltriquitrón fue destrozado esta madrugada por las llamas. En los alrededores la policía encontró panfletos con mensajes contra la gobernadora Arabela Carreras y el intendente Bruno Pogliano, así como el empresario británico Joe Lewis y la familia Benetton.

El fuego se desató alrededor de las 4 de la mañana en la sede del club con más de 60 años de historia y pese a los esfuerzos de los Bomberos Voluntarios de El Bolsón las pérdidas fueron totales.

Según medios locales, en la zona se encontraron dos bidones con combustible, además de panfletos impresos que decían “Benetton, Lewis, Arabela, Pogliano, el agua y la tierra no se venden, se defienden”.

En Twitter, Carreras confirmó que se trató de un incendio intencional y que desde el gobierno provincial realizarán una nueva denuncia ante la justicia federal. La mandataria expresó su “más enérgico repudio contra el ataque incendiario al Club Andino Piltriquitrón de El Bolsón”. “No vamos a avalar ningún tipo de violencia en Río Negro, que ponga en peligro la vida de las personas y destruya las instituciones”, escribió.