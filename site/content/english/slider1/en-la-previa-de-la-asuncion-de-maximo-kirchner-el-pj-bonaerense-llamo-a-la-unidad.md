+++
author = ""
date = 2021-12-16T03:00:00Z
description = ""
image = "/images/fgsmlziwqaiqqzh.jpg"
image_webp = "/images/fgsmlziwqaiqqzh.jpg"
title = "EN LA PREVIA DE LA ASUNCIÓN DE MÁXIMO KIRCHNER, EL PJ BONAERENSE LLAMÓ A LA UNIDAD"

+++

#### **_Tras el encuentro, el actual presidente de esa fuerza a nivel provincial, el intendente Gustavo Menéndez, afirmó que el peronismo está "unido para seguir fortaleciendo al Frente de Todos y trabajar en políticas que incluyan a todos los argentinos"_**

El Partido Justicialista bonaerense realizó el miércoles un fuerte llamado a la unidad durante un congreso del que participaron ministros nacionales, provinciales, intendentes y legisladores, en la previa de la asunción el sábado de Máximo Kirchner como titular de ese espacio.

Tras el encuentro, que se desarrolló en la sede justicialista de Matheu 130, el actual presidente de esa fuerza a nivel provincial, el intendente Gustavo Menéndez, afirmó que el peronismo está "unido para seguir fortaleciendo al Frente de Todos y trabajar en políticas que incluyan a todos los argentinos".

"Este consejo del partido va a garantizar que el partido justicialista sea mucho más grande de lo que es hoy, porque la República Argentina necesita un peronismo unido, amplio, consolidado y organizado", publicó Menéndez en su cuenta de Twitter.

Además de Menéndez, participaron del encuentro los ministros del Interior, Eduardo 'Wado' De Pedro; y de Desarrollo Social, Juan Zabaleta; el intendente de La Matanza, Fernando Espinoza; la vicegobernadora bonaerense, Verónica Magario; la intendenta de Cañuelas, Marisa Fassi; la jefa comunal de Quilmes, Mayra Mendoza; la ministra de Gobierno bonaerense, Cristina Álvarez Rodríguez y el diputado Mariano Cascallares, entre otros.

En el encuentro, el peronismo aprobó además la constitución de la comisión de poderes y la confirmación de la decisión de la junta electoral partidaria de prorrogar los mandatos de los consejos distritales, que elegirán sus autoridades en marzo.

La reunión del miércoles se produce en la previa del acto que se realizará el sábado en la Quinta de San Vicente, donde descansan los restos de Juan Domingo Perón, durante el cual asumirá como presidente del partido en el distrito bonaerense el jefe del bloque de diputados del Frente de Todos y referente de La Cámpora, Máximo Kirchner.

En un posteo en redes sociales, Menéndez -intendente de Merlo y actual titular partidario- indicó tras el cónclave: "Realizamos el Congreso del Partido Justicialista de la provincia de Buenos Aires junto a compañeros y compañeras congresales de los 135 municipios".

El jefe comunal sostuvo que el peronismo está "unido para seguir fortaleciendo al Frente de Todos y trabajar en políticas que incluyan a todos los argentinos".

En tanto, la ministra de Gobierno bonaerense, Cristina Alvarez Rodríguez, posteó en su cuenta de Twitter: "Participé del Congreso del Partido Justicialista de la provincia de Buenos Aires junto a compañeros y compañeras congresales de los 135 municipios" y agregó: "Estamos fortaleciendo la participación democrática de nuestro partido para seguir defendiendo los intereses del pueblo".

En la misma línea, Mendoza señaló: "Participé de la sesión del PJ" y analizó que "estos espacios enriquecen el trabajo militante y nos permiten profundizar aspectos centrales de nuestro partido nacional, popular y cercano a las demandas del pueblo".

"Los días más felices fueron son y serán los días peronistas", cerró.

Por su parte, la intendenta de Cañuelas, Marisa Fassi, contó en la red social Twitter: "Junto a grandes compañeros y compañeras, encabecé el Congreso extraordinario de PJ bonaerense".

"Seguimos fortaleciendo la unidad del partido. Nuestro compromiso es continuar trabajando para volver a llevar felicidad a nuestro pueblo, con políticas que incluyan a todos y todas", dijo.