+++
author = ""
date = 2021-09-29T03:00:00Z
description = ""
image = "/images/sujarchuk-1.jpg"
image_webp = "/images/sujarchuk.jpg"
title = "SUJARCHUK APUNTÓ CONTRA KICILLOF POR LA DERROTA EN LAS PASO"

+++

#### **_El intendente de Escobar blanqueó su malestar con el rumbo de la gestión provincial y nacional. "No hay manera de revertir la derrota", admitió._**

Ariel Sujarchuk venía dando señales de su malestar con el gobierno de Axel Kicillof y hasta se especulaba con una ruptura con el Frente de Todos. El intendente de Escobar finalmente blanqueó su enojo y apuntó al gobernador por la derrota en las PASO, blanqueando un malestar de un sector de los intendentes que anticipó LPO y forzó los cambios del gabinete provincial.

Tras varios días de generar expectativa, Sujarchuk finalmente rompió el silencio y, aunque aseguró que no piensa romper con el oficialismo, hizo un durísimo diagnóstico sobre el rumbo de las gestiones nacional y bonaerense.

"Me decidí a hacer público algo que vengo sintiendo con tristeza desde hace tiempo. Y no soy el único que lo piensa, sino todo lo contrario. La mayoría comparte mi visión", dijo el intendente, que eligió blanquear sus críticas en una entrevista con el diario Clarín, lo que seguramente no pasa desapercibido en el oficialismo.

Sujarchuk apuntó contra Kicillof y su gabinete por no atender las sugerencias y pedidos de los jefes comunales durante los meses anteriores a la elección. "Inclusive casi no hizo campaña. Se lo vio hasta guardado antes de las PASO", cuestionó. "El gobernador se encerró junto a su anterior Gabinete, muchos de los cuales siguen en otros puestos de la administración. A nosotros nos oía pero no nos escuchaba", agregó.

El escobarense también afirmó que -como reveló LPO en su momento- los cambios en el gabinete bonaerense "se los impusieron (a Kicillof) los intendentes y dirigentes que viajaron a El Calafate para lograr que Cristina intercediera". "Está claro que el gobernador resistió modificar la estructura de su gobierno. Hay que ver si realmente aceptará la incidencia de los que se sumaron para tratar de reencauzar el rumbo", agregó.

"Me parece que lo que Cristina y Máximo Kirchner se dieron cuenta es que si no se intentaba un cambio de timón, se le venía una rebeldía de los intendentes del GBA", declaró Sujarchuk, que insistió en que sus críticas son compartidas por otros pares del peronismo.

Sujarchuk también blanqueó la bronca de los intendentes bonaerenses con los nombramientos en el gabinete nacional de Aníbal Fernández y Julián Domínguez, como reveló este medio. Aunque no los mencionó directamente, habló de funcionarios "que perdieron su elección" y no generan empatía. "Confunden hasta a nuestros votantes, que no sienten expectativas que lo que se hizo mal vaya a cambiar", señaló.

"No veo vocación de cambiar en serio ni valentía de promover una verdadera renovación para interpretar las demandas de la gente, que son muy claras y no pueden esperar. Estuvimos una semana paralizados para hacer cambios transitorios de un Gabinete nacional con ahora funcionarios que perdieron su elección", lamentó.

Finalmente, Sujarchuk dijo que "no hay manera de revertir en noviembre la derrota de las PASO". "Debemos ya ponernos a pensar cómo refundar el gobierno y recuperar las bases por las cuales nos eligieron", completó.