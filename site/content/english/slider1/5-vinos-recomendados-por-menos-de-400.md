+++
author = ""
date = 2021-05-24T03:00:00Z
description = ""
image = "/images/que-son-vinos-largos-864x576-1.jpg"
image_webp = "/images/que-son-vinos-largos-864x576-1.jpg"
title = "5 VINOS RECOMENDADOS POR MENOS DE $400"

+++

Poco a poco, vamos apreciando cómo se incrementa la calidad de los vinos de precio más bajo, no solo por la experiencia que dan las cosechas, sino también por la tecnificación de las bodegas. Esto hace posible que lleguen a las mesas etiquetas frescas y modernas hechas con uvas de distintas variedades. Lo importante es que sean agradables de beber, ligeros al paladar, con la justa graduación alcohólica y notas a fruta fáciles de reconocer.

La industria, hoy, sigue buscando nuevos recursos para ganar consumidores y los varietales de media gama, sobre todo los que están en la franja de los $200 y $600, muestran una creciente versatilidad. Con tan amplia oferta en el mercado, es bueno saber cuáles son los de mejor relación precio-calidad.

#### Rincón Famoso Tinto 2016

###### Bodega López, Maipú, Mendoza, $380

Para convertirse en un clásico hay que demostrar consistencia en el tiempo y el Rincón Famoso lo ha logrado. Es un tinto nacional que no pasa de moda, fresco y frutal, con un toque ahumado, como rasgos más perceptibles. Elaborado con Sangiovese, Merlot y Malbec de Maipú, y añejado durante 3 años en toneles de roble francés, es suave al paladar y de sabor persistente. Muy recomendable para acompañar menús donde las carnes predominen.

#### Abrasado Blend de Parcelas Malbec 2019

###### Bodega Familia Millán, Mendoza, $400

Refrescante y de carácter informal, este Malbec es un vino para disfrutar todos los días. Una versión moderna y ágil de este varietal con nariz frutada, paladar tenso, taninos dulces y electricidad, pero al mismo tiempo sin astringencias. Rico de principio a fin y muy agradable de beber. Sobresale por su cuerpo medio y delgado paso por boca. Si salen a cenar, no dejen de probarlo acompañado de carnes blancas poco condimentadas.

#### Altos del Plata Syrah 2019

###### Bodega Terrazas, Mendoza, $386

De antiguas fincas mendocinas, nace este Syrah distinto, de perfil fresco y frutado, con boca elegante, acidez vibrante, textura tersa y largo final. Su fuerte es la sutileza más que el impacto, tiene taninos pulidos, y los 6 meses de crianza (el 70% del corte) le aportaron volumen y suaves notas a roble que suman armonía sin perder frescura. Está en un gran momento para beber y es ideal para llevar a casa de amigos una tarde de sábado. No falla.

#### Latitud 33 Malbec 2020

###### Bodega Latitud 33, Mendoza, $315

Latitud 33 empieza a recuperar terreno y este Malbec es un ejemplo de un tinto de estilo bien argentino. Digno y expresivo, con aromas a frutas rojas y dejos especiados de su paso por madera, fresco al paladar, con un dejo mineral y buena profundidad hacia el final. A pesar de ser muy joven, puede beberse por su textura suave, taninos dulces y acidez justa. Compañero ideal de platos clásicos como pastel de papas, carne al horno y asados.

#### Estiba I Cabernet Sauvignon 2020

###### Bodega Esmeralda, Mendoza, $231

Típico Cabernet frutado y fácil de beber, con recuerdos de finas hierbas y un toque ahumado, como rasgos más perceptibles. Elaborado con uvas de Agrelo y una corta crianza en roble americano y francés, destaca su paladar amable, taninos finos y buen volumen en el marco de un final largo y redondo. Su fórmula es simple: muy buen vino a un precio muy accesible. Ideal para tenerlo siempre a mano y adoptarlo en la mesa de todos los días.