+++
author = ""
date = 2021-07-12T03:00:00Z
description = ""
image = "/images/a24tqpmwlzgsvan5kmxgwphc44.jpg"
image_webp = "/images/a24tqpmwlzgsvan5kmxgwphc44.jpg"
title = "SANTA CRUZ: NUEVA LÍNEA DE ALTA TENSIÓN EN CALETA OLIVIA"

+++
**_La gobernadora de la provincia de Santa Cruz, Alicia Kirchner, y el presidente de YPF, Pablo González, inauguraron la línea de alta tensión Santa Cruz Norte, en Caleta Olivia._**

Según un comunicado, la misma “mejora la calidad del suministro de energía eléctrica y brinda la energía necesaria para abastecer la planta potabilizadora de agua” y permitirá conectar al Parque Cañadón León, que YPF Luz construye en la provincia, a la red nacional de interconexión.

“Esta obra es el fruto de una política de Estado, lo que tuvimos fue una visión a mediano y largo plazo, una decisión de avanzar en buscar el desarrollo para Caleta Olivia que tanto lo necesita. Tenemos el compromiso de seguir acompañando ese crecimiento”, dijo afirmó González.

“Es un orgullo para mí como santacruceña que sea con YPF Luz, que sea con Pablo González, un hombre de esta tierra, con quien pongamos en marcha esta línea. En este momento, estamos celebrando que los santacruceños cuando queremos podemos hacer las cosas. Siempre vamos a apostar a la esperanza”, agregó la gobernadora.

“La obra, que se comenzó a construir en 2019 mediante un acuerdo entre YPF Luz y el gobierno provincial, implicó el tendido eléctrico de 53 kilómetros entre Pico Truncado y Caleta Olivia, con 257 estructuras de hormigón y 2 subestaciones eléctricas ampliadas y generó empleo para 250 trabajadores y trabajadoras”, se explicó.

Además, como parte del programa Brecha Digital, la Fundación YPF donó más de 800 notebooks a estudiantes secundarios de 1° año de todas las escuelas técnicas de Caleta Olivia, Pico Truncado, Las Heras y Puerto Deseado.

En el acto estuvieron presentes Federico Basualdo, subsecretario de Energía Eléctrica de la Nación; Leandro Zuliani, ministro de Gobierno de la provincia; Ignacio Perincioli, ministro de Economía provincial; Nelson Gleadell, presidente de Servicios Públicos Sociedad del Estado; Cecilia Vázquez, presidenta del Consejo de Educación provincial; y Luis Barletta, presidente del CAFF. 

Siguieron el acto vía streaming el secretario de Energía de la Nación, Darío Martínez, el intendente de Caleta Olivia, Fernando Cotillo, el intendente de Pico Truncado, Osvaldo Maimo entre otros funcionarios y otras funcionarias nacionales y provinciales. Por YPF, participaron Sergio Affronti, CEO de la compañía; Martin Mandarano, CEO de YPF LUZ.