+++
author = ""
date = 2021-05-03T03:00:00Z
description = ""
image = "/images/cropped-tractor.jpg"
image_webp = "/images/cropped-tractor.jpg"
title = "PARA EL AGRO EXISTE UNA \"EXPECTATIVA FUERTE DE DESARROLLAR OTROS MERCADOS\""

+++

**_El presidente de la Federación Argentina del Citrus (Federcitrus), José Carbonell, afirmó este domingo que en el sector empresario existe "una expectativa muy fuerte de ir desarrollando mercados" y auguró que este será "un año de consolidación" de los destinos que se abrieron durante 2020, como China._**

En una entrevista con la Agencia de Noticias TELAM, Carbonell saludó la reapertura del Viejo Continente para lo cítricos argentinos, si bien consideró que los requisitos impuestos por la Unión Europea (UE) para reabrir su mercado son de "duro cumplimiento" y que podrían limitar "un poco la exportación".

\-Télam: ¿Qué expectativas productivas tienen para esta campaña de cítricos?

\-José Carbonell: La cosecha se inició primero en mandarina, luego en naranjas tempranas y la de limón hace poco tiempo. La campaña de exportación, que ya se inició con envíos a Rusia y Hong Kong, va a tomar fuerza la semana que viene. Las expectativas de producción de limón son a la baja por cuestiones climáticas de, por lo menos, entre un 10% o 15% hasta los 1,1 millones de toneladas. Hemos recuperado el mercado europeo, pero con una serie de requisitos e imposiciones que van a limitar un poco la exportación. Confiamos en el desarrollo de otros mercados.

En naranjas confiamos que puede haber una pequeña suba en la producción, con buenas expectativas de exportación por buenos precios y en mandarina confiamos en una campaña similar a la del año pasado.

\-T: ¿Cuáles son los requisitos impuestos por la UE? ¿La mayoría del sector está en condiciones de cumplirlos?

\-JC: Están inscriptas para exportar a la UE un 30% menos de superficie o unidades productivas que el promedio de los últimos años. Desde ya hay productores y exportadores que han decidido no exportar, esto mucho antes de que se conocieran los requisitos. Los problemas nuestros con Europa derivan de una fungosis que se llama mancha negra, que tiene la particularidad que a veces es indetectable al ojo humano o las máquinas clasificadores y desarrolla su latencia durante el viaje y recién aparece cuando llega a destino. Es un tema que evidentemente está enarbolado por la citricultura española como una barrera para arancelaria porque Argentina y los demás exportadores del hemisferio sur tienen esta fungosis y a lo largo de tanto años de exportar jamás se transmitió por frutos (a su producción).

Los requisitos son de duro cumplimiento porque van a suponer aumento de los costos, suponen inspecciones en campo, empaques, en puertos de origen y una parte que resulta especialmente dura es que en el memento que fuera la detección de mancha negra u otra enfermedad cuarentenaria caen inmediatamente toda la fruta proveniente de esa unidad productiva y eso involucra a fruta que puede estar viajando en los barcos presta a llegar a destino. Es una condición muy dura, muy riesgosa y cuya consecuencia económica puede ser muy grave.

\-T: Ante una eventual presión española, ¿temen que se puede volver a dar un cierre?

\-JC: Este año vamos a tener otros recaudos en cuanto a requerir los exámenes de PCR, que es el único criterio válido para determinar la existencia de una enfermedad cuarentenaria. Tenemos una fuerte prevención con España. Si actuaron del modo que actuaron el año pasado, este año con un precio más bajo en el limón es de esperar que redoblen sus esfuerzos. Ya hay entidades que requirieron a Bruselas medidas más duras en caso de que haya detecciones para Argentina.

De acá al futuro y para preservar los volúmenes historicismos que representaba la UE para nosotros, estamos empezando trámites con Cancillería para intentar generar un nuevo protocolo para que los países miembro se segmenten, como pasó en Estados Unidos, que había estados que recibían el limón, mientras que los estados productores no.

O sea, los países nórdicos, Bélgica o Alemania, por ejemplo, no tienen ninguna preocupación propia en ese tipo de enfermedades cuarentenarias porque no tienen citrus. Es un desafío para el sector público y privado llevar adelante un proceso para establecer nuevos protocolos.

\-T: Ante esta situación, ¿Argentina ha ido por otros mercados? ¿Hay un plan para conseguirlos?

\-JC: Siempre hay. Hemos tenido en estos últimos años dos grandes aperturas, como Estados Unidos y China. También acaba de abrirse Vietnam, reabrirse Indonesia y estamos buscando la apertura del mercado chileno. Queda por desarrollar el mercado mexicano, el de India. Hay una expectativa muy fuerte de ir desarrollando otros mercados, pero cuesta mucho abrirlos y cuesta tiempo desarrollarlos. Creemos que este año va a ser de consolidación de los mercados que se abrieron el año pasado.

\-T: ¿Cómo está afectando a la actividad el conflicto con cortes de rutas en Tucumán?

\-JC: Que quede claro que no hay un conflicto con la Unión de Trabajadores Rurales y Estibadores (Uatre). Uatre y la Asociación Citrícola del Nororeste Argentino (Acnoa) acordaron una paritaria que se cerró hace más de un mes con un aumento del 40,6% que fue homologada por el Ministerio de Trabajo. Hay un sector con una persona que es un delegado seccional de Uatre de Tucumán que, aparentemente, estaría apoyando a un sector de supuestos trabajadores cosecheros del limón que han venido cortando rutas y accesos a fábricas durante la semana pasada. Hay un tufo también de conflicto político por disputas del partido oficialista. A causa de esto la cosecha estuvo prácticamente paralizada durante la semana, con falta de frutas en empaques y fábricas.