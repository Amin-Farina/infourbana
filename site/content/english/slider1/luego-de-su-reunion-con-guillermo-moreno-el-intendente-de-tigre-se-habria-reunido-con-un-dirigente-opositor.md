+++
author = ""
date = 2021-05-07T03:00:00Z
description = ""
draft = true
image = "/images/602e80082e295_600_315.jpg"
image_webp = "/images/602e80082e295_600_315.jpg"
title = "LUEGO DE SU REUNIÓN CON GUILLERMO MORENO, EL INTENDENTE DE TIGRE SE HABRÍA REUNIDO CON UN DIRIGENTE OPOSITOR"

+++
**_El intendente de Tigre difundió el encuentro en con el ex secretario de Comercio Interior quien busca avanzar con un armado en el Conurbano._** 

El intendente de Tigre, Julio Zamora, no oculta su acercamiento a Guillermo Moreno, una de las voces más críticas al Presidente dentro del universo peronista y que busca avanzar con un armado en el Conurbano.

El miércoles, Zamora se reunió con Moreno en su despacho de Tigre y hasta hubo foto del encuentro que el intendente se ocupó de difundir. "Es un placer recibir a Guillermo y escuchar su experiencia de gobierno", escribió el intendente en su cuenta de Twitter.

Afirman además que Zamora mantuvo reuniones incluso con dirigentes opositores como Nicolás Massot, que busca avanzar en un armado en Tigre. Según explican, son instancias de diálogo.

##### ACTO POR LA UNIDAD EN ENSENADA

Desde el Frente de Todos dejaron trascender que Zamora se retiró antes del acto de Ensenada donde Alberto Fernández, Cristina Kirchner y Sergio Massa mostraron una foto de unidad. Sin embargo, desde Tigre niegan esa versión y afirman que Zamora estuvo durante todo el acto en Ensenada.

Lo que si se sabe es que el intendente y el ex funcionario mantuvieron una extensa charla en la que analizaron la situación del país de cara a las próximas elecciones.

Respecto de la reunión con Moreno, aseguran que el dirigente es presidente de un partido político y a través de un dirigente de Tigre le pidió a Zamora una reunión para conversar de política a la que el intendente aceptó.