+++
author = ""
date = 2022-01-06T03:00:00Z
description = ""
image = "/images/1cb1d44a6c25568d7386d0e68ce98aa8-1.jpg"
image_webp = "/images/1cb1d44a6c25568d7386d0e68ce98aa8.jpg"
title = "ASUME EL MASSISTA JORGE D'ONOFRIO COMO MINISTRO DE TRANSPORTE BONAERENSE"

+++

#### **_El gobernador bonaerense, Axel Kicillof, tomará juramento este jueves al nuevo ministro de Transporte de la provincia de Buenos Aires, Jorge D´Onofrio, se informó oficialmente._**

El acto tendrá lugar desde las 11.30 en el Salón Dorado de la Casa de Gobierno, en La Plata, y se estima que participarán los restantes miembros del gabinete.

D’Onofrio es parte del Frente Renovador, la fuerza política que lidera el presidente de la Cámara de Diputados nacional, Sergio Massa, desde su creación, en 2010.

Abogado, fue senador y diputado por ese espacio en la Legislatura bonaerense, y también se postuló a la intendencia de Pilar.

Como legislador se dedicó, sobre todo, a temas relacionados con la seguridad y la justicia.

Ahora, en el marco del plan "Seis por Seis" según el cual, tras seis años de crisis y pérdidas generadas por los cuatro años de macrismo y los dos de la pandemia, hacen falta seis años más "de reconstrucción de la provincia", Kicillof realizó modificaciones en su equipo.

Además de D'Onofrio, sumó al gabinete al kirchnerista Alberto Sileoni como director general de Cultura y Educación (su antecesora, Agustina Vila, pasó a ser la Secretaría General); y a las camporistas Daniela Vilar como ministra de Ambiente y Florencia Saintout como titular del Instituto Cultura.