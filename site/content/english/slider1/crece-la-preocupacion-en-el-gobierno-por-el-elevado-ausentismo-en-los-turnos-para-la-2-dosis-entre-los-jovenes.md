+++
author = ""
date = 2021-11-03T03:00:00Z
description = ""
image = "/images/vacunacion_-_jovenes_madrid-width-1920.jpg"
image_webp = "/images/vacunacion_-_jovenes_madrid-width-1920.jpg"
title = "CRECE LA PREOCUPACIÓN EN EL GOBIERNO POR EL ELEVADO AUSENTISMO EN LOS TURNOS PARA LA 2° DOSIS ENTRE LOS JÓVENES"

+++

##### **_Se estima que alrededor del 40% de los argentinos entre 18 y 40 años no se presentaron a vacunarse con la segunda dosis de la vacuna Covid-19._**

Desde hace semanas, un dato preocupa a la ministra de Salud de la Nación Carla Vizzotti y a sus funcionarios de más confianza: buena parte de las personas de entre 18 y 40 años no completan sus esquemas de vacunación en medio del avance de la variante Delta de coronavirus, el linaje más peligroso de todos.

“Vemos un alto nivel de ausentismo en ese grupo etario. Entre un 30 y 40 por ciento de los que tienen entre 18 y 40 años no se aplicó la segunda dosis de la vacuna”, comentó un alto funcionario de la cartera de Salud.

Uno de los últimos informes oficiales marca que el 75,4% de la población ya inició su esquema de vacunación mientras que el 56,7% completó sus esquemas. Por estos días el problema no radica ni en los adolescentes ni niños (grupos en los que la inoculación avanza a buen ritmo) sino en los mayores de 18 años que deciden no completar su esquema.

“Persiste en la sociedad una percepción de que bajó el riesgo, que la pandemia ya pasó y como tienen una dosis no van a buscar la segunda”, agrega la misma fuente.

Durante la reunión del Consejo Federal de Salud (Cofesa) que se realizó en Mar del Plata en septiembre pasado, la ministra Vizzotti le solicitó a sus pares de la Ciudad de Buenos Aires y las provincias que redoblen los esfuerzos para que estos ciudadanos completen sus esquemas.

“El consenso (con las provincias) es avanzar hacia el mayor número de personas con al menos una dosis e ir casa por casa, duda por duda, con las personas que aún no se han inscripto”, dijo en conferencia de prensa.

De acuerdo a la última actualización del Monitor Público de Vacunación, 34.543.451 personas ya iniciaron su esquema de vacunación pero solo 26.209.203 completó su esquema. Es decir, aún restan aplicar 8,3 millones para completar estos esquemas. En tanto ya hay 116.348 personas que tuvieron una dosis adicional, es decir, una tercera aplicación.

Hasta el momento, y siempre según las cifras oficiales, en las personas que tienen entre 18 y 39 años sin factores de riesgo se aplicaron 13.911.696 dosis. Los otros rangos etarios que aún restan vacunar son los menores de edad y adolescentes pero en ambos casos la inoculación avanza a buen ritmo. Ya se colocaron 209.590 en los chicos de entre 3 y 11 años con factores de riesgo; 2.110.862 en los que no tienen comorbilidades; 2.493.280 en los adolescentes sin riesgo y 890.554 en los que sí presentan comorbilidades.

“Los más jóvenes tienen el incentivo de tener la vacuna para poder ir a bailar, entrar a los estadios de fútbol y hacer una vida lo más normal que se pueda”, recuerda otra fuente de Salud que monitorea la realidad epidemiológica del país.

Aunque se desconocen las cifras exactas, la Argentina tiene un importante stock de vacunas Sputnik V, AstraZeneca y Pfizer para cubrir a estos sectores que restan vacunar.

En las últimas semanas el Ministerio de Salud de la Nación también inició la campaña de vacunación con las dosis de CanSino, el suero monodosis que posee la Argentina y que se utiliza en aquellas poblaciones difíciles de alcanzar: personas en situación de calle e integrantes de Pueblos Originarios son solo dos de los ejemplos de personas a las que se les aplicó esta dosis.

##### Preocupación por el aumento de la variante Delta en la Argentina

Por estos días, uno de cada cuatro contagios de COVID-19 está relacionado a la variante Delta, sin embargo esta relación podría modificarse en los próximos días, ya que en el AMBA la presencia de esta mutación está cercana al 50%, según confirmó Analía Rearte, directora nacional de Epidemiología y Análisis de Situación de Salud. Asimismo, advirtió que “es esperable que el número de casos aumente” y pidió “seguir con la vacunación y mantener las medidas de prevención y control”.

Desde que a finales de julio fue detectado el primer caso de contagio por variante Delta sin nexo con viajeros, esta mutación comenzó a ganar terreno y según el último reporte emitido por el Ministerio de Salud ahora se posiciona en el 24%. Es decir que, tras analizar el “DNI” del virus, esta variación originaria de India es responsable de uno de cada cuatro nuevos casos de coronavirus. Sin embargo, “a nivel nacional, ahora estaría en un 30% de prevalencia y es peor en los aglomerados urbanos”, dijo Rearte.