+++
author = ""
date = 2022-01-26T03:00:00Z
description = ""
image = "/images/2z5u5xh52fbi5kyl5obfybk2sq.jpg"
image_webp = "/images/2z5u5xh52fbi5kyl5obfybk2sq.jpg"
title = "SEGÚN PATRICIA BULLRICH, LA DERROTA EN EL 2019 SE DEBIÓ A QUE: “GUARDAMOS IDEAS EN EL PLACARD”"

+++

##### **_La titular del partido PRO, Patricia Bullrich, mandó este martes un fuerte mensaje hacia el corazón de Juntos por el Cambio, de cara a la definición del 2023, desde Pinamar, donde presentó un cuadernillo partidario. “En 2019 perdimos porque guardamos ideas en el placard”, señaló._**

La exministra de Seguridad se mostró en la localidad balnearia junto al pensador Santiago Kovadloff con quien presentó “Nuestras ideas”, un libro de 48 páginas que recoge reflexiones políticas y da cuenta de la tarea de movilización en el partido PRO desde el año 2020, con la pandemia. Fue editado por la Fundación Konrad-Adenauer-Stiftung (KAS).

“Cuando perdimos las elecciones salimos del placard. Y empezamos a pensar que nuestro partido y que nuestra coalición tenía que profundizar ideas que fuesen el rumbo, el horizonte, que expresase el mandato de una sociedad”, afirmó Bullrich, en el encuentro.

Con un discurso que pide consolidar el trabajo interno en PRO, Bullrich lanzó su discurso ante la medida atenta del intendente de Pinamar, Martín Yeza, el diputado Hernán Lombardi, y el exministro e intendente de Capitán Sarmiento, Javier Iguacel.

La exministra profundizó su autocrítica sobre la derrota ante el Frente de Todos. “No fuimos mayoría, creo yo, porque guardamos muchas ideas en el placard. Si las hubiéramos desplegado más, habríamos pasado esa elección”, evaluó.

Para la extitular de Seguridad, la derrota representó un nuevo desafío. “Perdiendo una elección decidimos algo, que en la gestión hicimos poco: salimos a la calle a luchar y a decir que no nos rendimos”, indicó.

##### **“Camino ético”**

Bullrich consignó que cuando Mauricio Macri la eligió para ponerse al frente del partido Pro, le transmitió su pensamiento acerca de cómo continuar. “Dije que quiero conducir un PRO que salga un poco del marketing y entre en el debate duro de las ideas”.

La referente, que blanqueó su deseo presidencial, pidió “cuidar a la coalición opositora. “Mauricio Macri logró que el PRO sea el tercer partido en la historia que no depende solamente de un liderazgo, sino de un gran equipo extrendido por todo el país. Eso es lo que perdura”, razonó.

La exministra no evitó refererirse en esta línea a los dirigentes que tomaron distancia de Juntos por el Cambio y aludió a la salida de la diputada provincial de Natalia Sánchez Jáuregui, que pasó a integrar las filas del Frente de Todos.

“Cuando tuvimos un error, con diputados que se fueron casi perdimos todo lo que habíamos ganado en las elecciones. No tenemos margen de error. El mandato de la sociedad es no salirnos del camino ético, cualquier otro mandato es traición a los millones de argentinos que nos votaron”, apuntó.

##### **“Laburar por Zoom”**

La presidenta del PRO lanzó fuertes críticas a la administración de Alberto Fernández e hizo hincapié en la falta de presencialidad en el sistema educativo en los últimos dos años.

En esa línea, se refirió a las decisión de varias universidades de exigir un pase sanitario para asistir a clases presenciales. “Otra vez vamos a empezar... Ponemos en duda la posibilidad de que los chicos vayan a clases. Las universidades, muchas ni abrieron y quieren hacer obligatoria la vacunación” manifestó indignada y continuó: “lo que quiere es seguir laburando por Zoom”.

Bullrich recordó que se había enfrentado al empresario Alfredo Yabrán, responsable del asesinato del fotógrafo José Luis Cabezas, de cuyo crimen se cumplieron este martes 25 años. Y lo hizo para remarcar la importancia de sobreponerse a las restricciones que aplica el kirchnerismo, a quien acusó de cercenar libertades.

“Hace 25 años un mafioso mató a Cabezas. Yo me peleé con él; me enfrenté. Tuve miedo, pero tuve más decisión. Si me vencía el miedo, tenía que irme de la acción política”, analizó. La dirigente llamó a reclamar para “recuperar la libertad” y, citó al Himno nacional, al decir que es necesario “romper nuestras cadenas”.

La dirigente recordó el caso de Santiago Maldonado, mencionó que fue absuelta por el juez Gustavo Lleral y alentó al Poder Judicial a resolver asuntos con más decisión. “Los jueces tienen que animarse a actuar, a respetar la ley. La independencia de la Justicia es hacer que ningún poder fáctico y mediático le diga cómo es la ley”, enfatizó.