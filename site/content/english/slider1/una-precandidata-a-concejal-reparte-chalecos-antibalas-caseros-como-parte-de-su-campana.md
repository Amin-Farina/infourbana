+++
author = ""
date = 2021-08-23T03:00:00Z
description = ""
image = "/images/52hn2ds7zvcencbstyxwgwgy5m.jpg"
image_webp = "/images/52hn2ds7zvcencbstyxwgwgy5m.jpg"
title = "UNA PRECANDIDATA A CONCEJAL REPARTE CHALECOS ANTIBALAS CASEROS COMO PARTE DE SU CAMPAÑA"

+++

**_Una precandidata a concejal de Rosario por un espacio evangélico afín a Juntos por el Cambio reparte chalecos antibalas caseros como señal de advertencia ante la inseguridad que atraviesa la ciudad._**

Se trata de Silvia Canterella, de 50 años, precandidata a concejal por el espacio evangélico Una Nueva Oportunidad, que forma parte de la alianza de Juntos por el Cambio. Lo hace con vistas a las Primarias Abiertas, Simultáneas y Obligatorias (PASO) del 12 de septiembre próximo.

“No queremos que la gente se arme, pero tampoco queremos que una bala nos perfore el estómago”, argumentó Canterella. En diálogo con La Capital de Rosario, dijo que la idea surgió a partir del consenso entre colaboradores y colaboradoras en los barrios donde afirma que trabaja desde hace 12 años, entre ellos Tablada, Ludueña, Zona Cero y Empalme Graneros.

> Y afirmó: “No hace mucho que comenzamos; se me ocurrió la idea porque en los barrios que estamos trabajando son los más peligrosos de la ciudad. Y debido a las balaceras que hay, la gente tiene miedo”.

“Es una forma de pedir ayuda, necesitamos que nos protejan, más presencia de la policía, del Estado, la seguridad y la tranquilidad y poder sentarnos tranquilos en la vereda”, sostuvo.

Esta no es la primera vez que un precandidato lanza una iniciativa de este tipo. El exdiputado provincial santafesino Sergio Más Varela repartió en 2015 gas pimienta para repudiar la falta de seguridad que atravesaban los barrios de la ciudad bajo el gobierno de la entonces intendenta Mónica Fein, recordó el diario.

##### ¿CÓMO SON LOS CHALECOS ANTIBALAS CASEROS?

Canterella dijo que sus chalecos antibala están fabricados con tela resistente denominada “cordura”. Además viene con bolsillos en el frente y en el dorso para agregarle cualquier tipo de material extra.

“Un gimnasio nos donó discos de fundición que se utilizan para hacer pesas, pero después el o la que lo va a usar le puede colocar el material que quiera como planchuelas de fundición y todo tipo de cosas resistentes”, detalló.

La precandidata dijo que hace 12 años trabaja en los barrios más peligrosos de la ciudad. “Es la segunda vez que voy como candidata. Estuve en 2019 con el partido Unite. Nos fue muy bien por ser la primera vez, sin recursos, sin estructura, fui sola y mi alma. Pero tengo un trabajo territorial de hace muchos años y el voto fue de la gente que me conoce, compartiendo cara a cara todo el tiempo, no solo el merendero sino jóvenes en situación de consumos problemáticos y vulnerabilidad”, indicó.

La mujer se definió como “cristiana evangélica” y afirmó que trabaja en una iglesia en la que comenzó con el trabajo de asistencia a niños y niñas y luego se volcó a la labor social. “No es solo lo material sino lo espiritual, ya que también soy operadora social y asistente jurídica”, concluyó.