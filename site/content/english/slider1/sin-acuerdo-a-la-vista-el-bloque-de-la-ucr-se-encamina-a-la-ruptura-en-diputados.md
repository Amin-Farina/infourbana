+++
author = ""
date = 2021-12-02T03:00:00Z
description = ""
image = "/images/5ff775c627968_671_377.jpg"
image_webp = "/images/5ff775c627968_671_377.jpg"
title = "SIN ACUERDO A LA VISTA, EL BLOQUE DE LA UCR SE ENCAMINA A LA RUPTURA EN DIPUTADOS"

+++

#### **_El sector que respalda al actual jefe de bloque, Mario Negri, se reunió sin éxito con el sector que acompaña a Emiliano Yacobitti. Los “disidentes” armarían una nueva bancada dentro de la coalición opositora._**

Pasan los días en la cuenta regresiva para el recambio legislativo, y la interna de la Unión Cívica Radical por la jefatura del bloque en Diputados sigue al rojo vivo. Este jueves, los diputados que respaldan la reelección de Mario Negri y el sector de Evolución Radical, que lideran Martín Lousteau y Emiliano Yacobitti, no alcanzaron un acuerdo sobre la designación de autoridades y el perfil de la bancada y crecen las posibilidades de un quiebre.

Según confirmaron fuentes del radicalismo a TN, de la reunión participaron los diputados Ricardo Buryaile, Miguel Bazze y Karina Banfi -por el sector de Negri- y el propio Yacobitti, María Dolores Martínez y Alejandro Cacace -por el sector de los “disidentes”.

De los 45 integrantes del bloque radical, el cordobés Mario Negri, sostienen en Diputados, cuenta con 27 legisladores que lo acompañan para que continúe como jefe de bancada.

Ante ese panorama, los diputados de Evolución fueron directamente con el reclamo de que la jefatura del interbloque (que engloba al PRO, la UCR y la Coalición Cívica) deje de estar ocupada por uno de los jefes de bancada, y que sea un “tercer (o cuarto) jugador” que actúe como vocero de todos los sectores: y pidieron el puesto.

“La charla fue amable, nadie se gritó. Se les explicó que numéricamente le corresponde al PRO por la cantidad de Diputados y que Negri no les puede dar algo que no es de él. Y que además los tres jefes de bloque no creen que sea apropiado un desdoblamiento, porque el presidente de interbloque es un coordinador”, señalaron en el sector del cordobés.

Yacobitti y sus acompañantes se retiraron con la advertencia de que evaluarán la ruptura de bloque: es decir, armar una bancada dentro de Juntos por el Cambio, pero fuera de la UCR, tal como sucedió entre 2015 y 2017, cuando el mismo Lousteau encabezó una bancada de cuatro integrantes bajo el paraguas del entonces oficialista Cambiemos.

Este sector cuenta con el respaldo de 15 diputados. Sin embargo, advierten en el radicalismo, los dos diputados mendocinos que responden al gobernador Rodolfo Suárez tendrían instrucciones de mantenerse dentro del bloque UCR, y el pampeano Martín Berhongaray y el salteño Miguel Nanni no tendrían demasiadas intenciones de salirse.

“Nosotros representamos a la mayoría de los intendentes radicales del país, y sectores muy importantes como la provincia de Buenos Aires, Santa Fe, y los diputados que ganaron en Córdoba, donde Negri intentó ser senador y perdió la interna. ¿Cómo a ese grupo no se les va a reconocer uno de los dos puestos clave en el Congreso?”, señalan en el sector de Yacobitti.

El panorama se terminará de definir el próximo lunes a las 18, cuando se realice la reunión de bloque convocada por las actuales autoridades parlamentarias del radicalismo.

“Que recapaciten que el radicalismo no puede unificar sus vocerías detrás de una persona que va a reelegir por quinta vez, cuando encima perdió en su provincia. Si la respuesta el lunes es no, no vemos más salida que la ruptura. Nuestra prioridad es escuchar a la gente y la gente pidió renovar los partidos”, agregan en el sector “disidente”.

##### ¿Cómo sigue la discusión por el interbloque de Juntos por el Cambio en Diputados?

En los últimos años, Negri ocupó el doble rol de jefe de la bancada radical y del interbloque, esquema de poder avalado por los socios del PRO y de la Coalición Cívica, más allá de que la bancada “amarilla” tuviera mayor número de diputados como para ostentar ese lugar.

El cordobés señaló que no era su prioridad mantener la jefatura del interbloque, que podría pasar a manos del jefe del bloque del PRO, Cristian Ritondo, que fue ratificado en su puesto en las últimas semanas.

La jefatura de la UCR se definirá el lunes. El martes tienen que inscribirse los bloques en Mesa de Entradas antes de la sesión de jura de diputados. Sin embargo, explican en Diputados, como no existe la figura formal de Interbloque en el Reglamento, no es necesario brindar el nombre del presidente o jefe de Interbloque.

La primera sesión con la nueva composición sería la tercera semana de diciembre, según anticipan en la presidencia de la Cámara que conduce Sergio Massa. Recién entonces podría llegar a oficializarse quién será el jefe del interbloque de Juntos por el Cambio.