+++
author = ""
date = 2021-08-11T03:00:00Z
description = ""
image = "/images/i55ex37zojhmvhbsbno5ylluia.jpg"
image_webp = "/images/i55ex37zojhmvhbsbno5ylluia.jpg"
title = "RESERVE, LA CRIPTOMONEDA QUE YA ES FUROR EN VENEZUELA Y BUSCA INSTALARSE EN ARGENTINA"

+++

**_Con el apoyo de uno de los fundadores de PayPal, es una criptomoneda atada al valor del dólar y servirá tanto para ahorrar como para hacer pagos en comercios a través de una app. Su objetivo es proteger al público contra las devaluaciones y los procesos inflacionarios._**

Mientras el mundo entero en los últimos años se volcó a las criptomonedas como una vía sofisticada para invertir, ahorrar, pagar y mover dinero en paralelo a la banca tradicional y a las reglas de los gobiernos, un proyecto se desarrolló pensando en el otro extremo del sistema: los que simplemente necesitan vivir con una moneda sin inflación. La criptomoneda en cuestión se llama Reserve, fue creada en 2019 y tiene gran desarrollo en Venezuela, donde la inflación fue del 19% solo en el mes de julio. El segundo mercado para expandirse, naturalmente, es la Argentina. El tercero, según anticipan, es Líbano.

Reserve es una stablecoin atada al dólar, es decir, una critpomoneda cuyo valor sigue al de la moneda estadounidense y por ello no tiene la volatilidad de otras como el Bitcoin o Ether. Pero además su uso viene atado a una app que permite hacer pagos, aún por pequeñas compras en comercios.

“Lo que nos diferencia de otras criptomonedas estables es que no apuntamos al público inversor ni al público mas sofisticado en cuanto a las criptomonedas, sino que tratamos de ir a la gente de a pie. Procuramos darle una herramienta a la gente para su vida cotidiana”, señalo el cofundador y CEO de Reserve, Nevin Freeman.

Si bien ya puede comprarse a través de Binance o de otros exchange, Freeman anticipa un desembarco comercial fuerte de Reserve al país en el corto plazo, con el expertise de la hiperinflacionaria Venezuela. Allí, Reserve es aceptada para hacer pagos en supermercados, pequeños comercios y hasta en aplicaciones del estilo Uber o Rappi, como una forma de esquivar el drama de usar dinero en efectivo en el que hace falta una gran cantidad de billetes para una compra pequeña. El próximo 1° de octubre, Venezuela le quitará seis ceros al bolívar, tal como ocurriese en otros tiempos en la Argentina.

Reserve también es utilizada para darle valor de dólar a los devaluados bolívares y para facilitar las remesas con los 5 millones de venezolanos que viven fuera del país.

Sin los extremos de la economía venezolana, en Reserve vieron en la Argentina un destino obligado para su expansión, aún cuando otros mercados, como México o Brasil, hubieran sido más rentables. “En Argentina hay mucho interés por este tipo de herramientas y por el espacio cripto. Queremos expandirlo a un público mas amplio, que todos puedan usar estas tecnologías para transacciones de la vida cotidiana. Una alternativa más para poder ahorrar, ir al supermercado, a la farmacia, o pagar cualquier tipo de servicio”, señaló Freeman.

Entre sus principales invesores aparece Peter Thiel, confundador de PayPal. El gigante de los pagos digitales fue creado por Thiel, junto a Elon Musk y otros emprendedores en 2001, mientras los ahorros de millones de argentinos quedaban atrapados en los bancos. En Reserve aseguran que Thiel se vio impactado por esa incautación masiva de depósitos y que se propuso a sí mismo que algún día generaría una solución para esos casos. Dos décadas más tarde, impulsó la primera “criptomoneda para países con alta inflación”.

En tren de expandir la “usabilidad” de las criptomonedas, Reserve ya había puesto un pie en la Argentina como uno de los inversores de Lemon Cash, la app liderada por dos jóvenes argentinos, Marcelo Cavazzoli y Borja Martel, que recientemente sorprendió al obtener USD 16,3 millones en el mercado para expandirse en la región. Lemon también tiene el foco en llevar las cripto al día a día: su app es una billetera dual, de pesos y cripto, que se une a una tarjeta Visa prepaga que permitirá pagar con cualquiera de las dos opciones.

Entre otros accionistas de Reserve aparece Sam Altman, habitual “inversor ángel” de proyectos cripto en todo el mundo y fundador de YCombinator, la “aceleradora” de Lemon Cash. También se alistó Coinbase Ventures, el fondo inversor de una de las exchange más importantes del mundo.

Las stablecoins, o criptomonedas estables, ya tienen recorrido un camino en la Argentina, con ejemplos como DAI, USDT o USDC, atadas al dólar. Frente a los crecientes controles cambiarios, se trata de opciones para dolarizarse en forma legal aunque por fuera del sistema bancario tradicional. Reserve promete sumar a esa vía de ahorro para esquivar el cepo algunos servicios que permitan transformarla en un medio de pago de uso frecuente.

¿Qué servicios ofrecerá la app de Reserve? Un equipo de 50 personas ya se está trabajando en su versión Beta para la Argentina para ajustar su funcionamiento. No obstante, anticipan que ya están en conversaciones con las emisoras de tarjetas, Visa y Mastercard, y que se adaptará a todas las formas de pago, incluyendo el código QR y cualquier clase de transferencia.

¿Cómo funciona la stablecoin de Reserve? El sistema une dos tokens: Reserve (RSV) y Reserve Rights (RSR). El primero es una stablecoin atada al valor del dólar estadounidense y, por lo tanto, de bajísima fluctuación en su precio. El RSR es un token atado a una canasta de criptomonedas y que es utilizado para asegurar la estabilidad del anterior a través de compras o ventas según el precio del RSV se encuentre levemente por encima o por debajo de 1 dólar.

El proyecto trae detrás una postura que va un paso más allá del discurso tradicional de las criptomonedas y tiene un enfoque de tono humanitario. Para los impulsores de Reserve, vivir con una moneda estable, que esté exenta de la inflación y las devaluaciones es algo a lo que cualquier persona debe tener acceso. “El dinero de todos debe estar seguro. Miles de millones de personas en todo el mundo no tienen asegurado un lugar para depositar su dinero. No se puede confiar en los bancos de algunos países y algunos gobiernos inflan su propia moneda para pagar deudas, perjudicando a los ciudadanos en ese proceso”, explica la compañía, que para septiembre iniciará una campaña global de recolección de firmas. Quiere pedirle a las Naciones Unidas que tener una moneda estable sea considerado un derecho humano.