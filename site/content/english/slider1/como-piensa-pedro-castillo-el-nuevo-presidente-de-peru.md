+++
author = ""
date = 2021-06-08T03:00:00Z
description = ""
image = "/images/pedro-castillo-peru-1186858.jpg"
image_webp = "/images/pedro-castillo-peru-1186858-1.jpg"
title = "¿CÓMO PIENSA PEDRO CASTILLO, EL NUEVO PRESIDENTE DE PERÚ?"

+++

**_Pedro Castillo, un maestro de escuela rural que salió del anonimato hace cuatro años como líder de una huelga nacional del magisterio, irrumpió con un discurso de izquierda y la promesa de “no más pobres en un país rico”, consignas con las que busca convertirse en presidente de Perú._** 

En una entrevista televisiva el candidato de izquierda se pronunció en contra de los monopolios y adelantó que, en caso de llegar a la presidencia, impulsaría un plan económico con fuerte presencia estatal. Esto, pese a que algunos de los ejemplos que ofreció no se ajustaban a lo que planteaba.

En el reportaje se expresó en contra del “monopolio de las empresas de transporte, la línea aérea Latam”, y comercios como “Zara, Falabella y Metro”. Castillo explicó que se trata de monopolios porque “aglutinan su economía para sacar un beneficio empresarial sin importarle el Estado, sin importarle el pueblo”.

También planteó la posibilidad de “traer el modelo de Singapur”. Sin embargo, el periodista a cargo de la entrevista le aclaró que el modelo del país asiático “fue totalmente tirado a la derecha”. “Usted propone un estado estatista, eso no es lo que hizo Singapur. Singapur tiene una de las tasas de impuestas más bajas del mundo”.

“Hay que sacar lo bueno de lo que tiene cada país (…) Pero en todo caso su forma de implementar la educación, ¿por qué no evaluarla y traerla a Perú”, respondió el candidato de Perú Libre.

Por su parte, dijo que “es insuficiente pagar impuesto a la renta” y remarcó la necesidad de “revertir” a la empresa minera. Pero en este punto volvió a plantear una contradicción. “La empresa minera hay que revertirla toda. De cada 100 soles de la riqueza que se saca de la venta...”, comenzó su explicación, que rápidamente fue refutada por el entrevistador: “Pero eso no es utilidad, profesor. Yo no puedo tributar sobre la venta. Yo solamente puedo tributar sobre la utilidad que me queda a mí, la venta es recaudación bruta”.

“¿Pero quién se lo lleva?”, añadió Castillo. “Se lo lleva quien puso el dinero, esa es su renta”, subrayó el periodista.

Sobre la dictadura de Juan Velasco Alvarado (1968-1975), opinó: “Más allá de cualquier estigmatización que se le ha dado, es un hombre que ha interiorizado una gran problemática de los peruanos”. Además, indicó que “en materia económica fue un buen gobierno”.

“Si nos remitimos a las cifras, no le dan la razón. El gobierno militar de los 70 generó de los más altos déficits de la historia de nuestro país”, apuntó el periodista.

Con el 95,73% de las actas procesadas, la Oficina Nacional de Procesos Electorales (ONPE) informó que Pedro Castillo obtiene un 50,26% de los votos, mientras que Keiko Fujimori suma 49,74% de respaldo, tras las elecciones presidenciales del domingo.

Sin embargo, la carga de datos es progresiva, por lo que el resultado final aún es incierto y las autoridades ya adelantaron que recién se sabrá quién ganó en los próximos días.

La propuesta electoral de Perú Libre se basó en una tríada: salud, educación y agricultura, los sectores prioritarios para impulsar el desarrollo nacional, según Castillo. El candidato ha anunciado, además, que si llega al poder, el país recuperará el control de sus riquezas energéticas y minerales, como el gas, el litio y el oro, ahora bajo control de multinacionales. Sin embargo no ha precisado cómo lo hará.

También prevé convocar a una Asamblea Constituyente para redactar en seis meses una nueva carta magna en reemplazo de la actual, que privilegia la economía de libre mercado.

La Constitución de 1993 es un legado del gobierno de derecha de Alberto Fujimori (1990-2000), padre de Keiko. La rival de Castillo, en cambio, se opone a cambiar la carta magna.

El candidato promete además expulsar a los extranjeros que cometan delitos, en tácita alusión a los migrantes venezolanos que llegaron desde 2017 y que superan el millón. “\[Daremos un\] plazo de 72 horas a extranjeros ilegales para dejar el país, los que han venido a delinquir”, aseguró Castillo, que con el fin de combatir la inseguridad propone que Perú se retire del Pacto de San José para restablecer la pena de muerte a los delincuentes.

Perú Libre es uno de los pocos partidos peruanos de izquierda que defiende al dictador venezolano Nicolás Maduro.