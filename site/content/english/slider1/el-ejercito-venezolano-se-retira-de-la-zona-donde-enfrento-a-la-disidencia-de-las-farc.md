+++
author = ""
date = 2021-05-31T03:00:00Z
description = ""
image = "/images/fichero_23053_20200706.jpg"
image_webp = "/images/fichero_23053_20200706-1.jpg"
title = "EL EJERCITO VENEZOLANO SE RETIRA DE LA ZONA DONDE ENFRENTÓ A LA DISIDENCIA DE LAS FARC"

+++

**_Tras dos meses y medio de conflicto, el balance arroja 16 militares asesinados, ocho secuestrados y algunos desaparecidos. La guerrilla se mostró celebrando la retirada de los uniformados en varios pueblos de Apure._**

La Fuerza Armada Nacional Bolivariana (FANB) ha marcado un repliegue en su enfrentamiento con las disidencias de las Fuerzas Armadas Revolucionarias de Colombia (FARC) en el estado Apure, a casi dos meses y medio desde que se inició el conflicto armado el 21 de marzo 2021 y que le ha costado a la institución castrense 16 militares asesinados, ocho secuestrados y algunos desaparecidos. Gran parte del personal fue desplazado de la zona.

El 28 de mayo numerosos militares del Ejército y los rurales de la Guardia Nacional salieron de la zona. En la vía desde La Victoria hasta La Soledad, no quedó ninguna de las 9 alcabalas que había. Desde La Victoria a Guasdualito solo quedaron dos de las siete 7 alcabalas o puntos de control. La mitad del personal militar, que fue llevado durante estos dos meses, salió del lugar.

“Los tanques de guerra fueron trasladados hasta una instalación militar cercana a La Victoria; los vehículos que estaban en buenas condiciones también fueron llevados hasta ese lugar. Lo que sí se llevaron fue los carros dañados. Las tanquetas del CONAS (Comando Antisecuestro) estaban ocultas, pero ya las sacaron y las tienen en el comando de la GNB en La Victoria”.

Militares dijeron a Infobae que les han asegurado que sustituirán a todo el personal que quedó por ahora en la zona. El sábado, 29 de mayo, en la noche se oyeron ráfagas de armas largas y cuatro estallidos de lo que serían bombas o minas antipersonales. En La Primavera aseguran que el Ejército venezolano está desactivando las minas y bombas colocadas por la guerrilla en el lugar.

“No sabemos qué tan cierto es, pero por información de Inteligencia se supo que la carretera entre La Victoria y El Nula está minada en varios puntos”.

Es importante destacar que esa misma noche del 28 de mayo, la guerrilla se mostró en el pueblo de La Victoria, El Ripial, La Capilla y Santa Rosa, celebrando la retirada militar.

“En el pueblo hay varios guerrilleros de los que estaban al mando de alias Ferley (fracción Gentil Duarte) que aseguran que se van a enfrentar con hombres de la Segunda Marquetalia, porque tienen información que son ellos quienes los van a combatir ahora que el Ejército venezolano se fue”, dijo a Infobae un habitante del Alto Apure.

#### Enfrentamientos

El 26 de mayo a las 12:20 PM la disidencia de las FARC fracción Gentil Duarte dispararon contra “las voladoras” de la Armada colombiana, que se desplazaba por el río, a la altura del sector Montillero y/o La Capilla. Se inició fuego cruzado entre militares colombianos y guerrilleros, lo que aterrorizó a los habitantes del sector.

Voladoras es el nombre que los habitantes de la frontera dan a las embarcaciones rápidas con motores fuera de borda normalmente utilizada para transporte de personas o materiales de diverso tipo.

“Aquí en Venezuela no hay gobierno para la guerrilla”, dice JA, un habitante del sector, quien expresa su indignación porque “esa gente, las FARC, anda uniformada y armada por territorio venezolano”.

En diálogo con Infobae agrega JA que “la guerrilla es visible con sus uniformes y armamento, desde Santa Rita hasta La Capilla se les ve, se han metido para los arenales, donde se están atrincherado”, agregando que eso ocurre desde que se agudizó el problema entre la Fuerzas Armada Venezolana y las disidencias de las Fuerzas Armadas Revolucionarias de Colombia (FARC).

El ataque de la guerrilla contra las voladoras de la Marina Colombiana se originó cuando los militares colombianos les decomisaron dos falcas, es decir dos canoas grandes y planas, a las que le hacen unos techos de lona o plástico, que tiran de un lado a otro del río y con las cuales la guerrilla estaba transportando alimentos para sus campamentos en territorio venezolano.

“Las dos falcas estaban llenas de mercado. Las FARC se molesta con la Marina Colombiana y subieron en moto para emboscarlos en la orilla del río Arauca. Cuando las voladoras de la Marina subían, los guerrilleros les cayeron a plomo. Ahí se armó el enfrentamiento porque los marinos colombianos respondieron al ataque”, finaliza diciendo.

Desde que hace unos días las FARC-Gentil Duarte filtró un video, como fe de vida, con los ocho militares que tienen secuestrados, no ha habido respuesta de la Fuerza Armada ni de los altos jerarcas del Gobierno. El repliegue del Ejército en Apure pudiera facilitar la entrega de los militares secuestrados o pudiera recrudecer la guerra si es una estrategia militar para replantear los ataques.