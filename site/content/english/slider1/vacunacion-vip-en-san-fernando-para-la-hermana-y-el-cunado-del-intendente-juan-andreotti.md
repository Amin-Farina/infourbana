+++
author = ""
date = 2021-09-08T03:00:00Z
description = ""
image = "/images/bbc64ff83c6b5c288f1ceb74b7647359_xl.jpg"
image_webp = "/images/bbc64ff83c6b5c288f1ceb74b7647359_xl.jpg"
title = "VACUNACIÓN VIP EN SAN FERNANDO PARA LA HERMANA Y EL CUÑADO DEL INTENDENTE JUAN ANDREOTTI"

+++

##### **_María Eva Andreotti y su pareja, Gonzalo Cornejo, recibieron las dos dosis de la vacuna Sputnik V contra coronavirus. No integran el grupo de personas de riesgo, ni tampoco tienen la edad necesaria para recibir anticipadamente la inoculación. Se reactivó el escándalo de la "Vacunación VIP"._**

Lejos de aplacarse, el escándalo por la vacunación VIP en ámbitos gubernamentales sigue escalando y genera más indignación y polémica. En este caso, se pone el foco en un nuevo episodio plagado de irregularidades que tiene como epicentro al municipio bonaerense de San Fernando.

 Se trata de un nuevo "salto" en la fila de quienes esperaban pacientemente sus vacunas contra el coronavirus, y que ahora recobra fuerza, luego de que la Justicia determinara reabrir la causa.

 María Eva Andreotti y su pareja, Gonzalo Cornejo, recibieron las dos dosis de la vacuna Sputnik V contra el Covid-19.

 La mujer es hermana de Juan Andreotti, intendente de San Fernando, y el hombre es su cuñado, quienes ocupan cargos en el municipio.

 Andreotti, de 41 años de edad, integra la secretaria de Desarrollo Social, Educación y Medio Ambiente desde el 4 de enero, fecha en que fue publicada su designación en el Boletín Oficial. Cornejo ronda los 44 y es secretario de Modernización y Gestión Informativa, área que tiene como objetivo utilizar tecnología como nexo entre el Municipio, los vecinos y visitantes.

 Ellos no forman el grupo de riesgo ni tampoco tienen la edad necesaria para recibir la vacuna. Tampoco están la frente de algún programa local que los exponga al virus. María Eva tuvo coronavirus en octubre.

 Según el Sistema Integrado de Información Sanitaria Argentino (SISA), la primera vacuna fue recibida por ellos el pasado 16 de enero; en tanto que el 11 de febrero, Cornejo recibió la segunda dosis. El día posterior, 12 de febrero, llegó finalmente el turno de Andreotti. La inoculación se realizó en el hospital local Petrona de Cordero.

 Vale destacar que María Eva Andreotti ha sido subsecretaria de la Secretaria de Salud de San Fernando. El 4 de enero, de acuerdo al Boletín Oficial, la designaron en el nuevo cargo. No es personal de salud. Se dedicó, hasta ocupar una función pública, a manejar los locales de ropa que su familia tenía en San Fernando y al diseño de modas. A mediados de octubre contó en sus redes sociales que se había contagiado de coronavirus.

 Con este marco, Cornejo no respondió preguntas de los medios al respecto. Pidió, a cambio, que nos comuniquemos "el jueves así lo agarramos en la oficina bien tranquillo" dado que "se tomó una semana en San Juan". "Te agradezco el llamado pero estoy de vacaciones y trato de desestresarme unos minutos". Tras ser llamado por medios periodísticos, Cornejo no atendió los llamados.

 Juan Andreotti, intendente actual del municipio de San Fernando, es hijo del ex intendente de San Fernando entre 2011 y 2019, Luis Andreotti, muy cercano a Sergio Massa. A punto de cumplir 70 años, su hijo lo sucedió en el cargo. La gestión de Luis Andreotti tuvo una alta aprobación de los vecinos. En 2016, de acuerdo a una encuesta de la consultora privada Aresco, el 78,7% de los habitantes de San Fernando valoraron positivamente su mandato. Ese dato lo ubicó primero en el ranking de jefes comunales del AMBA. Luis Andreotti también se vacunó. Lo hizo bajo el registro de "personal de salud". Desde el entorno lo justificaron: "Es una persona de riesgo, tiene tres stents y es hipertenso".