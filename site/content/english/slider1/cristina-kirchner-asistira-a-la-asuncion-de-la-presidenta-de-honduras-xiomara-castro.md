+++
author = ""
date = 2022-01-24T03:00:00Z
description = ""
image = "/images/udcsohjpzjdyjl7u6vwlnt5ssm.jpg"
image_webp = "/images/udcsohjpzjdyjl7u6vwlnt5ssm.jpg"
title = "CRISTINA KIRCHNER ASISTIRÁ A LA ASUNCIÓN DE LA PRESIDENTA DE HONDURAS, XIOMARA CASTRO"

+++

#### **_La vicepresidenta Cristina Kirchner viajará esta semana a Honduras, donde el jueves asistirá a la toma de posesión de la presidenta electa de ese país centroamericano, Xiomara Castro._**

Lo anunció este domingo un comunicado de la Vicepresidencia citado por la agencia de noticias oficial Télam.

El acto de asunción se llevará a cabo el jueves en el estadio nacional de Tegucigalpa.

La exmandataria tendrá la oportunidad de codearse en la capital hondureña con la vicepresidenta de Estados Unidos, Kamala Harris, quien encabezará una delegación oficial de la Casa Blanca.

Xiomara Castro fue proclamada oficialmente presidenta por la autoridades electoral de su país para el período 2022-26, tras ganar las elecciones del 28 de noviembre pasado.

La candidata del partido de izquierda Libertad y Refundación (Libre) obtuvo poco más de 1,7 millón de votos, equivalentes a 51,12% de los sufragios válidos en los comicios con una diferencia de casi 20 puntos sobre su rival de derecha, Nasry Asfura, del oficialista Partido Nacional.

Xiomara Castro es la esposa del expresidente hondureño José Manuel Zelaya, derrocado en 2009.

##### ¿Qué dijo Cristina Kirchner cuando se confirmó el triunfo en las elecciones de Xiomara Castro?

Cristina Kirchner considera a Xiomara Castro una dirigente afín a su proyecto político “latinoamericanista”.

Al confirmarse su triunfo en las elecciones de Honduras, la expresidenta argentina escribió en sus redes sociales el 1° de diciembre el siguiente mensaje: “Finalmente, mi querida compañera y amiga Xiomara, más tarde o más temprano, el pueblo y la historia siempre hacen justicia. Comienzo de la charla que mantuvimos el día lunes con Xiomara Castro de Zelaya, primera Presidenta electa de la República de Honduras”, indicó.

También el presidente Alberto Fernández saludó por la misma red social el triunfo de la izquierda hondureña: “En comunicación con Xiomara Castro, primera mujer electa presidenta de Honduras, pude expresarle mis felicitaciones por la victoria electoral”, escribió. Y agregó: “Su mensaje de unidad, solidaridad y diálogo es un nuevo motivo de esperanza para los pueblos de América Latina y el Caribe”.

Cristina Fernández viajará oficialmente al exterior por primera vez desde la asunción del Gobierno de Alberto Fernández.

En tanto, el presidente prevé viajar a Rusia y China a principios de febrero. En Moscú y Beijing se reunirá con los presidentes Vladimir Putin y Xi Jinping, respectivamente.

##### Crisis en el partido de Xiomara Castro a pocos días de su asunción

Xiomara Castro asumirá el poder en medio de una crisis interna en su partido, después de que un grupo de diputados de Libertad y Refundación (Libre) votó como presidente de la Junta Directiva del Congreso a Jorge Cálix y no a Luis Redondo, integrante del Partido Salvador de Honduras (PSH), tal como había acordado la mandataria electa.

Por ello, este domingo, el Parlamento quedó dividido en dos. Un grupo votó por unanimidad por Redondo, mientras que fuera del edificio legislativo otra asamblea eligió a Cálix como presidente.

En su cuenta de Twitter, Xiomara Castro dijo que que reconoce a Redondo como titular del Congreso y lo invitó a la ceremonia de jura el jueves 27.