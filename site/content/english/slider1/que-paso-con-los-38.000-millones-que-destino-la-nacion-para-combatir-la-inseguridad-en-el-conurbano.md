+++
author = ""
date = 2021-11-09T03:00:00Z
description = ""
image = "/images/berni-operativo-sol-2020-scaled.jpeg"
image_webp = "/images/berni-operativo-sol-2020-scaled.jpeg"
title = "Qué pasó con los $38.000 millones que destinó la Nación para combatir la inseguridad en el Conurbano"

+++

#### **_En septiembre de 2020, el presidente Alberto Fernández anunció el plan a pedido de una veintena de intendentes. Los cruces por un fondo que resultó insuficiente para disminuir el delito._**

En agosto de 2020, el clima en materia de seguridad se comenzaba a tensar por un acelerado crecimiento del delito en el Conurbano. El Gobierno lanzó entonces el Programa de Fortalecimiento de la Seguridad, con una inversión de $37.700 millones, para dar respuesta a los reclamos de un grupo de intendentes. El plan fue comunicado por Alberto Fernández desde la Quinta de Olivos, junto al gobernador Axel Kicillof, la por entonces ministra Sabina Frederic y su par bonaerense Sergio Berni, rodeados por 24 jefes comunales de las zonas más afectadas, a quienes le destinarían unos $10.000 millones para la compra de equipamiento y gastos operativos.

Dentro de la inversión, se otorgó a los municipios del Conurbano un fondo de $7.500 millones para equipamiento de seguridad, que fue ampliado por la Provincia hasta alcanzar los 8.123 millones de pesos. Con ese dinero, los intendentes compraron móviles y cámaras, en mayor medida. Uno de los principales impulsores del reclamo había sido, en su momento, Martín Insaurralde. El exintendente de Lomas de Zamora es ahora el jefe de Gabinete de Kicillof.

A 14 meses del lanzamiento, qué medidas se ejecutaron del plan que contemplaba salarios, la formación de 10.000 nuevos agentes, la incorporación de 2.200 nuevos móviles policiales, la construcción de 4.000 paradas seguras con cámaras y diversos dispositivos de seguridad; la realización de obras en 96 comisarías de los 24 partidos del Gran Buenos Aires y la construcción de 12 unidades carcelarias con capacidad para alojar a 5.000 internos.

##### El tironeo con los intendentes por la seguridad y las paradas seguras

La implementación del Programa trajo aparejado un tironeo entre Berni y los intendentes por el control del dinero para la obtención de equipamiento. En pocas palabras, los jefes comunales se negaban a que los fondos sean repartidos por el Ministerio de Seguridad: querían manejarlo con autonomía. Las idas y vueltas por esa discusión demoró la puesta en marcha del Programa, indicaron a TN fuentes municipales. Recién en diciembre, los recursos de Nación llegaron a las arcas de la Provincia.

El plan se puso en marcha de forma oficial en Almirante Brown, con la entrega de 113 patrulleros, motos y equipamiento tecnológico, al filo del nuevo año. Pero la discusión por algún punto que resultó controversial en el convenio retrasó aún más los llamados a licitación, y por ende la compras.

En lo que va del año, solo Lanús recibió 300 millones de pesos con los que se adquirieron móviles y cámaras de seguridad. Unas 30 motos no pueden ser entregadas por cuestiones formales vinculadas al convenio firmado con Provincia, detallaron fuentes de ese municipio.

En tanto, la Provincia transfirió más de $178,213,243 a Vicente López, de los cuales se ejecutaron $161,542,563.40. En el municipio que encabeza Jorge Macri se adquirieron móviles policiales, blindaje (equipamiento y accesorios); equipos de radiocomunicación; 100 cámaras y se destinó una parte al mantenimiento de los móviles.

Por otro lado, el plan a ejecutarse incluía el despliegue de unos 4 mil agentes federales. Berni pretendía control pleno, pero el Presidente decidió que Frederic articule de forma directa la distribución de los efectivos con los intendentes. Un punto que generó resquemor entre el funcionario de Kicillof y Alberto Fernández, aunque el gobierno bonaerense afirma que el ministro de Seguridad tuvo injerencia en la distribución de los recursos y que tiene diálogo con todos los jefes comunales, independientemente de su signo político. Este martes, Aníbal Fernández indicó que ya son 6 mil los integrantes de fuerzas de seguridad bajo su órbita que recorren el Conurbano.

En tanto, en La Plata, aseguran que no fueron incluidos en el esquema. Incluso, alegan que el municipio destina más de 5 millones de pesos por mes en nafta para los patrulleros porque los móviles se quedan sin combustible, en medio de los recorridos. En octubre, la Provincia firmó un convenio con YPF para subsanar esa dificultad.

En la capital provincial hay un 39% más de robos, 29% más de hurtos y los asaltos se incrementaron en un 18%. Además, se triplicaron las entraderas y subió un 13% el robo automotor. En este contexto, Julio Garro reclama que se traspase el control de la responsabilidad de la seguridad al municipio.

En tanto, las paradas seguras, que implican la instalación de cámaras y un botón antipánico, generaron rechazo en algunas intendencias debido a que no cuentan con la logística necesaria para acudir al auxilio de una víctima en esas circunstancias. No obstante, el gobierno bonaerense ya instaló algunas en distritos en rojo.

##### Equipamiento de la policía bonaerense y la situación en las cárceles

Fuentes de la gobernación indicaron que se crearon 14 Superintendencias de Seguridad Regionales, 58 estaciones de Policía de Seguridad Departamental y otras 77 de Seguridad Comunal. Además, entregaron 2.697 móviles, 785 adquiridos por el Ministerio de Seguridad bonaerense y 1912, mediante el Fondo de Fortalecimiento.

Destacan, además, la creación de 20 talleres logísticos para el mantenimiento y reparación de los patrulleros. Pusieron en marcha 10 centros de formación y se incorporaron 1285 cadetes.

También se creó la Escuela de Manejo y Mantenimiento de Móviles policiales, en la que se capacitó a 2159 conductores de móviles y 173 motoristas. En una nueva escuela de tiro y armamento, se capacitó a 78.522 efectivos.

Asimismo, se reoganizó la logística y el despliegue policial para enfrentar la pandemia: se instalaron 58 bases de acantonamiento con 2500 policías.

Desde el inicio de la gestión, la gobernación comunicó que se adquirieron 500 camionetas patrulleras, 250 motos de 150 cc, 80 minibusus, 35 camionetas 4x2, 34 furgones para el traslado de detenidos, 20 camiones 4x4, 15 ambulancias, 12 camiones con hidrogrúa, 12 cuatriciclos, 6 tratolevadores hidraúlicos, 6 carretones viales, 6 trailers, 5 grúas livianas, 4 furgones, 2 camiones, 1 grúa, 1 manipulador telescópico, 1 helicóptero y 6750 chalecos antibalas.

Prevén una inversión de $1.920 millones para el fortalecimiento en el ámbito rural. En tanto, se inauguraron 1.350 nuevas plazas en las cárceles.

Salarios y formación de la policía bonaerense

En el último informe de gestión entregado a la Legislatura en septiembre, la cartera a cargo de Berni destacó, en primer lugar el incremento del sueldo de 39.342 policías. El plan había empezado con el pie izquierdo: ningún funcionario había contemplado en el anuncio los sueldos de quienes integran la fuerza más numerosa del país. El yerro derivó en una protesta policial que mantuvo en vilo al país. Un segundo anuncio, tras la quita de 1 punto de coparticipación a la Ciudad que le valió a Fernández un quiebre en la relación con Horacio Rodríguez Larreta, sí incluía la recomposición salarial de los efectivos, que debió ser reforzada semanas atrás debido a la inflación.

En este punto, se elevó la remuneración de 39.342 oficiales de policía; se aumentó el monto que se destina a cada efectivo para la compra de uniformes y se triplicó el valor de las horas CORES (aunque se limitó la cantidad); se otorgó un bono para ayudar ante la situación de pandemia de $5.000 de bolsillo y en enero 2021, se volvió a incrementar el viático diario del Operativo Sol.

El gobierno bonerense enumera dentro de las medidas la implementación de “un plan estratégico para el desarrollo de estándares profesionales similares a los de las fuerzas federales”. En ese sentido, “se puso en funcionamiento el Instituto Universitario Juan Vucetich con el objeto de avanzar en la jerarquización de la fuerza” y “se conformó una red de centros de entrenamiento para la práctica periódica de operaciones policiales conforme a los estándares internacionales en el uso racional de la fuerza”.

##### La situación de la seguridad en La Matanza tras el crimen de Roberto Sabo

El homicidio del kiosquero Roberto Sabo en Ramos Mejía provocó una masiva marcha en reclamo de seguridad en esa localidad de La Matanza. Desde el municipio, deslizaron que el crimen no está vinculado a la falta de prevención, incluso, mencionaron que en ese momento se desarrollaban tres operativos en la zona y atribuyeron el desenlace a la determinación del asaltante: “Salió a matar”, opinaron.

Asimismo, detallaron que la intendencia, que conduce Fernando Espinoza, adquirió 250 patrulleros nuevos, instaló 3.500 alarmas comunitarias, 15 Torres de Vigilancia con conexión satelital y cámaras 360. Apuntaron que se están instalando 2000 nuevas cámaras de seguridad que se suman a las 1000 con las que ya cuenta el distrito.

También destacaron que el 90% del combustible de los patrulleros es suministrado por el Municipio y que se están instalando anillos de seguridad con detectores de patentes en los accesos y que están en proceso de instalación “tótems de seguridad” en los principales centros comerciales, que cuentan con cámaras y botones antipánico.

Por otro lado, al igual que lo hizo Frederic cuando le preguntaron por la situación en Rosario, en La Matanza también se comparan con la Ciudad de Buenos Aires: “El presupuesto de CABA supera los 380 mil millones al año, el de La Matanza es de 19.700 millones al año; el director de Parques y Paseos de CABA tiene el mismo presupuesto que toda La Matanza; CABA tiene 25.000 efectivos de seguridad, contando las fuerzas federales; La Matanza tiene cerca de 4500 y tenía menos con Mauricio Macri y María Eugenia Vidal″, argumentan a la vez que detallan que los delitos prevenibles bajaron durante 2020 un 27%.

Al mismo tiempo, aclararon: “No es la responsabilidad del municipio la seguridad, sino de la Provincia” y destacaron la actuación de la policía que atrapó a la pareja que atacó al comerciante minutos más tarde.

Por último, responsabilizaron a un referente vecinal, ahora candidato a concejal por Juntos por el Cambio, por los incidentes registrados en la manifestación.