+++
author = ""
date = 2021-06-13T03:00:00Z
description = ""
image = "/images/carne_kosher_agrofy_news-1.jpg"
image_webp = "/images/carne_kosher_agrofy_news.jpg"
title = "EMBAJADA DE ISRAEL: \"NO PUEDE SER QUE CADA VEZ QUE A LA ARGENTINA SE LE DE LA GANA, ISRAEL SE QUEDE SIN CARNE\""

+++

**_La decisión del Gobierno de restringir la exportación de carne vacuna podría generar un conflicto diplomático con Israel. Galit Ronen, la embajadora de ese país en Argentina, afirmó hoy: “Si no podemos saber que nos van a vender carne de forma regular, vamos a buscar otros lugares”._**

Según un relevamiento realizado por la Cámara de la Industria y Comercio de Carnes y Derivados (CICCRA), durante los primeros cuatro meses de 2021 -antes de las restricciones- las ventas a Israel sumaron 12.266 toneladas peso producto en cuatro meses y resultaron 18,2% mayores que las de un año atrás (6,2% del total). Por estas ventas, Argentina facturó 84,2 millones de dólares. En 2020, Israel importó de la Argentina 27.310 toneladas peso producto, un 15,1% más que en 2019.

Durante una charla con el Colegio de Abogados de La Plata, Ronen señaló que “no puede ser que cada vez que le da ganas a la Argentina, Israel se queda sin carne”, y aseguró que mantuvo conversaciones con diferentes funcionarios de Argentina.

“Hablé, y les sugerí que se nos dé una cuota como Hilton con la Unión Europea, o de carne kosher a Estados Unidos, pero esto es una solución que demora; hacer una cuota es un proceso legal que lleva tiempo. Fueron muy amables, pero al final no me dieron el sí, supongo que ellos piensan que la solución (del conflicto) está cerca, con lo cual consideran que no hay que tener una solución especial para Israel”, completó la embajadora en el país.

El cepo a las exportaciones vence el domingo 20. A una semana de ese plazo, desde el consorcio de exportadores de Carne ABC esperan que en los próximos días el Gobierno brinde una respuesta a la propuesta que presentaron y que estaría pensada en “destinar mayor cantidad de cortes al mercado doméstico”.

Esta decisión generó, al mismo tiempo, un conflicto con el campo, pero también en el nivel de las relaciones diplomáticas, sobre todo con Israel, país que ya hizo manifiesto su descontento por la votación de Argentina en el Consejo de Derechos Humanos de la Organización de Naciones Unidas (ONU) del 27 de mayo sobre el conflicto bélico en Medio Oriente, por considerar que esa resolución “no menciona a Hamas ni el derecho de Israel a defenderse”.

Las exportaciones de carne vacuna se derrumbaron 35% en mayo último y los frigoríficos pudieron cumplir apenas con el 65% de sus planes de venta al exterior, en medio de las restricciones impuestas por el Gobierno, según datos difundidos por el sector. El mes pasado se exportaron 45.200 toneladas, por debajo de las 69.500 de abril.

Los frigoríficos sostienen que con las restricciones a las exportaciones de carne “se interrumpió el muy buen desempeño que venía mostrando la actividad”. Según un relevamiento de CICCRA, en mayo se colocaron en el exterior poco más de 45 mil toneladas de res con hueso.

Miguel Schiariti, presidente de CICCRA, dijo que estimaron esos números a partir de encuestas realizadas a los exportadores, para conocer lo antes posible el impacto de la medida que restringió las ventas externas. “Es un 65% de lo que los frigoríficos tenían producido para exportar y con la declaración jurada aprobada”, lamentó.

En abril último, las exportaciones antes de la restricción oficial habían dejado ingresos por USD 203,7 millones, con un precio promedio de 2.931 dólares la tonelada. En mayo último, con el recorte de las ventas al exterior a 45.200 toneladas, se habría vendido por apenas USD 132,4 millones. Así, con la medida anunciada el 17 de mayo último y oficializada el 20 de ese mes, se estima una pérdida de ingresos por USD 71,3 millones.

Según Schiariti, los exportadores están esperando una definición sobre el futuro de los negocios con mercadería ya lista para vender. Sostuvo que los cortes están envasados, preparados y con el ticket correspondiente.

“Hay mucha incertidumbre. Cuando se vuelva a exportar, a China que es el principal comprador, te van a querer sacar 300 dólares en el valor de la tonelada”, se quejó.

También lamentó que la suspensión de las exportaciones de carne vacuna “demolió los excelentes números que arrojó abril”. “Fue el mejor abril de la historia. En términos interanuales se registró un crecimiento de 8,4%. En abril, 8 kilos de cada 10 kilos exportados fueron enviados a China, país que explicó casi la mitad del crecimiento del volumen exportado por los frigoríficos argentinos en comparación con el mismo mes de 2020”, indicó el informe de Ciccra.

En el primer cuatrimestre del año, las ventas al exterior sumaron 278.600 toneladas de res con hueso, 20% por encima de enero-abril de 2020, y un nuevo récord para esa época.

Ciccra alertó que el cierre de exportaciones generó “pérdida de horas trabajadas, básicamente por la industria exportadora, dado que la mayoría de los establecimientos anticiparon vacaciones, suspendieron con garantía horaria y terminaron los contratos con el personal contratado”.

La cámara resaltó que toda la cadena de la carne tiene 422.300 empleos directos. También llamó a “no repetir la experiencia del anterior cierre de exportaciones e intervención en la carne, desde 2006, porque llevó a la pérdida de 19.800 puestos de trabajo”.

En mayo de 2021 se produjeron 217 mil toneladas de res con hueso, un 9,7% menos que en abril último, y 17,4% por debajo de mayo de 2020.