+++
author = ""
date = 2021-09-23T03:00:00Z
description = ""
image = "/images/lzjyl4jgmbhdjotsnac2vj2uq4.jpg"
image_webp = "/images/lzjyl4jgmbhdjotsnac2vj2uq4.jpg"
title = "LA CONFIANZA EN EL GOBIERNO ALCANZÓ EL PEOR REGISTRO DESDE EL INICIO DE LA GESTIÓN"

+++

#### **El indicador elaborado por la Universidad Torcuato Di Tella (UTDT) arrojó que la percepción, inclusive, se encuentra un 20% por debajo de la última medición de la administración de Mauricio Macri.**

El gobierno de Alberto Fernández alcanzó el peor registro en el Índice de Confianza en el Gobierno (ICG), que elabora periódicamente la Universidad Torcuato Di Tella (UTDT), desde diciembre de 2019, cuando asumió como Presidente. El relevamiento es coincidente con el cimbronazo electoral del 12 de septiembre de 2021.

Durante el mes de septiembre, el indicador alcanzó 1,58 puntos, lo que implica una baja de 14,7% en comparación con respecto a agosto. La evolución agrava la caída de la administración del Frente de Todos al colocarla en un 20% por debajo de la peor imagen de la que exhibía el gobierno de Mauricio Macri en su último mes en el poder, cuando esta se ubicaba en 1,97 puntos. Además, es 32% inferior al del primer mes completo del gobierno de Alberto Fernández, en enero de 2020.

En términos interanuales, el índice relevado en el mes de septiembre disminuyó 30 por ciento.

“La pronunciada caída del ICG de septiembre, desde un nivel anterior que ya era bajo, permite dar cuenta de la derrota electoral del gobierno en las PASO legislativas del 12 de septiembre”, dice el reporte, cuyo trabajo de campo finalizó dos días antes de la elección.

“Es particularmente notable la abrupta caída en el interior del país, donde la coalición de gobierno tuvo un inusualmente pobre desempeño electoral, y también entre los ciudadanos de menor nivel educativo”, señala el documento supervisado por los investigadores Carlos Gervasoni y Javier Zelaznik.

El análisis de la casa de altos estudios tiene como intención detectar los cambios de opinión pública sobre la gestión del Poder Ejecutivo. El diseño del ICG evalúa lo que los ciudadanos piensan sobre aspectos como la imagen general del Gobierno; la percepción sobre si se gobierna pensando en el bien general o en el de sectores particulares; la eficiencia en el gasto público; la honestidad de los miembros del Gabinete y la capacidad del Gobierno para resolver los problemas del país.

La encuesta, realizada de manera telefónica entre el 30 de agosto y el 10 de septiembre por la consultora Poliarquía, relevó que la variación intermensual con respecto a agosto fue “negativa y significativa” en todos los componentes del ICG: Eficiencia en la administración del gasto público (1,24 puntos, -21,0%), Capacidad para resolver los problemas del país (1,75 puntos, -18,0%), Evaluación general del gobierno (1,34 puntos, -16,8%), Preocupación por el interés general (1,58 puntos, -10,8%) y Honestidad de los funcionarios (1,99 puntos, -8,7%).

La muestra, además, detectó un cambio de tendencia en la base de sustentación del Frente de Todos. Aunque en meses anteriores hubo una inclinación “ligeramente mayor” positiva sobre la gestión de Alberto Fernández, en septiembre el ICG “fue mayor entre los hombres, entre quienes viven en los partidos del GBA” y quienes “alcanzaron sólo educación primaria”.

Sin embargo, la caída fue importante entre los varones: el valor del ICG alcanzó 1,69 puntos, con una disminución de 17,6% respecto de agosto; mientras que entre las mujeres fue de 1,49 puntos, con una variación negativa de 10,2 por ciento.

En ese grupo más favorable, también aparecen aquellos que “dicen no haber sido (ellos o sus familias) víctima de delitos en los últimos 12 meses, y entre quienes creen que la situación económica del país dentro de un año será mejor que la actual”.

Contrastando con mediciones anteriores, no se registraron diferencias significativas de acuerdo al grupo etario.

En lo que refiere a percepción según zona geográfica, las personas encuestadas en el GBA promediaron los 2 puntos en el índice, con una disminución de 2,4% respecto de agosto. A diferencia del mes anterior, le sigue la Ciudad de Buenos Aires (1,48 puntos), con una variación negativa de 5,1 por ciento; y finalmente el interior se ubicó en 1,39 puntos, con una notable caída de 22,8 por ciento.

Acerca de la variable sobre la situación económica, el ICG fue mayor entre quienes creen que mejorará dentro de un año (3,76 puntos, con una disminución de 8,1% respecto de agosto), que entre quienes creen que se mantendrá igual (1,62 puntos, con una caída de 23,2%) o que empeorará (0,47 puntos), con un desplome de 53,5 por ciento.

“Es probable que esta correlación sea causalmente bidireccional: el optimismo (pesimismo) sobre el futuro económico refuerza (debilita) la confianza en el gobierno, pero a la vez una mayor (menor) confianza en el gobierno genera mayor (menor) optimismo”, sintetiza el informe.