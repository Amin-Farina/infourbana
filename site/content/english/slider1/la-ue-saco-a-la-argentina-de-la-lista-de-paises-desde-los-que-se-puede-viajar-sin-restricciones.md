+++
author = ""
date = 2022-01-17T03:00:00Z
description = ""
image = "/images/pcr-truchos-550x309-1.jpg"
image_webp = "/images/pcr-truchos-550x309.jpg"
title = "LA UE SACÓ A LA ARGENTINA DE LA LISTA DE PAÍSES DESDE LOS QUE SE PUEDE VIAJAR SIN RESTRICCIONES"

+++

#### **_Había sido integrada el 29 de octubre a la lista de “naciones seguras” por la situación epidemiológica. La medida también afecta a Australia y Canadá._**

La Unión Europea quitó a la Argentina, Australia y Canadá de la lista de países del bloque desde los que se pueden viajar sin restricciones, por la alta cantidad de casos de coronavirus.

La Argentina había sido incorporada el 29 de octubre a la nómina de “naciones seguras” para viajar por la situación epidemiológica.

> El Consejo de la UE anunció en un comunicado que tomó la decisión “tras una revisión en el marco de la recomendación sobre el levantamiento gradual de las restricciones temporales a los viajes no esenciales a la UE”.

El organismo actualizó “la lista de países, regiones administrativas especiales y otras entidades y autoridades territoriales para los que deben levantarse las restricciones de viaje” y mencionó que la Argentina “Australia y Canadá fueron eliminados de la lista”.

La UE sacó a la Argentina de la lista de países desde los que se puede viajar sin restricciones

> La UE comunicó que “los criterios para determinar los terceros países para los que se debe levantar la actual restricción de viaje se actualizaron el 20 de mayo de 2021″.

Explicaron que los criterios “cubren la situación epidemiológica y la respuesta general al COVID-19, así como la confiabilidad de la información y las fuentes de datos disponibles”.

##### **¿Cuáles son los países que integran la lista de “países seguros” para viajar?**

El consejo de la UE comunicó que “sobre la base de los criterios y condiciones establecidos en la recomendación, a partir del 17 de enero de 2022, los Estados miembros deben levantar gradualmente las restricciones de viaje en las fronteras exteriores para los residentes de los siguientes terceros países”:

* Bahréin
* Chile
* Colombia
* Indonesia
* Kuwait
* Nueva Zelanda
* Perú
* Qatar
* Ruanda
* Arabia Saudita
* Corea del Sur
* Emiratos Árabes Unidos
* Uruguay
* China (sujeta a confirmación de reciprocidad)

El Consejo de la UE aclaró que los cambios en la lista “tienen en cuenta los posibles riesgos que plantean las nuevas variantes al establecer un mecanismo de freno de emergencia para reaccionar rápidamente ante la aparición de una variante de interés o preocupación en un tercer país”.

Desde el organismo europeo mencionaron que la recomendación del Consejo “no es un instrumento jurídicamente vinculante” y que “las autoridades de los estados miembros siguen siendo responsables de implementar el contenido de la recomendación”.

Aclararon que “un Estado miembro no debe decidir levantar las restricciones de viaje para terceros países no incluidos en la lista antes de que se haya decidido de manera coordinada”.

##### ¿Cuáles son las vacunas contra el coronavirus autorizadas por Europa?

La Agencia Europea del Medicamento (EMA, por sus siglas en inglés) autorizó hasta el momento cinco vacunas contra el coronavirus: BioNTech/Pfizer, Moderna, AstraZeneca, Janssen y Novavax. 

La EMA todavía no autorizó la inmunización rusa Sputnik V, de la que advirtió que le faltan algunos datos necesarios para la revisión.