+++
author = ""
date = 2021-10-24T03:00:00Z
description = ""
image = "/images/sjmgwtnyne_1256x620.jpg"
image_webp = "/images/sjmgwtnyne_1256x620.jpg"
title = "MATARON A BALAZOS A UN EXJEFE DE LA BARRA BRAVA DE NEWELLS"

+++

##### **_Un exjefe de la barrabrava de Newell’s Old Boys de Rosario, Nelson “Chivo” Saravia, fue asesinado esta madrugada de varios disparos cuando cuatro personas irrumpieron en su domicilio y lo balearon en el interior de la vivienda, informaron hoy fuentes judiciales._**

El crimen de Saravia fue el tercero en 12 horas ocurrido en la ciudad santafesina, luego de la multitudinaria concentración del último jueves en reclamo por justicia y seguridad tras el asesinato del arquitecto Joaquín Pérez durante un intento de robo de su auto.

Según informó el Ministerio Público de la Acusación (MPA), el “Chivo” Saravia fue asesinado en su casa de San Nicolás al 3700, en la zona sudoeste de Rosario.

Ese domicilio había sido baleado en 2015 cuando la víctima estaba al frente de la barrabrava "Leprosa", durante una de las transiciones en el poder de los paravalanchas que compartió con otras dos personas asesinadas en 2016: Matías “Cuatrerito” Franchetti y Maximiliano La Rocca.

De acuerdo con los primeros datos aportados a la investigación, a cargo de la fiscal Marisol Fabbro, cuatro personas ingresaron alrededor de las 2 de esta madrugada a la vivienda de Saravia, que aparentemente estaba descansando.

Los agresores efectuaros varios disparos hacia el cuerpo del “Chivo” y escaparon en un automóvil, cuyas características la Fiscalía se “reserva” para agilizar la investigación.

La víctima fue trasladada en un vehículo particular al hospital de Emergencias de esta ciudad, donde los médicos diagnosticaron su muerte por múltiples heridas de arma de fuego, dijeron voceros del caso.

El cuerpo de Saravia fue enviado al Instituto Médico Legal para la autopsia de rigor, y la fiscal Fabbro solicitó la intervención de Gabinete Criminalístico policial para el relevamiento de la escena del crimen, el levantamiento de rastros y material balístico, que será enviado a peritar.

Si bien no se descartaba ninguna hipótesis en la reciente pesquisa, voceros del caso indicaron que entre ellas estaba la de alguna interna o vieja deuda de la barrabrava de Newell’s.

Saravia llegó a la barra durante la conducción de Diego “Panadero” Ochoa, que había desplazado a Roberto “Pimpi” Camino, asesinado en marzo de 2010.

Ochoa fue arrestado en 2013 y luego condenado como instigador del crimen de Camino y de otro joven vinculado a la narcocriminalidad, Maximiliano “Quemadito” Rodríguez, cuyo padre cumple condena por el triple crimen de militantes sociales de Rosario en Año Nuevo de 2012.

Aparentemente, Saravia se había alejado hace unos años del grupo que conduce la hinchada del club ubicado en el Parque Independencia.