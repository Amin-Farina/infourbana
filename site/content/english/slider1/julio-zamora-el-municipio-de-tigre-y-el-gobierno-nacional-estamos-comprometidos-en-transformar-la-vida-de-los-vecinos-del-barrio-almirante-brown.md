+++
author = ""
date = 2021-08-07T03:00:00Z
description = ""
image = "/images/zamora-1.jpg"
image_webp = "/images/zamora.jpg"
title = "JULIO ZAMORA: \"EL MUNICIPIO DE TIGRE Y EL GOBIERNO NACIONAL ESTAMOS COMPROMETIDOS EN TRANSFORMAR LA VIDA DE LOS VECINOS DEL BARRIO ALMIRANTE BROWN\""

+++

**_El intendente Julio Zamora junto a la concejala Gisela Zamora encabezaron la inauguración de la plazoleta Almirante Brown, ubicada en el barrio homónimo de Tigre centro._**

Se trata del espacio público N°61 renovado en la gestión del jefe comunal, que ahora cuenta con postas aeróbicas, juegos para niños y niñas, mesa de ping-pong, canchas de fútbol tenis, nuevo mobiliario e iluminación y bancos.

"Es una gran alegría haber intervenido este espacio en un barrio donde no abundan los lugares de encuentro para los vecinos. Lo pudimos hacer gracias a la colaboración de un benefactor que nos brindó los juegos, y nosotros ayudamos con la obra pública. Buscamos mejorar la calidad de vida en esta comunidad, dándole los servicios y la modernidad que tiene que tener. Por eso el Municipio de Tigre y el Gobierno Nacional estamos comprometidos en transformar la vida de los vecinos de Almirante Brown, y seguiremos trabajando para lograrlo", destacó el intendente.

El encuentro inició con un show del mago Reymon, para el disfrute de los niños y niñas. Luego, el intendente acompañado de los vecinos del barrio realizaron el corte de cinta, que dio por inaugurada la plazoleta. También descubrieron una placa con el nombre del espacio y plantaron un Lapacho como símbolo del crecimiento de la ciudad. Acto seguido, presentaron un banco rojo en conmemoración de las víctimas de femicidios y violencia de género.

La concejala Gisela Zamora sostuvo: "Ya son 61 los espacios que renueva el Municipio, con la premisa de crear lugares donde las familias puedan disfrutar de actividades deportivas al aire libre. Queremos que estas plazas estén en cada barrio y que los vecinos y vecinas no tengan que movilizarse para encontrar espacios públicos de calidad."

La obra se encuentra dentro del plan de remodelación que impulsa el Municipio de Tigre. Ya se renovaron plazas en las localidades de El Talar, General Pacheco, Benavídez, Don Torcuato, Tigre centro, Rincón de Milberg, Villa La Ñata, Ricardo Rojas y Troncos del Talar.

La verdad que es increíble lo que hicieron, estoy muy agradecida con el intendente Zamora. Ahora los chicos van a tener un lugar para jugar tranquilos cerca de sus casas, contó María, vecina de Almirante Brown. En tanto, Paula, también vecina del barrio, dijo: "Estoy muy emocionada con la nueva plaza, voy a venir todos los días."

Estuvieron presentes: el presidente del Honorable Concejo Deliberante, Fernando Mantelli; los concejales Javier Parbst, Rodrigo Molinos y Sandra Rossi; el secretario General, de Obras y Servicios Públicos, Pedro Heyde; la secretaria de Desarrollo Social y Políticas de Inclusión, Cecilia Ferreira; la secretaria de Protección Ciudadana, Ximena Guzmán; el secretario de Desarrollo Económico y Relaciones con la Comunidad, Emiliano Mansilla; el secretario de Comunicación, Ignacio Castro Cranwell; el presidente del Consejo Escolar, Adrián Pintos y el delegado de Tigre Centro, Miguel Escalante.