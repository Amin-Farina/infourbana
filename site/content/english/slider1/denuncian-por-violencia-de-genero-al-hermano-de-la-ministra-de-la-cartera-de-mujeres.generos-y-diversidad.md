+++
author = ""
date = 2021-09-20T03:00:00Z
description = ""
image = "/images/gomez-alcorta-1.jpg"
image_webp = "/images/gomez-alcorta.jpg"
title = "DENUNCIAN POR VIOLENCIA DE GÉNERO AL HERMANO DE LA MINISTRA DE LA CARTERA DE MUJERES. GÉNEROS Y DIVERSIDAD  "

+++

#### **_El hermano de la ministra Elizabeth Gómez Alcorta, a cargo de la cartera de las Mujeres, Géneros y Diversidad en el gabinete de Alberto Fernández, fue denunciado por su expareja por violencia de género._**

Si bien la Justicia desestimó la acusación por "violencia familiar", apercibió al acusado para que cese "en todo acto que signifique violencia verbal o psicológica, intromisión injustificada, perturbación o intimidación".

Se trata de Raúl Guillermo Gómez Alcorta, quien fuera acusado por Sofía Cristiani, su ex, con quien tiene un hijo en común, ante la Oficina de Violencia Doméstica (OVD).

La mujer se presentó el pasado 3 de septiembre ante el organismo que depende de la Corte Suprema de Justicia, aunque la denuncia trascendió este martes con documentación judicial.

En su calidad de denunciante, Cristiani expuso situaciones de "violencia psicológica, amenazas y hostigamiento" tras su separación de Gómez Alcorta y en el marco del régimen de visitas que acordaba para su hijo.

Esa presentación en la OVD derivó en una causa judicial, abierta en el juzgado civil 84, a cargo de Mónica Fernández, donde ya tramitaba otra relativa "al régimen de comunicación de las partes" por su hijo, según aseveró la jueza cuando desestimó la denuncia por "violencia familiar".

La jueza Fernández argumentó que la OVD había tomado la presentación de la mujer como "de riesgo bajo". 

"De la evaluación de riesgo efectuada en la OVD, surge que se trataría de una conflictiva vincular en el marco de las desavenencias relativas al ejercicio de la responsabilidad parental", sostuvo la jueza en su dictamen.

Y fue más allá: Fernández pareció cuestionar a la denunciante por recurrir a la oficina de la Corte, al considerar que para presentarse en ese organismo "la denuncia debe estar referida a hechos graves y actuales y no a situaciones ocurridas con anterioridad, que aparecen como salvadas por las partes".

#### "Cesar en todo acto"

No obstante, la jueza advirtió al hermano de la funcionaria. "Sin perjuicio de lo dispuesto precedentemente, hágase saber al Sr. Gómez Alcorta Raúl Guillermo que deberá cesar en todo acto que signifique violencia verbal o psicológica, intromisión injustificada, perturbación o intimidación, respecto de Cristiani Sofía", sostuvo.

Pero también, mientras descartaba su denuncia por violencia, intimó a la mujer a no interrumpir el normal cumplimiento del régimen de comunicación vigente entre su expareja y su hijo. 

Junto al fallo, el sitio MDZ aportó textuales de lo que la mujer habría denunciado ante la oficina de la Corte. “'Egoísta, mala madre' (me dice). Me amenaza con que tengo que volver, me manda leyes que no sé, y me dice que puede mandarme a la policía. Me manda imágenes de que está sentado en la puerta de mi casa: 'Acá estoy esperando', y me vuelve a mandar fotos dos horas después: 'Más te vale que vuelvas. 'Hacé lo que te conviene'", consignaron sobre la denuncia de la mujer.

Elizabeth Gómez Alcorta llegó a la función pública tras defender a la dirigente social kirchnerista Milagro Sala. Desde la asunción de Alberto Fernández, está a cargo de la cartera que rige sobre las políticas de género, una promesa presidencial que se hizo Ministerio, tras los reclamos por la ola de femicidios que se canalizó ante el reclamo de Ni Una Menos.