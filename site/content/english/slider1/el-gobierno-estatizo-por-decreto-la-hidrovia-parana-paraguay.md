+++
author = ""
date = 2021-07-01T03:00:00Z
description = ""
image = "/images/barcos_fila_parana_1614949520-1.jpeg"
image_webp = "/images/barcos_fila_parana_1614949520.jpeg"
title = "EL GOBIERNO ESTATIZÓ POR DECRETO LA HIDROVÍA PARANÁ-PARAGUAY"

+++

**_Finalmente el Gobierno decidió estatizar la Hidrovía. En principio es por un año y la medida se implementó hoy mediante la publicación en el Boletín Oficial del Decreto de Necesidad y Urgencia 427 ._**

El control estará a cargo de la Administración General de Puertos (AGP). Además, la decisión del Gobierno puede ser prorrogable hasta la toma del servicio por parte de quienes resulten adjudicatarios de la licitación.

El Decreto lleva la firma del presidente Alberto Fernández, el jefe de gabinete, Santiago Cafiero, y el ministro de Transporte, Alexis Guerrera. El mismo otorgó la concesión de la operación para el mantenimiento del sistema de señalización, tareas de dragado y redragado y el correspondiente control hidrológico de la Vía Navegable Troncal a la AGP. La misma comprende el tramo del kilómetro 1238 del río Paraná hasta la Zona de Aguas Profundas Naturales en el río de La Plata exterior.

Hay que recordar que por las aguas de la Hidrovía circula el 90% de las exportaciones agroindustriales del país, por lo que es el canal principal de salida para el complejo agroexportador más importante de América del Sur. Desde hace 25 años, el manejo de la Hidrovía está en manos de la empresa belga Jan de Nul y de su socia local, Emepa.

En los últimos días, el ministro de Transporte de la Nación, Alexis Guerrera, en diálogo con El Destape Radio, adelantó que la Administración General de Puertos tomará el mando de la “licitación corta” previa a la “licitación larga” para la Hidrovía. “Por 12 meses tendrá un control estatal y comienza a funcionar el cobro del canon por parte del Estado para pagarle a las concesionarias las tareas realizadas, pero no altera y modifica la obra de lo que hoy se viene haciendo”, comentó el funcionario. A todo esto, en el Congreso de la Nación el Frente de Todos promueve la creación de una Comisión Bicameral. La iniciativa ya tiene la media sanción del Senado, y en los últimos días se aprobó el dictamen en comisión de Diputados.

#### Alcances del Decreto

El Decreto también contempla que “la base tarifaria y el mecanismo de financiamiento por el sistema tarifa o peaje que percibirá la Administración General de Puertos Sociedad del Estado en el marco de la Ley N° 17.520 y sus reglamentaciones, sin perjuicio de las subvenciones, subsidios y/o transferencias de aportes del Estado Nacional que transitoriamente resulte necesario efectuar, a los fines establecidos en el contrato. El procedimiento para la eventual revisión de tarifas y/o peajes, contemplando la participación u opinión de los usuarios, entidades representativas de los sectores afines y obligados al pago”.

Por otro lado, en los considerandos del Decreto se señaló que la Administración General de Puertos “posee la capacidad técnica para planificar la ejecución, por sí o por terceros, de los actos y obras necesarias para la adecuada prestación de los servicios de dragado, balizamiento y control hidrológico de la Red Navegable Troncal, asegurando la presencia estatal en el cumplimiento de las prestaciones que actualmente se realizan a riesgo empresario”. También sostienen que la AGP “cuenta con el formato jurídico y técnico apropiado para cumplir con los mencionados cometidos hasta tanto se desarrolle la licitación pública prevista en el Decreto N° 949/20″.

#### Repercusiones

En los últimos tiempos creció el malestar y la incertidumbre en el ámbito privado por los proyectos del Gobierno en torno a la Hidrovía. A horas de publicarse el Decreto, el presidente de la Cámara de Puertos Privados Comerciales, Luis Zubizarreta dijo en diálogo con este medio: “Hay un licitación corta como como definió el ministro de Transporte, que veremos cómo organiza la AGP. Una parte mala de la medida es que ya se define que el peaje lo va a cobrar dicho organismo, con lo cual eso va a encarecer el sistema, porque cualquiera que se presente en una licitación que tiene que cobrarle al Estado le agregará una prima de riesgo y de esa manera toda la economía argentina va a estar soportando un peaje más alto sin sentido”.

Y agregó: “En los ´90 el calado era de 25 a 26 pies y estaba a cargo del Estado a través del ministerio de Obras Públicas con dragas viejas. Hoy tenemos 34 pies y estamos próximos a llegar a 40 pies en la próxima licitación. La actual concesión generó un cambio mayúsculo en la logística de todo el complejo agroindustrial”.

Por otro lado, el presidente de la Bolsa de Comercio de Rosario, Daniel Nasini, aseguró en diálogo con este medio, que la decisión del Gobierno “genera preocupación, porque no da certidumbre ni lineamientos de largo plazo que aseguren la continuidad de la vía de navegación más importante que tiene el país. Es imperioso garantizar la continuidad del servicio a corto plazo, especialmente dadas las circunstancias extraordinarias de la bajante histórica del Paraná. Para ello es indispensable que las obras sigan en manos de empresas especializadas en la materia, seleccionadas bajo un riguroso y transparente proceso de licitación. El Estado no tiene ni la experiencia ni la capacidad técnica para realizar estas tareas, pero sí ejercer sus funciones indelegables de control”.

“Otro aspecto a tener en cuenta es el costo del peaje. Argentina está lejos de los principales mercados del mundo, con lo cual, todas las ineficiencias que impacten en los costos logísticos repercute negativamente en la competitividad de la producción nacional. Por ello, es fundamental que los fondos aportados por las cargas continúen siendo percibidos directamente por quien lleve adelante las obras para evitar demoras en la liquidación y asegurar la ejecución de los trabajos en tiempo y forma”, agregó Nasini.

Por último, el dirigente dijo que en los próximos dos años, como mínimo y aunque se asegure la continuidad de las obras como hasta ahora, “no se van a realizar mejoras que son necesarias para optimizar la navegación en la vía y permitan que esté acorde a los estándares de la navegación moderna. Por ello es necesario que la licitación de largo plazo se realice lo más pronto posible incluyendo desde el inicio las obras que son necesarias para mejorar la competitividad del país”.