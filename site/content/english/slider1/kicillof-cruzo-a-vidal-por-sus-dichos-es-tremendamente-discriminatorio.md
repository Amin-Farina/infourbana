+++
author = ""
date = 2021-09-01T03:00:00Z
description = ""
image = "/images/d6pul2tsanctlkyli3e2yzgbve.jpg"
image_webp = "/images/d6pul2tsanctlkyli3e2yzgbve.jpg"
title = "KICILLOF CRUZÓ A VIDAL POR SUS DICHOS: \"ES TREMENDAMENTE DISCRIMINATORIO\""

+++
#### _El gobernador bonaerense aseguró que lo que la precandidata a diputada por la Ciudad dijo sobre el consumo de marihuana “explica bien por qué se vuelve a Capital”._

El gobernador bonaerense, Axel Kicillof, criticó este miércoles a su antecesora, la actual precandidata a diputada por la Ciudad de Juntos por el Cambio, María Eugenia Vidal, quien generó polémica en las últimas horas al considerar que “fumarse un porro en Palermo con amigos” no es lo mismo que hacerlo en la villa 21-24 o la Zabaleta.

“Muestra un pensamiento tremendamente discriminador”, señaló el mandatario provincial, y aseguró que la frase “explica bien por qué se vuelve a Capital, porque lo que ocurre en la Provincia es algo que no llega a comprender”, en referencia a la candidatura de Vidal -ex gobernadora de Buenos Aires- por la Ciudad.

En diálogo con radio 10, Kicillof dijo que (a los opositores) “hay que dejarlos hablar porque cuanto más hablan más aparecen como fogonazos lo que piensan”.

Y sumó, con ironía: “Ojalá hubiera una ley de etiquetado frontal para candidatos, para que se vea bien qué piensan y opinan, porque eso es lo que luego llevan a la práctica cuando son Gobierno”.

Vidal reflexionó sobre el consumo de marihuana durante una entrevista con Julio Leiva para Filo.news. Si bien dijo estar “a favor de la libertad y de que cada uno decida qué hacer”, la exgobernadora se pronunció en contra de una eventual legalización de esa sustancia.

Así lo justificó: ”Una cosa es fumarte un porro en Palermo un sábado a la noche, relajado, con amigos, tu pareja o solo, y otra cosa es vivir en la 21-24 de Zabaleta, en la 1-11-14, rodeado de narcos, y que te ofrezcan un porro”.

“Para esos pibes la marihuana no es un consumo ocasional y de recreo, plenamente elegido, sino que es parte del inicio de un camino mucho más jodido y más duro, donde tienen muchas menos oportunidades de elegir”, sumó.

Más allá de la polémica sobre el “porro”, el actual gobernador bonaerense consideró que “la sociedad no va a perder la memoria del desastre que hicieron (Mauricio) Macri, (María Eugenia) Vidal y (Horacio Rodríguez) Larreta, que buscan que la pandemia los limpie de toda culpa”.

“Resta que den explicaciones por lo que han hecho”, expuso el mandatario en la misma entrevista. Además, planteó que “si están volviendo a presentarse, que aclaren que vienen a hacer lo mismo, porque escuché a varios candidatos del PRO diciendo que el problema (de ese gobierno) fue que el ajuste, el endeudamiento y el tarifazo Macri no los hizo rápidamente”.

Por otro lado, sobre la situación del coronavirus, Kicillof analizó que “ni el Covid se cansó de los argentinos, ni la recuperación vino como un rebote”, sino que el coronavirus “lucha contra una campaña de vacunación y estamos avanzando tan fuertemente que hay más gente inmunizada y el virus tiene menos posibilidad de contagiar, enfermar y matar”.